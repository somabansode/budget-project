using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
namespace SD.COMMON.DAL {
    public sealed class SqlHelper {
        #region Constants
        private const string _strSectionName = "ProMan/database";
        private const string _strItemName = "SQL";
        private static string _strConnString = ConfigurationManager.ConnectionStrings["ERPConnectionString"].ConnectionString;
        //private static int _intCommandTimeout = 1000;
        #endregion
        #region private utility methods & constructors
        private SqlHelper() { }
        private static void AttachParameters(SqlCommand command, SqlParameter[] commandParameters) {
            if (command == null) throw new ArgumentNullException("command");
            if (commandParameters != null) {
                foreach (SqlParameter p in commandParameters) {
                    if (p != null) {
                        // Check for derived output value with no value assigned
                        if ((p.Direction == ParameterDirection.InputOutput ||
                            p.Direction == ParameterDirection.Input) &&
                            (p.Value == null)) {
                            p.Value = DBNull.Value;
                        }
                        command.Parameters.Add(p);
                    }
                }
            }
        }
        private static void PrepareCommand(SqlCommand command, SqlConnection connection, SqlTransaction transaction, CommandType commandType, string commandText, SqlParameter[] commandParameters, out bool mustCloseConnection) {
            if (command == null) throw new ArgumentNullException("command");
            if (commandText == null || commandText.Length == 0) throw new ArgumentNullException("commandText");
            // If the provided connection is not open, we will open it
            if (connection.State != ConnectionState.Open) {
                mustCloseConnection = true;
                try {
                    connection.Open();
                } catch (Exception e) {
                    string msg = "Database connection Error! Maybe your Database is not runing or database connection string is mistake?";
                    throw new Exception(msg, e);
                }
            } else {
                mustCloseConnection = false;
            }
            // Associate the connection with the command
            command.Connection = connection;
            // Set the command text (stored procedure name or SQL statement)
            command.CommandText = commandText;
            // If we were provided a transaction, assign it
            if (transaction != null) {
                if (transaction.Connection == null) throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
                command.Transaction = transaction;
            }
            // Set the command type
            command.CommandType = commandType;
            // Attach the command parameters if they are provided
            if (commandParameters != null) {
                AttachParameters(command, commandParameters);
            }
            return;
        }
        #endregion private utility methods & constructors
        #region ExecuteNonQuery
        public static int ExecuteNonQuery(string commandText) {
            return ExecuteNonQuery(commandText, (SqlParameter[])null);
        }
        public static int ExecuteNonQuery(string commandText, params SqlParameter[] commandParameters) {
            //if (connection == null) throw new ArgumentNullException("connection");
            // Create a command and prepare it for execution
            SqlCommand cmd = new SqlCommand();
            bool mustCloseConnection = false;
            SqlConnection connection = new SqlConnection(_strConnString);
            PrepareCommand(cmd, connection, (SqlTransaction)null, CommandType.StoredProcedure, commandText, commandParameters, out mustCloseConnection);
            // Finally, execute the command
            int retval = cmd.ExecuteNonQuery();
            // Detach the SqlParameters from the command object, so they can be used again
            cmd.Parameters.Clear();
            if (mustCloseConnection)
                connection.Close();
            return retval;
        }
        public static int ExecuteNonQuery(SqlTransaction transaction, string commandText) {
            // Pass through the call providing null for the set of SqlParameters
            return ExecuteNonQuery(transaction, commandText, (SqlParameter[])null);
        }
        public static int ExecuteNonQuery(SqlTransaction transaction, string commandText, params SqlParameter[] commandParameters) {
            if (transaction == null) throw new ArgumentNullException("transaction");
            if (transaction != null && transaction.Connection == null) throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
            // Create a command and prepare it for execution
            SqlCommand cmd = new SqlCommand();
            bool mustCloseConnection = false;
            PrepareCommand(cmd, transaction.Connection, transaction, CommandType.StoredProcedure, commandText, commandParameters, out mustCloseConnection);
            // Finally, execute the command
            int retval = cmd.ExecuteNonQuery();
            // Detach the SqlParameters from the command object, so they can be used again
            cmd.Parameters.Clear();
            return retval;
        }
        #endregion ExecuteNonQuery
        #region ExecuteDataset
        public static DataSet ExecuteDataset(string commandText) {
            return ExecuteDataset(commandText, (SqlParameter[])null);
        }
        public static DataSet ExecuteDataset(string commandText, params SqlParameter[] commandParameters) {
            SqlCommand cmd = new SqlCommand();
            bool mustCloseConnection = false;
            SqlConnection connection = new SqlConnection(_strConnString);
            PrepareCommand(cmd, connection, (SqlTransaction)null, CommandType.StoredProcedure, commandText, commandParameters, out mustCloseConnection);
            // Create the DataAdapter & DataSet
            using (SqlDataAdapter da = new SqlDataAdapter(cmd)) {
                DataSet ds = new DataSet();
                // Fill the DataSet using default values for DataTable names, etc
                da.Fill(ds);
                // Detach the SqlParameters from the command object, so they can be used again
                cmd.Parameters.Clear();
                if (mustCloseConnection)
                    connection.Close();
                // Return the dataset
                return ds;
            }
        }
        public static DataSet ExecuteDataset(SqlTransaction transaction, string commandText) {
            // Pass through the call providing null for the set of SqlParameters
            return ExecuteDataset(transaction, commandText, (SqlParameter[])null);
        }
        public static DataSet ExecuteDataset(SqlTransaction transaction, string commandText, params SqlParameter[] commandParameters) {
            if (transaction == null) throw new ArgumentNullException("transaction");
            if (transaction != null && transaction.Connection == null) throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
            // Create a command and prepare it for execution
            SqlCommand cmd = new SqlCommand();
            bool mustCloseConnection = false;
            PrepareCommand(cmd, transaction.Connection, transaction, CommandType.StoredProcedure, commandText, commandParameters, out mustCloseConnection);
            // Create the DataAdapter & DataSet
            using (SqlDataAdapter da = new SqlDataAdapter(cmd)) {
                DataSet ds = new DataSet();
                // Fill the DataSet using default values for DataTable names, etc
                da.Fill(ds);
                // Detach the SqlParameters from the command object, so they can be used again
                cmd.Parameters.Clear();
                // Return the dataset
                return ds;
            }
        }
        #endregion ExecuteDataset
        #region ExecuteReader
        /// <summary>
        /// This enum is used to indicate whether the connection was provided by the caller, or created by SqlHelper, so that
        /// we can set the appropriate CommandBehavior when calling ExecuteReader()
        /// </summary>
        private enum SqlConnectionOwnership {
            /// <summary>Connection is owned and managed by SqlHelper</summary>
            Internal,
            /// <summary>Connection is owned and managed by the caller</summary>
            External
        }
        private static SqlDataReader ExecuteReader(SqlTransaction transaction, string commandText, SqlParameter[] commandParameters, SqlConnectionOwnership connectionOwnership) {
            bool mustCloseConnection = false;
            // Create a command and prepare it for execution
            SqlCommand cmd = new SqlCommand();
            SqlConnection connection = new SqlConnection();
            if (transaction == null) {
                connection = new SqlConnection(_strConnString);
            } else {
                connection = transaction.Connection;
            }
            try {
                PrepareCommand(cmd, connection, transaction, CommandType.StoredProcedure, commandText, commandParameters, out mustCloseConnection);
                // Create a reader
                SqlDataReader dataReader;
                // Call ExecuteReader with the appropriate CommandBehavior
                if (connectionOwnership == SqlConnectionOwnership.External) {
                    dataReader = cmd.ExecuteReader();
                } else {
                    dataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                }
                bool canClear = true;
                foreach (SqlParameter commandParameter in cmd.Parameters) {
                    if (commandParameter.Direction != ParameterDirection.Input)
                        canClear = false;
                }
                if (canClear) {
                    cmd.Parameters.Clear();
                }
                return dataReader;
            } catch (Exception ex) {
                if (mustCloseConnection)
                    connection.Close();
                throw ex;
            }
        }
        public static SqlDataReader ExecuteReader(string commandText) {
            // Pass through the call providing null for the set of SqlParameters
            return ExecuteReader(commandText, (SqlParameter[])null);
        }
        public static SqlDataReader ExecuteReader(string commandText, params SqlParameter[] commandParameters) {
            return ExecuteReader(null, commandText, commandParameters, SqlConnectionOwnership.Internal);
        }
        public static SqlDataReader ExecuteReader(SqlTransaction transaction, string commandText) {
            // Pass through the call providing null for the set of SqlParameters
            return ExecuteReader(transaction, commandText, (SqlParameter[])null, SqlConnectionOwnership.External);
        }
        public static SqlDataReader ExecuteReader(SqlTransaction transaction, string commandText, params SqlParameter[] commandParameters) {
            if (transaction == null) throw new ArgumentNullException("transaction");
            if (transaction != null && transaction.Connection == null) throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
            // Pass through to private overload, indicating that the connection is owned by the caller
            return ExecuteReader(transaction, commandText, commandParameters, SqlConnectionOwnership.External);
        }
        #endregion ExecuteReader
        #region ExecuteScalar
        public static object ExecuteScalar(string commandText) {
            return ExecuteScalar(commandText, (SqlParameter[])null);
        }
        public static object ExecuteScalar(string commandText, params SqlParameter[] commandParameters) {
            SqlCommand cmd = new SqlCommand();
            SqlConnection connection = new SqlConnection(_strConnString);
            bool mustCloseConnection = false;
            PrepareCommand(cmd, connection, (SqlTransaction)null, CommandType.StoredProcedure, commandText, commandParameters, out mustCloseConnection);
            // Execute the command & return the results
            object retval = cmd.ExecuteScalar();
            // Detach the SqlParameters from the command object, so they can be used again
            cmd.Parameters.Clear();
            if (mustCloseConnection)
                connection.Close();
            return retval;
        }
        public static object ExecuteScalar(SqlTransaction transaction, string commandText) {
            // Pass through the call providing null for the set of SqlParameters
            return ExecuteScalar(transaction, commandText, (SqlParameter[])null);
        }
        public static object ExecuteScalar(SqlTransaction transaction, string commandText, params SqlParameter[] commandParameters) {
            if (transaction == null) throw new ArgumentNullException("transaction");
            if (transaction != null && transaction.Connection == null) throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
            // Create a command and prepare it for execution
            SqlCommand cmd = new SqlCommand();
            bool mustCloseConnection = false;
            PrepareCommand(cmd, transaction.Connection, transaction, CommandType.StoredProcedure, commandText, commandParameters, out mustCloseConnection);
            // Execute the command & return the results
            object retval = cmd.ExecuteScalar();
            // Detach the SqlParameters from the command object, so they can be used again
            cmd.Parameters.Clear();
            return retval;
        }
        #endregion ExecuteScalar
        public static SqlDataReader ExecuteReader(string connectionString, CommandType commandType, string commandText) {
            // Pass through the call providing null for the set of SqlParameters
            return ExecuteReader(connectionString, commandType, commandText, null);
        }
        public static SqlDataReader ExecuteReader(string connectionString, CommandType commandType, string commandText, params SqlParameter[] commandParameters) {
            if (string.IsNullOrEmpty(connectionString)) throw new ArgumentNullException("connectionString");
            SqlConnection connection = null;
            try {
                connection = new SqlConnection(connectionString);
                connection.Open();
                // Call the private overload that takes an internally owned connection in place of the connection string
                return ExecuteReader1(connection, null, commandType, commandText, commandParameters, SqlConnectionOwnership.Internal);
            } catch {
                // If we fail to return the SqlDatReader, we need to close the connection ourselves
                if (connection != null) connection.Close();
                throw;
            }
        }
        private static SqlDataReader ExecuteReader1(SqlConnection connection, SqlTransaction transaction, CommandType commandType, string commandText, IEnumerable<SqlParameter> commandParameters, SqlConnectionOwnership connectionOwnership) {
            if (connection == null) throw new ArgumentNullException("connection");
            var mustCloseConnection = false;
            var cmd = new SqlCommand();
            try {
                PrepareCommand(cmd, connection, transaction, commandType, commandText, commandParameters, out mustCloseConnection);
                var dataReader = connectionOwnership == SqlConnectionOwnership.External ? cmd.ExecuteReader() : cmd.ExecuteReader(CommandBehavior.CloseConnection);
                var canClear = true;
                foreach (SqlParameter commandParameter in cmd.Parameters) {
                    if (commandParameter.Direction != ParameterDirection.Input)
                        canClear = false;
                }
                if (canClear) {
                    cmd.Parameters.Clear();
                }
                return dataReader;
            } catch {
                if (mustCloseConnection)
                    connection.Close();
                throw;
            }
        }
        private static void PrepareCommand(SqlCommand command, SqlConnection connection, SqlTransaction transaction,
            CommandType commandType, string commandText, IEnumerable<SqlParameter> commandParameters, out bool mustCloseConnection) {
            if (command == null) throw new ArgumentNullException("command");
            if (string.IsNullOrEmpty(commandText)) throw new ArgumentNullException("commandText");
            // If the provided connection is not open, we will open it
            if (connection.State != ConnectionState.Open) {
                mustCloseConnection = true;
                connection.Open();
            } else {
                mustCloseConnection = false;
            }
            // Associate the connection with the command
            command.Connection = connection;
            // Set the command text (stored procedure name or SQL statement)
            command.CommandText = commandText;
            // If we were provided a transaction, assign it
            if (transaction != null) {
                if (transaction.Connection == null) throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
                command.Transaction = transaction;
            }
            // Set the command type
            command.CommandType = commandType;
            // Attach the command parameters if they are provided
            if (commandParameters != null) {
                AttachParameters(command, commandParameters);
            }
        }
        private static void AttachParameters(SqlCommand command, IEnumerable<SqlParameter> commandParameters) {
            if (command == null) throw new ArgumentNullException("command");
            if (commandParameters == null) return;
            foreach (var p in commandParameters.Where(p => p != null)) {
                // Check for derived output value with no value assigned
                if ((p.Direction == ParameterDirection.InputOutput ||
                     p.Direction == ParameterDirection.Input) &&
                    (p.Value == null)) {
                    p.Value = DBNull.Value;
                }
                command.Parameters.Add(p);
            }
        }
    }
}
