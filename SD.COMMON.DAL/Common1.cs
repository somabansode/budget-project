﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
namespace SD.COMMON.DAL {
    /// <summary>
    /// Summary description for Common1
    /// </summary>
    /// 
    public class Common1 {
        public Common1() {
            //
            // TODO: Add constructor logic here
            //
        }
        public DataSet Validate_Login(string Login_Name, string Password, string MachineName, int ModuleId, string HostIP, string CompanyName) {
            try {
                //DataSet ds = new DataSet();
                SqlParameter[] objParam = new SqlParameter[6];
                objParam[0] = new SqlParameter("@Login_Name", Login_Name);
                objParam[1] = new SqlParameter("@Password", Password);
                objParam[2] = new SqlParameter("@MacName", MachineName);
                objParam[3] = new SqlParameter("@ModuleId", ModuleId);
                objParam[4] = new SqlParameter("@HostIP", HostIP);
                objParam[5] = new SqlParameter("@CompanyName", CompanyName);
                return SQLDBUtil.ExecuteDataset("CP_Login", objParam);
            } catch (Exception ex) {
                throw ex;
            }
        }
        public DataSet GetUcMenu(int ModuleId, int RoleId, int Id) {
            try {
                //DataSet ds = new DataSet();
                return SQLDBUtil.ExecuteDataset("CP_GetModuleMenu", new SqlParameter[] { new SqlParameter("@ModuleId", ModuleId), new SqlParameter("@RoleId", RoleId), new SqlParameter("@Id", Id) });
            } catch (Exception ex) {
                throw ex;
            }
        }
        public DataSet GetLinks(int MenuId, int RoleID) {
            try {
                SqlParameter[] p = new SqlParameter[2];
                p[0] = new SqlParameter("@MenuID", MenuId);
                p[1] = new SqlParameter("@RoleID", RoleID);
                //DataSet ds = new DataSet();
                return SQLDBUtil.ExecuteDataset("GetLinks", p);
            } catch (Exception ex) {
                throw ex;
            }
        }
        public DataSet GetAllowed(int RoleId, int ModuleId, string URL, int Id) {
            //DataSet ds = new DataSet();
            SqlParameter[] objParam = new SqlParameter[4];
            objParam[0] = new SqlParameter("@RoleId", RoleId);
            objParam[1] = new SqlParameter("@ModuleId", ModuleId);
            objParam[2] = new SqlParameter("@URL", URL);
            objParam[3] = new SqlParameter("@Id", Id);
            return SQLDBUtil.ExecuteDataset("CP_GetPageAccess", objParam);
        }
        public DataSet Validate_Login(string Login_Name, string Password, string MachineName, int ModuleId) {
            try {
                //DataSet ds = new DataSet();
                SqlParameter[] objParam = new SqlParameter[4];
                objParam[0] = new SqlParameter("@Login_Name", Login_Name);
                objParam[1] = new SqlParameter("@Password", Password);
                objParam[2] = new SqlParameter("@MacName", MachineName);
                objParam[3] = new SqlParameter("@ModuleId", ModuleId);
                return SQLDBUtil.ExecuteDataset("CP_Login", objParam);
            } catch (Exception ex) {
                throw ex;
            }
        }
        public static DataSet GetLoginSessions(string Login_Name, int ModuleId) {
            try {
                SqlParameter[] objParam = new SqlParameter[3];
                objParam[0] = new SqlParameter("@Login_Name", Login_Name);
                objParam[1] = new SqlParameter("@ModuleId", ModuleId);
                string logInMachine = HttpContext.Current.Session["MacName"] != null ?
                        HttpContext.Current.Session["MacName"].ToString() : "Missing";
                objParam[2] = new SqlParameter("@MacName", logInMachine);
                return SQLDBUtil.ExecuteDataset("CP_GetLoginSessions", objParam);
            } catch (Exception ex) {
                throw ex;
            }
        }
        public static DataSet SessionVariables(string loginId, int moduleId) {
            try {
                SqlParameter[] objParam = new SqlParameter[3];
                objParam[0] = new SqlParameter("@loginId", loginId);
                objParam[1] = new SqlParameter("@moduleId", moduleId);
                string logInMachine = HttpContext.Current.Session["MacName"] != null ?
                        HttpContext.Current.Session["MacName"].ToString() : "Missing";
                objParam[2] = new SqlParameter("@MacName", logInMachine);
                return SQLDBUtil.ExecuteDataset("sg_SessionVariables", objParam);
            } catch (Exception ex) {
                throw ex;
            }
        }
        public DataSet PageHelp(string URL, int ModuleId) {
            try {
                //DataSet ds = new DataSet();
                SqlParameter[] objParam = new SqlParameter[2];
                objParam[0] = new SqlParameter("@URL", URL);
                objParam[1] = new SqlParameter("@ModuleId", ModuleId);
                return SQLDBUtil.ExecuteDataset("CP_Get_PageHelp", objParam);
            } catch (Exception ex) {
                throw ex;
            }
        }
        public static string getWebConfig(string strSectionName, string strItemName) {
            NameValueCollection nvc = (NameValueCollection)ConfigurationSettings.GetConfig(strSectionName);
            return nvc[strItemName];
        }
        public static DataSet GetPurchacesReportSummary() {
            try {
                //DataSet ds = new DataSet();
                return SQLDBUtil.ExecuteDataset("MMS_MIS_PurchacesReportSummary");
            } catch (Exception e) {
                // clsErrorLog.Log(e);
                throw e;
            }
        }
        public DataSet GetGDNsCount() {
            try {
                DataSet ds = SQLDBUtil.ExecuteDataset("MMS_GetNotArrivedGDNsCount");
                return ds;
            } catch (Exception e) { throw e; }
        }
    }
}
