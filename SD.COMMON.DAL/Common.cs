﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace SD.COMMON.DAL {
    public enum IsActive { All, Active, Inactive };
    public class Common {
        public static DataSet GetUserSettings(int? EmpID, int? CompanyID) {
            try {
                //  DataSet ds = new DataSet();
                SqlParameter[] parm = new SqlParameter[2];
                parm[0] = new SqlParameter("@UID", EmpID);
                parm[1] = new SqlParameter("@CompanyID", CompanyID);
                return SQLDBUtil.ExecuteDataset("AMS_GetUserSettings", parm);
            } catch (Exception ex) {
                throw ex;
            }
        }
        public static DataSet CP_GetTutorials(int ModuleId, int? MenuId) {
            try {
                // DataSet ds = new DataSet();
                //ds = SQLDBUtil.ExecuteDataset("MMS_GetTutorials", new SqlParameter[] { new SqlParameter("@ModuleId", ModuleId), new SqlParameter("@MenuId",MenuId) });
                SqlParameter[] par = new SqlParameter[2];
                par[0] = new SqlParameter("@ModuleId", ModuleId);
                if (MenuId != 0) {
                    par[1] = new SqlParameter("@MenuId", MenuId);
                } else {
                    par[1] = new SqlParameter("@MenuId", System.Data.SqlDbType.Int);
                }
                return SQLDBUtil.ExecuteDataset("MMS_GetTutorials", par);
            } catch (Exception ex) {
                throw ex;
            }
        }
        public DataSet GetUcMenu(int ModuleId, int RoleId, int Id) {
            try {
                // DataSet ds = new DataSet();
                return SQLDBUtil.ExecuteDataset("CP_GetModuleMenu", new SqlParameter[] { new SqlParameter("@ModuleId", ModuleId), new SqlParameter("@RoleId", RoleId), new SqlParameter("@Id", Id) });
            } catch (Exception ex) {
                throw ex;
            }
        }
        public static DataSet GEN_SearchCompany(String SearchKey) {
            try {
                SqlParameter[] par = new SqlParameter[1];
                par[0] = new SqlParameter("@Search", SearchKey);
                return SQLDBUtil.ExecuteDataset("GEN_SearchCompany", par);
            } catch (Exception ex) {
                throw ex;
            }
        }
        public static DataSet GetCompany(String SearchKey) {
            try {
                SqlParameter[] par = new SqlParameter[1];
                par[0] = new SqlParameter("@Search", SearchKey);
                return SQLDBUtil.ExecuteDataset("GEN_Service_SearchCompany", par);
            } catch (Exception ex) {
                throw ex;
            }
        }
        public static DataSet HR_GetMonitorEngineer(int EmpID) {
            // DataSet ds = new DataSet();
            SqlParameter[] objParam = new SqlParameter[1];
            objParam[0] = new SqlParameter("@EmpID", EmpID);
            return SQLDBUtil.ExecuteDataset("HR_GetMonitorEngineer", objParam);
        }
        public static DataSet GetRequiredData(string StoredProcName) {
            return SQLDBUtil.ExecuteDataset(StoredProcName);
        }
        public DataSet CP_Login(string Login_Name, string Password, string MachineName, int ModuleId, string HostIP, string CompanyName) {
            try {
                //  DataSet ds = new DataSet();
                SqlParameter[] objParam = new SqlParameter[6];
                objParam[0] = new SqlParameter("@Login_Name", Login_Name);
                objParam[1] = new SqlParameter("@Password", Password);
                objParam[2] = new SqlParameter("@MacName", MachineName);
                objParam[3] = new SqlParameter("@ModuleId", ModuleId);
                objParam[4] = new SqlParameter("@HostIP", HostIP);
                objParam[5] = new SqlParameter("@CompanyName", CompanyName);
                return SQLDBUtil.ExecuteDataset("CP_Login", objParam);
            } catch (Exception ex) {
                throw ex;
            }
        }
        public DataSet GetLinks(int MenuId, int RoleID) {
            try {
                SqlParameter[] p = new SqlParameter[2];
                p[0] = new SqlParameter("@MenuID", MenuId);
                p[1] = new SqlParameter("@RoleID", RoleID);
                // DataSet ds = new DataSet();
                return SQLDBUtil.ExecuteDataset("GetLinks", p);
            } catch (Exception ex) {
                throw ex;
            }
        }
        public DataSet GetAllowed(int RoleId, int ModuleId, string URL, int Id) {
            // DataSet ds = new DataSet();
            SqlParameter[] objParam = new SqlParameter[4];
            objParam[0] = new SqlParameter("@RoleId", RoleId);
            objParam[1] = new SqlParameter("@ModuleId", ModuleId);
            objParam[2] = new SqlParameter("@URL", URL);
            objParam[3] = new SqlParameter("@Id", Id);
            return SQLDBUtil.ExecuteDataset("CP_GetPageAccess", objParam);
        }
        public DataSet Validate_Login(string Login_Name, string Password, string MachineName, int ModuleId) {
            try {
                // DataSet ds = new DataSet();
                SqlParameter[] objParam = new SqlParameter[4];
                objParam[0] = new SqlParameter("@Login_Name", Login_Name);
                objParam[1] = new SqlParameter("@Password", Password);
                objParam[2] = new SqlParameter("@MacName", MachineName);
                objParam[3] = new SqlParameter("@ModuleId", ModuleId);
                DataSet ds = SQLDBUtil.ExecuteDataset("CP_Login", objParam);
                return ds;
            } catch (Exception ex) {
                throw ex;
            }
        }
        public DataSet PageHelp(string URL, int ModuleId) {
            try {
                // DataSet ds = new DataSet();
                SqlParameter[] objParam = new SqlParameter[2];
                objParam[0] = new SqlParameter("@URL", URL);
                objParam[1] = new SqlParameter("@ModuleId", ModuleId);
                return SQLDBUtil.ExecuteDataset("CP_Get_PageHelp", objParam);
            } catch (Exception ex) {
                throw ex;
            }
        }
        public static string getWebConfig(string strSectionName, string strItemName) {
            NameValueCollection nvc = (NameValueCollection)ConfigurationSettings.GetConfig(strSectionName);
            return nvc[strItemName];
        }
        public static DataSet GetPurchacesReportSummary() {
            try {
                // DataSet ds = new DataSet();
                return SQLDBUtil.ExecuteDataset("MMS_MIS_PurchacesReportSummary");
            } catch (Exception e) {
                //  clsErrorLog.Log(e);
                throw e;
            }
        }
        public DataSet GetGDNsCount() {
            try {
                return SQLDBUtil.ExecuteDataset("MMS_GetNotArrivedGDNsCount");
            } catch (Exception e) { throw e; }
        }
        #region Generic Function
        #endregion
        #region private members
        private int _PageSize;
        private int _CurrentPage;
        private int _NoofRecords;
        private int _TotalPages;
        #endregion private members
        #region Properties
        public int TotalPages {
            get { return _TotalPages; }
            set { _TotalPages = value; }
        }
        public int NoofRecords {
            get { return _NoofRecords; }
            set { _NoofRecords = value; }
        }
        public int CurrentPage {
            get { return _CurrentPage; }
            set { _CurrentPage = value; }
        }
        public int PageSize {
            get { return _PageSize; }
            set { _PageSize = value; }
        }
        private string _Username;
        private string _NewPassWord;
        private int _EmpID;
        public int EmpID {
            get { return _EmpID; }
            set { _EmpID = value; }
        }
        private int _Status;
        public int Status {
            get { return _Status; }
            set { _Status = value; }
        }
        public string Username {
            get { return _Username; }
            set { _Username = value; }
        }
        public string NewPassWord {
            get { return _NewPassWord; }
            set { _NewPassWord = value; }
        }
        #endregion Properties
        public static DataSet OMS_GetDocumentTypesByDocTypeId(int DocTypeID) {
            try {
                //   DataSet ds = new DataSet();
                SqlParameter[] sqlParams = new SqlParameter[1];
                sqlParams[0] = new SqlParameter("@DocTypeID", DocTypeID);
                return SQLDBUtil.ExecuteDataset("OMS_GetDocumentTypesByDocTypeID", sqlParams);
            } catch (Exception ex) {
                throw ex;
            }
        }
        public static DataSet BingGroupItems(string Filter, int GroupID,int cid) {
            SqlParameter[] p = new SqlParameter[5];
            p[0] = new SqlParameter("@id", 1);
            p[1] = new SqlParameter("@Desc", Filter);
            p[2] = new SqlParameter("@GroupId", GroupID);
            p[3] = new SqlParameter("@Cat_Type", 1);
            p[4] = new SqlParameter("@cid", cid);
            return SQLDBUtil.ExecuteDataset("PM_GroupWiseItems_Indent", p);
        }
        public static string DataRowToDouble(DataRow dr) {
            return dr["Name"].ToString();
        }
        public static string[] ConvertStingArray(DataSet ds) {
            string[] rtval = Array.ConvertAll(ds.Tables[0].Select(), new Converter<DataRow, string>(DataRowToDouble));
            return rtval;
        }
        public static DataSet GetUOM() {
            try {
                // DataSet ds = new DataSet();
                return SQLDBUtil.ExecuteDataset("MMS_DDL_AU");
            } catch (Exception e) {
                // clsErrorLog.Log(e);
                throw e;
            }
        }
        public static DataSet MMS_ChangeStatus(int GDNId, int Status, int UserId) {
            try {
                SqlParameter[] sqlParams = new SqlParameter[3];
                sqlParams[0] = new SqlParameter("@GDNId", GDNId);
                sqlParams[1] = new SqlParameter("@Status", Status);
                sqlParams[2] = new SqlParameter("@UserId", UserId);
                return SQLDBUtil.ExecuteDataset("MMS_ChangeStatus", sqlParams);
            } catch (Exception e) {
                //  clsErrorLog.Log(e);
                throw e;
            }
        }
        public static DataSet BindAllResourceGroups(int cid) {
            SqlParameter[] p = new SqlParameter[4];
            p[0] = new SqlParameter("@id", 1);
            p[1] = new SqlParameter("@Cat", "");
            p[2] = new SqlParameter("@Cat_Type", 1);
            p[3] = new SqlParameter("@cid",cid);
            return SQLDBUtil.ExecuteDataset("SP_PM_SearchCategories", p);
        }
        public static int InsUpdateDeleteOperations(int ID, string Operation, int UserId, int Status) {
            try {
                SqlParameter[] sqlParams = new SqlParameter[4];
                sqlParams[0] = new SqlParameter("@ID", ID);
                sqlParams[1] = new SqlParameter("@Operation", Operation);
                sqlParams[2] = new SqlParameter("@UserId", UserId);
                sqlParams[3] = new SqlParameter("@Status", Status);
                SQLDBUtil.ExecuteNonQuery("HR_InsUpdateDeleteOperations", sqlParams);
                return 1;
            } catch (Exception e) {
                return -1;
            }
        }
        public static DataSet HR_GetToDolistCount(int EmpID, int CompanyID) {
            try {
                //  DataSet ds = new DataSet();
                SqlParameter[] objParam = new SqlParameter[2];
                objParam[0] = new SqlParameter("@EmpID", EmpID);
                objParam[1] = new SqlParameter("@CompanyID", CompanyID);
                return SQLDBUtil.ExecuteDataset("HR_GetToDolistCount", objParam);
            } catch (Exception ex) {
                throw ex;
            }
        }
        public static DataSet GetLoginSessions(string Login_Name, int ModuleId) {
            try {
                // DataSet ds = new DataSet();
                SqlParameter[] objParam = new SqlParameter[2];
                objParam[0] = new SqlParameter("@Login_Name", Login_Name);
                objParam[1] = new SqlParameter("@ModuleId", ModuleId);
                return SQLDBUtil.ExecuteDataset("CP_GetLoginSessions", objParam);
            } catch (Exception ex) {
                throw ex;
            }
        }
        public static void OMS_InsUpdateDocumentTypesByModuleId(int DocTypeID, string DocType, int FolderID, int ModuleID) {
            try {
                SqlParameter[] sqlParams = new SqlParameter[4];
                sqlParams[0] = new SqlParameter("@DocTypeID", DocTypeID);
                sqlParams[1] = new SqlParameter("@DocType", DocType);
                sqlParams[2] = new SqlParameter("@FolderID", FolderID);
                sqlParams[3] = new SqlParameter("@ModuleID", ModuleID);
                SQLDBUtil.ExecuteNonQuery("OMS_InsUpdateDocumentTypesByModuleId", sqlParams);
            } catch (Exception ex) {
                throw ex;
            }
        }
        public static DataSet GetApplicantscount() {
            try {
                // DataSet ds = new DataSet();
                return SQLDBUtil.ExecuteDataset("HR_Get_Applicantscount");
            } catch (Exception ex) {
                throw ex;
            }
        }
        public static DataSet HR_GetTaskUpdates() {
            try {
                // DataSet ds = new DataSet();
                return SQLDBUtil.ExecuteDataset("HR_GetTaskUpdates");
            } catch (Exception ex) {
                throw ex;
            }
        }
        public static DataSet GetUcMenu(int ModuleId, int RoleId) {
            try {
                //  DataSet ds = new DataSet();
                return SQLDBUtil.ExecuteDataset("CP_GetModuleMenu", new SqlParameter[] { new SqlParameter("@ModuleId", ModuleId), new SqlParameter("@RoleId", RoleId) });
            } catch (Exception ex) {
                throw ex;
            }
        }
        public DataSet CP_Login(string Login_Name, string Password, string MachineName, int ModuleId) {
            try {
                //  DataSet ds = new DataSet();
                SqlParameter[] objParam = new SqlParameter[4];
                objParam[0] = new SqlParameter("@Login_Name", Login_Name);
                objParam[1] = new SqlParameter("@Password", Password);
                objParam[2] = new SqlParameter("@MacName", MachineName);
                objParam[3] = new SqlParameter("@ModuleId", ModuleId);
                return SQLDBUtil.ExecuteDataset("CP_Login", objParam);
            } catch (Exception ex) {
                throw ex;
            }
        }
        public static DataSet ClosePO(int PODetID, int EmpID) {
            try {
                return SQLDBUtil.ExecuteDataset("MMS_ClosePoFromGDN", new SqlParameter[] { new SqlParameter("@podetid", PODetID), new SqlParameter("@EmpId", EmpID) });
            } catch (Exception e) {
                throw e;
            }
        }
        public static string MMS_CLOSEPO(int PONO, int Itemid) {
            try {
                SqlParameter[] sqlParams = new SqlParameter[2];
                sqlParams[0] = new SqlParameter("@PONO", PONO);
                sqlParams[1] = new SqlParameter("@Itemid", Itemid);
                SQLDBUtil.ExecuteNonQuery("MMS_CLOSEPO", sqlParams);
                return "";
            } catch (Exception e) {
                throw e;
            }
        }
        public static DataSet GetDropDownWorkSite() {
            try {
                // DataSet ds = new DataSet();
                return SQLDBUtil.ExecuteDataset("MMS_DDL_WorkSite");
            } catch (Exception e) {
                throw e;
            }
        }
        public static DataSet GetDropDownWorkSite(int CompanyID) {
            try {
                // DataSet ds = new DataSet();
                return SQLDBUtil.ExecuteDataset("MMS_DDL_WorkSite", new SqlParameter[] { new SqlParameter("@CompanyID", CompanyID) });
            } catch (Exception e) {
                throw e;
            }
        }
        public static DataSet GetProjectsList(Common objCommon, int ProjectId, string RefNo) {
            try {
                //  DataSet ds = new DataSet();
                SqlParameter[] sqlParams = new SqlParameter[9];
                sqlParams[0] = new SqlParameter("@CurrentPage", objCommon.CurrentPage);
                sqlParams[1] = new SqlParameter("@PageSize", objCommon.PageSize);
                sqlParams[2] = new SqlParameter("@TotalPages", objCommon.TotalPages);
                sqlParams[2].Direction = ParameterDirection.Output;
                sqlParams[3] = new SqlParameter("@NoofRecords", objCommon.NoofRecords);
                sqlParams[3].Direction = ParameterDirection.Output;
                sqlParams[4] = new SqlParameter("@Status", objCommon.Status);
                if (ProjectId != 0)
                    sqlParams[5] = new SqlParameter("@PrjId", ProjectId);
                else
                    sqlParams[5] = new SqlParameter("@PrjId", SqlDbType.Int);
                sqlParams[6] = new SqlParameter("@RefNo", RefNo);
                sqlParams[7] = new SqlParameter("@ProjectName", objCommon.ProjectName);
                sqlParams[8] = new SqlParameter("@ProjectLocation", objCommon.ProjectLocation);
                DataSet ds = SQLDBUtil.ExecuteDataset("MMS_PROJECTSByPaging", sqlParams);
                objCommon.NoofRecords = (int)sqlParams[3].Value;
                objCommon.TotalPages = (int)sqlParams[2].Value;
                return ds;
            } catch (Exception ex) {
                throw;
            }
        }
        public static DataSet GetAllowed(int RoleId, int ModuleId, string URL) {
            //  DataSet ds = new DataSet();
            SqlParameter[] objParam = new SqlParameter[3];
            objParam[0] = new SqlParameter("@RoleId", RoleId);
            objParam[1] = new SqlParameter("@ModuleId", ModuleId);
            objParam[2] = new SqlParameter("@URL", URL);
            return SQLDBUtil.ExecuteDataset("CP_GetPageAccess", objParam);
        }
        public static DataSet GetValidUserDetails(string Username, string NewPassWord) {
            try {
                SqlParameter[] sqlParams = new SqlParameter[2];
                sqlParams[0] = new SqlParameter("@UserName", Username);
                sqlParams[1] = new SqlParameter("@password", NewPassWord);
                //  DataSet ds = new DataSet();
                return SQLDBUtil.ExecuteDataset("HR_ValidUser", sqlParams);
            } catch (Exception e) {
                throw e;
            }
        }
        public static DataSet GetTempValidUserDetails(string Username, string NewPassWord) {
            try {
                SqlParameter[] sqlParams = new SqlParameter[2];
                sqlParams[0] = new SqlParameter("@UserName", Username);
                sqlParams[1] = new SqlParameter("@password", NewPassWord);
                //  DataSet ds = new DataSet();
                return SQLDBUtil.ExecuteDataset("HR_TempValidUser", sqlParams);
            } catch (Exception e) {
                throw e;
            }
        }
        public static DataSet GetUserRolesDetails(int EmpID) {
            try {
                // DataSet ds = new DataSet();
                return SQLDBUtil.ExecuteDataset("MMS_GetUserRolesDetails", new SqlParameter[] { new SqlParameter("@EmpID", EmpID) });
            } catch (Exception e) {
                throw e;
            }
        }
        public static DataSet GetClientEmployee(int EmpID) {
            try {
                //  DataSet ds = new DataSet();
                return SQLDBUtil.ExecuteDataset("CMS_GetClientEmployeeCOC", new SqlParameter[] { new SqlParameter("@EmpId", EmpID) });
            } catch (Exception e) {
                throw e;
            }
        }
        #region DropDownLists
        //Get Resources
        public static DataSet GetResourcesListFilter(string SearchKeyWord) {
            try {
                return SQLDBUtil.ExecuteDataset("MMS_DDL_SearchResources", new SqlParameter[] { new SqlParameter("@SEARCH", SearchKeyWord) });
            } catch (Exception e) {
                throw e;
            }
        }
        #region Get Resources List
        public static DataSet GetResourcesListFilter(IsActive Status) {
            try {
                DataSet ds = null;
                SqlParameter[] sqlPrms = new SqlParameter[1];
                switch (Status) {
                    case IsActive.All:
                        ds = SQLDBUtil.ExecuteDataset("MMS_DDL_SearchResources");
                        break;
                    case IsActive.Active:
                        sqlPrms[0] = new SqlParameter("@IsActive", 1);
                        ds = SQLDBUtil.ExecuteDataset("MMS_DDL_SearchResources", sqlPrms);
                        break;
                    case IsActive.Inactive:
                        sqlPrms[0] = new SqlParameter("@IsActive", 0);
                        ds = SQLDBUtil.ExecuteDataset("MMS_DDL_SearchResources", sqlPrms);
                        break;
                }
                return ds;
            } catch (Exception e) {
                throw e;
            }
        }
        /// <summary>
        /// Get DataSet for All Active Projects  CLID, EmpName for List Bindings
        /// </summary>
        public static DataSet GetResourcesListFilter(IsActive Status, String Filter) {
            try {
                DataSet ds = null;
                SqlParameter[] sqlPrms = new SqlParameter[2];
                switch (Status) {
                    case IsActive.All:
                        sqlPrms = new SqlParameter[1];
                        sqlPrms[0] = new SqlParameter("@SEARCH", Filter);
                        ds = SQLDBUtil.ExecuteDataset("MMS_DDL_SearchResources");
                        break;
                    case IsActive.Active:
                        sqlPrms[0] = new SqlParameter("@IsActive", 1);
                        sqlPrms[1] = new SqlParameter("@SEARCH", Filter);
                        ds = SQLDBUtil.ExecuteDataset("MMS_DDL_SearchResources", sqlPrms);
                        break;
                    case IsActive.Inactive:
                        sqlPrms[0] = new SqlParameter("@IsActive", 1);
                        sqlPrms[1] = new SqlParameter("@SEARCH", Filter);
                        ds = SQLDBUtil.ExecuteDataset("MMS_DDL_SearchResources", sqlPrms);
                        break;
                }
                return ds;
            } catch (Exception e) {
                throw e;
            }
        }
        #endregion Get Resources List
        #region Get Projects List
        /// <summary>
        /// Get DataSet for All Active Projects  CLID, EmpName for List Bindings
        /// </summary>
        public static DataSet GetProjectsListFilter(IsActive Status) {
            try {
                DataSet ds = null;
                SqlParameter[] sqlPrms = new SqlParameter[1];
                switch (Status) {
                    case IsActive.All:
                        ds = SQLDBUtil.ExecuteDataset("[OMS_Get_ProjectListFilter]");
                        break;
                    case IsActive.Active:
                        sqlPrms[0] = new SqlParameter("@IsActive", 1);
                        ds = SQLDBUtil.ExecuteDataset("[OMS_Get_ProjectListFilter]", sqlPrms);
                        break;
                    case IsActive.Inactive:
                        sqlPrms[0] = new SqlParameter("@IsActive", 0);
                        ds = SQLDBUtil.ExecuteDataset("[OMS_Get_ProjectListFilter]", sqlPrms);
                        break;
                }
                return ds;
            } catch (Exception e) {
                throw e;
            }
        }
        /// <summary>
        /// Get DataSet for All Active Projects  CLID, EmpName for List Bindings
        /// </summary>
        /// <param name="Filter">Search String</param>
        /// <returns>System.Data.DataSet</returns>
        public static DataSet GetProjectsListFilter(IsActive Status, String Filter) {
            try {
                DataSet ds = null;
                SqlParameter[] sqlPrms = new SqlParameter[2];
                switch (Status) {
                    case IsActive.All:
                        sqlPrms = new SqlParameter[1];
                        sqlPrms[0] = new SqlParameter("@Filter", Filter);
                        ds = SQLDBUtil.ExecuteDataset("[OMS_Get_ProjectListFilter]");
                        break;
                    case IsActive.Active:
                        sqlPrms[0] = new SqlParameter("@IsActive", 1);
                        sqlPrms[1] = new SqlParameter("@Filter", Filter);
                        ds = SQLDBUtil.ExecuteDataset("[OMS_Get_ProjectListFilter]", sqlPrms);
                        break;
                    case IsActive.Inactive:
                        sqlPrms[0] = new SqlParameter("@IsActive", 1);
                        sqlPrms[1] = new SqlParameter("@Filter", Filter);
                        ds = SQLDBUtil.ExecuteDataset("[OMS_Get_ProjectListFilter]", sqlPrms);
                        break;
                }
                return ds;
            } catch (Exception e) {
                throw e;
            }
        }
        #endregion Get Projects List
        #region Get Clients List
        /// <summary>
        /// Get DataSet for All Active clients CLID, EmpName for List Bindings
        /// </summary>
        /// <returns>System.Data.DataSet</returns>
        /// 
        public static DataSet GetClientsListFilter(IsActive Status) {
            try {
                DataSet ds = null;
                SqlParameter[] sqlPrms = new SqlParameter[1];
                switch (Status) {
                    case IsActive.All:
                        ds = SQLDBUtil.ExecuteDataset("[OMS_Get_ClientsListFilter]");
                        break;
                    case IsActive.Active:
                        sqlPrms[0] = new SqlParameter("@IsActive", 1);
                        ds = SQLDBUtil.ExecuteDataset("[OMS_Get_ClientsListFilter]", sqlPrms);
                        break;
                    case IsActive.Inactive:
                        sqlPrms[0] = new SqlParameter("@IsActive", 0);
                        ds = SQLDBUtil.ExecuteDataset("[OMS_Get_ClientsListFilter]", sqlPrms);
                        break;
                }
                return ds;
            } catch (Exception e) {
                throw e;
            }
        }
        /// <summary>
        /// Get DataSet for All Active Projects  CLID, EmpName for List Bindings
        /// </summary>
        /// <param name="Filter">Search String</param>
        /// <returns>System.Data.DataSet</returns>
        public static DataSet GetClientsListFilter(IsActive Status, String Filter) {
            try {
                DataSet ds = null;
                SqlParameter[] sqlPrms = new SqlParameter[2];
                switch (Status) {
                    case IsActive.All:
                        sqlPrms = new SqlParameter[1];
                        sqlPrms[0] = new SqlParameter("@Filter", Filter);
                        ds = SQLDBUtil.ExecuteDataset("[OMS_Get_ClientsListFilter]");
                        break;
                    case IsActive.Active:
                        sqlPrms[0] = new SqlParameter("@IsActive", 1);
                        sqlPrms[1] = new SqlParameter("@Filter", Filter);
                        ds = SQLDBUtil.ExecuteDataset("[OMS_Get_ClientsListFilter]", sqlPrms);
                        break;
                    case IsActive.Inactive:
                        sqlPrms[0] = new SqlParameter("@IsActive", 1);
                        sqlPrms[1] = new SqlParameter("@Filter", Filter);
                        ds = SQLDBUtil.ExecuteDataset("[OMS_Get_ClientsListFilter]", sqlPrms);
                        break;
                }
                return ds;
            } catch (Exception e) {
                throw e;
            }
        }
        #endregion Get Clients List
        #endregion DropDownLists
        public int OMS_InsUpProjectFiles(int ProjectNo, string ProjectName, string FileName, string FileExt) {
            try {
                SqlParameter[] sqlParams = new SqlParameter[5];
                sqlParams[0] = new SqlParameter("@ProjectNo", ProjectNo);
                sqlParams[1] = new SqlParameter("@ProjectName", ProjectName);
                sqlParams[2] = new SqlParameter("@FileName", FileName);
                sqlParams[3] = new SqlParameter("@FileExt", FileExt);
                sqlParams[4] = new SqlParameter("ReturnValue", System.Data.SqlDbType.Int);
                sqlParams[4].Direction = ParameterDirection.ReturnValue;
                SQLDBUtil.ExecuteNonQuery("OMS_InsUpProjectFiles", sqlParams);
                return Convert.ToInt32(sqlParams[4].Value);
            } catch (Exception e) {
                throw e;
            }
        }
        public static DataSet MMS_Projectslist(Common objCommon) {
            try {
                //  DataSet ds = new DataSet();
                SqlParameter[] sqlParams = new SqlParameter[4];
                sqlParams[0] = new SqlParameter("@CurrentPage", objCommon.CurrentPage);
                sqlParams[1] = new SqlParameter("@PageSize", objCommon.PageSize);
                sqlParams[2] = new SqlParameter("@TotalPages", objCommon.TotalPages);
                sqlParams[2].Direction = ParameterDirection.Output;
                sqlParams[3] = new SqlParameter("@NoofRecords", objCommon.NoofRecords);
                sqlParams[3].Direction = ParameterDirection.Output;
                DataSet ds = SQLDBUtil.ExecuteDataset("MMS_Projectslist", sqlParams);
                objCommon.NoofRecords = (int)sqlParams[3].Value;
                objCommon.TotalPages = (int)sqlParams[2].Value;
                return ds;
            } catch (Exception ex) {
                throw;
            }
        }
        public static DataSet MMS_ProjectslistByProject(Common objCommon, int ProjectId, int Worksite, string Package) {
            try {
                //  DataSet ds = new DataSet();
                SqlParameter[] sqlParams = new SqlParameter[7];
                sqlParams[0] = new SqlParameter("@CurrentPage", objCommon.CurrentPage);
                sqlParams[1] = new SqlParameter("@PageSize", objCommon.PageSize);
                sqlParams[2] = new SqlParameter("@TotalPages", objCommon.TotalPages);
                sqlParams[2].Direction = ParameterDirection.Output;
                sqlParams[3] = new SqlParameter("@NoofRecords", objCommon.NoofRecords);
                sqlParams[3].Direction = ParameterDirection.Output;
                if (ProjectId != 0)
                    sqlParams[4] = new SqlParameter("@PrjId", ProjectId);
                else
                    sqlParams[4] = new SqlParameter("@PrjId", SqlDbType.Int);
                if (objCommon.ProjectID != 0)
                    sqlParams[4].Value = objCommon.ProjectID;
                if (Worksite != 0)
                    sqlParams[5] = new SqlParameter("@Worksite", Worksite);
                else
                    sqlParams[5] = new SqlParameter("@Worksite", SqlDbType.Int);
                if (Package != "")
                    sqlParams[6] = new SqlParameter("@Pkg", Package);
                else
                    sqlParams[6] = new SqlParameter("@Pkg", SqlDbType.VarChar);
                DataSet ds = SQLDBUtil.ExecuteDataset("MMS_Projectslist", sqlParams);
                objCommon.NoofRecords = (int)sqlParams[3].Value;
                objCommon.TotalPages = (int)sqlParams[2].Value;
                return ds;
            } catch (Exception ex) {
                throw;
            }
        }
        public static DataSet OMS_GetProjectFiles() {
            try {
                //   DataSet ds = new DataSet();
                return SQLDBUtil.ExecuteDataset("OMS_GetProjectFiles");
            } catch (Exception ex) {
                throw ex;
            }
        }
        public int OMS_InsUpAgrmtTenders(int DocId, int ProjectId, string DocName, string Doc, int AgmtTypeNo, int CreatedBy) {
            try {
                SqlParameter[] sqlParams = new SqlParameter[7];
                sqlParams[0] = new SqlParameter("@DocId", DocId);
                sqlParams[1] = new SqlParameter("@ProjectId", ProjectId);
                sqlParams[2] = new SqlParameter("@DocName", DocName);
                sqlParams[3] = new SqlParameter("@Doc", Doc);
                sqlParams[4] = new SqlParameter("@AgmtTypeNo", AgmtTypeNo);
                sqlParams[5] = new SqlParameter("@CreatedBy", CreatedBy);
                sqlParams[6] = new SqlParameter("ReturnValue", System.Data.SqlDbType.Int);
                sqlParams[6].Direction = ParameterDirection.ReturnValue;
                SQLDBUtil.ExecuteNonQuery("OMS_InsUpAgrmtTenders", sqlParams);
                return Convert.ToInt32(sqlParams[6].Value);
            } catch (Exception e) {
                throw e;
            }
        }
        public static DataSet OMS_GetAgmtTenders() {
            try {
                // DataSet ds = new DataSet();
                return SQLDBUtil.ExecuteDataset("OMS_GetAgmtTenders");
            } catch (Exception ex) {
                throw ex;
            }
        }
        public static DataSet OMS_GetCodalReq() {
            try {
                // DataSet ds = new DataSet();
                return SQLDBUtil.ExecuteDataset("OMS_GetCodalReq");
            } catch (Exception ex) {
                throw ex;
            }
        }
        #region AgreementTenders
        public static DataSet OMS_GetAgreementTypes(int DocType) {
            try {
                // DataSet ds = new DataSet();
                SqlParameter[] sqlParams = new SqlParameter[1];
                sqlParams[0] = new SqlParameter("@Parent", DocType);
                return SQLDBUtil.ExecuteDataset("OMS_GetAgreementTypes", sqlParams);
            } catch (Exception ex) {
                throw ex;
            }
        }
        public static void OMS_InsupAgmtTypes(int AgmtTypeNo, string AgmtName, int DocType) {
            try {
                SqlParameter[] sqlParams = new SqlParameter[3];
                sqlParams[0] = new SqlParameter("@AgmtTypeNo", AgmtTypeNo);
                sqlParams[1] = new SqlParameter("@AgmtName", AgmtName);
                sqlParams[2] = new SqlParameter("@Parent", DocType);
                SQLDBUtil.ExecuteNonQuery("OMS_InsUpdateDocumentTypes", sqlParams);
            } catch (Exception e) {
                throw e;
            }
        }
        #endregion
        public static int _strItemName { get; set; }
         
        public int OMS_InsUpCodalReq(int ProjectId, string DocName, string Doc, int CreatedBy) {
            try {
                SqlParameter[] sqlParams = new SqlParameter[5];
                //   sqlParams[0] = new SqlParameter("@DocId", DocId);
                sqlParams[0] = new SqlParameter("@ProjectId", ProjectId);
                sqlParams[1] = new SqlParameter("@DocName", DocName);
                sqlParams[2] = new SqlParameter("@Doc", Doc);
                //    sqlParams[4] = new SqlParameter("@AgmtTypeNo", AgmtTypeNo);
                sqlParams[3] = new SqlParameter("@CreatedBy", CreatedBy);
                sqlParams[4] = new SqlParameter("ReturnValue", System.Data.SqlDbType.Int);
                sqlParams[4].Direction = ParameterDirection.ReturnValue;
                SQLDBUtil.ExecuteNonQuery("OMS_InsUpCodalReq", sqlParams);
                return Convert.ToInt32(sqlParams[4].Value);
            } catch (Exception e) {
                throw e;
            }
        }
        protected static DataSet GetSurveyTypes() {
            throw new NotImplementedException();
        }
        
        #region Projects images
        private int _ProjectID;
        private string _ProjectName;
        private string _ProjectLocation;
        public string ProjectName {
            get { return _ProjectName; }
            set { _ProjectName = value; }
        }
        public int ProjectID {
            get { return _ProjectID; }
            set { _ProjectID = value; }
        }
        public string ProjectLocation {
            get { return _ProjectLocation; }
            set { _ProjectLocation = value; }
        }
        public static DataSet GetProjects(int ProjectsType) {
            try {
                SqlParameter[] p = new SqlParameter[1];
                p[0] = new SqlParameter("@ProId", ProjectsType);
                //  DataSet ds = new DataSet();
                return SQLDBUtil.ExecuteDataset("G_GetProjects", p);
            } catch (Exception ex) {
                throw ex;
            }
        }
        public static DataSet GetProjectsListDetails(Common objCommon) {
            try {
                // DataSet ds = new DataSet();
                SqlParameter[] sqlParams = new SqlParameter[5];
                sqlParams[0] = new SqlParameter("@CurrentPage", objCommon.CurrentPage);
                sqlParams[1] = new SqlParameter("@PageSize", objCommon.PageSize);
                sqlParams[2] = new SqlParameter("@TotalPages", objCommon.TotalPages);
                sqlParams[2].Direction = ParameterDirection.Output;
                sqlParams[3] = new SqlParameter("@NoofRecords", objCommon.NoofRecords);
                sqlParams[3].Direction = ParameterDirection.Output;
                sqlParams[4] = new SqlParameter("@Status", objCommon.Status);
                DataSet ds = SQLDBUtil.ExecuteDataset("MMS_PROJECTSByPaging", sqlParams);
                objCommon.NoofRecords = (int)sqlParams[3].Value;
                objCommon.TotalPages = (int)sqlParams[2].Value;
                return ds;
            } catch (Exception ex) {
                throw;
            }
        }
        public static DataSet GetProjectsListDetailsbyProjectID(Common objCommon, int ProjectId, int Pid, string RefNo) {
            try {
                // DataSet ds = new DataSet();
                SqlParameter[] sqlParams = new SqlParameter[8];
                sqlParams[0] = new SqlParameter("@CurrentPage", objCommon.CurrentPage);
                sqlParams[1] = new SqlParameter("@PageSize", objCommon.PageSize);
                sqlParams[2] = new SqlParameter("@TotalPages", objCommon.TotalPages);
                sqlParams[2].Direction = ParameterDirection.Output;
                sqlParams[3] = new SqlParameter("@NoofRecords", objCommon.NoofRecords);
                sqlParams[3].Direction = ParameterDirection.Output;
                sqlParams[4] = new SqlParameter("@Status", objCommon.Status);
                if (ProjectId == 0)
                    sqlParams[5] = new SqlParameter("@PrjId", SqlDbType.Int);
                else
                    sqlParams[5] = new SqlParameter("@PrjId", ProjectId);
                if (Pid == 0)
                    sqlParams[6] = new SqlParameter("@Pid", SqlDbType.Int);
                else
                    sqlParams[6] = new SqlParameter("@Pid", Pid);
                if (RefNo == string.Empty)
                    sqlParams[7] = new SqlParameter("@RefNo", SqlDbType.Int);
                else
                    sqlParams[7] = new SqlParameter("@RefNo", RefNo);
                DataSet ds = SQLDBUtil.ExecuteDataset("MMS_PROJECTSByPaging", sqlParams);
                objCommon.NoofRecords = (int)sqlParams[3].Value;
                objCommon.TotalPages = (int)sqlParams[2].Value;
                return ds;
            } catch (Exception ex) {
                throw;
            }
        }
        public static DataSet GetProjectsList() {
            try {
                // DataSet ds = new DataSet();
                return SQLDBUtil.ExecuteDataset("MMS_PROJECTS");
            } catch (Exception ex) {
                throw;
            }
        }
        public static DataSet GetProjectImagesforAdmin(Common objCommon) {
            try {
                SqlParameter[] sqlParams = new SqlParameter[5];
                sqlParams[0] = new SqlParameter("@CurrentPage", objCommon.CurrentPage);
                sqlParams[1] = new SqlParameter("@PageSize", objCommon.PageSize);
                sqlParams[2] = new SqlParameter("@TotalPages", System.Data.SqlDbType.Int);
                sqlParams[2].Direction = ParameterDirection.Output;
                sqlParams[3] = new SqlParameter("@NoofRecords", System.Data.SqlDbType.Int);
                sqlParams[3].Direction = ParameterDirection.Output;
                sqlParams[4] = new SqlParameter("@ProjectID", objCommon.ProjectID);
                // DataSet ds = new DataSet();
                DataSet ds = SQLDBUtil.ExecuteDataset("WMS_spselctrpoEdit", sqlParams);
                objCommon.NoofRecords = (int)sqlParams[3].Value;
                objCommon.TotalPages = (int)sqlParams[2].Value;
                return ds;
            } catch (Exception e) {
                throw e;
            }
        }
        public static DataSet GetEditProjects(Common objCommon) {
            try {
                SqlParameter[] sqlParams = new SqlParameter[5];
                sqlParams[0] = new SqlParameter("@CurrentPage", objCommon.CurrentPage);
                sqlParams[1] = new SqlParameter("@PageSize", objCommon.PageSize);
                sqlParams[2] = new SqlParameter("@TotalPages", System.Data.SqlDbType.Int);
                sqlParams[2].Direction = ParameterDirection.Output;
                sqlParams[3] = new SqlParameter("@NoofRecords", System.Data.SqlDbType.Int);
                sqlParams[3].Direction = ParameterDirection.Output;
                sqlParams[4] = new SqlParameter("@ProjectName", objCommon.ProjectName);
                // DataSet ds = new DataSet();
                DataSet ds = SQLDBUtil.ExecuteDataset("WMS_GetEditProjectsList", sqlParams);
                objCommon.NoofRecords = (int)sqlParams[3].Value;
                objCommon.TotalPages = (int)sqlParams[2].Value;
                return ds;
            } catch (Exception e) {
                throw e;
            }
        }
        #endregion
        public DataSet GetUcMenu(int ModuleId, int RoleId, string SearchString) {
            try {
                //  DataSet ds = new DataSet();
                return SQLDBUtil.ExecuteDataset("CP_GetModuleMenu", new SqlParameter[] { new SqlParameter("@ModuleId", ModuleId), new SqlParameter("@RoleId", RoleId), new SqlParameter("@SearchString", SearchString) });
            } catch (Exception ex) {
                throw ex;
            }
        }
        public static void EMS_CLOSEPOFromGDN(int Itemid, int EmpId) {
            try {
                SqlParameter[] sqlParams = new SqlParameter[2];
                sqlParams[0] = new SqlParameter("@podetid", Itemid);
                sqlParams[1] = new SqlParameter("@EmpId", EmpId);
                SQLDBUtil.ExecuteNonQuery("MMS_ClosePoFromGDN", sqlParams);
            } catch (Exception e) {
                throw e;
            }
        }
        
        #region Gratuity
        public static int HMS_InsUpd_GratuityConfig(int GID, int From, int To, int Accrue, int Resign, int Termination, int Indiscipline, int ContractCompleation, int CompanyID) {
            try {
                SqlParameter[] objParam = new SqlParameter[10];
                objParam[0] = new SqlParameter("@GID", GID);
                objParam[1] = new SqlParameter("@From", From);
                objParam[2] = new SqlParameter("@To", To);
                objParam[3] = new SqlParameter("@Accrue", Accrue);
                objParam[4] = new SqlParameter("@Resign", Resign);
                objParam[5] = new SqlParameter("@Termination", Termination);
                objParam[6] = new SqlParameter("@Indiscipline", Indiscipline);
                objParam[7] = new SqlParameter("@CompanyID", CompanyID);
                objParam[8] = new SqlParameter("@ContractCompleation", ContractCompleation);
                objParam[9] = new SqlParameter("@Returnvalue", System.Data.SqlDbType.Int);
                objParam[9].Direction = ParameterDirection.ReturnValue;
                SQLDBUtil.ExecuteNonQuery("HMS_InsUpd_GratuityConfig", objParam);
                int val;
                return val = Convert.ToInt32(objParam[9].Value);
            } catch (Exception e) {
                throw e;
            }
        } //k
        public static DataSet GetGratuityConfigBypaging(Common objcommon) {
            try {
                SqlParameter[] sqlParams = new SqlParameter[4];
                sqlParams[0] = new SqlParameter("@CurrentPage", objcommon.CurrentPage);
                sqlParams[1] = new SqlParameter("@PageSize", objcommon.PageSize);
                sqlParams[2] = new SqlParameter("@ReturnValue", System.Data.SqlDbType.Int);
                sqlParams[2].Direction = ParameterDirection.ReturnValue;
                sqlParams[3] = new SqlParameter("@NoofRecords", System.Data.SqlDbType.Int);
                sqlParams[3].Direction = ParameterDirection.Output;
                // DataSet ds = new DataSet();
                DataSet ds = SQLDBUtil.ExecuteDataset("HMS_Get_GratuityConfigBypaging", sqlParams);
                objcommon.NoofRecords = (int)sqlParams[3].Value;
                objcommon.TotalPages = (int)sqlParams[2].Value;
                return ds;
            } catch (Exception ex) {
                throw ex;
            }
        }//k
        public static DataSet GratuityConfigDetails(int GID) {
            try {
                SqlParameter[] objParam = new SqlParameter[1];
                objParam[0] = new SqlParameter("@GID", GID);
                // DataSet ds = new DataSet();
                return SQLDBUtil.ExecuteDataset("HMS_GratuityConfigDetails", objParam);
            } catch (Exception ex) {
                throw ex;
            }
        }//k
        public static DataSet GetGratuityBypaging(Common objCommon, int CompanyID, int SiteID, int Year) {
            try {
                // DataSet ds = new DataSet();
                SqlParameter[] sqlParams = new SqlParameter[7];
                sqlParams[0] = new SqlParameter("@CurrentPage", objCommon.CurrentPage);
                sqlParams[1] = new SqlParameter("@PageSize", objCommon.PageSize);
                sqlParams[2] = new SqlParameter("@ReturnValue", System.Data.SqlDbType.Int);
                sqlParams[2].Direction = ParameterDirection.ReturnValue;
                sqlParams[3] = new SqlParameter("@NoofRecords", System.Data.SqlDbType.Int);
                sqlParams[3].Direction = ParameterDirection.Output;
                sqlParams[4] = new SqlParameter("@CompanyID", CompanyID);
                sqlParams[5] = new SqlParameter("@WorkSiteID", SiteID);
                sqlParams[6] = new SqlParameter("@Year", Year);
                DataSet ds = SQLDBUtil.ExecuteDataset("HMS_Get_Gratuity", sqlParams);
                objCommon.NoofRecords = (int)sqlParams[3].Value;
                objCommon.TotalPages = (int)sqlParams[2].Value;
                return ds;
            } catch (Exception ex) {
                throw;
            }
        }
        public static DataSet GetWorkSites(int CompanyID) {
            try {
                // DataSet ds = new DataSet();
                return SQLDBUtil.ExecuteDataset("HR_GetWorkSite", new SqlParameter[] { new SqlParameter("@WSID", "0"), new SqlParameter("@WSStatus", "1"), new SqlParameter("@CompanyID", CompanyID) });
            } catch (Exception e) {
                throw e;
            }
        }
        #endregion Gratuity
        #region Self help
        public static DataSet GEN_GetMenuSelfHelp(int MenuID) {
            try {
                SqlParameter[] objParam = new SqlParameter[1];
                objParam[0] = new SqlParameter("@menuID", MenuID);
                return SQLDBUtil.ExecuteDataset("GEN_GetMenuSelfHelp", objParam);
            } catch {
                return null;
            }
        }
        #endregion
        public static DataSet HomePageUrls(int EmpID) {
            try {
                SqlParameter[] objParam = new SqlParameter[1];
                objParam[0] = new SqlParameter("@EmpID", EmpID);
                return SQLDBUtil.ExecuteDataset("GEN_HomePage", objParam);
            } catch (Exception ex) {
                throw ex;
            }
        }
        public static DataSet GEN_InserFavGrp(ref int? GrpFavID, int EmpID, string GrPName, int TypeID) {
            try {
                SqlParameter[] objParam = new SqlParameter[5];
                objParam[0] = new SqlParameter("@EmpID", EmpID);
                objParam[1] = new SqlParameter("@GrpName", GrPName);
                objParam[2] = new SqlParameter("ReturnValue", System.Data.SqlDbType.Int);
                objParam[2].Direction = ParameterDirection.ReturnValue;
                objParam[3] = new SqlParameter("@GrpFavID", GrpFavID);
                objParam[4] = new SqlParameter("@Type", TypeID);
                DataSet ds = SQLDBUtil.ExecuteDataset("GEN_InserFavGrp", objParam);
                GrpFavID = Convert.ToInt32(objParam[2].Value);
                return ds;
            } catch (Exception ex) {
                throw ex;
            }
        }
        public static void GEN_FAV_SetGroupOrder(int EmpID, int GroupID, int Direction) {
            try {
                SqlParameter[] objParam = new SqlParameter[3];
                objParam[0] = new SqlParameter("@EmpID", EmpID);
                objParam[1] = new SqlParameter("@GroupID", GroupID);
                objParam[2] = new SqlParameter("@Direction", Direction);
                SQLDBUtil.ExecuteNonQuery("GEN_FAV_SetGroupOrder", objParam);
            } catch (Exception ex) {
                throw ex;
            }
        }
        public static int G_InsertFavLink(int EmpID, string URL, int ModuleID, string FavPageName, int GrpID) {
            SqlParameter[] objParam = new SqlParameter[6];
            objParam[0] = new SqlParameter("@EmpID", EmpID);
            objParam[1] = new SqlParameter("@Pageurl", URL);
            objParam[2] = new SqlParameter("@ModuleID", ModuleID);
            objParam[4] = new SqlParameter("@GrpID", GrpID);
            objParam[5] = new SqlParameter("@FavPageName", FavPageName);
            objParam[3] = new SqlParameter("ReturnValue", System.Data.SqlDbType.Int);
            objParam[3].Direction = ParameterDirection.ReturnValue;
            SQLDBUtil.ExecuteNonQuery("G_InsertFavpage", objParam);
            return Convert.ToInt32(objParam[3].Value);
        }
        public static DataSet BindFavGroups(int EmpID) {
            try {
                SqlParameter[] objParam = new SqlParameter[1];
                objParam[0] = new SqlParameter("@EmpID", EmpID);
                return SQLDBUtil.ExecuteDataset("GEN_BindFavGrps", objParam);
            } catch (Exception ex) {
                throw ex;
            }
        }
        public static void DeleFav(int FavID) {
            SqlParameter[] objParam = new SqlParameter[1];
            objParam[0] = new SqlParameter("@FavID", FavID);
            SQLDBUtil.ExecuteDataset("G_DeletFav", objParam);
        }
        public static DataSet PendingCount(int Menuid) {
            try {
                SqlParameter[] objParam = new SqlParameter[2];
                objParam[0] = new SqlParameter("@submenuid", Menuid);
                System.Web.HttpContext context = System.Web.HttpContext.Current;
                objParam[1] = new SqlParameter("@userid", context.Session["UserId"]);
                return SQLDBUtil.ExecuteDataset("USP_MenuPendingCount", objParam);
            } catch (Exception ex) {
                throw ex;
            }
        }
        public static DataSet CP_GetMenuIDbyURL(int RoleId, string URL) {
            // DataSet ds = new DataSet();
            SqlParameter[] objParam = new SqlParameter[2];
            objParam[0] = new SqlParameter("@RoleId", RoleId);
            objParam[1] = new SqlParameter("@URL", URL);
            return SQLDBUtil.ExecuteDataset("CP_GetMenuIDbyURL", objParam);
            //CP_GetMenuIDbyURL(@URL varchar(100),@RoleId int)
        }

        public static System.Collections.Generic.List<LowestOffers> clLowestOffers = new System.Collections.Generic.List<LowestOffers>();
        public static string strDiscloseVendorPrice { get; set; }
        public static System.Collections.Generic.List<LowestOffers> getLowestOffers(int cid)
        {
            SqlParameter[] objParam = new SqlParameter[1];
            objParam[0] = new SqlParameter("@cid", cid);
            clLowestOffers = new System.Collections.Generic.List<LowestOffers>();
            using (SqlDataReader rdr = SQLDBUtil.ExecuteDataReader("sp_LowestOffers", objParam))
            {
                while (rdr.Read())
                {
                    LowestOffers lo = new LowestOffers();
                    lo.cid = cid;
                    lo.enqID = (int)rdr["enqID"];
                    lo.venID = (int)rdr["venID"];
                    lo.lowPrice = Convert.ToDecimal(rdr["lowPrice"]);
                    lo.isDiscl = (bool)rdr["isDiscl"];
                    clLowestOffers.Add(lo);
                }
                return clLowestOffers;
            }
        }


    }

    public static class AsVariables
    {
        public static string POCurrency { get; set; }
        public static string PoFor { get; set; }
        public static string CompanyName { get; set; }
        public static string PONUM { get; set; }

        public static string POCompanyName { get; set; }
        public static string CompanyAddress { get; set; }
    }

    public class LowestOffers
    {
        public int cid { get; set; }
        public int enqID { get; set; }
        public int venID { get; set; }
        public decimal lowPrice { get; set; }
        public bool isDiscl { get; set; }
    }

}
