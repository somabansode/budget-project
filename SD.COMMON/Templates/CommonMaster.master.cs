using System;

namespace SD.COMMON {
    public partial class CommonMaster : System.Web.UI.MasterPage {
        string releaseMode = System.Configuration.ConfigurationManager.AppSettings["ReleaseMode"].ToString().ToLower();
        public string RedirectLogInPage { get { if (releaseMode == "false") return "../../Login/login.html"; else return "~/login.html"; } }
        protected void Page_Load(object sender, EventArgs e) {
            try {
                //var buildDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                Img3.ImageUrl = "~/App_Themes/Profile1/IMAGES/logo.png"; // ImageUrl="~/App_Themes/Profile1/IMAGES/logo.png"
                if (Session["UserId"] == null) {
                    Response.Redirect(RedirectLogInPage + "?LoggedOut=true");
                }
            } catch (Exception) {
            }
        }
        protected void lnkChangeProject_Click(object sender, EventArgs e) {
            if(int.Parse(Session["ModuleID"].ToString()) == 9) Response.Redirect(Request.ApplicationPath + "/OMS/DashBoard.aspx?mode=select");
            else  AlertMsg.MsgBox(Page, "Allowed to change Project only from OMS module!", AlertMsg.MessageType.Warning);
        }
        protected void lnkLogout_Click1(object sender, EventArgs e) {
            Session.Abandon();
            Session["token"] = null;
            Session["email"] = null;
            Global.clSessionRoleMenu = new System.Collections.Generic.List<RoleMenu>();
            Response.Redirect(RedirectLogInPage + "?LoggedOut=true");
        }
        protected void LinkButton1_Click(object sender, EventArgs e) {
            SD.COMMON.WebFormMaster.displayGraph = true;
            Response.Redirect("AdminDefault.aspx");
        }
    }
}