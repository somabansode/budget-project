using System;
using System.Configuration;
using System.Data;
namespace SD.COMMON {
    public partial class ucMenu1 : System.Web.UI.UserControl {
        DataTable _DataSource;
        int SubItemCnt = 0, _RoleId, _MenuId, _MId, m = 0;
        string _mname,mname, _mname1  ;
        string n;
        public int MId { get { return _MId; } set { _MId = value; } }
        public string Mname { get { return _mname; } set { _mname = value; } }
        public string Mname1 { get { return _mname1; } set { _mname1 = value; } }
        public int MenuId { get { return _MenuId; } set { _MenuId = value; } }
        public int RoleId { get { return _RoleId; } set { _RoleId = value; } }
        public DataTable DataSource { get { return _DataSource; } set { _DataSource = value; } }
        public string ModulePrefix { get { return Session["ModulePrefix"].ToString(); } set { Session["ModulePrefix"] = value; } }
        public int ModuleID { get { return Convert.ToInt16(Session["ModuleID"]); } set { Session["ModuleID"] = value; } }
        public string GetPageName(string URL) {
            string url = URL;
            char[] patt = { '/' };
            char[] p = { '.' };
            string[] arr = url.Split(patt);
            url = arr[arr.Length - 1];
            if (url.IndexOf('.') > -1) {
                url = url.Remove(url.IndexOf('.'));
            }
            return url;
        }
        protected override void OnInit(EventArgs e) { base.OnInit(e); }
        protected void filtersearchclick(object sender, EventArgs e) { DataBind(); }
        protected override void OnPreRender(EventArgs e) { base.OnPreRender(e); }
        protected void Page_Load(object sender, EventArgs e) { }
        public override void DataBind() {
            navbar.Controls.Clear();
            if (txtsearch.Text != "") {
                int iMenuID = Convert.ToInt32(txtsearch.Text);
                DataRow[] drs = DataSource.Select("MenuId='" + iMenuID.ToString() + "'", "PreOrder Asc");
                if (drs.Length > 0 && drs[0]["URL"].ToString().Trim() != "#" && drs[0]["URL"].ToString().Trim() != "")  
                    Response.Redirect("~/" + Session["ModulePrefix"].ToString()+ "/" + drs[0]["URL"].ToString());
                 
            }
            DataRow[] drMainMenus = DataSource.Select("Under = '-1'", "PreOrder Asc");
             
            foreach (DataRow dr in drMainMenus) {
                AddMainDiv(navbar, dr["MenuName"].ToString(), dr["MenuID"].ToString(), dr["Help"].ToString());
            }
        }
        protected void lnkChangeProject_Click(object sender, EventArgs e) {
            Response.Redirect("/OMS/DashBoard.aspx?mode=select");
        }
        protected void AddMainDiv(System.Web.UI.HtmlControls.HtmlGenericControl navbar, String MenuText, String MenuId, String Help) {
            System.Web.UI.HtmlControls.HtmlGenericControl mainDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
            mainDiv.Attributes.Add("class", "mainDiv");
            navbar.Controls.Add(mainDiv);
            AddMainDivItems(mainDiv, MenuText, MenuId, Help);
        }
        protected void AddMainDivItems(System.Web.UI.HtmlControls.HtmlGenericControl mainDiv, String MenuText, String MenuId, String Help) {
            System.Web.UI.HtmlControls.HtmlGenericControl ATag = new System.Web.UI.HtmlControls.HtmlGenericControl("A");
            ATag.Attributes.Add("href", "#");
            if (Help.Trim() != String.Empty) {
                ATag.Attributes.Add("onmouseover", "if(t1)t1.Show(event,'" + Help + "')");
                ATag.Attributes.Add("onmouseout", "if(t1)t1.Hide(event)");
            }
            ATag.InnerText = MenuText;
            System.Web.UI.HtmlControls.HtmlGenericControl topItem = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
            topItem.Attributes.Add("class", "topItem");
            topItem.Controls.Add(ATag);
            mainDiv.Controls.Add(topItem);
            System.Web.UI.HtmlControls.HtmlGenericControl dropMenu = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
            dropMenu.Attributes.Add("class", "dropMenu");
            AddDropMenu(dropMenu, MenuId);
            mainDiv.Controls.Add(dropMenu);
        }
        protected void AddDropMenu(System.Web.UI.HtmlControls.HtmlGenericControl dropMenu, String MenuId) {
            System.Web.UI.HtmlControls.HtmlGenericControl subMenu = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
            subMenu.Attributes.Add("class", "subMenu");
            subMenu.Style.Add("display", "inline");
            dropMenu.Controls.Add(subMenu);
            DataRow[] drSubMenus = DataSource.Select("Under='" + MenuId + "'", "PreOrder Asc");
            int i = 0;
            foreach (DataRow dr in drSubMenus) {
                int mid  = Convert.ToInt32(dr["MenuID"].ToString());
                int Menu = Convert.ToInt32(MenuId);
                if (m == mid)  n = dr["MenuName"].ToString();  //flag2 = true;
                if (mid == MId) {  ViewState["mid"] = mid; mname = dr["MenuName"].ToString(); subMenu.Attributes.Add("class", "selected"); } //flag = true;
                AddsubItem(subMenu, dr["MenuName"].ToString(), dr["URL"].ToString(), dr["Help"].ToString(), Convert.ToInt32(drSubMenus[i].ItemArray[0]));
                i++;
            }
        }
        protected void AddsubItem(System.Web.UI.HtmlControls.HtmlGenericControl subMenu, String SubItem, String NavigateUrl, string Help, int submenu) {
            System.Web.UI.HtmlControls.HtmlGenericControl subItem = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
            subItem.Attributes.Add("class", "subItem");
            subMenu.Controls.Add(subItem);
            System.Web.UI.HtmlControls.HtmlGenericControl ATag = new System.Web.UI.HtmlControls.HtmlGenericControl("A");
            ATag.ID = SubItem + (SubItemCnt++).ToString();
            if (ConfigurationManager.AppSettings["ReleaseMode"] == "TRUE") {
                ATag.Attributes.Add("href", Request.ApplicationPath + "/" + ModulePrefix + "/" + NavigateUrl);
            } else {
                ATag.Attributes.Add("href", "../" + ModulePrefix + "/" + NavigateUrl);
            }
            if (Help.Trim() != String.Empty) {
                ATag.Attributes.Add("onmouseover", "if(t1)t1.Show(event,'" + Help + "')");
                ATag.Attributes.Add("onmouseout", "if(t1)t1.Hide(event)");
            }
            ATag.InnerText = SubItem;
            if (GetPageName(Request.RawUrl) == GetPageName(NavigateUrl)) {
                if (Request.RawUrl.Split('/')[Request.RawUrl.Split('/').Length - 1] == NavigateUrl)
                    ATag.Attributes.Add("class", "selected");
            }
            subItem.Controls.Add(ATag);
        }
        protected void txtsearch_Init(object sender, EventArgs e) {
        }
        protected void btnUndoSearch_Click(object sender, EventArgs e) {
            txtsearch.Text = "";
        }
    }
}