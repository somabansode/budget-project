﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace SD.COMMON {
    public partial class topmenu : System.Web.UI.UserControl {
        private int _roleID;
        private int _menuId;
        private int _moduleId;
        private int _SelectedMenu;
        public int SelectedMenu { get { return _SelectedMenu; } set { _SelectedMenu = value; } }
        public int ModuleId { get { return _moduleId; } set { _moduleId = value; } }
        public int RoleID { get { return _roleID; } set { _roleID = value; } }
        public int MenuId { get { return _menuId; } set { _menuId = value; } }
        protected void Page_Load(object sender, EventArgs e) { }
        public override void DataBind() {
            pnltopmenu.Controls.Clear();
             
            var table0 = (from t1 in Global.clMenuAll join t2 in Global.clSessionRoleMenu on t1.MenuID equals t2.MenuID
                          where t2.ModuleID == ModuleId && t1.Under == MenuId
                          select new { t1.MenuID, t1.MenuName, t1.URL, t2.Allowed, t2.Editable, t2.ViewAll, t1.PreOrder }
                          ).ToList().OrderBy(x=>x.PreOrder);
            var parentId = (from t1 in Global.clMenuAll where t1.MenuID ==MenuId  select t1.Under).FirstOrDefault();
            var table1 = (from t1 in Global.clMenuAll join t2 in Global.clSessionRoleMenu on t1.MenuID equals t2.MenuID
                          where t2.ModuleID == ModuleId && t1.Under == parentId
                          select new { t1.MenuID, t1.MenuName, t1.URL, t2.Allowed, t2.Editable, t2.ViewAll, t1.Under, t1.PreOrder }
                          ).ToList().OrderBy(x => x.PreOrder);
            if (table1.Count() > 0)
                foreach (var dr in table1) {
                    int menuid = dr.MenuID; // Convert.ToInt32(dr["MenuId"].ToString());
                    int parent = dr.Under;// Convert.ToInt32(dr["Parent"].ToString());
                    if (pnltopmenu.Controls.Count > 0) {
                        Label lbl = new Label(); lbl.Text = " | "; lbl.ID = UniqueID; pnltopmenu.Controls.Add(lbl);
                    }
                    HyperLink lnk = new HyperLink();
                    lnk.ID = UniqueID; lnk.Text = dr.MenuName;// dr["MenuName"].ToString();
                    if (ConfigurationManager.AppSettings["ReleaseMode"] == "TRUE")
                        lnk.NavigateUrl = HttpContext.Current.Request.ApplicationPath + "/" + Session["ModulePrefix"].ToString() + "/" + dr.URL ; // dr["URL"].ToString();
                    else  lnk.NavigateUrl = "../" + Session["ModulePrefix"].ToString() + "/" + dr.URL; // dr["URL"].ToString();
                    if (parent == menuid) lnk.CssClass = "selected";
                    pnltopmenu.Controls.Add(lnk);
                }
            if (table1.Count() > 0) {
                pnltopmenu.Controls.Add(new LiteralControl("</BR>"));
            }
            foreach (var dr in table0) {
                int menuid = dr.MenuID ;// Convert.ToInt32(dr["MenuId"].ToString());
                if (pnltopmenu.Controls.Count > table1.Count() * 2 ) { // > 0 + (ds.Tables[1].Rows.Count * 2)
                    Label lbl = new Label(); lbl.Text = " | "; lbl.ID = UniqueID; pnltopmenu.Controls.Add(lbl);
                }
                HyperLink lnk = new HyperLink();
                lnk.ID = UniqueID; lnk.Text = dr.MenuName ;// dr["MenuName"].ToString();
                if (ConfigurationManager.AppSettings["ReleaseMode"] == "TRUE") 
                    lnk.NavigateUrl = HttpContext.Current.Request.ApplicationPath + "/" + Session["ModulePrefix"].ToString() + "/" + dr.URL; //dr["URL"].ToString();
                else  lnk.NavigateUrl = "../" + Session["ModulePrefix"].ToString() + "/" + dr.URL; // dr["URL"].ToString();
                if (SelectedMenu == menuid) lnk.CssClass = "selected";
                pnltopmenu.Controls.Add(lnk);
            }
        }
    }
}
