﻿using SD.COMMON.DAL;
using System;
using System.Data;
using System.Data.SqlClient;

namespace SD.COMMON {
    public partial class EmpNotification : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e) {
            SqlParameter[] parms1 = new SqlParameter[2];
            parms1[0] = new SqlParameter("@empid", clSession.cmnUserId);
            parms1[1] = new SqlParameter("ReturnValue", System.Data.SqlDbType.Int);
            parms1[1].Direction = ParameterDirection.ReturnValue;
            DataSet ds = SqlHelper.ExecuteDataset("sg_NotificationsEmpwise", parms1);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                xdata.DataSource = ds;
            else
                xdata.DataSource = null;
            xdata.DataBind();
        }
    }
}