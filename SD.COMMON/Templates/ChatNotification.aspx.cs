﻿using SD.COMMON.DAL;
using System;
using System.Data;
using System.Data.SqlClient;

namespace SD.COMMON {
    public partial class ChatNotification : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e) {
            SqlParameter[] parmsc = new SqlParameter[2];
            parmsc[0] = new SqlParameter("@empid", Session["UserID"].ToString());
            parmsc[1] = new SqlParameter("ReturnValue", System.Data.SqlDbType.Int);
            parmsc[1].Direction = ParameterDirection.ReturnValue;
            DataSet dsc = SqlHelper.ExecuteDataset("sg_chatNotifications", parmsc);
            if (dsc != null && dsc.Tables.Count > 0 && dsc.Tables[0].Rows.Count > 0) 
                gvChatNotification.DataSource = dsc.Tables[1];
             else 
                gvChatNotification.DataSource = null;
            gvChatNotification.DataBind();

        }
    }
}