﻿using SD.COMMON;
using SD.COMMON.DAL;
using System;
using System.Data;
using System.Data.SqlClient;
namespace AECLOGIC.ERP.Templates {
    public partial class OnLineUsers : System.Web.UI.Page {
        protected void Page_Load(object sender, EventArgs e) {
            SqlParameter[] parms12 = new SqlParameter[2];
            parms12[0] = new SqlParameter("ReturnValue", System.Data.SqlDbType.Int);
            parms12[0].Direction = ParameterDirection.ReturnValue;
            parms12[1] = new SqlParameter("@empid", clSession.cmnUserId);
            DataSet ds12 = SqlHelper.ExecuteDataset("sg_noofusercount", parms12);
            if (ds12.Tables[0].Rows.Count > 0) {
                grdNoOfUsers.DataSource = ds12.Tables[0];
            } else
                grdNoOfUsers.DataSource = null;
            grdNoOfUsers.DataBind();
        }
    }
}