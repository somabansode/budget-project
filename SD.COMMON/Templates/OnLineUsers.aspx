﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OnLineUsers.aspx.cs" Inherits="AECLOGIC.ERP.Templates.OnLineUsers" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../App_Themes/Profile1/StyleSheet.css" rel="stylesheet" />
    <link href="../App_Themes/Profile1/aec_main_styles.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:GridView ID="grdNoOfUsers" runat="server" AutoGenerateColumns="false"
                EmptyDataText="No Records Found" CssClass="gridview" Style="border-collapse: collapse; width: 250px; font-size: 11px">
                <Columns>
                    <asp:BoundField HeaderText="Details" DataField="Details" />
                </Columns>
            </asp:GridView>
        </div>
    </form>
</body>
</html>
