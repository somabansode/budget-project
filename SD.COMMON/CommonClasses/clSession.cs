﻿using System;
using System.Web;
namespace SD.COMMON {
    [Serializable]
    public class clSession {
        private static int _UserId;
        public static int cmnUserId {
            get { return Convert.ToInt32(HttpContext.Current.Session["UserId"]); }
            set { HttpContext.Current.Session["UserId"] = value; }
        }
        private static string _UserName;
        public static string cmnUserName {
            get { return Convert.ToString(HttpContext.Current.Session["UserName"]); }
            set { HttpContext.Current.Session["UserName"] = value; }
        }
        private static int _CompanyID;
        public static int cmnCompanyID {
            get { return Convert.ToInt32(HttpContext.Current.Session["CompanyID"]); }
            set { HttpContext.Current.Session["CompanyID"] = value; }
        }
        private static int _PrjID;
        public static int cmnPrjID {
            get { return Convert.ToInt32(HttpContext.Current.Session["PrjId"]); }
            set { HttpContext.Current.Session["PrjId"] = value; }
        }
        private static int _CostCenterID;
        public static int cmnCostCenterID {
            get { return Convert.ToInt32(HttpContext.Current.Session["CostCenterID"]); }
            set { HttpContext.Current.Session["CostCenterID"] = value; }
        }
    }
}