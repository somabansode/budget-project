﻿using Newtonsoft.Json;
using SD.COMMON.DAL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
namespace SD.COMMON {
    public class WebFormMaster : System.Web.UI.Page {
        private bool InSession = false;
        public string accessMode { get; set; }
        private string loginId = "", errMsg = "";
        public ucProcess_3 ucProcess;
        public List<AppMenu> userAppMenu = new List<AppMenu>();
        public string PageTitle {set {Page.Title = value;}get {return PageTitle;}}
        private string ModuleTitle = string.Empty;
        public int ModuleID { get; set; }//this gets set from every ERP page 
        public int CompanyID { get { return Convert.ToInt32(HttpContext.Current.Session["CompanyID"]); } }
        public int gEmpID { get {return Convert.ToInt32(HttpContext.Current.Session["gEmpID"]);} }
        public string releaseMode = System.Configuration.ConfigurationManager.AppSettings["ReleaseMode"].ToString().ToLower();
        public static AppSetting AppSettings = new AppSetting();
        public string RedirectLogInPage { get { if (releaseMode == "false") return "../../Login/login.html"; else return "~/login.html"; }   }
        //public string RedirectLogInPage = "../../Login/Login.html"; //"~/Logon.aspx"
        protected override void OnPreInit(EventArgs e) {
            if (Session["accessMode"] == null) SetAccessMode(); else accessMode = Session["accessMode"].ToString();
            base.OnPreInit(e); Page.Theme = "Profile1";
            if(accessMode == "angular") Page.MasterPageFile = Request.ApplicationPath + "/Templates/Angular.master";
            else Page.MasterPageFile = Request.ApplicationPath + "/Templates/CommonMaster.master";
        }
        private void SetAccessMode() {
            try { Session["accessMode"] = accessMode = Request.QueryString["dotnet_angular"]; } //["mode"]
            catch(Exception ex) {
                clsErrorLog.Log(ex);
                Response.Redirect(RedirectLogInPage + "?Session[accessMode] missing!"); }
            if (accessMode == "dotnet") {
                Session["token"] = Request.QueryString["token"].ToString();
                Session["email"] = Request.QueryString["userId"].ToString();
            }
        }
        protected override void OnInit(EventArgs e) {

            if (accessMode == "dotnet") {
                if (Session["token"] != null) authenticationCheck_GetRole(Session["email"].ToString(), Session["token"].ToString());
                else Response.Redirect(RedirectLogInPage + "?Session token found null");
            }
            string strSessionID = HttpContext.Current.Session.SessionID;
            var appUser = (from t in Global.clAppUsers where t.appSession == strSessionID & t.mid == ModuleID 
                           & t.cid == CompanyID & t.uid == gEmpID select t).FirstOrDefault();
            if (appUser != null && Session["LoginId"] != null) { //&& Global.clSessionRoleMenu.Count !=0 && Session["ModuleID"].ToString() == ModuleID.ToString()) 
                InSession = true;
                loginId = Session["LoginId"].ToString(); 
                AppSettings = (from t in Global.clAppSettings where t.CompanyID == appUser.cid.ToString() select t).FirstOrDefault();
                var modTitle = (from t in Global.clModules where t.mid == ModuleID select t.Title).FirstOrDefault();
                Session["ModulePrefix"] = modTitle;
                if (Global.clSessionRoleMenu.Count == 0) roleAll(loginId);
                MenuHreferences(ModuleID);
            } else {
                try {
                    appUser = SetAppCurUsers(strSessionID, out errMsg);
                    if(appUser == null) Response.Redirect(RedirectLogInPage + "?ERPMaster_OnInit_SetAppCurUsers__" + errMsg);
                    if (Global.clModules.Count() == 0) SetModules(out errMsg);
                    var modTitle = (from t in Global.clModules where t.mid == ModuleID select t.Title).FirstOrDefault();
                    Session["ModulePrefix"] = modTitle;
                    if (errMsg != "") Response.Redirect(RedirectLogInPage + "?ERPMaster_OnInit_SetModules__" + errMsg);
                    SessionVariables(Session["LoginId"].ToString());
                } catch (Exception ex) { clsErrorLog.Log(ex);
                    Session.Abandon(); Response.Redirect(RedirectLogInPage + "?ERPMaster_OnInit_ss_IsNullCase=" + ex.Message); }
            }
            base.OnInit(e);
            FindMasterControls(); Session["UserId"] = appUser.uid; //for use in CommonMaster Page //clSession.cmnUserId
            if (InSession)  Session["CurrentPage"] = Request.Url.Segments[Request.Url.Segments.Length - 1] + Request.Url.Query;
            else Response.Redirect(RedirectLogInPage + "?ErrorNew=0");
            TopMenuWithPagePath(); //QAID first line of page.
            
            if (accessMode == "dotnet") { FindSecondLevelControls(); }
        }
        private DataSet GetCurrentPagePath(string URL, int ModuleId)
        {
            try
            {
                SqlParameter[] objParam = new SqlParameter[2];
                objParam[0] = new SqlParameter("@URL", URL);
                objParam[1] = new SqlParameter("@ModuleId", ModuleId);
                return SQLDBUtil.ExecuteDataset("CP_Get_PageHelp", objParam);//GetCurrentPagePath
            }
            catch (Exception ex) { clsErrorLog.Log(ex);throw ex; }
        }

        private void BindCurPath()
        {
            //if (!IsPostBack)
            //{
                Session["CurrentPage"] = Request.Url.Segments[Request.Url.Segments.Length - 1] + Request.Url.Query;
                string URL = Session["CurrentPage"].ToString();
                DataSet dsCurPath = GetCurrentPagePath(URL, ModuleID);
                using (dsCurPath = GetCurrentPagePath(Session["CurrentPage"].ToString(), ModuleID))
                {
                    if (dsCurPath != null && dsCurPath.Tables.Count > 0 && dsCurPath.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = dsCurPath.Tables[0].Rows[0];
                        if (ucProcess != null) ucProcess.MenuID = dr["MenuId"].ToString();
                        if (dr["Help"].ToString().Trim() != "") PageTitle = ModuleTitle + "::" + dr["Title"].ToString().Replace(" ", "");
                        PageTitle = ModuleTitle + "::" + dr["Title"].ToString().Replace(" ", "");
                        divPagePath.InnerHtml = dr["BreadCrumb"].ToString();
                        ViewState["PageTitle"] = this.Page.Title;
                    }
                }
            //}
        } //QAID first line of page.
        public Label lblCID, lblUID;
        private void FindMasterControls() {
            divPagePath = (HtmlGenericControl)Page.Master.FindControl("lblBreadCrumb");//QAID is with this control
            pagetopmenu = (topmenu)Page.Master.FindControl("topmenu");
            trPrj = (HtmlTableRow)Page.Master.FindControl("trPrj");
            tdCostCenter = (HtmlTableCell)Page.Master.FindControl("tdCostCenter");
            lblCostCenter = (Label)Page.Master.FindControl("lblCostCenter");
            lblPrjName = (Label)Page.Master.FindControl("lblPrjName");
            lblCID = (Label)Page.Master.FindControl("lblCID");
            lblUID = (Label)Page.Master.FindControl("lblUID");
            ucProcess = (ucProcess_3)Page.Master.FindControl("Process");
        }
        private AppUser SetAppCurUsers(string strSessionID, out string err) {
            try {
                loginId = Request.QueryString["userId"].ToString();
                int mid = int.Parse(Request.QueryString["moduleId"]);
                int cid = int.Parse(Request.QueryString["companyId"]);
                accessMode = Request.QueryString["dotnet_angular"];//["mode"];
                Session["accessMode"] = accessMode;
                if (accessMode == "angular") {
                    Session["RoleId"] = Request.QueryString["roleId"];
                }
                string erpCS = ConfigurationManager.ConnectionStrings["ERPConnectionString"].ConnectionString;
                SqlParameter[] sp = new SqlParameter[4];
                sp[0] = new SqlParameter("@loginId", loginId);
                sp[1] = new SqlParameter("@mid", mid);
                sp[2] = new SqlParameter("@cid", cid);
                sp[3] = new SqlParameter("@mode", accessMode);
                DataSet ds = new DataSet();
                if(mid==13) ds =SQLDBUtil.ExecuteDataset("sg_appCurUsers_Vendors", sp);
                else ds = SQLDBUtil.ExecuteDataset("sg_appCurUsers", sp);
                if (ds.Tables[0].Rows.Count > 0) {
                    DataRow dr = ds.Tables[0].Rows[0];
                    int ngs = dr["ngSession"] == DBNull.Value ? 0 : (int)dr["ngSession"];
                    Global.clAppUsers.Add(new AppUser(cid, (int)dr["eid"], mid, ngs, strSessionID));
                    Session["CompanyID"] = cid;
                    AppUser _appUser = (from t in Global.clAppUsers where t.appSession == strSessionID & t.mid == ModuleID
                                                & t.cid == CompanyID & t.uid == gEmpID select t).FirstOrDefault();
                    if (_appUser == null) { err = "app user null"; return null; }
                    InSession = true;
                   Session["ModuleID"] = _appUser.mid; Session["LoginId"] = loginId;
                    var appSet = (from t in Global.clAppSettings where t.CompanyID == cid.ToString() select t.CompanyID).Distinct();
                    if (appSet.Count() == 0) SetAppSettings(cid);
                    if (Global.clMenuAll.Count == 0) menuAll();
                    if (Global.clSessionRoleMenu.Count == 0) roleAll(loginId);

                    //roleBasedMenu(mid, int.Parse(Session["RoleId"].ToString()));
                    MenuHreferences(ModuleID);//populates ClMenuPaths
                    err = "";
                    return _appUser;
                } else { err = "User does not have licence to use the application"; return null; }
            } catch (Exception ex) { clsErrorLog.Log(ex); err = ex.Message; return null; }
        }
        private void authenticationCheck_GetRole(string email, string token) {
            try {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://" + Request.Url.Host + "/Login/api/master/?data=" + email + "," + ModuleID.ToString());
                request.Method = "GET";
                request.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + token);
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                if (response.StatusCode == HttpStatusCode.OK) {
                    using (var reader = new StreamReader(response.GetResponseStream())) {
                        var ApiStatus = reader.ReadToEnd();
                            Session["gEmpID"] = int.Parse((((Newtonsoft.Json.Linq.JObject)JsonConvert.DeserializeObject(ApiStatus))
                                                                        ["Table"].ToArray()[0]["EmpId"]).ToString().Replace("{", "").Replace("}", ""));
                        Session["RoleId"] = int.Parse((((Newtonsoft.Json.Linq.JObject)JsonConvert.DeserializeObject(ApiStatus))
                                            ["Table"].ToArray()[0]["RoleId"]).ToString().Replace("{", "").Replace("}", ""));
                    }
                } else Response.Redirect( RedirectLogInPage + "?" + response.StatusCode + "; " + response.StatusDescription);
            } catch (Exception ex) { clsErrorLog.Log(ex); Response.Redirect(RedirectLogInPage + "?" + ex.Message); }
        }
        private void FindSecondLevelControls() {
            tdModuleBar = (HtmlTableCell)Page.Master.FindControl("tdModuleBar");
            BuildModuleBar();
            ucMenu = (ucMenu1)Page.Master.FindControl("ucMenu1");
            BindMenu();
            ucProcess = (ucProcess_3)Page.Master.FindControl("Process");
            BindCurPath();
        }
        private void BuildModuleBar() {
            StringBuilder sbModuleBar = new StringBuilder();
            string strSelectedClass = "ModuleTabSelected";
            string strNormalClass = "ModuleTab";
            string strModuleName = string.Empty;
            for (int i = 0; i < Global.clModules.Count; i++) {
                int iModuleID = Convert.ToInt16(Global.clModules[i].mid);
                //CREATING HYPERLINK for Module BUTTON 
                string url = "?userId=" + Session["email"] + "&moduleId=" + Global.clModules[i].mid + "&companyId=" + Session["CompanyID"]
                    + "&dotnet_angular=dotnet&token=" + Session["token"].ToString();
                if(releaseMode == "false") sbModuleBar.Append("<a href='../../").Append(Global.clModules[i].Redirect).Append(url + "'  ");
                else sbModuleBar.Append("<a href='../").Append(Global.clModules[i].Title + "/AdminDefault.aspx").Append(url + "'  ");
                //BUILDING MODULE BUTTON
                sbModuleBar.Append("class='");
                if (Request.QueryString["ModuleId"] != null) {
                    if (Convert.ToInt32(Request.QueryString["ModuleId"]) == iModuleID) {
                        Session["ModulePrefix"] = ViewState["ModuleTitle"] = Global.clModules[i].Title;
                        sbModuleBar.Append(strSelectedClass).Append("'>").Append(Global.clModules[i].Title);
                    } else {
                        sbModuleBar.Append(strNormalClass).Append("'>").Append(Global.clModules[i].Title.Substring(0, 1));
                    }
                } else if (ModuleID == iModuleID) {
                    Session["ModulePrefix"] = Global.clModules[i].Title;
                    sbModuleBar.Append(strSelectedClass).Append("'>").Append(Global.clModules[i].Title);
                } else {
                    sbModuleBar.Append(strNormalClass).Append("'>").Append(Global.clModules[i].Title.Substring(0, 1));
                }
                sbModuleBar.Append("</a>");
            }
            tdModuleBar.InnerHtml = sbModuleBar.ToString();
        }
        protected void BindMenu() {
            //if (!ucMenu.IsPostBack || ucMenu.DataSource == null) {
            if (Session["menuid"] != null) ucMenu.MenuId = Convert.ToInt32(Session["menuid"].ToString());
            if (Session["MId"] != null) ucMenu.MId = Convert.ToInt32(Session["MId"].ToString());
            if (Session["menuname"] != null) ucMenu.Mname = Session["MenuName"].ToString();
            if (Session["RoleId"] != null) {
                ucMenu.RoleId = int.Parse(Session["RoleId"].ToString());
                userAppMenu = new List<AppMenu>();
                userAppMenu = (from t1 in Global.clMenuAll join t2 in Global.clSessionRoleMenu on t1.MenuID equals t2.MenuID where t2.ModuleID == ModuleID select t1).ToList();
                if(ucMenu.DataSource!=null) ucMenu.DataSource.Clear();
                ucMenu.DataSource = ConvertList2DataTable<AppMenu>(userAppMenu)  ; //GetUcMenu(ModuleID, int.Parse(Session["RoleId"].ToString()));
                if (Request.QueryString["ModuleId"] != null)   ucMenu.ModuleID = Convert.ToInt32(Request.QueryString["ModuleId"]);
                else ucMenu.ModuleID = ModuleID;
                ucMenu.ModulePrefix = Session["ModulePrefix"].ToString();
                ucMenu.DataBind();
            }
        }
        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
        }
       protected override void OnPreRender(EventArgs e) {
            base.OnPreRender(e);
            if(accessMode == "dotnet") { 
                lblCostCenter.Text = Session["CostCenterID"].ToString();
                lblPrjName.Text = Session["PrjId"].ToString();
                lblCID.Text = "CompanyID: "  + (Session["CompanyID"] == null ?  Request.QueryString["companyId"] : Session["CompanyID"].ToString());
                lblUID.Text = "Email: " + Session["email"].ToString();
            }
            try {
                Session["CurrentPage"] = "";//why is this??
            } catch (Exception ex){ clsErrorLog.Log(ex); }
            if (topRowHeader != null) topRowHeader.Visible = true;//for ams show change topmenu option
            else {
                if (topRowHeader != null) topRowHeader.Visible = false;
            }
        }
        public int ProjectID { get { return Convert.ToInt32(Session["PrjId"]); } set { Session["PrjId"]=value; } }
        private void SessionVariables(string loginID) {
            DataSet dsLogin = Common1.SessionVariables(loginID, ModuleID);
            if (dsLogin.Tables.Count > 0 && dsLogin.Tables[0].Rows.Count > 0) {
                Session["UserId"] = dsLogin.Tables[0].Rows[0]["empid"].ToString();
                clSession.cmnUserId = Convert.ToInt32(Session["UserId"]);
                Session["UserName"] = dsLogin.Tables[0].Rows[0]["Name"].ToString();
                clSession.cmnUserName = Session["UserName"].ToString();
                Session["Type"] = dsLogin.Tables[0].Rows[0]["Type"].ToString();
                Session["Site"] = dsLogin.Tables[0].Rows[0]["Categary"].ToString();
            } else Response.Redirect(RedirectLogInPage + "?SessionExpire=" + true + 2);
            if (dsLogin.Tables.Count > 1 && dsLogin.Tables[1].Rows.Count > 0) {
                Session["RoleId"] = dsLogin.Tables[1].Rows[0]["RoleId"].ToString();
                Session["RoleName"] = dsLogin.Tables[1].Rows[0]["RoleName"].ToString();
            } else {
                AlertMsg.MsgBox(Page, "No Roles Assigned for this module" + "\n" + "Contact your Administrator");
                Response.Redirect(RedirectLogInPage + "?SessionExpire=" + true);
            }
            if (dsLogin.Tables.Count > 2 && dsLogin.Tables[2].Rows.Count > 0) {
                Session["PrjName"] = dsLogin.Tables[2].Rows[0]["PrjName"].ToString();
                Session["PrjId"] = dsLogin.Tables[2].Rows[0]["PrjId"].ToString();
                Session["CostCenterID"] = dsLogin.Tables[2].Rows[0]["CostCenterID"].ToString();
            }
            if (dsLogin.Tables.Count > 3 && dsLogin.Tables[3].Rows.Count > 0) {
                Session["FinYearID"] = dsLogin.Tables[3].Rows[0][0].ToString();
            } else Response.Redirect(RedirectLogInPage + "?SessionExpire=" + true + 3);
        }
        private void MenuHreferences(int MID) {
            Global.clMenuPaths = new List<MenuPathHref>();
            var nwArray = (from t1 in Global.clMenuAll join t2 in Global.clSessionRoleMenu on t1.MenuID equals t2.MenuID where t2.ModuleID == MID
                           select t1).ToList();//DataTable dt = ConvertList2DataTable<AppMenu>(nwArray);
            foreach (AppMenu am in nwArray) {
                if (am.Under != -1 && am.URL != "") {
                    MenuPathHref mp = new MenuPathHref(
                            am.MenuID,
                            am.MenuGroup + " -> " + am.MenuName + " -> " + am.URL,
                            buildHref(am.MenuID, am.URL).Replace(" -> " + am.URL, "")
                        );
                    Global.clMenuPaths.Add(mp);
                }
            }//DataTable  dt2 = ConvertList2DataTable<MenuPathHref>(clMenuPaths);
        }
        private static string buildHref(int menuID, string pageURL) {
            string xPath = string.Empty;
            var varPar = (from t in Global.clMenuAll where t.MenuID == menuID select new { t.Under, t.MenuName, t.URL }).FirstOrDefault();
            if (varPar != null) {
                int parent = varPar.Under;
                if (varPar.URL == "") xPath = varPar.MenuName;
                else xPath = "<a href='" + varPar.URL + "' >" + varPar.MenuName + "</a>";
                if (parent > 0) xPath = buildHref(parent, xPath);
            }
            if (pageURL != "") xPath = xPath + " -> " + pageURL;
            return xPath;
        }
        private void TopMenuWithPagePath() {
            try {
                string URL = Session["CurrentPage"].ToString();
                if (!URL.Contains("AdminDefault.aspx")) {
                    _pageAccess = pageAccess;
                    if (pageAccess != null) {
                        MenuID = mid = _pageAccess.MenuID;
                        Editable = _pageAccess.Editable;
                        ViewAll = _pageAccess.ViewAll;
                        menuname = _pageAccess.MenuName;
                        Page.Title = Session["ModulePrefix"].ToString() + "::" + _pageAccess.Title.ToString().Replace(" ", "");
                        string QAID = "   <SPAN title='Quick Access: Alt+Q and type " + MenuID.ToString()
                           + " then click Enter key.' style='color:gray; font-wieght:bolder; font-size:xx-small;'> " +
                           "[QAID:<span  style='color:red;'>" + MenuID.ToString() + "</span>]</SPAN>";
                        divPagePath.InnerHtml = _pageAccess.Href + QAID;
                        ViewState["PageTitle"] = this.Page.Title;
                        //if (pagetopmenu != null) {
                        pagetopmenu.MenuId = MenuID;
                        if (Request.QueryString["ModuleId"] != null) pagetopmenu.ModuleId = Convert.ToInt32(Request.QueryString["ModuleId"]);
                        else pagetopmenu.ModuleId = ModuleID;
                        pagetopmenu.RoleID = int.Parse(Session["RoleId"].ToString());
                        pagetopmenu.SelectedMenu = Convert.ToInt32(MenuID.ToString());
                        pagetopmenu.DataBind();
                        ViewState["mid"] = MenuID;
                        ViewState["MenuID"] = MenuID;
                        ViewState["MenuName"] = menuname;
                        Session["ProcessMenuID"] = MenuID;
                        pagetopmenu.Visible = true;
                    }
                    //else MsgBox("Page NOT INCLUDED IN MENU; Redesign the page with Model Popup");
                }
            } catch (Exception ex) {
                clsErrorLog.Log(ex);
                MsgBox(ex.Message);
                //SessionVariables(loginId);
            }
        }
        public int    GetParentMenuId() {
            try {
                string URL="";
                if (Session["CurrentPage"] != null){
                      URL = Session["CurrentPage"].ToString();//string URL = Request.Url.Segments[Request.Url.Segments.Length - 1] + Request.Url.Query;
                }
                else { Response.Redirect(RedirectLogInPage + "?" + "Current page session timeout"); }
                if (!URL.Contains("AdminDefault.aspx")) {
                    var pageAccess = (from t1 in Global.clMenuAll join t2 in Global.clSessionRoleMenu on t1.MenuID equals t2.MenuID where t1.URL == URL
                                      select new { t1.MenuID, t1.MenuName, t1.URL, t2.Editable, t2.ViewAll }).FirstOrDefault();
                    MenuID = mid = pageAccess.MenuID;
                    Editable = pageAccess.Editable == 1 ? true : false;
                    ViewAll = pageAccess.ViewAll == 1 ? true : false;
                    menuname = pageAccess.MenuName;
                }
            } catch (Exception ex) { clsErrorLog.Log(ex); }
            return MenuID;
        }
        //these three methods are used in Accounts several places
        public int GetParentMenuId(string URL, int RoleId) {
            return GetParentMenuId();
        }
        public DataSet GetAllowed() {
            DataSet ds = new DataSet();
            List<PageAccess> pa = new List<PageAccess>();
            pa.Add(_pageAccess);
            DataTable dt = ConvertList2DataTable<PageAccess>(pa);
            ds.Tables.Add(dt);
            return ds;
        }
        public void CallScriptFunction(object curPage, string msg) {
            Page myPage = (Page)curPage;
            ScriptManager.RegisterStartupScript(myPage, myPage.GetType(), Guid.NewGuid().ToString(), msg, true);
        }
        public void CallScriptMethod(Page Source, string Method) {
            string strScript = "<script language='javascript'>" + Method + ";</script>";
            ScriptManager.RegisterStartupScript(Source, Source.GetType(), "PopupCP", strScript, false);
        }
        protected void MsgBox(string alert) {
            string strScript = "<script language='javascript'>alert('" + alert.Replace("'", "\\'") + "');</script>";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PopupCP", strScript, false);
        }
        public void ApplyStyle(ref Object Ctrl, string Style, string Value) {
            if (Ctrl.GetType() == typeof(DropDownList)) {
                ((DropDownList)Ctrl).Style.Remove(Style);
                ((DropDownList)Ctrl).Style.Add(Style, Value);
            } else if (Ctrl.GetType() == typeof(Label)) {
                ((Label)Ctrl).Style.Remove(Style);
                ((Label)Ctrl).Style.Add(Style, Value);
            } else if (Ctrl.GetType() == typeof(TextBox)) {
                ((TextBox)Ctrl).Style.Remove(Style);
                ((TextBox)Ctrl).Style.Add(Style, Value);
            }
        }
        public string CompanyName { get { return AppSettings.CompanyName; } }
        #region Global Messages
        public static string GetGlobalMessages(string Key) {
            try {
                return HttpContext.GetGlobalResourceObject("Messages", Key).ToString();
            } catch(Exception ex) { clsErrorLog.Log(ex); return Key; }
        }
        public static string MessageFormat(string key, string[] args) {
            return String.Format(GetGlobalMessages(key), args);
        }
        public static string GetGlobalResource(string Key) {
            return HttpContext.GetGlobalResourceObject("GlobalRes", Key).ToString();
        }
        #endregion Global Messages
        private string GetUserIP() {
            string rtval = "";
            try {
                rtval = Request.ServerVariables["REMOTE_ADDR"];
            } catch (Exception ex) { clsErrorLog.Log(ex);  }
            return rtval;
        }
        decimal TotalCr; decimal TotalDr;
        string menuname; int mid = 0;
        //public static string DateDisplayFormat =  AppSettings.DateDisplayFormat;
        public static string DateDisplayFormat { get { return AppSettings.DateDisplayFormat;  } }
        public string SMSStatus = System.Configuration.ConfigurationManager.AppSettings["SMSStatus"];
        public static Hashtable _pageArraylist;
        public topmenu pagetopmenu;
        public HtmlGenericControl myBody ;
        public HtmlGenericControl divPagePath;
        public HtmlGenericControl divModuleBar;
        public HtmlGenericControl dvheader;
        public HtmlGenericControl sidenavigation;
        public HtmlTableCell tdModuleBar;
        public static ucMenu1 ucMenu;
        //public ucProcess_3 ucProcess;
        public LinkButton lnkChangeProject;
        public HtmlTableRow trPrj, topRowHeader;
        public static ArrayList alRoles = new ArrayList();
        public HtmlTableCell tdCostCenter;
        public Label lblCostCenter, lblPrjName; 
        public Hashtable QueryString;
        
        public static bool displayGraph = false;
        int _MenuID;
        bool _viewAll, _editable;
        public bool Editable { get { return _editable; } set { _editable = value; } }
        public bool ViewAll { get { return _viewAll; } set { _viewAll = value; } }
        public int MenuID { get { return _MenuID; } set { _MenuID = value; } }
        public string CompanyAddress { get { return AppSettings.CompanyAddress; } }
        public string HeadOffice { get { return AppSettings.CostCenterID; } }
        public string DecimalPlaces { get { return AppSettings.DecimalPlaces; } }
        public string SMSUserID { get { return AppSettings.SMSUserID; } }
        public string SMSPassword { get { return AppSettings.SMSPassword; } }
        public string DateValueFormat { get { return HttpContext.GetGlobalResourceObject("GlobalRes", "DateValueFormat").ToString(); } }        
        
        public string GetAmt(decimal Price) {
            string amt = string.Empty;
            Price = Convert.ToDecimal(Price.ToString(DecimalPlaces));
            if (Price != 0) {
                amt = Price.ToString(DecimalPlaces);
            }
            return amt;
        }
        public string GetAmtCr(decimal Price) {
            string amt = string.Empty;
            TotalCr += Price;
            Price = Convert.ToDecimal(Price);
            Session["val"] = TotalCr;
            if (Price != 0) {
                amt = NUMCommon.NumberFormatting(Convert.ToDouble(Price.ToString("N2")));
            }
            return amt;
        }
        protected string GetAmtDr(decimal Price) {
            string amt = string.Empty;
            TotalDr += Price;
            Price = Convert.ToDecimal(Price);
            if (Price != 0) {
                amt = NUMCommon.NumberFormatting(Convert.ToDouble(Price.ToString("N2")));
            }
            return amt;
        }
        public string GetTotalDr() {
            return TotalDr.ToString(DecimalPlaces);
        }
        public string GetTotalCr() {
            return TotalCr.ToString(DecimalPlaces);
        }
        public string GetDrAmt(decimal Price) {
            TotalDr += Price;
            Price = Convert.ToDecimal(Price.ToString(DecimalPlaces));
            return Price.ToString(DecimalPlaces);
        }
        public string GetCrAmt(decimal Price) {
            TotalCr += Price;
            Price = Convert.ToDecimal(Price.ToString(DecimalPlaces));
            return Price.ToString(DecimalPlaces);
        }
        public string ComputeTotals(string TransID, string TransType) {
            DataSet dstrans = (DataSet)ViewState["DataSet"];
            double ctotA = 0;
            double dtotA = 0;
            string cTot = "";
            string dTot = "";
            if (TransType == "Cr") {
                cTot = dstrans.Tables[1].Compute("sum(CreditAmt)", "TransID='" + TransID + "'").ToString();
                ctotA = Convert.ToDouble(cTot == "" ? "0" : cTot);
                return ctotA.ToString("N2");
            } else {
                dTot = dstrans.Tables[1].Compute("sum(debitAmt)", "TransID='" + TransID + "'").ToString();
                dtotA = Convert.ToDouble(dTot == "" ? "0" : dTot);
                return dtotA.ToString("N2");
            }
        }
        public DataView BindTransdetails(string TransId) {
            try {
                DataSet dstrans = (DataSet)ViewState["DataSet"];
                DataView dv = dstrans.Tables[1].DefaultView;
                dv.RowFilter = "transid='" + TransId + "'";
                return dv;
            } catch (Exception Ex) {
                clsErrorLog.Log(Ex);
                   throw Ex;
            }
        }
        //private static readonly Regex REGEX_FOR_TAGS = new Regex(@">s+<", RegexOptions.Compiled);
        //private static readonly Regex REGEX_FOR_BREAKS = new Regex(@">[\s]*<", RegexOptions.Compiled);
        public TreeNode findTreeNodebyValue(ref TreeNodeCollection nodes, string value) {
            TreeNode retTreeNode = null;
            foreach (TreeNode tn in nodes) {
                tn.Expand();
                if (tn.Value.Trim().ToLower() == value.Trim().ToLower())
                    retTreeNode = tn;
                else {
                    tn.Selected = false;
                    TreeNodeCollection tnc = tn.ChildNodes;
                    retTreeNode = findTreeNodebyValue(ref tnc, value);
                }
                if (retTreeNode != null)
                    break;
                else
                    tn.Collapse();
            }
            return retTreeNode;
        }
        public void Expand(TreeNode Selected) {
            if (Selected != null) {
                Selected.Expand();
                if (Selected.Parent != null) {
                    Expand(Selected.Parent);
                }
            }
        }
        private PageAccess _pageAccess = null;
        public PageAccess pageAccess { get {
                string URL = Session["CurrentPage"].ToString();
                return (from t1 in Global.clMenuAll join t2 in Global.clSessionRoleMenu on t1.MenuID equals t2.MenuID
                        join t3 in Global.clMenuPaths on t2.MenuID equals t3.MenuID where t1.URL == URL
                        select new PageAccess(t1.MenuID, t1.ModID, t2.RoleID, t2.Allowed == 1 ? true : false, t2.Editable == 1 ? true : false, t2.ViewAll == 1 ? true : false
                        , t1.Under, t1.MenuName, t3.Title, t3.Href)).FirstOrDefault();
            }
        }
        private static DataTable ConvertList2DataTable<T>(List<T> items) {
            DataTable dataTable = new DataTable(typeof(T).Name);
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);//Get all the properties
            foreach (PropertyInfo prop in Props) { dataTable.Columns.Add(prop.Name); }//Setting column names as Property names
            foreach (T item in items) {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++) { values[i] = Props[i].GetValue(item, null); }//inserting property values to datatable rows
                dataTable.Rows.Add(values);
            }
            return dataTable;//put a breakpoint here and check datatable
        }
        public void menuAll() {
            Global.clMenuAll=new List<AppMenu>();
            try {
                DataSet ds = SQLDBUtil.ExecuteDataset("sg_menuAll"); // if this SP is slow use other SP 'sg_menuAll_Admin'
                foreach (DataRow dr in ds.Tables[0].Rows) {
                    AppMenu mnu = new AppMenu(ModuleID, dr["Title"].ToString(), Convert.ToInt32(dr["MenuID"]), dr["MenuName"].ToString(),
                        dr["MenuGroup"].ToString(), dr["URL"].ToString()
                        , dr["Under"] == DBNull.Value ? -1 : (int)dr["Under"]
                        , dr["PreOrder"] == DBNull.Value ? -1 : (int)dr["PreOrder"]
                        , (int)dr["Level"], dr["Help"].ToString());
                    Global.clMenuAll.Add(mnu);
                    //int? Under  int? PreOrder   int ngs = dr["ngSession"] == DBNull.Value ? 0 : (int)dr["ngSession"];
                }
            } catch (Exception ex) {
                clsErrorLog.Log(ex);
                //throw ex;
            }
        }
        private void roleAll(string loggedID) {
            try {
                DataSet ds = SQLDBUtil.ExecuteDataset("sg_roleAll", new SqlParameter[] { new SqlParameter("@loginID", loggedID) });
                foreach (DataRow dr in ds.Tables[0].Rows) {
                    RoleMenu rm = new RoleMenu(Convert.ToInt32(dr["MenuID"]), Convert.ToInt32(dr["ModuleID"]), Convert.ToInt32(dr["RoleID"])
                       , Convert.ToInt32(dr["Allowed"]), Convert.ToInt32(dr["Editable"]), Convert.ToInt32(dr["ViewAll"]));
                    Global.clSessionRoleMenu.Add(rm);
                }
            } catch (Exception ex) {
                clsErrorLog.Log(ex);
                //  throw ex;
            }
        }
        private void xxroleBasedMenu(int ModuleId, int RoleId) {
            var ids = (from t in Global.clSessionRoleMenu where t.ModuleID == ModuleId select t.MenuID).ToList();
            //if (ids.Count != 0) return;
            try {
                DataSet ds = SQLDBUtil.ExecuteDataset("sg_roleBasedMenu", new SqlParameter[] {
                            new SqlParameter("@ModuleID", ModuleId), new SqlParameter("@RoleId", RoleId) });
                foreach (DataRow dr in ds.Tables[0].Rows) {
                    RoleMenu rm = new RoleMenu(Convert.ToInt32(dr["MenuID"]), Convert.ToInt32(dr["ModuleID"]), Convert.ToInt32(dr["RoleID"])
                        , Convert.ToInt32(dr["Allowed"]), Convert.ToInt32(dr["Editable"]), Convert.ToInt32(dr["ViewAll"]));
                    if (!ids.Contains(rm.ModuleID)) Global.clSessionRoleMenu.Add(rm);
                }
            } catch (Exception ex)  { clsErrorLog.Log(ex);
                throw ex;
            }
        }
        private void SetAppSettings(int cid) {
            string erpCS = ConfigurationManager.ConnectionStrings["ERPConnectionString"].ConnectionString;
            SqlParameter[] sp = new SqlParameter[1];
            sp[0] = new SqlParameter("@cid", cid);
            AppSetting os = new AppSetting();
            DataSet ds = SQLDBUtil.ExecuteDataset("sp_appSettings", sp);
            //table 1
            DataRow dr = ds.Tables[0].Rows[0];
            os.CompanyID = cid.ToString();
            os.CompanyName = dr["CompanyName"].ToString();
            os.CompanyAddress = dr["CompanyAddress"].ToString();
            os.City = dr["City"].ToString();
            os.State = dr["State"].ToString();
            os.Country = dr["Country"].ToString();
            //table 2
            dr = ds.Tables[1].Rows[0];
            os.CostCenterID = dr["CostCenterID"].ToString();
            os.HeadOffice = dr["HeadOffice"].ToString();
            //table 3
            DataTable dt = ds.Tables[2];
            os.DomainName = (dt.AsEnumerable().Where(p => p["key"].ToString() == "DomainName").Select(p => p["value"].ToString())).FirstOrDefault();
            os.SiteUrl = (dt.AsEnumerable().Where(p => p["key"].ToString() == "SiteUrl").Select(p => p["value"].ToString())).FirstOrDefault();
            os.WebSiteID = (dt.AsEnumerable().Where(p => p["key"].ToString() == "WebSiteID").Select(p => p["value"].ToString())).FirstOrDefault();
            os.RegEmailID = (dt.AsEnumerable().Where(p => p["key"].ToString() == "RegEmailID").Select(p => p["value"].ToString())).FirstOrDefault();
            os.SMTPServer = (dt.AsEnumerable().Where(p => p["key"].ToString() == "SMTPServer").Select(p => p["value"].ToString())).FirstOrDefault();
            os.EmailSSLPortNo = (dt.AsEnumerable().Where(p => p["key"].ToString() == "EmailSSLPortNo").Select(p => p["value"].ToString())).FirstOrDefault();
            os.PageSize = (dt.AsEnumerable().Where(p => p["key"].ToString() == "PageSize").Select(p => p["value"].ToString())).FirstOrDefault();
            os.GridImageHeight = (dt.AsEnumerable().Where(p => p["key"].ToString() == "GridImageHeight").Select(p => p["value"].ToString())).FirstOrDefault();
            os.DecimalPlaces = (dt.AsEnumerable().Where(p => p["key"].ToString() == "DecimalPlaces").Select(p => p["value"].ToString())).FirstOrDefault();
            os.DateFormat = (dt.AsEnumerable().Where(p => p["key"].ToString() == "DateFormat").Select(p => p["value"].ToString())).FirstOrDefault();
            os.DateDisplayFormat = (dt.AsEnumerable().Where(p => p["key"].ToString() == "DateDisplayFormat").Select(p => p["value"].ToString())).FirstOrDefault();
            os.ReportURL = (dt.AsEnumerable().Where(p => p["key"].ToString() == "ReportURL").Select(p => p["value"].ToString())).FirstOrDefault();
            os.PONUM = (dt.AsEnumerable().Where(p => p["key"].ToString() == "PONUM").Select(p => p["value"].ToString())).FirstOrDefault();
            os.PoFor = (dt.AsEnumerable().Where(p => p["key"].ToString() == "PoFor").Select(p => p["value"].ToString())).FirstOrDefault();
            os.POcurrancy = (dt.AsEnumerable().Where(p => p["key"].ToString() == "POcurrancy").Select(p => p["value"].ToString())).FirstOrDefault();
            os.POCompanyName = (dt.AsEnumerable().Where(p => p["key"].ToString() == "POCompanyName").Select(p => p["value"].ToString())).FirstOrDefault();
            os.POLevel1 = (dt.AsEnumerable().Where(p => p["key"].ToString() == "POLevel1").Select(p => p["value"].ToString())).FirstOrDefault();
            os.POLevel2 = (dt.AsEnumerable().Where(p => p["key"].ToString() == "POLevel2").Select(p => p["value"].ToString())).FirstOrDefault();
            os.PoPrintType = (dt.AsEnumerable().Where(p => p["key"].ToString() == "PoPrintType").Select(p => p["value"].ToString())).FirstOrDefault();
            os.CompanyNamePart1 = (dt.AsEnumerable().Where(p => p["key"].ToString() == "CompanyNamePart1").Select(p => p["value"].ToString())).FirstOrDefault();
            os.CompanyNamePart2 = (dt.AsEnumerable().Where(p => p["key"].ToString() == "CompanyNamePart2").Select(p => p["value"].ToString())).FirstOrDefault();
            os.SMSUserID = (dt.AsEnumerable().Where(p => p["key"].ToString() == "SMSUserID").Select(p => p["value"].ToString())).FirstOrDefault();
            os.SMSPassword = (dt.AsEnumerable().Where(p => p["key"].ToString() == "SMSPassword").Select(p => p["value"].ToString())).FirstOrDefault();
            os.clearssid = (dt.AsEnumerable().Where(p => p["key"].ToString() == "clearssid").Select(p => p["value"].ToString())).FirstOrDefault();
            os.ExcelConnString = (dt.AsEnumerable().Where(p => p["key"].ToString() == "ExcelConnString").Select(p => p["value"].ToString())).FirstOrDefault();
            Global.clAppSettings.Add(os);
            Session["SMTPServer"] = os.SMTPServer;
            Session["DomainName"] = os.DomainName;
        }
        private static void SetModules(out string err) {
            try {
                string erpCS = ConfigurationManager.ConnectionStrings["ERPConnectionString"].ConnectionString;
                using (SqlConnection con = new SqlConnection(erpCS)) {
                    using (SqlDataReader rdr = SqlHelper.ExecuteReader(erpCS, CommandType.Text,
                        "SELECT MODULEID as mid, MODULENAME Title, FULLNAME Name, Redirect FROM T_G_ModuleMaster where status=1")) {
                        while (rdr.Read()) {
                            module mod = new module();
                            mod.mid = (int)rdr[0];
                            mod.Title = rdr[1].ToString();
                            mod.Name = rdr[2].ToString();
                            mod.Redirect = rdr[3].ToString();
                            Global.clModules.Add(mod);
                        }
                    }
                }
                err = "";
            } catch (Exception ex) { clsErrorLog.Log(ex); err = ex.Message; }
        }
        public PcTask gbPcTask { get; set; }
    }
    public class AppUser {
        public int cid { get; set; }//companyID
        public int uid { get; set; }//userID
        public int mid { get; set; }//moduleID
        public int ngs { get; set; }//last DbSsessionID/ngSession
        public string appSession { get; set; }//Aspx Session
        public AppUser(int c, int u, int m, int s, string ss) { cid = c; uid = u; mid = m; ngs = s; appSession = ss; }
    }
    public class AppMenu {
        public int ModID { get; set; }
        public string Title { get; set; }
        public int MenuID { get; set; }
        public string MenuName { get; set; }
        public string MenuGroup { get; set; }
        public string URL { get; set; }
        public int Under { get; set; }
        public int PreOrder { get; set; }
        public int Level { get; set; }
        public string Help { get; set; }
        public AppMenu(int modId, string title, int menuId, string menuName, string menuGp, string url, int under,  int preOrder, int level, string help) {
             ModID = modId; Title = title; MenuID = menuId; MenuName = menuName; MenuGroup = menuGp; URL = url; Under = under;  PreOrder = preOrder; Level = level; Help = help;
        }
    }
    public class RoleMenu {
        public int MenuID { get; set; }
        public int ModuleID { get; set; }
        public int RoleID { get; set; }
        public int Allowed { get; set; }
        public int Editable { get; set; }
        public int ViewAll { get; set; }
        public RoleMenu( int menuID, int moduleID, int roleID, int allowed, int editable, int viewAll) {
            MenuID = menuID; ModuleID = moduleID; RoleID = roleID; Allowed = allowed; Editable = editable; ViewAll = viewAll;
        }
    }
    public class MenuPathHref {
        public int MenuID { get; set; }
        public string Title { get; set; }
        public string Href { get; set; }
        public MenuPathHref(int menuID, string path, string href) {
            MenuID = menuID; Title = path; Href = href;
        }
    }
    public class PageAccess {
        public int MenuID { get; set; }
        public int ModuleID { get; set; }
        public int RoleID { get; set; }
        public bool Allowed { get; set; }
        public bool Editable { get; set; }
        public bool ViewAll { get; set; }
        public int Under { get; set; }
        public string MenuName { get; set; }
        public string Title { get; set; }
        public string Href { get; set; }
        public PageAccess(int menuID, int moduleID, int roleID, bool allowed, bool editable, bool viewAll, int under, string menuName, string title, string href) {
            MenuID = menuID; ModuleID = moduleID; RoleID = roleID; Allowed = allowed; Editable = editable; ViewAll = viewAll;
            Under = under; MenuName = menuName; Title = title; Href = href;
        }
    }
    public class PcTask
    {
        //TaskID=1758&TQty=18&SubActivityID=0&STID=24208&SAQTY
        public int TaskID { get; set; }
        public double TQty { get; set; }
        public int SubActivityID { get; set; }
        public int STID { get; set; }
        public double SAQTY { get; set; }
        public PcTask(int tid, double tqty, int saID, int stID, double saQty) {
            TaskID= tid; TQty = tqty; SubActivityID = saID; STID = stID; SAQTY = saQty;
        }
    }
}