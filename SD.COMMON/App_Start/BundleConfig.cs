﻿using System.Web.Optimization;
namespace SD.COMMON {
    public class BundleConfig {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkID=303951
        public static void RegisterBundles(BundleCollection bundles) {
            //bundles.Add(new StyleBundle("~/bundles/ProfileCSS").Include("~/App_Themes/Profile1/", new CssRewriteUrlTransform()));//https://forum.aspnetboilerplate.com/viewtopic.php?p=1962
            bundles.Add(new StyleBundle("~/bundles/CSS").IncludeDirectory("~/App_Themes/Profile1/", "*css", true));
            bundles.Add(new ScriptBundle("~/bundles/JS").IncludeDirectory("~/App_Themes/Profile1/JS/", "*js", true));
            BundleTable.EnableOptimizations = true;
            
        }
    }
}
