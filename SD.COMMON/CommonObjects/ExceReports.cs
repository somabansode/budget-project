﻿using SD.COMMON.DAL;
using System;
//using DataAccessLayer;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for ExceReports
/// </summary>
public class ExceReports {
    public ExceReports() {
        //
        // TODO: Add constructor logic here
        //
    }
    //public static DataSet ExceRptAttendance(DateTime? Date, int? Month, int? Year, int? SiteID, int Hours, int? EmpID, int? DeptID, int WHSType)
    //{
    //    try
    //    {
    //        DataSet ds = new DataSet();
    //        ds = SQLDBUtil.ExecuteDataset("HR_ExceRptAttendance", new SqlParameter[] { new SqlParameter("@Date", Date),
    //                                                                                   new SqlParameter("@Month", Month),
    //                                                                                   new SqlParameter("@Year",Year ),
    //                                                                                   new SqlParameter("@SiteID", SiteID),
    //                                                                                   new SqlParameter("@Hours",Hours ),
    //                                                                                   new SqlParameter("@EmpID",EmpID ) ,
    //                                                                                   new SqlParameter("@DeptID",DeptID ),
    //                                                                                   new SqlParameter("@WHSType",WHSType )
    //                                                                                 });
    //        return ds;
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //}
    public static DataSet ExceRptAttendanceByEmpID(DateTime? Date, int? Month, int? Year, int? SiteID, int Hours, int? EmpID, int WHSType, int CompanyID) {
        try {
            //DataSet ds = new DataSet();
            return SQLDBUtil.ExecuteDataset("HR_ExceRptAttendanceByEmpID", new SqlParameter[] { new SqlParameter("@Date", Date),
                                                                                       new SqlParameter("@Month", Month),
                                                                                       new SqlParameter("@Year",Year ),
                                                                                       new SqlParameter("@SiteID", SiteID),
                                                                                       new SqlParameter("@Hours",Hours ),
                                                                                       new SqlParameter("@EmpID",EmpID ) ,
                                                                                       new SqlParameter("@WHSType",WHSType ),
                                                                                       new SqlParameter("@CompanyID",CompanyID)
                                                                                     });
        } catch (Exception ex) {
            throw ex;
        }
    }
    public static DataSet ExceRptLeaves(DateTime? Date, int? Month, int? Year, int? SiteID, int? EmpID, int? DeptID) {
        try {
            //DataSet ds = new DataSet();
            return SQLDBUtil.ExecuteDataset("HR_ExceRptLeaves", new SqlParameter[] { new SqlParameter("@Date", Date),
                                                                                       new SqlParameter("@Month", Month),
                                                                                       new SqlParameter("@Year",Year ),
                                                                                       new SqlParameter("@SiteID", SiteID),
                                                                                       new SqlParameter("@EmpID",EmpID ) ,
                                                                                       new SqlParameter("@DeptID",DeptID ),
                                                                                     });
        } catch (Exception ex) {
            throw ex;
        }
    }
    public static DataSet ExceRptLeavesByEmpID(DateTime? Date, int? Month, int? Year, int? SiteID, int? EmpID) {
        try {
            //DataSet ds = new DataSet();
            return SQLDBUtil.ExecuteDataset("HR_ExceRptLeavesByEmpID", new SqlParameter[] { new SqlParameter("@Date", Date),
                                                                                       new SqlParameter("@Month", Month),
                                                                                       new SqlParameter("@Year",Year ),
                                                                                       new SqlParameter("@SiteID", SiteID),
                                                                                       new SqlParameter("@EmpID",EmpID )
                                                                                     });
        } catch (Exception ex) {
            throw ex;
        }
    }
    public static DataSet ExceRptInOutTimesByEmpID(int? Month, int? Year, int? EmpID, string EmpName) {
        try {
            //DataSet ds = new DataSet();
            return SQLDBUtil.ExecuteDataset("HR_ExceRptInOutTimesByEmpID", new SqlParameter[] { new SqlParameter("@Month", Month),
                                                                                              new SqlParameter("@Year",Year ),
                                                                                              new SqlParameter("@EmpName",EmpName ),
                                                                                              new SqlParameter("@EmpID",EmpID )
                                                                                     });
        } catch (Exception ex) {
            throw ex;
        }
    }
}