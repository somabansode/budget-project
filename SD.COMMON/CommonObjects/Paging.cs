
/// <summary>
/// Summary description for Paging
/// </summary>
public class Paging_Objects
{
    public Paging_Objects()
    {

    }
    private int _PageSize;
    private int _CurrentPage;
    private int _NoofRecords;
    private int _TotalPages;

    public int TotalPages
    {
        get { return _TotalPages; }
        set { _TotalPages = value; }
    }

    public int NoofRecords
    {
        get { return _NoofRecords; }
        set { _NoofRecords = value; }
    }

    public int CurrentPage
    {
        get { return _CurrentPage; }
        set { _CurrentPage = value; }
    }

    public int PageSize
    {
        get { return _PageSize; }
        set { _PageSize = value; }
    }
}
