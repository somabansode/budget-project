using System;
using System.Web.UI;
public class AlertMsg {
    public AlertMsg() {
    }
    #region "MsgBox"
    public enum MessageType { Success, Error, Info, Warning };
    public static void MsgBox(object curPage, string Message, MessageType type) {
        string URL = "";
        Page myPage = (Page)curPage;
        ScriptManager.RegisterStartupScript(myPage, myPage.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');" + URL, true);
    }
    public static void MsgBox(object curPage, string msg) {
        Page myPage = (Page)curPage;
        string js = "ShowMessage('" + msg + "','" + MessageType.Success + "');";
        ScriptManager.RegisterStartupScript(myPage, myPage.GetType(), Guid.NewGuid().ToString(), js, true);
    }
    public static void MsgBoxDown(object curPage, string msg) {
        Page myPage = (Page)curPage;
        string js = "ShowMessageDown('" + msg + "','" + MessageType.Success + "');";
        ScriptManager.RegisterStartupScript(myPage, myPage.GetType(), Guid.NewGuid().ToString(), js, true);
    }
    public static void GnrlMsgBox(object curPage, string msg) {
        Page myPage = (Page)curPage;
        string js = "ShowMessage('" + msg + "','" + MessageType.Info + "');";
        ScriptManager.RegisterStartupScript(myPage, myPage.GetType(), Guid.NewGuid().ToString(), js, true);
    }
    public static void CallScriptFunction(object curPage, string func) {
        Page myPage = (Page)curPage;
        string js = "<script language='javascript' type='text/javascript'>" + func + "</script>";
        ScriptManager.RegisterStartupScript(myPage, myPage.GetType(), "func", js, false);
    }
    #endregion
}
