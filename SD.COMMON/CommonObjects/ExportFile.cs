﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;

/// <summary>
/// Summary description for ExportToFile
/// </summary>
public class ExportFileUtil
{
    public ExportFileUtil()
	{
		//
		// TODO: Add constructor logic here
		//
    }

    #region SQLDataReader

    public static void ExportToFile(SqlDataReader sqlDataReader, String ItemColor, String AltItemColor, String FileName, String ContentType)
    {
        ExportToFile(sqlDataReader, ItemColor, AltItemColor, "", FileName, ContentType);
    }

    public static void ExportToFile(SqlDataReader sqlDataReader, String ItemColor, String FileName, String ContentType)
    {
        ExportToFile( sqlDataReader, ItemColor, "", "", FileName, ContentType);
    }

    public static void ExportToFile(SqlDataReader sqlDataReader, String FileName, String ContentType)
    {
        ExportToFile( sqlDataReader, "", "", "", FileName, ContentType);
    }

    public static void ExportToExcel(SqlDataReader sqlDataReader, String ItemColor, String AltItemColor, String FileName)
    {
        ExportToFile( sqlDataReader, ItemColor, AltItemColor, "", FileName + ".xls", "application/vnd.xls");
    }

    public static void ExportToExcel(SqlDataReader sqlDataReader, String ItemColor, String AltItemColor,String CrossItemColor, String FileName)
    {
        ExportToFile(sqlDataReader, ItemColor, AltItemColor, CrossItemColor, FileName + ".xls", "application/vnd.xls");
    }
    public static void ExportToExcel(SqlDataReader sqlDataReader, String ItemColor, String FileName)
    {
        ExportToFile( sqlDataReader, ItemColor, "", "", FileName + ".xls", "application/vnd.xls");
    }

    public static void ExportToExcel(SqlDataReader sqlDataReader, String FileName)
    {
        ExportToFile( sqlDataReader, "", "", "", FileName + ".xls", "application/vnd.xls");
    }

    public static void ExportToPDF(SqlDataReader sqlDataReader, String ItemColor, String AltItemColor, String FileName)
    {
        ExportToFile( sqlDataReader, ItemColor, AltItemColor, "", FileName + ".pdf", "application/pdf");
    }

    public static void ExportToPDF(SqlDataReader sqlDataReader, String ItemColor, String FileName)
    {
        ExportToFile( sqlDataReader, ItemColor, "", "", FileName + ".pdf", "application/pdf");
    }

    public static void ExportToPDF(SqlDataReader sqlDataReader, String FileName)
    {
        ExportToFile( sqlDataReader, "", "", "", FileName + ".pdf", "application/pdf");
    }

    public static void ExportToWord(SqlDataReader sqlDataReader, String ItemColor, String AltItemColor, String FileName)
    {
        ExportToFile( sqlDataReader, ItemColor, AltItemColor, "", FileName + ".doc", "application/ms-word");
    }

    public static void ExportToWord(SqlDataReader sqlDataReader, String ItemColor, String FileName)
    {
        ExportToFile( sqlDataReader, ItemColor, "", "", FileName + ".doc", "application/ms-word");
    }

    public static void ExportToWord(SqlDataReader sqlDataReader, String FileName)
    {
        ExportToFile( sqlDataReader, "", "", "", FileName + ".doc", "application/ms-word");
    }


    public static void ExportToFile(SqlDataReader sqlDataReader, String ItemColor, String AltItemColor, String CrossItemColor, String FileName, String ContentType)
    {

        //Clear the response
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + FileName);
        HttpContext.Current.Response.Charset = "";
        HttpContext.Current.Response.ContentType = ContentType;
        HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8;
        //You use these variables throughout the application.
        //Initialize the string that is used to build the file.
        HttpContext.Current.Response.Write("<table>");
        //Enumerate the field names and the records that are used to build 
        //the file.
        HttpContext.Current.Response.Write("<tr style='color:White; background-color:Navy; font-weight:bold;'>");
        for (int i = 0; i <= sqlDataReader.FieldCount - 1; i++)
        {

            HttpContext.Current.Response.Write("<td>" + sqlDataReader.GetName(i).ToString() + "</td>");

        }
        HttpContext.Current.Response.Write("</tr>");

        if (ItemColor == "")
            ItemColor = "#ffffff";
        if (AltItemColor == "")
            AltItemColor = "#ffffff";

        //Enumerate the database that is used to populate the file.
        int cnt = 0;
        while (sqlDataReader.Read())
        {

            HttpContext.Current.Response.Write("<tr>");


            for (int i = 0; i <= sqlDataReader.FieldCount - 1; i++)
            {
                if (cnt % 2 == 0)
                {
                    if (i % 2 == 0)
                        HttpContext.Current.Response.Write("<td style='background-color:" + ItemColor + ";'>" + sqlDataReader.GetValue(i).ToString() + "</td>");
                    else
                        HttpContext.Current.Response.Write("<td  style='background-color:" + AltItemColor + ";'>" + sqlDataReader.GetValue(i).ToString() + "</td>");
                }
                else
                {
                    if (i % 2 == 0)
                    {
                        
                        HttpContext.Current.Response.Write("<td  style='background-color:" + (CrossItemColor == "" ? ItemColor : AltItemColor) + ";'>" + sqlDataReader.GetValue(i).ToString() + "</td>");
                    }
                    else
                    {
                        HttpContext.Current.Response.Write("<td   style='background-color:" + (CrossItemColor == "" ? AltItemColor : CrossItemColor) + ";'>" + sqlDataReader.GetValue(i).ToString() + "</td>");
                    }
                }
            }
            HttpContext.Current.Response.Write("</tr>");
            cnt++;
        }

        HttpContext.Current.Response.Write("</table>");

        //Clean up.
        sqlDataReader.Close();

        HttpContext.Current.Response.End();

    }
    #endregion SQLDataReader


    #region DataTable

    public static void ExportToFile(DataTable dataTable, String ItemColor, String AltItemColor, String FileName, String ContentType)
    {
        ExportToFile(dataTable, ItemColor, AltItemColor, "", FileName, ContentType);
    }

    public static void ExportToFile(DataTable dataTable, String ItemColor, String FileName, String ContentType)
    {
        ExportToFile(dataTable, ItemColor, "", "", FileName, ContentType);
    }

    public static void ExportToFile(DataTable dataTable, String FileName, String ContentType)
    {
        ExportToFile(dataTable, "", "", "", FileName, ContentType);
    }

    public static void ExportToExcel(DataTable dataTable, String ItemColor, String AltItemColor, String FileName)
    {
        ExportToFile(dataTable, ItemColor, AltItemColor, "", FileName + ".xls", "application/vnd.xls");
    }

    public static void ExportToExcel(DataTable dataTable, String ItemColor, String AltItemColor, String CrossItemColor, String FileName)
    {
        ExportToFile(dataTable, ItemColor, AltItemColor, CrossItemColor, FileName + ".xls", "application/vnd.xls");
    }

    public static void ExportToExcel(DataTable dataTable, String ItemColor, String FileName)
    {
        ExportToFile(dataTable, ItemColor, "", "", FileName + ".xls", "application/vnd.xls");
    }

    public static void ExportToExcel(DataTable dataTable, String FileName)
    {
        ExportToFile(dataTable, "", "", "", FileName + ".xls", "application/vnd.xls");
    }

    public static void ExportToPDF(DataTable dataTable, String ItemColor, String AltItemColor, String FileName)
    {
        ExportToFile(dataTable, ItemColor, AltItemColor, "", FileName + ".pdf", "application/pdf");
    }

    public static void ExportToPDF(DataTable dataTable, String ItemColor, String FileName)
    {
        ExportToFile(dataTable, ItemColor, "", "", FileName + ".pdf", "application/pdf");
    }

    public static void ExportToPDF(DataTable dataTable, String FileName)
    {
        ExportToFile(dataTable, "", "", "", FileName + ".pdf", "application/pdf");
    }

    public static void ExportToWord(DataTable dataTable, String ItemColor, String AltItemColor, String FileName)
    {
        ExportToFile(dataTable, ItemColor, AltItemColor, "", FileName + ".doc", "application/ms-word");
    }

    public static void ExportToWord(DataTable dataTable, String ItemColor, String FileName)
    {
        ExportToFile(dataTable, ItemColor, "", "", FileName + ".doc", "application/ms-word");
    }

    public static void ExportToWord(DataTable dataTable, String FileName)
    {
        ExportToFile(dataTable, "", "", "", FileName + ".doc", "application/ms-word");
    }


    public static void ExportToFile(DataTable dataTable, String ItemColor, String AltItemColor, String CrossItemColor, String FileName, String ContentType)
    {

        //Clear the response
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + FileName);
        HttpContext.Current.Response.Charset = "";
        HttpContext.Current.Response.ContentType = ContentType;
        HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8;

        //You use these variables throughout the application.
        //Initialize the string that is used to build the file.
        HttpContext.Current.Response.Write("<table>");
        //Enumerate the field names and the records that are used to build 
        //the file.
        HttpContext.Current.Response.Write("<tr style='color:White; background-color:Navy; font-weight:bold;'>");

        foreach (DataColumn dataColumn in dataTable.Columns)
        {
            HttpContext.Current.Response.Write("<td>" + dataColumn.ColumnName + "</td>");
            
        }
       
        HttpContext.Current.Response.Write("</tr>");

        if (ItemColor == "")
            ItemColor = "#ffffff";
        if (AltItemColor == "")
            AltItemColor = "#ffffff";

        //Enumerate the database that is used to populate the file.
        int cnt = 0;
        foreach(DataRow dataRow in dataTable.Rows)
        {

            HttpContext.Current.Response.Write("<tr>");

           
            for (int i = 0; i <= dataTable.Columns.Count - 1; i++)
            {
                if (cnt % 2 == 0)
                {
                    if (i % 2 == 0)
                        HttpContext.Current.Response.Write("<td style='background-color:" + ItemColor + ";'>" + dataRow[dataTable.Columns[i]].ToString() + "</td>");
                    else
                        HttpContext.Current.Response.Write("<td  style='background-color:" + AltItemColor + ";'>" + dataRow[dataTable.Columns[i]].ToString() + "</td>");
                }
                else
                {
                    if (i % 2 == 0)
                    {

                        HttpContext.Current.Response.Write("<td  style='background-color:" + (CrossItemColor == "" ? ItemColor : AltItemColor) + ";'>" + dataRow[dataTable.Columns[i]].ToString() + "</td>");
                    }
                    else
                    {
                        HttpContext.Current.Response.Write("<td   style='background-color:" + (CrossItemColor == "" ? AltItemColor : CrossItemColor) + ";'>" + dataRow[dataTable.Columns[i]].ToString() + "</td>");
                    }
                }
            }
            HttpContext.Current.Response.Write("</tr>");
            cnt++;
        }

        HttpContext.Current.Response.Write("</table>");

        //Clean up.

        HttpContext.Current.Response.End();

    }
    #endregion DataTable


}