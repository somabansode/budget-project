using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;

public enum AspectRatio { Width, Height, None }

/// <summary>
/// Summary description for PicUtil
/// </summary>
public class PicUtil
{
	public PicUtil()
	{
		//
		// TODO: Add constructor logic here
		//
        //ProjectDataSet.ResourcesRow drRes = AECSSRMDI.dsProject.Resources.FindByResId(ResId);
        ////Initialize Byte Array
        //Byte[] content = null;
        //OpenFileDialog ofd = new OpenFileDialog();
        //ofd.Filter = "JPEG Interchange Format|*.jpg|GIF files(*.gif)|*.gif";

        //if (ofd.ShowDialog() == DialogResult.OK)
        //{
        //    content = ConvertPic(ofd.FileName);

        //}
	}


    public static void ConvertPic(string SourceFilename, int Width, int Height, String SaveAs, AspectRatio AR)
    {
        try
        {
            Bitmap loBMP = new Bitmap(SourceFilename);
            switch (AR)
            {

                case AspectRatio.Width:
                    Height = Convert.ToInt32(Math.Round(((double)loBMP.Height / (double)loBMP.Width) * Width, 0));
                    break;
                case AspectRatio.Height:
                    Width = Convert.ToInt32(Math.Round(((double)loBMP.Width / (double)loBMP.Height) * Height, 0));
                    break;
            }
            loBMP.Dispose();
            loBMP = null;
            ConvertPic(SourceFilename, Width, Height, SaveAs);

        }
        catch { }

    }
    public static void ConvertPic(string SourceFilename,int Width,int Height,String SaveAs)
        {
            try
            {
                System.Drawing.Image imgOut = null;
                Bitmap loBMP = new Bitmap(SourceFilename);
 
                //DirectoryInfo di = new DirectoryInfo(Path.GetTempPath() + "\\Res");
                //if (di.Exists == false)
                //{
                //    di.Create();
                //}
 
                ////Getting ThumbnailImage
                //String strsFn = Path.GetTempPath() + "\\Res\\res" + DateTime.Now.ToFileTime() + ".jpg";
 
 
                System.Drawing.Imaging.ImageFormat loFormat = loBMP.RawFormat;
                if (loBMP.Width <= Width && loBMP.Height <= Height)
                {
                    //loBMP.Save(strsFn);
                    imgOut = loBMP.GetThumbnailImage(loBMP.Width, loBMP.Height, null, IntPtr.Zero);
                }
                else
                {
                    imgOut = loBMP.GetThumbnailImage(Width, Height, null, IntPtr.Zero);
                }
 
                System.Drawing.Image imgPhoto = imgOut;
               
                Bitmap bmPhoto = new Bitmap(Width, Height, PixelFormat.Format16bppRgb565);
                bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);
                Graphics grPhoto = Graphics.FromImage(bmPhoto);
 
                System.Drawing.Image imgone = imgOut;
                
                grPhoto.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighSpeed;
                grPhoto.DrawImage(imgPhoto, new Rectangle(0, 0, Width, Height), 0, 0, Width, Height, GraphicsUnit.Pixel);
 
                SizeF crSize = new SizeF();
                int yPixlesFromBottom = (int)(Height * 0.5);
                float yPosFromBottom = ((Height - yPixlesFromBottom) - (crSize.Height / 2));
                float xCenterOfImg = (Width / 2);
                StringFormat StrFormat = new StringFormat();
                StrFormat.Alignment = StringAlignment.Center;
                Bitmap bmimg = new Bitmap(bmPhoto);
                bmimg.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);
                Graphics grimg = Graphics.FromImage(bmimg);
                ImageAttributes imageAttributes = new ImageAttributes();
                ColorMap colorMap = new ColorMap();
                colorMap.OldColor = Color.FromArgb(255, 0, 255, 0);
                colorMap.NewColor = Color.FromArgb(0, 0, 0, 0);
                ColorMap[] remapTable = { colorMap };
                imageAttributes.SetRemapTable(remapTable, ColorAdjustType.Bitmap);
                float[][] colorMatrixElements = {
 
                                                new float[] {0.0f,  0.0f,  0.0f,  0.0f, 0.0f},      
                                                new float[] {0.0f,  0.0f,  0.0f,  0.0f, 0.0f},       
                                                new float[] {0.0f,  0.0f,  0.0f,  0.0f, 0.0f},       
                                                new float[] {0.0f,  0.0f,  0.0f,  0.0f, 0.0f},       
                                                new float[] {0.0f,  0.0f,  0.0f,  0.0f, 0.0f}
                                            };
 
                ColorMatrix wmColorMatrix = new ColorMatrix(colorMatrixElements);
                imageAttributes.SetColorMatrix(wmColorMatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
                int xPosOfWm = ((Width - Width));
                int yPosOfWm = 0;
                grimg.DrawImage(imgone, new Rectangle(xPosOfWm, yPosOfWm, Width, Height), 0, 0, Width, Height, GraphicsUnit.Pixel, imageAttributes);
                imgPhoto = bmimg;
                grPhoto.Dispose();
                grimg.Dispose();
                loBMP.Dispose();
                FileInfo fi = new FileInfo(SaveAs);
                if (fi.Exists)
                    fi.Delete();
                imgPhoto.Save(SaveAs, ImageFormat.Jpeg);
                //pbResource.Image = Image.FromFile(strsFn);
                imgPhoto.Dispose();
                imgone.Dispose();
                loBMP.Dispose();
                loBMP = null;
 
 
                //try
                //{
                //    //content = ReadFileToByteArray(strsFn);
                //    //MemoryStream stream = new MemoryStream(content);
                //    //Bitmap image = new Bitmap(stream);                 
 
                //}
                //catch (Exception ex)
                //{
                //    throw ex;
                //}
                //curDeletableRes =Convert.ToInt32(sResId);
                //FileInfo fi = new FileInfo(ProBIDMDI.strImgResPath + sResId + ".jpg");
                //try
                //{
                //    if (fi.Exists)
                //        fi.Delete();
                //}
                //catch { }
            }
            catch (Exception ex)
            {
                throw ex;
            }
          
 
        }
 
        protected static byte[] ReadFileToByteArray(string fileName)
        {
            //try
            //{
                FileStream fileStream = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Read);
                long len;
                len = fileStream.Length;
                Byte[] fileAsByte = new Byte[len];
                fileStream.Read(fileAsByte, 0, fileAsByte.Length);
                MemoryStream memoryStream = new MemoryStream(fileAsByte);
                byte[] content = memoryStream.ToArray();
                fileStream.Dispose();
                return content;
            //}
            //catch
            //{
               
              
            //}
        }
}

