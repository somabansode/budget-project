﻿using SD.COMMON.DAL;
using System;
using System.Collections;
//using DataAccessLayer;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
/// <summary>
/// Summary description for WebFormMaster
/// </summary>
public class WebFormMaster : System.Web.UI.Page {
    private String _BreadCrumb;
    public static Hashtable _pageArraylist;
    public String BreadCrumb {
        get { return _BreadCrumb; }
        set { _BreadCrumb = value; }
    }
    void Page_PreInit(Object sender, EventArgs e) {
        if (Session["UserId"] != null) {
            Session["CurrentPage"] = Request.Url.Segments[Request.Url.Segments.Length - 1] + Request.Url.Query;
        } else {
            Response.Redirect("AdminDefault.aspx");
        }
    }
    public static void WriteToErrorLog(string Message, Exception ex) {
        //throw new NotImplementedException();
        if (ex != null) {
            string LogFile = System.Web.HttpContext.Current.Server.MapPath("~\\Logs\\ErrorLog.txt");
            System.IO.StreamWriter fs = new System.IO.StreamWriter(LogFile, true);
            fs.WriteLine("------------" + DateTime.Now.ToString() + "------------");
            fs.WriteLine(Message);
            fs.WriteLine(ex.ToString());
            fs.AutoFlush = true;
            fs.Close();
        }
    }
    public void Expand(TreeNode Selected) {
        if (Selected != null) {
            Selected.Expand();
            if (Selected.Parent != null) {
                Expand(Selected.Parent);
            }
        }
    }
    public static void BindFavorites(int EmpID) {
        _pageArraylist = new Hashtable();
        if (Common.HomePageUrls(EmpID).Tables.Count > 1 && Common.HomePageUrls(EmpID).Tables[2].Rows.Count > 0) {
            foreach (DataRow dr in Common.HomePageUrls(EmpID).Tables[2].Rows) {
                if (!_pageArraylist.Contains(dr["URL"].ToString()))
                    _pageArraylist.Add(dr["URL"].ToString(), dr["FavID"].ToString());
            }
        }
    }
    public static DataSet BindFavGroups(int EmpID) {
        try {
            SqlParameter[] objParam = new SqlParameter[1];
            objParam[0] = new SqlParameter("@EmpID", EmpID);
            return SQLDBUtil.ExecuteDataset("GEN_BindFavGrps", objParam);
        } catch (Exception ex) {
            throw ex;
        }
    }
}