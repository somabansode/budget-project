﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.XPath;
using System.IO;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for CodeUtil
/// </summary>
public class CodeUtil
{
    public CodeUtil()
    {
        //
        // TODO: Add constructor logic here
        //
    }


    public const int Cost_Center = 1;

    public const int Allow_Zero_Entries = 2;

    public const int Ask_Contact_for_ledger = 4;

    public const int Use_Payment_Receipt_As_Contra = 5;

    public const int Allow_Cash_in_Journals = 6;

    public const int Warn_on_NegativeCashBalance = 7;

    public const int Cost_Center_for_Ledger = 8;

    public const string Contra = "0000";

    public const string Journal = "0001";

    public const string Payment = "0002";

    public const string Receipt = "0003";

    public const string Reversal = "0004";

    public const string PettyAdvances = "0005";
    public const string DebitNote = "0006";
    public const string CreditNote = "0007";
    public const string Sales = "0008";
    public const string Purchase = "0009";
    public const string Save_Voucher = "Voucher Saved Successfully";
    public const string Update_Voucher = "Voucher Updated Successfully";
    // public CodeUtil.EntityTypes EntityType { get; set; }
    public enum DateFormat
    {
        DayMonthYear = 0,
        MonthDayYear = 1,
        YearMonthDay = 6,
        DayMonthYearHourMinMeridiem = 3,
        MonthDayYearHourMinMeridiem = 4,
        YearMonthDayHourMinMeridiem = 5,
        ddMMMyyyy = 2
    }

    public static void CallScriptFunction(object curPage, string msg)
    {
        Page myPage = (Page)curPage;
        ScriptManager.RegisterStartupScript(myPage, myPage.GetType(), Guid.NewGuid().ToString(), msg, true);
    }


    public static double EvaluateArithmethicExpression(String expression)
    {
        XPathDocument xpdoc;
        StringReader sr = new StringReader("<r/>");
        // doesn't matter whats in the doc
        // we just need something to call evaluate on
        xpdoc = new XPathDocument(sr);
        sr.Close();

        // give me a null and you will get on back
        //if (expression.IsNull)
        //{
        //    return SqlDouble.Null;
        //}
        // use regex's to convert typical arithmetic ops to the ones XPath likes
        // also force the result to be a double
        String useExpression = "number(" + Regex.Replace(expression, "/", " div ") + ")";
        useExpression = Regex.Replace(useExpression, "%", " mod ");
        XPathNavigator nav = xpdoc.CreateNavigator();
        double retval = 0;
        try
        {
            retval = (double)nav.Evaluate(useExpression);
        }
        catch
        {
            // if its not a plain ol' arithmetic expression
            // you will get a null back
            retval = 0;
        }
        return retval;
    }

    public static void CallScriptMethod(Page Source, string Method)
    {
        string strScript = "<script language='javascript'>" + Method + ";</script>";
        ScriptManager.RegisterStartupScript(Source, Source.GetType(), "PopupCP", strScript, false);
    }




    public static DateTime ConverttoDate(string StrDate, DateFormat Format)
    {
        DateTime dt = new DateTime();
        // modify by pratap date: 21-01-2016
        //StrDate = StrDate.Replace('-', '/');
        if (StrDate != "")
        {
            if (StrDate.Split(' ').Count() > 1)
            {
                //StrDate = StrDate.Split(' ')[0];
                //Format = DateFormat.DayMonthYear;
                switch (Format)
                {
                    case DateFormat.ddMMMyyyy:
                        dt = new DateTime(Convert.ToInt32(StrDate.Split(' ')[2]), getMonth(StrDate.Split(' ')[1]), Convert.ToInt32(StrDate.Split(' ')[0]));
                        break;
                    case DateFormat.DayMonthYear:
                        dt = new DateTime(Convert.ToInt32(StrDate.Split('/')[2]), Convert.ToInt32(StrDate.Split('/')[1]), Convert.ToInt32(StrDate.Split('/')[0]));
                        break;
                    case DateFormat.MonthDayYear:
                        dt = new DateTime(Convert.ToInt32(StrDate.Split('/')[2]), Convert.ToInt32(StrDate.Split('/')[0]), Convert.ToInt32(StrDate.Split('/')[1]));
                        break;

                    default:
                        break;
                }
            }
        }
        return dt;
    }


    private static int getMonth(string sMonth)
    {
        int rVal = 0;
        switch (sMonth.ToUpper().Trim())
        {
            case "JAN":
                rVal = Convert.ToInt32(Month.JAN);
                break;
            case "FEB":
                rVal = Convert.ToInt32(Month.FEB);
                break;
            case "MAR":
                rVal = Convert.ToInt32(Month.MAR);
                break;
            case "APR":
                rVal = Convert.ToInt32(Month.APR);
                break;
            case "MAY":
                rVal = Convert.ToInt32(Month.MAY);
                break;
            case "JUN":
                rVal = Convert.ToInt32(Month.JUN);
                break;
            case "JUL":
                rVal = Convert.ToInt32(Month.JUL);
                break;
            case "AUG":
                rVal = Convert.ToInt32(Month.AUG);
                break;
            case "SEP":
                rVal = Convert.ToInt32(Month.SEP);
                break;
            case "OCT":
                rVal = Convert.ToInt32(Month.OCT);
                break;
            case "NOV":
                rVal = Convert.ToInt32(Month.NOV);
                break;
            case "DEC":
                rVal = Convert.ToInt32(Month.DEC);
                break;
        }
        return rVal;
    }

    public enum EmployeeRoles
    {
        OMs_Admin = 1,
        OMs_Technical = 2,
        OMs_General = 3,
        OMs_Supervisor = 4
    }

    public enum Month
    {
        JAN = 1,
        FEB = 2,
        MAR = 3,
        APR = 4,
        MAY = 5,
        JUN = 6,
        JUL = 7,
        AUG = 8,
        SEP = 9,
        OCT = 10,
        NOV = 11,
        DEC = 12
    }

    //public enum EntityTypes
    //{
    //    Segment = 1,
    //    Year,
    //    ProductGroup,
    //}

    #region "MsgBox"

    //public enum MessageType { Success, Error, Info, Warning };

    //public static void ShowMessage(object curPage, string Message, MessageType type, string URL)
    //{
    //    Page myPage = (Page)curPage;
    //    //ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
    //    ScriptManager.RegisterStartupScript(myPage, myPage.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');" + URL, true);
    //}

    //public static void MsgBox(object curPage, string msg)
    //{
    //    AlertMsg.MsgBox(curPage, msg);
    //    //Page myPage = (Page)curPage;
    //    ////msg = myPage.Server.HtmlEncode(msg);
    //    ////msg = msg.Replace("\r", "\\r");
    //    ////msg = msg.Replace("\t", "\\t");
    //    ////msg = msg.Replace("\n", "\\n");
    //    ////msg = msg.Replace("\a", "\\a");
    //    ////msg = msg.Replace("\b", "\\b");
    //    ////msg = msg.Replace("\f", "\\f");
    //    ////msg = msg.Replace("\v", "\\v");
    //    ////msg = msg.Replace("'", " - ");
    //    ////string js = "window.alert('" + msg + "');";
    //    //string js = "ShowMessage('" + msg + "','" + MessageType.Success + "');";
    //    //ScriptManager.RegisterStartupScript(myPage, myPage.GetType(), Guid.NewGuid().ToString(), js, true);
    //}

    public static void CallClientFunction(object curPage, string msg)
    {
        Page myPage = (Page)curPage;
        ScriptManager.RegisterStartupScript(myPage, myPage.GetType(), Guid.NewGuid().ToString(), msg, true);
    }
    #endregion

    public static string DecimalFormat(string Value, string Format)
    {
        string RVal = Value;
        try
        {
            RVal = Convert.ToDouble(Value).ToString(Format);
        }
        catch { }
        return RVal;
    }
}
