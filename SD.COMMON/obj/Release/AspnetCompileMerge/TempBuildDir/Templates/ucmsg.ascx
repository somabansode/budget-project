﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucmsg.ascx.cs" Inherits="SD.COMMON.Templates.ucmsg" %>
<style type="text/css">
.messagealert { width: 100%; position: fixed; top: 100px; z-index: 100000; padding: 0; font-size: 15px; }
.fixed { position: fixed; bottom: 0; right: 0; width: 150px; height:30px; margin-bottom :5px; margin-right :10px; border: 3px solid #73AD21; }
.alert-successd { color: #3c763d; background-color: #dff0d8; border-color: #d6e9c6; }
.alert-successd hr { border-top-color: #c9e2b3; }
.alert-successd .alert-link { color: #2b542c; }
.alert-infod { color: #31708f; background-color: #d9edf7; border-color: #bce8f1; }
.alert-infod hr { border-top-color: #a6e1ec; }
.alert-infod .alert-link { color: #245269; }
.alert-warningd { color: #8a6d3b; background-color: #fcf8e3; border-color: #faebcc; }
.alert-warningd hr { border-top-color: #f7e1b5; }
.alert-warning .alert-link { color: #66512c; }
.alert-dangerd { color: #a94442; background-color: #f2dede; border-color: #ebccd1; }
.alert-dangerd hr { border-top-color: #e4b9c0; }
.alert-danger .alert-link { color: #843534; }
</style>
<script type="text/javascript">
    function ShowMessage(message, messagetype) {
        var cssclass;
        switch (messagetype) {
            case 'Success': cssclass = 'alert-success'; break;
            case 'Error': cssclass = 'alert-danger'; break;
            case 'Warning': cssclass = 'alert-warning'; break;
            default: cssclass = 'alert-info'
        }
        $('#alert_container').append('<div id="alert_div" style="margin: 0 0.5%; -webkit-box-shadow: 3px 4px 6px #999;" class="alert fade in ' + cssclass + '"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>' + messagetype + '!</strong> <span>' + message + '</span></div>');
        setTimeout(function () {
            jQuery("#alert_div").fadeTo(2000, 500).slideUp(500, function () {
                jQuery("#alert_div").remove();
            });
        }, 5000);//5000=5 seconds
    }
    function ShowMessageDown(message, messagetype) {
        var cssclass;
        switch (messagetype) {
            case 'Success': cssclass = 'alert-successd'; break;
            case 'Error': cssclass = 'alert-dangerd'; break;
            case 'Warning': cssclass = 'alert-warningd'; break;
            default: cssclass = 'alert-infod'
        }
        jQuery('#alert_container').append('<div id="alert_divd" class="fixed in '
            + cssclass + '"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <span>' + message + '</span> </div>');
        setTimeout(function () {
           jQuery("#alert_divd").fadeTo(2000, 500).slideUp(500, function () {
                jQuery("#alert_divd").remove();
            });
        }, 5000);//5000=5 seconds
    }
</script>
<div class="messagealert" id="alert_container" style="width: 430px; margin-left: 720px">
</div>
