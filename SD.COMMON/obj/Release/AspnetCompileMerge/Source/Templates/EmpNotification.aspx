﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmpNotification.aspx.cs" Inherits="SD.COMMON.EmpNotification" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../App_Themes/Profile1/StyleSheet.css" rel="stylesheet" />
    <link href="../App_Themes/Profile1/aec_main_styles.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:GridView ID="xdata" runat="server" AutoGenerateColumns="false" RowStyle-BackColor="#A1DCF2"
                    EmptyDataText="No Records Found" CssClass="gridview" Style="border-collapse: collapse; width: 350px; height: 350px; font-size: 11px">
                    <Columns>
                         <asp:BoundField HeaderText="Menu Path" DataField="MenuPath" />
                                                    <asp:BoundField HeaderText="Stage" DataField="Stage" />
                                                    <asp:BoundField HeaderText="Nos" DataField="Nos" />
                    </Columns>
                </asp:GridView>
  </div>
    </form>
</body>
</html>