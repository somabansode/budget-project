﻿>>> Existing scenario of traditional ASPX as under
	HMS Module and common master 
	Single signon in HMS module
	Step 1 >> Looks for CommonMaster >> public class WebFormMaster : System.Web.UI.Page
	Step 2 >> OnPreInit(EventArgs e)
	Step 3 >> Initializes >> base.OnPreInit(e); >> this is for Page class
	Step 4 >> Sets Page.Theme = "Profile1"; && Page.MasterPageFile = Request.ApplicationPath + "/Templates/CommonMaster.master";
	Step 5 >> Control goes to Content page >> Class ContentPage : SD.COMMON.WebFormMaster 
	Step 6 >> Takes ModuleID of that Content page and returns back to Master >> ModuleID = 1; base.OnInit(e);
>>> New process with Angular Project
	1. AngularApp Fires WebAPI LoginController >> HttpPost = GetLoginDetails with login credentials which have companyID, Login, Password
	2. In the above method, we are initializing dbSession with default moduleID = 1 and  do db.PerformSession();   
		a. dbSession class initializes a SessionID for transaction on two tables tg_LoginSession && tg_sessions
		b. A particular companyID + UserID + ModuleID >> can work only in one module at a time in one session
		c. table tg_sessions is Cleared all previous  SessionID = NULL 
		d. table tg_sessions is Updated of lid, cid, eid, mid, ss, dt, SessionID 
			from angular Login to DB by using Proc sc_angularSession2DB after checking licenses 
		e. CompleteSession by clearing session variable in table tg_LoginSession with last ngSessionID in tg_sessions table
	3. The control is partially with the DB from the above steps so far as LoginID, CompId and ModuleID
	4. Now, AngularApp can send for the first page 'AdminDefault.aspx' with credentials queryString  such as compaID, UserId + HMS module as Landing page
	5. As the login succeeds query string values are parsed to get a dataset from SP sg_appCurUsers 
		with few missing parameters to build list of current AppUsers maintained in Global.asax page as static list
	6. Since Aspx works in stateless app for every page we need to depend of only session variables and/or 
		static class of variables in Global asax page. In the current scenario both/mixed approach is adopted.
	7. Landing page adds record to Global.clAppCurUsers. Subsequent pages simply use this list to skip binding session variables
>>> new improvements added to Common master are as under 
	Existing sessionState is removed and following added on 01 Aug 2018 to implement webgardedn and webfarms
	<sessionState timeout="20" mode="SQLServer" 
	sqlConnectionString="Password=Jan26_2018;Persist Security Info=True;User ID=sa; Data Source=182.18.165.68;"/>
>>> Several round trips to SQL Server shouyld be removed now as next step