﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChatNotification.aspx.cs" Inherits="SD.COMMON.ChatNotification" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:GridView ID="gvChatNotification" runat="server" AutoGenerateColumns="false"
                                                EmptyDataText="No Records Found" CssClass="gridview" Style="border-collapse: collapse; width: 150px;  font-size: 11px">
                                                <Columns>
                                                    <asp:BoundField HeaderText="Name" DataField="Name" />
                                                    <asp:BoundField HeaderText="Count" DataField="Count" />
                                                </Columns>
                                            </asp:GridView>
    </div>
    </form>
</body>
</html>
