<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ucMenu.ascx.cs" Inherits="SD.COMMON.ucMenu1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<style type="text/css">
    .txtboxw {
        -moz-border-radius-topleft: 30px;
        -webkit-border-top-left-radius: 30px;
        -moz-border-radius-bottomleft: 30px;
        -webkit-border-bottom-left-radius: 30px;
        -moz-border-radius-topright: 30px;
        -webkit-border-top-right-radius: 30px;
        -moz-border-radius-bottomright: 30px;
        -webkit-border-bottom-right-radius: 30px;
    }
</style>
<table border="0" style="height: 600px; width: 110px;">
    <tr style="height: 50px;">
        <td id="sidenavigation">
            <script src="../App_Themes/Profile1/JS/Tooltip.js" type="text/javascript"></script>
            <script type="text/javascript">
                var t1 = null;
                addLoadEvent(function ToolTipInit() {
                    t1 = new ToolTip("dvToolTip", false);
                });
                function addLoadEvent(func) {
                    var oldonload = window.onload;
                    if (typeof window.onload != 'function') {
                        window.onload = func;
                    } else {
                        window.onload = function () {
                            if (oldonload) {
                                oldonload();
                            }
                            func();
                        }
                    }
                }
                function searchclick() {
                    document.getElementById('ucMenu1_SearchButton').click();
                }
            </script>
            <div class="sidebar-form">
                <div class="input-group">
                    <asp:TextBox ID="txtsearch" Text="" placeholder="Menu ID..." ForeColor="White" CssClass="form-control" AccessKey="q" Style="font-size=10px; width: 120px; height: 19px; padding: 0px; float: left;" runat="server"></asp:TextBox>
                    <button style="float: right; display: inline;" type="submit" id="btnSubmit" runat="server" onserverclick="filtersearchclick"><i class="fa fa-search"></i></button>
                </div>
            </div>
            <div style="text-align: left;">
            </div>
            <div class="navbar" id="navbar" runat="server" >
            </div>
            <div id="dvToolTip" class="selected" style="background-color: #fffff0; padding: 3px 3px 3px 3px; width: 110px; height: 50px; top: -60px; position: absolute; border: solid 1px gray; text-align: left;">
            </div>
            <script type="text/javascript" src="../App_Themes/Profile1/JS/xpmenuv21.js"></script>
        </td>
    </tr>
</table>
