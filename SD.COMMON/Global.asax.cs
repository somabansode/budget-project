﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Optimization;

namespace SD.COMMON {
    public class Global : HttpApplication 
    {
        public static List<AppSetting> clAppSettings = new List<AppSetting>();
        public static List<AppUser> clAppUsers = new List<AppUser>();
        public static List<module> clModules = new List<module>();
        public static List<AppMenu> clMenuAll = new List<AppMenu>();
        public static List<RoleMenu> clSessionRoleMenu = new List<RoleMenu>();
        public static List<MenuPathHref> clMenuPaths = new List<MenuPathHref>();

        protected void Application_Start(object sender, EventArgs e) {
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            //AuthConfig.RegisterOpenAuth();
            Application["usersOnline"] = 0;
            clModules = new List<module>();
            clAppUsers = new List<AppUser>();
            clMenuAll = new List<AppMenu>();
        }
        protected void Application_BeginRequest(object sender, EventArgs e) {

        }
        protected void Application_AuthenticateRequest(object sender, EventArgs e) {

        }
        protected void Application_Error(object sender, EventArgs e) {

        }
        protected void Session_Start(object sender, EventArgs e) {
            Application.Lock();
            clAppSettings = new List<AppSetting>();
            clSessionRoleMenu = new List<RoleMenu>();
            clMenuPaths = new List<MenuPathHref>();
            Application.UnLock();
        }
        protected void Session_End(object sender, EventArgs e) {
            Application.Lock();
            //Application["usersOnline"] = (int)Application["usersOnline"] - 1;
            try { 
                //if(clAppUsers.Count > 0) clAppUsers.Remove(clAppUsers.Where(x => x.appSession == Session.SessionID).FirstOrDefault());
                clAppSettings = new List<AppSetting>();
                clSessionRoleMenu = new List<RoleMenu>();
                clMenuPaths = new List<MenuPathHref>();
            } catch { Application.Lock(); throw; }
            Application.UnLock();
        }
        protected void Application_EndRequest(object sender, EventArgs e) {

        }
    }
    public class module {
        //mid Title   FULLNAME Redirect
        public int mid { get; set; }//1 for HMS === ModuleID
        public string Title { get; set; } //HMS for HMS === Session["ModulePrefix"]
        public string Name { get; set; } //FullName >>  Human Resource Management System
        public string Redirect { get; set; } // example ../HMS/HMS/AdminDefault.aspx
    }
    public class AppSetting {
        public string CompanyID { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string CostCenterID { get; set; }
        public string HeadOffice { get; set; }
        public string DomainName { get; set; }
        public string SiteUrl { get; set; }
        public string WebSiteID { get; set; }
        public string RegEmailID { get; set; }
        public string SMTPServer { get; set; }
        public string EmailSSLPortNo { get; set; }
        public string PageSize { get; set; }
        public string GridImageHeight { get; set; }
        public string DecimalPlaces { get; set; }
        public string DateFormat { get; set; }
        public string DateDisplayFormat { get; set; }
        public string ReportURL { get; set; }
        public string PONUM { get; set; }
        public string PoFor { get; set; }
        public string POcurrancy { get; set; }
        public string POCompanyName { get; set; }
        public string POLevel1 { get; set; }
        public string POLevel2 { get; set; }
        public string PoPrintType { get; set; }
        public string CompanyNamePart1 { get; set; }
        public string CompanyNamePart2 { get; set; }
        public string SMSUserID { get; set; }
        public string SMSPassword { get; set; }
        public string clearssid { get; set; }
        public string ExcelConnString { get; set; }
    }
}