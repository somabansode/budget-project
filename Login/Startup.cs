﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Login.Startup))]

namespace Login {
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            ConfigureAuth(app);
        }
    }
}
