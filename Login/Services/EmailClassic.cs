﻿using Microsoft.AspNet.Identity;
using SD.COMMON.DAL;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Threading.Tasks;
namespace Login.Services
{
    public class EMail : IIdentityMessageService
    {
        #region Private Fields
        public static int cid { get; set; }
        private static string FromAddress;
        private static string strSmtpClient;
        private static string UserID;
        private static string Password;
        private static string SMTPPort;
        private static bool bEnableSSL;
        #endregion
        #region Interface Implementation
        public async Task SendAsync(IdentityMessage message)
        {
            await configSendGridasync(message);
        }
        #endregion
        #region Send Email Method
        public async Task configSendGridasync(IdentityMessage message)
        {
            GetMailData();
            dynamic MailMessage = new MailMessage();
            MailMessage.From = new MailAddress(FromAddress);
            MailMessage.To.Add(message.Destination);
            MailMessage.Subject = message.Subject;
            MailMessage.IsBodyHtml = true;
            MailMessage.Body = message.Body;
            SmtpClient SmtpClient = new SmtpClient();
            SmtpClient.Host = strSmtpClient;
            SmtpClient.EnableSsl = true;
            SmtpClient.Port = Int32.Parse(SMTPPort);
            SmtpClient.Credentials = new System.Net.NetworkCredential(UserID, Password);
            try
            {
                try
                {
                    SmtpClient.Send(MailMessage);
                }
                catch (Exception ex)
                {
                }
            }
            catch (SmtpFailedRecipientsException ex)
            {
                for (int i = 0; i <= ex.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = ex.StatusCode;
                    if ((status == SmtpStatusCode.MailboxBusy) | (status == SmtpStatusCode.MailboxUnavailable))
                    {
                        System.Threading.Thread.Sleep(5000);
                        SmtpClient.Send(MailMessage);
                    }
                }
            }
        }
        #endregion
        #region Get Email provider data From Web.config file
        private static void GetMailData()
        {
            //  cid OptionID    key value
            //1   9800    RegEmailID info@6dproptech.com
            //1   10000   SMTPServer mail.aeclogic.com
            //1   10200   EmailSSLPortNo  995
            //1   14000   MailPassword    1

            string erpCS = ConfigurationManager.ConnectionStrings["ERPConnectionString"].ConnectionString;
            SqlParameter[] sp = new SqlParameter[1];
            sp[0] = new SqlParameter("@cid", cid);
            DataSet ds = SQLDBUtil.ExecuteDataset("sp_emailSettings", sp);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count == 4)
            {
                FromAddress = ds.Tables[0].Rows[0][3].ToString();
                strSmtpClient = ds.Tables[0].Rows[1][3].ToString();
                UserID = ds.Tables[0].Rows[0][3].ToString();
                Password = ds.Tables[0].Rows[3][3].ToString();
                SMTPPort = ds.Tables[0].Rows[2][3].ToString();
                bEnableSSL = true;
            }
        }
    }
    #endregion
}
