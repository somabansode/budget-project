﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Login.Controllers.HMS {
    public class FamilyController :ApiController {
        [HttpGet]
        [Route("api/family/relations")]
        public HttpResponseMessage getRelations() {
            try {          //http://localhost/Login/api/family/relations
                List<T_HMS_Relation> clRelations = new List<T_HMS_Relation>();
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    con.Open();
                    SqlCommand cmd = new SqlCommand(" select  ID, Name, active from T_HMS_Relation", con);
                    using(SqlDataReader rdr = cmd.ExecuteReader()) {
                        while(rdr.Read()) {
                            T_HMS_Relation t = new T_HMS_Relation();
                            t.ID = rdr["ID"] == DBNull.Value ? -1 : (int)rdr["ID"];
                            t.Name = rdr["Name"] == DBNull.Value ? "" : rdr["Name"].ToString();
                            t.active = rdr["active"] == DBNull.Value ? -1 : (int)rdr["active"];
                            clRelations.Add(t);
                        }
                    }
                    con.Close();
                    return Request.CreateResponse(HttpStatusCode.OK, clRelations);
                }
            } catch(Exception ex) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message); }
        }
        [HttpGet]
        [Route("api/family")]
        public HttpResponseMessage getEmpFamilyDetails(int cid) {
            try {          //http://localhost/Login/api/family?cid=1
                List<T_G_EmpFamilyDetails> clFamilyDetails = new List<T_G_EmpFamilyDetails>();
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    con.Open();
                    SqlCommand cmd = new SqlCommand(" select t1.* from T_G_EmpFamilyDetails t1 join T_G_EmployeeMaster t2 on t1.EmpID = t2.EmpId where t2.cid = @cid", con);
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    using(SqlDataReader rdr = cmd.ExecuteReader()) {
                        while(rdr.Read()) {
                            T_G_EmpFamilyDetails t = new T_G_EmpFamilyDetails();
                            t.EmpID = rdr["EmpID"] == DBNull.Value ? -1 : (int)rdr["EmpID"];
                            t.RID = rdr["RID"] == DBNull.Value ? -1 : (int)rdr["RID"];
                            t.Name = rdr["Name"] == DBNull.Value ? "" : rdr["Name"].ToString();
                            t.DOB = rdr["DOB"] == DBNull.Value ? "" : rdr["DOB"].ToString();
                            t.BloodGroup = rdr["BloodGroup"] == DBNull.Value ? "" : rdr["BloodGroup"].ToString();
                            t.Gender = rdr["Gender"] == DBNull.Value ? false : (bool)rdr["Gender"];
                            clFamilyDetails.Add(t);
                        }
                    }
                    con.Close();
                    return Request.CreateResponse(HttpStatusCode.OK, clFamilyDetails);
                }
            } catch(Exception ex) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message); }
        }        [HttpPost]
        [Route("api/family")]
        public HttpResponseMessage postEmpFamilyDetails([FromBody] List<T_G_EmpFamilyDetails> clFamilyDetails, int cid) {
            if(clFamilyDetails.Count > 0) {
                try {          //http://localhost/Login/api/family?cid=1  //[{  EmpID: 1281, RID: 1, Name: 'Ramya KK', DOB: '1995-01-01', BloodGroup: 'O+', Gender: 0,  }]
                    using(SqlConnection con = new SqlConnection(common.erpCS)) {
                        con.Open();
                        using(SqlTransaction tran = con.BeginTransaction()) {
                            T_G_EmpFamilyDetails t = new T_G_EmpFamilyDetails();
                            using(SqlBulkCopy bulkCopy = new SqlBulkCopy(con, SqlBulkCopyOptions.Default, tran)) {
                                DataTable dtClass = t.dtClass("dtClass");
                                int i = 0;
                                foreach(T_G_EmpFamilyDetails E in clFamilyDetails) {
                                    DataRow dr = dtClass.NewRow();
                                    dr["EmpID"] = E.EmpID;
                                    dr["RID"] = E.RID;
                                    dr["Name"] = E.Name;
                                    dr["DOB"] = E.DOB;
                                    dr["BloodGroup"] = E.BloodGroup;
                                    dr["Gender"] = E.Gender;
                                    dtClass.Rows.Add(dr);
                                }
                                bulkCopy.DestinationTableName = "[dbo].[T_G_EmpFamilyDetails]";
                                bulkCopy.BulkCopyTimeout = 30000;
                                try {
                                    bulkCopy.WriteToServer(dtClass);
                                    tran.Commit();
                                    con.Close();
                                    return Request.CreateResponse(HttpStatusCode.OK, clFamilyDetails.Count.ToString() + " record(s) inserted!");
                                } catch(Exception ex) {
                                    tran.Rollback();
                                    con.Close();
                                    return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                                }
                            }
                        }
                    }
                } catch(Exception ex) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message); }
            } else return Request.CreateResponse(HttpStatusCode.BadRequest, "Nothing to INSERT!");
        }
    }
    public class T_G_EmpFamilyDetails {
        public int EmpID { get; set; }
        public int RID { get; set; }
        public string Name { get; set; }
        public string DOB { get; set; }
        public string BloodGroup { get; set; }
        public bool Gender { get; set; }

        public T_G_EmpFamilyDetails() { }
        public T_G_EmpFamilyDetails(int _EmpID, int _RID, string _Name, string _DOB, string _BloodGroup, bool _Gender) {
            EmpID = _EmpID; RID = _RID; Name = _Name; DOB = _DOB; BloodGroup = _BloodGroup; Gender = _Gender;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            DataColumn EmpID = new DataColumn();
            EmpID.DataType = System.Type.GetType("System.Int32");
            EmpID.ColumnName = "EmpID";
            EmpID.AutoIncrement = true;
            dtName.Columns.Add(EmpID);
            dtName.Columns.Add("RID", typeof(System.Int32));
            dtName.Columns.Add("Name", typeof(System.String));
            dtName.Columns.Add("DOB", typeof(System.String));
            dtName.Columns.Add("BloodGroup", typeof(System.String));
            dtName.Columns.Add("Gender", typeof(System.Boolean));

            DataColumn[] keys = new DataColumn[1];
            keys[0] = EmpID;
            dtName.PrimaryKey = keys;
            return dtName;
        }
         
    }
    public class T_HMS_Relation {
        public int ID { get; set; }
        public string Name { get; set; }
        public int active { get; set; }

        public T_HMS_Relation() { }
        public T_HMS_Relation(int _ID, string _Name, int _active) {
            ID = _ID; Name = _Name; active = _active;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            DataColumn ID = new DataColumn();
            ID.DataType = System.Type.GetType("System.Int32");
            ID.ColumnName = "ID";
            ID.AutoIncrement = true;
            dtName.Columns.Add("ID");
            dtName.Columns.Add("Name", typeof(System.String));
            dtName.Columns.Add("active", typeof(System.Int32));

            DataColumn[] keys = new DataColumn[1];
            keys[0] = ID;
            dtName.PrimaryKey = keys;
            return dtName;
        }
      
    }
}
