﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Login.Controllers.HMS {
    public class BioAttendanceController :ApiController {
        [HttpGet]
        [Route("api/BioAttendance/empsInFences")] // finding In & Out of GF for each employee by worksite optionally
        public async Task<HttpResponseMessage> empsInFences(int cid, DateTime dt, int? wsID) {
            try {     // http://localhost/Login/api/BioAttendance/empsInFences?cid=1&dt=2019-01-19&wsID=null
                List<EmpInOutStatus> clEmpAttInOut = await Task.Run(() => DAL.ComputeEmpInsideFenceAsync(cid, dt,wsID));
                return Request.CreateResponse(HttpStatusCode.OK, clEmpAttInOut);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost]
        [Route("api/BioAttendance/passAtt2Geofence")]//Passing the Out employees to approved status
        public async Task<HttpResponseMessage> passAtt2Geofence([FromBody] List<th_empAtt2Geofence> clth_empAtt2Geofence, int cid) {
            try {   // http://localhost/Login/api/BioAttendance/passAtt2Geofence?cid=1 // [{ empID: 6, gfID: 9, attID: 151, pass: 1,  }]
                if(clth_empAtt2Geofence.Count > 0) {
                    string result = await Task.Run(() => DAL.CreatePassAtt2GeofenceAsync(clth_empAtt2Geofence, cid));
                    if(result == clth_empAtt2Geofence.Count.ToString() + " record(s) inserted!")
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    else return Request.CreateResponse(HttpStatusCode.BadRequest, result);
                } else return Request.CreateResponse(HttpStatusCode.BadRequest, "Nothing to INSERT!");
            } catch(Exception ex) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message); }
        }
        [HttpGet]
        [Route("api/BioAttendance/passAtt2Geofence")] //to show all emps with approved/pass status 
        public async Task<HttpResponseMessage> passAtt2Geofence(int cid, DateTime dt, int? wsID) {
            try {   //  http://localhost/Login/api/BioAttendance/passAtt2Geofence?cid=1&dt=2019-01-19&wsID=1
                List<th_empAtt2Geofence> clempAtt2Geofence = await Task.Run(() => DAL.ReadPassAtt2GeofenceAsync(cid, dt, wsID));
                return Request.CreateResponse(HttpStatusCode.OK, clempAtt2Geofence);
            } catch(Exception ex) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message); }
        }
        [HttpGet]
        [Route("api/BioAttendance/empAttInOut")]
        public async Task<HttpResponseMessage> empAttInOut(int cid, int? wsID, int? empID, DateTime? dt) {
            try {     // http://localhost/Login/api/BioAttendance/empAttInOut?cid=1&wsID=1&empID=6&dt=2019-01-19
                List<vh_empAttInOut> clEmpAttInOut = await Task.Run(() => DAL.ReadEmpAttInOutAsync(cid, wsID, empID, dt));
                return Request.CreateResponse(HttpStatusCode.OK, clEmpAttInOut);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]
        [Route("api/BioAttendance/empAttTrack")]
        public async Task<HttpResponseMessage> empAttInOut(int cid, DateTime dt, int? wsID) {
            try {   // http://localhost/Login/api/BioAttendance/empAttTrack?cid=1&dt=2019-01-19&wsID=null
                List<EmpAttTrack> clEmpAttTrack = await Task.Run(() => DAL.ReadEmpAttTrackAsync(cid, dt, wsID ));
                return Request.CreateResponse(HttpStatusCode.OK, clEmpAttTrack);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]
        [Route("api/BioAttendance/bioRegistrations")]
        public async Task<HttpResponseMessage> bioRegistrations(int cid) {
            try {     //http://localhost/Login/api/BioAttendance/bioRegistrations?cid=1
                List<vh_bioRegistrations> clvh_bioRegistrations = await Task.Run(() => DAL.ReadBioRegistrationsAsync(cid));
                return Request.CreateResponse(HttpStatusCode.OK, clvh_bioRegistrations);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost]
        [Route("api/BioAttendance/geoFenceVsEmpID")]//used in Bio-Registration Page to SetGF method
        public async Task<HttpResponseMessage> geoFenceVsEmpID([FromBody] List<th_geoFenceVsEmpID> clth_geoFenceVsEmpID, int cid) {
            try {   // http://localhost/Login/api/BioAttendance/geoFenceVsEmpID?cid=1 //[{  empID: -1, gfID: -1, dtStamp: '2019-02-13', ia: 1 }]
                if(clth_geoFenceVsEmpID.Count > 0) {
                    string result = await Task.Run(() => DAL.CreateGeoFenceVsEmpIDAsync(clth_geoFenceVsEmpID, cid));
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                } else return Request.CreateResponse(HttpStatusCode.BadRequest, "Nothing to INSERT!");
            } catch(Exception ex) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message); }
        }        [HttpPut]
        [Route("api/BioAttendance/geoFenceVsEmpID")]//used in Bio-Registration Page to ChangeGF method
        public async Task<HttpResponseMessage> geoFenceVsEmpID([FromBody] List<twoIDs> clGfEmp) {
            try {   // http://localhost/Login/api/BioAttendance/geoFenceVsEmpID //[ { empID: 41, ID2: 11 } , { empID: 42, ID2: 11 } ]
                if(clGfEmp.Count > 0) {
                    string result = await Task.Run(() => DAL.UpdateGeoFenceVsEmpIDAsync(clGfEmp));
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                } else return Request.CreateResponse(HttpStatusCode.BadRequest, "Nothing to INSERT!");
            } catch(Exception ex) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message); }
        }        [HttpGet]
        [Route("api/BioAttendance/empLocations")]
        public async Task<HttpResponseMessage> empLocations(int cid, DateTime dt) {
            try {          //http://localhost/Login/api/BioAttendance/empLocations?cid=1&dt=2019-02-01
                List<emplatlng> clEmpAttGeoLoc = await Task.Run(() => DAL.ReadEmpLocationsAsync(cid, dt));
                return Request.CreateResponse(HttpStatusCode.OK, clEmpAttGeoLoc);
            } catch(Exception ex) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message); }
        }        [HttpGet]
        [Route("api/BioAttendance/geoFence")]
        public async Task<HttpResponseMessage> GeoFence(int cid) {
            try {          //http://localhost/Login/api/BioAttendance/geoFence?cid=1
                List<th_geoFence> clGeoFence = await Task.Run(() => DAL.ReadGeoFencesAsync(cid));
                return Request.CreateResponse(HttpStatusCode.OK, clGeoFence);
            } catch(Exception ex) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message); }
        }        [HttpPut]
        [Route("api/BioAttendance/geoFence")]
        public async Task<HttpResponseMessage> GeoFence(int fid, string fenceName) {
            try {          //http://localhost/Login/api/BioAttendance/geoFence?fid=1&fenceName=First_Fence_in_ws1
                string res = await Task.Run(() => DAL.UpdateGeoFencesAsync(fid,fenceName));
               if(res == "success") return Request.CreateResponse(HttpStatusCode.OK, "Updated");
               else return Request.CreateResponse(HttpStatusCode.BadRequest, res);
            } catch(Exception ex) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message); }
        }        [HttpDelete]
        [Route("api/BioAttendance/geoFence")]
        public async Task<HttpResponseMessage> deleteGeoFence(int id) {
            try {          //http://localhost/Login/api/BioAttendance/geoFence?id=6
                string res = await Task.Run(() => DAL.DeleteGeoFenceCoordsAsync(id));
               if(res == "success") return Request.CreateResponse(HttpStatusCode.OK, "Fence ID:" + id.ToString() + " deleted");
               else return Request.CreateResponse(HttpStatusCode.BadRequest, res);
            } catch(Exception ex) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message); }
        }        [HttpGet]
        [Route("api/BioAttendance/geoFenceCoords")]
        public async Task<HttpResponseMessage> geoFenceCoords(int cid) {
            try {          //http://localhost/Login/api/BioAttendance/geoFenceCoords?cid=1 
                List<th_geoFenceCoords> clgeoFenceCoords = await Task.Run(() => DAL.ReadGeoFenceCoordsAsync(cid));
                return Request.CreateResponse(HttpStatusCode.OK, clgeoFenceCoords);
            } catch(Exception ex) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message); }
        }        [HttpPost]
        [Route("api/BioAttendance/geoFenceWithCoords")]
        public async Task<HttpResponseMessage> geoFenceWithCoords([FromBody] List<fenceCoords> clGeoFence, int cid, int ws) {
            try {   // http://localhost/Login/api/BioAttendance/geoFenceWithCoords?cid=1&ws=1 
                //[ {fid:'3.200001_101.500000', v:0, x:2.100001, y:101.700000}, 
                //{fid:'3.200001_101.500000', v:1, x:2.110001, y:101.700000},
                //{fid:'3.200001_101.500000', v:2, x:2.100001, y:102.520000}]
                if(clGeoFence.Count > 0) {
                    string result = await Task.Run(() => DAL.CreateGeoFenceWithCoordsAsync(clGeoFence, cid, ws));
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Nothing to INSERT!");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }        [HttpGet]
        [Route("api/BioAttendance/geoFenceWithCoords")]
        public async Task<HttpResponseMessage> geoFenceWithCoords(int cid, int ws) {
            try {     // http://localhost/Login/api/BioAttendance/geoFenceWithCoords?cid=1&ws=1
                List<vh_wsGeoFenceCoords> clwsGeoFenceCoords = await Task.Run(() => DAL.Readvh_wsGeoFenceCoordsAsync(cid, ws));
                return Request.CreateResponse(HttpStatusCode.OK, clwsGeoFenceCoords);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        } 
        [HttpGet]
        [Route("api/BioAttendance/worksitelatLng")]
        public async Task<HttpResponseMessage> worksitelatLng(int cid) {
            try {     // http://localhost/Login/api/BioAttendance/worksitelatLng?cid=1
                List<th_worksitelatLng> clth_worksitelatLng = await Task.Run(() => DAL.ReadWorksitelatLngAsync(cid));
                return Request.CreateResponse(HttpStatusCode.OK, clth_worksitelatLng);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost]
        [Route("api/BioAttendance/worksitelatLng")]
        public async Task<HttpResponseMessage> worksitelatLng([FromBody] List<th_worksitelatLng> clWorksitelatLng, int cid) {
            try { // http://localhost/Login/api/BioAttendance/worksitelatLng?cid=1 // [{  id: 1, lat: 0, lng: 0  }]
                if(clWorksitelatLng.Count > 0) {
                    string result = await Task.Run(() => DAL.CreateWorksitelatLngAsync(clWorksitelatLng, cid));
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Nothing to INSERT!");
            } catch(Exception ex) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message); }
        }
        [HttpPost]
        [Route("api/BioAttendance/geoFence")]//DROPPED 
        public async Task<HttpResponseMessage> geoFence([FromBody] List<th_geoFence> clGeoFence, int cid) {
            try {   //http://localhost/Login/api/BioAttendance/geoFence?cid=1  //[{  id: 0, cid: 1, ws: 1, fenceID: '3.850500_101.852968', ia: 1 }]
                if(clGeoFence.Count > 0) {
                    string result = await Task.Run(() => DAL.CreateGeoFencesAsync(clGeoFence, cid));
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Nothing to INSERT!");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }        [HttpPost]
        [Route("api/BioAttendance/geoFenceCoords")] //DROPPED
        public async Task<HttpResponseMessage> geoFenceCoords([FromBody] List<th_geoFenceCoords> clth_geoFenceCoords, int cid) {
            try { //http://localhost/Login/api/BioAttendance/geoFenceCoords?cid=1 //[{  gfid: -1, v: -1, x: 0, y: 0,  }]
                if(clth_geoFenceCoords.Count > 0) {
                    string result = await Task.Run(() => DAL.CreateGeoFenceCoordsAsync(clth_geoFenceCoords, cid));
                    return Request.CreateResponse(HttpStatusCode.BadRequest, result);
                } else return Request.CreateResponse(HttpStatusCode.BadRequest, "Nothing to INSERT!");
            } catch(Exception ex) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message); }
        }        [HttpGet]
        [Route("api/BioAttendance/geoFenceVsEmpID")]//DROPPED
        public async Task<HttpResponseMessage> geoFenceVsEmpID(int cid) {
            try {   //http://localhost/Login/api/BioAttendance/geoFenceVsEmpID?cid=1
                List<th_geoFenceVsEmpID> clgeoFenceVsEmpID = await Task.Run(() => DAL.ReadGeoFenceVsEmpIDAsync(cid));
                return Request.CreateResponse(HttpStatusCode.OK, clgeoFenceVsEmpID);
            } catch(Exception ex) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message); }
        }        [HttpGet]
        [Route("api/BioAttendance/master")]//DROP THIS 
        public async Task<HttpResponseMessage> EmployeeMaster_add(int cid) {
            try {   // http://localhost/Login/api/BioAttendance/master?cid=1
                List<T_G_EmployeeMaster_add> regEmps = await Task.Run(() => DAL.ReadBioRegisteredEmployeesAsync(cid));
                return Request.CreateResponse(HttpStatusCode.OK, regEmps);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        
    }
    public class T_G_EmployeeMaster_add {
        public int id { get; set; }
        public int empID { get; set; }
        public string fingerPrintValue { get; set; }
        public bool ia { get; set; }

        public T_G_EmployeeMaster_add() { }
        public T_G_EmployeeMaster_add(int _id, int _empID, string _fingerPrintValue, bool _ia) {
            id = _id; empID = _empID; fingerPrintValue = _fingerPrintValue; ia = _ia;
        }
    }
    public class EmpAttTrack {
        public int id { get; set; }
        public DateTime dtStamp { get; set; }
        public double lat { get; set; }
        public double lng { get; set; }
        public int empID { get; set; }
        public int cid { get; set; }
        public int wsID { get; set; }

        public EmpAttTrack() { }
        public EmpAttTrack(int _id, DateTime _dtStamp, double _lat, double _lng, int _empID, int _cid, int _wsID) {
            id = _id; dtStamp = _dtStamp; lat = _lat; lng = _lng; empID = _empID; cid= _cid; wsID = _wsID;
        }
    }
    public class emplatlng {
        public int wsID { get; set; }
        public int empID { get; set; }
        public double lat { get; set; }
        public double lng { get; set; }
    }
    public class th_geoFence {
        public int id { get; set; }
        public int cid { get; set; }
        public int ws { get; set; }
        public string fenceID { get; set; }
        public bool ia { get; set; }

        public th_geoFence() { }
        public th_geoFence(int _id, int _cid, int _ws, string _fenceID, bool _ia) {
            id = _id; cid = _cid; ws = _ws; fenceID = _fenceID; ia = _ia;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            DataColumn id = new DataColumn();
            id.DataType = System.Type.GetType("System.Int32");
            id.ColumnName = "id";
            id.AutoIncrement = true;
            dtName.Columns.Add(id);
            dtName.Columns.Add("cid", typeof(System.Int32));
            dtName.Columns.Add("ws", typeof(System.Int32));
            dtName.Columns.Add("fenceID", typeof(System.String));
            dtName.Columns.Add("ia", typeof(System.Boolean));

            DataColumn[] keys = new DataColumn[1];
            keys[0] = id;
            dtName.PrimaryKey = keys;
            return dtName;
        }

    }
    public class th_geoFenceCoords {
        public int gfid { get; set; }
        public int v { get; set; }
        public double x { get; set; }
        public double y { get; set; }

        public th_geoFenceCoords() { }
        public th_geoFenceCoords(int _gfid, int _v, double _x, double _y) {
            gfid = _gfid; v = _v; x = _x; y = _y;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            DataColumn gfid = new DataColumn();
            gfid.DataType = System.Type.GetType("System.Int32");
            gfid.ColumnName = "gfid";
            gfid.AutoIncrement = true;
            dtName.Columns.Add(gfid);
            dtName.Columns.Add("v", typeof(System.Int32));
            dtName.Columns.Add("x", typeof(System.Double));
            dtName.Columns.Add("y", typeof(System.Double));

            DataColumn[] keys = new DataColumn[1];
            keys[0] = gfid;
            dtName.PrimaryKey = keys;
            return dtName;
        }

        //export interface th_geoFenceCoords {
        // gfid: number; 
        // v: number; 
        // x: number; 
        // y: number; 
        //}
    }
    public class EmpInOutStatus {
        public int wsID { get; set; }
        public int gfID { get; set; }
        public int empID { get; set; }
        public int attID { get; set; }
        public DateTime dtStamp { get; set; }
        public bool ia { get; set; }
        public EmpInOutStatus() { }
        public EmpInOutStatus(int ws, int gf, int emp, int att, DateTime dt, bool s) {
            wsID = ws; gfID = gf; empID = emp; attID=att; dtStamp = dt; ia = s;
        }
    }
    public class th_geoFenceVsEmpID {
        public int empID { get; set; }
        public int gfID { get; set; }
        public DateTime dtStamp { get; set; }
        public bool ia { get; set; }

        public th_geoFenceVsEmpID() { }
        public th_geoFenceVsEmpID(int _empID, int _gfID, DateTime _dtStamp, bool _ia) {
            empID = _empID; gfID = _gfID; dtStamp = _dtStamp; ia = _ia;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("empID", typeof(System.Int32));
            dtName.Columns.Add("gfID", typeof(System.Int32));
            dtName.Columns.Add("dtStamp", typeof(System.DateTime));
            dtName.Columns.Add("ia", typeof(System.Boolean));
            return dtName;
        }
    }
    public class th_empAtt2Geofence {
        public int empID { get; set; }
        public int gfID { get; set; }
        public int attID { get; set; }
        public bool pass { get; set; }

        public th_empAtt2Geofence() { }
        public th_empAtt2Geofence(int _empID, int _gfID, int _attID, bool _pass) {
            empID = _empID; gfID = _gfID; attID = _attID; pass = _pass;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("empID", typeof(System.Int32));
            dtName.Columns.Add("gfID", typeof(System.Int32));
            dtName.Columns.Add("attID", typeof(System.Int32));
            dtName.Columns.Add("pass", typeof(System.Boolean));
            return dtName;
        }
    }
    public class vh_empAttInOut {
        public int EmpID { get; set; }
        public string Employee { get; set; }
        public DateTime Date { get; set; }
        public int Times { get; set; }
        public string InTime { get; set; }
        public string OutTime { get; set; }
        public bool AttRes { get; set; }
        public vh_empAttInOut() { }
        public vh_empAttInOut(int _EmpID, string _Employee, DateTime _Date, int _Times, string _InTime, string _OutTime, bool _attRes) {
            EmpID = _EmpID; Employee = _Employee; Date = _Date; Times = _Times; InTime = _InTime; OutTime = _OutTime; AttRes = _attRes;
        }
        //public DataTable dtClass(string tblName) {
        //    DataTable dtName = new DataTable(tblName);
        //    DataColumn EmpID = new DataColumn();
        //    dtName.Columns.Add("EmpID", typeof(System.Int32));
        //    dtName.Columns.Add("Employee", typeof(System.String));
        //    dtName.Columns.Add("Date", typeof(System.DateTime));
        //    dtName.Columns.Add("Times", typeof(System.Int32));
        //    dtName.Columns.Add("InTime", typeof(System.String));
        //    dtName.Columns.Add("OutTime", typeof(System.String));
        //    dtName.Columns.Add("AttRes", typeof(System.Boolean));
        //    return dtName;
        //}
    }
    public class vh_bioRegistrations {
        public int cid { get; set; }
        public int wsID { get; set; }
        public int empID { get; set; }
        public int emSt { get; set; }
        public string deviceID { get; set; }
        public bool rgSt { get; set; }
        public int gfID { get; set; }
        public bool gfSt { get; set; }

        public vh_bioRegistrations() { }
        public vh_bioRegistrations(int _cid, int _empID, int _emSt, string _deviceID, bool _rgSt, int _gfID, bool _gfSt) {
            cid = _cid; empID = _empID; emSt = _emSt; deviceID = _deviceID; rgSt = _rgSt; gfID = _gfID; gfSt = _gfSt;
        }
    }
    public class th_worksitelatLng {
        public int id { get; set; }
        public double lat { get; set; }
        public double lng { get; set; }
        public th_worksitelatLng() { }
        public th_worksitelatLng(int _id, double _lat, double _lng) {
            id = _id; lat = _lat; lng = _lng;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("id", typeof(System.Int32));
            dtName.Columns.Add("lat", typeof(System.Double));
            dtName.Columns.Add("lng", typeof(System.Double));
            return dtName;
        }    }
    public class vh_wsGeoFenceCoords {
        public int cid { get; set; }
        public int ws { get; set; }
        public int id { get; set; }
        public string fenceID { get; set; }
        public int v { get; set; }
        public double x { get; set; }
        public double y { get; set; }
        public vh_wsGeoFenceCoords() { }
        public vh_wsGeoFenceCoords(int _cid, int _ws, int _id, string _fenceID, int _v, double _x, double _y) {
            cid = _cid; ws = _ws; id = _id; fenceID = _fenceID; v = _v; x = _x; y = _y;
        }
    }
}
