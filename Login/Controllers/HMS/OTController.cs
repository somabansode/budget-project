﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Login.Controllers.HMS {
    public class OTController :ApiController {
        [HttpGet]
        [Route("api/ot/designations")]
        public async Task<HttpResponseMessage> designations(int cid) {
            try {     //http://localhost/Login/api/ot/designations?cid=1
                List<th_otDesignations> clth_otDesignations = await Task.Run(() => DAL.ReadOTDesignationsAsync(cid));
                return Request.CreateResponse(HttpStatusCode.OK, clth_otDesignations);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost]
        [Route("api/ot/designations")]
        public async Task<HttpResponseMessage> BulkCreateDesignations([FromBody]List<th_otDesignations> clth_otDesignations, int cid, int userID) {
            try { //http://localhost/Login/api/ot/designations?cid=1&userID=1 //[{  desID: -1, isSup: -1, cb: -1, co: '2019-02-27', ub: -1, uo: '2019-02-27',  }]

                if(clth_otDesignations.Count > 0) {
                    string result = await Task.Run(() => DAL.CreateOTDesignations(clth_otDesignations, cid, userID));
                    if(!result.Contains("failed")) {
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No records to Update");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPut]
        [Route("api/ot/designations")] // http://localhost/Login/api/ot/designations?cid=1&userID=1 // [{  id: 6, bl: true}]
        public async Task<HttpResponseMessage> BulkUpdateOTDesignations([FromBody]List<intBool> clotDesignations, int cid, int userID) {
            try {
                if(clotDesignations.Count > 0) {
                    string result = await Task.Run(() => DAL.UpdateOTDesignationsAsync(clotDesignations, cid, userID));
                    if(!result.Contains("failed")) {
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No records to Update");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpDelete]
        [Route("api/ot/designations")]//http://localhost/Login/api/ot/designations?cid=1&userID=1 from body << [6,10]
        public async Task<HttpResponseMessage> DeleteOTDesignations([FromBody] List<int> IDs, int userID) {
            try {
                if(IDs.Count > 0) {
                    string result = await Task.Run(() => DAL.DeleteOTDesignation(IDs, userID));
                    if(!result.Contains("failed")) {
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No records to Delete");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]
        [Route("api/ot/incentives")]
        public async Task<HttpResponseMessage> incentives(int cid) {
            try {   //http://localhost/Login/api/ot/incentives?cid=1
                List<th_otMalaysia_Incentives> clIncentives = await Task.Run(() => DAL.ReadIncentivesAsync(cid));
                return Request.CreateResponse(HttpStatusCode.OK, clIncentives);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPut]
        [Route("api/ot/incentives")]
        public async Task<HttpResponseMessage> BulkUpdateIncentives([FromBody]List<th_otMalaysia_Incentives> clIncentives, int cid, int userID) {
            try {         //http://localhost/Login/api/ot/incentives?cid=1&userID=1 //[{  id: 1, bracket: 'First 4 hours', start: 0, finish: 4, rate: 15, isNight: 0, cid: 1,  }]
                if(clIncentives.Count > 0) {
                    string result = await Task.Run(() => DAL.UpdateIncentivesAsync(clIncentives, cid, userID));
                    if(!result.Contains("failed")) {
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No records to Update");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]
        [Route("api/ot/employeesOnOT")]
        public async Task<HttpResponseMessage> getvh_employeesOnOT(int cid) {
            try {     //http://localhost/Login/api/ot/employeesOnOT?cid=1
                List<vh_employeesOnOT> clvh_employeesOnOT = await Task.Run(() => DAL.Readvh_employeesOnOTAsync(cid));
                return Request.CreateResponse(HttpStatusCode.OK, clvh_employeesOnOT);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]
        [Route("api/ot/otParameters")]
        public async Task<HttpResponseMessage> getvh_otParameters(int cid) {
            try {     // http://localhost/Login/api/ot/otParameters?cid=1
                List<vh_otParameters> clvh_otParameters = await Task.Run(() => DAL.ReadOTParametersAsync(cid));
                return Request.CreateResponse(HttpStatusCode.OK, clvh_otParameters);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPut]
        [Route("api/ot/NatureOfEmployment")]
        public async Task<HttpResponseMessage> BulkUpdateT_HR_NatureOfEmployment(int NatureOfEmp, int value) {
            try {   //http://localhost/Login/api/ot/NatureOfEmployment?NatureOfEmp=1&NatureOfEmp=1
                string result = await Task.Run(() => DAL.UpdateNatureOfEmploymentAsync(NatureOfEmp, value));
                if(!result.Contains("failed")) {
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]
        [Route("api/ot/otWorkersMaster")]
        public async Task<HttpResponseMessage> getth_otMalaysia_Workers(int cid) {
            try {     //http://localhost/Login/api/ot/otWorkersMaster?cid=1
                List<th_otMalaysia_Workers> clotMalaysia_Workers = await Task.Run(() => DAL.ReadotMalaysia_WorkersAsync(cid));
                return Request.CreateResponse(HttpStatusCode.OK, clotMalaysia_Workers);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]
        [Route("api/ot/otSupervisors")]
        public async Task<HttpResponseMessage> getvh_otSupervisors(int cid, DateTime st, DateTime ed) {
            try {     //http://localhost/Login/api/ot/otSupervisors?cid=1&st=2019-01-01&ed=2019-01-31
                List<vh_otSupervisors> clvh_otSupervisors = await Task.Run(() => DAL.ReadotSupervisorsAsync(cid, st, ed));
                return Request.CreateResponse(HttpStatusCode.OK, clvh_otSupervisors);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]
        [Route("api/ot/otWorkers")]
        public async Task<HttpResponseMessage> getvh_otWorkers(int cid, DateTime st, DateTime ed) {
            try {     //http://localhost/Login/api/ot/otWorkers?cid=1&st=2019-01-01&ed=2019-01-31
                List<vh_otWorkers> clvh_otWorkers = await Task.Run(() => DAL.ReadotWorkersAsync(cid, st, ed));
                return Request.CreateResponse(HttpStatusCode.OK, clvh_otWorkers);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

       
    }
    public class vh_otWorkers {
        public DateTime attDate { get; set; }
        public int empID { get; set; }
        public int DesgID { get; set; }
        public string InTime { get; set; }
        public string OutTime { get; set; }
        public decimal wkHrs { get; set; }
        public decimal shHrs { get; set; }
        public decimal br { get; set; }
        public bool isWO { get; set; }
        public bool isPH { get; set; }
        public bool isSplDay { get; set; }
        public decimal Basic { get; set; }
        public decimal OT { get; set; }
        public decimal nrRateEx { get; set; }
        public decimal woRate { get; set; }
        public decimal woRateEx { get; set; }
        public decimal phRate { get; set; }
        public decimal phRateEx { get; set; }

        public vh_otWorkers() { }
        public vh_otWorkers(DateTime _attDate, int _empID, int _DesgID, string _InTime, string _OutTime, decimal _wkHrs
            ,decimal _shHrs, decimal _br, bool _isWO, bool _isPH, bool _isSplDay, decimal _Basic, decimal _OT, decimal _nrRateEx
            , decimal _woRate, decimal _woRateEx, decimal _phRate, decimal _phRateEx) {
            attDate = _attDate; empID = _empID; DesgID = _DesgID; InTime = _InTime; OutTime = _OutTime; wkHrs = _wkHrs;
            shHrs = _shHrs; br = _br; isWO = _isWO; isPH = _isPH; isSplDay = _isSplDay; Basic = _Basic; OT = _OT;
            nrRateEx = _nrRateEx; woRate = _woRate; woRateEx = _woRateEx; phRate = _phRate; phRateEx = _phRateEx;
        }
    }
    public class vh_otSupervisors {
        public DateTime attDate { get; set; }
        public int empID { get; set; }
        public int DesgID { get; set; }
        public string InTime { get; set; }
        public string OutTime { get; set; }
        public decimal wkHrs { get; set; }
        public decimal shHrs { get; set; }
        public decimal br { get; set; }
        public bool isWO { get; set; }
        public bool isPH { get; set; }
        public bool isSplDay { get; set; }
        public decimal Basic { get; set; }
        public decimal OT { get; set; }
        public decimal Bkt1 { get; set; }
        public decimal Rate1 { get; set; }
        public decimal Bkt2 { get; set; }
        public decimal Rate2 { get; set; }
        public decimal NgtAlw { get; set; }

        public vh_otSupervisors() { }
        public vh_otSupervisors(DateTime _attDate, int _empID, int _DesgID, string _InTime, string _OutTime, decimal _wkHrs
             , decimal _shHrs, decimal _br, bool _isWO, bool _isPH, bool _isSplDay, decimal _Basic, decimal _OT, decimal _Bkt1
            , decimal _Rate1, decimal _Bkt2, decimal _Rate2, decimal _NgtAlw) {
            attDate = _attDate; empID = _empID; DesgID = _DesgID; InTime = _InTime; OutTime = _OutTime; wkHrs = _wkHrs;
            shHrs = _shHrs; br = _br; isWO = _isWO; isPH = _isPH; isSplDay = _isSplDay; Basic = _Basic; OT = _OT; Bkt1 = _Bkt1;
            Rate1 = _Rate1; Bkt2 = _Bkt2; Rate2 = _Rate2; NgtAlw = _NgtAlw;
        }
    }
    public class th_otDesignations {
        public int desID { get; set; }
        public string Designation { get; set; }
        public bool isSup { get; set; }
        public int cb { get; set; }
        public DateTime co { get; set; }
        public int ub { get; set; }
        public DateTime uo { get; set; }

        public th_otDesignations() { }
        public th_otDesignations(int _desID, string _designation, bool _isSup, int _cb, DateTime _co, int _ub, DateTime _uo) {
            desID = _desID; Designation = _designation; isSup = _isSup; cb = _cb; co = _co; ub = _ub; uo = _uo;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("desID", typeof(System.Int32));
            dtName.Columns.Add("isSup", typeof(System.Boolean));
            dtName.Columns.Add("cb", typeof(System.Int32));
            dtName.Columns.Add("co", typeof(System.DateTime));
            dtName.Columns.Add("ub", typeof(System.Int32));
            dtName.Columns.Add("uo", typeof(System.DateTime));
            return dtName;
        }
    }
    public class th_otMalaysia_Incentives {
        public int id { get; set; }
        public string bracket { get; set; }
        public decimal start { get; set; }
        public decimal finish { get; set; }
        public decimal rate { get; set; }
        public bool isNight { get; set; }
        public int cid { get; set; }

        public th_otMalaysia_Incentives() { }
        public th_otMalaysia_Incentives(int _id, string _bracket, decimal _start, decimal _finish, decimal _rate, bool _isNight, int _cid) {
            id = _id; bracket = _bracket; start = _start; finish = _finish; rate = _rate; isNight = _isNight; cid = _cid;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("id", typeof(System.Int32));
            dtName.Columns.Add("bracket", typeof(System.String));
            dtName.Columns.Add("start", typeof(System.Double));
            dtName.Columns.Add("finish", typeof(System.Double));
            dtName.Columns.Add("rate", typeof(System.Double));
            dtName.Columns.Add("isNight", typeof(System.Boolean));
            dtName.Columns.Add("cid", typeof(System.Int32));
            return dtName;
        }
    }
    public class vh_employeesOnOT {
        public int cid { get; set; }
        public int empID { get; set; }
        public string FullName { get; set; }
        public int desID { get; set; }
        public string Designation { get; set; }
        public string Nature { get; set; }
        public bool isSup { get; set; }

        public vh_employeesOnOT() { }
        public vh_employeesOnOT(int _cid, int _empID, string _FullName, int _desID, string _Designation, string _Nature, bool _isSup) {
            cid = _cid; empID = _empID; FullName = _FullName; desID = _desID; Designation = _Designation; Nature = _Nature; isSup = _isSup;
        }
    }
    public class vh_otParameters {
        public int EmpID { get; set; }
        public int DesgID { get; set; }
        public DateTime AttDate { get; set; }
        public string inTime { get; set; }
        public string outTime { get; set; }
        public int nat { get; set; }
        public int shift { get; set; }
        public decimal Basic { get; set; }
        public decimal wkHrs { get; set; }
        public decimal shHrs { get; set; }
        public bool isSplDay { get; set; }
        public decimal splWkHrs { get; set; }
        public bool isWO { get; set; }
        public bool isPH { get; set; }
        public bool br { get; set; }

        public vh_otParameters() { }
        public vh_otParameters(int _EmpID, int _DesgID, DateTime _AttDate, string _inTime, string _outTime, int _nat
            , int _shift, decimal _Basic, decimal _wkHrs, decimal _shHrs, bool _isSplDay, decimal _splWkHrs, bool _isWO, bool _isPH, bool _br) {
            EmpID = _EmpID; DesgID = _DesgID; AttDate = _AttDate; inTime = _inTime; outTime = _outTime; nat = _nat; shift = _shift;
            Basic = _Basic; wkHrs = _wkHrs; shHrs = _shHrs; isSplDay = _isSplDay; splWkHrs = _splWkHrs; isWO = _isWO; isPH = _isPH; br = _br;
        }
    }
    public class th_otMalaysia_Workers {
        public int id { get; set; }
        public decimal salUpto { get; set; }
        public decimal NorRateEx { get; set; }
        public decimal woRate { get; set; }
        public decimal woRateEx { get; set; }
        public decimal phRate { get; set; }
        public decimal phRatEx { get; set; }
        public int cid { get; set; }
        public bool ia { get; set; }

        public th_otMalaysia_Workers() { }
        public th_otMalaysia_Workers(int _id, decimal _salUpto, decimal _NorRateEx, decimal _woRate
            , decimal _woRateEx, decimal _phRate, decimal _phRatEx, int _cid, bool _ia) {
            id = _id; salUpto = _salUpto; NorRateEx = _NorRateEx; woRate = _woRate; woRateEx = _woRateEx;
            phRate = _phRate; phRatEx = _phRatEx; cid = _cid; ia = _ia;
        }
    }
}
