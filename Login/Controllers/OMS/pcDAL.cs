﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Login.Controllers.OMS {
    public class pcDAL {
        public static async Task<List<intStrIntArray>> ddlTaskBidAsync(int pid) {
            List<intIntString> clTidBid = new List<intIntString>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "oms.unplannedTasks";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pid", pid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            intIntString t = new intIntString();
                            t.id1 = rdr["tid"] == DBNull.Value ? -1 : (int)rdr["tid"];
                            t.id2 = rdr["bid"] == DBNull.Value ? -1 : (int)rdr["bid"];
                            t.str = rdr["TaskName"] == DBNull.Value ? "" :  rdr["TaskName"].ToString();
                            clTidBid.Add(t);
                        }
                    }
                }
                con.Close();
                var distT = (from t in clTidBid select t.id1).Distinct();
                List<intStrIntArray> clIntArray  = new List<intStrIntArray>();
                foreach(int i in distT) {
                    var task = (from t in clTidBid where t.id1 == i select t.str).FirstOrDefault();
                    var chds = (from t in clTidBid where t.id1 == i select t.id2).ToList();
                    intStrIntArray ia = new intStrIntArray();
                    ia.id = i; ia.str = task; ia.intAr = chds;
                    clIntArray.Add(ia);
                }
                return clIntArray;
            }
        }
        public static async Task<List<BTA>> ReadBtaAllAsync(int tid, int? bid) {
            List<BTA> clBtaAll = new List<BTA>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "oms.BtaRead";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@tid", tid));
                    cmd.Parameters.Add(new SqlParameter("@bid", bid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            BTA t = new BTA();
                            t.bid = rdr["bid"] == DBNull.Value ? -1 : (int)rdr["bid"];
                            t.tid = rdr["tid"] == DBNull.Value ? -1 : (int)rdr["tid"];
                            t.tud = rdr["tud"] == DBNull.Value ? -1 : (int)rdr["tud"];
                            t.aid = rdr["aid"] == DBNull.Value ? -1 : (int)rdr["aid"];
                            t.cat = rdr["cat"] == DBNull.Value ? -1 : (int)rdr["cat"];
                            t.rid = rdr["rid"] == DBNull.Value ? -1 : (int)rdr["rid"];
                            t.ResourceName = rdr["ResourceName"] == DBNull.Value ? "" : rdr["ResourceName"].ToString();
                            t.rud = rdr["rud"] == DBNull.Value ? -1 : (int)rdr["rud"];
                            t.coeff = rdr["coeff"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["coeff"]);
                            t.RaQty = rdr["RaQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["RaQty"]);
                            t.rBL = rdr["rBL"] == DBNull.Value ? -1 : (int)rdr["rBL"];
                            clBtaAll.Add(t);
                        }
                    }
                }
                con.Close();
                return clBtaAll;
            }
        }
        public static async Task<List<stWithPl>> ReadUnplannedSTsAsync(int tid, int? bid) {
            List<unplanST> clUnplannedSTs = new List<unplanST>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "oms.unplannedSTs";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@tid", tid));
                    cmd.Parameters.Add(new SqlParameter("@bid", bid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            unplanST t = new unplanST();
                            t.bid = rdr["bid"] == DBNull.Value ? -1 : (int)rdr["bid"];
                            t.stID = rdr["stID"] == DBNull.Value ? -1 : (int)rdr["stID"];
                            t.subTask = rdr["subTask"] == DBNull.Value ? "" : rdr["subTask"].ToString();
                            t.Qty = rdr["Qty"] == DBNull.Value ? 0: Convert.ToDecimal(rdr["Qty"]);
                            t.id = rdr["id"] == DBNull.Value ? -1 : (int)rdr["id"];
                            t.Date = rdr["Date"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["Date"]);
                            t.PlQty = rdr["PlQty"] == DBNull.Value ? 0: Convert.ToDecimal(rdr["PlQty"]);
                            t.pcID = rdr["pcID"] == DBNull.Value ? -1 : (int)rdr["pcID"];
                            clUnplannedSTs.Add(t);
                        }
                    }
                }
                con.Close();
                List<stWithPl> clStWithPl = new List<stWithPl>();
                var distSTs = (from t in clUnplannedSTs select t.stID).Distinct();
                foreach(int st in distSTs) {
                    List<stPl> stPls = (from t in clUnplannedSTs  where t.stID == st select new stPl(t.id, t.Date, t.PlQty)).ToList(); 
                    unplanST upSt = (from t in clUnplannedSTs where t.stID == st select t).FirstOrDefault(); 
                    stWithPl swpl = new stWithPl(upSt.bid, upSt.stID, upSt.subTask,upSt.Qty, upSt.pcID, stPls );
                    clStWithPl.Add(swpl);
                }
                return clStWithPl;
            }
        }
        public static async Task<List<mtPCs>> ReadMasterTaskPCsAsync(int tid, int? bid, bool match) {
            List<mtPCs> clmtPCs = new List<mtPCs>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "oms.pcLoadIDs";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@tid", tid));
                    cmd.Parameters.Add(new SqlParameter("@bid", bid));
                    cmd.Parameters.Add(new SqlParameter("@match", match));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            mtPCs t = new mtPCs();
                            t.tmID = rdr["tmID"] == DBNull.Value ? -1 : (int)rdr["tmID"];
                            t.pcID = rdr["pcID"] == DBNull.Value ? -1 : (int)rdr["pcID"];
                            t.UoM = rdr["UoM"] == DBNull.Value ? -1 : (int)rdr["UoM"];
                            t.pCoeffCode = rdr["pCoeffCode"] == DBNull.Value ? "" : rdr["pCoeffCode"].ToString();
                            t.tgtQty = rdr["tgtQty"] == DBNull.Value ? 0: Convert.ToDecimal(rdr["tgtQty"]);
                            clmtPCs.Add(t);
                        }
                    }
                }
                con.Close();
                return clmtPCs;
            }
        }
        public static async Task<List<pCoeff>> ReadPCoeffAsync(int pcid) {
            List<pCoeff> clpCoeff = new List<pCoeff>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "oms.pCoeff";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pcid", pcid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            pCoeff t = new pCoeff();
                            t.pcid = rdr["pcid"] == DBNull.Value ? -1 : (int)rdr["pcid"];
                            t.cat = rdr["cat"] == DBNull.Value ? -1 : (int)rdr["cat"];
                            t.rid = rdr["rid"] == DBNull.Value ? -1 : (int)rdr["rid"];
                            t.ResourceName = rdr["ResourceName"] == DBNull.Value ? "" : rdr["ResourceName"].ToString();
                            t.rud = rdr["rud"] == DBNull.Value ? -1 : (int)rdr["rud"];
                            t.coeff = rdr["coeff"] == DBNull.Value ? 0: Convert.ToDecimal(rdr["coeff"]);
                            clpCoeff.Add(t);
                        }
                    }
                }
                con.Close();
                return clpCoeff;
            }
        }
        public static async Task<pcCompoundObject> xxLoadPcObjects(int tid, int? bid) {
            List<stWithPl> unPlSts =  await Task.Run(()=> ReadUnplannedSTsAsync(tid, bid));
            List<BTA> bTAs =  await Task.Run(()=> ReadBtaAllAsync(tid, bid));
            bool isMatchingPC = true; 
            List<mtPCs> mTPCs =  await Task.Run(()=> ReadMasterTaskPCsAsync(tid, bid, isMatchingPC));
            int pcid = (from t in mTPCs select t.pcID).FirstOrDefault();
            List<pCoeff> pCoeff =  await Task.Run(()=> ReadPCoeffAsync(pcid));
            pcCompoundObject objectRange = new pcCompoundObject(tid,unPlSts, bTAs,mTPCs,pCoeff);
            return objectRange;
        }
        public static async Task<pcCompoundObject> LoadPcObjects(int tid, int? bid) {
            using(SqlConnection conn = new SqlConnection(common.erpCS)) {
                await conn.OpenAsync();
                DataSet ds = new DataSet();
                List<unplanST> clUnplannedSTs = new List<unplanST>();
                List<stWithPl> clUnPlSts = new List<stWithPl>();
                List<BTA> clBTAs = new List<BTA>();
                List<mtPCs> clMTPCs = new List<mtPCs>();
                List<pCoeff> clPCoeff = new List<pCoeff>();
                using(SqlCommand cmd = new SqlCommand("oms.pcObjects", conn)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@tid", tid));
                    cmd.Parameters.Add(new SqlParameter("@bid", bid));
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(ds);
                    foreach(DataRow rdr in ds.Tables[0].Rows) { //unPlSts
                        unplanST t = new unplanST();
                        t.bid = rdr["bid"] == DBNull.Value ? -1 : (int)rdr["bid"];
                        t.stID = rdr["stID"] == DBNull.Value ? -1 : (int)rdr["stID"];
                        t.subTask = rdr["subTask"] == DBNull.Value ? "" : rdr["subTask"].ToString();
                        t.Qty = rdr["Qty"] == DBNull.Value ? 0: Convert.ToDecimal(rdr["Qty"]);
                        t.id = rdr["id"] == DBNull.Value ? -1 : (int)rdr["id"];
                        t.Date = rdr["Date"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["Date"]);
                        t.PlQty = rdr["PlQty"] == DBNull.Value ? 0: Convert.ToDecimal(rdr["PlQty"]);
                        t.pcID = rdr["pcID"] == DBNull.Value ? -1 : (int)rdr["pcID"];
                        clUnplannedSTs.Add(t);
                    }
                    
                    foreach(DataRow rdr in ds.Tables[1].Rows) { //bTAs
                        BTA t = new BTA();
                        t.bid = rdr["bid"] == DBNull.Value ? -1 : (int)rdr["bid"];
                        t.tid = rdr["tid"] == DBNull.Value ? -1 : (int)rdr["tid"];
                        t.tud = rdr["tud"] == DBNull.Value ? -1 : (int)rdr["tud"];
                        t.aid = rdr["aid"] == DBNull.Value ? -1 : (int)rdr["aid"];
                        t.cat = rdr["cat"] == DBNull.Value ? -1 : (int)rdr["cat"];
                        t.rid = rdr["rid"] == DBNull.Value ? -1 : (int)rdr["rid"];
                        t.ResourceName = rdr["ResourceName"] == DBNull.Value ? "" : rdr["ResourceName"].ToString();
                        t.rud = rdr["rud"] == DBNull.Value ? -1 : (int)rdr["rud"];
                        t.coeff = rdr["coeff"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["coeff"]);
                        t.RaQty = rdr["RaQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["RaQty"]);
                        t.rBL = rdr["rBL"] == DBNull.Value ? -1 : (int)rdr["rBL"];
                        clBTAs.Add(t);
                    } 
                    foreach(DataRow rdr in ds.Tables[2].Rows) { //mTPCs
                        mtPCs t = new mtPCs();
                        t.tmID = rdr["tmID"] == DBNull.Value ? -1 : (int)rdr["tmID"];
                        t.pcID = rdr["pcID"] == DBNull.Value ? -1 : (int)rdr["pcID"];
                        t.UoM = rdr["UoM"] == DBNull.Value ? -1 : (int)rdr["UoM"];
                        t.pCoeffCode = rdr["pCoeffCode"] == DBNull.Value ? "" : rdr["pCoeffCode"].ToString();
                        t.tgtQty = rdr["tgtQty"] == DBNull.Value ? 0: Convert.ToDecimal(rdr["tgtQty"]);
                        clMTPCs.Add(t);
                    } 
                    foreach(DataRow rdr in ds.Tables[3].Rows) { //pCoeff
                        pCoeff t = new pCoeff();
                        t.pcid = rdr["pcid"] == DBNull.Value ? -1 : (int)rdr["pcid"];
                        t.cat = rdr["cat"] == DBNull.Value ? -1 : (int)rdr["cat"];
                        t.rid = rdr["rid"] == DBNull.Value ? -1 : (int)rdr["rid"];
                        t.ResourceName = rdr["ResourceName"] == DBNull.Value ? "" : rdr["ResourceName"].ToString();
                        t.rud = rdr["rud"] == DBNull.Value ? -1 : (int)rdr["rud"];
                        t.coeff = rdr["coeff"] == DBNull.Value ? 0: Convert.ToDecimal(rdr["coeff"]);
                        clPCoeff.Add(t);
                    }
                    var distSTs = (from t in clUnplannedSTs select t.stID).Distinct();
                    foreach(int st in distSTs) {
                        List<stPl> stPls = (from t in clUnplannedSTs  where t.stID == st select new stPl(t.id, t.Date, t.PlQty)).ToList(); 
                        unplanST upSt = (from t in clUnplannedSTs where t.stID == st select t).FirstOrDefault(); 
                        stWithPl swpl = new stWithPl(upSt.bid, upSt.stID, upSt.subTask,upSt.Qty,upSt.pcID, stPls );
                        clUnPlSts.Add(swpl);
                    }
                }
                conn.Close();
                return new pcCompoundObject(tid,clUnPlSts, clBTAs,clMTPCs,clPCoeff);
            }
        }
    }
    public class BTA {
        public int bid { get; set; }
        public int tid { get; set; }
        public int tud { get; set; }
        public int aid { get; set; }
        public int cat { get; set; }
        public int rid { get; set; }
        public string ResourceName { get; set; }
        public int rud { get; set; }
        public decimal coeff { get; set; }
        public decimal RaQty { get; set; }
        public int rBL { get; set; }

        public BTA() { }
        public BTA(int _bid, int _tid, int _Unit, int _aid, int _cat, int _rid, string _ResourceName, int _rud, decimal _coeff, decimal _RaQty, int _rBL) {
            bid = _bid; tid = _tid; tud = _Unit; aid = _aid; cat = _cat; rid = _rid; ResourceName = _ResourceName; rud = _rud; coeff = _coeff; RaQty = _RaQty; rBL = _rBL;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("bid", typeof(System.Int32));
            dtName.Columns.Add("tid", typeof(System.Int32));
            dtName.Columns.Add("Unit", typeof(System.Int32));
            dtName.Columns.Add("aid", typeof(System.Int32));
            dtName.Columns.Add("cat", typeof(System.Int32));
            dtName.Columns.Add("rid", typeof(System.Int32));
            dtName.Columns.Add("ResourceName", typeof(System.String));
            dtName.Columns.Add("rud", typeof(System.Int32));
            dtName.Columns.Add("coeff", typeof(System.Double));
            dtName.Columns.Add("RaQty", typeof(System.Double));
            dtName.Columns.Add("rBL", typeof(System.Int32));
            return dtName;
        }
    }
    public class unplanST {
        public int bid { get; set; }
        public int stID { get; set; }
        public string subTask { get; set; }
        public decimal Qty { get; set; }
        public int id { get; set; }
        public DateTime Date { get; set; }
        public decimal PlQty { get; set; }
        public int pcID { get; set; }
        public unplanST(int _bid, int _stID,string _subTask, decimal _Qty, int _id, DateTime _Date, decimal _PlQty, int _pcID) {
            bid=_bid;  stID = _stID; subTask = _subTask; Qty = _Qty;  id= _id;   Date= _Date;  PlQty = _PlQty;   pcID = _pcID;
        }
        public unplanST() {

        }
    }
     public class stWithPl {
        public int bid { get; set; }
        public int stID { get; set; }
        public string subTask { get; set; }
        public decimal Qty { get; set; }
        public int pcID { get; set; }
        public List<stPl> stPls { get;set;}
        public stWithPl(int _bid, int _stID,string _subTask, decimal _Qty, int _pcID,  List<stPl> _stPls ) {
              bid=_bid;  stID = _stID; subTask = _subTask; Qty = _Qty;  pcID = _pcID; stPls = _stPls;
        }
    }
    public class stPl {
        public int id { get; set; }
        public DateTime Date { get; set; }
        public decimal PlQty { get; set; }
        public stPl(int _id, DateTime _Date, decimal _PlQty ) {
            id= _id;   Date= _Date;  PlQty = _PlQty;
        }
    }
    public class mtPCs {
        public int tmID { get; set; }
        public int pcID { get; set; }
        public string pCoeffCode { get; set; }
        public int UoM { get; set; }
        public decimal tgtQty { get; set; }

    }
    public class pCoeff {
        public int pcid { get; set; }
        public int cat { get; set; }
        public int rid { get; set; }
        public string ResourceName { get; set; }
        public int rud { get; set; }
        public decimal coeff { get; set; }

    }
    public class pcCompoundObject {
        public int tid { get; set; }
        public List<stWithPl> unPlSts { get; set; }
        public List<BTA> bTAs { get; set; }
        public List<mtPCs> mTPCs { get; set; }
        public List<pCoeff> pCoeffs { get; set; }
        public pcCompoundObject() { }
        public pcCompoundObject(int _tid, List<stWithPl> _unPlSts, List<BTA> _bTAs , List<mtPCs> _mTPCs, List<pCoeff> _pCoeffs) { 
            tid = _tid; unPlSts= _unPlSts; bTAs =_bTAs;  mTPCs = _mTPCs; pCoeffs =_pCoeffs;
        }

    }
}
