﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Login.Controllers.OMS {
    public class PCoeffController :ApiController {
        
        [HttpGet]  // http://localhost/Login/api/PCoeff/ddlTasks?pid=71
        [Route("api/PCoeff/ddlTasks")]
        public async Task<HttpResponseMessage> ddlTasks(int pid) {
            try {   
                List<intStrIntArray> clTidBidsAll = await Task.Run(() => pcDAL.ddlTaskBidAsync(pid));
                return Request.CreateResponse(HttpStatusCode.OK, clTidBidsAll);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]  // http://localhost/Login/api/PCoeff/bta?tid=1&bid=null
        [Route("api/PCoeff/bta")]
        public async Task<HttpResponseMessage> getBTA(int tid, int? bid) {
            try {   
                List<BTA> clBtaAll = await Task.Run(() => pcDAL.ReadBtaAllAsync(tid, bid));
                return Request.CreateResponse(HttpStatusCode.OK, clBtaAll);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]  // http://localhost/Login/api/PCoeff/unplannedSTs?tid=3012&bid=20
        [Route("api/PCoeff/unplannedSTs")]
        public async Task<HttpResponseMessage> unplannedSTs(int tid, int? bid) {
            try {   
                List<stWithPl> clUnplanST = await Task.Run(() => pcDAL.ReadUnplannedSTsAsync(tid, bid));
                return Request.CreateResponse(HttpStatusCode.OK, clUnplanST);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]  // http://localhost/Login/api/PCoeff/masterTaskPCs?tid=3012&bid=20&match=false
        [Route("api/PCoeff/masterTaskPCs")]
        public async Task<HttpResponseMessage> masterTaskPCs(int tid, int? bid, bool match) {
            try {   
                List<mtPCs> clmtPCs = await Task.Run(() => pcDAL.ReadMasterTaskPCsAsync(tid, bid, match));
                return Request.CreateResponse(HttpStatusCode.OK, clmtPCs);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]  //  http://localhost/Login/api/PCoeff/pcObjects?tid=3012&bid=20
        [Route("api/PCoeff/pcObjects")]
        public async Task<HttpResponseMessage> pCoeff(int tid, int? bid) {
            try {   
                pcCompoundObject pcObjects = await Task.Run(() => pcDAL.LoadPcObjects(tid, bid));
                return Request.CreateResponse(HttpStatusCode.OK, pcObjects);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]  // http://localhost/Login/api/PCoeff/pCoeff?pcid=2556
        [Route("api/PCoeff/pCoeff")]
        public async Task<HttpResponseMessage> pCoeff(int pcid) {
            try {   
                List<pCoeff> clpCoeff = await Task.Run(() => pcDAL.ReadPCoeffAsync(pcid));
                return Request.CreateResponse(HttpStatusCode.OK, clpCoeff);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }
    
}
