﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Login.Controllers.OMS {
    public class stController :ApiController {
        [HttpGet] // http://localhost/Login/api/st/masterWBS?cid=1
        [Route("api/st/masterWBS")]
        public async Task<HttpResponseMessage> getT_OMS_Components(int cid) {
            try {
                List<T_OMS_Components> clT_OMS_Components = await Task.Run(() => oDAL.ReadT_OMS_ComponentsAsync(cid));
                return Request.CreateResponse(HttpStatusCode.OK, clT_OMS_Components);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost] //http://localhost/Login/api/st/masterWBS?cid=1 
        [Route("api/st/masterWBS")]
        public async Task<HttpResponseMessage> BulkCreateT_OMS_Components([FromBody]List<T_OMS_Components> clT_OMS_Components, int cid) {
            try { //[{  CompId: -1, Component: 'YOUR DATA', Specification: 'YOUR DATA', IsActive: -1, PC_OrderID: -1, Cid: -1,  }]
                string result = await Task.Run(() => oDAL.CreateMasterComponentsAsync(clT_OMS_Components, cid));
                if(!result.Contains("failed")) {
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPut] // http://localhost/Login/api/st/masterWBS
        [Route("api/st/masterWBS")]
        public async Task<HttpResponseMessage> BulkUpdateT_OMS_Components([FromBody]List<T_OMS_Components> clT_OMS_Components) {
            try {   // [{  CompId: -1, Component: 'YOUR DATA'}]
                if(clT_OMS_Components.Count > 0) {
                    string result = await Task.Run(() => oDAL.UpdateT_OMS_ComponentsAsync(clT_OMS_Components));
                    if(!result.Contains("failed")) {
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No records to Update");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpDelete]  //http://localhost/Login/api/st/masterWBS //[13]
        [Route("api/st/masterWBS")]
        public async Task<HttpResponseMessage> BulkDeleteT_OMS_Components([FromBody] List<int> IDs) {
            try {
                if(IDs.Count > 0) {
                    string result = await Task.Run(() => oDAL.DeleteT_OMS_ComponentsAsync(IDs));
                    if(!result.Contains("failed")) {
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No records to Delete");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]  // http://localhost/Login/api/st/masterTask
        [Route("api/st/masterTask")]
        public async Task<HttpResponseMessage> getT_OMS_TasksMaster() {
            try {
                List<T_OMS_TasksMaster> clT_OMS_TasksMaster = await Task.Run(() => oDAL.ReadTasksMasterAsync());
                return Request.CreateResponse(HttpStatusCode.OK, clT_OMS_TasksMaster);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost] //http://localhost/Login/api/st/masterTask?userID=1
        [Route("api/st/masterTask")]
        public async Task<HttpResponseMessage> BulkCreateT_OMS_TasksMaster([FromBody]List<T_OMS_TasksMaster> clT_OMS_TasksMaster, int userID) {
            try {  //[{Task: 'New Task2 in malaysia'}]
                string result = await Task.Run(() => oDAL.CreateTasksMasterAsync(clT_OMS_TasksMaster, userID));
                if(!result.Contains("failed")) {
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPut]//http://localhost/Login/api/st/masterTask
        [Route("api/st/masterTask")]
        public async Task<HttpResponseMessage> BulkUpdateT_OMS_TasksMaster([FromBody]List<T_OMS_TasksMaster> clT_OMS_TasksMaster) {
            try {  //[{  TaskMasterID: 180, Task: 'Changed Task'}]
                if(clT_OMS_TasksMaster.Count > 0) {
                    string result = await Task.Run(() => oDAL.UpdateTasksMasterAsync(clT_OMS_TasksMaster));
                    if(!result.Contains("failed")) {
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No records to Update");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpDelete]   //http://localhost/Login/api/st/masterTask
        [Route("api/st/masterTask")]
        public async Task<HttpResponseMessage> BulkDeleteT_OMS_TasksMaster([FromBody] List<int> IDs) {
            try {
                if(IDs.Count > 0) {
                    string result = await Task.Run(() => oDAL.DeleteTasksMasterAsync(IDs));
                    if(!result.Contains("failed")) {
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No records to Delete");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]// http://localhost/Login/api/st/ProjectComponents?pid=1
        [Route("api/st/ProjectComponents")]
        public async Task<HttpResponseMessage> getT_OMS_ProjectComponents(int pid) {
            try {
                List<T_OMS_ProjectComponents> clT_OMS_ProjectComponents = await Task.Run(() => oDAL.ReadProjectComponentsAsync(pid));
                return Request.CreateResponse(HttpStatusCode.OK, clT_OMS_ProjectComponents);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost] // http://localhost/Login/api/st/ProjectComponents?pid=1 
        [Route("api/st/ProjectComponents")]
        public async Task<HttpResponseMessage> BulkCreateT_OMS_ProjectComponentsAsync([FromBody]List<T_OMS_ProjectComponents> clT_OMS_ProjectComponents, int pid) {
            try {   //  [{  CompName: 'AngNew Comp', ProjectID: 1, CompId: 1, Details: 'AngNew Details', FromCh: `1, ToCh: 22, IsActive: 1, PC_OrderID: 1, LagDays: 11, Cid: 1 }]
                string result = await Task.Run(() => oDAL.CreateProjectComponentsAsync(clT_OMS_ProjectComponents, pid));
                if(!result.Contains("failed")) {
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPut] // http://localhost/Login/api/st/ProjectComponents
        [Route("api/st/ProjectComponents")]
        public async Task<HttpResponseMessage> BulkUpdateT_OMS_ProjectComponents([FromBody]List<T_OMS_ProjectComponents> clT_OMS_ProjectComponents) {
            try {  //[{PrjCompId: 53,  CompName: 'AngNew 11', ProjectID: 1, CompId: 1, Details: 'aaaa', FromCh: 33, ToCh: 44, IsActive: 1, PC_OrderID: 1, LagDays: 4, Cid: 1 }]
                if(clT_OMS_ProjectComponents.Count > 0) {
                    string result = await Task.Run(() => oDAL.UpdateProjectComponentsAsync(clT_OMS_ProjectComponents));
                    if(!result.Contains("failed")) {
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No records to Update");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpDelete] //http://localhost/Login/api/st/ProjectComponents?userID=xx
        [Route("api/st/ProjectComponents")]
        public async Task<HttpResponseMessage> BulkDeleteT_OMS_ProjectComponents([FromBody] List<int> IDs) {
            try {
                if(IDs.Count > 0) {
                    string result = await Task.Run(() => oDAL.DeleteProjectComponentsAsync(IDs));
                    if(!result.Contains("failed")) {
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No records to Delete");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]// http://localhost/Login/api/st/subtasks?pid=1&tid=null&mrp=10&cp=1&ps=20
        [Route("api/st/subtasks")]
        public async Task<HttpResponseMessage> getT_OMS_SubTasks(int pid, int? tid, int mrp, int cp, int ps) {
            try {
                Tuple<int, List<T_OMS_SubTasks>> tupleSubTasks = await Task.Run(() => oDAL.ReadSubTasksAsync(pid, tid, mrp, cp, ps));
                intObject intObject = new intObject(tupleSubTasks.Item1, tupleSubTasks.Item2);
                return Request.CreateResponse(HttpStatusCode.OK, intObject);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost] // http://localhost/Login/api/st/subtasks?tid=1&userID=1
        [Route("api/st/subtasks")]
        public async Task<HttpResponseMessage> BulkCreateT_OMS_SubTasksAsync([FromBody]List<T_OMS_SubTasks> clsubTasks, int tid, int userID) {
            try {         // [{SubTaskName: 'st64d', StartDate: '2019-03-02', EndDate: '2019-03-02', Nos: 2, B: 2, D: 2, L: 2,
                          // FromChainage: 0, ToChainage: 0, IsActive: 1, PrjCompId: 1, IsPBAllow: 1 }]
                string result = await Task.Run(() => oDAL.CreateSubTasksAsync(clsubTasks, tid, userID));
                if(!result.Contains("failed")) {
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPut] // http://localhost/Login/api/st/subtasks?userID=1
        [Route("api/st/subtasks")]
        public async Task<HttpResponseMessage> BulkUpdateT_OMS_SubTasks([FromBody]List<T_OMS_SubTasks> clsubTasks, int userID) {
            try {    //[{  SubTaskID: -1, TaskID: -1, SubTaskName: 'YOUR DATA', QTY: 0, StartDate: '2019-03-02', EndDate: '2019-03-02', SubSecId: -1, Remarks: 'YOUR DATA', CreatedOn: '2019-03-02', CreatedBy: -1, UpdatedOn: '2019-03-02', UpdatedBy: -1, Nos: 0, B: 0, D: 0, L: 0, FromChainage: 0, ToChainage: 0, IsActive: -1, PrjCompId: -1, TempCompID: -1, TempEleID: -1, IsPBAllow: -1, Sub_Pred: 'YOUR DATA', Sub_Sucs: 'YOUR DATA', Dur: -1, RefCode: 'YOUR DATA', stPCID: -1,  }]     
                if(clsubTasks.Count > 0) {
                    string result = await Task.Run(() => oDAL.UpdateSubTasksAsync(clsubTasks, userID));
                    if(!result.Contains("failed")) {
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No records to Update");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpDelete]// http://localhost/Login/api/st/subtasks?userID=1
        [Route("api/st/subtasks")]
        public async Task<HttpResponseMessage> BulkDeleteT_OMS_SubTasks([FromBody] List<int> IDs, int userID) {
            try {
                if(IDs.Count > 0) {
                    string result = await Task.Run(() => oDAL.DeleteSubTasksAsync(IDs, userID));
                    if(!result.Contains("failed")) {
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No records to Delete");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]// http://localhost/Login/api/st/baseLines?pid=1
        [Route("api/st/baseLines")]
        public async Task<HttpResponseMessage> getbaseLines(int pid) {
            try {
                List<baseLines> clbaseLines = await Task.Run(() => oDAL.ReadbaseLinesAsync(pid));
                return Request.CreateResponse(HttpStatusCode.OK, clbaseLines);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }        [HttpGet] // http://localhost/Login/api/st/BLTasks?pid=1
        [Route("api/st/BLTasks")]
        public async Task<HttpResponseMessage> getT_OMS_BL_Tasks(int pid) {
            try {
                List<T_OMS_BL_Tasks> clT_OMS_BL_Tasks = await Task.Run(() => oDAL.ReadBlTasksAsync(pid));
                return Request.CreateResponse(HttpStatusCode.OK, clT_OMS_BL_Tasks);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }        [HttpGet] // http://localhost/Login/api/st/BLAssignments?pid=1&bid=1
        [Route("api/st/BLAssignments")]
        public async Task<HttpResponseMessage> getT_OMS_BL_Assignments(int pid, int bid) {
            try {
                List<T_OMS_BL_Assignments> clT_OMS_BL_Assignments = await Task.Run(() => oDAL.ReadBlAssignmentsAsync(pid, bid));
                return Request.CreateResponse(HttpStatusCode.OK, clT_OMS_BL_Assignments);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }        [HttpPost] // http://localhost/Login/api/st/sendToProduction?userID=1 
        [Route("api/st/sendToProduction")]        public async Task<HttpResponseMessage> sendToProductionAsync([FromBody]List<int> sts, int userID) {
            try { // [24308,24309]
                string result = await Task.Run(() => oDAL.sendSendToProductionAsync(sts, userID));
                if(!result.Contains("failed")) {
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }        [HttpGet] // http://localhost/Login/api/st/stplans?tid=64&stID=24335&cp=1&ps=20
        [Route("api/st/stplans")]
        public async Task<HttpResponseMessage> getto_stplans(int tid, int? stID, int cp, int ps) {
            try {
                Tuple<int, List<to_stplans>> clstplans = await Task.Run(() => oDAL.ReadStplansAsync(tid, stID, cp, ps));
                intObject intObject = new intObject(clstplans.Item1, clstplans.Item2);
                return Request.CreateResponse(HttpStatusCode.OK, intObject);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost] // http://localhost/Login/api/st/stplans?userID=1
        [Route("api/st/stplans")]
        public async Task<HttpResponseMessage> BulkCreateto_stplansAsync([FromBody]List<to_stplans> clStplans, int userID) {
            try {  //[{ stid: 24309, plDtID: 97217, PlQty: 2.33, UnPlanID: null  }]
                string result = await Task.Run(() => oDAL.CreateStplansAsync(clStplans, userID));
                if(!result.Contains("failed")) {
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPut] // http://localhost/Login/api/st/stplans?userID=1
        [Route("api/st/stplans")]
        public async Task<HttpResponseMessage> BulkUpdateto_stplans([FromBody]List<to_stplans> clStplans, int userID) {
            try {       //[{  id: 195949, stid: 24363, plDtID: 97231, PlQty: 2.22, UnPlanID: null  }]    
                if(clStplans.Count > 0) {
                    string result = await Task.Run(() => oDAL.UpdateStplansAsync(clStplans, userID));
                    if(!result.Contains("failed")) {
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No records to Update");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpDelete] // http://localhost/Login/api/st/stplans?userID=1
        [Route("api/st/stplans")]
        public async Task<HttpResponseMessage> BulkDeleteto_stplans([FromBody] List<twoIDs> IDs, int userID) {
            try {
                if(IDs.Count > 0) {
                    string result = await Task.Run(() => oDAL.DeleteStplansAsync(IDs, userID));
                    if(!result.Contains("failed")) {
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No records to Delete");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet] // http://localhost/Login/api/st/date?back=5&forw=5
        [Route("api/st/date")]
        public async Task<HttpResponseMessage> getDate(int back, int forw) {
            try {
                List<vg_Date> clDate = await Task.Run(() => oDAL.ReadDateAsync(back, forw));
                return Request.CreateResponse(HttpStatusCode.OK, clDate);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }        [HttpGet]  // http://localhost/Login/api/st/dt
        [Route("api/st/dt")]
        public async Task<HttpResponseMessage> getDt() {
            try {
                List<IDDt> clDt = await Task.Run(() => oDAL.ReadDtAsync());
                return Request.CreateResponse(HttpStatusCode.OK, clDt);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet] // http://localhost/Login/api/st/mrpStatges 
        [Route("api/st/mrpStatges")]
        public async Task<HttpResponseMessage> getT_OMS_MRPStatges() {
            try {     
                List<T_OMS_MRPStatges> clT_OMS_MRPStatges = await Task.Run(() => oDAL.ReadMRPStatgesAsync());
                return Request.CreateResponse(HttpStatusCode.OK, clT_OMS_MRPStatges);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        //oms.TasksInProduction 71
        [HttpGet]  // http://localhost/Login/api/st/TasksInProduction?pid=71
        [Route("api/st/TasksInProduction")]
        public async Task<HttpResponseMessage> TasksInProduction(int pid) {
            try {
                List<TasksInProd> clTasksInProd = await Task.Run(() => oDAL.ReadclTasksInProdAsync(pid));
                return Request.CreateResponse(HttpStatusCode.OK, clTasksInProd);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }
    public class TasksInProd {
        public int pid { get; set; }
        public int tid { get; set; }
        public string Task { get; set; }
    }
    public class T_OMS_Components {
        public int CompId { get; set; }
        public string Component { get; set; }
        public string Specification { get; set; }
        public bool IsActive { get; set; }
        public int PC_OrderID { get; set; }

        public T_OMS_Components() { }
        public T_OMS_Components(int _CompId, string _Component, string _Specification, bool _IsActive, int _PC_OrderID) {
            CompId = _CompId; Component = _Component; Specification = _Specification; IsActive = _IsActive; PC_OrderID = _PC_OrderID;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            DataColumn CompId = new DataColumn();
            CompId.DataType = System.Type.GetType("System.Int32");
            CompId.ColumnName = "CompId";
            CompId.AutoIncrement = true;
            dtName.Columns.Add(CompId);
            dtName.Columns.Add("Component", typeof(System.String));
            dtName.Columns.Add("Specification", typeof(System.String));
            dtName.Columns.Add("IsActive", typeof(System.Boolean));
            dtName.Columns.Add("PC_OrderID", typeof(System.Int32));

            DataColumn[] keys = new DataColumn[1];
            keys[0] = CompId;
            dtName.PrimaryKey = keys;
            return dtName;
        }

    }
    public class T_OMS_TasksMaster {
        public int TaskMasterID { get; set; }
        public string Task { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int? LedgerID { get; set; }
        public int? UnitID { get; set; }
        public decimal Weight { get; set; }
        public decimal TargetQty { get; set; }
        public int? resID { get; set; }

        public T_OMS_TasksMaster() { }
        public T_OMS_TasksMaster(int _TaskMasterID, string _Task, bool _IsActive, int _CreatedBy, DateTime _CreatedOn, int _UpdatedBy
            , DateTime _UpdatedOn, int _LedgerID, int _UnitID, decimal _Weight, decimal _TargetQty, int _resID) {
            TaskMasterID = _TaskMasterID; Task = _Task; IsActive = _IsActive; CreatedBy = _CreatedBy; CreatedOn = _CreatedOn;
            UpdatedBy = _UpdatedBy; UpdatedOn = _UpdatedOn; LedgerID = _LedgerID; UnitID = _UnitID; Weight = _Weight; TargetQty = _TargetQty; resID = _resID;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            DataColumn TaskMasterID = new DataColumn();
            TaskMasterID.DataType = System.Type.GetType("System.Int32");
            TaskMasterID.ColumnName = "TaskMasterID";
            TaskMasterID.AutoIncrement = true;
            dtName.Columns.Add(TaskMasterID);
            dtName.Columns.Add("Task", typeof(System.String));
            dtName.Columns.Add("IsActive", typeof(System.Boolean));
            dtName.Columns.Add("CreatedBy", typeof(System.Int32));
            dtName.Columns.Add("CreatedOn", typeof(System.DateTime));
            dtName.Columns.Add("UpdatedBy", typeof(System.Int32));
            dtName.Columns.Add("UpdatedOn", typeof(System.DateTime));
            dtName.Columns.Add("LedgerID", typeof(System.Int32));
            dtName.Columns.Add("UnitID", typeof(System.Int32));
            dtName.Columns.Add("Weight", typeof(System.Decimal));
            dtName.Columns.Add("TargetQty", typeof(System.Decimal));
            dtName.Columns.Add("resID", typeof(System.Int32));

            DataColumn[] keys = new DataColumn[1];
            keys[0] = TaskMasterID;
            dtName.PrimaryKey = keys;
            return dtName;
        }


    }
    public class T_OMS_ProjectComponents {
        public int PrjCompId { get; set; }
        public string CompName { get; set; }
        public int ProjectID { get; set; }
        public int CompId { get; set; }
        public string Details { get; set; }
        public decimal FromCh { get; set; }
        public decimal ToCh { get; set; }
        public bool IsActive { get; set; }
        public int PC_OrderID { get; set; }
        public int LagDays { get; set; }
        public int Cid { get; set; }

        public T_OMS_ProjectComponents() { }
        public T_OMS_ProjectComponents(int _PrjCompId, string _CompName, int _ProjectID, int _CompId, string _Details,
            decimal _FromCh, decimal _ToCh, bool _IsActive, int _PC_OrderID, int _LagDays, int _Cid) {
            PrjCompId = _PrjCompId; CompName = _CompName; ProjectID = _ProjectID; CompId = _CompId; Details = _Details;
            FromCh = _FromCh; ToCh = _ToCh; IsActive = _IsActive; PC_OrderID = _PC_OrderID; LagDays = _LagDays; Cid = _Cid;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            DataColumn PrjCompId = new DataColumn();
            PrjCompId.DataType = System.Type.GetType("System.Int32");
            PrjCompId.ColumnName = "PrjCompId";
            PrjCompId.AutoIncrement = true;
            dtName.Columns.Add(PrjCompId);
            //dtName.Columns.Add("PrjCompId", typeof(System.Int32));
            dtName.Columns.Add("CompName", typeof(System.String));
            dtName.Columns.Add("ProjectID", typeof(System.Int32));
            dtName.Columns.Add("CompId", typeof(System.Int32));
            dtName.Columns.Add("Details", typeof(System.String));
            dtName.Columns.Add("FromCh", typeof(System.Double));
            dtName.Columns.Add("ToCh", typeof(System.Double));
            dtName.Columns.Add("IsActive", typeof(System.Boolean));
            dtName.Columns.Add("PC_OrderID", typeof(System.Int32));
            dtName.Columns.Add("LagDays", typeof(System.Int32));
            dtName.Columns.Add("Cid", typeof(System.Int32));

            DataColumn[] keys = new DataColumn[1];
            keys[0] = PrjCompId;
            dtName.PrimaryKey = keys;
            return dtName;
        }
    }

    public class T_OMS_SubTasks {
        public int SubTaskID { get; set; }
        public int TaskID { get; set; }
        public string SubTaskName { get; set; }
        public decimal QTY { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int SubSecId { get; set; }
        public string Remarks { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public decimal Nos { get; set; }
        public decimal B { get; set; }
        public decimal D { get; set; }
        public decimal L { get; set; }
        public decimal FromChainage { get; set; }
        public decimal ToChainage { get; set; }
        public bool IsActive { get; set; }
        public int PrjCompId { get; set; }
        public int? TempCompID { get; set; }
        public int? TempEleID { get; set; }
        public bool IsPBAllow { get; set; }
        public string Sub_Pred { get; set; }
        public string Sub_Sucs { get; set; }
        public int? Dur { get; set; }
        public string RefCode { get; set; }
        public int? stPCID { get; set; }

        public T_OMS_SubTasks() { }
        public T_OMS_SubTasks(int _SubTaskID, int _TaskID, string _SubTaskName, decimal _QTY, DateTime _StartDate, DateTime _EndDate
            , int _SubSecId, string _Remarks, DateTime _CreatedOn, int _CreatedBy, DateTime _UpdatedOn, int _UpdatedBy, decimal _Nos
            , decimal _B, decimal _D, decimal _L, decimal _FromChainage, decimal _ToChainage, bool _IsActive, int _PrjCompId
            , int? _TempCompID, int? _TempEleID, bool _IsPBAllow, string _Sub_Pred, string _Sub_Sucs, int? _Dur, string _RefCode, int? _stPCID) {
            SubTaskID = _SubTaskID; TaskID = _TaskID; SubTaskName = _SubTaskName; QTY = _QTY; StartDate = _StartDate; EndDate = _EndDate;
            SubSecId = _SubSecId; Remarks = _Remarks; CreatedOn = _CreatedOn; CreatedBy = _CreatedBy; UpdatedOn = _UpdatedOn;
            UpdatedBy = _UpdatedBy; Nos = _Nos; B = _B; D = _D; L = _L; FromChainage = _FromChainage; ToChainage = _ToChainage;
            IsActive = _IsActive; PrjCompId = _PrjCompId; TempCompID = _TempCompID; TempEleID = _TempEleID; IsPBAllow = _IsPBAllow;
            Sub_Pred = _Sub_Pred; Sub_Sucs = _Sub_Sucs; Dur = _Dur; RefCode = _RefCode; stPCID = _stPCID;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            DataColumn SubTaskID = new DataColumn();
            SubTaskID.DataType = System.Type.GetType("System.Int32");
            SubTaskID.ColumnName = "SubTaskID";
            SubTaskID.AutoIncrement = true;
            dtName.Columns.Add(SubTaskID);
            //dtName.Columns.Add("SubTaskID", typeof(System.Int32));
            dtName.Columns.Add("TaskID", typeof(System.Int32));
            dtName.Columns.Add("SubTaskName", typeof(System.String));
            dtName.Columns.Add("QTY", typeof(System.Double));
            dtName.Columns.Add("StartDate", typeof(System.DateTime));
            dtName.Columns.Add("EndDate", typeof(System.DateTime));
            dtName.Columns.Add("SubSecId", typeof(System.Int32));
            dtName.Columns.Add("Remarks", typeof(System.String));
            dtName.Columns.Add("CreatedOn", typeof(System.DateTime));
            dtName.Columns.Add("CreatedBy", typeof(System.Int32));
            dtName.Columns.Add("UpdatedOn", typeof(System.DateTime));
            dtName.Columns.Add("UpdatedBy", typeof(System.Int32));
            dtName.Columns.Add("Nos", typeof(System.Double));
            dtName.Columns.Add("B", typeof(System.Double));
            dtName.Columns.Add("D", typeof(System.Double));
            dtName.Columns.Add("L", typeof(System.Double));
            dtName.Columns.Add("FromChainage", typeof(System.Double));
            dtName.Columns.Add("ToChainage", typeof(System.Double));
            dtName.Columns.Add("IsActive", typeof(System.Boolean));
            dtName.Columns.Add("PrjCompId", typeof(System.Int32));
            dtName.Columns.Add("TempCompID", typeof(System.Int32)).AllowDBNull = true;
            dtName.Columns.Add("TempEleID", typeof(System.Int32)).AllowDBNull = true;
            dtName.Columns.Add("IsPBAllow", typeof(System.Boolean));
            dtName.Columns.Add("Sub_Pred", typeof(System.String));
            dtName.Columns.Add("Sub_Sucs", typeof(System.String));
            dtName.Columns.Add("Dur", typeof(System.Int32)).AllowDBNull = true;
            dtName.Columns.Add("RefCode", typeof(System.String));
            dtName.Columns.Add("stPCID", typeof(System.Int32)).AllowDBNull = true;

            DataColumn[] keys = new DataColumn[1];
            keys[0] = SubTaskID;
            dtName.PrimaryKey = keys;
            return dtName;
        }
    }
    public class baseLines {
        public int bID { get; set; }
        public string Baseline { get; set; }
        public int PID { get; set; }
        public baseLines() { }        public baseLines(int _bID, string _Baseline, int _PID) {            bID = _bID; Baseline = _Baseline; PID = _PID;
        }    }
    public class T_OMS_BL_Tasks {
        public int BID { get; set; }
        public int TaskID { get; set; }
        public int ProjectID { get; set; }
        public string TaskRefNo { get; set; }
        public string TaskName { get; set; }
        public string TaskDesc { get; set; }
        public int Unit { get; set; }
        public int SectionID { get; set; }
        public decimal Quantity { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal RaQty { get; set; }
        public int IsActive { get; set; }
        public decimal TotalCost { get; set; }
        public decimal AdFixed { get; set; }
        public decimal Adruning { get; set; }
        public decimal OP { get; set; }
        public decimal TaskCost { get; set; }

        public T_OMS_BL_Tasks() { }
        public T_OMS_BL_Tasks(int _BID, int _TaskID, int _ProjectID, string _TaskRefNo, string _TaskName, string _TaskDesc
            , int _Unit, int _SectionID, decimal _Quantity, DateTime _StartDate, DateTime _EndDate, decimal _RaQty
            , int _IsActive, decimal _TotalCost, decimal _AdFixed, decimal _Adruning, decimal _OP, decimal _TaskCost) {
            BID = _BID; TaskID = _TaskID; ProjectID = _ProjectID; TaskRefNo = _TaskRefNo; TaskName = _TaskName; TaskDesc = _TaskDesc; Unit = _Unit; SectionID = _SectionID; Quantity = _Quantity; StartDate = _StartDate; EndDate = _EndDate; RaQty = _RaQty; IsActive = _IsActive; TotalCost = _TotalCost; AdFixed = _AdFixed; Adruning = _Adruning; OP = _OP; TaskCost = _TaskCost;
        }
    }
    public class T_OMS_BL_Assignments {
        public int BID { get; set; }
        public int AssignMent { get; set; }
        public int TaskID { get; set; }
        public int ResourceID { get; set; }
        public decimal Coeff { get; set; }
        public string Remarks { get; set; }
        public int IsActive { get; set; }
        public int LeadResID { get; set; }
        public decimal Wastage { get; set; }

        public T_OMS_BL_Assignments() { }
        public T_OMS_BL_Assignments(int _BID, int _AssignMent, int _TaskID, int _ResourceID, decimal _Coeff, string _Remarks
            , int _IsActive, int _LeadResID, decimal _Wastage) {
            BID = _BID; AssignMent = _AssignMent; TaskID = _TaskID; ResourceID = _ResourceID; Coeff = _Coeff; Remarks = _Remarks;
            IsActive = _IsActive; LeadResID = _LeadResID; Wastage = _Wastage;
        }
    }
    public class to_stplans {
        public int id { get; set; }
        public int stid { get; set; }
        public string SubTaskName { get; set; }
        public int plDtID { get; set; }
        public DateTime date { get; set; }
        public int ia { get; set; }
        public int? cb { get; set; }
        public DateTime? co { get; set; }
        public int? mb { get; set; }
        public DateTime? mo { get; set; }
        public decimal PlQty { get; set; }
        public int? UnPlanID { get; set; }

        public to_stplans() { }
        public to_stplans(int _id, int _stid, string _name, int _plDtID,DateTime _date, int _ia, int? _cb, DateTime? _co, int? _mb, DateTime? _mo, decimal _PlQty, int? _UnPlanID) {
            id = _id; stid = _stid; SubTaskName = _name; plDtID = _plDtID; date = _date; ia = _ia; cb = _cb; co = _co; mb = _mb; mo = _mo; PlQty = _PlQty; UnPlanID = _UnPlanID;
        }
        public to_stplans(int _id, int _stid,string _name, int _plDtID, decimal _PlQty, int? _UnPlanID) {
            id = _id; stid = _stid; SubTaskName = _name; plDtID = _plDtID;  PlQty = _PlQty; UnPlanID = _UnPlanID;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            DataColumn id = new DataColumn();
            id.DataType = System.Type.GetType("System.Int32");
            id.ColumnName = "id";
            id.AutoIncrement = true;
            dtName.Columns.Add(id);
            // dtName.Columns.Add("id", typeof(System.Int32));
            dtName.Columns.Add("stid", typeof(System.Int32));
            dtName.Columns.Add("SubTaskName", typeof(System.String));
            dtName.Columns.Add("plDtID", typeof(System.Int32));
            dtName.Columns.Add("date", typeof(System.DateTime));
            dtName.Columns.Add("ia", typeof(System.Int32));
            dtName.Columns.Add("cb", typeof(System.Int32));
            dtName.Columns.Add("co", typeof(System.DateTime));
            dtName.Columns.Add("mb", typeof(System.Int32));
            dtName.Columns.Add("mo", typeof(System.DateTime));
            dtName.Columns.Add("PlQty", typeof(System.Double));
            dtName.Columns.Add("UnPlanID", typeof(System.Int32));

            DataColumn[] keys = new DataColumn[1];
            keys[0] = id;
            dtName.PrimaryKey = keys;
            return dtName;
        }
        public DataTable dtClass2(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("id", typeof(System.Int32));
            dtName.Columns.Add("stid", typeof(System.Int32));
            dtName.Columns.Add("SubTaskName", typeof(System.String));
            dtName.Columns.Add("plDtID", typeof(System.Int32));
            dtName.Columns.Add("PlQty", typeof(System.Double));
            dtName.Columns.Add("UnPlanID", typeof(System.Int32));
            return dtName;
        }
    }
    public class vg_Date {
        public int DateID { get; set; }
        public DateTime Date { get; set; }
        public int Day { get; set; }
        public int WkNo { get; set; }
        public string WkNm { get; set; }
        public string WkDay { get; set; }
        public int MoNo { get; set; }
        public string MoNm { get; set; }
        public int QrNo { get; set; }
        public string QrNm { get; set; }
        public int YrNo { get; set; }
        public string YrNm { get; set; }

        public vg_Date() { }
        public vg_Date(int _DateID, DateTime _Date, int _Day, int _WkNo, string _WkNm, string _WkDay, int _MoNo
            , string _MoNm, int _QrNo, string _QrNm, int _YrNo, string _YrNm) {
            DateID = _DateID; Date = _Date; Day = _Day; WkNo = _WkNo; WkNm = _WkNm; WkDay = _WkDay;
            MoNo = _MoNo; MoNm = _MoNm; QrNo = _QrNo; QrNm = _QrNm; YrNo = _YrNo; YrNm = _YrNm;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("DateID", typeof(System.Int32));
            dtName.Columns.Add("Date", typeof(System.DateTime));
            dtName.Columns.Add("Day", typeof(System.Int16));
            dtName.Columns.Add("WkNo", typeof(System.Int16));
            dtName.Columns.Add("WkNm", typeof(System.String));
            dtName.Columns.Add("WkDay", typeof(System.String));
            dtName.Columns.Add("MoNo", typeof(System.Byte));
            dtName.Columns.Add("MoNm", typeof(System.String));
            dtName.Columns.Add("QrNo", typeof(System.Byte));
            dtName.Columns.Add("QrNm", typeof(System.String));
            dtName.Columns.Add("YrNo", typeof(System.Int16));
            dtName.Columns.Add("YrNm", typeof(System.String));
            return dtName;
        }
    }
    public class IDDt {
        public int DateID { get; set; }
        public DateTime Date { get; set; }
        public IDDt() { }
        public IDDt(int _DateID, DateTime _Date) { DateID = _DateID; Date = _Date; }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("DateID", typeof(System.Int32));
            dtName.Columns.Add("Date", typeof(System.DateTime));
            return dtName;
        }
    }
    public class T_OMS_MRPStatges {
        public int mrpStageID { get; set; }
        public string StageName { get; set; }
        public T_OMS_MRPStatges() { }
        public T_OMS_MRPStatges(int _mrpStageID, string _StageName) {
            mrpStageID = _mrpStageID; StageName = _StageName;
        }
    }
}
