﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Login.Controllers.OMS {
    public class oDAL {
        public static async Task<string> CreateMasterComponentsAsync(List<T_OMS_Components> clT_OMS_Components, int cid) {
            try {
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("createComps"));
                    T_OMS_Components t = new T_OMS_Components();
                    DataTable dtClass = t.dtClass("dtClass");
                    int i = 0; //CONSIDER CHANGING THE AUTOINCREMENTING TABLE ID FIELD TO THIS VARIABLE
                    foreach(T_OMS_Components E in clT_OMS_Components) {
                        DataRow dr = dtClass.NewRow();
                        dr["CompId"] = i++;
                        dr["Component"] = E.Component;
                        dr["Specification"] = E.Specification;
                        dr["IsActive"] = E.IsActive;
                        dr["PC_OrderID"] = E.PC_OrderID;
                        dtClass.Rows.Add(dr);
                    }
                    using(SqlBulkCopy bcp = new SqlBulkCopy(con, SqlBulkCopyOptions.Default, tran)) {
                        bcp.DestinationTableName = "[dbo].[T_OMS_Components]";
                        try {
                            await bcp.WriteToServerAsync(dtClass);
                            tran.Commit();
                            con.Close();
                            return clT_OMS_Components.Count.ToString() + " record(s) inserted!";
                        } catch(Exception ex) {
                            tran.Rollback();
                            con.Close();
                            return "failed " + ex.Message;
                        }
                    }
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
        public static async Task<List<T_OMS_Components>> ReadT_OMS_ComponentsAsync(int cid) {
            List<T_OMS_Components> clT_OMS_Components = new List<T_OMS_Components>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "select t1.* from T_OMS_Components t1";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    //cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            T_OMS_Components t = new T_OMS_Components();
                            t.CompId = rdr["CompId"] == DBNull.Value ? -1 : (int)rdr["CompId"];
                            t.Component = rdr["Component"] == DBNull.Value ? "" : rdr["Component"].ToString();
                            t.Specification = rdr["Specification"] == DBNull.Value ? "" : rdr["Specification"].ToString();
                            t.IsActive = rdr["IsActive"] == DBNull.Value ? false : (bool)rdr["IsActive"];
                            t.PC_OrderID = rdr["PC_OrderID"] == DBNull.Value ? -1 : (int)rdr["PC_OrderID"];
                            clT_OMS_Components.Add(t);
                        }
                    }
                }
                con.Close();
                return clT_OMS_Components;
            }
        }
        public static async Task<string> UpdateT_OMS_ComponentsAsync(List<T_OMS_Components> clT_OMS_Components) {
            try {
                DataTable dtClass = new intString().dtClass("dtClass");
                foreach(T_OMS_Components E in clT_OMS_Components) {
                    DataRow dr = dtClass.NewRow();
                    dr["id"] = E.CompId;
                    dr["str"] = E.Component;
                    dtClass.Rows.Add(dr);
                }
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("blkUpdate"));
                    string query = "sh_globalCompUpdate";
                    int recordsUpdated = 0;
                    using(SqlCommand cmd = new SqlCommand(query, con, tran)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@tblType", dtClass));
                        recordsUpdated = await cmd.ExecuteNonQueryAsync();
                    }
                    if(recordsUpdated > 0) tran.Commit();
                    else return "failed to update!";
                    con.Close();
                    return recordsUpdated.ToString() + " records(s) updated";
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
        public static async Task<string> DeleteT_OMS_ComponentsAsync(List<int> ids) {
            try {
                DataTable dt = new DataTable();
                dt.Columns.Add("id", typeof(System.Int32));
                foreach(int t in ids) {
                    DataRow dr = dt.NewRow();
                    dr["id"] = t;
                    dt.Rows.Add(dr);
                }
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("blkUpdate"));
                    string query = "sh_globalCompDelete";
                    int recordsUpdated = 0;
                    using(SqlCommand cmd = new SqlCommand(query, con, tran)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@tblType", dt));
                        recordsUpdated = await cmd.ExecuteNonQueryAsync();
                    }
                    if(recordsUpdated > 0) tran.Commit();
                    else return "failed to update!";
                    con.Close();
                    return recordsUpdated.ToString() + " records(s) deleted";
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
        public static async Task<string> CreateTasksMasterAsync(List<T_OMS_TasksMaster> clT_OMS_TasksMaster, int userID) {
            try {
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("createT_OMS_TasksMaster"));
                    T_OMS_TasksMaster t = new T_OMS_TasksMaster();
                    DataTable dtClass = t.dtClass("dtClass");
                    int i = 0; //CONSIDER CHANGING THE AUTOINCREMENTING TABLE ID FIELD TO THIS VARIABLE
                    foreach(T_OMS_TasksMaster E in clT_OMS_TasksMaster) {
                        DataRow dr = dtClass.NewRow();
                        dr["TaskMasterID"] = i++;
                        dr["Task"] = E.Task;
                        dr["IsActive"] = 1;
                        dr["CreatedBy"] = userID;
                        dr["CreatedOn"] = DateTime.Now;
                        dr["LedgerID"] = DBNull.Value;
                        dr["UnitID"] = DBNull.Value;
                        dr["Weight"] = 0;
                        dr["TargetQty"] = 0;
                        dr["resID"] = DBNull.Value;
                        dtClass.Rows.Add(dr);
                    }
                    using(SqlBulkCopy bcp = new SqlBulkCopy(con, SqlBulkCopyOptions.Default, tran)) {
                        bcp.DestinationTableName = "[dbo].[T_OMS_TasksMaster]";
                        try {
                            await bcp.WriteToServerAsync(dtClass);
                            tran.Commit();
                            con.Close();
                            return clT_OMS_TasksMaster.Count.ToString() + " record(s) inserted!";
                        } catch(Exception ex) {
                            tran.Rollback();
                            con.Close();
                            return "failed " + ex.Message;
                        }
                    }
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
        public static async Task<List<T_OMS_TasksMaster>> ReadTasksMasterAsync() {
            List<T_OMS_TasksMaster> clT_OMS_TasksMaster = new List<T_OMS_TasksMaster>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "select t1.* from T_OMS_TasksMaster t1";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    //cmd.CommandType = CommandType.StoredProcedure;
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            T_OMS_TasksMaster t = new T_OMS_TasksMaster();
                            t.TaskMasterID = rdr["TaskMasterID"] == DBNull.Value ? -1 : (int)rdr["TaskMasterID"];
                            t.Task = rdr["Task"] == DBNull.Value ? "" : rdr["Task"].ToString();
                            t.IsActive = rdr["IsActive"] == DBNull.Value ? false : (bool)rdr["IsActive"];
                            t.CreatedBy = rdr["CreatedBy"] == DBNull.Value ? -1 : (int)rdr["CreatedBy"];
                            t.CreatedOn = rdr["CreatedOn"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["CreatedOn"]);
                            t.UpdatedBy = rdr["UpdatedBy"] == DBNull.Value ? -1 : (int)rdr["UpdatedBy"];
                            t.UpdatedOn = rdr["UpdatedOn"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["UpdatedOn"]);
                            t.LedgerID = rdr["LedgerID"] == DBNull.Value ? -1 : (int)rdr["LedgerID"];
                            t.UnitID = rdr["UnitID"] == DBNull.Value ? -1 : (int)rdr["UnitID"];
                            t.Weight = rdr["Weight"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["Weight"]);
                            t.TargetQty = rdr["TargetQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["TargetQty"]);
                            t.resID = rdr["resID"] == DBNull.Value ? -1 : (int)rdr["resID"];
                            clT_OMS_TasksMaster.Add(t);
                        }
                    }
                }
                con.Close();
                return clT_OMS_TasksMaster;
            }
        }
        public static async Task<string> UpdateTasksMasterAsync(List<T_OMS_TasksMaster> clT_OMS_TasksMaster) {
            try {
                DataTable dtClass = new intString().dtClass("dtClass");
                foreach(T_OMS_TasksMaster E in clT_OMS_TasksMaster) {
                    DataRow dr = dtClass.NewRow();
                    dr["id"] = E.TaskMasterID;
                    dr["str"] = E.Task;
                    dtClass.Rows.Add(dr);
                }
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("blkUpdate"));
                    string query = "sh_TasksMasterUpdate";
                    int recordsUpdated = 0;
                    using(SqlCommand cmd = new SqlCommand(query, con, tran)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@tblType", dtClass));
                        recordsUpdated = await cmd.ExecuteNonQueryAsync();
                    }
                    if(recordsUpdated > 0) tran.Commit();
                    else return "failed to update!";
                    con.Close();
                    return recordsUpdated.ToString() + " records(s) updated";
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
        public static async Task<string> DeleteTasksMasterAsync(List<int> ids) {
            try {
                DataTable dt = new DataTable();
                dt.Columns.Add("id", typeof(System.Int32));
                foreach(int t in ids) {
                    DataRow dr = dt.NewRow();
                    dr["id"] = t;
                    dt.Rows.Add(dr);
                }
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("blkUpdate"));
                    string query = "sh_taskMasterDelete";
                    int recordsUpdated = 0;
                    using(SqlCommand cmd = new SqlCommand(query, con, tran)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@tblType", dt));
                        recordsUpdated = await cmd.ExecuteNonQueryAsync();
                    }
                    if(recordsUpdated > 0) tran.Commit();
                    else return "failed to update!";
                    con.Close();
                    return recordsUpdated.ToString() + " records(s) deleted";
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }        public static async Task<List<T_OMS_ProjectComponents>> ReadProjectComponentsAsync(int pid) {
            List<T_OMS_ProjectComponents> clProjectComponents = new List<T_OMS_ProjectComponents>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "select t1.* from T_OMS_ProjectComponents t1 where t1.ProjectID = @pid";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    //cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pid", pid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            T_OMS_ProjectComponents t = new T_OMS_ProjectComponents();
                            t.PrjCompId = rdr["PrjCompId"] == DBNull.Value ? -1 : (int)rdr["PrjCompId"];
                            t.CompName = rdr["CompName"] == DBNull.Value ? "" : rdr["CompName"].ToString();
                            t.ProjectID = rdr["ProjectID"] == DBNull.Value ? -1 : (int)rdr["ProjectID"];
                            t.CompId = rdr["CompId"] == DBNull.Value ? -1 : (int)rdr["CompId"];
                            t.Details = rdr["Details"] == DBNull.Value ? "" : rdr["Details"].ToString();
                            t.FromCh = rdr["FromCh"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["FromCh"]);
                            t.ToCh = rdr["ToCh"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["ToCh"]);
                            t.IsActive = rdr["IsActive"] == DBNull.Value ? false : (bool)rdr["IsActive"];
                            t.PC_OrderID = rdr["PC_OrderID"] == DBNull.Value ? -1 : (int)rdr["PC_OrderID"];
                            t.LagDays = rdr["LagDays"] == DBNull.Value ? -1 : (int)rdr["LagDays"];
                            t.Cid = rdr["Cid"] == DBNull.Value ? -1 : (int)rdr["Cid"];
                            clProjectComponents.Add(t);
                        }
                    }
                }
                con.Close();
                return clProjectComponents;
            }
        }
        public static async Task<string> CreateProjectComponentsAsync(List<T_OMS_ProjectComponents> clProjectComponents, int pid) {
            try {
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("createT_OMS_ProjectComponents"));
                    T_OMS_ProjectComponents t = new T_OMS_ProjectComponents();
                    DataTable dtClass = t.dtClass("dtClass");
                    int i = 0;// CONSIDER CHANGING THE AUTOINCREMENTING TABLE ID FIELD TO THIS VARIABLE
                    foreach(T_OMS_ProjectComponents E in clProjectComponents) {
                        DataRow dr = dtClass.NewRow();
                        dr["PrjCompId"] = i++;
                        dr["CompName"] = E.CompName;
                        dr["ProjectID"] = pid;
                        dr["CompId"] = E.CompId;
                        dr["Details"] = E.Details;
                        dr["FromCh"] = E.FromCh;
                        dr["ToCh"] = E.ToCh;
                        dr["IsActive"] = E.IsActive;
                        dr["PC_OrderID"] = E.PC_OrderID;
                        dr["LagDays"] = E.LagDays;
                        dr["Cid"] = E.Cid;
                        dtClass.Rows.Add(dr);
                    }
                    using(SqlBulkCopy bcp = new SqlBulkCopy(con, SqlBulkCopyOptions.Default, tran)) {
                        bcp.DestinationTableName = "[dbo].[T_OMS_ProjectComponents]";
                        try {
                            await bcp.WriteToServerAsync(dtClass);
                            tran.Commit();
                            con.Close();
                            return clProjectComponents.Count.ToString() + " record(s) inserted!";
                        } catch(Exception ex) {
                            tran.Rollback();
                            con.Close();
                            return "failed " + ex.Message;
                        }
                    }
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
        public static async Task<string> UpdateProjectComponentsAsync(List<T_OMS_ProjectComponents> clProjectComponents) {
            try {
                DataTable dtClass = new T_OMS_ProjectComponents().dtClass("dtClass");
                foreach(T_OMS_ProjectComponents E in clProjectComponents) {
                    DataRow dr = dtClass.NewRow();
                    dr["PrjCompId"] = E.PrjCompId;
                    dr["CompName"] = E.CompName;
                    dr["ProjectID"] = E.ProjectID;
                    dr["CompId"] = E.CompId;
                    dr["Details"] = E.Details;
                    dr["FromCh"] = E.FromCh;
                    dr["ToCh"] = E.ToCh;
                    dr["IsActive"] = E.IsActive;
                    dr["PC_OrderID"] = E.PC_OrderID;
                    dr["LagDays"] = E.LagDays;
                    dr["Cid"] = E.Cid;
                    dtClass.Rows.Add(dr);
                }
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("blkUpdate"));
                    string query = "sh_ProjectComponentsUpdate";
                    int recordsUpdated = 0;
                    using(SqlCommand cmd = new SqlCommand(query, con, tran)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@tblType", dtClass));
                        recordsUpdated = await cmd.ExecuteNonQueryAsync();
                    }
                    if(recordsUpdated > 0) tran.Commit();
                    else return "failed to update!";
                    con.Close();
                    return recordsUpdated.ToString() + " records(s) updated";
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
        public static async Task<string> DeleteProjectComponentsAsync(List<int> ids) {
            try {
                DataTable dt = new DataTable();
                dt.Columns.Add("id", typeof(System.Int32));
                foreach(int t in ids) {
                    DataRow dr = dt.NewRow();
                    dr["id"] = t;
                    dt.Rows.Add(dr);
                }
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("blkUpdate"));
                    string query = "sh_ProjectComponentsDelete";
                    int recordsUpdated = 0;
                    using(SqlCommand cmd = new SqlCommand(query, con, tran)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@tblType", dt));
                        recordsUpdated = await cmd.ExecuteNonQueryAsync();
                    }
                    if(recordsUpdated > 0) tran.Commit();
                    else return "failed to update!";
                    con.Close();
                    return recordsUpdated.ToString() + " records(s) deleted";
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
        //-------------------------------
        public static async Task<Tuple<int, List<T_OMS_SubTasks>>> ReadSubTasksAsync(int pid, int? tid, int mrp, int cp, int ps) {
            List<T_OMS_SubTasks> subTasks = new List<T_OMS_SubTasks>();
            int totalRecords = 0;
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "oms.subTasksRead";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pid", pid));
                    cmd.Parameters.Add(new SqlParameter("@tid", (object)tid ?? DBNull.Value)); //null-coalescing operator It returns the left-hand operand if the operand is not null; otherwise, it returns the right operand
                    cmd.Parameters.Add(new SqlParameter("@mrp", mrp));
                    cmd.Parameters.Add(new SqlParameter("@cp", cp));
                    cmd.Parameters.Add(new SqlParameter("@ps", ps));
                    cmd.Parameters.Add(new SqlParameter("@tr", SqlDbType.Int)).Direction = ParameterDirection.Output;
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            T_OMS_SubTasks t = new T_OMS_SubTasks();
                            t.SubTaskID = rdr["SubTaskID"] == DBNull.Value ? -1 : (int)rdr["SubTaskID"];
                            t.TaskID = rdr["TaskID"] == DBNull.Value ? -1 : (int)rdr["TaskID"];
                            t.SubTaskName = rdr["SubTaskName"] == DBNull.Value ? "" : rdr["SubTaskName"].ToString();
                            t.QTY = rdr["QTY"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["QTY"]);
                            t.StartDate = rdr["StartDate"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["StartDate"]);
                            t.EndDate = rdr["EndDate"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["EndDate"]);
                            t.SubSecId = rdr["SubSecId"] == DBNull.Value ? -1 : (int)rdr["SubSecId"];
                            //t.Remarks = rdr["Remarks"] == DBNull.Value ? "" : rdr["Remarks"].ToString();
                            //t.CreatedOn = rdr["CreatedOn"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["CreatedOn"]);
                            //t.CreatedBy = rdr["CreatedBy"] == DBNull.Value ? -1 : (int)rdr["CreatedBy"];
                            //t.UpdatedOn = rdr["UpdatedOn"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["UpdatedOn"]);
                            //t.UpdatedBy = rdr["UpdatedBy"] == DBNull.Value ? -1 : (int)rdr["UpdatedBy"];
                            t.Nos = rdr["Nos"] == DBNull.Value ? 1 : Convert.ToDecimal(rdr["Nos"]);
                            t.B = rdr["B"] == DBNull.Value ? 1 : Convert.ToDecimal(rdr["B"]);
                            t.D = rdr["D"] == DBNull.Value ? 1 : Convert.ToDecimal(rdr["D"]);
                            t.L = rdr["L"] == DBNull.Value ? 1 : Convert.ToDecimal(rdr["L"]);
                            t.FromChainage = rdr["FromChainage"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["FromChainage"]);
                            t.ToChainage = rdr["ToChainage"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["ToChainage"]);
                            t.IsActive = rdr["IsActive"] == DBNull.Value ? false : (bool)rdr["IsActive"];
                            t.PrjCompId = rdr["PrjCompId"] == DBNull.Value ? -1 : (int)rdr["PrjCompId"];
                            //t.TempCompID = rdr["TempCompID"] == DBNull.Value ? -1 : (int)rdr["TempCompID"];
                            //t.TempEleID = rdr["TempEleID"] == DBNull.Value ? -1 : (int)rdr["TempEleID"];
                            t.IsPBAllow = rdr["IsPBAllow"] == DBNull.Value ? false : (bool)rdr["IsPBAllow"];
                            //t.Sub_Pred = rdr["Sub_Pred"] == DBNull.Value ? "" : rdr["Sub_Pred"].ToString();
                            //t.Sub_Sucs = rdr["Sub_Sucs"] == DBNull.Value ? "" : rdr["Sub_Sucs"].ToString();
                            //t.Dur = rdr["Dur"] == DBNull.Value ? -1 : (int)rdr["Dur"];
                            //t.RefCode = rdr["RefCode"] == DBNull.Value ? "" : rdr["RefCode"].ToString();
                            //t.stPCID = rdr["stPCID"] == DBNull.Value ? -1 : (int)rdr["stPCID"];
                            subTasks.Add(t);
                        }
                    }
                    totalRecords = (int)cmd.Parameters["@tr"].Value;
                }
                con.Close();
                return new Tuple<int, List<T_OMS_SubTasks>>(totalRecords, subTasks);

            }
        }
        public static async Task<string> CreateSubTasksAsync(List<T_OMS_SubTasks> clSubTasks, int tid, int userID) {
            try {
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("createSubTasks"));
                    T_OMS_SubTasks t = new T_OMS_SubTasks();
                    DataTable dtClass = t.dtClass("dtClass");
                    int i = 0;
                    foreach(T_OMS_SubTasks st in clSubTasks) {
                        DataRow dr = dtClass.NewRow();
                        dr["SubTaskID"] = i++;
                        dr["TaskID"] = tid;
                        dr["SubTaskName"] = st.SubTaskName;
                        dr["QTY"] = (st.Nos == 0 ? 1 : st.Nos) * (st.L == 0 ? 1 : st.L) * (st.B == 0 ? 1 : st.B) * (st.D == 0 ? 1 : st.D);
                        dr["StartDate"] = st.StartDate;
                        dr["EndDate"] = st.EndDate;
                        dr["SubSecId"] = 10; // with ref to table T_OMS_MRPStatges stage  = 10 just created
                        dr["Remarks"] = st.Remarks;
                        dr["CreatedOn"] = DateTime.Now;
                        dr["CreatedBy"] = userID;
                        dr["UpdatedOn"] = DBNull.Value;
                        dr["UpdatedBy"] = DBNull.Value;
                        dr["Nos"] = st.Nos == 0 ? 1 : st.Nos;
                        dr["B"] = st.B == 0 ? 1 : st.B;
                        dr["D"] = st.D == 0 ? 1 : st.D;
                        dr["L"] = st.L == 0 ? 1 : st.L;
                        dr["FromChainage"] = st.FromChainage;
                        dr["ToChainage"] = st.ToChainage;
                        dr["IsActive"] = st.IsActive;
                        dr["PrjCompId"] = st.PrjCompId;
                        dr["TempCompID"] = (object)st.TempCompID ?? DBNull.Value;
                        dr["TempEleID"] = (object)st.TempEleID ?? DBNull.Value;
                        dr["IsPBAllow"] = st.IsPBAllow;
                        dr["Sub_Pred"] = st.Sub_Pred;
                        dr["Sub_Sucs"] = st.Sub_Sucs;
                        dr["Dur"] = (object)st.Dur ?? DBNull.Value;
                        dr["RefCode"] = st.RefCode;
                        dr["stPCID"] = (object)st.stPCID ?? DBNull.Value;
                        dtClass.Rows.Add(dr);
                    }
                    using(SqlBulkCopy bcp = new SqlBulkCopy(con, SqlBulkCopyOptions.Default, tran)) {
                        bcp.DestinationTableName = "[dbo].[T_OMS_SubTasks]";
                        try {
                            await bcp.WriteToServerAsync(dtClass);
                            List<int> StIDs = await Task.Run(() => getInsertedSTIDs(con, tran));
                            DataTable dt = common.idList2Table(StIDs);
                            string query = "oms.mrpBlSTCreate";
                            int recordsUpdated = 0;
                            using(SqlCommand cmd = new SqlCommand(query, con, tran)) {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add(new SqlParameter("@stIDs", dt));
                                recordsUpdated = await cmd.ExecuteNonQueryAsync();
                            }
                            tran.Commit();
                            con.Close();
                            return clSubTasks.Count.ToString() + " record(s) inserted!";
                        } catch(Exception ex) {
                            tran.Rollback();
                            con.Close();
                            return "failed " + ex.Message;
                        }
                    }
                    //insert into T_OMS_MRP_BL_SubTasks(MRP_STID,BID_STID,MRP_ID_ST) select scope_identity(),0,10,0	
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
        private static async Task<List<int>> getInsertedSTIDs(SqlConnection conn, SqlTransaction tran) {
            SqlCommand cmd = new SqlCommand("select SubTaskID from T_OMS_SubTasks st left join T_OMS_MRP_BL_SubTasks BL " +
                        "ON BL.MRP_STID=ST.SubTaskID WHERE BL.MRP_STID IS  NULL", conn, tran);
            List<int> StIDs = new List<int>();
            using(SqlDataReader reader = await cmd.ExecuteReaderAsync()) {
                while(await reader.ReadAsync()) {
                    StIDs.Add((int)reader[0]);
                }
            }
            return StIDs;
        }
        public static async Task<string> UpdateSubTasksAsync(List<T_OMS_SubTasks> clSubTasks, int userID) {
            try {
                DataTable dtClass = new T_OMS_SubTasks().dtClass("dtClass");
                foreach(T_OMS_SubTasks E in clSubTasks) {
                    DataRow dr = dtClass.NewRow();
                    dr["SubTaskID"] = E.SubTaskID;
                    dr["TaskID"] = E.TaskID;
                    dr["SubTaskName"] = E.SubTaskName;
                    dr["QTY"] = E.Nos * E.L * E.B * E.D;
                    dr["StartDate"] = E.StartDate;
                    dr["EndDate"] = E.EndDate;
                    dr["SubSecId"] = E.SubSecId;
                    dr["Remarks"] = E.Remarks;
                    dr["CreatedOn"] = DBNull.Value;
                    dr["CreatedBy"] = DBNull.Value;
                    dr["UpdatedOn"] = DateTime.Now;
                    dr["UpdatedBy"] = userID;
                    dr["Nos"] = E.Nos;
                    dr["B"] = E.B;
                    dr["D"] = E.D;
                    dr["L"] = E.L;
                    dr["FromChainage"] = E.FromChainage;
                    dr["ToChainage"] = E.ToChainage;
                    dr["IsActive"] = E.IsActive;
                    dr["PrjCompId"] = E.PrjCompId;
                    dr["TempCompID"] = (object)E.TempCompID ?? DBNull.Value;
                    dr["TempEleID"] = (object)E.TempEleID ?? DBNull.Value;
                    dr["IsPBAllow"] = E.IsPBAllow;
                    dr["Sub_Pred"] = E.Sub_Pred;
                    dr["Sub_Sucs"] = E.Sub_Sucs;
                    dr["Dur"] = (object)E.Dur ?? DBNull.Value;
                    dr["RefCode"] = E.RefCode;
                    dr["stPCID"] = (object)E.stPCID ?? DBNull.Value;
                    dtClass.Rows.Add(dr);
                }
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("blkUpdate"));
                    string query = "oms.SubTasksUpdate";
                    int recordsUpdated = 0;
                    using(SqlCommand cmd = new SqlCommand(query, con, tran)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@tblType", dtClass));
                        cmd.Parameters.Add(new SqlParameter("@userID", userID));
                        recordsUpdated = await cmd.ExecuteNonQueryAsync();
                    }
                    if(recordsUpdated > 0) tran.Commit();
                    else return "failed to update!";
                    con.Close();
                    return recordsUpdated.ToString() + " records(s) updated";
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
        public static async Task<string> DeleteSubTasksAsync(List<int> ids, int userID) {
            try {
                DataTable dt = common.idList2Table(ids);
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("blkUpdate"));
                    string query = "oms.subTaskDelete";
                    int recordsUpdated = 0;
                    using(SqlCommand cmd = new SqlCommand(query, con, tran)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@tblType", dt));
                        cmd.Parameters.Add(new SqlParameter("@userID", userID));
                        recordsUpdated = await cmd.ExecuteNonQueryAsync();
                    }
                    if(recordsUpdated > 0) tran.Commit();
                    else return "failed to update!";
                    con.Close();
                    return recordsUpdated.ToString() + " records(s) deleted";
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
         
        public static async Task<List<baseLines>> ReadbaseLinesAsync(int pid) {
            List<baseLines> clbaseLines = new List<baseLines>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = " select * from oms.BaseLines where pid = @pid";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    //cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pid", pid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            baseLines t = new baseLines();
                            t.bID = rdr["bID"] == DBNull.Value ? -1 : (int)rdr["bID"];
                            t.Baseline = rdr["Baseline"] == DBNull.Value ? "" : rdr["Baseline"].ToString();
                            t.PID = rdr["PID"] == DBNull.Value ? -1 : (int)rdr["PID"];
                            clbaseLines.Add(t);
                        }
                    }
                }
                con.Close();
                return clbaseLines;
            }
        }
        public static async Task<List<T_OMS_BL_Tasks>> ReadBlTasksAsync(int pid) {
            List<T_OMS_BL_Tasks> clT_OMS_BL_Tasks = new List<T_OMS_BL_Tasks>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "select t1.* from T_OMS_BL_Tasks t1 where t1.ProjectID = @pid";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    //cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pid", pid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            T_OMS_BL_Tasks t = new T_OMS_BL_Tasks();
                            t.BID = rdr["BID"] == DBNull.Value ? -1 : (int)rdr["BID"];
                            t.TaskID = rdr["TaskID"] == DBNull.Value ? -1 : (int)rdr["TaskID"];
                            t.ProjectID = rdr["ProjectID"] == DBNull.Value ? -1 : (int)rdr["ProjectID"];
                            t.TaskRefNo = rdr["TaskRefNo"] == DBNull.Value ? "" : rdr["TaskRefNo"].ToString();
                            t.TaskName = rdr["TaskName"] == DBNull.Value ? "" : rdr["TaskName"].ToString();
                            t.TaskDesc = rdr["TaskDesc"] == DBNull.Value ? "" : rdr["TaskDesc"].ToString();
                            t.Unit = rdr["Unit"] == DBNull.Value ? -1 : (int)rdr["Unit"];
                            t.SectionID = rdr["SectionID"] == DBNull.Value ? -1 : (int)rdr["SectionID"];
                            t.Quantity = rdr["Quantity"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["Quantity"]);
                            t.StartDate = rdr["StartDate"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["StartDate"]);
                            t.EndDate = rdr["EndDate"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["EndDate"]);
                            t.RaQty = rdr["RaQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["RaQty"]);
                            t.IsActive = rdr["IsActive"] == DBNull.Value ? -1 : (int)rdr["IsActive"];
                            t.TotalCost = rdr["TotalCost"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["TotalCost"]);
                            t.AdFixed = rdr["AdFixed"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["AdFixed"]);
                            t.Adruning = rdr["Adruning"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["Adruning"]);
                            t.OP = rdr["OP"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["OP"]);
                            t.TaskCost = rdr["TaskCost"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["TaskCost"]);
                            clT_OMS_BL_Tasks.Add(t);
                        }
                    }
                }
                con.Close();
                return clT_OMS_BL_Tasks;
            }
        }
        public static async Task<List<T_OMS_BL_Assignments>> ReadBlAssignmentsAsync(int pid, int bid) {
            List<T_OMS_BL_Assignments> clT_OMS_BL_Assignments = new List<T_OMS_BL_Assignments>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "select t1.* from T_OMS_BL_Assignments t1 join T_OMS_BL_Tasks t2 on t1.TaskID = t2.TaskID where ProjectID = @pid and t1.BID = @bid";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    //cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pid", pid));
                    cmd.Parameters.Add(new SqlParameter("@bid", bid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            T_OMS_BL_Assignments t = new T_OMS_BL_Assignments();
                            t.BID = rdr["BID"] == DBNull.Value ? -1 : (int)rdr["BID"];
                            t.AssignMent = rdr["AssignMent"] == DBNull.Value ? -1 : (int)rdr["AssignMent"];
                            t.TaskID = rdr["TaskID"] == DBNull.Value ? -1 : (int)rdr["TaskID"];
                            t.ResourceID = rdr["ResourceID"] == DBNull.Value ? -1 : (int)rdr["ResourceID"];
                            t.Coeff = rdr["Coeff"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["Coeff"]);
                            t.Remarks = rdr["Remarks"] == DBNull.Value ? "" : rdr["Remarks"].ToString();
                            t.IsActive = rdr["IsActive"] == DBNull.Value ? -1 : (int)rdr["IsActive"];
                            t.LeadResID = rdr["LeadResID"] == DBNull.Value ? -1 : (int)rdr["LeadResID"];
                            t.Wastage = rdr["Wastage"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["Wastage"]);
                            clT_OMS_BL_Assignments.Add(t);
                        }
                    }
                }
                con.Close();
                return clT_OMS_BL_Assignments;
            }
        }
        public static async Task<string> sendSendToProductionAsync(List<int> stIDs, int userID) {
            DataTable dt = common.idList2Table(stIDs);
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                int stPlanRecords = 0;
                string res = string.Empty;
                string queryString = "oms.stPlndtsUpdate";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@stIDs", dt));
                    cmd.Parameters.Add(new SqlParameter("@UID", userID));
                    cmd.Parameters.Add(new SqlParameter("@result", SqlDbType.VarChar, 100)).Direction = ParameterDirection.Output;
                    stPlanRecords = await cmd.ExecuteNonQueryAsync();
                    res = cmd.Parameters["@result"].Value.ToString();
                }
                queryString = "oms.SendToProduction";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@sts", dt));
                    cmd.Parameters.Add(new SqlParameter("@result", SqlDbType.VarChar, 100)).Direction = ParameterDirection.Output;
                    int toProduction = await cmd.ExecuteNonQueryAsync();
                    res = cmd.Parameters["@result"].Value.ToString();
                }
                con.Close();
                return res;
            }
        }

        public static async Task<Tuple<int, List<to_stplans>>> ReadStplansAsync(int tid, int? stID, int cp, int ps) {
            List<to_stplans> clto_stplans = new List<to_stplans>();
            int totalRecords = 0;
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "oms.stplansRead";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@tid", tid));
                    cmd.Parameters.Add(new SqlParameter("@stID", (object)stID ?? DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@cp", cp));
                    cmd.Parameters.Add(new SqlParameter("@ps", ps));
                    cmd.Parameters.Add(new SqlParameter("@tr", SqlDbType.Int)).Direction = ParameterDirection.Output;
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            to_stplans t = new to_stplans();
                            t.id = rdr["id"] == DBNull.Value ? -1 : (int)rdr["id"];
                            t.stid = rdr["stid"] == DBNull.Value ? -1 : (int)rdr["stid"];
                            t.SubTaskName = rdr["SubTaskName"] == DBNull.Value ? "" : rdr["SubTaskName"].ToString();
                            t.plDtID = rdr["plDtID"] == DBNull.Value ? -1 : (int)rdr["plDtID"];
                            t.date = rdr["date"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["date"]);
                            t.ia = rdr["ia"] == DBNull.Value ? -1 : (int)rdr["ia"];
                            t.cb = rdr["cb"] == DBNull.Value ? -1 : (int)rdr["cb"];
                            t.co = rdr["co"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["co"]);
                            t.mb = rdr["mb"] == DBNull.Value ? -1 : (int)rdr["mb"];
                            t.mo = rdr["mo"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["mo"]);
                            t.PlQty = rdr["PlQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["PlQty"]);
                            t.UnPlanID = rdr["UnPlanID"] == DBNull.Value ? -1 : (int)rdr["UnPlanID"];
                            clto_stplans.Add(t);
                        }
                    }
                    totalRecords = (int)cmd.Parameters["@tr"].Value;
                }
                con.Close();
                return new Tuple<int, List<to_stplans>>(totalRecords, clto_stplans);
            }
        }
        public static async Task<string> CreateStplansAsync(List<to_stplans> clto_stplans, int userID) {
            try {
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("createto_stplans"));
                    to_stplans t = new to_stplans();
                    DataTable dtClass = t.dtClass("dtClass");
                    int i = 0; // CONSIDER CHANGING THE AUTOINCREMENTING TABLE ID FIELD TO THIS VARIABLE
                    foreach(to_stplans E in clto_stplans) {
                        DataRow dr = dtClass.NewRow();
                        dr["id"] = i++;
                        dr["stid"] = E.stid;
                        dr["plDtID"] = E.plDtID;
                        dr["ia"] = 1;
                        dr["cb"] = userID;
                        dr["co"] = DateTime.Now;
                        dr["mb"] = DBNull.Value;
                        dr["mo"] = DBNull.Value;
                        dr["PlQty"] = E.PlQty;
                        dr["UnPlanID"] = DBNull.Value;
                        dtClass.Rows.Add(dr);
                    }
                    using(SqlBulkCopy bcp = new SqlBulkCopy(con, SqlBulkCopyOptions.Default, tran)) {
                        bcp.DestinationTableName = "[dbo].[to_stplans]";
                        try {
                            await bcp.WriteToServerAsync(dtClass);
                            tran.Commit();
                            con.Close();
                            return clto_stplans.Count.ToString() + " record(s) inserted!";
                        } catch(Exception ex) {
                            tran.Rollback();
                            con.Close();
                            return "failed " + ex.Message;
                        }
                    }
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
        public static async Task<string> UpdateStplansAsync(List<to_stplans> clto_stplans, int userID) {
            try {
                DataTable dtClass2 = new to_stplans().dtClass2("dtClass");
                foreach(to_stplans e in clto_stplans) {
                    DataRow dr = dtClass2.NewRow();
                    dr["id"] = e.id;
                    dr["stid"] = e.stid;
                    dr["plDtID"] = e.plDtID;
                    dr["PlQty"] = e.PlQty;
                    dr["UnPlanID"] = e.UnPlanID;
                    dtClass2.Rows.Add(dr);
                }
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("blkUpdate"));
                    string query = "oms.stplansUpdate";
                    int recordsUpdated = 0;
                    using(SqlCommand cmd = new SqlCommand(query, con, tran)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@tblType", dtClass2));
                        cmd.Parameters.Add(new SqlParameter("@userID", userID));
                        recordsUpdated = await cmd.ExecuteNonQueryAsync();
                    }
                    if(recordsUpdated > 0) tran.Commit();
                    else return "failed to update!";
                    con.Close();
                    return recordsUpdated.ToString() + " records(s) updated";
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
        public static async Task<string> DeleteStplansAsync(List<twoIDs> ids, int userID) {
            try {
                DataTable dt = common.twoIDsList2Table(ids);
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("blkDelete"));
                    string query = "oms.stPlansDelete";
                    int recordsUpdated = 0;
                    using(SqlCommand cmd = new SqlCommand(query, con, tran)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@tblType", dt));
                        cmd.Parameters.Add(new SqlParameter("@userID", userID));
                        recordsUpdated = await cmd.ExecuteNonQueryAsync();
                    }
                    if(recordsUpdated > 0) tran.Commit();
                    else return "failed to update!";
                    con.Close();
                    return recordsUpdated.ToString() + " records(s) deleted";
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }

        public static async Task<List<vg_Date>> ReadDateAsync(int back, int forw) {
            List<vg_Date> clvg_Date = new List<vg_Date>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "select * from vg_Date where date >  dateadd(year,-@back,getdate()) and date < dateadd(year, @forw,getdate())";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.Parameters.Add(new SqlParameter("@back", back));
                    cmd.Parameters.Add(new SqlParameter("@forw", forw));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            vg_Date t = new vg_Date();
                            t.DateID = rdr["DateID"] == DBNull.Value ? -1 : (int)rdr["DateID"];
                            t.Date = rdr["Date"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["Date"]);
                            t.Day = rdr["Day"] == DBNull.Value ? -1 : (Int16)rdr["Day"];
                            t.WkNo = rdr["WkNo"] == DBNull.Value ? -1 : (Int16)rdr["WkNo"];
                            t.WkNm = rdr["WkNm"] == DBNull.Value ? "" : rdr["WkNm"].ToString();
                            t.WkDay = rdr["WkDay"] == DBNull.Value ? "" : rdr["WkDay"].ToString();
                            t.MoNo = rdr["MoNo"] == DBNull.Value ? -1 : (byte)rdr["MoNo"];
                            t.MoNm = rdr["MoNm"] == DBNull.Value ? "" : rdr["MoNm"].ToString();
                            t.QrNo = rdr["QrNo"] == DBNull.Value ? -1 : (byte)rdr["QrNo"];
                            t.QrNm = rdr["QrNm"] == DBNull.Value ? "" : rdr["QrNm"].ToString();
                            t.YrNo = rdr["YrNo"] == DBNull.Value ? -1 : (Int16)rdr["YrNo"];
                            t.YrNm = rdr["YrNm"] == DBNull.Value ? "" : rdr["YrNm"].ToString();
                            clvg_Date.Add(t);
                        }
                    }
                }
                con.Close();
                return clvg_Date;
            }
        }
        public static async Task<List<IDDt>> ReadDtAsync() {
            List<IDDt> clDt = new List<IDDt>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "select dateID, Date from vg_Date";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            IDDt t = new IDDt();
                            t.DateID = (int)rdr["DateID"];
                            t.Date = Convert.ToDateTime(rdr["Date"]);
                            clDt.Add(t);
                        }
                    }
                }
                con.Close();
                return clDt;
            }
        }

        public static async Task<List<T_OMS_MRPStatges>> ReadMRPStatgesAsync() {
            List<T_OMS_MRPStatges> clMRPStatges = new List<T_OMS_MRPStatges>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "select * from T_OMS_MRPStatges order by mrpStageID";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            T_OMS_MRPStatges t = new T_OMS_MRPStatges();
                            t.mrpStageID = rdr["mrpStageID"] == DBNull.Value ? -1 : (int)rdr["mrpStageID"];
                            t.StageName = rdr["StageName"] == DBNull.Value ? "" : rdr["StageName"].ToString();
                            clMRPStatges.Add(t);
                        }
                    }
                }
                con.Close();
                return clMRPStatges;
            }
        }
        
        public static async Task<List<TasksInProd>> ReadclTasksInProdAsync(int pid) {
            List<TasksInProd> clTsks = new List<TasksInProd>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "oms.TasksInProduction";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType=CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pid", pid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            TasksInProd t = new TasksInProd();
                            t.pid = (int)rdr["pid"];
                            t.tid =(int)rdr["tid"];
                            t.Task = rdr["Task"].ToString();
                            clTsks.Add(t);
                        }
                    }
                }
                con.Close();
                return clTsks;
            }
        }
    }
}

 

 