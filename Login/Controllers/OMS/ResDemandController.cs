﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Login.Controllers.OMS {
    public class ResDemandController :ApiController {
        [HttpPost]// http://localhost/Login/api/ResDemand/byRes?pid=71&stDt=2019-02-19&edDt=2019-03-21
        [Route("api/ResDemand/byRes")] // OMS >> Resource Plan >> First grid 
        public async Task<HttpResponseMessage> getResDemand(int pid, [FromBody] List<int> tids, DateTime? stDt, DateTime? edDt) {
            try {
                List<ResDemand> clResDemand = await Task.Run(() => rdDAL.ReadResDemandAsync(pid, tids, stDt, edDt));
                return Request.CreateResponse(HttpStatusCode.OK, clResDemand);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost] // http://localhost/Login/api/ResDemand/byResSt?pid=71&bid=7&stDt=2019-02-19&edDt=2019-03-21
        [Route("api/ResDemand/byResSt")]//OMS >> Resource Plan >> Second grid 
        public async Task<HttpResponseMessage> getResStPlan(int pid, [FromBody] List<int> rids, int bid, DateTime? stDt, DateTime? edDt) {
            try {
                List<ResStPlan> clResStPlan = await Task.Run(() => rdDAL.ReadResStPlanAsync(pid, rids, bid, stDt, edDt));
                return Request.CreateResponse(HttpStatusCode.OK, clResStPlan);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost]// http://localhost/Login/api/ResDemand/byResPlan?pid=71&stDt=2019-02-19&edDt=2019-03-21
        [Route("api/ResDemand/byResPlan")]//fromBody >> {array1: [6,2,1],array2:[]} OMS >> Resource Plan >> Third grid 
        public async Task<HttpResponseMessage> getResStResPlan(int pid, [FromBody] twoArrays twoAs, DateTime? stDt, DateTime? edDt) {
            try {
                List<ResStResPlan> clResStResPlan = await Task.Run(() => rdDAL.ReadResStResPlanAsync(pid, twoAs, stDt, edDt));
                return Request.CreateResponse(HttpStatusCode.OK, clResStResPlan);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet] // http://localhost/Login/api/ResDemand/get?pid=20&pcid=null&gpid=null&tid=null&stid=null&cat=null&rid=null&stDt=2018-02-19&edDt=2018-03-19
        [Route("api/ResDemand/get")]// DEPRECATED
        public async Task<HttpResponseMessage> getResourceDemand(int pid, int? pcid, int? gpid,
                int? tid, int? stid, int? cat, int? rid, DateTime? stDt, DateTime? edDt) {
            try {
                List<ResourceDemand> clgetResourceDemand = await Task.Run(() => rdDAL.ReadResourceDemandAsync(pid, pcid, gpid,
                tid, stid, cat, rid, stDt, edDt));
                return Request.CreateResponse(HttpStatusCode.OK, clgetResourceDemand);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost]// http://localhost/Login/api/ResDemand/send?userID=1&RHD=0
        [Route("api/ResDemand/send")] // To Approvals BUTTON on Resource Plans
        public async Task<HttpResponseMessage> BulkCreateRsPlansAsync([FromBody]List<rsPlans> clRsPlans, int userID, int RHD) {
            try {           //[{  id: 20018, plID: 196130, rbid: 4, rID: 1, pID: 71, stg: 11, ia: 1  }]
                string result = await Task.Run(() => rdDAL.CreateRsPlansAsync(clRsPlans, userID, RHD));
                if(!result.Contains("failed")) {
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]// http://localhost/Login/api/ResDemand/missingSTs?pid=71&tid=3066&rid=6&stg=11
        [Route("api/ResDemand/missingSTs")] //  missingSTs BUTTON on Resource Plans
        public async Task<HttpResponseMessage> missingSTs(int pid, int tid, int rid, int stg) {
            try {
                List<int> missed = await Task.Run(() => rdDAL.MissingSTs(pid, tid, rid, stg));
                if(missed.Count > 0) {
                    return Request.CreateResponse(HttpStatusCode.OK, missed);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "None missed");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost]// http://localhost/Login/api/ResDemand/resetSTs?pid=71&userID=1 
        [Route("api/ResDemand/resetSTs")] //  reset BUTTON on Resource Plans
        public async Task<HttpResponseMessage> resetSTs(int pid, [FromBody]List<int> sts, int userID) {
            try {
                string result = await Task.Run(() => rdDAL.resetSTsInRsPlans(pid, sts, userID));
                return Request.CreateResponse(HttpStatusCode.OK, result);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]// http://localhost/Login/api/ResDemand/STsInProduction?tid=3062
        [Route("api/ResDemand/STsInProduction")] //  missingSTs BUTTON on Resource Plans
        public async Task<HttpResponseMessage> STsInProduction(int tid) {
            try {
                List<intString> sts = await Task.Run(() => rdDAL.STsInProduction(tid));
                if(sts.Count > 0) {
                    return Request.CreateResponse(HttpStatusCode.OK, sts);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "None missed");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost]  // http://localhost/Login/api/ResDemand/StsInProd from body from body [24537, 24539, 24540]
        [Route("api/ResDemand/StsInProd")]
        public async Task<HttpResponseMessage> getStsInProd([FromBody] List<int> sts) {
            try {
                List<StsInProd> clStsInProd = await Task.Run(() => rdDAL.ReadStsInProdAsync(sts));
                return Request.CreateResponse(HttpStatusCode.OK, clStsInProd);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpGet] // http://localhost/Login/api/ResDemand/LoadApprovals?pid=71&modID=7&IsActive=true
        [Route("api/ResDemand/LoadApprovals")]//LOAD GRID of table T_OMS_ProjectIndents data for prosessing Approvals
        public async Task<HttpResponseMessage> LoadApprovalsProjectIndents(int pid, int? modID, bool IsActive) {
            try {
                List<T_OMS_ProjectIndents> clT_OMS_ProjectIndents = await Task.Run(() => rdDAL.ReadProjectIndentsAsync(pid,modID,IsActive));
                return Request.CreateResponse(HttpStatusCode.OK, clT_OMS_ProjectIndents);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpPut]// http://localhost/Login/api/ResDemand/ApprovalActions?userID=1&action=1 (recommend =1, approve=2, Reject=3, restore =4)
        [Route("api/ResDemand/ApprovalActions")]
        public async Task<HttpResponseMessage> ApprovalActions([FromBody]List<intString> clProjectIndents, int userID, int action) {
            try {
                if(clProjectIndents.Count > 0) {
                    string result = await Task.Run(() => rdDAL.UpdateProjectIndentsAsync(clProjectIndents, userID, action));
                    if(!result.Contains("failed")) {
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No records to Update");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]// http://localhost/Login/api/ResDemand/taskIndentByPRID?prid=3167
        [Route("api/ResDemand/taskIndentByPRID")]
        public async Task<HttpResponseMessage> gettaskIndentByPRID(int prid) {
            try {     
                List<taskIndentByPRID> cltaskIndentByPRID = await Task.Run(() => rdDAL.ReadtaskIndentByPRIDAsync(prid));
                return Request.CreateResponse(HttpStatusCode.OK, cltaskIndentByPRID);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpDelete] //  http://localhost/Login/api/ResDemand/Delete?userID=1
        [Route("api/ResDemand/Delete")]
        public async Task<HttpResponseMessage> RejectPI([FromBody] List<int> IDs, int userID) {
            try {
                if(IDs.Count > 0) {
                    string result = await Task.Run(() => rdDAL.DeleteProjectIndentsAsync(IDs, userID));
                    if(!result.Contains("failed")) {
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No records to Delete");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        //CreatePurchaseRequestAsync(List<int> IReqIDs,int pid, int cat, bool transfer, int hrPerDay, bool isJob, int userID)
        //CreatePurchaseRequestAsync(IReqIDs, pid, cat, transfer, hrPerDay, isJob, userID)

        [HttpPost]// http://localhost/Login/api/ResDemand/toPMS?pid=1&gs=true&userID=1  
        [Route("api/ResDemand/toPMS")] // bit,@gs bit=1,@bID int
        public async Task<HttpResponseMessage> PurchaseRequestRaise([FromBody]List<int> IReqIDs, int pid, bool gs, int userID) {
            try {
                if(IReqIDs.Count > 0) {
                    string result = await Task.Run(() => rdDAL.CreatePurchaseRequestAsync(IReqIDs, pid, false, gs, userID));
                    if(!result.Contains("failed")) {
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No records to Update");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost]// http://localhost/Login/api/ResDemand/transfer?pid=1&gs=true&userID=1
        [Route("api/ResDemand/transfer")]
        public async Task<HttpResponseMessage> PurchaseRequestTransfer([FromBody]List<int> IReqIDs, int pid, bool gs, int userID) {
            try {
                if(IReqIDs.Count > 0) {
                    bool tfr = true;
                    string result = await Task.Run(() => rdDAL.CreatePurchaseRequestAsync(IReqIDs, pid, tfr, gs, userID));
                    if(!result.Contains("failed")) {
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No records to Update");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }
}


//this.gridOptions.columnDefs = [
//{ headerName: 'IndentReqID', field: 'IndentReqID', width: 70, checkboxSelection: true, cellStyle: { textAlign: 'right' } },
//{ headerName: 'TaskID', field: 'TaskID', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'SubTaskID', field: 'SubTaskID', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'Subtask', field: 'Subtask', width: 200, cellStyle: { textAlign: 'left' } },
//{ headerName: 'Qty', field: 'Qty', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'SAID', field: 'SAID', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'ReqDate', field: 'ReqDate', width: 100, cellStyle: { textAlign: 'center' } },

//];export interface taskIndentByPRID {
//	IndentReqID: number; 
//	TaskID: number; 
//	SubTaskID: number; 
//	Subtask: string; 
//	Qty: number; 
//	SAID: number; 
//	ReqDate: Date; 
// }


