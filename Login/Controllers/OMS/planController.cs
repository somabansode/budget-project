﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Login.Controllers.OMS {
    public class planController :ApiController {
        [HttpGet] // http://localhost/Login/api/plan/scPlan?pid=71&tid=3066
        [Route("api/plan/scPlan")]
        public async Task<HttpResponseMessage> scPlan(int pid, int tid) {
            try {
                List<scPlan> clscPlan = await Task.Run(() => plDAL.ReadscPlanAsync(pid, tid));
                return Request.CreateResponse(HttpStatusCode.OK, clscPlan);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]//http://localhost/Login/api/plan/StockJournalDetail?cid=1&pid=71
        [Route("api/plan/StockJournalDetail")]
        public async Task<HttpResponseMessage> getStockJournalDetail(int cid, int? pid) {
            try {
                List<StockJournalDetail> clStockJournalDetail = await Task.Run(() => plDAL.ReadStockJournalDetailAsync(cid, pid));
                return Request.CreateResponse(HttpStatusCode.OK, clStockJournalDetail);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost] // http://localhost/Login/api/plan/matPlanReq?tid=3062&bid=21 from body [24529,24530]
        [Route("api/plan/matPlanReq")]
        public async Task<HttpResponseMessage> getmatPlanReq(int tid, List<int?> sts, int bid) {
            try {
                List<matPlanReq> clmatPlanReq = await Task.Run(() => plDAL.ReadmatPlanReqAsync(tid,sts, bid));
                return Request.CreateResponse(HttpStatusCode.OK, clmatPlanReq);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost] // http://localhost/Login/api/plan/matAvailability2Grid?pid=71 >> from body >> [1,6,90]
        [Route("api/plan/matAvailability2Grid")] 
        public async Task<HttpResponseMessage> getmatAvailability2(int pid, [FromBody] List<int> rids) {
            try {
                List<object> clmatAvailability2 = await Task.Run(() => plDAL.ReadmatAvailability2Async(pid, rids));
                return Request.CreateResponse(HttpStatusCode.OK, clmatAvailability2);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost] // http://localhost/Login/api/plan/matPlanExecute?bid=21&userID=1 >> from body >> {array1: [90],array2:[24527]}
        [Route("api/plan/matPlanExecute")]
        public async Task<HttpResponseMessage> matPlanExecute([FromBody] twoArrays twoAs, int bid, int userID) {
            try {
                List<object> returnTables = await Task.Run(() => plDAL.matPlanExecuteAsync(twoAs.array1, twoAs.array2, bid, userID));
                return Request.CreateResponse(HttpStatusCode.OK, returnTables);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]  // http://localhost/Login/api/plan/deleteAssignedMaterials?pid=71&rid=6&stid=2552&dtFr=2019-04-14&dtTo=2019-04-14
        [Route("api/plan/deleteAssignedMaterials")]
        public async Task<HttpResponseMessage> deleteAssignedMaterialsAsync(int pid,int rid,int stid, DateTime dtFr, DateTime dtTo) {
            try {  
                string  result = await Task.Run(() => plDAL.deleteAssignedMaterialsAsync(pid,rid,stid, dtFr, dtTo));
                return Request.CreateResponse(HttpStatusCode.OK, result);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }
}

