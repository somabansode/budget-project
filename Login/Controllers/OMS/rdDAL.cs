﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Login.Controllers.OMS {
    public class rdDAL {
        public static async Task<List<ResourceDemand>> ReadResourceDemandAsync(int pid, int? pcid, int? gpid,
                int? tid, int? stid, int? cat, int? rid, DateTime? stDt, DateTime? edDt) {
            List<ResourceDemand> clResourceDemand = new List<ResourceDemand>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "oms.getResourceDemandRead";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pid", pid));
                    cmd.Parameters.Add(new SqlParameter("@pcid", (object)pcid ?? DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@gpid", (object)gpid ?? DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@tid", (object)tid ?? DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@stid", (object)stid ?? DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@cat", (object)cat ?? DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@rid", (object)rid ?? DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@stDt", (object)stDt ?? DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@edDt", (object)edDt ?? DBNull.Value));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            ResourceDemand t = new ResourceDemand();
                            t.id = rdr["id"] == DBNull.Value ? -1 : (int)rdr["id"];
                            t.rbid = rdr["rbid"] == DBNull.Value ? -1 : (int)rdr["rbid"];
                            t.pID = rdr["pID"] == DBNull.Value ? -1 : (int)rdr["pID"];
                            t.pcID = rdr["pcID"] == DBNull.Value ? -1 : (int)rdr["pcID"];
                            t.CompName = rdr["CompName"] == DBNull.Value ? "" : rdr["CompName"].ToString();
                            t.gpID = rdr["gpID"] == DBNull.Value ? -1 : (int)rdr["gpID"];
                            t.SectionName = rdr["SectionName"] == DBNull.Value ? "" : rdr["SectionName"].ToString();
                            t.tID = rdr["tID"] == DBNull.Value ? -1 : (int)rdr["tID"];
                            t.TaskName = rdr["TaskName"] == DBNull.Value ? "" : rdr["TaskName"].ToString();
                            t.unit = rdr["unit"] == DBNull.Value ? -1 : (int)rdr["unit"];
                            t.tUoM = rdr["tUoM"] == DBNull.Value ? "" : rdr["tUoM"].ToString();
                            t.stID = rdr["stID"] == DBNull.Value ? -1 : (int)rdr["stID"];
                            t.SubTaskName = rdr["SubTaskName"] == DBNull.Value ? "" : rdr["SubTaskName"].ToString();
                            t.aID = rdr["aID"] == DBNull.Value ? -1 : (int)rdr["aID"];
                            t.bID = rdr["bID"] == DBNull.Value ? -1 : (int)rdr["bID"];
                            t.stgID = rdr["stgID"] == DBNull.Value ? -1 : (int)rdr["stgID"];
                            t.DateId = rdr["DateId"] == DBNull.Value ? -1 : (int)rdr["DateId"];
                            t.plID = rdr["plID"] == DBNull.Value ? -1 : (int)rdr["plID"];
                            t.Date = rdr["Date"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["Date"]);
                            t.plQty = rdr["plQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["plQty"]);
                            t.stQty = rdr["stQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["stQty"]);
                            t.cat = rdr["cat"] == DBNull.Value ? -1 : (int)rdr["cat"];
                            t.Category = rdr["Category"] == DBNull.Value ? "" : rdr["Category"].ToString();
                            t.rID = rdr["rID"] == DBNull.Value ? -1 : (int)rdr["rID"];
                            t.ResourceName = rdr["ResourceName"] == DBNull.Value ? "" : rdr["ResourceName"].ToString();
                            t.rud = rdr["rud"] == DBNull.Value ? -1 : (int)rdr["rud"];
                            t.rUoM = rdr["rUoM"] == DBNull.Value ? "" : rdr["rUoM"].ToString();
                            t.rsDyQty = rdr["rsDyQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["rsDyQty"]);
                            t.rsQty = rdr["rsQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["rsQty"]);
                            clResourceDemand.Add(t);
                        }
                    }
                }
                con.Close();
                return clResourceDemand;
            }
        }
        public static async Task<List<int>> MissingSTs(int pid, int tid, int rid, int stg) {
            List<int> missed = new List<int>();
            try {
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    string query = "oms.missingSTsInRsPlans";
                    using(SqlCommand cmd = new SqlCommand(query, con)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@pid", pid));
                        cmd.Parameters.Add(new SqlParameter("@tid", tid));
                        cmd.Parameters.Add(new SqlParameter("@rid", rid));
                        cmd.Parameters.Add(new SqlParameter("@stg", stg));
                        using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                            while(await rdr.ReadAsync()) {
                                missed.Add((int)rdr[0]);
                            }
                        }
                    }
                    con.Close();
                    if(missed.Count == 0) missed.Add(-1);
                    return missed;
                }
            } catch { missed.Add(-1); return missed; }
        }
        public static async Task<string> resetSTsInRsPlans(int pid, List<int> sts, int userID) {
            string result = string.Empty;
            DataTable dtSts = common.idList2Table(sts);
            try {
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    string query = "oms.resetSTsInRsPlans";
                    using(SqlCommand cmd = new SqlCommand(query, con)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@pid", pid));
                        cmd.Parameters.Add(new SqlParameter("@sts", dtSts));
                        cmd.Parameters.Add(new SqlParameter("@userID", userID));
                        cmd.Parameters.Add(new SqlParameter("@result", SqlDbType.VarChar, 100)).Direction = ParameterDirection.Output;
                        await cmd.ExecuteNonQueryAsync();
                        result = cmd.Parameters["@result"].Value.ToString();

                    }
                    con.Close();
                    return result;

                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
        public static async Task<List<intString>> STsInProduction(int tid) {
            List<intString> stNames = new List<intString>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string query = "oms.STsInProduction";
                using(SqlCommand cmd = new SqlCommand(query, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@tid", tid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            intString obj = new intString();
                            obj.id = (int)rdr[0];
                            obj.str = rdr[1].ToString();
                            stNames.Add(obj);
                        }
                    }
                }
                con.Close();
                return stNames;
            }
        }
        public static async Task<List<StsInProd>> ReadStsInProdAsync(List<int> sts) {
            List<StsInProd> clStsInProd = new List<StsInProd>();
            DataTable dtSts = common.idList2Table(sts);
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "oms.STsInProductionData11";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@sts", dtSts));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            StsInProd t = new StsInProd();
                            t.BID = rdr["BID"] == DBNull.Value ? -1 : (int)rdr["BID"];
                            t.stid = rdr["stid"] == DBNull.Value ? -1 : (int)rdr["stid"];
                            t.tid = rdr["tid"] == DBNull.Value ? -1 : (int)rdr["tid"];
                            t.SubTaskName = rdr["SubTaskName"] == DBNull.Value ? "" : rdr["SubTaskName"].ToString();
                            t.QTY = rdr["QTY"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["QTY"]);
                            t.stdt = rdr["stdt"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["stdt"]);
                            t.eddt = rdr["eddt"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["eddt"]);
                            t.Nos = rdr["Nos"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["Nos"]);
                            t.B = rdr["B"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["B"]);
                            t.D = rdr["D"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["D"]);
                            t.L = rdr["L"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["L"]);
                            t.chFrom = rdr["chFrom"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["chFrom"]);
                            t.chTo = rdr["chTo"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["chTo"]);
                            t.ia = rdr["ia"] == DBNull.Value ? false : (bool)rdr["ia"];
                            t.comp = rdr["comp"] == DBNull.Value ? -1 : (int)rdr["comp"];
                            t.stg = rdr["stg"] == DBNull.Value ? -1 : (int)rdr["stg"];
                            clStsInProd.Add(t);
                        }
                    }
                }
                con.Close();
                return clStsInProd;
            }
        }

        public static async Task<string> CreateRsPlansAsync(List<rsPlans> clRsPlans, int userID, int RHD) {
            try {
                rsPlans t = new rsPlans();
                DataTable dtClass = t.dtClass("dtClass");
                foreach(rsPlans E in clRsPlans) {
                    DataRow dr = dtClass.NewRow();
                    dr["id"] = E.id;
                    dr["plID"] = E.plID;
                    dr["rbid"] = E.rbid;
                    dr["rID"] = E.rID;
                    dr["pID"] = E.pID;
                    dr["stg"] = E.stg;
                    dr["ia"] = E.ia;
                    dtClass.Rows.Add(dr);
                }
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("creatersPlans"));
                    int recordsUpdated = 0;
                    using(SqlCommand cmd = new SqlCommand("oms.RequestIndent", con, tran)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@rsPlans", dtClass));
                        cmd.Parameters.Add(new SqlParameter("@userID", userID));
                        cmd.Parameters.Add(new SqlParameter("@RHD", RHD));
                        DataSet ds = new DataSet();
                        recordsUpdated = await cmd.ExecuteNonQueryAsync();
                    }
                    if(recordsUpdated > 0) { tran.Commit(); return "SUCCESS! Next step: Approvals in Logistics"; } else { tran.Rollback(); return "FAILED"; }
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }        public static async Task<List<T_OMS_ProjectIndents>> ReadProjectIndentsAsync(int pid,  int? mid, bool IsActive) {
            List<T_OMS_ProjectIndents> clT_OMS_ProjectIndents = new List<T_OMS_ProjectIndents>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "oms.projectIndents";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pid", pid));
                    cmd.Parameters.Add(new SqlParameter("@mid", (object)mid ?? DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@IsActive",  IsActive));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            T_OMS_ProjectIndents t = new T_OMS_ProjectIndents();
                            t.IndentReqId = rdr["IndentReqId"] == DBNull.Value ? -1 : (int)rdr["IndentReqId"];
                            t.Remarks = rdr["Remarks"] == DBNull.Value ? "" : rdr["Remarks"].ToString();
                            t.ProjectID = rdr["ProjectID"] == DBNull.Value ? -1 : (int)rdr["ProjectID"];
                            t.ResourceID = rdr["ResourceID"] == DBNull.Value ? -1 : (int)rdr["ResourceID"];
                            t.ResourceName = rdr["ResourceName"] == DBNull.Value ? "" : rdr["ResourceName"].ToString();
                            t.rUoM = rdr["rUoM"] == DBNull.Value ? "" : rdr["rUoM"].ToString();
                            t.ProjectQty = rdr["ProjectQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["ProjectQty"]);
                            t.IndentedQty = rdr["IndentedQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["IndentedQty"]);
                            t.ProcuredQty = rdr["ProcuredQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["ProcuredQty"]);
                            t.ReqQty = rdr["ReqQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["ReqQty"]);
                            t.FromDate = rdr["FromDate"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["FromDate"]);
                            t.ToDate = rdr["ToDate"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["ToDate"]);
                            t.RequiredOn = rdr["RequiredOn"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["RequiredOn"]);
                            // t.IndentType = rdr["IndentType"] == DBNull.Value ? -1 : (int)rdr["IndentType"];
                            t.RequestBy = rdr["RequestBy"] == DBNull.Value ? -1 : (int)rdr["RequestBy"];
                            t.ReuestedOn = rdr["ReuestedOn"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["ReuestedOn"]);
                            //t.IndentID = rdr["IndentID"] == DBNull.Value ? -1 : (int)rdr["IndentID"];
                            //t.RHD = rdr["RHD"] == DBNull.Value ? -1 : (int)rdr["RHD"];
                            t.RcmdBy = rdr["RcmdBy"] == DBNull.Value ? -1 : (int)rdr["RcmdBy"];
                            t.RcmdOn = rdr["RcmdOn"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["RcmdOn"]);
                            t.ApprvdBy = rdr["ApprvdBy"] == DBNull.Value ? -1 : (int)rdr["ApprvdBy"];
                            t.ApprvOn = rdr["ApprvOn"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["ApprvOn"]);
                            t.Rejectby = rdr["Rejectby"] == DBNull.Value ? -1 : (int)rdr["Rejectby"];
                            t.RejectOn = rdr["RejectOn"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["RejectOn"]);
                            t.TrfID = rdr["TrfID"] == DBNull.Value ? -1 : (int)rdr["TrfID"];
                            t.IsActive = rdr["IsActive"] == DBNull.Value ? false : (bool)rdr["IsActive"];
                            t.moduleID = rdr["moduleID"] == DBNull.Value ? -1 : (int)rdr["moduleID"];
                            clT_OMS_ProjectIndents.Add(t);
                        }
                    }
                }
                con.Close();
                return clT_OMS_ProjectIndents;
            }
        }
        public static async Task<List<taskIndentByPRID>> ReadtaskIndentByPRIDAsync(int prid) {
            List<taskIndentByPRID> cltaskIndentByPRID = new List<taskIndentByPRID>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "oms.taskIndentByPRID";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@prid", prid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            taskIndentByPRID t = new taskIndentByPRID();
                            t.IndentReqID = rdr["IndentReqID"] == DBNull.Value ? -1 : (int)rdr["IndentReqID"];
                            t.TaskID = rdr["TaskID"] == DBNull.Value ? -1 : (int)rdr["TaskID"];
                            t.SubTaskID = rdr["SubTaskID"] == DBNull.Value ? -1 : (int)rdr["SubTaskID"];
                            t.Subtask = rdr["Subtask"] == DBNull.Value ? "" : rdr["Subtask"].ToString();
                            t.Qty = rdr["Qty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["Qty"]);
                            t.SAID = rdr["SAID"] == DBNull.Value ? -1 : (int)rdr["SAID"];
                            t.ReqDate = rdr["ReqDate"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["ReqDate"]);
                            cltaskIndentByPRID.Add(t);
                        }
                    }
                }
                con.Close();
                return cltaskIndentByPRID;
            }
        }
        public static async Task<string> UpdateProjectIndentsAsync(List<intString> clProjectIndents, int userID, int act) {
            try {
                DataTable dtClass = new intString().dtClass("dtClass");
                foreach(intString r in clProjectIndents) {
                    DataRow dr = dtClass.NewRow();
                    dr["id"] = r.id;
                    dr["str"] = r.str == string.Empty ? "No Remarks" : r.str;
                    dtClass.Rows.Add(dr);
                }
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("blkUpdate"));
                    string query = "oms.ProjectIndentsActions";
                    int recordsUpdated = 0;
                    using(SqlCommand cmd = new SqlCommand(query, con, tran)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@kvp", dtClass));
                        cmd.Parameters.Add(new SqlParameter("@userID", userID));
                        cmd.Parameters.Add(new SqlParameter("@act", act));
                        recordsUpdated = await cmd.ExecuteNonQueryAsync();
                    }
                    if(recordsUpdated > 0) tran.Commit();
                    else return "failed to update!";
                    con.Close();
                    return recordsUpdated.ToString() + " records(s) updated";
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
        public static async Task<string> DeleteProjectIndentsAsync(List<int> ids, int userID) {
            try {
                DataTable dt = common.idList2Table(ids);
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("blkUpdate"));
                    string query = "oms.ProjectIndentsDelete";
                    int recordsUpdated = 0;
                    using(SqlCommand cmd = new SqlCommand(query, con, tran)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@tblType", dt));
                        cmd.Parameters.Add(new SqlParameter("@userID", userID));
                        recordsUpdated = await cmd.ExecuteNonQueryAsync();
                    }
                    if(recordsUpdated > 0) tran.Commit();
                    else return "failed to update!";
                    con.Close();
                    return recordsUpdated.ToString() + " records(s) deleted";
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
        public static async Task<string> CreatePurchaseRequestAsync(List<int> IReqIDs, int pid, bool transfer, bool gs, int userID) {
            try {
                DataTable dt = common.idList2Table(IReqIDs);
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("blkUpdate"));
                    string query = "oms.manageIndent";
                    string result = string.Empty;
                    //if(cat == 1) RHD = 10;
                    using(SqlCommand cmd = new SqlCommand(query, con, tran)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@pid", pid));
                        //cmd.Parameters.Add(new SqlParameter("@cat", cat));
                        cmd.Parameters.Add(new SqlParameter("@IReqIDs", dt));
                        cmd.Parameters.Add(new SqlParameter("@userID", userID));
                        cmd.Parameters.Add(new SqlParameter("@tfr", transfer));
                        //cmd.Parameters.Add(new SqlParameter("@RHD", (object)RHD ?? DBNull.Value));
                        //cmd.Parameters.Add(new SqlParameter("@isJob", isJob));
                        cmd.Parameters.Add(new SqlParameter("@gs", gs));
                        cmd.Parameters.Add(new SqlParameter("@result", SqlDbType.VarChar, 200)).Direction = ParameterDirection.Output;
                        await cmd.ExecuteNonQueryAsync();
                        result = cmd.Parameters["@result"].Value.ToString();
                    }
                    if(result.Contains("success")) { tran.Commit(); con.Close(); return result; } else { con.Close(); return result; }
                }
            } catch(Exception ex) { return "Unknown error and failed " + ex.Message; }
        }        public static async Task<List<ResDemand>> ReadResDemandAsync(int pid, List<int> tids, DateTime? stDt, DateTime? edDt) {
            List<ResDemand> clResDemand = new List<ResDemand>();
            DataTable dtTids = common.idList2Table(tids);
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "oms.ResourceDemandResourceLevel";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pid", pid));
                    cmd.Parameters.Add(new SqlParameter("@tIDs", dtTids));
                    cmd.Parameters.Add(new SqlParameter("@stDt", (object)stDt ?? DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@edDt", (object)edDt ?? DBNull.Value));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            ResDemand t = new ResDemand();
                            t.bid = rdr["bid"] == DBNull.Value ? -1 : (int)rdr["bid"];
                            t.rbid = rdr["rbid"] == DBNull.Value ? -1 : (int)rdr["rbid"];
                            t.rid = rdr["rid"] == DBNull.Value ? -1 : (int)rdr["rid"];
                            t.ResourceName = rdr["ResourceName"] == DBNull.Value ? "" : rdr["ResourceName"].ToString();
                            t.rud = rdr["rud"] == DBNull.Value ? -1 : (int)rdr["rud"];
                            t.ResQty = rdr["ResQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["ResQty"]);
                            t.ResRate = rdr["ResRate"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["ResRate"]);
                            t.Earliest = rdr["Earliest"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["Earliest"]);
                            t.Latest = rdr["Latest"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["Latest"]);
                            t.cat = rdr["cat"] == DBNull.Value ? -1 : (int)rdr["cat"];
                            t.Category = rdr["Category"] == DBNull.Value ? "" : rdr["Category"].ToString();
                            t.pcID = rdr["pcID"] == DBNull.Value ? -1 : (int)rdr["pcID"];
                            t.CompName = rdr["CompName"] == DBNull.Value ? "" : rdr["CompName"].ToString();
                            t.sts = rdr["sts"] == DBNull.Value ? -1 : (int)rdr["sts"];
                            clResDemand.Add(t);
                        }
                    }
                }
                con.Close();
                return clResDemand;
            }
        }
        public static async Task<List<ResStPlan>> ReadResStPlanAsync(int pid, List<int> rids, int bid, DateTime? stDt, DateTime? edDt) {
            List<ResStPlan> clResStPlan = new List<ResStPlan>();
            DataTable dt = common.idList2Table(rids);
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "oms.ResourceDemandResSTLevel";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pid", pid));
                    cmd.Parameters.Add(new SqlParameter("@rids", dt));
                    cmd.Parameters.Add(new SqlParameter("@bid", bid));
                    cmd.Parameters.Add(new SqlParameter("@stDt", (object)stDt ?? DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@edDt", (object)edDt ?? DBNull.Value));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            ResStPlan t = new ResStPlan();
                            t.gpID = rdr["gpID"] == DBNull.Value ? -1 : (int)rdr["gpID"];
                            t.SectionName = rdr["SectionName"] == DBNull.Value ? "" : rdr["SectionName"].ToString();
                            t.tID = rdr["tID"] == DBNull.Value ? -1 : (int)rdr["tID"];
                            t.task = rdr["task"] == DBNull.Value ? "" : rdr["task"].ToString();
                            t.pcID = rdr["pcID"] == DBNull.Value ? -1 : (int)rdr["pcID"];
                            t.CompName = rdr["CompName"] == DBNull.Value ? "" : rdr["CompName"].ToString();
                            t.stID = rdr["stID"] == DBNull.Value ? -1 : (int)rdr["stID"];
                            t.subtask = rdr["subtask"] == DBNull.Value ? "" : rdr["subtask"].ToString();
                            t.unit = rdr["unit"] == DBNull.Value ? -1 : (int)rdr["unit"];
                            t.tUoM = rdr["tUoM"] == DBNull.Value ? "" : rdr["tUoM"].ToString();
                            t.stQty = rdr["stQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["stQty"]);
                            t.plQty = rdr["plQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["plQty"]);
                            t.plDays = rdr["plDays"] == DBNull.Value ? -1 : (int)rdr["plDays"];
                            t.Earliest = rdr["Earliest"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["Earliest"]);
                            t.Latest = rdr["Latest"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["Latest"]);
                            t.pls = rdr["pls"] == DBNull.Value ? -1 : (int)rdr["pls"];
                            clResStPlan.Add(t);
                        }
                    }
                }
                con.Close();
                return clResStPlan;
            }
        }
        public static async Task<List<ResStResPlan>> ReadResStResPlanAsync(int pid, twoArrays twoAs, DateTime? stDt, DateTime? edDt) {
            List<ResStResPlan> clResStResPlan = new List<ResStResPlan>();
            DataTable dtRs = common.idList2Table(twoAs.array1);
            DataTable dtSTs = common.idList2Table(twoAs.array2);

            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "oms.ResourceDemandResStDateLevel";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pid", pid));
                    cmd.Parameters.Add(new SqlParameter("@rids", dtRs));
                    cmd.Parameters.Add(new SqlParameter("@stIDs", dtSTs));
                    cmd.Parameters.Add(new SqlParameter("@stDt", (object)stDt ?? DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@edDt", (object)edDt ?? DBNull.Value));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            ResStResPlan t = new ResStResPlan();
                            t.plID = rdr["plID"] == DBNull.Value ? -1 : (int)rdr["plID"];
                            t.Date = rdr["Date"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["Date"]);
                            t.plQty = rdr["plQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["plQty"]);
                            t.id = rdr["id"] == DBNull.Value ? -1 : (int)rdr["id"];
                            t.rbid = rdr["rbid"] == DBNull.Value ? -1 : (int)rdr["rbid"];
                            t.rid = rdr["rid"] == DBNull.Value ? -1 : (int)rdr["rid"];
                            t.rsDyQty = rdr["rsDyQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["rsDyQty"]);
                            t.rsQty = rdr["rsQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["rsQty"]);
                            t.reqdDt = rdr["reqdDt"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["reqdDt"]);
                            clResStResPlan.Add(t);
                        }
                    }
                }
                con.Close();
                return clResStResPlan;
            }
        }
    }
    public class ResourceDemand {
        public int id { get; set; }
        public int rbid { get; set; }
        public int pID { get; set; }
        public int pcID { get; set; }
        public string CompName { get; set; }
        public int gpID { get; set; }
        public string SectionName { get; set; }
        public int tID { get; set; }
        public string TaskName { get; set; }
        public int unit { get; set; }
        public string tUoM { get; set; }
        public int stID { get; set; }
        public string SubTaskName { get; set; }
        public int aID { get; set; }
        public int bID { get; set; }
        public int stgID { get; set; }
        public int DateId { get; set; }
        public int plID { get; set; }
        public DateTime Date { get; set; }
        public decimal plQty { get; set; }
        public decimal stQty { get; set; }
        public int cat { get; set; }
        public string Category { get; set; }
        public int rID { get; set; }
        public string ResourceName { get; set; }
        public int rud { get; set; }
        public string rUoM { get; set; }
        public decimal rsDyQty { get; set; }
        public decimal rsQty { get; set; }

        public ResourceDemand() { }
        public ResourceDemand(int _id, int _rbid, int _pID, int _pcID, string _CompName, int _gpID, string _SectionName, int _tID, string _TaskName
            , int _unit, string _tUoM, int _stID, string _SubTaskName, int _aID, int _bID, int _stgID, int _DateId, int _plid, DateTime _Date
            , decimal _plQty, decimal _stQty, int _cat, string _Category, int _rID, string _ResourceName
            , int _rud, string _rUoM, decimal _rsDyQty, decimal _rsQty) {
            id = _id; rbid = _rbid; pID = _pID; pcID = _pcID; CompName = _CompName; gpID = _gpID; SectionName = _SectionName; tID = _tID; TaskName = _TaskName;
            unit = _unit; tUoM = _tUoM; stID = _stID; SubTaskName = _SubTaskName; aID = _aID; bID = _bID; stgID = _stgID;
            DateId = _DateId; plID = _plid; Date = _Date; plQty = _plQty; stQty = _stQty; cat = _cat; Category = _Category;
            rID = _rID; ResourceName = _ResourceName; rud = _rud; rUoM = _rUoM; rsDyQty = _rsDyQty; rsQty = _rsQty;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("id", typeof(System.Int32));
            dtName.Columns.Add("rbid", typeof(System.Int32));
            dtName.Columns.Add("pID", typeof(System.Int32));
            dtName.Columns.Add("pcID", typeof(System.Int32));
            dtName.Columns.Add("CompName", typeof(System.String));
            dtName.Columns.Add("gpID", typeof(System.Int32));
            dtName.Columns.Add("SectionName", typeof(System.String));
            dtName.Columns.Add("tID", typeof(System.Int32));
            dtName.Columns.Add("TaskName", typeof(System.String));
            dtName.Columns.Add("unit", typeof(System.Int32));
            dtName.Columns.Add("tUoM", typeof(System.String));
            dtName.Columns.Add("stID", typeof(System.Int32));
            dtName.Columns.Add("SubTaskName", typeof(System.String));
            dtName.Columns.Add("aID", typeof(System.Int32));
            dtName.Columns.Add("bID", typeof(System.Int32));
            dtName.Columns.Add("stgID", typeof(System.Int32));
            dtName.Columns.Add("DateId", typeof(System.Int32));
            dtName.Columns.Add("id", typeof(System.Int32));
            dtName.Columns.Add("Date", typeof(System.DateTime));
            dtName.Columns.Add("plQty", typeof(System.Double));
            dtName.Columns.Add("stQty", typeof(System.Double));
            dtName.Columns.Add("cat", typeof(System.Int32));
            dtName.Columns.Add("Category", typeof(System.String));
            dtName.Columns.Add("rID", typeof(System.Int32));
            dtName.Columns.Add("ResourceName", typeof(System.String));
            dtName.Columns.Add("rud", typeof(System.Int32));
            dtName.Columns.Add("rUoM", typeof(System.String));
            dtName.Columns.Add("rsDyQty", typeof(System.Double));
            dtName.Columns.Add("rsQty", typeof(System.Double));
            return dtName;
        }
    }
    public class rsPlans {
        public int id { get; set; }
        public int plID { get; set; }
        public int rbid { get; set; }
        public int rID { get; set; }
        public int pID { get; set; }
        public int stg { get; set; }
        public bool ia { get; set; }

        public rsPlans() { }
        public rsPlans(int _id, int _plID, int _rbid, int _rID, int _pID, int _stg, bool _ia) {
            id = _id; plID = _plID; rbid = _rbid; rID = _rID; pID = _pID; stg = _stg; ia = _ia;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("id", typeof(System.Int32));
            dtName.Columns.Add("plID", typeof(System.Int32));
            dtName.Columns.Add("rbid", typeof(System.Int32));
            dtName.Columns.Add("rID", typeof(System.Int32));
            dtName.Columns.Add("pID", typeof(System.Int32));
            dtName.Columns.Add("stg", typeof(System.Int32));
            dtName.Columns.Add("ia", typeof(System.Boolean));
            return dtName;
        }
    }
    public class T_OMS_ProjectIndents {
        public int IndentReqId { get; set; }
        public int ProjectID { get; set; }
        public int ResourceID { get; set; } 
        public string ResourceName { get; set; }//added
        public string rUoM { get; set; }//changed
        public int TrfID { get; set; }//changed
        public decimal ProjectQty { get; set; }
        public decimal IndentedQty { get; set; }
        public decimal ProcuredQty { get; set; }
        public decimal ReqQty { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime RequiredOn { get; set; }
        public int IndentType { get; set; }
        public bool IsActive { get; set; }
        public int RequestBy { get; set; }
        public DateTime ReuestedOn { get; set; }
        public int IndentID { get; set; }
        public int RHD { get; set; }
        public int RcmdBy { get; set; }
        public DateTime RcmdOn { get; set; }
        public int ApprvdBy { get; set; }
        public DateTime ApprvOn { get; set; }
        public string Remarks { get; set; }
        public int Rejectby { get; set; }
        public DateTime RejectOn { get; set; }
        public int moduleID { get; set; }

        public T_OMS_ProjectIndents() { }
        public T_OMS_ProjectIndents(int _IndentReqId, int _ProjectID, int _ResourceID, decimal _ProjectQty, decimal _IndentedQty
            , decimal _ProcuredQty, decimal _ReqQty, DateTime _FromDate, DateTime _ToDate, DateTime _RequiredOn, string _UoM
            , int _IndentType, bool _IsActive, int _RequestBy, DateTime _ReuestedOn, int _IndentID, int _RHD, int _RcmdBy
            , DateTime _RcmdOn, int _ApprvdBy, DateTime _ApprvOn, int _IndentTransferID, string _Remarks, int _Rejectby, DateTime _RejectOn, int _mid) {
            IndentReqId = _IndentReqId; ProjectID = _ProjectID; ResourceID = _ResourceID; ProjectQty = _ProjectQty; IndentedQty = _IndentedQty;
            ProcuredQty = _ProcuredQty; ReqQty = _ReqQty; FromDate = _FromDate; ToDate = _ToDate; RequiredOn = _RequiredOn; rUoM = _UoM;
            IndentType = _IndentType; IsActive = _IsActive; RequestBy = _RequestBy; ReuestedOn = _ReuestedOn; IndentID = _IndentID; RHD = _RHD;
            RcmdBy = _RcmdBy; RcmdOn = _RcmdOn; ApprvdBy = _ApprvdBy; ApprvOn = _ApprvOn; TrfID = _IndentTransferID; Remarks = _Remarks;
            Rejectby = _Rejectby; RejectOn = _RejectOn; moduleID = _mid;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("IndentReqId", typeof(System.Int32));
            dtName.Columns.Add("ProjectID", typeof(System.Int32));
            dtName.Columns.Add("ResourceID", typeof(System.Int32));
            dtName.Columns.Add("ProjectQty", typeof(System.Double));
            dtName.Columns.Add("IndentedQty", typeof(System.Double));
            dtName.Columns.Add("ProcuredQty", typeof(System.Double));
            dtName.Columns.Add("ReqQty", typeof(System.Double));
            dtName.Columns.Add("FromDate", typeof(System.DateTime));
            dtName.Columns.Add("ToDate", typeof(System.DateTime));
            dtName.Columns.Add("RequiredOn", typeof(System.DateTime));
            dtName.Columns.Add("rUoM", typeof(System.String));
            dtName.Columns.Add("IndentType", typeof(System.Int32));
            dtName.Columns.Add("IsActive", typeof(System.Boolean));
            dtName.Columns.Add("RequestBy", typeof(System.Int32));
            dtName.Columns.Add("ReuestedOn", typeof(System.DateTime));
            dtName.Columns.Add("IndentID", typeof(System.Int32));
            dtName.Columns.Add("RHD", typeof(System.Int32));
            dtName.Columns.Add("RcmdBy", typeof(System.Int32));
            dtName.Columns.Add("RcmdOn", typeof(System.DateTime));
            dtName.Columns.Add("ApprvdBy", typeof(System.Int32));
            dtName.Columns.Add("ApprvOn", typeof(System.DateTime));
            dtName.Columns.Add("TrfID", typeof(System.Int32));
            dtName.Columns.Add("Remarks", typeof(System.String));
            dtName.Columns.Add("Rejectby", typeof(System.Int32));
            dtName.Columns.Add("RejectOn", typeof(System.DateTime));
            dtName.Columns.Add("moduleID", typeof(System.Int32));
            return dtName;
        }
    }
    public class ResDemand {
        public int bid { get; set; }
        public int rbid { get; set; }
        public int rid { get; set; }
        public string ResourceName { get; set; }
        public int rud { get; set; }
        public decimal ResQty { get; set; }
        public decimal ResRate { get; set; }
        public DateTime Earliest { get; set; }
        public DateTime Latest { get; set; }
        public int cat { get; set; }
        public string Category { get; set; }
        public int pcID { get; set; }
        public string CompName { get; set; }
        public int sts { get; set; }

        public ResDemand() { }
        public ResDemand(int _bid, int _rbid, int _rid, string _ResourceName, int _rud, decimal _ResQty, decimal _ResRate
            , DateTime _Earliest, DateTime _Latest, int _cat, string _Category, int _pcID, string _CompName, int _sts) {
            bid = _bid; rbid = _rbid; rid = _rid; ResourceName = _ResourceName; rud = _rud; ResQty = _ResQty; ResRate = _ResRate;
            Earliest = _Earliest; Latest = _Latest; cat = _cat; Category = _Category; pcID = _pcID; CompName = _CompName; sts = _sts;
        }

        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("bid", typeof(System.Int32));
            dtName.Columns.Add("rbid", typeof(System.Int32));
            dtName.Columns.Add("rid", typeof(System.Int32));
            dtName.Columns.Add("ResourceName", typeof(System.String));
            dtName.Columns.Add("rud", typeof(System.Int32));
            dtName.Columns.Add("ResQty", typeof(System.Double));
            dtName.Columns.Add("ResRate", typeof(System.Double));
            dtName.Columns.Add("Earliest", typeof(System.DateTime));
            dtName.Columns.Add("Latest", typeof(System.DateTime));
            dtName.Columns.Add("cat", typeof(System.Int32));
            dtName.Columns.Add("Category", typeof(System.String));
            dtName.Columns.Add("pcID", typeof(System.Int32));
            dtName.Columns.Add("CompName", typeof(System.String));
            dtName.Columns.Add("sts", typeof(System.Int32));
            return dtName;
        }
    }
    public class ResStPlan {
        public int gpID { get; set; }
        public string SectionName { get; set; }
        public int tID { get; set; }
        public string task { get; set; }
        public int pcID { get; set; }
        public string CompName { get; set; }
        public int stID { get; set; }
        public string subtask { get; set; }
        public int unit { get; set; }
        public string tUoM { get; set; }
        public decimal stQty { get; set; }
        public decimal plQty { get; set; }
        public int plDays { get; set; }
        public DateTime Earliest { get; set; }
        public DateTime Latest { get; set; }
        public int pls { get; set; }

        public ResStPlan() { }
        public ResStPlan(int _gpID, string _SectionName, int _tID, string _task, int _pcID, string _CompName
            , int _stID, string _subtask, int _unit, string _tUoM, decimal _stQty, decimal _plQty
            , int _plDays, DateTime _Earliest, DateTime _Latest, int _pls) {
            gpID = _gpID; SectionName = _SectionName; tID = _tID; task = _task; pcID = _pcID; CompName = _CompName;
            stID = _stID; subtask = _subtask; unit = _unit; tUoM = _tUoM; stQty = _stQty; plQty = _plQty;
            plDays = _plDays; Earliest = _Earliest; Latest = _Latest; pls = _pls;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("gpID", typeof(System.Int32));
            dtName.Columns.Add("SectionName", typeof(System.String));
            dtName.Columns.Add("tID", typeof(System.Int32));
            dtName.Columns.Add("task", typeof(System.String));
            dtName.Columns.Add("pcID", typeof(System.Int32));
            dtName.Columns.Add("CompName", typeof(System.String));
            dtName.Columns.Add("stID", typeof(System.Int32));
            dtName.Columns.Add("subtask", typeof(System.String));
            dtName.Columns.Add("unit", typeof(System.Int32));
            dtName.Columns.Add("tUoM", typeof(System.String));
            dtName.Columns.Add("stQty", typeof(System.Double));
            dtName.Columns.Add("plQty", typeof(System.Double));
            dtName.Columns.Add("plDays", typeof(System.Int32));
            dtName.Columns.Add("Earliest", typeof(System.DateTime));
            dtName.Columns.Add("Latest", typeof(System.DateTime));
            dtName.Columns.Add("pls", typeof(System.Int32));
            return dtName;
        }
    }
    public class ResStResPlan {
        public int plID { get; set; }
        public DateTime Date { get; set; }
        public decimal plQty { get; set; }
        public int id { get; set; }
        public int rbid { get; set; }
        public int rid { get; set; }
        public decimal rsDyQty { get; set; }
        public decimal rsQty { get; set; }
        public DateTime reqdDt { get; set; }

        public ResStResPlan() { }
        public ResStResPlan(int _plID, DateTime _Date, decimal _plQty, int _id, int _rbid, int _rid, decimal _rsDyQty, decimal _rsQty, DateTime _reqdDt) {
            plID = _plID; Date = _Date; plQty = _plQty; id = _id; rbid = _rbid; rid = _rid; rsDyQty = _rsDyQty; rsQty = _rsQty; reqdDt = _reqdDt;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("plID", typeof(System.Int32));
            dtName.Columns.Add("Date", typeof(System.DateTime));
            dtName.Columns.Add("plQty", typeof(System.Double));
            dtName.Columns.Add("id", typeof(System.Int32));
            dtName.Columns.Add("rbid", typeof(System.Int32));
            dtName.Columns.Add("rid", typeof(System.Int32));
            dtName.Columns.Add("rsDyQty", typeof(System.Double));
            dtName.Columns.Add("rsQty", typeof(System.Double));
            dtName.Columns.Add("reqdDt", typeof(System.DateTime));
            return dtName;
        }
    }

    public class StsInProd {
        public int BID { get; set; }
        public int stid { get; set; }
        public int tid { get; set; }
        public string SubTaskName { get; set; }
        public decimal QTY { get; set; }
        public DateTime stdt { get; set; }
        public DateTime eddt { get; set; }
        public decimal Nos { get; set; }
        public decimal B { get; set; }
        public decimal D { get; set; }
        public decimal L { get; set; }
        public decimal chFrom { get; set; }
        public decimal chTo { get; set; }
        public bool ia { get; set; }
        public int comp { get; set; }
        public int stg { get; set; }

        public StsInProd() { }
        public StsInProd(int _BID, int _stid, int _tid, string _SubTaskName, decimal _QTY, DateTime _stdt, DateTime _eddt
            , decimal _Nos, decimal _B, decimal _D, decimal _L, decimal _chFrom, decimal _chTo, bool _ia, int _comp, int _stg) {
            BID = _BID; stid = _stid; tid = _tid; SubTaskName = _SubTaskName; QTY = _QTY; stdt = _stdt; eddt = _eddt;
            Nos = _Nos; B = _B; D = _D; L = _L; chFrom = _chFrom; chTo = _chTo; ia = _ia; comp = _comp; stg = _stg;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("BID", typeof(System.Int32));
            dtName.Columns.Add("stid", typeof(System.Int32));
            dtName.Columns.Add("tid", typeof(System.Int32));
            dtName.Columns.Add("SubTaskName", typeof(System.String));
            dtName.Columns.Add("QTY", typeof(System.Double));
            dtName.Columns.Add("stdt", typeof(System.DateTime));
            dtName.Columns.Add("eddt", typeof(System.DateTime));
            dtName.Columns.Add("Nos", typeof(System.Double));
            dtName.Columns.Add("B", typeof(System.Double));
            dtName.Columns.Add("D", typeof(System.Double));
            dtName.Columns.Add("L", typeof(System.Double));
            dtName.Columns.Add("chFrom", typeof(System.Double));
            dtName.Columns.Add("chTo", typeof(System.Double));
            dtName.Columns.Add("ia", typeof(System.Boolean));
            dtName.Columns.Add("comp", typeof(System.Int32));
            dtName.Columns.Add("stg", typeof(System.Int32));
            return dtName;
        }

    }

    public class taskIndentByPRID {
        public int IndentReqID { get; set; }
        public int TaskID { get; set; }
        public int SubTaskID { get; set; }
        public string Subtask { get; set; }
        public decimal Qty { get; set; }
        public int SAID { get; set; }
        public DateTime ReqDate { get; set; }

        public taskIndentByPRID() { }
        public taskIndentByPRID(int _IndentReqID, int _TaskID, int _SubTaskID, string _Subtask, decimal _Qty, int _SAID, DateTime _ReqDate) {
            IndentReqID = _IndentReqID; TaskID = _TaskID; SubTaskID = _SubTaskID; Subtask = _Subtask; Qty = _Qty; SAID = _SAID; ReqDate = _ReqDate;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("IndentReqID", typeof(System.Int32));
            dtName.Columns.Add("TaskID", typeof(System.Int32));
            dtName.Columns.Add("SubTaskID", typeof(System.Int32));
            dtName.Columns.Add("Subtask", typeof(System.String));
            dtName.Columns.Add("Qty", typeof(System.Double));
            dtName.Columns.Add("SAID", typeof(System.Int32));
            dtName.Columns.Add("ReqDate", typeof(System.DateTime));
            return dtName;
        }
    }
}


