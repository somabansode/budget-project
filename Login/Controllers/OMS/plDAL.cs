﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Login.Controllers.OMS {
    public class plDAL {

        public static async Task<List<scPlan>> ReadscPlanAsync(int pid, int tid) {
            List<scPlan> clscPlan = new List<scPlan>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "oms.scPlan";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pid", pid));
                    cmd.Parameters.Add(new SqlParameter("@tid", tid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            scPlan t = new scPlan();
                            t.tid = rdr["tid"] == DBNull.Value ? -1 : (int)rdr["tid"];
                            t.rid = rdr["rid"] == DBNull.Value ? -1 : (int)rdr["rid"];
                            t.ResourceName = rdr["ResourceName"] == DBNull.Value ? "" : rdr["ResourceName"].ToString();
                            t.POID = rdr["POID"] == DBNull.Value ? -1 : (int)rdr["POID"];
                            t.poDate = rdr["poDate"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["poDate"]);
                            t.PoName = rdr["PoName"] == DBNull.Value ? "" : rdr["PoName"].ToString();
                            t.rUoM = rdr["rUoM"] == DBNull.Value ? -1 : (int)rdr["rUoM"];
                            t.pUoM = rdr["pUoM"] == DBNull.Value ? -1 : (int)rdr["pUoM"];
                            t.Cost = rdr["Cost"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["Cost"]);
                            t.ratID = rdr["ratID"] == DBNull.Value ? -1 : (int)rdr["ratID"];
                            clscPlan.Add(t);
                        }
                    }
                }
                con.Close();
                return clscPlan;
            }
        }
        public static async Task<List<StockJournalDetail>> ReadStockJournalDetailAsync(int cid, int? pid) {
            List<StockJournalDetail> clStockJournalDetail = new List<StockJournalDetail>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "select * from mms.StockJournalDetail where cid = @cid and pid = isnull(@pid,pid)";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    //cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    cmd.Parameters.Add(new SqlParameter("@pid", pid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            StockJournalDetail t = new StockJournalDetail();
                            t.cid = rdr["cid"] == DBNull.Value ? -1 : (int)rdr["cid"];
                            t.ws = rdr["ws"] == DBNull.Value ? -1 : (int)rdr["ws"];
                            t.pid = rdr["pid"] == DBNull.Value ? -1 : (int)rdr["pid"];
                            t.trID = rdr["trID"] == DBNull.Value ? -1 : (int)rdr["trID"];
                            t.PDID = rdr["PDID"] == DBNull.Value ? -1 : (int)rdr["PDID"];
                            t.GDID = rdr["GDID"] == DBNull.Value ? -1 : (int)rdr["GDID"];
                            t.rid = rdr["rid"] == DBNull.Value ? -1 : (int)rdr["rid"];
                            t.uom = rdr["uom"] == DBNull.Value ? -1 : (int)rdr["uom"];
                            t.Rcvd = rdr["Rcvd"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["Rcvd"]);
                            t.eClB = rdr["eClB"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["eClB"]);
                            t.pClB = rdr["pClB"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["pClB"]);
                            t.Issued = rdr["Issued"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["Issued"]);
                            clStockJournalDetail.Add(t);
                        }
                    }
                }
                con.Close();
                return clStockJournalDetail;
            }
        }
        public static async Task<List<matPlanReq>> ReadmatPlanReqAsync(int tid, List<int?> sts, int bid) {
            List<matPlanReq> clmatPlanReq = new List<matPlanReq>();
            DataTable dtSts = common.idList2TableNullable(sts);
            //if(sts != null || sts.Count > 0) dtSts =  common.idList2TableNullable(sts);
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "oms.matPlanReq";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@tid", tid));
                    cmd.Parameters.Add(new SqlParameter("@sts", dtSts));
                    cmd.Parameters.Add(new SqlParameter("@bid", bid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            matPlanReq t = new matPlanReq();
                            t.stid = rdr["stid"] == DBNull.Value ? -1 : (int)rdr["stid"];
                            t.SubtaskName = rdr["SubtaskName"] == DBNull.Value ? "" : rdr["SubtaskName"].ToString();
                            t.Unit = rdr["Unit"] == DBNull.Value ? -1 : (int)rdr["Unit"];
                            t.QTY = rdr["QTY"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["QTY"]);
                            t.Date = rdr["Date"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["Date"]);
                            t.PlQty = rdr["PlQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["PlQty"]);
                            t.rid = rdr["rid"] == DBNull.Value ? -1 : (int)rdr["rid"];
                            t.ResourceName = rdr["ResourceName"] == DBNull.Value ? "" : rdr["ResourceName"].ToString();
                            t.Au_ID = rdr["uom"] == DBNull.Value ? -1 : (int)rdr["uom"];
                            t.rdQty = rdr["rdQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["rdQty"]);
                            clmatPlanReq.Add(t);
                        }
                    }
                }
                con.Close();
                return clmatPlanReq;
            }
        }
        public static async Task<List<object>> ReadmatAvailability2Async(int pid, List<int> rids) {
            List<matAvailability2Grid> clmatAvailability2 = new List<matAvailability2Grid>();
            List<gdnBals> clGdnBals = new List<gdnBals>();
            List<object> bothTables = new List<object>();

            DataSet ds = new DataSet();
            DataTable dt = common.idList2Table(rids);
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "oms.matAvailability2grid";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pid", pid));
                    cmd.Parameters.Add(new SqlParameter("@rids", dt));
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(ds);
                    foreach(DataRow rdr in ds.Tables[0].Rows) {
                        matAvailability2Grid t = new matAvailability2Grid();
                        t.RID = rdr["RID"] == DBNull.Value ? -1 : (int)rdr["RID"];
                        t.ResourceName = rdr["ResourceName"] == DBNull.Value ? "" : rdr["ResourceName"].ToString();
                        t.POID = rdr["POID"] == DBNull.Value ? -1 : (int)rdr["POID"];
                        t.POName = rdr["POName"] == DBNull.Value ? "" : rdr["POName"].ToString();
                        t.pUoM = rdr["pUoM"] == DBNull.Value ? -1 : (int)rdr["pUoM"];
                        t.Rate = rdr["Rate"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["Rate"]);
                        t.ClBal = rdr["ClBal"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["ClBal"]);
                        clmatAvailability2.Add(t);
                    }
                    foreach(DataRow rdr in ds.Tables[1].Rows) {
                        gdnBals t = new gdnBals();
                        t.POID = rdr["POID"] == DBNull.Value ? -1 : (int)rdr["POID"];
                        t.GDID = rdr["GDID"] == DBNull.Value ? -1 : (int)rdr["GDID"];
                        t.ClBal = rdr["ClBal"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["ClBal"]);
                        t.Loan = rdr["Loan"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["Loan"]);
                        t.Asgd = rdr["Asgd"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["Asgd"]);
                        clGdnBals.Add(t);
                    }
                }
                con.Close();
                bothTables.Add(clmatAvailability2);
                bothTables.Add(clGdnBals);
                return bothTables;
            }
        }
        public static async Task<List<object>> matPlanExecuteAsync(List<int> giids, List<int> stids, int bid, int userID) {
            try {
                DataTable dtGiids = common.idList2Table(giids);
                DataTable dtStids = common.idList2Table(stids);
                List<plannedSTs> clPlannedSTs = new List<plannedSTs>();
                List<plannedSTs> clPartialPlannedSTs = new List<plannedSTs>();
                List<matPlanReq> clUnPlanned = new List<matPlanReq>();
                List<stockStatus> clStockStatus = new List<stockStatus>();
                List<object> clAll = new List<object>();
                DataSet ds = new DataSet();
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("createPlans"));
                    string queryString = "oms.matPlanExecute";
                    using(SqlCommand cmd = new SqlCommand(queryString, con, tran)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@giids", dtGiids));
                        cmd.Parameters.Add(new SqlParameter("@stids", dtStids));
                        cmd.Parameters.Add(new SqlParameter("@bid", bid));
                        cmd.Parameters.Add(new SqlParameter("@userID", userID));
                        cmd.Parameters.Add(new SqlParameter("@msg", SqlDbType.VarChar, 200)).Direction = ParameterDirection.Output;
                        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                        adapter.Fill(ds);
                    }
                    if(ds.Tables.Count > 0) {
                        foreach(DataRow rdr in ds.Tables[0].Rows) {//planned	 @stResAsg  >> oms.matPlanExecute	
                            plannedSTs t = new plannedSTs();
                            t.stid = rdr["stid"] == DBNull.Value ? -1 : (int)rdr["stid"];
                            t.rid = rdr["rid"] == DBNull.Value ? -1 : (int)rdr["rid"];
                            t.dt = rdr["dt"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["dt"]);
                            t.pdid = rdr["pdid"] == DBNull.Value ? -1 : (int)rdr["pdid"];
                            t.gdid = rdr["gdid"] == DBNull.Value ? -1 : (int)rdr["gdid"];
                            t.rdQty = rdr["rdQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["rdQty"]);
                            t.rud = rdr["rud"] == DBNull.Value ? -1 : (int)rdr["rud"];
                            t.rate = rdr["rate"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["rate"]);
                            t.ok = rdr["ok"] == DBNull.Value ? "" : rdr["ok"].ToString();
                            clPlannedSTs.Add(t);
                        }
                        foreach(DataRow rdr in ds.Tables[1].Rows) { //partially @stResAsg  >>	oms.matPlanExecute
                            plannedSTs t = new plannedSTs();
                            t.stid = rdr["stid"] == DBNull.Value ? -1 : (int)rdr["stid"];
                            t.rid = rdr["rid"] == DBNull.Value ? -1 : (int)rdr["rid"];
                            t.dt = rdr["dt"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["dt"]);
                            t.pdid = rdr["pdid"] == DBNull.Value ? -1 : (int)rdr["pdid"];
                            t.gdid = rdr["gdid"] == DBNull.Value ? -1 : (int)rdr["gdid"];
                            t.rdQty = rdr["rdQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["rdQty"]);
                            t.rud = rdr["rud"] == DBNull.Value ? -1 : (int)rdr["rud"];
                            t.rate = rdr["rate"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["rate"]);
                            t.ok = rdr["ok"] == DBNull.Value ? "" : rdr["ok"].ToString();
                            clPartialPlannedSTs.Add(t);
                        }
                        foreach(DataRow rdr in ds.Tables[2].Rows) {//unplanned @reqTbl >>	oms.matPlanExecute	
                            matPlanReq t = new matPlanReq();
                            t.stid = rdr["stid"] == DBNull.Value ? -1 : (int)rdr["stid"];
                            t.SubtaskName = rdr["SubtaskName"] == DBNull.Value ? "" : rdr["SubtaskName"].ToString();
                            t.Unit = rdr["Unit"] == DBNull.Value ? -1 : (int)rdr["Unit"];
                            t.QTY = rdr["QTY"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["QTY"]);
                            t.Date = rdr["Date"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["Date"]);
                            t.PlQty = rdr["PlQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["PlQty"]);
                            t.rid = rdr["rid"] == DBNull.Value ? -1 : (int)rdr["rid"];
                            t.ResourceName = rdr["ResourceName"] == DBNull.Value ? "" : rdr["ResourceName"].ToString();
                            t.Au_ID = rdr["rud"] == DBNull.Value ? -1 : (int)rdr["rud"];
                            t.rdQty = rdr["rdQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["rdQty"]);
                            clUnPlanned.Add(t);
                        }
                        foreach(DataRow rdr in ds.Tables[3].Rows) { //stock @avaTbl	>>	oms.matPlanExecute	
                            stockStatus t = new stockStatus();
                            t.rid = rdr["rid"] == DBNull.Value ? -1 : (int)rdr["rid"];
                            t.pdid = rdr["pdid"] == DBNull.Value ? -1 : (int)rdr["pdid"];
                            t.gdid = rdr["gdid"] == DBNull.Value ? -1 : (int)rdr["gdid"];
                            t.rud = rdr["rud"] == DBNull.Value ? -1 : (int)rdr["rud"];
                            t.rate = rdr["rate"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["rate"]);
                            t.avaQty = rdr["avaQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["avaQty"]);
                            t.asgQty = rdr["asgQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["asgQty"]);
                            clStockStatus.Add(t);
                        }
                        clAll.Add(clPlannedSTs);
                        clAll.Add(clPartialPlannedSTs);
                        clAll.Add(clUnPlanned);
                        clAll.Add(clStockStatus);
                        tran.Commit(); 
                        con.Close();
                        return clAll;
                    } else {
                        tran.Rollback(); return new List<object> { "falied", "no records found!" };  
                    }
                }
            } catch(Exception ex) { return new List<object> { "falied", ex.Message }; }
        }
        public static async Task<string> deleteAssignedMaterialsAsync(int pid,int resID,int stid, DateTime dtFr, DateTime dtTo) {
            string result = string.Empty;
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "oms.deleteAssignedMaterials";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pid", pid));
                    cmd.Parameters.Add(new SqlParameter("@resID", resID));
                    cmd.Parameters.Add(new SqlParameter("@stid", stid));
                    cmd.Parameters.Add(new SqlParameter("@dtFr", dtFr));
                    cmd.Parameters.Add(new SqlParameter("@dtTo", dtTo));
                    cmd.Parameters.Add(new SqlParameter("@result", SqlDbType.VarChar, 200)).Direction = ParameterDirection.Output;
                    await cmd.ExecuteNonQueryAsync();
                    result = cmd.Parameters["@result"].Value.ToString();
                }
                con.Close();
                return result;
            }
         }

    }

    public class scPlan {
        public int tid { get; set; }
        public int rid { get; set; }
        public string ResourceName { get; set; }
        public int POID { get; set; }
        public DateTime poDate { get; set; }
        public string PoName { get; set; }
        public int rUoM { get; set; }
        public int pUoM { get; set; }
        public decimal Cost { get; set; }
        public int ratID { get; set; }

        public scPlan() { }
        public scPlan(int _tid, int _rid, string _ResourceName, int _POID, DateTime _poDate, string _PoName, int _rUoM, int _pUoM, decimal _Cost, int _ratID) {
            tid = _tid; rid = _rid; ResourceName = _ResourceName; POID = _POID; poDate = _poDate; PoName = _PoName; rUoM = _rUoM; pUoM = _pUoM; Cost = _Cost; ratID = _ratID;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("tid", typeof(System.Int32));
            dtName.Columns.Add("rid", typeof(System.Int32));
            dtName.Columns.Add("ResourceName", typeof(System.String));
            dtName.Columns.Add("POID", typeof(System.Int32));
            dtName.Columns.Add("poDate", typeof(System.DateTime));
            dtName.Columns.Add("PoName", typeof(System.String));
            dtName.Columns.Add("rUoM", typeof(System.Int32));
            dtName.Columns.Add("pUoM", typeof(System.Int32));
            dtName.Columns.Add("Cost", typeof(System.Double));
            dtName.Columns.Add("ratID", typeof(System.Int32));
            return dtName;
        }
    }
    public class StockJournalDetail {
        public int cid { get; set; }
        public int ws { get; set; }
        public int pid { get; set; }
        public int trID { get; set; }
        public int PDID { get; set; }
        public int GDID { get; set; }
        public int rid { get; set; }
        public int uom { get; set; }
        public decimal Rcvd { get; set; }
        public decimal eClB { get; set; }
        public decimal pClB { get; set; }
        public decimal Issued { get; set; }

        public StockJournalDetail() { }
        public StockJournalDetail(int _cid, int _ws, int _pid, int _trID, int _PDID, int _GDID, int _rid, int _uom, decimal _Rcvd
            , decimal _eClB, decimal _pClB, decimal _Issued) {
            cid = _cid; ws = _ws; pid = _pid; trID = _trID; PDID = _PDID; GDID = _GDID; rid = _rid; uom = _uom; Rcvd = _Rcvd;
            eClB = _eClB; pClB = _pClB; Issued = _Issued;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("cid", typeof(System.Int32));
            dtName.Columns.Add("ws", typeof(System.Int32));
            dtName.Columns.Add("pid", typeof(System.Int32));
            dtName.Columns.Add("trID", typeof(System.Int32));
            dtName.Columns.Add("PDID", typeof(System.Int32));
            dtName.Columns.Add("GDID", typeof(System.Int32));
            dtName.Columns.Add("rid", typeof(System.Int32));
            dtName.Columns.Add("uom", typeof(System.Int32));
            dtName.Columns.Add("Rcvd", typeof(System.Double));
            dtName.Columns.Add("eClB", typeof(System.Double));
            dtName.Columns.Add("pClB", typeof(System.Double));
            dtName.Columns.Add("Issued", typeof(System.Double));
            return dtName;
        }
    }
    public class matPlanReq {
        public int stid { get; set; }
        public int Unit { get; set; }
        public string SubtaskName { get; set; }
        public decimal QTY { get; set; }
        public DateTime Date { get; set; }
        public decimal PlQty { get; set; }
        public int rid { get; set; }
        public string ResourceName { get; set; }
        public int Au_ID { get; set; }
        public decimal rdQty { get; set; }

        public matPlanReq() { }
        public matPlanReq(int _stid, string _subtaskName, int _Unit, decimal _QTY, DateTime _Date, decimal _PlQty, int _rid
            , string _ResourceName, int _Au_ID, decimal _rdQty) {
            stid = _stid; SubtaskName = _subtaskName; Unit = _Unit; QTY = _QTY; Date = _Date; PlQty = _PlQty; rid = _rid;
            ResourceName = _ResourceName; Au_ID = _Au_ID; rdQty = _rdQty;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("stid", typeof(System.Int32));
            dtName.Columns.Add("SubtaskName", typeof(System.String));
            dtName.Columns.Add("Unit", typeof(System.Int32));
            dtName.Columns.Add("QTY", typeof(System.Double));
            dtName.Columns.Add("Date", typeof(System.DateTime));
            dtName.Columns.Add("PlQty", typeof(System.Double));
            dtName.Columns.Add("rid", typeof(System.Int32));
            dtName.Columns.Add("ResourceName", typeof(System.String));
            dtName.Columns.Add("Au_ID", typeof(System.Int32));
            dtName.Columns.Add("rdQty", typeof(System.Decimal));
            return dtName;
        }


    }

    public class planCompoundObject {
        public int tid { get; set; }
        public List<stWithPl> unPlSts { get; set; }
        public List<BTA> bTAs { get; set; }
        public List<mtPCs> mTPCs { get; set; }
        //public List<planCompoundObject() { }
        //public planCompoundObject(int _tid, List<stWithPl> _unPlSts, List<BTA> _bTAs , List<mtPCs> _mTPCs, List<pCoeff> _pCoeffs) { 
        //    tid = _tid; unPlSts= _unPlSts; bTAs =_bTAs;  mTPCs = _mTPCs; pCoeffs =_pCoeffs;
        //}
    }
    public class matAvailability2Grid {
        public int RID { get; set; }
        public string ResourceName { get; set; }
        public int POID { get; set; }
        public string POName { get; set; }
        public int pUoM { get; set; }
        public decimal Rate { get; set; }
        public decimal ClBal { get; set; }

        public matAvailability2Grid() { }
        public matAvailability2Grid(int _RID, string _ResourceName, int _POID, string _POName, int _pUoM, decimal _Rate, decimal _ClBal) {
            RID = _RID; ResourceName = _ResourceName; POID = _POID; POName = _POName; pUoM = _pUoM; Rate = _Rate; ClBal = _ClBal;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            DataColumn RID = new DataColumn();
            RID.DataType = System.Type.GetType("System.Int32");
            RID.ColumnName = "RID";
            RID.AutoIncrement = true;
            dtName.Columns.Add(RID);
            dtName.Columns.Add("RID", typeof(System.Int32));
            dtName.Columns.Add("ResourceName", typeof(System.String));
            dtName.Columns.Add("POID", typeof(System.Int32));
            dtName.Columns.Add("POName", typeof(System.String));
            dtName.Columns.Add("pUoM", typeof(System.Int32));
            dtName.Columns.Add("Rate", typeof(System.Decimal));
            dtName.Columns.Add("ClBal", typeof(System.Decimal));

            DataColumn[] keys = new DataColumn[1];
            keys[0] = RID;
            dtName.PrimaryKey = keys;
            return dtName;
        }
    }
    public class gdnBals {
        public int POID { get; set; }
        public int GDID { get; set; }
        public decimal ClBal { get; set; }
        public decimal Loan { get; set; }
        public decimal Asgd { get; set; }
    }

    public class plannedSTs {
        public int stid { get; set; }
        public int rid { get; set; }
        public DateTime dt { get; set; }
        public int pdid { get; set; }
        public int gdid { get; set; }
        public decimal rdQty { get; set; }
        public int rud { get; set; }
        public decimal rate { get; set; }
        public string ok { get; set; }

        public plannedSTs() { }
        public plannedSTs(int _stid, int _rid, DateTime _dt, int _pdid, int _gdid, decimal _rdQty
            , int _rud, decimal _rate, string _ok) {
            stid = _stid; rid = _rid; dt = _dt; pdid = _pdid; gdid = _gdid; rdQty = _rdQty;
            rud = _rud; rate = _rate; ok = _ok;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("stid", typeof(System.Int32));
            dtName.Columns.Add("rid", typeof(System.Int32));
            dtName.Columns.Add("dt", typeof(System.DateTime));
            dtName.Columns.Add("pdid", typeof(System.Int32));
            dtName.Columns.Add("gdid", typeof(System.Int32));
            dtName.Columns.Add("rdQty", typeof(System.Double));
            dtName.Columns.Add("rud", typeof(System.Int32));
            dtName.Columns.Add("rate", typeof(System.Double));
            dtName.Columns.Add("ok", typeof(System.String));
            return dtName;
        }
    }

    public class stockStatus {
        public int rid { get; set; }
        public int pdid { get; set; }
        public int gdid { get; set; }
        public int rud { get; set; }
        public decimal rate { get; set; }
        public decimal avaQty { get; set; }
        public decimal asgQty { get; set; }

        public stockStatus() { }
        public stockStatus(int _rid, int _pdid, int _gdid, int _rud, decimal _rate, decimal _avaQty, decimal _asgQty) {
            rid = _rid; pdid = _pdid; gdid = _gdid; rud = _rud; rate = _rate; avaQty = _avaQty; asgQty = _asgQty;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("rid", typeof(System.Int32));
            dtName.Columns.Add("pdid", typeof(System.Int32));
            dtName.Columns.Add("gdid", typeof(System.Int32));
            dtName.Columns.Add("rud", typeof(System.Int32));
            dtName.Columns.Add("rate", typeof(System.Double));
            dtName.Columns.Add("avaQty", typeof(System.Double));
            dtName.Columns.Add("asgQty", typeof(System.Double));
            return dtName;
        }


    }
}
//                                                                                                                                                                               this.gridOptions.columnDefs = [
//{ headerName: 'Stid', field: 'stid', width: 70, checkboxSelection: true, cellStyle: { textAlign: 'right' } },
//{ headerName: 'Rid', field: 'rid', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'Dt', field: 'dt', width: 100, cellStyle: { textAlign: 'center' } },
//{ headerName: 'Pdid', field: 'pdid', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'Gdid', field: 'gdid', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'RdQty', field: 'rdQty', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'Rud', field: 'rud', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'Rate', field: 'rate', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'Ok', field: 'ok', width: 200, cellStyle: { textAlign: 'left' } },

//];
//export interface plannedSTs {
//    stid: number; 
//	rid: number; 
//	dt: Date; 
//	pdid: number; 
//	gdid: number; 
//	rdQty: number; 
//	rud: number; 
//	rate: number; 
//	ok: string; 
// }

//                                                                                                                                                                        this.gridOptions.columnDefs = [
//{ headerName: 'Rid', field: 'rid', width: 70, checkboxSelection: true, cellStyle: { textAlign: 'right' } },
//{ headerName: 'Pdid', field: 'pdid', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'Gdid', field: 'gdid', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'Rud', field: 'rud', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'Rate', field: 'rate', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'AvaQty', field: 'avaQty', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'AsgQty', field: 'asgQty', width: 70, cellStyle: { textAlign: 'right' } },

//];
//export interface stockStatus {
//    rid: number; 
//	pdid: number; 
//	gdid: number; 
//	rud: number; 
//	rate: number; 
//	avaQty: number; 
//	asgQty: number; 
// }