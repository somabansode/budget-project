﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
namespace Login.Controllers {
    public class hmsMastersController :ApiController {
        [HttpGet]
        [Route("api/hmsMasters/countries")]
        public async Task<HttpResponseMessage> Countries() { // http://localhost/Login/api/hmsMasters/countries
            try {
                List<t_g_countryMaster> clCountries = await Task.Run(() => DAL.ReadCountriesAsync());
                return Request.CreateResponse(HttpStatusCode.OK, clCountries);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet] [Route("api/hmsMasters/currencies")]
        public async Task<HttpResponseMessage> Currencies() { // http://localhost/Login/api/hmsMasters/currencies
            try {
                List<t_g_currencyMaster> clCurrencies = await Task.Run(() => DAL.ReadCurrenciesAsync());
                return Request.CreateResponse(HttpStatusCode.OK, clCurrencies);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet] [Route("api/hmsMasters/countryInfo")]
        public async Task<HttpResponseMessage> CountryInfo() { // http://localhost/Login/api/hmsMasters/countryInfo
            try {
                List<t_g_countryInfo> clCountryInfo = await Task.Run(() => DAL.ReadCountryInfoAsync());
                return Request.CreateResponse(HttpStatusCode.OK, clCountryInfo);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet] [Route("api/hmsMasters/states")]
        public async Task<HttpResponseMessage> States(int countryID) { // http://localhost/Login/api/hmsMasters/states?countryID=1
            try {
                List<t_g_stateMaster> clstates = await Task.Run(() => DAL.ReadStatesAsync(countryID));
                return Request.CreateResponse(HttpStatusCode.OK, clstates);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet] [Route("api/hmsMasters/cities")]
        public async Task<HttpResponseMessage> Cities(int stateID) { // http://localhost/Login/api/hmsMasters/cities?stateID=1
            try {
                List<t_g_cityMaster> clstates = await Task.Run(() => DAL.ReadCitiesAsync(stateID));
                return Request.CreateResponse(HttpStatusCode.OK, clstates);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        //---------------------------------------------
        [HttpGet] [Route("api/hmsMasters/worksites")]
        public async Task<HttpResponseMessage> worksites(int cid) { // http://localhost/Login/api/hmsMasters/worksites?cid=1
            try {
                List<worksite> list = await Task.Run(()=> DAL.ReadWorksitesAsync(cid));
                return Request.CreateResponse(HttpStatusCode.OK, list);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost] [Route("api/hmsMasters/worksites")]
        public async Task<HttpResponseMessage> Post([FromBody]ws WS) {
            //http://localhost/Login/api/hmsMasters/worksites  //{WorkSite:"attapur1",Address:"Hyd",STATEID:1,CityID:1,cid:1}
            worksite addedWs = await Task.Run(() => DAL.CreateWorksitesAsync(WS));
            if(addedWs !=null) {
                return Request.CreateResponse(HttpStatusCode.OK, addedWs);
            } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Failure to insert record");
        }
        [HttpPut] [Route("api/hmsMasters/worksites")]
        public async Task<HttpResponseMessage> Put(int id, [FromBody]ws WS) {
            string res = await Task.Run(() => DAL.UpdateWorksiteAsync(id,WS));
            if(res == "success") {
                return Request.CreateResponse(HttpStatusCode.OK, "success");
            } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Failure to update record");
        }
        [HttpDelete] [Route("api/hmsMasters/worksites")]
        public async Task<HttpResponseMessage> Delete(int id) {
            string res = await Task.Run(() => DAL.DeleteWorksiteAsync(id));
            if(res == "success") {
                return Request.CreateResponse(HttpStatusCode.OK, "deleted");
            } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Failed to delete the record");
            
        }
        //-------------------------------------------
        [HttpGet] [Route("api/hmsMasters/employeeType")]
        public async Task< HttpResponseMessage> ReadEemployeeTypeAsync(int cid) {
            try {  // http://localhost/Login/api/hmsMasters/employeeType?cid=1  
                List<EmployeeType> list = await Task.Run(()=> DAL.ReadEemployeeTypeAsync(cid));
                return Request.CreateResponse(HttpStatusCode.OK, list);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet] [Route("api/hmsMasters/trades")]
        public async Task<HttpResponseMessage> ReadEemployeeTradesAsync(int cid) {
            try {  //http://localhost/Login/api/hmsMasters/trades?cid=1  
                List<T_G_CategoryMaster> list = await Task.Run(()=> DAL.ReadEmployeeTradesAsync(cid));
                return Request.CreateResponse(HttpStatusCode.OK, list);
            } catch(Exception ex) {
               return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet] [Route("api/hmsMasters/designations")]
        public async Task<HttpResponseMessage> Designations(int cid) { // http://localhost/Login/api/hmsMasters/designations?cid=1
            try {
                List<T_G_DesignationMaster> list = await Task.Run(()=> DAL.ReadDesignationsAsync(cid));
                return Request.CreateResponse(HttpStatusCode.OK, list);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet] [Route("api/hmsMasters/departments")]
        public async Task<HttpResponseMessage> Departments(int cid) { // http://localhost/Login/api/hmsMasters/departments?cid=1
            try {
                List<T_G_DepartmentMaster> clDeptts = await Task.Run(()=> DAL.ReadDepartmentAsync(cid)); 
                return Request.CreateResponse(HttpStatusCode.OK, clDeptts);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet] [Route("api/hmsMasters/empNature")]
        public async Task<HttpResponseMessage> EmpNature(int cid) { // http://localhost/Login/api/hmsMasters/empNature?cid=1
            try {
                List<T_HR_NatureOfEmployment> clNature = await Task.Run(()=> DAL.ReadNatureOfEmploymentAsync(cid));  
                return Request.CreateResponse(HttpStatusCode.OK, clNature);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet] [Route("api/hmsMasters/shifts")]
        public async Task<HttpResponseMessage> Shifts(int cid) { // http://localhost/Login/api/hmsMasters/shifts?cid=1
            try {
                List<T_G_Shifts> clShifts = await Task.Run(() => DAL.ReadShiftsAsync(cid));
                return Request.CreateResponse(HttpStatusCode.OK, clShifts);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet] [Route("api/hmsMasters/banks")]
        public async Task<HttpResponseMessage> Banks() { // http://localhost/Login/api/hmsMasters/banks
            try {
                List<T_G_BankMaster> clBanks = await Task.Run(()=> DAL.ReadBanksAsync());
               return Request.CreateResponse(HttpStatusCode.OK, clBanks);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet] [Route("api/hmsMasters/bankBranches")]
        public async Task<HttpResponseMessage> BankBranches(int bankID) { // http://localhost/Login/api/hmsMasters/bankBranches?bankID=1
            try {
                List<T_G_BankBranches> clBranches = await Task.Run(() => DAL.xxxReadBankBranchesByBankAsync(bankID) );
                return Request.CreateResponse(HttpStatusCode.OK, clBranches);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet] [Route("api/hmsMasters/allBankBranches")]
        public async Task<HttpResponseMessage> AllBankBranches() { //, SqlConnection conn, SqlTransaction tran 
            try {//http://localhost/Login/api/hmsMasters/allBankBranches 
                 List<BankBranches> clBranches = await Task.Run(() => DAL.ReadAllBankBranchesAsync() );
                return Request.CreateResponse(HttpStatusCode.OK, clBranches);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet] [Route("api/hmsMasters/AppDetails")]
        public async Task<HttpResponseMessage> AppDetails(int cid) { // http://localhost/Login/api/hmsMasters/AppDetails?cid=1
            try {
                List<T_HR_AppDetails> clAppdets = await Task.Run(() => DAL.ReadEmpAppDetailsAsync(cid));
                return Request.CreateResponse(HttpStatusCode.OK, clAppdets);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost] [Route("api/hmsMasters/AppDetails")]
        public async Task<HttpResponseMessage> BulkInsertAppDetails([FromBody]List<T_HR_AppDetails> clEmpAppDetails, int cid, int userID) {
            try { 
                // http://localhost/Login/api/hmsMasters/AppDetails?cid=1&userID=1
                // [{
                //	EmpId:1289,PerAddress:'B101, Street x1',PerPhone:'9999988888',PerPin:'55555',permanentCity:2,SameAddress:0
                //	,ResAddress:'B101, Street x1',ResPhone:'9999988888',ResPin:'55555',ResidentCity:2,Bloodgroup:'O+',Salary:100000
                //	,DOJ:'2010-10-25',PassportNo:'Y1234567',Issuer:'Embassy of India',IssuePlace:'Delhi',IssueDate:'2010-10-25'
                //	,ExpiryDate:'2010-10-25',PasportRemarks:'none',EmergContact:'9999988888',EmergContactName:'John Doe',EmergRelation:'Friend'
                //	,EmergEmail:'emergency@gmail.com',EmergResiPhone:'9999988888',DateOfMarriage:'2010-10-25',MaritalStat:1
                //	,OREmergContact:'9999988888',OREmergContactName:'John Doe',OREmergRelation:'Friend',OREmergEmail:'emergency@gmail.com'
                //	,OREmergResiPhone:'9999988888',EOC_date:'2010-10-25'
                //}]
                string result = await Task.Run(() => DAL.CreateBulkEmpAppDetailsAsync(clEmpAppDetails, cid, userID));
                if(result == clEmpAppDetails.Count + " record(s) are inserted") {
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result); 
                 
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet] [Route("api/hmsMasters/allCities")]
        public async Task<HttpResponseMessage> AllCities() {
            try { // http://localhost/Login/api/hmsMasters/AllCities
                var clCities = await Task.Run(() => DAL.clCityStatCountryAsync());
                return Request.CreateResponse(HttpStatusCode.OK, clCities);
            } catch(Exception ex) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message); }
        }
        [HttpGet] [Route("api/hmsMasters/allStates")]
        public async Task<HttpResponseMessage> allStates() {
            try { // http://localhost/Login/api/hmsMasters/allStates
                //List<StatCountry> clStates = clStateCountry();
                var clStates = await Task.Run(() => DAL.clStateCountryAsync());
                return Request.CreateResponse(HttpStatusCode.OK, clStates);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }

        }
        [HttpGet] [Route("api/hmsMasters/insurance")]
        public async Task<HttpResponseMessage> Insurance(int cid) {// http://localhost/Login/api/hmsMasters/insurance?cid=1
            try {
                var clInsurance = await Task.Run(() => DAL.ReadInsuranceAsync(cid));
                return Request.CreateResponse(HttpStatusCode.OK, clInsurance);
            } catch(Exception ex) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message); }
        }
        [HttpPost] [Route("api/hmsMasters/insurance")]
        public async Task<HttpResponseMessage> BulkInsurance([FromBody] List<T_HR_Insurance> clInsurance) {
            // http://localhost/Login/api/hmsMasters/insurance
            //[{ID:1,EmpID:1271,PolicyNo:'a12345',IssueDate:'2019-01-01',MonthlyPremium:2000,CertificateNo:'no554433',ExpiryDate:'2020-01-01',Remarks:'ok',Ext:'.jpg',NameOfInsurer:'New India'}]
            if(clInsurance.Count > 0) {
                string result = await Task.Run(() => DAL.BulkInsertInsuranceAsync(clInsurance));
                return Request.CreateResponse(HttpStatusCode.OK, result);
            } else return Request.CreateResponse(HttpStatusCode.OK, "Nothing to Import!");
        }
        [HttpGet] [Route("api/hmsMasters/qualifications")]
        public async Task<HttpResponseMessage> qualifications(int cid) {// http://localhost/Login/api/hmsMasters/qualifications?cid=1
            try {
                List<T_HR_EmpEducationDetails> clQualifs = await Task.Run(() => DAL.ReadQualiicationsAsync(cid));
                return Request.CreateResponse(HttpStatusCode.OK, clQualifs);
            } catch(Exception ex) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message); }
        }
        [HttpPost] [Route("api/hmsMasters/qualifications")]
        public async Task<HttpResponseMessage> bulkQualifications([FromBody] List<T_HR_EmpEducationDetails> clQualifs) {
            // http://localhost/Login/api/hmsMasters/qualifications
            //[{  EduID:1, EmpID:1281,  Qualification: 'MTECH', Institute: 'Institution of Engineers',  YOP: 2010, Specialization: 'Civil Engg', Percentage: 60,  Mode:'Normal' }]
            if(clQualifs.Count > 0) {
                string res = await Task.Run(() => DAL.BulkInsertQualificationsAsync(clQualifs));
                return Request.CreateResponse(HttpStatusCode.OK, res);
            } else return Request.CreateResponse(HttpStatusCode.BadRequest, "Nothing to INSERT!");
        }
        [HttpGet] [Route("api/hmsMasters/experience")]        public async Task<HttpResponseMessage> getEmpExperience(int cid) {
            try {   //http://localhost/Login/api/hmsMasters/experience?cid=1
                List<T_HR_EmpExperience> clEmpExperience = await Task.Run(() => DAL.ReadExperiencesAsync(cid));                return Request.CreateResponse(HttpStatusCode.OK, clEmpExperience);
            } catch(Exception ex) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message); }        }
        [HttpPost] [Route("api/hmsMasters/experience")]        public async Task<HttpResponseMessage> postBulkExperience([FromBody] List<T_HR_EmpExperience> clExperience, int cid) {
            // http://localhost/Login/api/hmsMasters/experience?cid=1
            //[{  ExpID: 0, EmpID: 1282, Organization: 'CPWD', City: 2, Type: 'PERMANENT', FromDate: '2010-02-11', ToDate: '2019-02-11', Designation: 'Jr Engr', CTC: 250000 }]
            if(clExperience.Count > 0) {                var watch = System.Diagnostics.Stopwatch.StartNew();                string res = await Task.Run(() => DAL.BulkInsertExperiencesAsync(clExperience));                watch.Stop();                var elapsedTime = watch.ElapsedMilliseconds;                return Request.CreateResponse(HttpStatusCode.OK, "ElapsedTime: " + elapsedTime.ToString() + Environment.NewLine + res);            } else return Request.CreateResponse(HttpStatusCode.BadRequest, "Nothing to INSERT!");        }    }
    public class t_g_countryMaster {
        public int CountryID { get; set; }
        public string CountryName { get; set; }
        public int Bitsstatus { get; set; }
        public string Nationality { get; set; }
        public string ArabiccountryName { get; set; }
        public t_g_countryMaster() { }        public t_g_countryMaster(int _CountryID, string _CountryName, int _Bitsstatus, string _Nationality, string _ArabiccountryName) {            CountryID = _CountryID; CountryName = _CountryName; Bitsstatus = _Bitsstatus; Nationality = _Nationality; ArabiccountryName = _ArabiccountryName;
        }    }
    public class t_g_currencyMaster {
        public int CurrencyID { get; set; }
        public string CurrencyName { get; set; }
        public string Currency { get; set; }
        public string Symbol { get; set; }
        public int AlowDecimalPlaces { get; set; }
        public bool IsActive { get; set; }
        public int CompId { get; set; }
        public int Cid { get; set; }
        public t_g_currencyMaster() { }        public t_g_currencyMaster(int _CurrencyID, string _CurrencyName, string _Currency, string _Symbol, int _AlowDecimalPlaces, bool _IsActive) {
            CurrencyID = _CurrencyID; CurrencyName = _CurrencyName; Currency = _Currency; Symbol = _Symbol; AlowDecimalPlaces = _AlowDecimalPlaces; IsActive = _IsActive;
        }    }
    public class t_g_countryInfo {
        public int CountryID { get; set; }
        public string CallingCode { get; set; }
        public int CurrencyId { get; set; }
        public string Code2 { get; set; }
        public string Code3 { get; set; }
        //public byte[]   Flag            { get; set; } 
        public t_g_countryInfo() { }        public t_g_countryInfo(int _CountryID, string _CallingCode, int _CurrencyId, string _Code2, string _Code3) {            CountryID = _CountryID; CallingCode = _CallingCode; CurrencyId = _CurrencyId; Code2 = _Code2; Code3 = _Code3;
        }    }
    public class t_g_stateMaster {
        public int StateID { get; set; }
        public int CountryID { get; set; }
        public string StateName { get; set; }
        public int Bitstatus { get; set; }
        // public int EntryTaxLedgerID { get; set; } 
        // public string ArabicStateName { get; set; } 
        public t_g_stateMaster() { }        public t_g_stateMaster(int _StateID, int _CountryID, string _StateName, int _Bitstatus) { // int _EntryTaxLedgerID, string _ArabicStateName
            StateID = _StateID; CountryID = _CountryID; StateName = _StateName; Bitstatus = _Bitstatus; //EntryTaxLedgerID = _EntryTaxLedgerID; ArabicStateName = _ArabicStateName; 
        }    }
    public class t_g_cityMaster {
        public int CityID { get; set; }
        public string CityName { get; set; }
        public int status { get; set; }
        public int StateId { get; set; }
        //public string ArabicCityName { get; set; } 
        public t_g_cityMaster() { }        public t_g_cityMaster(int _CityID, string _CItyName, int _status, int _StateId) {
            CityID = _CityID; CityName = _CItyName; status = _status; StateId = _StateId;
        }    }
    public class ws {
        public string WorkSite { get; set; }
        public string Address { get; set; }
        public int STATEID { get; set; }
        public int cid { get; set; }
        public int CityID { get; set; }
    }
    public class worksite {
        public int Site_ID { get; set; }
        public string Site_Name { get; set; }
        public string Site_Address { get; set; }
        public string WSStatus { get; set; }
        public int PreOrder { get; set; }
        public int STATEID { get; set; }
        public int LedgerID { get; set; }
        public int PMLedgerID { get; set; }
        public int CityID { get; set; }
        public int CompanyID { get; set; }
        public worksite(int site_ID, string site_Name, string site_Address, string wSStatus, int preOrder, int sTATEID, int ledgerID
            , int pMLedgerID, int cityID, int companyID) {
            Site_ID = site_ID;
            Site_Name = site_Name;
            Site_Address = site_Address;
            WSStatus = wSStatus;
            PreOrder = preOrder;
            STATEID = sTATEID;
            LedgerID = ledgerID;
            PMLedgerID = pMLedgerID;
            CityID = cityID;
            CompanyID = companyID;
        }
    }
    public class EmployeeType {
        public int EmpTyID { get; set; }
        public string EMpType { get; set; }
        public int cid { get; set; }
        public EmployeeType() { }
        public EmployeeType(int _EmpTyID, string _EMpType, int _cid) {
            EmpTyID = _EmpTyID; EMpType = _EMpType; cid = _cid;
        }
    }
    public class T_G_CategoryMaster {
        public short CateId { get; set; }
        public string Category { get; set; }
        public bool Status { get; set; }
        public int cid { get; set; }
        public T_G_CategoryMaster() { }
        public T_G_CategoryMaster(Int16 _CateId, string _Category, bool _Status, int _cid) {
            CateId = _CateId;
            Category = _Category;
            Status = _Status;
            cid = _cid;
        }
    }
    public class T_G_DesignationMaster {
        public short DesigId { get; set; }
        public string Designation { get; set; }
        public bool Status { get; set; }
        public int cid { get; set; }
        //public string   IndSupervisor   { get; set; } 
        public T_G_DesignationMaster() { }        public T_G_DesignationMaster(short _DesigId, string _Designation, bool _Status, int _cid) { //string _IndSupervisor,
            DesigId = _DesigId; Designation = _Designation; Status = _Status; cid = _cid; // IndSupervisor = _IndSupervisor;
        }    }
    public class T_G_DepartmentMaster {
        public decimal DepartmentUId { get; set; }
        public string DepartmentName { get; set; }
        public string BitsStatus { get; set; }
        public int cid { get; set; }
        // public string   DepartmentHOD   { get; set; } 
        // public short    PreOrder        { get; set; } 

        public T_G_DepartmentMaster() { }        public T_G_DepartmentMaster(decimal _DepartmentUId, string _DepartmentName, string _BitsStatus, int _cid) { // string _DepartmentHOD,short _PreOrder,
            DepartmentUId = _DepartmentUId; DepartmentName = _DepartmentName; // DepartmentHOD = _DepartmentHOD; PreOrder = _PreOrder;
            BitsStatus = _BitsStatus; cid = _cid;
        }    }
    public class T_HR_NatureOfEmployment {
        public int NatureOfEmp { get; set; }
        public string Nature { get; set; }
        public string NoofWD { get; set; }
        public bool IsActive { get; set; }
        public int WD1 { get; set; }
        public int cid { get; set; }

        public T_HR_NatureOfEmployment() { }        public T_HR_NatureOfEmployment(int _NatureOfEmp, string _Nature, string _NoofWD, bool _IsActive, int _WD1, int _cid) {            NatureOfEmp = _NatureOfEmp; Nature = _Nature; NoofWD = _NoofWD; IsActive = _IsActive; WD1 = _WD1; cid = _cid;
        }    }
    public class T_G_Shifts {
        public int ShiftID { get; set; }
        public string Name { get; set; }
        public string InTime { get; set; }
        public string OutTime { get; set; }
        public bool IsActive { get; set; }
        public int cid { get; set; }

        public T_G_Shifts() { }        public T_G_Shifts(int _ShiftID, string _Name, string _InTime, string _OutTime, bool _IsActive, int _cid) {            ShiftID = _ShiftID; Name = _Name; InTime = _InTime; OutTime = _OutTime; IsActive = _IsActive; cid = _cid;
        }    }
    public class T_G_BankMaster { // 
        public int BankId { get; set; }
        public string BankName { get; set; }
        public bool IsActive { get; set; }
        //public int Cid { get; set; }
        //public int CreatedBy { get; set; } 
        //public DateTime CreatedOn { get; set; } 
        //public int UpdatedBy { get; set; } 
        //public DateTime UpdatedOn { get; set; } 
        public T_G_BankMaster() { }        public T_G_BankMaster(int _BankId, string _BankName, bool _IsActive) { // int _CreatedBy, DateTime _CreatedOn, int _UpdatedBy, DateTime _UpdatedOn,
            BankId = _BankId; BankName = _BankName; IsActive = _IsActive; // CreatedBy = _CreatedBy; CreatedOn = _CreatedOn; UpdatedBy = _UpdatedBy; UpdatedOn = _UpdatedOn;
        }    }
    public class T_G_BankBranches { //
        public int BranchId { get; set; }
        public int BankID { get; set; }
        public string BranchName { get; set; }
        public string SWIFT { get; set; }
        public int CityID { get; set; }
        public int StateID { get; set; }
        public int CountryID { get; set; }
        public bool IsActive { get; set; }
        //public string RTGS { get; set; } 
        //public string PHONE1 { get; set; } 
        //public string PHONE2 { get; set; } 
        //public string FAX { get; set; } 
        //public string Address { get; set; } 
        // public bool IsAudited { get; set; } 
        public T_G_BankBranches() { }        public T_G_BankBranches(int _BranchId, int _BankID, string _BranchName, string _SWIFT, int _CityID, int _StateID            , int _CountryID, bool _IsActive) { // , string _RTGS, string _PHONE1, string _PHONE2, string _FAX, string _Address, bool _IsAudited 
            BranchId = _BranchId; BankID = _BankID; BranchName = _BranchName; SWIFT = _SWIFT;            CityID = _CityID; StateID = _StateID; CountryID = _CountryID; IsActive = _IsActive; // RTGS = _RTGS; PHONE1 = _PHONE1; PHONE2 = _PHONE2; FAX = _FAX; Address = _Address; IsAudited = _IsAudited;
        }    }
    public class BankBranches {        public int BranchId { get; set; }
        public string BranchName { get; set; }
        public int BankID { get; set; }
        public string BankName { get; set; }
        public string SWIFT { get; set; }
        public BankBranches() { }        public BankBranches(int _BranchId, string _BranchName, int _BankID, string _BankName, string _SWIFT) {
            BranchId = _BranchId; BranchName = _BranchName; BankID = _BankID; BankName = _BankName; SWIFT = _SWIFT;        }    }
    public class CityStateCountry {        public int CityID { get; set; }
        public string CityName { get; set; }
        public int CountryID { get; set; }
        public string CountryName { get; set; }
        public int StateID { get; set; }
        public string StateName { get; set; }
        public CityStateCountry() { }        public CityStateCountry(int _CityID, string _CityName, int _CountryID, string _CountryName, int _StateID, string _StateName) {
            CityID = _CityID; CityName = _CityName; CountryID = _CountryID; CountryName = _CountryName; StateID = _StateID; StateName = _StateName;        }    }
    public class StateCountry {        public int CountryID { get; set; }
        public string CountryName { get; set; }
        public int StateID { get; set; }
        public string StateName { get; set; }
        public StateCountry() { }        public StateCountry(int _CountryID, string _CountryName, int _StateID, string _StateName) {
            CountryID = _CountryID; CountryName = _CountryName; StateID = _StateID; StateName = _StateName;        }    }
    public class T_HR_Insurance { //
        public int ID { get; set; }
        public int EmpID { get; set; }
        public string PolicyNo { get; set; }
        public DateTime IssueDate { get; set; }
        public decimal MonthlyPremium { get; set; }
        public string CertificateNo { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string Remarks { get; set; }
        public string Ext { get; set; }
        public string NameOfInsurer { get; set; }
        public T_HR_Insurance() { }        public T_HR_Insurance(int _ID, int _EmpID, string _PolicyNo, DateTime _IssueDate, decimal _MonthlyPremium, string _CertificateNo            , DateTime _ExpiryDate, string _Remarks, string _Ext, string _NameOfInsurer) {            ID = _ID; EmpID = _EmpID; PolicyNo = _PolicyNo; IssueDate = _IssueDate; MonthlyPremium = _MonthlyPremium; CertificateNo = _CertificateNo;
            ExpiryDate = _ExpiryDate; Remarks = _Remarks; Ext = _Ext; NameOfInsurer = _NameOfInsurer;
        }        public DataTable dtInsurance(string tblName) {            DataTable dtName = new DataTable(tblName);            DataColumn ID = new DataColumn();
            ID.DataType = System.Type.GetType("System.Int32");            ID.ColumnName = "ID"; ID.AutoIncrement = true; dtName.Columns.Add(ID);            dtName.Columns.Add("EmpID", typeof(System.Int32));            dtName.Columns.Add("PolicyNo", typeof(System.String));            dtName.Columns.Add("IssueDate", typeof(System.DateTime));            dtName.Columns.Add("MonthlyPremium", typeof(System.Decimal));            dtName.Columns.Add("CertificateNo", typeof(System.String));            dtName.Columns.Add("ExpiryDate", typeof(System.DateTime));            dtName.Columns.Add("Remarks", typeof(System.String));            dtName.Columns.Add("Ext", typeof(System.String));            dtName.Columns.Add("NameOfInsurer", typeof(System.String));
            DataColumn[] keys = new DataColumn[1];            keys[0] = ID;            dtName.PrimaryKey = keys;            return dtName;        }
    }
    public class T_HR_EmpEducationDetails {
        public int EduID { get; set; }
        public int EmpID { get; set; }
        public string Qualification { get; set; }
        public string Institute { get; set; }
        public int YOP { get; set; }
        public string Specialization { get; set; }
        public decimal Percentage { get; set; }
        public string Mode { get; set; }
        public T_HR_EmpEducationDetails() { }        public T_HR_EmpEducationDetails(int _EduID, int _EmpID, string _Qualification, string _Institute, int _YOP, string _Specialization            , decimal _Percentage, string _Mode) {
            EduID = _EduID; EmpID = _EmpID; Qualification = _Qualification; Institute = _Institute; YOP = _YOP; Specialization = _Specialization;
            Percentage = _Percentage; Mode = _Mode;
        }        public DataTable dtClass(string tblName) {            DataTable dtName = new DataTable(tblName);            DataColumn EduID = new DataColumn();
            EduID.DataType = System.Type.GetType("System.Int32");            EduID.ColumnName = "EduID"; dtName.Columns.Add(EduID);            dtName.Columns.Add("EmpID", typeof(System.Int32));            dtName.Columns.Add("Qualification", typeof(System.String));            dtName.Columns.Add("Institute", typeof(System.String));            dtName.Columns.Add("YOP", typeof(System.Int32));            dtName.Columns.Add("Specialization", typeof(System.String));            dtName.Columns.Add("Percentage", typeof(System.Decimal));            dtName.Columns.Add("Mode", typeof(System.String));
            DataColumn[] keys = new DataColumn[1];            keys[0] = EduID;            dtName.PrimaryKey = keys;            return dtName;        }    }
    public class T_HR_EmpExperience {
        public int ExpID { get; set; }
        public int EmpID { get; set; }
        public string Organization { get; set; }
        public int City { get; set; }
        public string Type { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Designation { get; set; }
        public decimal CTC { get; set; }
        public T_HR_EmpExperience() { }        public T_HR_EmpExperience(int _ExpID, int _EmpID, string _Organization, int _City, string _Type, DateTime _FromDate, DateTime _ToDate            , string _Designation, decimal _CTC) {            ExpID = _ExpID; EmpID = _EmpID; Organization = _Organization; City = _City; Type = _Type; FromDate = _FromDate; ToDate = _ToDate;
            Designation = _Designation; CTC = _CTC;
        }        public DataTable dtClass(string tblName) {            DataTable dtName = new DataTable(tblName);            DataColumn ExpID = new DataColumn();
            ExpID.DataType = System.Type.GetType("System.Int32");            ExpID.ColumnName = "ExpID";
            dtName.Columns.Add(ExpID);            dtName.Columns.Add("EmpID", typeof(System.Int32));            dtName.Columns.Add("Organization", typeof(System.String));            dtName.Columns.Add("City", typeof(System.Int32));            dtName.Columns.Add("Type", typeof(System.String));            dtName.Columns.Add("FromDate", typeof(System.DateTime));            dtName.Columns.Add("ToDate", typeof(System.DateTime));            dtName.Columns.Add("Designation", typeof(System.String));            dtName.Columns.Add("CTC", typeof(System.Decimal));
            DataColumn[] keys = new DataColumn[1];            keys[0] = ExpID;            dtName.PrimaryKey = keys;            return dtName;        }    }
    public class T_HR_AppDetails {
        public int cid { get; set; }
        public int AppDetId { get; set; }
        public int EmpId { get; set; }
        public string PerAddress { get; set; } //1000
        public string PerPhone { get; set; } //50
        public string PerPin { get; set; } //10
        public bool SameAddress { get; set; }
        public string ResAddress { get; set; } //1000
        public string ResPin { get; set; } //
        public string ResPhone { get; set; }
        public string Bloodgroup { get; set; }
        public int Salary { get; set; }
        public DateTime DOJ { get; set; }
        public int permanentCountry { get; set; }
        public int permanentState { get; set; }
        public int permanentCity { get; set; }
        public int ResidentCountry { get; set; }
        public int ResidentState { get; set; }
        public int ResidentCity { get; set; }
        public string PassportNo { get; set; } //150
        public string Issuer { get; set; } //150
        public string IssuePlace { get; set; } //150
        public DateTime IssueDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string PasportRemarks { get; set; } //250
        public string PassportExt { get; set; } //10
        public string EmergContact { get; set; } //20
        public string EmergContactName { get; set; } //50
        public string EmergRelation { get; set; } //50
        public string EmergEmail { get; set; } //100
        public string EmergResiPhone { get; set; } //
        public DateTime Dateofmarriage { get; set; }
        public int MaritalStat { get; set; }
        public string OREmergContact { get; set; } //20
        public string OREmergContactName { get; set; } //50
        public string OREmergEmail { get; set; } //100
        public string OREmergRelation { get; set; } //50
        public string OREmergResiPhone { get; set; } //20
        public DateTime EOC_date { get; set; }
        //public int    Contract_Yr         { get; set; } 
        //public int    Contract_Month      { get; set; } 
        //public int    Contract_days       { get; set; } 
        //public string PerCity             { get; set; } 
        //public string PerState            { get; set; } 
        //public string PerCountry          { get; set; } 
        //public string ResCity             { get; set; } 
        //public string ResState            { get; set; } 
        //public string ResCountry          { get; set; } 
        //public string PerDoorNo           { get; set; } 
        //public string PerBuilding         { get; set; } 
        //public string PerStreet           { get; set; } 
        //public string PerArea             { get; set; } 
        //public string ResDoorNo           { get; set; } 
        //public string ResBuilding         { get; set; } 
        //public string ResStreet           { get; set; } 
        //public string ResArea             { get; set; } 
        public T_HR_AppDetails() { }        public T_HR_AppDetails(int _cid, int _AppDetId, int _EmpId, string _PerAddress  
         , string _PerPhone, string _PerPin, bool _SameAddress, string _ResAddress  
        , string _ResPin, string _ResPhone, string _Bloodgroup, int _Salary, DateTime _DOJ, int _permanentCountry              , int _permanentState, int _permanentCity, int _ResidentCountry, int _ResidentState, int _ResidentCity  
            , string _PassportNo, string _Issuer, string _IssuePlace, DateTime _IssueDate, DateTime _ExpiryDate, string _PasportRemarks            , string _PassportExt, string _EmergContact, string _EmergContactName, string _EmergRelation, string _EmergEmail, string _EmergResiPhone            , DateTime _Dateofmarriage, int _MaritalStat, string _OREmergContact, string _OREmergContactName, string _OREmergEmail            , string _OREmergRelation, string _OREmergResiPhone //, int _Contract_Yr, int _Contract_Month, int _Contract_days
            , DateTime _EOC_date) {            cid = _cid; AppDetId = _AppDetId; EmpId = _EmpId; PerAddress = _PerAddress;
            PerPhone = _PerPhone; PerPin = _PerPin; SameAddress = _SameAddress; ResAddress = _ResAddress;
            ResPin = _ResPin; ResPhone = _ResPhone; Bloodgroup = _Bloodgroup; Salary = _Salary; DOJ = _DOJ;
            permanentCountry = _permanentCountry; permanentState = _permanentState;
            permanentCity = _permanentCity; ResidentCountry = _ResidentCountry; ResidentState = _ResidentState; ResidentCity = _ResidentCity;
            PassportNo = _PassportNo; Issuer = _Issuer;
            IssuePlace = _IssuePlace; IssueDate = _IssueDate; ExpiryDate = _ExpiryDate; PasportRemarks = _PasportRemarks;
            PassportExt = _PassportExt; EmergContact = _EmergContact; EmergContactName = _EmergContactName; EmergRelation = _EmergRelation;
            EmergEmail = _EmergEmail; EmergResiPhone = _EmergResiPhone; Dateofmarriage = _Dateofmarriage; MaritalStat = _MaritalStat;
            OREmergContact = _OREmergContact; OREmergContactName = _OREmergContactName; OREmergEmail = _OREmergEmail;
            OREmergRelation = _OREmergRelation; OREmergResiPhone = _OREmergResiPhone;
            EOC_date = _EOC_date;
        }        public DataTable dtAppDetails(string tblName) {            DataTable dtName = new DataTable(tblName);            DataColumn AppDetId = new DataColumn();
            AppDetId.DataType = System.Type.GetType("System.Int32");            AppDetId.ColumnName = "AppDetId"; dtName.Columns.Add(AppDetId);            dtName.Columns.Add("EmpId", typeof(System.Int32));            dtName.Columns.Add("PerAddress", typeof(System.String));            dtName.Columns.Add("PerCity", typeof(System.String));//REMOVED
            dtName.Columns.Add("PerState", typeof(System.String));//REMOVED
            dtName.Columns.Add("PerCountry", typeof(System.String));//REMOVED
            dtName.Columns.Add("PerPhone", typeof(System.String));            dtName.Columns.Add("PerPin", typeof(System.String));            dtName.Columns.Add("SameAddress", typeof(System.Boolean));            dtName.Columns.Add("ResAddress", typeof(System.String));            dtName.Columns.Add("ResCity", typeof(System.String));   //REMOVED
            dtName.Columns.Add("ResState", typeof(System.String));  //REMOVED
            dtName.Columns.Add("ResCountry", typeof(System.String));//REMOVED
            dtName.Columns.Add("ResPin", typeof(System.String));            dtName.Columns.Add("ResPhone", typeof(System.String));            dtName.Columns.Add("Bloodgroup", typeof(System.String));            dtName.Columns.Add("Salary", typeof(System.Int32));            dtName.Columns.Add("DOJ", typeof(System.DateTime));            dtName.Columns.Add("permanentCountry", typeof(System.Int32));//REMOVED
            dtName.Columns.Add("permanentState", typeof(System.Int32));  //REMOVED
            dtName.Columns.Add("permanentCity", typeof(System.Int32));   //REMOVED
            dtName.Columns.Add("ResidentCountry", typeof(System.Int32)); //REMOVED
            dtName.Columns.Add("ResidentState", typeof(System.Int32));   //REMOVED
            dtName.Columns.Add("ResidentCity", typeof(System.Int32));    //REMOVED

            dtName.Columns.Add("PerDoorNo", typeof(System.String));     //REMOVED
            dtName.Columns.Add("PerBuilding", typeof(System.String));   //REMOVED
            dtName.Columns.Add("PerStreet", typeof(System.String));     //REMOVED
            dtName.Columns.Add("PerArea", typeof(System.String));       //REMOVED
            dtName.Columns.Add("ResDoorNo", typeof(System.String));     //REMOVED
            dtName.Columns.Add("ResBuilding", typeof(System.String));   //REMOVED
            dtName.Columns.Add("ResStreet", typeof(System.String));     //REMOVED
            dtName.Columns.Add("ResArea", typeof(System.String));       //REMOVED

            dtName.Columns.Add("PassportNo", typeof(System.String));            dtName.Columns.Add("Issuer", typeof(System.String));            dtName.Columns.Add("IssuePlace", typeof(System.String));            dtName.Columns.Add("IssueDate", typeof(System.DateTime));            dtName.Columns.Add("ExpiryDate", typeof(System.DateTime));            dtName.Columns.Add("PasportRemarks", typeof(System.String));            dtName.Columns.Add("PassportExt", typeof(System.String));            dtName.Columns.Add("EmergContact", typeof(System.String));            dtName.Columns.Add("EmergContactName", typeof(System.String));            dtName.Columns.Add("EmergRelation", typeof(System.String));            dtName.Columns.Add("EmergEmail", typeof(System.String));            dtName.Columns.Add("EmergResiPhone", typeof(System.String));            dtName.Columns.Add("Dateofmarriage", typeof(System.DateTime));            dtName.Columns.Add("MaritalStat", typeof(System.Int32));            dtName.Columns.Add("OREmergContact", typeof(System.String));            dtName.Columns.Add("OREmergContactName", typeof(System.String));            dtName.Columns.Add("OREmergEmail", typeof(System.String));            dtName.Columns.Add("OREmergRelation", typeof(System.String));            dtName.Columns.Add("OREmergResiPhone", typeof(System.String));            dtName.Columns.Add("Contract_Yr", typeof(System.Int32));            dtName.Columns.Add("Contract_Month", typeof(System.Int32));            dtName.Columns.Add("Contract_days", typeof(System.Int32));            dtName.Columns.Add("EOC_date", typeof(System.DateTime));

            DataColumn[] keys = new DataColumn[1];            keys[0] = AppDetId;            dtName.PrimaryKey = keys;            return dtName;        }
    }
    public class T_HR_EmpSal {
        public int EmpSalID { get; set; }
        public int EmpId { get; set; }
        public decimal Salary { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int CreatedBY { get; set; }
        public int UpdatedBY { get; set; }
        public DateTime CreatedON { get; set; }
        public DateTime UpdatedON { get; set; }

        public T_HR_EmpSal() { }        public T_HR_EmpSal(int _EmpSalID, int _EmpId, decimal _Salary, DateTime _FromDate, DateTime _ToDate            , int _CreatedBY, int _UpdatedBY, DateTime _CreatedON, DateTime _UpdatedON) {            EmpSalID = _EmpSalID; EmpId = _EmpId; Salary = _Salary; FromDate = _FromDate; ToDate = _ToDate;
            CreatedBY = _CreatedBY; UpdatedBY = _UpdatedBY; CreatedON = _CreatedON; UpdatedON = _UpdatedON;
        }        public DataTable dtEmpSal(string tblName) {            DataTable dtName = new DataTable(tblName);            DataColumn EmpSalID = new DataColumn();
            EmpSalID.DataType = System.Type.GetType("System.Int32");            EmpSalID.ColumnName = "EmpSalID";            dtName.Columns.Add(EmpSalID);            dtName.Columns.Add("EmpId", typeof(System.Int32));            dtName.Columns.Add("Salary", typeof(System.Decimal));            dtName.Columns.Add("FromDate", typeof(System.DateTime));            dtName.Columns.Add("ToDate", typeof(System.DateTime));            dtName.Columns.Add("CreatedBY", typeof(System.Int32));            dtName.Columns.Add("UpdatedBY", typeof(System.Int32));            dtName.Columns.Add("CreatedON", typeof(System.DateTime));            dtName.Columns.Add("UpdatedON", typeof(System.DateTime));
            DataColumn[] keys = new DataColumn[1];            keys[0] = EmpSalID;            dtName.PrimaryKey = keys;            return dtName;        }
    }
    public class T_HR_EmpWages {
        public int EmpWageId { get; set; }
        public int WagesID { get; set; }
        public int EmpId { get; set; }
        public bool IsActive { get; set; }

        public T_HR_EmpWages() { }        public T_HR_EmpWages(int _EmpWageId, int _WagesID, int _EmpId, bool _IsActive) {            EmpWageId = _EmpWageId; WagesID = _WagesID; EmpId = _EmpId; IsActive = _IsActive;
        }        public DataTable dtEmpWages(string tblName) {            DataTable dtName = new DataTable(tblName);            DataColumn EmpWageId = new DataColumn();
            EmpWageId.DataType = System.Type.GetType("System.Int32");            EmpWageId.ColumnName = "EmpWageId";            dtName.Columns.Add(EmpWageId);            dtName.Columns.Add("WagesID", typeof(System.Int32));            dtName.Columns.Add("EmpId", typeof(System.Int32));            dtName.Columns.Add("IsActive", typeof(System.Boolean));
            DataColumn[] keys = new DataColumn[1];            keys[0] = EmpWageId;            dtName.PrimaryKey = keys;            return dtName;        }    }
    public class T_HR_EmpWagesCentage {
        public int CentageID { get; set; }
        public int WagesID { get; set; }
        public int EmpID { get; set; }
        public decimal Centage { get; set; }
        public int IsActive { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int EmpSalID { get; set; }

        public T_HR_EmpWagesCentage() { }        public T_HR_EmpWagesCentage(int _CentageID, int _WagesID, int _EmpID, decimal _Centage, int _IsActive                , int _CreatedBy, DateTime _CreatedOn, int _UpdatedBy, DateTime _UpdatedOn, int _EmpSalID) {            CentageID = _CentageID; WagesID = _WagesID; EmpID = _EmpID; Centage = _Centage; IsActive = _IsActive;
            CreatedBy = _CreatedBy; CreatedOn = _CreatedOn; UpdatedBy = _UpdatedBy; UpdatedOn = _UpdatedOn; EmpSalID = _EmpSalID;
        }        public DataTable dtEmpWagesCentage(string tblName) {            DataTable dtName = new DataTable(tblName);            DataColumn CentageID = new DataColumn();
            CentageID.DataType = System.Type.GetType("System.Int32");            CentageID.ColumnName = "CentageID";            dtName.Columns.Add(CentageID);            dtName.Columns.Add("WagesID", typeof(System.Int32));            dtName.Columns.Add("EmpID", typeof(System.Int32));            dtName.Columns.Add("Centage", typeof(System.Decimal));            dtName.Columns.Add("IsActive", typeof(System.Int32));            dtName.Columns.Add("CreatedBy", typeof(System.Int32));            dtName.Columns.Add("CreatedOn", typeof(System.DateTime));            dtName.Columns.Add("UpdatedBy", typeof(System.Int32));            dtName.Columns.Add("UpdatedOn", typeof(System.DateTime));            dtName.Columns.Add("EmpSalID", typeof(System.Int32));
            DataColumn[] keys = new DataColumn[1];            keys[0] = CentageID;            dtName.PrimaryKey = keys;            return dtName;        }    }
}
