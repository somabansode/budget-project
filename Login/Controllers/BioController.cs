﻿using SD.COMMON.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;
// STATUS CODES FOR MOBILE APPs
// not found   "EmpID NOT found in the company employee list OR the EmpID might be deactivated	    " 404
// created	   "EmpID:44 Passang Tempa Sharpa >> new registration successful!	                    " 201
// found	   "Already registered from another device 862122044891249 and the record is active!	" 302
// accepted	   "EmpID:7 Vijay  Mariyala >> registration is updated on new device!	                " 202
// created	   "EmpID:7 Vijay  Mariyala >> has been deregistered!                               	" 201

namespace Login.Controllers { //FOR MOBILE APPs
    public class BioController :ApiController { //FOR MOBILE APPs
        class DbDomain {
            public string db { get; set; }
            public string domain { get; set; }
        }
        class IDEmp {
            public int ID { get; set; }
            public string Name { get; set; }
        }
        [HttpGet] [Route("api/bio/domains")]
        public HttpResponseMessage domains() {
            // http://localhost/Login/api/bio/domains
            try {
                List<DbDomain> dds = new List<DbDomain>();
                DataSet ds = SQLDBUtil.ExecuteDataset("s_DBsDomains");
                if(ds.Tables[0].Rows.Count > 0) {
                    foreach(DataRow dr in ds.Tables[0].Rows) {
                        DbDomain dd = new DbDomain();
                        dd.db = dr["db"].ToString();
                        dd.domain = dr["domain"].ToString();
                        dds.Add(dd);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, dds);
                } else return Request.CreateResponse(HttpStatusCode.BadRequest, new bio("failure", "Internal server Error", "500"));
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new bio("failure", ex.Message, "520"));
            }
        }
        [HttpGet] [Route("api/bio/fetchID")]
        public HttpResponseMessage fetchID(string EmployeeNameSearchString) {
            // http://localhost/Login/api/bio/fetchID?EmployeeNameSearchString=reddy%20kumar
            try {
                List<IDEmp> dds = new List<IDEmp>();
                SqlParameter[] sps = new SqlParameter[1];
                sps[0] = new SqlParameter("@EmployeeName", EmployeeNameSearchString);
                DataSet ds = SQLDBUtil.ExecuteDataset("sh_FilterEmployee", sps);
                if(ds.Tables[0].Rows.Count > 0) {
                    foreach(DataRow dr in ds.Tables[0].Rows) {
                        IDEmp dd = new IDEmp();
                        dd.ID = (int)dr["ID"];
                        dd.Name = dr["Name"].ToString();
                        dds.Add(dd);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, dds);
                } else return Request.CreateResponse(HttpStatusCode.BadRequest, new bio("failure", "Internal server Error", "500"));
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new bio("failure", ex.Message, "520"));
            }
        }
        [HttpGet] [Route("api/bio/check")]
        public HttpResponseMessage CheckFingerPrintValue(int empID, string fingerprintvalue, decimal lat, decimal lng) {
            // http://localhost/Login/api/bio/check?empID=6&fingerprintvalue=351578105508998&lat=3.1900001&lng=101.1900001&callback=getGPsPositions
            try {
                SqlParameter[] sps = new SqlParameter[4];
                sps[0] = new SqlParameter("@empID", empID);
                sps[1] = new SqlParameter("@fingerprintvalue", fingerprintvalue);
                sps[2] = new SqlParameter("@lat", lat);
                sps[3] = new SqlParameter("@lng", lng);
                DataSet ds = SQLDBUtil.ExecuteDataset("checkFingerPrintValue", sps);
                if(ds.Tables[0].Rows[0][0].ToString() != string.Empty) {
                    string res = ds.Tables[0].Rows[0][0].ToString();
                    if(res.Contains("Attendance PUNCHED")) return Request.CreateResponse(HttpStatusCode.OK, new bio("success", res, "100"));
                    else return Request.CreateResponse(HttpStatusCode.BadRequest, new bio("failure", res, "150")); 
                } else return Request.CreateResponse(HttpStatusCode.BadRequest, new bio("failure", "NULL", "150"));
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new bio("failure", ex.Message, "400"));
            }
        }
        [HttpGet] [Route("api/bio/register")]
        public HttpResponseMessage registerFingerPrintValue(string fingerprintvalue, int empID) {
            //http://localhost/Login/api/bio/register?fingerprintvalue=862122044891249&empID=2
            try {
                SqlParameter[] sps = new SqlParameter[2];
                sps[0] = new SqlParameter("@fingerprintvalue", fingerprintvalue);
                sps[1] = new SqlParameter("@empID", empID);
                DataSet ds = SQLDBUtil.ExecuteDataset("registerFingerPrintValue", sps);
                if(ds.Tables[0].Rows.Count > 0)
                    return Request.CreateResponse(HttpStatusCode.OK, new bio(ds.Tables[0].Rows[0][0].ToString()
                        , ds.Tables[0].Rows[0][1].ToString(), ds.Tables[0].Rows[0][2].ToString()));
                else return Request.CreateResponse(HttpStatusCode.BadRequest, new bio("failure", "Internal server Error", "500"));
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new bio("failure", ex.Message, "520"));
            }
        }
        [HttpGet] [Route("api/bio/registrationApproval")]
        public HttpResponseMessage registrationApproval(string fingerprintvalue, int empID) {
            //http://localhost/Login/api/bio/registrationApproval?fingerprintvalue=86212204489124&empID=1251
            try {
                SqlParameter[] sps = new SqlParameter[2];
                sps[0] = new SqlParameter("@fingerprintvalue", fingerprintvalue);
                sps[1] = new SqlParameter("@empID", empID);
                DataSet ds = SQLDBUtil.ExecuteDataset("registrationApproval", sps);
                if(ds.Tables[0].Rows.Count > 0)
                    return Request.CreateResponse(HttpStatusCode.OK, new bio(ds.Tables[0].Rows[0][0].ToString()
                        , ds.Tables[0].Rows[0][1].ToString(), ds.Tables[0].Rows[0][2].ToString()));
                else return Request.CreateResponse(HttpStatusCode.BadRequest, new bio("failure", "Internal server Error", "500"));
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new bio("failure", ex.Message, "520"));
            }
        }
        [HttpGet] [Route("api/bio/deregister")]
        public HttpResponseMessage deregisterFingerPrintValue(int empID) {
            //http://localhost/Login/api/bio/deregister?empID=6
            try {
                SqlParameter[] sps = new SqlParameter[1];
                sps[0] = new SqlParameter("@empID", empID);
                DataSet ds = SQLDBUtil.ExecuteDataset("deregisterFingerPrintValue", sps);
                if(ds.Tables[0].Rows.Count > 0)
                    return Request.CreateResponse(HttpStatusCode.OK,
                        new bio(ds.Tables[0].Rows[0][0].ToString(), ds.Tables[0].Rows[0][1].ToString()
                        , ds.Tables[0].Rows[0][2].ToString()));
                else return Request.CreateResponse(HttpStatusCode.BadRequest, new bio("failure", "Internal server Error", "500"));
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new bio("failure", ex.Message, "520"));
            }
        }
        [HttpGet]
        [Route("api/bio/geoloc")] //DROPPED
        public HttpResponseMessage AllEmployeeGeoPositions(int cid, DateTime dt) {
            //http://localhost/Login/api/bio/geoloc?callback=getGPsPositions&cid=1&dt=2019-01-27
            try {
                List<empGeoPositions> clEmpPositions = new List<empGeoPositions>();
                SqlParameter[] sps = new SqlParameter[2];
                sps[0] = new SqlParameter("@cid", cid);
                sps[1] = new SqlParameter("@dt", dt);
                DataSet ds = SQLDBUtil.ExecuteDataset("sh_AllEmployeeGeoPositions", sps);
                if(ds.Tables[0].Rows.Count > 0) {
                    foreach(DataRow dr in ds.Tables[0].Rows) {
                        empGeoPositions gl = new empGeoPositions();
                        gl.cid = cid; gl.empID = (int)dr["empID"]; gl.empName = dr["empName"].ToString();
                        gl.wsID = (int)dr["wsID"]; gl.Site = dr["site"].ToString();
                        gl.lat = Convert.ToDecimal(dr["lat"]); gl.lng = Convert.ToDecimal(dr["lng"]);
                        clEmpPositions.Add(gl);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, clEmpPositions);
                } else return Request.CreateResponse(HttpStatusCode.BadRequest, "No Employees Found");
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message + "; No Employees Found");
            }
        }
        [HttpGet]
        [Route("api/bio/admin")]
        public HttpResponseMessage bioRegisteringUserAuthen(string userName, string password) {
            try {// http://localhost/Login/api/bio/admin?userName=yudhi@aeclogic.com&password=admin111
                string pwBytes = GetMD5Hash(password);
                SqlParameter[] sps = new SqlParameter[2];
                sps[0] = new SqlParameter("@userName", userName);
                sps[1] = new SqlParameter("@password", pwBytes);
                DataSet ds = SQLDBUtil.ExecuteDataset("sh_bioRegisteringUserAuthen", sps);
                if((int)ds.Tables[0].Rows[0][0] == 1)
                    return Request.CreateResponse(HttpStatusCode.OK, "SUCCESS");
                else return Request.CreateResponse(HttpStatusCode.BadRequest, "FAILED");
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message + " FAILED");
            }
        }
        public string GetMD5Hash(string pw) {
            System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] bs = System.Text.Encoding.UTF8.GetBytes(pw);
            bs = x.ComputeHash(bs);
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            foreach(byte b in bs) {
                s.Append(b.ToString("x2").ToLower());
            }
            return s.ToString();
        }
        [HttpGet]
        [Route("api/bio/sites")]//DROPPED
        public HttpResponseMessage worksiteLatLng(int cid) {
            try {// http://localhost/Login/api/bio/sites?cid=1
                List<latlng> clSitesLatLng = new List<latlng>();
                SqlParameter[] sps = new SqlParameter[1];
                sps[0] = new SqlParameter("@cid", cid);
                DataSet ds = SQLDBUtil.ExecuteDataset("sh_worksiteLatLng", sps);
                if(ds.Tables[0].Rows.Count > 0) {
                    foreach(DataRow dr in ds.Tables[0].Rows) {
                        latlng gl = new latlng();
                        gl.cid = cid; gl.id = (int)dr["nos"]; gl.Name = dr["wsName"].ToString();
                        gl.lat = (float)Convert.ToDecimal(dr["lat"]); gl.lng = (float)Convert.ToDecimal(dr["lng"]);
                        clSitesLatLng.Add(gl);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, clSitesLatLng);
                } else return Request.CreateResponse(HttpStatusCode.BadRequest, "FAILED");
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message + " FAILED");
            }
        }
        [HttpPost]
        [Route("api/bio/addFence")]//DROPPED
        public HttpResponseMessage addFence(int cid, int ws, [FromBody] List<fenceCoords> clFenceCoords) {
            try {// http://localhost/Login/api/bio/addFence?cid=1&ws=1
                DataTable dt = new DataTable();
                dt.Columns.Add(new DataColumn("fID", typeof(System.String)));
                dt.Columns.Add(new DataColumn("v", typeof(System.Int32)));
                dt.Columns.Add(new DataColumn("x", typeof(System.Decimal)));
                dt.Columns.Add(new DataColumn("y", typeof(System.Decimal)));
                foreach(fenceCoords fc in clFenceCoords) {
                    DataRow dr = dt.NewRow();
                    dr[0] = fc.fid; dr[1] = fc.v; dr[2] = fc.x; dr[3] = fc.y;
                    dt.Rows.Add(dr);
                }
                SqlParameter[] sps = new SqlParameter[3];
                sps[0] = new SqlParameter("@cid", cid);
                sps[1] = new SqlParameter("@ws", ws);
                sps[2] = new SqlParameter("@fxy", dt);
                DataSet ds = SQLDBUtil.ExecuteDataset("sh_addFences", sps);
                if((int)ds.Tables[0].Rows[0][0] == 1)
                    return Request.CreateResponse(HttpStatusCode.OK, "SUCCESS");
                else return Request.CreateResponse(HttpStatusCode.BadRequest, "FAILED");
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message + " FAILED");
            }
        }
        [HttpPost] [Route("api/bio/assign")]
        public HttpResponseMessage assignGeoFenceVsEmpID(int empID , int gfID ) {
            // http://localhost/Login/api/bio/assign // http://adc.6dproptech.com/Login/api/bio/assign
            try {
                SqlParameter[] sps = new SqlParameter[2];
                sps[0] = new SqlParameter("@empID", empID);
                sps[1] = new SqlParameter("@gfID", gfID);
                DataSet ds = SQLDBUtil.ExecuteDataset("sh_assignGeoFence2EmpID");
                if(ds.Tables[0].Rows.Count > 0) {
                    return Request.CreateResponse(HttpStatusCode.OK, ds.Tables[0].Rows[0][0].ToString());
                } else return Request.CreateResponse(HttpStatusCode.BadRequest, new bio("failure", "Internal server Error", "500"));
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new bio("failure", ex.Message, "520"));
            }
        }
        [HttpPost] [Route("api/bio/attpass")]
        public HttpResponseMessage attendancePass(int empID , int gfID, int attID ) {
            // http://localhost/Login/api/bio/assign // http://adc.6dproptech.com/Login/api/bio/assign
            try {
                SqlParameter[] sps = new SqlParameter[3];
                sps[0] = new SqlParameter("@empID", empID);
                sps[1] = new SqlParameter("@gfID", gfID);
                sps[2] = new SqlParameter("@attID", attID);
                //
                sps[3] = new SqlParameter("@res", 1);
                DataSet ds = SQLDBUtil.ExecuteDataset("sh_empAtt2Geofence");
                if(ds.Tables[0].Rows.Count > 0) {
                    return Request.CreateResponse(HttpStatusCode.OK, ds.Tables[0].Rows[0][0].ToString());
                } else return Request.CreateResponse(HttpStatusCode.BadRequest, new bio("failure", "Internal server Error", "500"));
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new bio("failure", ex.Message, "520"));
            }
        }
        
    }

    public class fenceCoords {
        //public int cid { get; set; }
        //public int ws { get; set; }
        public string fid { get; set; }
        public int v { get; set; }
        public float x { get; set; }
        public float y { get; set; }
    }

    //public class cidDt {
    //    public int cid { get; set; }
    //    public DateTime dt { get; set; }
    //}
    public class bio {
        public string Description { get; set; }
        public string EmpName { get; set; }
        public string Status { get; set; }
        public bio(string des, string emp, string st) {
            Description = des; EmpName = emp; Status = st;
        }
    }
    public class latlng {
        public int cid { get; set; }
        public int id { get; set; }
        public string Name { get; set; }
        public float lat { get; set; }
        public float lng { get; set; }
    }
    public class empGeoPositions {
        public int cid { get; set; }
        public int wsID { get; set; }
        public string Site { get; set; }
        public int empID { get; set; }
        public string empName { get; set; }
        public decimal lat { get; set; }
        public decimal lng { get; set; }
    }
}
