﻿using Newtonsoft.Json.Converters;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
namespace Login.Controllers {
    //the following R&D took yudhi 8 hours from various websites
    //https://stackoverflow.com/questions/9421312/jsonp-with-asp-net-web-api
    //global.asax must also contain >> GlobalConfiguration.Configuration.Formatters.Insert(0, new JsonpFormatter());
    //the URL must contain one of the parameters as callback, for ex: ?callback=xxxx, apart from other parameters being passes fromURI
    //use ?callback=XXXXX as shpwn in the example to convert to jsonp format >>> http://localhost/Login/api/bio?callback=getGPsPositions
    /// <summary>
    /// Handles JsonP requests when requests are fired with text/javascript
    /// </summary>
    public class JsonpFormatter : JsonMediaTypeFormatter {
        public JsonpFormatter() {
            if (!SupportedMediaTypes.Contains(new MediaTypeHeaderValue("application/json"))) {
                SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/json"));
            }
            if (!SupportedMediaTypes.Contains(new MediaTypeHeaderValue("text/javascript"))) {
                SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/javascript"));
            }
            JsonpParameterName = "callback";
        }
        ///  Name of the query string parameter to look for the jsonp function name
        public string JsonpParameterName { get; set; }
        /// Captured name of the Jsonp function that the JSON call is wrapped in. Set in GetPerRequestFormatter Instance
        private string JsonpCallbackFunction;
        public override bool CanWriteType(Type type) {
            return true;
        }
        /// Override this method to capture the Request object
        public override MediaTypeFormatter GetPerRequestFormatterInstance(Type type, HttpRequestMessage request, MediaTypeHeaderValue mediaType) {
            var formatter = new JsonpFormatter() {
                JsonpCallbackFunction = GetJsonCallbackFunction(request)
            };
            // this doesn't work unfortunately
            //formatter.SerializerSettings = GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings;
            // You have to reapply any JSON.NET default serializer Customizations here    
            formatter.SerializerSettings.Converters.Add(new StringEnumConverter());
            formatter.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
            return formatter;
        }
        public override Task WriteToStreamAsync(Type type, object value, Stream stream, HttpContent content, TransportContext transportContext) {
            if (string.IsNullOrEmpty(JsonpCallbackFunction))  return base.WriteToStreamAsync(type, value, stream, content, transportContext);
            StreamWriter writer = null;
            // write the pre-amble
            try {
                writer = new StreamWriter(stream);
                writer.Write(JsonpCallbackFunction + "(");
                writer.Flush();
            } catch (Exception ex) {
                try { if (writer != null) writer.Dispose(); } catch { }
                var tcs = new TaskCompletionSource<object>();
                tcs.SetException(ex);
                return tcs.Task;
            }
            return base.WriteToStreamAsync(type, value, stream, content, transportContext)
                       .ContinueWith(innerTask => {
                           if (innerTask.Status == TaskStatus.RanToCompletion) {
                               writer.Write(")");
                               writer.Flush();
                           }
                           writer.Dispose();
                           return innerTask;
                       }, TaskContinuationOptions.ExecuteSynchronously).Unwrap();
        }
        /// Retrieves the Jsonp Callback function from the query string
        private string GetJsonCallbackFunction(HttpRequestMessage request) {
            if (request.Method != HttpMethod.Get) return null;
            var query = HttpUtility.ParseQueryString(request.RequestUri.Query);
            var queryVal = query[this.JsonpParameterName];
            if (string.IsNullOrEmpty(queryVal))  return null;
            return queryVal;
        }
    }
   
}