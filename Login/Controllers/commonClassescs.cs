﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;

namespace Login.Controllers {

    public static class common {
        public static string erpCS { get { return ConfigurationManager.ConnectionStrings["ERPConnectionString"].ConnectionString; } }
        public static void BulkInsertAny(DataTable _newValues, string _tableName) {
            string connectionString = erpCS;
            using(SqlConnection connection = new SqlConnection(connectionString)) {
                connection.Open();
                using(SqlBulkCopy bulkCopy = new SqlBulkCopy(connection)) {
                    bulkCopy.DestinationTableName = "dbo." + _tableName;
                    try { bulkCopy.WriteToServer(_newValues); }// Write from the source to the destination.
                    catch(Exception ex) { Console.WriteLine(ex.Message); }
                }
            }
        }
        public static DataTable idList2Table(List<int> ids) {
            DataTable dt = new DataTable();
            dt.Columns.Add("id", typeof(System.Int32));
            foreach(int t in ids) {
                DataRow dr = dt.NewRow();
                dr["id"] = t;
                dt.Rows.Add(dr);
            }
            return dt;
        }
        public static DataTable idList2TableNullable(List<int?> ids) {
            DataTable dt = new DataTable();
            dt.Columns.Add("id", typeof(System.Int32));
            if(ids != null) {
                foreach(int t in ids) {
                    DataRow dr = dt.NewRow();
                    dr["id"] = t;
                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }
        public static DataTable twoIDsList2Table(List<twoIDs> ids) {
            DataTable dt = new twoIDs().dt2IDs("dt2IDs");
            foreach(twoIDs e in ids) {
                DataRow dr = dt.NewRow();
                dr["id1"] = e.empID;
                dr["id2"] = e.ID2;
                dt.Rows.Add(dr);
            }
            return dt;
        }
        public static DataTable twoIntsList2Table(List<twoInts> ids) {
            DataTable dt = new twoInts().dt2Ints("dt2Ints");
            foreach(twoInts e in ids) {
                DataRow dr = dt.NewRow();
                dr["int1"] = e.int1;
                dr["int2"] = e.int2;
                dt.Rows.Add(dr);
            }
            return dt;
        }
        public static DataTable threeIDsList2Table(List<threeIDs> ids) {
            DataTable dt = new threeIDs().dt2IDs("dt2IDs");
            foreach(threeIDs e in ids) {
                DataRow dr = dt.NewRow();
                dr["id1"] = e.id1;
                dr["id2"] = e.id2;
                dr["id3"] = e.id3;
                dt.Rows.Add(dr);
            }
            return dt;
        }
        public static List<T> ConvertDataTable2LIST<T>(DataTable dt) {
            List<T> data = new List<T>();
            foreach(DataRow row in dt.Rows) {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        public static T GetItem<T>(DataRow dr) {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach(DataColumn column in dr.Table.Columns) {
                foreach(PropertyInfo pro in temp.GetProperties()) {
                    if(pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
    }
    public class twoInts {
        public int int1 { get; set; }
        public int int2 { get; set; }
        public DataTable dt2Ints(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("int1", typeof(System.Int32));
            dtName.Columns.Add("int2", typeof(System.Int32));
            return dtName;
        }
    }
    public class twoIDs {
        public int empID { get; set; }
        public int ID2 { get; set; }
        public DataTable dt2IDs(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("id1", typeof(System.Int32));
            dtName.Columns.Add("id2", typeof(System.Int32));
            return dtName;
        }
    }
    public class threeIDs {
        public int id1 { get; set; }
        public int id2 { get; set; }
        public int id3 { get; set; }
        public DataTable dt2IDs(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("id1", typeof(System.Int32));
            dtName.Columns.Add("id2", typeof(System.Int32));
            dtName.Columns.Add("id3", typeof(System.Int32));
            return dtName;
        }
    }
    public class intBool {
        public int id { get; set; }
        public bool bl { get; set; }
        public intBool() { }
        public intBool(int _id, bool _bl) { id = _id; bl = _bl; }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("id", typeof(System.Int32));
            dtName.Columns.Add("bl", typeof(System.Boolean));
            return dtName;
        }
    }
    public class intString {
        public int id { get; set; }
        public string str { get; set; }
        public intString() { }
        public intString(int _id, string _str) { id = _id; str = _str; }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("id", typeof(System.Int32));
            dtName.Columns.Add("str", typeof(System.String));
            return dtName;
        }
    }
    public class intIntString {
        public int id1 { get; set; }
        public int id2 { get; set; }
        public string str { get; set; }
        public intIntString() { }
        public intIntString(int _id1, int _id2, string _str) { id1 = _id1; id2 = _id2; str = _str; }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("id1", typeof(System.Int32));
            dtName.Columns.Add("id2", typeof(System.Int32));
            dtName.Columns.Add("str", typeof(System.String));
            return dtName;
        }
    }
    public class intObject {
        public int records { get; set; }
        public object objList { get; set; }
        public intObject() { }
        public intObject(int _id, object _obj) { records = _id; objList = _obj; }
    }
    public class intIntArray {
        public int id { get; set; }
        public List<int> intAr { get; set; }
        public intIntArray() { }
        public intIntArray(int _id, List<int> _intAr) { id = _id; intAr = _intAr; }
    }
    public class intStrIntArray {
        public int id { get; set; }
        public string str { get; set; }
        public List<int> intAr { get; set; }
        public intStrIntArray() { }
        public intStrIntArray(int _id, string _str, List<int> _intAr) { id = _id; str = _str; intAr = _intAr; }
    }
    public class twoArrays {
        public List<int> array1 { get; set; }
        public List<int> array2 { get; set; }
        public twoArrays() { }
        public twoArrays(List<int> ar1, List<int> ar2) { array1 = ar1; array2 = ar2; }
        //{array1: [6,2,1],array2:[24460, 246461]}
    }
}