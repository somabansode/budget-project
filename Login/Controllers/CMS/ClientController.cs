﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Login.Controllers.CMS {
    public class ClientController :ApiController {

        [HttpGet]// http://localhost/Login/api/client/types
        [Route("api/client/types")]
        public async Task<HttpResponseMessage> types() {
            try {
                List<T_G_ClientType> clT_G_ClientType = await Task.Run(() => clDAL.ReadT_G_ClientTypeAsync());
                return Request.CreateResponse(HttpStatusCode.OK, clT_G_ClientType);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]// http://localhost/Login/api/client/get?cid=1 //&cp=1&ps=20
        [Route("api/client/get")]
        public async Task<HttpResponseMessage> getT_G_Employer(int cid) {
            try {
                //Tuple<int, List<T_G_Employer>> clEmployers = await Task.Run(() => clDAL.ReadEmployerAsync(cid));
                List<T_G_Employer> clEmployers = await Task.Run(() => clDAL.ReadEmployerAsync(cid));
                //intObject intObject = new intObject(clEmployers.Item1, clEmployers.Item2);
                return Request.CreateResponse(HttpStatusCode.OK, clEmployers);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost]// http://localhost/Login/api/client/bulkInsert?cid=1&userID=1
        [Route("api/client/bulkInsert")]
        public async Task<HttpResponseMessage> BulkCreateEmployerAsync([FromBody]List<T_G_Employer> clEmployer, int cid, int userID) {
            try {   //[{EmpName: 'Cleint 1', EmpAddress: 'Cleint 1 Address', ContactPerson: 'Cleint 1 person', Mobile: '1234567890', OfficeNo: '1234567899', CompanyID: 1, ClientType: 2, StateId: 2},
                    //{EmpName: 'Cleint 2', EmpAddress: 'Cleint 2 Address', ContactPerson: 'Cleint 2 person', Mobile: '2234567890', OfficeNo: '2234567899', CompanyID: 1, ClientType: 2, StateId: 2}]
                string result = await Task.Run(() => clDAL.CreateEmployerAsync(clEmployer, cid, userID));
                if(!result.Contains("failed")) {
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPut]// http://localhost/Login/api/client/BulkUpdate?cid=1&userID=1
        [Route("api/client/BulkUpdate")]
        public async Task<HttpResponseMessage> BulkUpdateEmployer([FromBody]List<T_G_Employer> clEmployer, int cid, int userID) {
            try {  // [{ClID: 59, EmpName: 'Cleint 28', EmpAddress: 'Cleint 28 Address', ContactPerson: 'Cleint 1 person', Mobile: '1234567890', OfficeNo: '1234567899', CompanyID: 1, ClientType: 2, StateId: 2, VenderID: 128},
                   //{ClID:60, EmpName: 'Cleint 29', EmpAddress: 'Cleint 29 Address', ContactPerson: 'Cleint 2 person', Mobile: '2234567890', OfficeNo: '2234567899', CompanyID: 1, ClientType: 2, StateId: 2, VenderID: 129}]        
                if(clEmployer.Count > 0) {
                    string result = await Task.Run(() => clDAL.UpdateEmployerAsync(clEmployer, cid, userID));
                    if(!result.Contains("failed")) {
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No records to Update");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpDelete] // http://localhost/Login/api/client/Delete?userID=1
        [Route("api/client/Delete")]
        public async Task<HttpResponseMessage> Delete([FromBody] List<int> IDs, int userID) {
            try {
                if(IDs.Count > 0) {
                    string result = await Task.Run(() => clDAL.DeleteEmployerAsync(IDs, userID));
                    if(!result.Contains("failed")) {
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No records to Delete");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        
        [HttpGet]  // http://localhost/Login/api/client/Offices?cid=1&clID=1
        [Route("api/client/Offices")]
        public async Task<HttpResponseMessage> getClientOffices(int cid, int? clID) {
            try {
                List<ClientOffices> clClientOffices = await Task.Run(() => clDAL.ReadClientOfficesAsync(cid, clID));
                return Request.CreateResponse(HttpStatusCode.OK, clClientOffices);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost]// http://localhost/Login/api/client/Offices?cid=1&userID=1
        [Route("api/client/Offices")]
        public async Task<HttpResponseMessage> BulkCreateClientOfficesAsync([FromBody]List<ClientOffices> clClientOffices, int cid, int userID) {
            try { //[{  OrgID: 18, OfficeName: 'Lower office', status: 1, Under: 14, Address: 'adree trial 14'}]
                string result = await Task.Run(() => clDAL.CreateClientOfficesAsync(clClientOffices, cid, userID));
                if(!result.Contains("failed")) {
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPut] // http://localhost/Login/api/client/Offices?cid=1&userID=xx
        [Route("api/client/Offices")]
        public async Task<HttpResponseMessage> BulkUpdateClientOffices([FromBody]List<ClientOffices> clClientOffices, int cid, int userID) {
            try {  //[{officeId: 31,  OrgID: 18, OfficeName: 'Lower office11', status: 1, Under: 14, Address: 'adreess trial 1411'}]
                if(clClientOffices.Count > 0) {
                    string result = await Task.Run(() => clDAL.UpdateClientOfficesAsync(clClientOffices, cid, userID));
                    if(!result.Contains("failed")) {
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No records to Update");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpDelete] // http://localhost/Login/api/client/Offices?userID=1
        [Route("api/client/Offices")]
        public async Task<HttpResponseMessage> BulkDeleteClientOffices([FromBody] List<int> IDs, int userID) {
            try {
                if(IDs.Count > 0) {
                    string result = await Task.Run(() => clDAL.DeleteClientOfficesAsync(IDs, userID));
                    if(!result.Contains("failed")) {
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No records to Delete");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }

}

