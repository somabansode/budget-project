﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
//FILE UPLOAD in ANGULAR >> https://stackoverflow.com/questions/19723064/webapi-formdata-upload-to-db-with-extra-parameters/19728580#19728580
namespace Login.Controllers.CMS {
    public class clDAL {
        public static async Task<List<T_G_ClientType>> ReadT_G_ClientTypeAsync() {
            List<T_G_ClientType> clT_G_ClientType = new List<T_G_ClientType>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "select * from T_G_ClientType";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            T_G_ClientType t = new T_G_ClientType();
                            t.ID = rdr["ID"] == DBNull.Value ? -1 : (int)rdr["ID"];
                            t.Type = rdr["Type"] == DBNull.Value ? "" : rdr["Type"].ToString();
                            t.ShortName = rdr["ShortName"] == DBNull.Value ? "" : rdr["ShortName"].ToString();
                            t.CreatedBy = rdr["CreatedBy"] == DBNull.Value ? -1 : (int)rdr["CreatedBy"];
                            t.CreatedON = rdr["CreatedON"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["CreatedON"]);
                            t.IsActive = rdr["IsActive"] == DBNull.Value ? -1 : (int)rdr["IsActive"];
                            t.Code = rdr["Code"] == DBNull.Value ? "" : rdr["Code"].ToString();
                            clT_G_ClientType.Add(t);
                        }
                    }
                }
                con.Close();
                return clT_G_ClientType;
            }
        }
        public static async Task<List<T_G_Employer>> ReadEmployerAsync(int cid) {//, int cp, int ps
            List<T_G_Employer> clEmployers = new List<T_G_Employer>();
            //int totalRecords = 0;
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "cms.EmployerRead";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    //cmd.Parameters.Add(new SqlParameter("@cp", cp));
                    //cmd.Parameters.Add(new SqlParameter("@ps", ps));
                    //cmd.Parameters.Add(new SqlParameter("@tr", SqlDbType.Int)).Direction = ParameterDirection.Output;
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            T_G_Employer t = new T_G_Employer();
                            t.ClID = rdr["ClID"] == DBNull.Value ? -1 : (int)rdr["ClID"];
                            t.LedgerID = rdr["LedgerID"] == DBNull.Value ? -1 : (int)rdr["LedgerID"];
                            t.EmpName = rdr["EmpName"] == DBNull.Value ? "" : rdr["EmpName"].ToString();
                            t.EmpAddress = rdr["EmpAddress"] == DBNull.Value ? "" : rdr["EmpAddress"].ToString();
                            t.ContactPerson = rdr["ContactPerson"] == DBNull.Value ? "" : rdr["ContactPerson"].ToString();
                            t.Mobile = rdr["Mobile"] == DBNull.Value ? "" : rdr["Mobile"].ToString();
                            t.OfficeNo = rdr["OfficeNo"] == DBNull.Value ? "" : rdr["OfficeNo"].ToString();
                            t.homeNo = rdr["homeNo"] == DBNull.Value ? "" : rdr["homeNo"].ToString();
                            t.BitsStatus = rdr["BitsStatus"] == DBNull.Value ? -1 : (int)rdr["BitsStatus"];
                            t.CreatedOn = rdr["CreatedOn"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["CreatedOn"]);
                            t.CreatedBy = rdr["CreatedBy"] == DBNull.Value ? -1 : (int)rdr["CreatedBy"];
                            t.UpdatedOn = rdr["UpdatedOn"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["UpdatedOn"]);
                            t.PreOrder = rdr["PreOrder"] == DBNull.Value ? "" : rdr["PreOrder"].ToString();
                            t.UpdatedBY = rdr["UpdatedBY"] == DBNull.Value ? -1 : (int)rdr["UpdatedBY"];
                            t.StateId = rdr["StateId"] == DBNull.Value ? -1 : (int)rdr["StateId"];
                            t.UserName = rdr["UserName"] == DBNull.Value ? "" : rdr["UserName"].ToString();
                            t.Password = rdr["Password"] == DBNull.Value ? "" : rdr["Password"].ToString();
                            t.VenderID = rdr["VenderID"] == DBNull.Value ? -1 : (int)rdr["VenderID"];
                            t.CompanyID = rdr["CompanyID"] == DBNull.Value ? -1 : (int)rdr["CompanyID"];
                            t.ClientType = rdr["ClientType"] == DBNull.Value ? -1 : (int)rdr["ClientType"];
                            t.HardCopy = rdr["HardCopy"] == DBNull.Value ? "" : rdr["HardCopy"].ToString();
                            clEmployers.Add(t);
                        }
                    }
                    // totalRecords = (int)cmd.Parameters["@tr"].Value;
                }
                con.Close();
                return  clEmployers;
            }
        }
        public static async Task<string> CreateEmployerAsync(List<T_G_Employer> clEmployer, int cid, int userID) {
            try {
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    string res = string.Empty;
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("createT_G_Employer"));
                    T_G_Employer t = new T_G_Employer();
                    DataTable dtClass = t.dtClass("dtClass");
                    int i = 1;
                    foreach(T_G_Employer E in clEmployer) {
                        int j = i++;
                        DataRow dr = dtClass.NewRow();
                        dr["ClID"] = j;
                        dr["LedgerID"] = DBNull.Value;
                        dr["EmpName"] = E.EmpName;
                        dr["EmpAddress"] = E.EmpAddress;
                        dr["ContactPerson"] = E.ContactPerson;
                        dr["Mobile"] = E.Mobile;
                        dr["OfficeNo"] = E.OfficeNo;
                        dr["homeNo"] = E.homeNo;
                        dr["BitsStatus"] = 1;
                        dr["CreatedOn"] = DateTime.Now;
                        dr["CreatedBy"] = userID;
                        dr["UpdatedOn"] = DBNull.Value;
                        dr["PreOrder"] = E.PreOrder;
                        dr["UpdatedBY"] = DBNull.Value;
                        dr["StateId"] = E.StateId;
                        dr["UserName"] = E.UserName;
                        dr["Password"] = E.Password;
                        dr["VenderID"] = DBNull.Value;
                        dr["CompanyID"] = cid;
                        dr["ClientType"] = 2;
                        dr["HardCopy"] = DBNull.Value;
                        dr["ss"] = j;
                        dtClass.Rows.Add(dr);
                    }
                    using(SqlBulkCopy bcp = new SqlBulkCopy(con, SqlBulkCopyOptions.Default, tran)) {
                        bcp.DestinationTableName = "[dbo].[T_G_Employer]";
                        try {
                            await bcp.WriteToServerAsync(dtClass);
                            List<twoIDs> clIDs = await Task.Run(() => getInsertedClIDs(con, tran));
                            DataTable dtTwo = common.twoIDsList2Table(clIDs);
                            var joinResult = from t1 in dtClass.AsEnumerable() join t2 in dtTwo.AsEnumerable()
                                on Convert.ToInt32(t1.Field<int>("ClID")) equals Convert.ToInt32(t2.Field<int>("id2"))
                                             select new {
                                                 ClID = Convert.ToInt32(t2["id1"]),
                                                 EmpName = t1["EmpName"].ToString(),
                                                 EmpAddress = t1["EmpAddress"].ToString(),
                                                 ContactPerson = t1["ContactPerson"].ToString(),
                                                 Mobile = t1["Mobile"].ToString(),
                                                 OfficeNo = t1["OfficeNo"].ToString(),
                                                 homeNo = t1["homeNo"].ToString(),
                                                 PreOrder = t1["PreOrder"].ToString(),
                                                 StateId = Convert.ToInt32(t1["StateId"]),
                                                 UserName = t1["UserName"].ToString(),
                                                 Password = t1["Password"].ToString(),
                                                 VenderID = DBNull.Value,
                                                 CompanyID = Convert.ToInt32(t1["CompanyID"]),
                                                 ClientType = Convert.ToInt32(t1["ClientType"])
                                             };
                            DataTable dtType = t.dtTypes("dtType");
                            foreach(var E in joinResult) {
                                DataRow dr = dtType.NewRow();
                                dr["ClID"] = i++;
                                dr["EmpName"] = E.EmpName;
                                dr["EmpAddress"] = E.EmpAddress;
                                dr["ContactPerson"] = E.ContactPerson;
                                dr["Mobile"] = E.Mobile;
                                dr["OfficeNo"] = E.OfficeNo;
                                dr["homeNo"] = E.homeNo;
                                dr["PreOrder"] = E.PreOrder;
                                dr["StateId"] = E.StateId;
                                dr["UserName"] = E.UserName;
                                dr["Password"] = E.Password;
                                dr["VenderID"] = DBNull.Value;
                                dr["CompanyID"] = cid;
                                dr["ClientType"] = 2;
                                dtType.Rows.Add(dr);
                            }
                            string query = "cms.EmployerUpdate";
                            int recordsUpdated = 0;
                            using(SqlCommand cmd = new SqlCommand(query, con, tran)) {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add(new SqlParameter("@tblType", dtType));
                                cmd.Parameters.Add(new SqlParameter("@userID", userID));
                                cmd.Parameters.Add(new SqlParameter("@result", SqlDbType.VarChar, 100)).Direction = ParameterDirection.Output;
                                recordsUpdated = await cmd.ExecuteNonQueryAsync();
                                res = cmd.Parameters["@result"].Value.ToString();
                            }
                            if(res == "success") {
                                tran.Commit();
                                con.Close();
                                return clEmployer.Count.ToString() + " record(s) inserted!";
                            } else {
                                tran.Rollback();
                                con.Close();
                                return "failed ";
                            }
                        } catch(Exception ex) {
                            tran.Rollback();
                            con.Close();
                            return "failed " + ex.Message;
                        }
                    }
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
        private static async Task<List<twoIDs>> getInsertedClIDs(SqlConnection conn, SqlTransaction tran) {
            SqlCommand cmd = new SqlCommand("select ClID, ss  from t_g_employer where ss > 0", conn, tran);
            List<twoIDs> clIDs = new List<twoIDs>();
            using(SqlDataReader reader = await cmd.ExecuteReaderAsync()) {
                while(await reader.ReadAsync()) {
                    twoIDs two = new twoIDs();
                    two.empID = (int)reader[0];
                    two.ID2 = (int)reader[1];
                    clIDs.Add(two);
                }
            }
            using(SqlCommand cmd2 = new SqlCommand("update  t_g_employer set ss = null where ss > 0", conn, tran)) {
                await cmd.ExecuteNonQueryAsync();
            }
            return clIDs;
        }
        public static async Task<string> UpdateEmployerAsync(List<T_G_Employer> clEmployer, int cid, int userID) {
            try {
                string res = string.Empty;
                DataTable dtTypes = new T_G_Employer().dtTypes("dtTypes");
                foreach(T_G_Employer E in clEmployer) {
                    DataRow dr = dtTypes.NewRow();
                    dr["ClID"] = E.ClID;
                    dr["EmpName"] = E.EmpName;
                    dr["EmpAddress"] = E.EmpAddress;
                    dr["ContactPerson"] = E.ContactPerson;
                    dr["Mobile"] = E.Mobile;
                    dr["OfficeNo"] = E.OfficeNo;
                    dr["homeNo"] = E.homeNo;
                    dr["PreOrder"] = E.PreOrder;
                    dr["StateId"] = (object)E.StateId ?? DBNull.Value;
                    dr["UserName"] = E.UserName;
                    dr["Password"] = E.Password;
                    dr["VenderID"] = (object)E.VenderID ?? DBNull.Value;
                    dr["CompanyID"] = E.CompanyID;
                    dr["ClientType"] = E.ClientType;
                    dtTypes.Rows.Add(dr);
                }
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("blkUpdate"));
                    string query = "cms.EmployerUpdate";
                    int recordsUpdated = 0;
                    using(SqlCommand cmd = new SqlCommand(query, con, tran)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@tblType", dtTypes));
                        cmd.Parameters.Add(new SqlParameter("@userID", userID));
                        cmd.Parameters.Add(new SqlParameter("@result", SqlDbType.VarChar, 100)).Direction = ParameterDirection.Output;
                        recordsUpdated = await cmd.ExecuteNonQueryAsync();
                        res = cmd.Parameters["@result"].Value.ToString();
                    }
                    if(res == "success") {
                        tran.Commit();
                        con.Close();
                        return clEmployer.Count.ToString() + " record(s) updated!";
                    } else {
                        tran.Rollback();
                        con.Close();
                        return "failed ";
                    }
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
        public static async Task<string> DeleteEmployerAsync(List<int> ids, int userID) {
            try {
                DataTable dt = common.idList2Table(ids);
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("blkDelete"));
                    string query = "cms.EmployerDelete";
                    int recordsUpdated = 0;
                    using(SqlCommand cmd = new SqlCommand(query, con, tran)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@tblType", dt));
                        cmd.Parameters.Add(new SqlParameter("@userID", userID));
                        recordsUpdated = await cmd.ExecuteNonQueryAsync();
                    }
                    if(recordsUpdated > 0) tran.Commit();
                    else return "failed to update!";
                    con.Close();
                    return recordsUpdated.ToString() + " records(s) deleted";
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
        public static async Task<List<ClientOffices>> ReadClientOfficesAsync(int cid, int? clID) {
            List<ClientOffices> clClientOffices = new List<ClientOffices>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "cms.clientOfficesRead";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    cmd.Parameters.Add(new SqlParameter("@clID", clID));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            ClientOffices t = new ClientOffices();
                            t.officeID = rdr["officeID"] == DBNull.Value ? -1 : (int)rdr["officeID"];
                            t.OrgID = rdr["OrgID"] == DBNull.Value ? -1 : (int)rdr["OrgID"];
                            t.OfficeName = rdr["OfficeName"] == DBNull.Value ? "" : rdr["OfficeName"].ToString();
                            t.status = rdr["status"] == DBNull.Value ? -1 : (int)rdr["status"];
                            t.Under = rdr["Under"] == DBNull.Value ? -1 : (int)rdr["Under"];
                            t.Address = rdr["Address"] == DBNull.Value ? "" : rdr["Address"].ToString();
                            clClientOffices.Add(t);
                        }
                    }
                }
                con.Close();
                return clClientOffices;
            }
        }
        public static async Task<string> CreateClientOfficesAsync(List<ClientOffices> clClientOffices, int cid, int userID) {
            try {
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("createClientOffices"));
                    ClientOffices t = new ClientOffices();
                    DataTable dtClass = t.dtClass("dtClass");
                    int i = 0; 
                    foreach(ClientOffices E in clClientOffices) {
                        DataRow dr = dtClass.NewRow();
                        dr["officeID"] = i++;
                        dr["OrgID"] = E.OrgID;
                        dr["OfficeName"] = E.OfficeName;
                        dr["Under"] = E.Under;
                        dr["Address"] = E.Address;
                        dtClass.Rows.Add(dr);
                    }
                    string query = "cms.ClientOfficesCreate";
                    int recordsUpdated = 0;
                    using(SqlCommand cmd = new SqlCommand(query, con, tran)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@tblType", dtClass));
                        cmd.Parameters.Add(new SqlParameter("@userID", userID));
                        recordsUpdated = await cmd.ExecuteNonQueryAsync();
                    }
                    if(recordsUpdated > 0) {  
                        tran.Commit();  con.Close(); 
                        return recordsUpdated.ToString() + " records(s) created";
                    }
                    else { 
                        con.Close(); 
                        return "failed to update!";
                    }
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
        public static async Task<string> UpdateClientOfficesAsync(List<ClientOffices> clClientOffices, int cid, int userID) {
            try {
                DataTable dtClass = new ClientOffices().dtClass("dtClass");
                foreach(ClientOffices E in clClientOffices) {
                    DataRow dr = dtClass.NewRow();
                    dr["officeID"] = E.officeID;
                    dr["OrgID"] = E.OrgID;
                    dr["OfficeName"] = E.OfficeName;
                    dr["Under"] = E.Under;
                    dr["Address"] = E.Address;
                    dtClass.Rows.Add(dr);
                }
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("blkUpdate"));
                    string query = "cms.ClientOfficesUpdate";
                    int recordsUpdated = 0;
                    using(SqlCommand cmd = new SqlCommand(query, con, tran)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@tblType", dtClass));
                        cmd.Parameters.Add(new SqlParameter("@userID", userID));
                        recordsUpdated = await cmd.ExecuteNonQueryAsync();
                    }
                     if(recordsUpdated > 0) {  
                        tran.Commit();  con.Close(); 
                        return recordsUpdated.ToString() + " records(s) updated!";
                    }
                     else { 
                        con.Close(); 
                        return "failed to update!";
                    }
                   
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
        public static async Task<string> DeleteClientOfficesAsync(List<int> ids, int userID) {
            try {
                DataTable dt = common.idList2Table(ids);
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("blkDelete"));
                    string query = "cms.ClientOfficesDelete";
                    int recordsUpdated = 0;
                    using(SqlCommand cmd = new SqlCommand(query, con, tran)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@tblType", dt));
                        cmd.Parameters.Add(new SqlParameter("@userID", userID));
                        recordsUpdated = await cmd.ExecuteNonQueryAsync();
                    }
                    if(recordsUpdated > 0) tran.Commit();
                    else return "failed to update!";
                    con.Close();
                    return recordsUpdated.ToString() + " records(s) deleted";
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
    }
}

public class T_G_ClientType {
    public int ID { get; set; }
    public string Type { get; set; }
    public string ShortName { get; set; }
    public int CreatedBy { get; set; }
    public DateTime CreatedON { get; set; }
    public int IsActive { get; set; }
    public string Code { get; set; }

    public T_G_ClientType() { }    public T_G_ClientType(int _ID, string _Type, string _ShortName, int _CreatedBy, DateTime _CreatedON, int _IsActive, string _Code) {        ID = _ID; Type = _Type; ShortName = _ShortName; CreatedBy = _CreatedBy; CreatedON = _CreatedON; IsActive = _IsActive; Code = _Code;
    }    public DataTable dtClass(string tblName) {
        DataTable dtName = new DataTable(tblName);
        dtName.Columns.Add("ID", typeof(System.Int32));
        dtName.Columns.Add("Type", typeof(System.String));
        dtName.Columns.Add("ShortName", typeof(System.String));
        dtName.Columns.Add("CreatedBy", typeof(System.Int32));
        dtName.Columns.Add("CreatedON", typeof(System.DateTime));
        dtName.Columns.Add("IsActive", typeof(System.Int32));
        dtName.Columns.Add("Code", typeof(System.String));
        return dtName;
    }}
public class T_G_Employer {
    public int ClID { get; set; }
    public int? LedgerID { get; set; }
    public string EmpName { get; set; }
    public string EmpAddress { get; set; }
    public string ContactPerson { get; set; }
    public string Mobile { get; set; }
    public string OfficeNo { get; set; }
    public string homeNo { get; set; }
    public int BitsStatus { get; set; }
    public DateTime CreatedOn { get; set; }
    public int CreatedBy { get; set; }
    public DateTime UpdatedOn { get; set; }
    public string PreOrder { get; set; }
    public int UpdatedBY { get; set; }
    public int? StateId { get; set; }
    public string UserName { get; set; }
    public string Password { get; set; }
    public int? VenderID { get; set; }
    public int CompanyID { get; set; }
    public int ClientType { get; set; }
    public string HardCopy { get; set; }
    public int ss { get; set; }

    public T_G_Employer() { }
    public T_G_Employer(int _ClID, int _LedgerID, string _EmpName, string _EmpAddress, string _ContactPerson, string _Mobile
        , string _OfficeNo, string _homeNo, int _BitsStatus, DateTime _CreatedOn, int _CreatedBy, DateTime _UpdatedOn
        , string _PreOrder, int _UpdatedBY, int _StateId, string _UserName, string _Password, int _VenderID, int _CompanyID
        , int _ClientType, string _HardCopy, int _ss) {
        ClID = _ClID; LedgerID = _LedgerID; EmpName = _EmpName; EmpAddress = _EmpAddress; ContactPerson = _ContactPerson;
        Mobile = _Mobile; OfficeNo = _OfficeNo; homeNo = _homeNo; BitsStatus = _BitsStatus; CreatedOn = _CreatedOn; CreatedBy = _CreatedBy;
        UpdatedOn = _UpdatedOn; PreOrder = _PreOrder; UpdatedBY = _UpdatedBY; StateId = _StateId; UserName = _UserName;
        Password = _Password; VenderID = _VenderID; CompanyID = _CompanyID; ClientType = _ClientType; HardCopy = _HardCopy; ss = _ss;
    }
    public DataTable dtClass(string tblName) {
        DataTable dtName = new DataTable(tblName);
        DataColumn ClID = new DataColumn();
        ClID.DataType = System.Type.GetType("System.Int32");
        ClID.ColumnName = "ClID";
        ClID.AutoIncrement = true;
        dtName.Columns.Add(ClID);
        //dtName.Columns.Add("ClID", typeof(System.Int32));
        dtName.Columns.Add("LedgerID", typeof(System.Int32));
        dtName.Columns.Add("EmpName", typeof(System.String));
        dtName.Columns.Add("EmpAddress", typeof(System.String));
        dtName.Columns.Add("ContactPerson", typeof(System.String));
        dtName.Columns.Add("Mobile", typeof(System.String));
        dtName.Columns.Add("OfficeNo", typeof(System.String));
        dtName.Columns.Add("homeNo", typeof(System.String));
        dtName.Columns.Add("BitsStatus", typeof(System.Int32));
        dtName.Columns.Add("CreatedOn", typeof(System.DateTime));
        dtName.Columns.Add("CreatedBy", typeof(System.Int32));
        dtName.Columns.Add("UpdatedOn", typeof(System.DateTime));
        dtName.Columns.Add("PreOrder", typeof(System.String));
        dtName.Columns.Add("UpdatedBY", typeof(System.Int32));
        dtName.Columns.Add("StateId", typeof(System.Int32));
        dtName.Columns.Add("UserName", typeof(System.String));
        dtName.Columns.Add("Password", typeof(System.String));
        dtName.Columns.Add("VenderID", typeof(System.Int32));
        dtName.Columns.Add("CompanyID", typeof(System.Int32));
        dtName.Columns.Add("ClientType", typeof(System.Int32));
        dtName.Columns.Add("HardCopy", typeof(System.String));
        dtName.Columns.Add("ss", typeof(System.Int32));

        DataColumn[] keys = new DataColumn[1];
        keys[0] = ClID;
        dtName.PrimaryKey = keys;
        return dtName;
    }    public DataTable dtTypes(string tblName) {
        DataTable dtName = new DataTable(tblName);
        dtName.Columns.Add("ClID", typeof(System.Int32));
        dtName.Columns.Add("EmpName", typeof(System.String));
        dtName.Columns.Add("EmpAddress", typeof(System.String));
        dtName.Columns.Add("ContactPerson", typeof(System.String));
        dtName.Columns.Add("Mobile", typeof(System.String));
        dtName.Columns.Add("OfficeNo", typeof(System.String));
        dtName.Columns.Add("homeNo", typeof(System.String));
        dtName.Columns.Add("PreOrder", typeof(System.String));
        dtName.Columns.Add("StateId", typeof(System.Int32));
        dtName.Columns.Add("UserName", typeof(System.String));
        dtName.Columns.Add("Password", typeof(System.String));
        dtName.Columns.Add("VenderID", typeof(System.Int32));
        dtName.Columns.Add("CompanyID", typeof(System.Int32));
        dtName.Columns.Add("ClientType", typeof(System.Int32));
        return dtName;
    }}
public class ClientOffices {// T_CMS_OrganizationOffices
    public int officeID { get; set; }
    public int OrgID { get; set; }
    public string OfficeName { get; set; }
    public int status { get; set; }
    public int Under { get; set; }
    public string Address { get; set; }

    public ClientOffices() { }
    public ClientOffices(int _officeID, int _OrgID, string _OfficeName, int _status, int _Under, string _Address) {
        officeID = _officeID; OrgID = _OrgID; OfficeName = _OfficeName; status = _status; Under = _Under; Address = _Address;
    }    public DataTable dtClass(string tblName) {
        DataTable dtName = new DataTable(tblName);
        dtName.Columns.Add("officeID", typeof(System.Int32));
        dtName.Columns.Add("OrgID", typeof(System.Int32));
        dtName.Columns.Add("OfficeName", typeof(System.String));
        dtName.Columns.Add("Under", typeof(System.Int32));
        dtName.Columns.Add("Address", typeof(System.String));
        return dtName;
    }
}