﻿using SD.COMMON.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
namespace Login {
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LoginController : ApiController {
        
      [HttpGet]  public List<DropdownItem> GetCompanyList() { //http://localhost/login/api/login/getcompanylist
            List<DropdownItem> list = new List<DropdownItem>();
            DataSet dscomp = SQLDBUtil.ExecuteDataset("Get_Companies");
            foreach (DataRow dr in dscomp.Tables[0].Rows) {
                list.Add(new DropdownItem { Text = dr["companyname"].ToString(), Value = dr["companyid"].ToString() });
            }
            return list;
        }
        
       [Authorize] [HttpGet] public HttpResponseMessage Get(string email) { //http://localhost/Login/api/login?email=A1@A1.com
            try
            {
                //String roleId = SQLDBUtil.ExecuteScalar("select cid from t_g_employeemaster where username = '" + email + "'", CommandType.Text).ToString();
                SqlParameter[] parms = new SqlParameter[1];
                parms[0] = new SqlParameter("@email", email);
                DataSet ds = SqlHelper.ExecuteDataset("scp_loginResult", parms);
                loginResult lr = new loginResult();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    lr.cid = (int)ds.Tables[0].Rows[0][0];
                    lr.isVen = (int)ds.Tables[0].Rows[0][1];
                }
                return Request.CreateResponse(HttpStatusCode.OK, lr);
            }
            catch (Exception ex) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message); }
        }
        [HttpPost]  public UserData GetLoginDetails(Login obj) {
            try {
                UserData userData = new UserData();
                SqlParameter[] parms1 = new SqlParameter[2];
                parms1[0] = new SqlParameter("@Login_Name", obj.UserName);
                parms1[1] = new SqlParameter("@Password", GetMD5Hash(obj.Password.Trim()));
                userData.UserId = obj.UserName;//same as loginId/Email
                userData.password = GetMD5Hash(obj.Password); //objUserData.password = "BBAD8D72C1FAC1D081727158807A8798";
                DataSet ds1 = SqlHelper.ExecuteDataset("scp_MultiLoginChecking", parms1);
                if (ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0) {
                    DataRow dr = ds1.Tables[0].Rows[0];
                    int empID = (int)dr["Empid"];
                    if (empID > 0) {
                        userData.EmpID = empID.ToString();
                        userData.EmpName = dr["Name"].ToString();
                        userData.ModuleID = "1";
                        userData.CompanyID = obj.CompanyID.ToString();
                    } else {
                        userData.EmpID = empID.ToString();
                        userData.ErrorMsg = "<b>Unauthorized to Login</b>";
                    }
                }
                return userData;
            } catch (Exception) {
            }
            return null;
        }
        public string GetMD5Hash(string input) {
            System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] bs = System.Text.Encoding.UTF8.GetBytes(input);
            bs = x.ComputeHash(bs);
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            foreach (byte b in bs) {
                s.Append(b.ToString("x2").ToLower());
            }
            string password = s.ToString();
            return password;
        }
    }
    public class DropdownItem {
        public string Value { set; get; }
        public string Text { set; get; }
        public string Code { set; get; }
    }
    public class UserData {
        public string CompanyID { get; set; }
        public string UserId { get; set; }//same as loginId/Email
        public string ModuleID { get; set; }
        public string password { get; set; }
        public string EmpID { get; set; }
        public string EmpName { get; set; }
        public string ErrorMsg { get; set; }
    }
    public class Login {
        public string UserName { set; get; }
        public string Password { set; get; }
        public int CompanyID { get; set; }
    }
    public class loginResult
    {
        public int cid { get; set; }
        public int isVen { get; set; }
    }
}
