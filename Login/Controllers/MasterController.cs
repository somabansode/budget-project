﻿using SD.COMMON.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
// 1. FIRST API call >>  http://adc.6dproptech.com/Login/token
// 2. SECOND API call >> http://adc.6dproptech.com/Login/api/company?email=yudhi@aeclogic.com&mid=3
// 3. THIRD API CALL >> http://adc.6dproptech.com/Login/api/company/userDetails?userID=1&roleID=49
namespace Login {
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [Authorize] public class MasterController : ApiController {
        [HttpGet] public List<DropdownItem> GetModules() { // http://localhost/Login/api/master
            List<DropdownItem> list = new List<DropdownItem>();
            DataSet dscomp = SQLDBUtil.ExecuteDataset("getModules");
            foreach (DataRow dr in dscomp.Tables[0].Rows) {
                list.Add(new DropdownItem { Text = dr["FULLNAME"].ToString(), Value = dr["MODULEID"].ToString(), Code = dr["MODULENAME"].ToString() });
            }
            return list;
        }
        //[HttpGet] public HttpResponseMessage Get(string data) { //http://localhost/Login/api/master?data=A1@A1.com,1
        //    try {
        //        string[] ar = data.Split(',');
        //        SqlParameter[] sps = new SqlParameter[2];
        //        sps[0] = new SqlParameter("@userId", ar[0]);      //@userId nvarchar(100)
        //        sps[1] = new SqlParameter("@modId", int.Parse(ar[1]));                 //, @modId int
        //        DataSet ds = SQLDBUtil.ExecuteDataset("sg_getRole", sps);
        //        return Request.CreateResponse(HttpStatusCode.OK, ds);



        //    } catch (Exception ex ) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message); }
        //}
        [HttpGet]
        public HttpResponseMessage Get(string data)    { //http://localhost/Login/api/master?data=A1@A1.com,1
            try
            {
                string[] ar = data.Split(',');
                SqlParameter[] sps = new SqlParameter[2];
                sps[0] = new SqlParameter("@userId", ar[0]);      //@userId nvarchar(100)
                sps[1] = new SqlParameter("@modId", int.Parse(ar[1]));                 //, @modId int
                DataSet ds = SQLDBUtil.ExecuteDataset("sg_getRole", sps);
                return Request.CreateResponse(HttpStatusCode.OK, ds);
            } catch (Exception ex ) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message); }
        }
        // http://localhost/Login/api/master/GetUserMenu >> FromBody >> { ModuleID  : 1, ModuleName : 'Human Resource Management System' , ModuleCode: 'HMS'}
        //Authoristion >> Token >> PASTE HERE THE TOKEN
        [HttpPost] public HttpResponseMessage GetUserMenu(UserRole obj) {
            HttpError error = null;
            try {
                DataSet ds = SQLDBUtil.ExecuteDataset("cs_userModuleMenu_Cloud", new SqlParameter[] 
                        { new SqlParameter("@mids", obj.ModuleID), new SqlParameter("@uid", 12) });//?????????????
                List<NavMenu> lstMenu = ds.Tables[0].AsEnumerable().Select(datarow => new NavMenu {
                    id = datarow.Field<int>("MenuID"),
                    title = datarow.Field<string>("MenuName"),
                    code = datarow.Field<string>("ModuleName"),
                    parent = datarow.Field<int?>("Under"),
                    order = datarow.Field<int?>("PreOrder"),
                    type = "item",
                    url = datarow.Field<string>("URL"),
                }).ToList().OrderBy(s => s.parent).ToList();
                if (lstMenu != null) {
                    List<NavMenu> mainMenu = new List<NavMenu>();
                    List<NavMenu> herarichy = GetChildrens(lstMenu, null);
                    if (herarichy != null) {
                        herarichy.FindAll(s => s.children != null && s.children.Count > 0).ForEach(s => s.type = "collapse");
                        NavMenu main = new NavMenu { children = herarichy, type = "collapse", id = 0, title = obj.ModuleName, code = obj.ModuleCode };
                        mainMenu.Add(main);
                        return Request.CreateResponse(HttpStatusCode.OK, herarichy);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, herarichy);
                } else {
                    error = new HttpError("No Record Found");
                    return Request.CreateResponse(HttpStatusCode.NotFound, error);
                }
            } catch (Exception ex) {
                error = new HttpError(ex.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, error);
            }
        }
        
        private List<NavMenu> GetChildrens(List<NavMenu> allElements, NavMenu currentElement) {
            List<NavMenu> children = null;
            if (currentElement != null) {
                children = allElements.FindAll(s => s.parent == currentElement.id);
            } else {
                children = allElements.FindAll(s => s.parent == null);
            }
            foreach (NavMenu item in children) {
                item.children = GetChildrens(allElements, item);
            }
            return children;
        }
    }
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CompanyController : ApiController {
        //[Route("api/Company/{action}/{email}")]
       [Authorize] public HttpResponseMessage Get(string email) { //http://localhost/Login/api/company?email=A1@A1.com
            try {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ERPConnectionString"].ConnectionString)) {
                    SqlCommand cmd = new SqlCommand("select cid from t_g_employeemaster where username = '" + email + "'", con);
                    con.Open(); string ret = cmd.ExecuteScalar().ToString(); cmd.Dispose(); con.Close();
                    if (ret != null) return Request.CreateResponse(HttpStatusCode.OK, ret);
                    else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "0");
                }
            } catch(Exception ex) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message); }
        }
        
       [Authorize] public async Task<HttpResponseMessage> Get(string email, int mid) { 
            // Run the Login App in local and login with credentials 
            // copy accessToken from sessionStorage of browser developer tools 
            // Postman URL >> http://localhost/Login/api/company?email=yudhi@aeclogic.com&mid=1
            // Postman Headers >> Key >> Authorization; Value >> bearer <<<<<<ENCODED accessToken GOES HERE>>>>>>> ;
            try { 
                DataSet ds = await Task.Run(() => checkIn(email, mid));                if(ds != null) {
                    return Request.CreateResponse(HttpStatusCode.OK, ds);
                } else  return Request.CreateResponse(HttpStatusCode.BadRequest, "Authorization for this request is denied!");            } catch (Exception ex) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message); }
        }
        private DataSet checkIn(string email, int mid) {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ERPConnectionString"].ConnectionString)) {
                DataSet ds = SQLDBUtil.ExecuteDataset("sg_LoginCompanyRole", 
                        new SqlParameter[] { new SqlParameter("@email", email), new SqlParameter("@mid", mid) });
                if (ds != null) {
                    ds.Tables[0].TableName = "EmpCoyRoleIDs";
                    ds.Tables[1].TableName = "UserINFO";
                    ds.Tables[2].TableName = "ModulesVsRoles";
                    ds.Tables[3].TableName = "CurrentModuleMENU";
                    DataRow dr = ds.Tables[0].Rows[0];
                    cidRid cr = new cidRid((int)dr["eid"], (int)dr["cid"], (int)dr["rid"]);
                    dbSession dbs = new dbSession(cr.cid, cr.eid, mid);
                    dbs.PerformSession();
                    return ds;
                } else return null;
            }
        }
       [HttpGet] [Route("api/company/userDetails")]
        public HttpResponseMessage userDetails(int userID, int roleID ) {
            try {  //http://localhost/Login/api/company/userDetails?userID=1&roleID=49 
                SqlParameter[] sps = new SqlParameter[2];
                sps[0] = new SqlParameter("@eid", userID);
                sps[1] = new SqlParameter("@rid", roleID);
                DataSet ds = SQLDBUtil.ExecuteDataset("sb_userDetails", sps);
                if(ds.Tables[0].Rows.Count > 0) {
                    DataRow dr =  ds.Tables[0].Rows[0];
                    UserDetails t = new UserDetails();
                    t.EmployeeName = dr["EmployeeName"].ToString();
                    t.Designation =  dr["Designation"].ToString();
                    t.CompanyName =  dr["CompanyName"].ToString();
                    t.RoleName =  dr["RoleName"].ToString();
                    t.ModuleName =  dr["ModuleName"].ToString();
                    return Request.CreateResponse(HttpStatusCode.OK, t);
                } else return Request.CreateResponse(HttpStatusCode.OK, "No user found");
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        
    }
    public class MenuController : ApiController { 
        [HttpGet] [Route("api/menu/UserMenu")]
        public HttpResponseMessage GetUserMenu(string mids, int uid) {
            HttpError error = null;
            try {// http://localhost/Login/api/menu/UserMenu?mids=3&uid=1
                DataSet ds = SQLDBUtil.ExecuteDataset("cs_userModuleMenu_Cloud", new SqlParameter[] 
                        { new SqlParameter("@mids", mids), new SqlParameter("@uid", uid) }); 
                List<NavMenu> lstMenu = ds.Tables[0].AsEnumerable().Select(datarow => new NavMenu {
                    id = datarow.Field<int>("MenuID"),
                    title = datarow.Field<string>("MenuName"),
                    code = datarow.Field<string>("ModuleName"),
                    parent = datarow.Field<int?>("Under"),
                    order = datarow.Field<int?>("PreOrder"),
                    type = "item",
                    url = datarow.Field<string>("URL"),
                }).ToList().OrderBy(s => s.parent).ToList();
                if (lstMenu != null) {
                    List<NavMenu> mainMenu = new List<NavMenu>();
                    List<NavMenu> herarichy = GetChildrens(lstMenu, null);
                    if (herarichy != null) {
                        herarichy.FindAll(s => s.children != null && s.children.Count > 0).ForEach(s => s.type = "collapse");
                        //NavMenu main = new NavMenu { children = herarichy, type = "collapse", id = 0, title = "MODULE NAME", code = "MODULE CODE" };
                        //mainMenu.Add(main);
                        return Request.CreateResponse(HttpStatusCode.OK, herarichy);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, herarichy);
                } else {
                    error = new HttpError("No Record Found");
                    return Request.CreateResponse(HttpStatusCode.NotFound, error);
                }
            } catch (Exception ex) {
                error = new HttpError(ex.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, error);
            }
        }
        private List<NavMenu> GetChildrens(List<NavMenu> allElements, NavMenu currentElement) {
            List<NavMenu> children = null;
            if (currentElement != null) {
                children = allElements.FindAll(s => s.parent == currentElement.id);
            } else {
                children = allElements.FindAll(s => s.parent == null);
            }
            foreach (NavMenu item in children) {
                item.children = GetChildrens(allElements, item);
            }
            return children;
        }
    }

    class UserDetails {
        public string EmployeeName { get; set; }
        public string Designation { get; set; }
        public string CompanyName { get; set; }
        public string RoleName { get; set; }
        public string ModuleName { get; set; }
        						
    }
    class cidRid {
        public int eid { get; set; }
        public int cid { get; set; }
        public int rid { get; set; }
        public cidRid(int e, int c, int r) {
            eid = e; cid = c; rid = r;
        }
    }
    public class dbSession {
        private string erpCS = ConfigurationManager.ConnectionStrings["ERPConnectionString"].ConnectionString;
        public int LicenseID { get; set; }
        public int CompanyID { get; set; }
        public int UserID { get; set; }
        public int ModuleID { get; set; }
        public int SessionID { get; set; }
        public string Module { get; set; }
        public string Login { get; set; }
        public dbSession(int companyID, int userID, int moduleID) { CompanyID = companyID; UserID = userID; ModuleID = moduleID; }
        public void PerformSession() {
            using (SqlConnection con = new SqlConnection(erpCS)) {
                con.Open();
                using (SqlTransaction tran = con.BeginTransaction()) {
                    int sessionId = initializeSession(con, tran);
                    int c = clearPriviousSessions(CompanyID, UserID, con, tran);
                    int t = angularSession2DB(CompanyID, UserID, ModuleID, sessionId, con, tran);
                    //angularSession4DB(CompanyID, UserID, sessionId, con, tran);
                    //int q = AuthQueue(CompanyID, UserID, ModuleID, sessionId, con, tran);
                    int o = CompleteSession(sessionId, con, tran);
                    tran.Commit();
                    con.Close();
                }
            }
        }

        private int CompleteSession(int sessionId, SqlConnection conn, SqlTransaction tran) {
            SqlCommand cmd = new SqlCommand("DELETE FROM [dbo].[tg_LoginSession] WHERE [SessionID] = " + sessionId, conn, tran);
            return cmd.ExecuteNonQuery();
        }
        private int clearPriviousSessions(int CompanyID, int UserID, SqlConnection conn, SqlTransaction tran) {
            SqlCommand cmd = new SqlCommand(
                 "UPDATE [dbo].[tg_sessions] SET SessionID = NULL where cid = " + CompanyID + " and eid = " + UserID, conn, tran);
            return cmd.ExecuteNonQuery();
        }
        private int initializeSession(SqlConnection conn, SqlTransaction tran) {
            //1st Trip to database
            SqlCommand cmd = new SqlCommand("INSERT INTO [dbo].[tg_LoginSession]([TsCreated]) VALUES (CURRENT_TIMESTAMP)", conn, tran);
            cmd.ExecuteNonQuery();
            cmd = new SqlCommand("SELECT [SessionID] FROM [dbo].[tg_LoginSession] " + "WHERE @@ROWCOUNT > 0 and [SessionID] = SCOPE_IDENTITY()", conn, tran);
            return (int)cmd.ExecuteScalar();
        }
        private int angularSession2DB(int CompanyID, int UserID, int moduleID, int sessionId, SqlConnection conn, SqlTransaction tran) {
            SqlParameter[] sp = new SqlParameter[4];
            sp[0] = new SqlParameter("@cid", CompanyID);
            sp[1] = new SqlParameter("@eid", UserID);
            sp[2] = new SqlParameter("@mid", moduleID);
            sp[3] = new SqlParameter("@sid", sessionId);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sc_angularSession2DB";
            cmd.Parameters.AddRange(sp); cmd.Connection = conn; cmd.Transaction = tran;
            return cmd.ExecuteNonQuery();
        }
        //private void angularSession4DB(int CompanyID, int UserID, int sessionId, SqlConnection conn, SqlTransaction tran) {
        //    SqlParameter[] sp = new SqlParameter[3];
        //    sp[0] = new SqlParameter("@cid", CompanyID);
        //    sp[1] = new SqlParameter("@eid", UserID);
        //    sp[2] = new SqlParameter("@sid", sessionId);
        //    SqlCommand cmd = new SqlCommand();
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.CommandText = "sc_angularSession4DB";
        //    cmd.Parameters.AddRange(sp);
        //    cmd.Connection = conn; cmd.Transaction = tran;
        //    using (SqlDataReader dr = cmd.ExecuteReader()) {
        //        while (dr.Read()) { //lid, cid, eid, mid
        //            Module = dr["ModuleId"].ToString();
        //            Login = dr["LoginId"].ToString();
        //        }
        //    }
        //}
    }
    public class NavMenu {
        public int id { get; set; }
        public string title { get; set; }
        public string code { get; set; }
        public string type { get; set; }
        public string icon { get; set; }
        public int? parent { get; set; }
        public int? order { get; set; }
        public string url { get; set; }
        public string function {
            get {
                if (!string.IsNullOrEmpty(url))
                    return "() => {this.iframeRedirect('" + url + "');}";
                else
                    return "";
            }
        }
        public List<NavMenu> children { get; set; }
    }
    public class UserRole {
        public int      ModuleID    { get; set; }
        public string   ModuleName { get; set; }
        public string   ModuleCode { get; set; }
    }
}
