﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Login.Controllers.MMS {
    public class persIssuesDAL {
        public static async Task<Tuple<int, List<tbPersonalIssues>>> ReadTaskBasedPersonalIssuesAsync(
            int pid, int? tid, int cp, int ps) {
            List<tbPersonalIssues> clTBPersonalIssues = new List<tbPersonalIssues>();
            int totalRecords = 0;
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "mms.TaskBasedPersonalIssuesRead";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pid", pid));
                    cmd.Parameters.Add(new SqlParameter("@tid", (object)tid ?? DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@cp", cp));
                    cmd.Parameters.Add(new SqlParameter("@ps", ps));
                    cmd.Parameters.Add(new SqlParameter("@tr", SqlDbType.Int)).Direction = ParameterDirection.Output;
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {

                            tbPersonalIssues t = new tbPersonalIssues();
                            t.TBPI = rdr["TBPI"] == DBNull.Value ? -1 : (int)rdr["TBPI"];
                            //t.CompanyID = rdr["CompanyID"] == DBNull.Value ? -1 : (int)rdr["CompanyID"];
                            //t.ProjectID = rdr["ProjectID"] == DBNull.Value ? -1 : (int)rdr["ProjectID"];
                            t.TaskID = rdr["TaskID"] == DBNull.Value ? -1 : (int)rdr["TaskID"];
                            t.SubTaskID = rdr["SubTaskID"] == DBNull.Value ? -1 : (int)rdr["SubTaskID"];
                            t.ResourceID = rdr["ResourceID"] == DBNull.Value ? -1 : (int)rdr["ResourceID"];
                            t.RevrEmpID = rdr["RevrEmpID"] == DBNull.Value ? -1 : (int)rdr["RevrEmpID"];
                            t.RQty = rdr["RQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["RQty"]);
                            t.IQty = rdr["IQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["IQty"]);
                            t.BQty = rdr["BQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["BQty"]);
                            t.PlanDate = rdr["PlanDate"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["PlanDate"]);
                            //t.ReqIndentID = rdr["ReqIndentID"] == DBNull.Value ? -1 : (int)rdr["ReqIndentID"];
                            t.ModeID = rdr["ModeID"] == DBNull.Value ? -1 : (int)rdr["ModeID"];
                            //t.TBIResTransID = rdr["TBIResTransID"] == DBNull.Value ? -1 : (int)rdr["TBIResTransID"];
                            //t.CreatedOn = rdr["CreatedOn"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["CreatedOn"]);
                            //t.CreatedBy = rdr["CreatedBy"] == DBNull.Value ? -1 : (int)rdr["CreatedBy"];
                            //t.IsActive = rdr["IsActive"] == DBNull.Value ? -1 : (int)rdr["IsActive"];
                            t.MatorTool = rdr["MatorTool"] == DBNull.Value ? -1 : (int)rdr["MatorTool"];
                            t.SubActivityID = rdr["SubActivityID"] == DBNull.Value ? -1 : (int)rdr["SubActivityID"];
                            t.subresourceid = rdr["subresourceid"] == DBNull.Value ? -1 : (int)rdr["subresourceid"];
                            t.GdnItemID = rdr["GdnItemID"] == DBNull.Value ? -1 : (int)rdr["GdnItemID"];
                            //t.PissueTaskAssignMatId = rdr["PissueTaskAssignMatId"] == DBNull.Value ? -1 : (int)rdr["PissueTaskAssignMatId"];
                            //t.ToolRetDate = rdr["ToolRetDate"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["ToolRetDate"]);
                            //t.SmsIsReqId = rdr["SmsIsReqId"] == DBNull.Value ? -1 : (int)rdr["SmsIsReqId"];
                            clTBPersonalIssues.Add(t);
                        }
                    }
                    totalRecords = (int)cmd.Parameters["@tr"].Value;
                }
                con.Close();
                return new Tuple<int, List<tbPersonalIssues>>(totalRecords, clTBPersonalIssues);

            }
        }
        public static async Task<string> CreateTBIssuesAsync(twoArrays resStIDs, int wid, int pid, int userID, int rcvr, DateTime dtSt, DateTime dtEd) {
             List<int> rids = resStIDs.array1;
             List<int> stIDs = resStIDs.array2;
            DataTable dtRids = common.idList2Table(rids);
            DataTable dtStids = common.idList2Table(stIDs);
            int recordsUpdated = 0;
            string result = string.Empty;
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("createTBPIs"));
                string queryString = "mms.matIssueSTBased";
                using(SqlCommand cmd = new SqlCommand(queryString, con, tran)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@wid", wid));
                    cmd.Parameters.Add(new SqlParameter("@pid", pid));
                    cmd.Parameters.Add(new SqlParameter("@userID", userID));
                    cmd.Parameters.Add(new SqlParameter("@rcvr", rcvr));
                    cmd.Parameters.Add(new SqlParameter("@rids", dtRids));
                    cmd.Parameters.Add(new SqlParameter("@stIDs", dtStids));
                    cmd.Parameters.Add(new SqlParameter("@dtSt", dtSt));
                    cmd.Parameters.Add(new SqlParameter("@dtEd", dtEd));
                    cmd.Parameters.Add(new SqlParameter("@result", SqlDbType.VarChar, 200)).Direction = ParameterDirection.Output;
                    recordsUpdated = await cmd.ExecuteNonQueryAsync();
                    result = cmd.Parameters["@result"].Value.ToString();
                }
                if(result.Contains("success")) {
                    tran.Commit(); 
                    con.Close();
                    return "Personal issue transaction(s) are successful!";
                }
                else return "failed to update!";
            }
        }
         
    }
    public class tbPersonalIssues { // [TaskBasedPersonalIssuesType]

        public int TBPI { get; set; }
        public int TaskID { get; set; }
        public int SubTaskID { get; set; }
        public int ResourceID { get; set; }
        public int RevrEmpID { get; set; }
        public decimal RQty { get; set; }
        public decimal IQty { get; set; }
        public decimal BQty { get; set; }
        public DateTime PlanDate { get; set; }
        public int ModeID { get; set; }
        public int MatorTool { get; set; }
        public int SubActivityID { get; set; }
        public int subresourceid { get; set; }
        public int GdnItemID { get; set; }

        public tbPersonalIssues() { }
        public tbPersonalIssues(int _TBPI, int _TaskID, int _SubTaskID,
            int _ResourceID, int _RevrEmpID, decimal _RQty, decimal _IQty, decimal _BQty, DateTime _PlanDate,
            int _ModeID, int _MatorTool, int _SubActivityID, int _subresourceid, int _GdnItemID) {
            TBPI = _TBPI; TaskID = _TaskID; SubTaskID = _SubTaskID;            ResourceID = _ResourceID; RevrEmpID = _RevrEmpID; RQty = _RQty; IQty = _IQty; BQty = _BQty;            PlanDate = _PlanDate; ModeID = _ModeID; MatorTool = _MatorTool; SubActivityID = _SubActivityID;
            subresourceid = _subresourceid; GdnItemID = _GdnItemID;
            //PissueTaskAssignMatId = _PissueTaskAssignMatId; ToolRetDate = _ToolRetDate;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("TBPI", typeof(System.Int32));
            dtName.Columns.Add("TaskID", typeof(System.Int32));
            dtName.Columns.Add("SubTaskID", typeof(System.Int32));
            dtName.Columns.Add("ResourceID", typeof(System.Int32));
            dtName.Columns.Add("RevrEmpID", typeof(System.Int32));
            dtName.Columns.Add("RQty", typeof(System.Double));
            dtName.Columns.Add("IQty", typeof(System.Double));
            dtName.Columns.Add("BQty", typeof(System.Double));
            dtName.Columns.Add("PlanDate", typeof(System.DateTime));
            dtName.Columns.Add("ModeID", typeof(System.Int32));
            dtName.Columns.Add("MatorTool", typeof(System.Int32));
            dtName.Columns.Add("SubActivityID", typeof(System.Int32));
            dtName.Columns.Add("subresourceid", typeof(System.Int32));
            dtName.Columns.Add("GdnItemID", typeof(System.Int32));
            return dtName;
        }
    }
}
