﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Login.Controllers.MMS {
    public class MmsMastersController :ApiController {
        [HttpGet]// http://localhost/Login/api/MmsMasters/MaterialReturnType
        [Route("api/MmsMasters/MaterialReturnType")]
        public async Task<HttpResponseMessage> MaterialReturnType() {
            try {
                List<T_G_MaterialReturnType> clMaterialReturnType = await Task.Run(() =>
                mmsMasterDAL.ReadMaterialReturnTypeAsync());
                return Request.CreateResponse(HttpStatusCode.OK, clMaterialReturnType);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]// http://localhost/Login/api/MmsMasters/IssueMode
        [Route("api/MmsMasters/IssueMode")]
        public async Task<HttpResponseMessage> IssueMode() {
            try {     
                List<T_MMS_IssueMode> clIssueMode = await Task.Run(() => mmsMasterDAL.ReadIssueModeAsync());
                return Request.CreateResponse(HttpStatusCode.OK, clIssueMode);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet] // http://localhost/Login/api/MmsMasters/TransType
        [Route("api/MmsMasters/TransType")]
        public async Task<HttpResponseMessage> TransType() {
            try {
                List<TransType> clTransType = await Task.Run(() => mmsMasterDAL.ReadTransTypeAsync());
                return Request.CreateResponse(HttpStatusCode.OK, clTransType);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }

}


