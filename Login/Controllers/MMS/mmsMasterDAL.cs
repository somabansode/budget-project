﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Login.Controllers.MMS {
    public class mmsMasterDAL {
        public static async Task<List<T_G_MaterialReturnType>> ReadMaterialReturnTypeAsync() {
            List<T_G_MaterialReturnType> clMaterialReturnType = new List<T_G_MaterialReturnType>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "select RetID,ReturnMode from T_G_MaterialReturnType order by RetID";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            T_G_MaterialReturnType t = new T_G_MaterialReturnType();
                            t.RetID = rdr["RetID"] == DBNull.Value ? -1 : (int)rdr["RetID"];
                            t.ReturnMode = rdr["ReturnMode"] == DBNull.Value ? "" : rdr["ReturnMode"].ToString();
                            //t.IsActive = rdr["IsActive"] == DBNull.Value ? -1 : (int)rdr["IsActive"];
                            clMaterialReturnType.Add(t);
                        }
                    }
                }
                con.Close();
                return clMaterialReturnType;
            }
        }
        public static async Task<List<T_MMS_IssueMode>> ReadIssueModeAsync() {
            List<T_MMS_IssueMode> clT_MMS_IssueMode = new List<T_MMS_IssueMode>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "select ModeId,Mode from T_MMS_IssueMode order by ModeId";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            T_MMS_IssueMode t = new T_MMS_IssueMode();
                            t.ModeId = rdr["ModeId"] == DBNull.Value ? -1 : (int)rdr["ModeId"];
                            t.Mode = rdr["Mode"] == DBNull.Value ? "" : rdr["Mode"].ToString();
                            //t.IsActive = rdr["IsActive"] == DBNull.Value ? -1 : (int)rdr["IsActive"];
                            clT_MMS_IssueMode.Add(t);
                        }
                    }
                }
                con.Close();
                return clT_MMS_IssueMode;
            }
        }
        public static async Task<List<TransType>> ReadTransTypeAsync() {
            List<TransType> clTransType = new List<TransType>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "select id, purpose from mms.TransType";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            TransType t = new TransType();
                            t.id = rdr["id"] == DBNull.Value ? "" : rdr["id"].ToString();
                            t.purpose = rdr["purpose"] == DBNull.Value ? "" : rdr["purpose"].ToString();
                            clTransType.Add(t);
                        }
                    }
                }
                con.Close();
                return clTransType;
            }
        }
    }
    public class T_G_MaterialReturnType {
        public int RetID { get; set; }
        public string ReturnMode { get; set; }//public int IsActive { get; set; }
        public T_G_MaterialReturnType() { }
        public T_G_MaterialReturnType(int _RetID, string _ReturnMode) {//, int _IsActive
            RetID = _RetID; ReturnMode = _ReturnMode; //IsActive = _IsActive;
        }
    }
    public class T_MMS_IssueMode {
        public int ModeId { get; set; }
        public string Mode { get; set; }
        //public int IsActive { get; set; }

        public T_MMS_IssueMode() { }
        public T_MMS_IssueMode(int _ModeId, string _Mode) {//, int _IsActive
            ModeId = _ModeId; Mode = _Mode;// IsActive = _IsActive;
        }
    }

    public class TransType {
        public string id { get; set; }
        public string purpose { get; set; }
        public TransType() { }
        public TransType(string _id, string _purpose) {
            id = _id; purpose = _purpose;
        }
         
    }
}
 