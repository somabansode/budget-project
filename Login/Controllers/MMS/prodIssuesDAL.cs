﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Login.Controllers.MMS {
    public class prodIssuesDAL {
        public static async Task<List<materialsForIssueRes>> Resources(int pid, DateTime dtFrom, DateTime dtTo, int? rid) {
            List<materialsForIssueRes> clmaterialsForIssueRes = new List<materialsForIssueRes>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "oms.materialsForIssueRes";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pid", pid));
                    cmd.Parameters.Add(new SqlParameter("@dtFrom", dtFrom));
                    cmd.Parameters.Add(new SqlParameter("@dtTo", dtTo));
                    cmd.Parameters.Add(new SqlParameter("@rid", (object)rid ?? DBNull.Value));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            materialsForIssueRes t = new materialsForIssueRes();
                            t.rid = rdr["rid"] == DBNull.Value ? -1 : (int)rdr["rid"];
                            t.ResourceName = rdr["ResourceName"] == DBNull.Value ? "" : rdr["ResourceName"].ToString();
                            t.pUoM = rdr["pUoM"] == DBNull.Value ? -1 : (int)rdr["pUoM"];
                            t.poNos = rdr["poNos"] == DBNull.Value ? -1 : (int)rdr["poNos"];
                            t.gDNs = rdr["gDNs"] == DBNull.Value ? -1 : (int)rdr["gDNs"];
                            t.ReqQty = rdr["ReqQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["ReqQty"]);
                            t.AsgQty = rdr["AsgQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["AsgQty"]);
                            t.Issued = rdr["Issued"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["Issued"]);
                            t.Stock = rdr["Stock"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["Stock"]);
                            clmaterialsForIssueRes.Add(t);
                        }
                    }
                }
                con.Close();
                return clmaterialsForIssueRes;
            }
        }
        public static async Task<List<materialsForIssueTASKs>> TASKs(int pid, DateTime dtFrom, DateTime dtTo, List<int> rids) {
            List<materialsForIssueTASKs> clmaterialsForIssueTASKs = new List<materialsForIssueTASKs>();
            DataTable dtr = common.idList2Table(rids);
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "oms.materialsForIssueTASKs";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pid", pid));
                    cmd.Parameters.Add(new SqlParameter("@dtFrom", dtFrom));
                    cmd.Parameters.Add(new SqlParameter("@dtTo", dtTo));
                    cmd.Parameters.Add(new SqlParameter("@rids", dtr));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            materialsForIssueTASKs t = new materialsForIssueTASKs();
                            t.TaskID = rdr["TaskID"] == DBNull.Value ? -1 : (int)rdr["TaskID"];
                            t.TaskName = rdr["TaskName"] == DBNull.Value ? "" : rdr["TaskName"].ToString();
                            t.stNos = rdr["stNos"] == DBNull.Value ? -1 : (int)rdr["stNos"];
                            t.tUoM = rdr["tUoM"] == DBNull.Value ? -1 : (int)rdr["tUoM"];
                            t.stQty = rdr["stQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["stQty"]);
                            t.RID = rdr["RID"] == DBNull.Value ? -1 : (int)rdr["RID"];
                            t.ResourceName = rdr["ResourceName"] == DBNull.Value ? "" : rdr["ResourceName"].ToString();
                            t.sUom = rdr["sUom"] == DBNull.Value ? -1 : (int)rdr["sUom"];
                            t.reqQty = rdr["reqQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["reqQty"]);
                            t.asgQty = rdr["asgQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["asgQty"]);
                            clmaterialsForIssueTASKs.Add(t);
                        }
                    }
                }
                con.Close();
                return clmaterialsForIssueTASKs;
            }
        }
        public static async Task<List<materialsForIssueSTs>> STs(int pid, DateTime dtFrom, DateTime dtTo, twoArrays TRids) {
            List<materialsForIssueSTs> clmaterialsForIssueSTs = new List<materialsForIssueSTs>();
            DataTable dtt = common.idList2Table(TRids.array1);
            DataTable dtr = common.idList2Table(TRids.array2);
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "oms.materialsForIssueSTs";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pid", pid));
                    cmd.Parameters.Add(new SqlParameter("@dtFrom", dtFrom));
                    cmd.Parameters.Add(new SqlParameter("@dtTo", dtTo));
                    cmd.Parameters.Add(new SqlParameter("@tids", dtt));
                    cmd.Parameters.Add(new SqlParameter("@rids", dtr));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            materialsForIssueSTs t = new materialsForIssueSTs();
                            t.stID = rdr["stID"] == DBNull.Value ? -1 : (int)rdr["stID"];
                            t.SubtaskName = rdr["SubtaskName"] == DBNull.Value ? "" : rdr["SubtaskName"].ToString();
                            t.TaskID = rdr["TaskID"] == DBNull.Value ? -1 : (int)rdr["TaskID"];
                            t.tUoM = rdr["tUoM"] == DBNull.Value ? -1 : (int)rdr["tUoM"];
                            t.stQty = rdr["stQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["stQty"]);
                            t.RID = rdr["RID"] == DBNull.Value ? -1 : (int)rdr["RID"];
                            t.ResourceName = rdr["ResourceName"] == DBNull.Value ? "" : rdr["ResourceName"].ToString();
                            t.sUom = rdr["sUom"] == DBNull.Value ? -1 : (int)rdr["sUom"];
                            t.reqQty = rdr["reqQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["reqQty"]);
                            t.asgQty = rdr["asgQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["asgQty"]);
                            clmaterialsForIssueSTs.Add(t);
                        }
                    }
                }
                con.Close();
                return clmaterialsForIssueSTs;
            }
        }
        public static async Task<List<materialsForIssuePop>> GdnPopUp(int pid, DateTime dtFrom, DateTime dtTo, int rid) {
            List<materialsForIssuePop> clmaterialsForIssuePop = new List<materialsForIssuePop>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "oms.materialsForIssuePop";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pid", pid));
                    cmd.Parameters.Add(new SqlParameter("@dtFrom", dtFrom));
                    cmd.Parameters.Add(new SqlParameter("@dtTo", dtTo));
                    cmd.Parameters.Add(new SqlParameter("@rid", rid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            materialsForIssuePop t = new materialsForIssuePop();
                            t.rid = rdr["rid"] == DBNull.Value ? -1 : (int)rdr["rid"];
                            t.ResourceName = rdr["ResourceName"] == DBNull.Value ? "" : rdr["ResourceName"].ToString();
                            t.pUoM = rdr["pUoM"] == DBNull.Value ? -1 : (int)rdr["pUoM"];
                            t.POID = rdr["POID"] == DBNull.Value ? -1 : (int)rdr["POID"];
                            t.PDID = rdr["PDID"] == DBNull.Value ? -1 : (int)rdr["PDID"];
                            t.poQty = rdr["poQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["poQty"]);
                            t.gdnID = rdr["gdnID"] == DBNull.Value ? -1 : (int)rdr["gdnID"];
                            t.gdnQty = rdr["gdnQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["gdnQty"]);
                            clmaterialsForIssuePop.Add(t);
                        }
                    }
                }
                con.Close();
                return clmaterialsForIssuePop;
            }
        }
        public static async Task<List<materialsForIssueRSPlans>>
                RSPlans(int pid, DateTime dtFrom, DateTime dtTo, int stid, int rid) {
            List<materialsForIssueRSPlans> clmaterialsForIssueRSPlans = new List<materialsForIssueRSPlans>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "oms.materialsForIssueRSPlans";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pid", pid));
                    cmd.Parameters.Add(new SqlParameter("@dtFrom", dtFrom));
                    cmd.Parameters.Add(new SqlParameter("@dtTo", dtTo));
                    cmd.Parameters.Add(new SqlParameter("@stid", stid));
                    cmd.Parameters.Add(new SqlParameter("@rid", rid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            materialsForIssueRSPlans t = new materialsForIssueRSPlans();
                            t.stID = rdr["stID"] == DBNull.Value ? -1 : (int)rdr["stID"];
                            t.tUoM = rdr["tUoM"] == DBNull.Value ? -1 : (int)rdr["tUoM"];
                            t.stQty = rdr["stQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["stQty"]);
                            t.rid = rdr["rid"] == DBNull.Value ? -1 : (int)rdr["rid"];
                            t.ResourceName = rdr["ResourceName"] == DBNull.Value ? "" : rdr["ResourceName"].ToString();
                            t.pUoM = rdr["pUoM"] == DBNull.Value ? -1 : (int)rdr["pUoM"];
                            t.Date = rdr["Date"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["Date"]);
                            t.ReqQty = rdr["ReqQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["ReqQty"]);
                            t.AsgQty = rdr["AsgQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["AsgQty"]);
                            t.Issued = rdr["Issued"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["Issued"]);
                            t.PDID = rdr["PDID"] == DBNull.Value ? -1 : (int)rdr["PDID"];
                            t.poQty = rdr["poQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["poQty"]);
                            t.gdnID = rdr["gdnID"] == DBNull.Value ? -1 : (int)rdr["gdnID"];
                            t.gdnQty = rdr["gdnQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["gdnQty"]);
                            clmaterialsForIssueRSPlans.Add(t);
                        }
                    }
                }
                con.Close();
                return clmaterialsForIssueRSPlans;
            }
        }
        public static async Task<List<productionIssues>> ReadProductionIssuesAsync(int pid, int rid, DateTime dtFr, DateTime dtTo) {
            List<productionIssues> clproductionIssues = new List<productionIssues>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "oms.productionIssues";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pid", pid));
                    cmd.Parameters.Add(new SqlParameter("@rid", rid));
                    cmd.Parameters.Add(new SqlParameter("@dtFr", dtFr));
                    cmd.Parameters.Add(new SqlParameter("@dtTo", dtTo));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            productionIssues t = new productionIssues();
                            t.id = rdr["id"] == DBNull.Value ? -1 : (int)rdr["id"];
                            t.plID = rdr["plID"] == DBNull.Value ? -1 : (int)rdr["plID"];
                            t.rbid = rdr["rbid"] == DBNull.Value ? -1 : (int)rdr["rbid"];
                            t.rID = rdr["rID"] == DBNull.Value ? -1 : (int)rdr["rID"];
                            t.Res = rdr["Res"] == DBNull.Value ? "" : rdr["Res"].ToString();
                            t.uom = rdr["uom"] == DBNull.Value ? -1 : (int)rdr["uom"];
                            t.rUnit = rdr["rUnit"] == DBNull.Value ? "" : rdr["rUnit"].ToString();
                            t.pcID = rdr["pcID"] == DBNull.Value ? -1 : (int)rdr["pcID"];
                            t.rdQty = rdr["rdQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["rdQty"]);
                            t.stg = rdr["stg"] == DBNull.Value ? -1 : (int)rdr["stg"];
                            t.stid = rdr["stid"] == DBNull.Value ? -1 : (int)rdr["stid"];
                            t.date = rdr["date"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["date"]);
                            t.asgQty = rdr["asgQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["asgQty"]);
                            t.cumIQty = rdr["cumIQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["cumIQty"]);
                            t.exQty = rdr["exQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["exQty"]);
                            t.TBPI = rdr["TBPI"] == DBNull.Value ? -1 : (int)rdr["TBPI"];
                            t.IssID = rdr["IssID"] == DBNull.Value ? -1 : (int)rdr["IssID"];
                            t.SJID = rdr["SJID"] == DBNull.Value ? -1 : (int)rdr["SJID"];
                            t.AsgID = rdr["AsgID"] == DBNull.Value ? -1 : (int)rdr["AsgID"];
                            clproductionIssues.Add(t);
                        }
                    }
                }
                con.Close();
                return clproductionIssues;
            }
        }
        public static async Task<List<intString>> ReadResListAsync(int pid) {
            List<intString> clresList = new List<intString>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "oms.resList";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pid", pid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            intString t = new intString();
                            t.id = rdr["rID"] == DBNull.Value ? -1 : (int)rdr["rID"];
                            t.str = rdr["Res"] == DBNull.Value ? "" : rdr["Res"].ToString();
                            clresList.Add(t);
                        }
                    }
                }
                con.Close();
                return clresList;
            }
        }
         public static async Task<string> deleteMatTaskBasedIssuesAsync(int cid, int pid,int resID,int stid, DateTime dtFr, DateTime dtTo) {
            string result = string.Empty;
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "oms.deleteMatTaskBasedIssues";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    cmd.Parameters.Add(new SqlParameter("@pid", pid));
                    cmd.Parameters.Add(new SqlParameter("@resID", resID));
                    cmd.Parameters.Add(new SqlParameter("@stid", stid));
                    cmd.Parameters.Add(new SqlParameter("@dtFr", dtFr));
                    cmd.Parameters.Add(new SqlParameter("@dtTo", dtTo));
                    cmd.Parameters.Add(new SqlParameter("@result", SqlDbType.VarChar, 200)).Direction = ParameterDirection.Output;
                    await cmd.ExecuteNonQueryAsync();
                    result = cmd.Parameters["@result"].Value.ToString();
                }
                con.Close();
                return result;
            }
         }
         
    }
    public class materialsForIssueRes {
        public int rid { get; set; }
        public string ResourceName { get; set; }
        public int pUoM { get; set; }
        public int poNos { get; set; }
        public int gDNs { get; set; }
        public decimal ReqQty { get; set; }
        public decimal AsgQty { get; set; }
        public decimal Issued { get; set; }
        public decimal Stock { get; set; }

        public materialsForIssueRes() { }
        public materialsForIssueRes(int _rid, string _ResourceName, int _pUoM, int _poNos, int _gDNs, decimal _ReqQty
            , decimal _AsgQty, decimal _Issued, decimal _Stock) {
            rid = _rid; ResourceName = _ResourceName; pUoM = _pUoM; poNos = _poNos; gDNs = _gDNs; ReqQty = _ReqQty;
            AsgQty = _AsgQty; Issued = _Issued; Stock = _Stock;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("rid", typeof(System.Int32));
            dtName.Columns.Add("ResourceName", typeof(System.String));
            dtName.Columns.Add("pUoM", typeof(System.Int32));
            dtName.Columns.Add("poNos", typeof(System.Int32));
            dtName.Columns.Add("gDNs", typeof(System.Int32));
            dtName.Columns.Add("ReqQty", typeof(System.Double));
            dtName.Columns.Add("AsgQty", typeof(System.Double));
            dtName.Columns.Add("Issued", typeof(System.Double));
            dtName.Columns.Add("Stock", typeof(System.Double));
            return dtName;
        }
    }
    public class materialsForIssueSTs {
        public int stID { get; set; }
        public string SubtaskName { get; set; }
        public int TaskID { get; set; }
        public int tUoM { get; set; }
        public decimal stQty { get; set; }
        public int RID { get; set; }
        public string ResourceName { get; set; }
        public int sUom { get; set; }
        public decimal reqQty { get; set; }
        public decimal asgQty { get; set; }

        public materialsForIssueSTs() { }
        public materialsForIssueSTs(int _stID, string _SubtaskName, int _TaskID, int _tUoM, decimal _stQty
            , int _RID, string _ResourceName, int _sUom, decimal _reqQty, decimal _asgQty) {
            stID = _stID; SubtaskName = _SubtaskName; TaskID = _TaskID; tUoM = _tUoM; stQty = _stQty;
            RID = _RID; ResourceName = _ResourceName; sUom = _sUom; reqQty = _reqQty; asgQty = _asgQty;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("stID", typeof(System.Int32));
            dtName.Columns.Add("SubtaskName", typeof(System.String));
            dtName.Columns.Add("TaskID", typeof(System.Int32));
            dtName.Columns.Add("tUoM", typeof(System.Int32));
            dtName.Columns.Add("stQty", typeof(System.Double));
            dtName.Columns.Add("RID", typeof(System.Int32));
            dtName.Columns.Add("ResourceName", typeof(System.String));
            dtName.Columns.Add("sUom", typeof(System.Int32));
            dtName.Columns.Add("reqQty", typeof(System.Double));
            dtName.Columns.Add("asgQty", typeof(System.Double));
            return dtName;
        }
    }
    public class materialsForIssueTASKs {
        public int TaskID { get; set; }
        public string TaskName { get; set; }
        public int stNos { get; set; }
        public int tUoM { get; set; }
        public decimal stQty { get; set; }
        public int RID { get; set; }
        public string ResourceName { get; set; }
        public int sUom { get; set; }
        public decimal reqQty { get; set; }
        public decimal asgQty { get; set; }

        public materialsForIssueTASKs() { }
        public materialsForIssueTASKs(int _TaskID, string _TaskName, int _stNos, int _tUoM, decimal _stQty
            , int _RID, string _ResourceName, int _sUom, decimal _reqQty, decimal _asgQty) {
            TaskID = _TaskID; TaskName = _TaskName; stNos = _stNos; tUoM = _tUoM; stQty = _stQty;
            RID = _RID; ResourceName = _ResourceName; sUom = _sUom; reqQty = _reqQty; asgQty = _asgQty;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("TaskID", typeof(System.Int32));
            dtName.Columns.Add("TaskName", typeof(System.String));
            dtName.Columns.Add("stNos", typeof(System.Int32));
            dtName.Columns.Add("tUoM", typeof(System.Int32));
            dtName.Columns.Add("stQty", typeof(System.Double));
            dtName.Columns.Add("RID", typeof(System.Int32));
            dtName.Columns.Add("ResourceName", typeof(System.String));
            dtName.Columns.Add("sUom", typeof(System.Int32));
            dtName.Columns.Add("reqQty", typeof(System.Double));
            dtName.Columns.Add("asgQty", typeof(System.Double));
            return dtName;
        }
    }
    public class materialsForIssuePop {
        public int rid { get; set; }
        public string ResourceName { get; set; }
        public int pUoM { get; set; }
        public int POID { get; set; }
        public int PDID { get; set; }
        public decimal poQty { get; set; }
        public int gdnID { get; set; }
        public decimal gdnQty { get; set; }

        public materialsForIssuePop() { }
        public materialsForIssuePop(int _rid, string _ResourceName, int _pUoM, int _POID, int _PDID
            , decimal _poQty, int _gdnID, decimal _gdnQty) {
            rid = _rid; ResourceName = _ResourceName; pUoM = _pUoM; POID = _POID; PDID = _PDID; poQty = _poQty;
            gdnID = _gdnID; gdnQty = _gdnQty;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("rid", typeof(System.Int32));
            dtName.Columns.Add("ResourceName", typeof(System.String));
            dtName.Columns.Add("pUoM", typeof(System.Int32));
            dtName.Columns.Add("POID", typeof(System.Int32));
            dtName.Columns.Add("PDID", typeof(System.Int32));
            dtName.Columns.Add("poQty", typeof(System.Double));
            dtName.Columns.Add("gdnID", typeof(System.Int32));
            dtName.Columns.Add("gdnQty", typeof(System.Double));
            return dtName;
        }
    }
    public class materialsForIssueRSPlans {
        public int stID { get; set; }
        public int tUoM { get; set; }
        public decimal stQty { get; set; }
        public int rid { get; set; }
        public string ResourceName { get; set; }
        public int pUoM { get; set; }
        public DateTime Date { get; set; }
        public decimal ReqQty { get; set; }
        public decimal AsgQty { get; set; }
        public decimal Issued { get; set; }
        public int PDID { get; set; }
        public decimal poQty { get; set; }
        public int gdnID { get; set; }
        public decimal gdnQty { get; set; }

        public materialsForIssueRSPlans() { }
        public materialsForIssueRSPlans(int _stID, int _tUoM, decimal _stQty, int _rid, string _ResourceName
            , int _pUoM, DateTime _Date, decimal _ReqQty, decimal _AsgQty, decimal _Issued, int _PDID
            , decimal _poQty, int _gdnID, decimal _gdnQty) {
            stID = _stID; tUoM = _tUoM; stQty = _stQty; rid = _rid; ResourceName = _ResourceName;
            pUoM = _pUoM; Date = _Date; ReqQty = _ReqQty; AsgQty = _AsgQty; Issued = _Issued; PDID = _PDID;
            poQty = _poQty; gdnID = _gdnID; gdnQty = _gdnQty;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("stID", typeof(System.Int32));
            dtName.Columns.Add("tUoM", typeof(System.Int32));
            dtName.Columns.Add("stQty", typeof(System.Double));
            dtName.Columns.Add("rid", typeof(System.Int32));
            dtName.Columns.Add("ResourceName", typeof(System.String));
            dtName.Columns.Add("pUoM", typeof(System.Int32));
            dtName.Columns.Add("Date", typeof(System.DateTime));
            dtName.Columns.Add("ReqQty", typeof(System.Double));
            dtName.Columns.Add("AsgQty", typeof(System.Double));
            dtName.Columns.Add("Issued", typeof(System.Double));
            dtName.Columns.Add("PDID", typeof(System.Int32));
            dtName.Columns.Add("poQty", typeof(System.Double));
            dtName.Columns.Add("gdnID", typeof(System.Int32));
            dtName.Columns.Add("gdnQty", typeof(System.Double));
            return dtName;
        }
    }


    public class productionIssues {
        public int id { get; set; }
        public int plID { get; set; }
        public int rbid { get; set; }
        public int rID { get; set; }
        public string Res { get; set; }
        public int uom { get; set; }
        public string rUnit { get; set; }
        public int pcID { get; set; }
        public decimal rdQty { get; set; }
        public int stg { get; set; }
        public int stid { get; set; }
        public DateTime date { get; set; }
        public decimal asgQty { get; set; }
        public decimal cumIQty { get; set; }
        public decimal exQty { get; set; }
        public int TBPI { get; set; }
        public int IssID { get; set; }
        public int SJID { get; set; }
        public int AsgID { get; set; }

        public productionIssues() { }
        public productionIssues(int _id, int _plID, int _rbid, int _rID, string _Res, int _uom, string _rUnit
            , int _pcID, decimal _rdQty, int _stg, int _stid, DateTime _date, decimal _asgQty, decimal _cumIQty
            , decimal _exQty, int _TBPI, int _IssID, int _SJID, int _AsgID) {
            id = _id; plID = _plID; rbid = _rbid; rID = _rID; Res = _Res; uom = _uom; rUnit = _rUnit; pcID = _pcID; rdQty = _rdQty; stg = _stg; stid = _stid; date = _date; asgQty = _asgQty; cumIQty = _cumIQty; exQty = _exQty; TBPI = _TBPI; IssID = _IssID; SJID = _SJID; AsgID = _AsgID;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("id", typeof(System.Int32));
            dtName.Columns.Add("plID", typeof(System.Int32));
            dtName.Columns.Add("rbid", typeof(System.Int32));
            dtName.Columns.Add("rID", typeof(System.Int32));
            dtName.Columns.Add("Res", typeof(System.String));
            dtName.Columns.Add("uom", typeof(System.Int32));
            dtName.Columns.Add("rUnit", typeof(System.String));
            dtName.Columns.Add("pcID", typeof(System.Int32));
            dtName.Columns.Add("rdQty", typeof(System.Double));
            dtName.Columns.Add("stg", typeof(System.Int32));
            dtName.Columns.Add("stid", typeof(System.Int32));
            dtName.Columns.Add("date", typeof(System.DateTime));
            dtName.Columns.Add("asgQty", typeof(System.Double));
            dtName.Columns.Add("cumIQty", typeof(System.Double));
            dtName.Columns.Add("exQty", typeof(System.Double));
            dtName.Columns.Add("TBPI", typeof(System.Int32));
            dtName.Columns.Add("IssID", typeof(System.Int32));
            dtName.Columns.Add("SJID", typeof(System.Int32));
            dtName.Columns.Add("AsgID", typeof(System.Int32));
            return dtName;
        }
    }
}

