﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Login.Controllers.MMS {
    public class stockDAL {
        public static async Task<Tuple<int, List<t_mms_stockJournal>>> ReadtStockJournalAsync(int cid, int? pid, int? wid, int userID, int cp, int ps) {
            List<t_mms_stockJournal> clstockJournal = new List<t_mms_stockJournal>();
            int totalRecords = 0;
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "mms.stockJournalRead";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    cmd.Parameters.Add(new SqlParameter("@pid", (object)pid?? DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@wid", (object)wid?? DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@userID", userID));
                    cmd.Parameters.Add(new SqlParameter("@cp", cp));
                    cmd.Parameters.Add(new SqlParameter("@ps", ps));
                    cmd.Parameters.Add(new SqlParameter("@tr", SqlDbType.Int)).Direction = ParameterDirection.Output;
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            t_mms_stockJournal t = new t_mms_stockJournal();
                            t.ResTransID = rdr["ResTransID"] == DBNull.Value ? -1 : (int)rdr["ResTransID"];
                            t.PGRNItemID = rdr["PGRNItemID"] == DBNull.Value ? -1 : (int)rdr["PGRNItemID"];
                            t.ResourceID = rdr["ResourceID"] == DBNull.Value ? -1 : (int)rdr["ResourceID"];
                            t.IssueDetID = rdr["IssueDetID"] == DBNull.Value ? -1 : (int)rdr["IssueDetID"];
                            t.PGDNDate = rdr["PGDNDate"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["PGDNDate"]);
                            t.IssueDate = rdr["IssueDate"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["IssueDate"]);
                            t.RcvdQty = rdr["RcvdQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["RcvdQty"]);
                            t.IssuedQty = rdr["IssuedQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["IssuedQty"]);
                            t.ClosingBalance = rdr["ClosingBalance"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["ClosingBalance"]);
                            t.UOM = rdr["UOM"] == DBNull.Value ? -1 : (int)rdr["UOM"];
                            t.CreatedOn = rdr["CreatedOn"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["CreatedOn"]);
                            t.CreatedBy = rdr["CreatedBy"] == DBNull.Value ? -1 : (int)rdr["CreatedBy"];
                            t.UpdatedOn = rdr["UpdatedOn"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["UpdatedOn"]);
                            t.UpdatedBy = rdr["UpdatedBy"] == DBNull.Value ? -1 : (int)rdr["UpdatedBy"];
                            t.IsActive = rdr["IsActive"] == DBNull.Value ? -1 : (int)rdr["IsActive"];
                            t.TransType = rdr["TransType"] == DBNull.Value ? "" : rdr["TransType"].ToString();
                            t.SWorkSiteID = rdr["SWorkSiteID"] == DBNull.Value ? -1 : (int)rdr["SWorkSiteID"];
                            t.SProjectID = rdr["SProjectID"] == DBNull.Value ? -1 : (int)rdr["SProjectID"];
                            t.SCompanyID = rdr["SCompanyID"] == DBNull.Value ? -1 : (int)rdr["SCompanyID"];
                            t.PageLocation = rdr["PageLocation"] == DBNull.Value ? "" : rdr["PageLocation"].ToString();
                            t.Status = rdr["Status"] == DBNull.Value ? -1 : (int)rdr["Status"];
                            t.TRcvdBY = rdr["TRcvdBY"] == DBNull.Value ? -1 : (int)rdr["TRcvdBY"];
                            t.RecvID = rdr["RecvID"] == DBNull.Value ? -1 : (int)rdr["RecvID"];
                            t.TTransferID = rdr["TTransferID"] == DBNull.Value ? -1 : (int)rdr["TTransferID"];
                            t.TAssignMatId = rdr["TAssignMatId"] == DBNull.Value ? -1 : (int)rdr["TAssignMatId"];
                            t.TypeOfIssue = rdr["TypeOfIssue"] == DBNull.Value ? "" : rdr["TypeOfIssue"].ToString();
                            t.ReturnMode = rdr["ReturnMode"] == DBNull.Value ? -1 : (int)rdr["ReturnMode"];
                            t.RetRemarks = rdr["RetRemarks"] == DBNull.Value ? "" : rdr["RetRemarks"].ToString();
                            t.RetDesc = rdr["RetDesc"] == DBNull.Value ? "" : rdr["RetDesc"].ToString();
                            t.TIssueID = rdr["TIssueID"] == DBNull.Value ? -1 : (int)rdr["TIssueID"];
                            t.TRBoundReceivedBy = rdr["TRBoundReceivedBy"] == DBNull.Value ? -1 : (int)rdr["TRBoundReceivedBy"];
                            t.TRBoundReceivedDt = rdr["TRBoundReceivedDt"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["TRBoundReceivedDt"]);
                            t.TransferDiffQty = rdr["TransferDiffQty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["TransferDiffQty"]);
                            t.TransverRemarks = rdr["TransverRemarks"] == DBNull.Value ? "" : rdr["TransverRemarks"].ToString();
                            t.PrjClosingBalance = rdr["PrjClosingBalance"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["PrjClosingBalance"]);
                            t.scrapqty = rdr["scrapqty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["scrapqty"]);
                            t.scrapstatus = rdr["scrapstatus"] == DBNull.Value ? -1 : (int)rdr["scrapstatus"];
                            t.RetAsgnMatId = rdr["RetAsgnMatId"] == DBNull.Value ? -1 : (int)rdr["RetAsgnMatId"];
                            t.execQtyAsgnMatID = rdr["execQtyAsgnMatID"] == DBNull.Value ? -1 : (int)rdr["execQtyAsgnMatID"];
                            t.FuelRemarks = rdr["FuelRemarks"] == DBNull.Value ? "" : rdr["FuelRemarks"].ToString();
                            t.TBPersonID = rdr["TBPersonID"] == DBNull.Value ? -1 : (int)rdr["TBPersonID"];
                            t.SmsExpBookingID = rdr["SmsExpBookingID"] == DBNull.Value ? -1 : (int)rdr["SmsExpBookingID"];
                            clstockJournal.Add(t);
                        }
                    }
                    totalRecords = (int)cmd.Parameters["@tr"].Value;
                }
                con.Close();
                 return new Tuple<int, List<t_mms_stockJournal>>(totalRecords, clstockJournal);
            }
        }
        public static async Task<string> Createt_mms_stockJournalAsync(List<t_mms_stockJournal> clt_mms_stockJournal, int cid, int userID) {
            try {
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("createt_mms_stockJournal"));
                    t_mms_stockJournal t = new t_mms_stockJournal();
                    DataTable dtClass = t.dtClass("dtClass");
                    int i = 0; //CONSIDER CHANGING THE AUTOINCREMENTING TABLE ID FIELD TO THIS VARIABLE
                foreach(t_mms_stockJournal E in clt_mms_stockJournal) {
                        DataRow dr = dtClass.NewRow();
                        dr["ResTransID"] = E.ResTransID;
                        dr["PGRNItemID"] = E.PGRNItemID;
                        dr["ResourceID"] = E.ResourceID;
                        dr["IssueDetID"] = E.IssueDetID;
                        dr["PGDNDate"] = E.PGDNDate;
                        dr["IssueDate"] = E.IssueDate;
                        dr["RcvdQty"] = E.RcvdQty;
                        dr["IssuedQty"] = E.IssuedQty;
                        dr["ClosingBalance"] = E.ClosingBalance;
                        dr["UOM"] = E.UOM;
                        dr["CreatedOn"] = E.CreatedOn;
                        dr["CreatedBy"] = E.CreatedBy;
                        dr["UpdatedOn"] = E.UpdatedOn;
                        dr["UpdatedBy"] = E.UpdatedBy;
                        dr["IsActive"] = E.IsActive;
                        dr["TransType"] = E.TransType;
                        dr["SWorkSiteID"] = E.SWorkSiteID;
                        dr["SProjectID"] = E.SProjectID;
                        dr["SCompanyID"] = E.SCompanyID;
                        dr["PageLocation"] = E.PageLocation;
                        dr["Status"] = E.Status;
                        dr["TRcvdBY"] = E.TRcvdBY;
                        dr["RecvID"] = E.RecvID;
                        dr["TTransferID"] = E.TTransferID;
                        dr["TAssignMatId"] = E.TAssignMatId;
                        dr["TypeOfIssue"] = E.TypeOfIssue;
                        dr["ReturnMode"] = E.ReturnMode;
                        dr["RetRemarks"] = E.RetRemarks;
                        dr["RetDesc"] = E.RetDesc;
                        dr["TIssueID"] = E.TIssueID;
                        dr["TRBoundReceivedBy"] = E.TRBoundReceivedBy;
                        dr["TRBoundReceivedDt"] = E.TRBoundReceivedDt;
                        dr["TransferDiffQty"] = E.TransferDiffQty;
                        dr["TransverRemarks"] = E.TransverRemarks;
                        dr["PrjClosingBalance"] = E.PrjClosingBalance;
                        dr["scrapqty"] = E.scrapqty;
                        dr["scrapstatus"] = E.scrapstatus;
                        dr["RetAsgnMatId"] = E.RetAsgnMatId;
                        dr["execQtyAsgnMatID"] = E.execQtyAsgnMatID;
                        dr["FuelRemarks"] = E.FuelRemarks;
                        dr["TBPersonID"] = E.TBPersonID;
                        dr["SmsExpBookingID"] = E.SmsExpBookingID;
                        dtClass.Rows.Add(dr);
                    }
                    using(SqlBulkCopy bcp = new SqlBulkCopy(con, SqlBulkCopyOptions.Default, tran)) {
                        bcp.DestinationTableName = "[dbo].[t_mms_stockJournal]";
                        try {
                            await bcp.WriteToServerAsync(dtClass);
                            tran.Commit();
                            con.Close();
                            return clt_mms_stockJournal.Count.ToString() + " record(s) inserted!";
                        } catch(Exception ex) {
                            tran.Rollback();
                            con.Close();
                            return "failed " + ex.Message;
                        }
                    }
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
        public static async Task<string> Updatet_mms_stockJournalAsync(List<t_mms_stockJournal> clt_mms_stockJournal, int cid, int userID) {
            try {
                DataTable dtClass = new t_mms_stockJournal().dtClass("dtClass");
                foreach(t_mms_stockJournal E in clt_mms_stockJournal) {
                    DataRow dr = dtClass.NewRow();
                    dr["ResTransID"] = E.ResTransID;
                    dr["PGRNItemID"] = E.PGRNItemID;
                    dr["ResourceID"] = E.ResourceID;
                    dr["IssueDetID"] = E.IssueDetID;
                    dr["PGDNDate"] = E.PGDNDate;
                    dr["IssueDate"] = E.IssueDate;
                    dr["RcvdQty"] = E.RcvdQty;
                    dr["IssuedQty"] = E.IssuedQty;
                    dr["ClosingBalance"] = E.ClosingBalance;
                    dr["UOM"] = E.UOM;
                    dr["CreatedOn"] = E.CreatedOn;
                    dr["CreatedBy"] = E.CreatedBy;
                    dr["UpdatedOn"] = E.UpdatedOn;
                    dr["UpdatedBy"] = E.UpdatedBy;
                    dr["IsActive"] = E.IsActive;
                    dr["TransType"] = E.TransType;
                    dr["SWorkSiteID"] = E.SWorkSiteID;
                    dr["SProjectID"] = E.SProjectID;
                    dr["SCompanyID"] = E.SCompanyID;
                    dr["PageLocation"] = E.PageLocation;
                    dr["Status"] = E.Status;
                    dr["TRcvdBY"] = E.TRcvdBY;
                    dr["RecvID"] = E.RecvID;
                    dr["TTransferID"] = E.TTransferID;
                    dr["TAssignMatId"] = E.TAssignMatId;
                    dr["TypeOfIssue"] = E.TypeOfIssue;
                    dr["ReturnMode"] = E.ReturnMode;
                    dr["RetRemarks"] = E.RetRemarks;
                    dr["RetDesc"] = E.RetDesc;
                    dr["TIssueID"] = E.TIssueID;
                    dr["TRBoundReceivedBy"] = E.TRBoundReceivedBy;
                    dr["TRBoundReceivedDt"] = E.TRBoundReceivedDt;
                    dr["TransferDiffQty"] = E.TransferDiffQty;
                    dr["TransverRemarks"] = E.TransverRemarks;
                    dr["PrjClosingBalance"] = E.PrjClosingBalance;
                    dr["scrapqty"] = E.scrapqty;
                    dr["scrapstatus"] = E.scrapstatus;
                    dr["RetAsgnMatId"] = E.RetAsgnMatId;
                    dr["execQtyAsgnMatID"] = E.execQtyAsgnMatID;
                    dr["FuelRemarks"] = E.FuelRemarks;
                    dr["TBPersonID"] = E.TBPersonID;
                    dr["SmsExpBookingID"] = E.SmsExpBookingID;
                    dtClass.Rows.Add(dr);
                }
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("blkUpdate"));
                    string query = "sh_t_mms_stockJournalUpdate";
                    int recordsUpdated = 0;
                    using(SqlCommand cmd = new SqlCommand(query, con, tran)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@tblType", dtClass));
                        cmd.Parameters.Add(new SqlParameter("@userID", userID));
                        recordsUpdated = await cmd.ExecuteNonQueryAsync();
                    }
                    if(recordsUpdated > 0) tran.Commit();
                    else return "failed to update!";
                    con.Close();
                    return recordsUpdated.ToString() + " records(s) updated";
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
    }
    public class t_mms_stockJournal {
        public int ResTransID { get; set; }
        public int PGRNItemID { get; set; }
        public int ResourceID { get; set; }
        public int IssueDetID { get; set; }
        public DateTime PGDNDate { get; set; }
        public DateTime IssueDate { get; set; }
        public decimal RcvdQty { get; set; }
        public decimal IssuedQty { get; set; }
        public decimal ClosingBalance { get; set; }
        public int UOM { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public int IsActive { get; set; }
        public string TransType { get; set; }
        public int SWorkSiteID { get; set; }
        public int SProjectID { get; set; }
        public int SCompanyID { get; set; }
        public string PageLocation { get; set; }
        public int Status { get; set; }
        public int TRcvdBY { get; set; }
        public int RecvID { get; set; }
        public int TTransferID { get; set; }
        public int TAssignMatId { get; set; }
        public string TypeOfIssue { get; set; }
        public int ReturnMode { get; set; }
        public string RetRemarks { get; set; }
        public string RetDesc { get; set; }
        public int TIssueID { get; set; }
        public int TRBoundReceivedBy { get; set; }
        public DateTime TRBoundReceivedDt { get; set; }
        public decimal TransferDiffQty { get; set; }
        public string TransverRemarks { get; set; }
        public decimal PrjClosingBalance { get; set; }
        public decimal scrapqty { get; set; }
        public int scrapstatus { get; set; }
        public int RetAsgnMatId { get; set; }
        public int execQtyAsgnMatID { get; set; }
        public string FuelRemarks { get; set; }
        public int TBPersonID { get; set; }
        public int SmsExpBookingID { get; set; }

        public t_mms_stockJournal() { }
        public t_mms_stockJournal(int _ResTransID, int _PGRNItemID, int _ResourceID, int _IssueDetID, DateTime _PGDNDate
            , DateTime _IssueDate, decimal _RcvdQty, decimal _IssuedQty, decimal _ClosingBalance, int _UOM, DateTime _CreatedOn
            , int _CreatedBy, DateTime _UpdatedOn, int _UpdatedBy, int _IsActive, string _TransType, int _SWorkSiteID, int _SProjectID
            , int _SCompanyID, string _PageLocation, int _Status, int _TRcvdBY, int _RecvID, int _TTransferID, int _TAssignMatId
            , string _TypeOfIssue, int _ReturnMode, string _RetRemarks, string _RetDesc, int _TIssueID, int _TRBoundReceivedBy
            , DateTime _TRBoundReceivedDt, decimal _TransferDiffQty, string _TransverRemarks, decimal _PrjClosingBalance
            , decimal _scrapqty, int _scrapstatus, int _RetAsgnMatId, int _execQtyAsgnMatID, string _FuelRemarks, int _TBPersonID
            , int _SmsExpBookingID) {
            ResTransID = _ResTransID; PGRNItemID = _PGRNItemID; ResourceID = _ResourceID; IssueDetID = _IssueDetID; PGDNDate = _PGDNDate; IssueDate = _IssueDate; RcvdQty = _RcvdQty; IssuedQty = _IssuedQty; ClosingBalance = _ClosingBalance; UOM = _UOM; CreatedOn = _CreatedOn; CreatedBy = _CreatedBy; UpdatedOn = _UpdatedOn; UpdatedBy = _UpdatedBy; IsActive = _IsActive; TransType = _TransType; SWorkSiteID = _SWorkSiteID; SProjectID = _SProjectID; SCompanyID = _SCompanyID; PageLocation = _PageLocation; Status = _Status; TRcvdBY = _TRcvdBY; RecvID = _RecvID; TTransferID = _TTransferID; TAssignMatId = _TAssignMatId; TypeOfIssue = _TypeOfIssue; ReturnMode = _ReturnMode; RetRemarks = _RetRemarks; RetDesc = _RetDesc; TIssueID = _TIssueID; TRBoundReceivedBy = _TRBoundReceivedBy; TRBoundReceivedDt = _TRBoundReceivedDt; TransferDiffQty = _TransferDiffQty; TransverRemarks = _TransverRemarks; PrjClosingBalance = _PrjClosingBalance; scrapqty = _scrapqty; scrapstatus = _scrapstatus; RetAsgnMatId = _RetAsgnMatId; execQtyAsgnMatID = _execQtyAsgnMatID; FuelRemarks = _FuelRemarks; TBPersonID = _TBPersonID; SmsExpBookingID = _SmsExpBookingID;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("ResTransID", typeof(System.Int32));
            dtName.Columns.Add("PGRNItemID", typeof(System.Int32));
            dtName.Columns.Add("ResourceID", typeof(System.Int32));
            dtName.Columns.Add("IssueDetID", typeof(System.Int32));
            dtName.Columns.Add("PGDNDate", typeof(System.DateTime));
            dtName.Columns.Add("IssueDate", typeof(System.DateTime));
            dtName.Columns.Add("RcvdQty", typeof(System.Double));
            dtName.Columns.Add("IssuedQty", typeof(System.Double));
            dtName.Columns.Add("ClosingBalance", typeof(System.Double));
            dtName.Columns.Add("UOM", typeof(System.Int32));
            dtName.Columns.Add("CreatedOn", typeof(System.DateTime));
            dtName.Columns.Add("CreatedBy", typeof(System.Int32));
            dtName.Columns.Add("UpdatedOn", typeof(System.DateTime));
            dtName.Columns.Add("UpdatedBy", typeof(System.Int32));
            dtName.Columns.Add("IsActive", typeof(System.Int32));
            dtName.Columns.Add("TransType", typeof(System.String));
            dtName.Columns.Add("SWorkSiteID", typeof(System.Int32));
            dtName.Columns.Add("SProjectID", typeof(System.Int32));
            dtName.Columns.Add("SCompanyID", typeof(System.Int32));
            dtName.Columns.Add("PageLocation", typeof(System.String));
            dtName.Columns.Add("Status", typeof(System.Int32));
            dtName.Columns.Add("TRcvdBY", typeof(System.Int32));
            dtName.Columns.Add("RecvID", typeof(System.Int32));
            dtName.Columns.Add("TTransferID", typeof(System.Int32));
            dtName.Columns.Add("TAssignMatId", typeof(System.Int32));
            dtName.Columns.Add("TypeOfIssue", typeof(System.String));
            dtName.Columns.Add("ReturnMode", typeof(System.Int32));
            dtName.Columns.Add("RetRemarks", typeof(System.String));
            dtName.Columns.Add("RetDesc", typeof(System.String));
            dtName.Columns.Add("TIssueID", typeof(System.Int32));
            dtName.Columns.Add("TRBoundReceivedBy", typeof(System.Int32));
            dtName.Columns.Add("TRBoundReceivedDt", typeof(System.DateTime));
            dtName.Columns.Add("TransferDiffQty", typeof(System.Double));
            dtName.Columns.Add("TransverRemarks", typeof(System.String));
            dtName.Columns.Add("PrjClosingBalance", typeof(System.Double));
            dtName.Columns.Add("scrapqty", typeof(System.Double));
            dtName.Columns.Add("scrapstatus", typeof(System.Int32));
            dtName.Columns.Add("RetAsgnMatId", typeof(System.Int32));
            dtName.Columns.Add("execQtyAsgnMatID", typeof(System.Int32));
            dtName.Columns.Add("FuelRemarks", typeof(System.String));
            dtName.Columns.Add("TBPersonID", typeof(System.Int32));
            dtName.Columns.Add("SmsExpBookingID", typeof(System.Int32));
            return dtName;
        }
    }
}
