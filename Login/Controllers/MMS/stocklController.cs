﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Login.Controllers.MMS
{
    public class stockController : ApiController
    {
        [HttpGet] // http://localhost/Login/api/stock/journal?cid=1&pid=71&wid=1&userID=1&cp=1&ps=20
        [Route("api/stock/journal")]
        public async Task<HttpResponseMessage> getstockJournal(int cid, int? pid, int? wid, int userID, int cp, int ps)
        {
            try
            {
                Tuple<int, List<t_mms_stockJournal>> clstockJournal = await Task.Run(() => stockDAL.ReadtStockJournalAsync(cid, pid, wid, userID, cp, ps));
                return Request.CreateResponse(HttpStatusCode.OK, clstockJournal);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpPost]
        [Route("api/stock/journal")]
        public async Task<HttpResponseMessage> BulkCreatet_mms_stockJournalAsync([FromBody]List<t_mms_stockJournal> clt_mms_stockJournal, int cid, int userID)
        {
            try
            {          // http://localhost/Login/api/stock/journal?cid=1&userID=xx
                       //[{  ResTransID: -1, PGRNItemID: -1, ResourceID: -1, IssueDetID: -1, PGDNDate: '2019-04-15', IssueDate: '2019-04-15', RcvdQty: 0, IssuedQty: 0, ClosingBalance: 0, UOM: -1, CreatedOn: '2019-04-15', CreatedBy: -1, UpdatedOn: '2019-04-15', UpdatedBy: -1, IsActive: -1, TransType: 'YOUR DATA', SWorkSiteID: -1, SProjectID: -1, SCompanyID: -1, PageLocation: 'YOUR DATA', Status: -1, TRcvdBY: -1, RecvID: -1, TTransferID: -1, TAssignMatId: -1, TypeOfIssue: 'YOUR DATA', ReturnMode: -1, RetRemarks: 'YOUR DATA', RetDesc: 'YOUR DATA', TIssueID: -1, TRBoundReceivedBy: -1, TRBoundReceivedDt: '2019-04-15', TransferDiffQty: 0, TransverRemarks: 'YOUR DATA', PrjClosingBalance: 0, scrapqty: 0, scrapstatus: -1, RetAsgnMatId: -1, execQtyAsgnMatID: -1, FuelRemarks: 'YOUR DATA', TBPersonID: -1, SmsExpBookingID: -1,  }]
                string result = await Task.Run(() => stockDAL.Createt_mms_stockJournalAsync(clt_mms_stockJournal, cid, userID));
                if (!result.Contains("failed"))
                {
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPut]
        [Route("api/stock/journal")]
        public async Task<HttpResponseMessage> BulkUpdatet_mms_stockJournal([FromBody]List<t_mms_stockJournal> clt_mms_stockJournal, int cid, int userID)
        {
            try
            {          // http://localhost/Login/api/stock/journal?cid=1&userID=xx
                       //[{  ResTransID: -1, PGRNItemID: -1, ResourceID: -1, IssueDetID: -1, PGDNDate: '2019-04-15', IssueDate: '2019-04-15', RcvdQty: 0, IssuedQty: 0, ClosingBalance: 0, UOM: -1, CreatedOn: '2019-04-15', CreatedBy: -1, UpdatedOn: '2019-04-15', UpdatedBy: -1, IsActive: -1, TransType: 'YOUR DATA', SWorkSiteID: -1, SProjectID: -1, SCompanyID: -1, PageLocation: 'YOUR DATA', Status: -1, TRcvdBY: -1, RecvID: -1, TTransferID: -1, TAssignMatId: -1, TypeOfIssue: 'YOUR DATA', ReturnMode: -1, RetRemarks: 'YOUR DATA', RetDesc: 'YOUR DATA', TIssueID: -1, TRBoundReceivedBy: -1, TRBoundReceivedDt: '2019-04-15', TransferDiffQty: 0, TransverRemarks: 'YOUR DATA', PrjClosingBalance: 0, scrapqty: 0, scrapstatus: -1, RetAsgnMatId: -1, execQtyAsgnMatID: -1, FuelRemarks: 'YOUR DATA', TBPersonID: -1, SmsExpBookingID: -1,  }]
                if (clt_mms_stockJournal.Count > 0)
                {
                    string result = await Task.Run(() => stockDAL.Updatet_mms_stockJournalAsync(clt_mms_stockJournal, cid, userID));
                    if (!result.Contains("failed"))
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    }
                    else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
                }
                else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No records to Update");
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        //[HttpDelete]
        //[Route("api/stock/journal")]
        //public async Task<HttpResponseMessage> BulkDeletet_mms_stockJournal([FromBody] List<int> IDs, int userID) {
        //    try {          // http://localhost/Login/api/stock/journal?userID=xx
        //        if(IDs.Count > 0) {
        //            string result = await Task.Run(() => stockDAL.Deletet_mms_stockJournalAsync(IDs, userID));
        //            if(!result.Contains("failed")) {
        //                return Request.CreateResponse(HttpStatusCode.OK, result);
        //            } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
        //        } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No records to Delete");
        //    } catch(Exception ex) {
        //        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
        //    }
        //}
    }
}