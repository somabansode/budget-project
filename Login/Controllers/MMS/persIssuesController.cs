﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Login.Controllers.MMS {
    public class persIssuesController :ApiController {
        //TESTED OK
        [HttpGet] // http://localhost/Login/api/persIssues/STBased?pid=1&tid=null&cp=1&ps=100
        [Route("api/persIssues/STBased")]
        public async Task<HttpResponseMessage> TaskBasedPersonalIssues(int pid, int? tid, int cp, int ps) {
            try {    
                Tuple<int, List<tbPersonalIssues>> clSTPersonalIssues = await Task.Run(() => 
                persIssuesDAL.ReadTaskBasedPersonalIssuesAsync(pid, tid, cp, ps));
                return Request.CreateResponse(HttpStatusCode.OK, clSTPersonalIssues);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost]// http://localhost/Login/api/persIssues/STBased?wid=1&pid=1&userID=1&rcvr=6&dtSt=2019-04-16&dtEd=2019-04-17
        [Route("api/persIssues/STBased")]
        public async Task<HttpResponseMessage> CreateTBIssuesAsync([FromBody] twoArrays resStIDs,int wid, int pid
            , int userID, int rcvr, DateTime dtSt, DateTime dtEd) {
            try {     
                string result = await Task.Run(() => persIssuesDAL.CreateTBIssuesAsync(resStIDs,wid, pid, userID,rcvr,dtSt,dtEd));
                  
                if(!result.Contains("failed")) {
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        //[HttpDelete]
        //[Route("api/persIssues/to_TaskBasedPersonalIssues")]
        //public async Task<HttpResponseMessage> BulkDeleteto_TaskBasedPersonalIssues([FromBody] List<int> IDs, int userID) {
        //    try {          // http://localhost/Login/api/persIssues/to_TaskBasedPersonalIssues?userID=xx
        //        if(IDs.Count > 0) {
        //            string result = await Task.Run(() => persIssuesDAL.Deleteto_TaskBasedPersonalIssuesAsync(IDs, userID));
        //            if(!result.Contains("failed")) {
        //                return Request.CreateResponse(HttpStatusCode.OK, result);
        //            } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
        //        } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No records to Delete");
        //    } catch(Exception ex) {
        //        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
        //    }
        //}
    }
}
