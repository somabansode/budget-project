﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Login.Controllers.MMS {
    public class issuesDAL {
        //tested OK
        public static async Task<Tuple<int, List<T_MMS_Issues>>> ReadIssuesAsync(int cid , int pid, int wid,int userID, int cp, int ps) {
            List<T_MMS_Issues> clT_MMS_Issues = new List<T_MMS_Issues>();
            int totalRecords =0;
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "mms.IssuesRead";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    cmd.Parameters.Add(new SqlParameter("@pid", pid));
                    cmd.Parameters.Add(new SqlParameter("@wid", wid));
                    cmd.Parameters.Add(new SqlParameter("@userID", userID));
                    cmd.Parameters.Add(new SqlParameter("@cp", cp));
                    cmd.Parameters.Add(new SqlParameter("@ps", ps));
                    cmd.Parameters.Add(new SqlParameter("@tr", SqlDbType.Int)).Direction = ParameterDirection.Output;
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            T_MMS_Issues t = new T_MMS_Issues();
                            t.IssueID = rdr["IssueID"] == DBNull.Value ? -1 : (int)rdr["IssueID"];
                            t.Reciever = rdr["Reciever"] == DBNull.Value ? -1 : (int)rdr["Reciever"];
                            t.TransMode = rdr["TransMode"] == DBNull.Value ? -1 : (int)rdr["TransMode"];
                            t.ResourceID = rdr["ResourceID"] == DBNull.Value ? -1 : (int)rdr["ResourceID"];
                            t.IssueDate = rdr["IssueDate"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["IssueDate"]);
                            t.Item = rdr["Item"] == DBNull.Value ? "" : rdr["Item"].ToString();
                            t.pUom = rdr["pUom"] == DBNull.Value ? -1 : (int)rdr["pUom"];
                            //t.CreatedOn = rdr["CreatedOn"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["CreatedOn"]);
                            //t.CreatedBy = rdr["CreatedBy"] == DBNull.Value ? -1 : (int)rdr["CreatedBy"];
                            //t.UpdatedOn = rdr["UpdatedOn"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["UpdatedOn"]);
                            //t.UpdatedBy = rdr["UpdatedBy"] == DBNull.Value ? -1 : (int)rdr["UpdatedBy"];
                            //t.IsActive = rdr["IsActive"] == DBNull.Value ? -1 : (int)rdr["IsActive"];
                            t.IssuedBy = rdr["IssuedBy"] == DBNull.Value ? -1 : (int)rdr["IssuedBy"];
                            t.JobCardId = rdr["JobCardId"] == DBNull.Value ? -1 : (int)rdr["JobCardId"];
                            t.ConversionID = rdr["ConversionID"] == DBNull.Value ? -1 : (int)rdr["ConversionID"];
                            t.TransferID = rdr["TransferID"] == DBNull.Value ? -1 : (int)rdr["TransferID"];
                            t.Description = rdr["Description"] == DBNull.Value ? "" : rdr["Description"].ToString();
                            t.SGDNID = rdr["SGDNID"] == DBNull.Value ? -1 : (int)rdr["SGDNID"];
                            t.TTypeOfIssue = rdr["TTypeOfIssue"] == DBNull.Value ? "" : rdr["TTypeOfIssue"].ToString();
                            //t.TaskAssignMatID = rdr["TaskAssignMatID"] == DBNull.Value ? -1 : (int)rdr["TaskAssignMatID"];//can be NULL
                            t.subresid = rdr["subresid"] == DBNull.Value ? -1 : (int)rdr["subresid"];
                            t.TaskBasePersonIssueID = rdr["TaskBasePersonIssueID"] == DBNull.Value ? -1 : (int)rdr["TaskBasePersonIssueID"];
                            clT_MMS_Issues.Add(t);
                        }
                    }  totalRecords = (int)cmd.Parameters["@tr"].Value;
                }
                con.Close();
                return new Tuple<int, List<T_MMS_Issues>>(totalRecords, clT_MMS_Issues);
                //return clT_MMS_Issues;
            }
        }
        //NOT IMPLEMENTED
        public static async Task<string> CreateT_MMS_IssuesAsync(List<T_MMS_Issues> clT_MMS_Issues, int cid, int userID) {
            try {
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("createT_MMS_Issues"));
                    T_MMS_Issues t = new T_MMS_Issues();
                    DataTable dtClass = t.dtClass("dtClass");
                    int i = 0; //CONSIDER CHANGING THE AUTOINCREMENTING TABLE ID FIELD TO THIS VARIABLE
                    foreach(T_MMS_Issues E in clT_MMS_Issues) {
                        DataRow dr = dtClass.NewRow();
                        dr["IssueID"] = E.IssueID;
                        dr["Reciever"] = E.Reciever;
                        dr["TransMode"] = E.TransMode;
                        dr["ResourceID"] = E.ResourceID;
                        dr["IssueDate"] = E.IssueDate;
                        //dr["CreatedOn"] = E.CreatedOn;
                        //dr["CreatedBy"] = E.CreatedBy;
                        //dr["UpdatedOn"] = E.UpdatedOn;
                        //dr["UpdatedBy"] = E.UpdatedBy;
                        //dr["IsActive"] = E.IsActive;
                        dr["IssuedBy"] = E.IssuedBy;
                        dr["JobCardId"] = E.JobCardId;
                        dr["ConversionID"] = E.ConversionID;
                        dr["TransferID"] = E.TransferID;
                        dr["Description"] = E.Description;
                        dr["SGDNID"] = E.SGDNID;
                        dr["TTypeOfIssue"] = E.TTypeOfIssue;
                        //dr["TaskAssignMatID"] = E.TaskAssignMatID;
                        dr["subresid"] = E.subresid;
                        dr["TaskBasePersonIssueID"] = E.TaskBasePersonIssueID;
                        dtClass.Rows.Add(dr);
                    }
                    using(SqlBulkCopy bcp = new SqlBulkCopy(con, SqlBulkCopyOptions.Default, tran)) {
                        bcp.DestinationTableName = "[dbo].[T_MMS_Issues]";
                        try {
                            await bcp.WriteToServerAsync(dtClass);
                            tran.Commit();
                            con.Close();
                            return clT_MMS_Issues.Count.ToString() + " record(s) inserted!";
                        } catch(Exception ex) {
                            tran.Rollback();
                            con.Close();
                            return "failed " + ex.Message;
                        }
                    }
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
         
        public static async Task<List<T_MMS_IssueDetails>> ReadIssueDetailsAsync(int IssueID) {
            List<T_MMS_IssueDetails> clT_MMS_IssueDetails = new List<T_MMS_IssueDetails>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "mms.IssueDetailsRead";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@IssueID", IssueID));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            T_MMS_IssueDetails t = new T_MMS_IssueDetails();
                            t.IssueDetID = rdr["IssueDetID"] == DBNull.Value ? -1 : (int)rdr["IssueDetID"];
                            t.IssueID = rdr["IssueID"] == DBNull.Value ? -1 : (int)rdr["IssueID"];
                            t.GRNItemID = rdr["GRNItemID"] == DBNull.Value ? -1 : (int)rdr["GRNItemID"];
                            t.StoreID = rdr["StoreID"] == DBNull.Value ? -1 : (int)rdr["StoreID"];
                            t.Qty = rdr["Qty"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["Qty"]);
                            //t.TaskID = rdr["TaskID"] == DBNull.Value ? -1 : (int)rdr["TaskID"];
                            //t.AuthorisedBy = rdr["AuthorisedBy"] == DBNull.Value ? -1 : (int)rdr["AuthorisedBy"];
                            //t.AuthorisedDate = rdr["AuthorisedDate"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["AuthorisedDate"]);
                            //t.AssignMatId = rdr["AssignMatId"] == DBNull.Value ? -1 : (int)rdr["AssignMatId"];
                            //t.TransID = rdr["TransID"] == DBNull.Value ? -1 : (int)rdr["TransID"];
                            //t.DeductedBy = rdr["DeductedBy"] == DBNull.Value ? -1 : (int)rdr["DeductedBy"];
                            //t.SalesItemId = rdr["SalesItemId"] == DBNull.Value ? -1 : (int)rdr["SalesItemId"];
                            clT_MMS_IssueDetails.Add(t);
                        }
                    }
                }
                con.Close();
                return clT_MMS_IssueDetails;
            }
        }
        public static async Task<string> CreateT_MMS_IssueDetailsAsync(List<T_MMS_IssueDetails> clT_MMS_IssueDetails, int cid, int userID) {
            try {
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("createT_MMS_IssueDetails"));
                    T_MMS_IssueDetails t = new T_MMS_IssueDetails();
                    DataTable dtClass = t.dtClass("dtClass");
                    int i = 0; //CONSIDER CHANGING THE AUTOINCREMENTING TABLE ID FIELD TO THIS VARIABLE
                foreach(T_MMS_IssueDetails E in clT_MMS_IssueDetails) {
                        DataRow dr = dtClass.NewRow();
                        dr["IssueDetID"] = E.IssueDetID;
                        dr["IssueID"] = E.IssueID;
                        dr["GRNItemID"] = E.GRNItemID;
                        dr["StoreID"] = E.StoreID;
                        dr["Qty"] = E.Qty;
                        //dr["TaskID"] = E.TaskID;
                        //dr["AuthorisedBy"] = E.AuthorisedBy;
                        //dr["AuthorisedDate"] = E.AuthorisedDate;
                        //dr["AssignMatId"] = E.AssignMatId;
                        //dr["TransID"] = E.TransID;
                        //dr["DeductedBy"] = E.DeductedBy;
                        //dr["SalesItemId"] = E.SalesItemId;
                        dtClass.Rows.Add(dr);
                    }
                    using(SqlBulkCopy bcp = new SqlBulkCopy(con, SqlBulkCopyOptions.Default, tran)) {
                        bcp.DestinationTableName = "[dbo].[T_MMS_IssueDetails]";
                        try {
                            await bcp.WriteToServerAsync(dtClass);
                            tran.Commit();
                            con.Close();
                            return clT_MMS_IssueDetails.Count.ToString() + " record(s) inserted!";
                        } catch(Exception ex) {
                            tran.Rollback();
                            con.Close();
                            return "failed " + ex.Message;
                        }
                    }
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
        
    }
    public class T_MMS_Issues {
        public int IssueID { get; set; }
        public int Reciever { get; set; }
        public int TransMode { get; set; }
        public int ResourceID { get; set; }
        public string Item { get; set; }
        public int pUom { get; set; }
        public DateTime IssueDate { get; set; }
        //public DateTime CreatedOn { get; set; }
        //public int CreatedBy { get; set; }
        //public DateTime UpdatedOn { get; set; }
        //public int UpdatedBy { get; set; }
        //public int IsActive { get; set; }
        public int IssuedBy { get; set; }
        public int JobCardId { get; set; }
        public int ConversionID { get; set; }
        public int TransferID { get; set; }
        public string Description { get; set; }
        public int SGDNID { get; set; }
        public string TTypeOfIssue { get; set; }
        //public int TaskAssignMatID { get; set; }
        public int subresid { get; set; }
        public int TaskBasePersonIssueID { get; set; }

        public T_MMS_Issues() { }
        public T_MMS_Issues(int _IssueID, int _Reciever, int _TransMode, int _ResourceID, DateTime _IssueDate,
            string _resourceName, int _pUom, int _IssuedBy , int _JobCardId, int _ConversionID, int _TransferID, string _Description
            , int _SGDNID, string _TTypeOfIssue, int _subresid, int _TaskBasePersonIssueID) {
            IssueID = _IssueID; Reciever = _Reciever; TransMode = _TransMode; ResourceID = _ResourceID; IssueDate = _IssueDate;
            Item=_resourceName;pUom = _pUom;
             IssuedBy = _IssuedBy; JobCardId = _JobCardId; ConversionID = _ConversionID;
            TransferID = _TransferID; Description = _Description; SGDNID = _SGDNID; TTypeOfIssue = _TTypeOfIssue;            subresid = _subresid; TaskBasePersonIssueID = _TaskBasePersonIssueID;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("IssueID", typeof(System.Int32));
            dtName.Columns.Add("Reciever", typeof(System.Int32));
            dtName.Columns.Add("TransMode", typeof(System.Int32));
            dtName.Columns.Add("ResourceID", typeof(System.Int32));
            dtName.Columns.Add("IssueDate", typeof(System.DateTime));
            //dtName.Columns.Add("CreatedOn", typeof(System.DateTime));
            //dtName.Columns.Add("CreatedBy", typeof(System.Int32));
            //dtName.Columns.Add("UpdatedOn", typeof(System.DateTime));
            //dtName.Columns.Add("UpdatedBy", typeof(System.Int32));
            dtName.Columns.Add("IsActive", typeof(System.Int32));
            dtName.Columns.Add("IssuedBy", typeof(System.Int32));
            dtName.Columns.Add("JobCardId", typeof(System.Int32));
            dtName.Columns.Add("ConversionID", typeof(System.Int32));
            dtName.Columns.Add("TransferID", typeof(System.Int32));
            dtName.Columns.Add("Description", typeof(System.String));
            dtName.Columns.Add("SGDNID", typeof(System.Int32));
            dtName.Columns.Add("TTypeOfIssue", typeof(System.String));
            dtName.Columns.Add("TaskAssignMatID", typeof(System.Int32));
            dtName.Columns.Add("subresid", typeof(System.Int32));
            dtName.Columns.Add("TaskBasePersonIssueID", typeof(System.Int32));
            return dtName;
        }
    }
    public class T_MMS_IssueDetails {
        public int IssueDetID { get; set; }
        public int IssueID { get; set; }
        public int GRNItemID { get; set; }
        public int StoreID { get; set; }
        public decimal Qty { get; set; }
        //public int TaskID { get; set; }
        //public int AuthorisedBy { get; set; }
        //public DateTime AuthorisedDate { get; set; }
        //public int AssignMatId { get; set; }
        //public int TransID { get; set; }
        //public int DeductedBy { get; set; }
        //public int SalesItemId { get; set; }
        //, int _TaskID, int _AuthorisedBy, DateTime _AuthorisedDate, int _AssignMatId, int _TransID
        //, int _DeductedBy, int _SalesItemId) 
        //TaskID = _TaskID; AuthorisedBy = _AuthorisedBy; AuthorisedDate = _AuthorisedDate;
            //AssignMatId = _AssignMatId; TransID = _TransID; DeductedBy = _DeductedBy; SalesItemId = _SalesItemId;
        public T_MMS_IssueDetails() { }
        public T_MMS_IssueDetails(int _IssueDetID, int _IssueID, int _GRNItemID, int _StoreID, decimal _Qty){
            IssueDetID = _IssueDetID; IssueID = _IssueID; GRNItemID = _GRNItemID; StoreID = _StoreID;  Qty = _Qty; }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            DataColumn IssueDetID = new DataColumn();
            IssueDetID.DataType = System.Type.GetType("System.Int32");
            IssueDetID.ColumnName = "IssueDetID";
            IssueDetID.AutoIncrement = true;
            dtName.Columns.Add(IssueDetID);
            dtName.Columns.Add("IssueDetID", typeof(System.Int32));
            dtName.Columns.Add("IssueID", typeof(System.Int32));
            dtName.Columns.Add("GRNItemID", typeof(System.Int32));
            dtName.Columns.Add("StoreID", typeof(System.Int32));
            dtName.Columns.Add("Qty", typeof(System.Double));
            dtName.Columns.Add("TaskID", typeof(System.Int32));
            dtName.Columns.Add("AuthorisedBy", typeof(System.Int32));
            dtName.Columns.Add("AuthorisedDate", typeof(System.DateTime));
            dtName.Columns.Add("AssignMatId", typeof(System.Int32));
            dtName.Columns.Add("TransID", typeof(System.Int32));
            dtName.Columns.Add("DeductedBy", typeof(System.Int32));
            dtName.Columns.Add("SalesItemId", typeof(System.Int32));

            DataColumn[] keys = new DataColumn[1];
            keys[0] = IssueDetID;
            dtName.PrimaryKey = keys;
            return dtName;
        }
    }
}
