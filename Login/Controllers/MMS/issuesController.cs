﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Login.Controllers.MMS {
    public class issuesController :ApiController {

        [HttpGet]
        [Route("api/issues/get")]// http://localhost/Login/api/issues/get?cid=1&pid=1&wid=1&userID=1&cp=1&ps=20
        public async Task<HttpResponseMessage> getT_MMS_Issues(int cid , int pid, int wid,int userID, int cp, int ps ) {
            try {     //int cid , int pid, int wid,int userID, int cp, int ps 
               Tuple<int, List<T_MMS_Issues>> clIssues = await Task.Run(() => issuesDAL.ReadIssuesAsync(cid , pid, wid,userID, cp, ps));
                intObject intObject = new intObject(clIssues.Item1, clIssues.Item2);
                return Request.CreateResponse(HttpStatusCode.OK, intObject);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpPost]
        [Route("api/issues/post")]
        public async Task<HttpResponseMessage> BulkCreateT_MMS_IssuesAsync([FromBody]List<T_MMS_Issues> clT_MMS_Issues, int cid, int userID) {
            try {          // http://localhost/Login/api/issues/post?cid=1&userID=xx
                           //[{  IssueID: -1, Reciever: -1, TransMode: -1, ResourceID: -1, IssueDate: '2019-04-15', CreatedOn: '2019-04-15', CreatedBy: -1, UpdatedOn: '2019-04-15', UpdatedBy: -1, IsActive: -1, IssuedBy: -1, JobCardId: -1, ConversionID: -1, TransferID: -1, Description: 'YOUR DATA', SGDNID: -1, TTypeOfIssue: 'YOUR DATA', TaskAssignMatID: -1, subresid: -1, TaskBasePersonIssueID: -1,  }]
                string result = await Task.Run(() => issuesDAL.CreateT_MMS_IssuesAsync(clT_MMS_Issues, cid, userID));
                if(!result.Contains("failed")) {
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        
        [HttpGet] // http://localhost/Login/api/issues/details?IssueID=4043
        [Route("api/issues/details")]
        public async Task<HttpResponseMessage> Details(int IssueID) {
            try {     //
                List<T_MMS_IssueDetails> clT_MMS_IssueDetails = await Task.Run(() => issuesDAL.ReadIssueDetailsAsync(IssueID));
                return Request.CreateResponse(HttpStatusCode.OK, clT_MMS_IssueDetails);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost]
        [Route("api/CONTROLLER/T_MMS_IssueDetails")]
        public async Task<HttpResponseMessage> BulkCreateT_MMS_IssueDetailsAsync([FromBody]List<T_MMS_IssueDetails> clT_MMS_IssueDetails, int cid, int userID) {
            try {          // http://localhost/Login/api/CONTROLLER/T_MMS_IssueDetails?cid=1&userID=xx
                           //[{  IssueDetID: -1, IssueID: -1, GRNItemID: -1, StoreID: -1, Qty: 0, TaskID: -1, AuthorisedBy: -1, AuthorisedDate: '2019-04-15', AssignMatId: -1, TransID: -1, DeductedBy: -1, SalesItemId: -1,  }]
                string result = await Task.Run(() => issuesDAL.CreateT_MMS_IssueDetailsAsync(clT_MMS_IssueDetails, cid, userID));
                if(!result.Contains("failed")) {
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        
    }
}




//this.gridOptions.columnDefs = [
//{ headerName: 'IssueID', field: 'IssueID', width: 70, checkboxSelection: true, cellStyle: { textAlign: 'right' } },
//{ headerName: 'Reciever', field: 'Reciever', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'TransMode', field: 'TransMode', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'ResourceID', field: 'ResourceID', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'IssueDate', field: 'IssueDate', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'CreatedOn', field: 'CreatedOn', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'CreatedBy', field: 'CreatedBy', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'UpdatedOn', field: 'UpdatedOn', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'UpdatedBy', field: 'UpdatedBy', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'IsActive', field: 'IsActive', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'IssuedBy', field: 'IssuedBy', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'JobCardId', field: 'JobCardId', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'ConversionID', field: 'ConversionID', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'TransferID', field: 'TransferID', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'Description', field: 'Description', width: 200, cellStyle: { textAlign: 'left' } },
//{ headerName: 'SGDNID', field: 'SGDNID', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'TTypeOfIssue', field: 'TTypeOfIssue', width: 200, cellStyle: { textAlign: 'left' } },
//{ headerName: 'TaskAssignMatID', field: 'TaskAssignMatID', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'Subresid', field: 'subresid', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'TaskBasePersonIssueID', field: 'TaskBasePersonIssueID', width: 70, cellStyle: { textAlign: 'right' } },

//];export interface T_MMS_Issues {
//	IssueID: number; 
//	Reciever: number; 
//	TransMode: number; 
//	ResourceID: number; 
//	IssueDate: Date; 
//	CreatedOn: Date; 
//	CreatedBy: number; 
//	UpdatedOn: Date; 
//	UpdatedBy: number; 
//	IsActive: number; 
//	IssuedBy: number; 
//	JobCardId: number; 
//	ConversionID: number; 
//	TransferID: number; 
//	Description: string; 
//	SGDNID: number; 
//	TTypeOfIssue: string; 
//	TaskAssignMatID: number; 
//	subresid: number; 
//	TaskBasePersonIssueID: number; 
// }
// DETAILS TABLE
//this.gridOptions.columnDefs = [
//{ headerName: 'IssueDetID', field: 'IssueDetID', width: 70, checkboxSelection: true, cellStyle: { textAlign: 'right' } },
//{ headerName: 'IssueID', field: 'IssueID', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'GRNItemID', field: 'GRNItemID', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'StoreID', field: 'StoreID', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'Qty', field: 'Qty', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'TaskID', field: 'TaskID', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'AuthorisedBy', field: 'AuthorisedBy', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'AuthorisedDate', field: 'AuthorisedDate', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'AssignMatId', field: 'AssignMatId', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'TransID', field: 'TransID', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'DeductedBy', field: 'DeductedBy', width: 70, cellStyle: { textAlign: 'right' } },
//{ headerName: 'SalesItemId', field: 'SalesItemId', width: 70, cellStyle: { textAlign: 'right' } },

//];export interface T_MMS_IssueDetails {
//	IssueDetID: number; 
//	IssueID: number; 
//	GRNItemID: number; 
//	StoreID: number; 
//	Qty: number; 
//	TaskID: number; 
//	AuthorisedBy: number; 
//	AuthorisedDate: Date; 
//	AssignMatId: number; 
//	TransID: number; 
//	DeductedBy: number; 
//	SalesItemId: number; 
// }