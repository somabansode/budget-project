﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Login.Controllers.MMS {
    public class ProdIssuesController :ApiController {
        [HttpGet] // http://localhost/Login/api/ProdIssues/Resources?pid=71&dtFrom=2019-04-13&dtTo=2019-04-13&rid=null
        [Route("api/ProdIssues/Resources")]
        public async Task<HttpResponseMessage> Resources(int pid, DateTime dtFrom, DateTime dtTo, int? rid) {
            try {
                List<materialsForIssueRes> clmaterialsForIssueRes = await Task.Run(() =>
                            prodIssuesDAL.Resources(pid, dtFrom, dtTo, rid));
                return Request.CreateResponse(HttpStatusCode.OK, clmaterialsForIssueRes);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost] // http://localhost/Login/api/ProdIssues/TASKs?pid=71&dtFrom=2019-04-13&dtTo=2019-04-13 
        [Route("api/ProdIssues/TASKs")]//[1,6,90]
        public async Task<HttpResponseMessage> TASKs(int pid, DateTime dtFrom, DateTime dtTo, List<int> rid) {
            try {
                List<materialsForIssueTASKs> clmaterialsForIssueTASKs = await Task.Run(() =>
                prodIssuesDAL.TASKs(pid, dtFrom, dtTo, rid));
                return Request.CreateResponse(HttpStatusCode.OK, clmaterialsForIssueTASKs);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost] // http://localhost/Login/api/ProdIssues/STs?pid=71&dtFrom=2019-04-13&dtTo=2019-04-13
        [Route("api/ProdIssues/STs")]//{array1:[3066,3088], array2[]}
        public async Task<HttpResponseMessage> STs(int pid, DateTime dtFrom, DateTime dtTo, twoArrays TRids) {
            try {
                List<materialsForIssueSTs> clmaterialsForIssueSTs = await Task.Run(() =>
                prodIssuesDAL.STs(pid, dtFrom, dtTo, TRids));
                return Request.CreateResponse(HttpStatusCode.OK, clmaterialsForIssueSTs);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet] // http://localhost/Login/api/ProdIssues/RSPlans?pid=71&dtFrom=2019-04-13&dtTo=2019-04-13&stid=24547&rid=null
        [Route("api/ProdIssues/RSPlans")]
        public async Task<HttpResponseMessage> RSPlans(int pid, DateTime dtFrom, DateTime dtTo, int stid, int rid) {
            try {
                List<materialsForIssueRSPlans> clmaterialsForIssueRSPlans = await Task.Run(() =>
                prodIssuesDAL.RSPlans(pid, dtFrom, dtTo, stid, rid));
                return Request.CreateResponse(HttpStatusCode.OK, clmaterialsForIssueRSPlans);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]// http://localhost/Login/api/ProdIssues/GdnPopUp?pid=71&dtFrom=2019-04-13&dtTo=2019-04-13&rid=null
        [Route("api/ProdIssues/GdnPopUp")]
        public async Task<HttpResponseMessage> GdnPopUp(int pid, DateTime dtFrom, DateTime dtTo, int rid) {
            try {
                List<materialsForIssuePop> clmaterialsForIssuePop = await Task.Run(() =>
                prodIssuesDAL.GdnPopUp(pid, dtFrom, dtTo, rid));
                return Request.CreateResponse(HttpStatusCode.OK, clmaterialsForIssuePop);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]  // http://localhost/Login/api/ProdIssues/report?pid=71&rid=6&dtFr=2019-04-23&dtTo=2019-04-24
        [Route("api/ProdIssues/report")]
        public async Task<HttpResponseMessage> report(int pid ,int rid ,DateTime dtFr ,DateTime dtTo) {
            try { 
                List<productionIssues> clproductionIssues = await Task.Run(() => prodIssuesDAL.ReadProductionIssuesAsync(pid ,rid ,dtFr ,dtTo));
                return Request.CreateResponse(HttpStatusCode.OK, clproductionIssues);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]  // http://localhost/Login/api/ProdIssues/resList?pid=71
        [Route("api/ProdIssues/resList")]
        public async Task<HttpResponseMessage> resList(int pid) {
            try { 
                List<intString> resList = await Task.Run(() => prodIssuesDAL.ReadResListAsync(pid));
                return Request.CreateResponse(HttpStatusCode.OK, resList);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]  // http://localhost/Login/api/ProdIssues/deleteMatTaskBasedIssue?cid=1&pid=71&rid=6&stid=2557&dtFr=2019-04-08&dtTo=2019-04-08
        [Route("api/ProdIssues/deleteMatTaskBasedIssue")]
        public async Task<HttpResponseMessage> deleteMatTaskBasedIssue( int cid, int pid,int rid,int stid, DateTime dtFr, DateTime dtTo) {
            try {  
                string  result = await Task.Run(() => prodIssuesDAL.deleteMatTaskBasedIssuesAsync(cid, pid,rid,stid, dtFr, dtTo));
                return Request.CreateResponse(HttpStatusCode.OK, result);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
       
    }
}
//this.gridOptions.columnDefs = [//{ headerName: 'Id', field: 'id', width: 70, checkboxSelection: true, cellStyle: { textAlign: 'right' } },//{ headerName: 'PlID', field: 'plID', width: 70, cellStyle: { textAlign: 'right' } },//{ headerName: 'Rbid', field: 'rbid', width: 70, cellStyle: { textAlign: 'right' } },//{ headerName: 'RID', field: 'rID', width: 70, cellStyle: { textAlign: 'right' } },//{ headerName: 'Res', field: 'Res', width: 200, cellStyle: { textAlign: 'left' } },//{ headerName: 'Uom', field: 'uom', width: 70, cellStyle: { textAlign: 'right' } },//{ headerName: 'RUnit', field: 'rUnit', width: 200, cellStyle: { textAlign: 'left' } },//{ headerName: 'PcID', field: 'pcID', width: 70, cellStyle: { textAlign: 'right' } },//{ headerName: 'RdQty', field: 'rdQty', width: 70, cellStyle: { textAlign: 'right' } },//{ headerName: 'Stg', field: 'stg', width: 70, cellStyle: { textAlign: 'right' } },//{ headerName: 'Stid', field: 'stid', width: 70, cellStyle: { textAlign: 'right' } },//{ headerName: 'Date', field: 'date', width: 100, cellStyle: { textAlign: 'center' } },//{ headerName: 'AsgQty', field: 'asgQty', width: 70, cellStyle: { textAlign: 'right' } },//{ headerName: 'CumIQty', field: 'cumIQty', width: 70, cellStyle: { textAlign: 'right' } },//{ headerName: 'ExQty', field: 'exQty', width: 70, cellStyle: { textAlign: 'right' } },//{ headerName: 'TBPI', field: 'TBPI', width: 70, cellStyle: { textAlign: 'right' } },//{ headerName: 'IssID', field: 'IssID', width: 70, cellStyle: { textAlign: 'right' } },//{ headerName: 'SJID', field: 'SJID', width: 70, cellStyle: { textAlign: 'right' } },//{ headerName: 'AsgID', field: 'AsgID', width: 70, cellStyle: { textAlign: 'right' } },//];// export interface productionIssues {//	id: number; //	plID: number; //	rbid: number; //	rID: number; //	Res: string; //	uom: number; //	rUnit: string; //	pcID: number; //	rdQty: number; //	stg: number; //	stid: number; //	date: Date; //	asgQty: number; //	cumIQty: number; //	exQty: number; //	TBPI: number; //	IssID: number; //	SJID: number; //	AsgID: number; // }


