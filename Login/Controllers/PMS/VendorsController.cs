﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Login.Controllers.PMS {
    public class VendorsController :ApiController {
        [HttpGet]
        [Route("api/vendors")]
        public HttpResponseMessage getVendorDetails(int cid) {
            try {          //http://localhost/Login/api/vendors?cid=1
                List<VendorDetails> clVendorDetails = new List<VendorDetails>();
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    con.Open();
                    SqlCommand cmd = new SqlCommand(" select t1.* from VendorDetails t1 where  cid = @cid", con);
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    using(SqlDataReader rdr = cmd.ExecuteReader()) {
                        while(rdr.Read()) {
                            VendorDetails t = new VendorDetails();
                            t.vendor_id = rdr["vendor_id"] == DBNull.Value ? -1 : (int)rdr["vendor_id"];
                            t.vendor_name = rdr["vendor_name"] == DBNull.Value ? "" : rdr["vendor_name"].ToString();
                            t.vendor_address = rdr["vendor_address"] == DBNull.Value ? "" : rdr["vendor_address"].ToString();
                            t.contact_person = rdr["contact_person"] == DBNull.Value ? "" : rdr["contact_person"].ToString();
                            t.mobile = rdr["mobile"] == DBNull.Value ? "" : rdr["mobile"].ToString();
                            t.phone = rdr["phone"] == DBNull.Value ? "" : rdr["phone"].ToString();
                            t.Fax = rdr["Fax"] == DBNull.Value ? "" : rdr["Fax"].ToString();
                            t.Email = rdr["Email"] == DBNull.Value ? "" : rdr["Email"].ToString();
                            t.BankID = rdr["BankID"] == DBNull.Value ? -1 : (int)rdr["BankID"];
                            t.BranchName = rdr["BranchName"] == DBNull.Value ? "" : rdr["BranchName"].ToString();
                            t.bankACCNo = rdr["bankACCNo"] == DBNull.Value ? "" : rdr["bankACCNo"].ToString();
                            t.IFSC = rdr["IFSC"] == DBNull.Value ? "" : rdr["IFSC"].ToString();
                            t.TINNO = rdr["TINNO"] == DBNull.Value ? "" : rdr["TINNO"].ToString();
                            t.UserName = rdr["UserName"] == DBNull.Value ? "" : rdr["UserName"].ToString();
                            t.Password = rdr["Password"] == DBNull.Value ? "" : rdr["Password"].ToString();
                            t.status = rdr["status"] == DBNull.Value ? -1 : (int)rdr["status"];
                            t.Createdby = rdr["Createdby"] == DBNull.Value ? -1 : (int)rdr["Createdby"];
                            t.Createdon = rdr["Createdon"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["Createdon"]);
                            t.modifiedby = rdr["modifiedby"] == DBNull.Value ? -1 : (int)rdr["modifiedby"];
                            t.modifiedon = rdr["modifiedon"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["modifiedon"]);
                            t.LedgerID = rdr["LedgerID"] == DBNull.Value ? -1 : (int)rdr["LedgerID"];
                            t.StateId = rdr["StateId"] == DBNull.Value ? -1 : (int)rdr["StateId"];
                            t.IsActive = rdr["IsActive"] == DBNull.Value ? true : (bool)rdr["IsActive"];
                            t.PayableACName = rdr["PayableACName"] == DBNull.Value ? "" : rdr["PayableACName"].ToString();
                            t.PANCardNo = rdr["PANCardNo"] == DBNull.Value ? "" : rdr["PANCardNo"].ToString();
                            t.DebitLimit = rdr["DebitLimit"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["DebitLimit"]);
                            t.CreditLimit = rdr["CreditLimit"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["CreditLimit"]);
                            t.ShortCode = rdr["ShortCode"] == DBNull.Value ? "" : rdr["ShortCode"].ToString();
                            t.BranchId = rdr["BranchId"] == DBNull.Value ? -1 : (int)rdr["BranchId"];
                            t.VendorNature = rdr["VendorNature"] == DBNull.Value ? -1 : (int)rdr["VendorNature"];
                            t.CurrecyID = rdr["CurrecyID"] == DBNull.Value ? -1 : (int)rdr["CurrecyID"];
                            t.Nature = rdr["Nature"] == DBNull.Value ? -1 : (byte)rdr["Nature"];
                            t.PrimaryNature = rdr["PrimaryNature"] == DBNull.Value ? -1 : (byte)rdr["PrimaryNature"];
                            t.CreditDays = rdr["CreditDays"] == DBNull.Value ? -1 : (int)rdr["CreditDays"];
                            t.POBox = rdr["POBox"] == DBNull.Value ? "" : rdr["POBox"].ToString();
                            t.ZIPCode = rdr["ZIPCode"] == DBNull.Value ? "" : rdr["ZIPCode"].ToString();
                            t.HardCopy = rdr["HardCopy"] == DBNull.Value ? "" : rdr["HardCopy"].ToString();
                            t.cid = rdr["cid"] == DBNull.Value ? -1 : (int)rdr["cid"];
                            clVendorDetails.Add(t);
                        }
                    }
                    con.Close();
                    return Request.CreateResponse(HttpStatusCode.OK, clVendorDetails);
                }
            } catch(Exception ex) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message); }
        }        [HttpPost]
        [Route("api/vendors")]
        public HttpResponseMessage postBulkVendorDetails([FromBody] List<VendorDetails> clVendorDetails, int cid, int userID) {
            if(clVendorDetails.Count > 0) {
                try {          //http://localhost/Login/api/vendors?cid=1&userID=1
                               // [{  vendor_id: 0, vendor_name: 'xxSiddh Contractors', vendor_address: 'xxSiddh Contractors', contact_person: 'John Doe', mobile: '9999999999'
                               //, phone: '9999999999', Fax: '', Email: 'keerthi@aeclogic.com', BankID: 1, bankACCNo: '1234567890000'
                               //, TINNO: '1234567890', UserName: 'Siddh', status: 1, LedgerID: 0, StateId: 1, IsActive: 1, PayableACName: 'Siddh', PANCardNo: 'Siddh123456789'
                               //, DebitLimit: 10000, CreditLimit:11110, ShortCode: 'c1234', BranchId: 1, VendorNature: 1, CurrecyID: 1, Nature: 1, PrimaryNature: 1
                               //, CreditDays: 01, POBox: '1234567', ZIPCode: '123' }]
                    using(SqlConnection con = new SqlConnection(common.erpCS)) {
                        con.Open();
                        using(SqlTransaction tran = con.BeginTransaction()) {
                            VendorDetails t = new VendorDetails();
                            using(SqlBulkCopy bulkCopy = new SqlBulkCopy(con, SqlBulkCopyOptions.Default, tran)) {
                                DataTable dtClass = t.dtClass("dtClass");
                                int i = 0; //CONSIDER CHANGING THE AUTOINCREMENTING TABLE ID FIELD TO THIS VARIABLE
                                foreach(VendorDetails E in clVendorDetails) {
                                    DataRow dr = dtClass.NewRow();
                                    dr["vendor_id"] = i++;
                                    dr["vendor_name"] = E.vendor_name;
                                    dr["vendor_address"] = E.vendor_address;
                                    dr["contact_person"] = E.contact_person;
                                    dr["mobile"] = E.mobile;
                                    dr["phone"] = E.phone;
                                    dr["Fax"] = E.Fax;
                                    dr["Email"] = E.Email;
                                    dr["BankID"] = E.BankID;
                                    dr["BranchName"] = DBNull.Value;
                                    dr["bankACCNo"] = E.bankACCNo;
                                    dr["IFSC"] = DBNull.Value;
                                    dr["TINNO"] = E.TINNO;
                                    dr["UserName"] = E.UserName;
                                    dr["Password"] = E.UserName;
                                    dr["status"] = E.status;
                                    dr["Createdby"] = userID;
                                    dr["Createdon"] = DateTime.Today;
                                    dr["modifiedby"] = DBNull.Value;
                                    dr["modifiedon"] =DBNull.Value;
                                    dr["LedgerID"] = DBNull.Value;
                                    dr["StateId"] = E.StateId;
                                    dr["IsActive"] = E.IsActive;
                                    dr["PayableACName"] = E.PayableACName;
                                    dr["PANCardNo"] = E.PANCardNo;
                                    dr["DebitLimit"] = E.DebitLimit;
                                    dr["CreditLimit"] = E.CreditLimit;
                                    dr["ShortCode"] = E.ShortCode;
                                    dr["BranchId"] = E.BranchId;
                                    dr["VendorNature"] = E.VendorNature;
                                    dr["CurrecyID"] = E.CurrecyID;
                                    dr["Nature"] = E.Nature;
                                    dr["PrimaryNature"] = E.PrimaryNature;
                                    dr["CreditDays"] = E.CreditDays;
                                    dr["POBox"] = E.POBox;
                                    dr["ZIPCode"] = E.ZIPCode;
                                    dr["HardCopy"] = DBNull.Value;
                                    dr["cid"] = cid;
                                    dtClass.Rows.Add(dr);
                                }
                                bulkCopy.DestinationTableName = "[dbo].[VendorDetails]";
                                bulkCopy.BulkCopyTimeout = 30000;
                                try {
                                    bulkCopy.WriteToServer(dtClass);
                                    tran.Commit();
                                    con.Close();
                                    return Request.CreateResponse(HttpStatusCode.OK, clVendorDetails.Count.ToString() + " record(s) inserted!");
                                } catch(Exception ex) {
                                    tran.Rollback();
                                    con.Close();
                                    return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                                }
                            }
                        }
                    }
                } catch(Exception ex) { return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message); }
            } else return Request.CreateResponse(HttpStatusCode.BadRequest, "Nothing to INSERT!");
        }
    }
    public class VendorDetails {
        public int vendor_id { get; set; }
        public string vendor_name { get; set; }
        public string vendor_address { get; set; }
        public string contact_person { get; set; }
        public string mobile { get; set; }
        public string phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public int BankID { get; set; }
        public string BranchName { get; set; }
        public string bankACCNo { get; set; }
        public string IFSC { get; set; }
        public string TINNO { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int status { get; set; }
        public int Createdby { get; set; }
        public DateTime Createdon { get; set; }
        public int modifiedby { get; set; }
        public DateTime modifiedon { get; set; }
        public int LedgerID { get; set; }
        public int StateId { get; set; }
        public bool IsActive { get; set; }
        public string PayableACName { get; set; }
        public string PANCardNo { get; set; }
        public decimal DebitLimit { get; set; }
        public decimal CreditLimit { get; set; }
        public string ShortCode { get; set; }
        public int BranchId { get; set; }
        public int VendorNature { get; set; }
        public int CurrecyID { get; set; }
        public int Nature { get; set; }
        public int PrimaryNature { get; set; }
        public int CreditDays { get; set; }
        public string POBox { get; set; }
        public string ZIPCode { get; set; }
        public string HardCopy { get; set; }
        public int cid { get; set; }

        public VendorDetails() { }
        public VendorDetails(int _vendor_id, string _vendor_name, string _vendor_address, string _contact_person, string _mobile
            , string _phone, string _Fax, string _Email, int _BankID, string _BranchName, string _bankACCNo, string _IFSC, string _TINNO
            , string _UserName, string _Password, int _status, int _Createdby, DateTime _Createdon, int _modifiedby, DateTime _modifiedon
            , int _LedgerID, int _StateId, bool _IsActive, string _PayableACName, string _PANCardNo, decimal _DebitLimit, decimal _CreditLimit
            , string _ShortCode, int _BranchId, int _VendorNature, int _CurrecyID, byte _Nature, byte _PrimaryNature, int _CreditDays
            , string _POBox, string _ZIPCode, string _HardCopy, int _cid) {
            vendor_id = _vendor_id; vendor_name = _vendor_name; vendor_address = _vendor_address; contact_person = _contact_person;
            mobile = _mobile; phone = _phone; Fax = _Fax; Email = _Email; BankID = _BankID; BranchName = _BranchName; bankACCNo = _bankACCNo;
            IFSC = _IFSC; TINNO = _TINNO; UserName = _UserName; Password = _Password; status = _status; Createdby = _Createdby;
            Createdon = _Createdon; modifiedby = _modifiedby; modifiedon = _modifiedon; LedgerID = _LedgerID; StateId = _StateId;
            IsActive = _IsActive; PayableACName = _PayableACName; PANCardNo = _PANCardNo; DebitLimit = _DebitLimit; CreditLimit = _CreditLimit;
            ShortCode = _ShortCode; BranchId = _BranchId; VendorNature = _VendorNature; CurrecyID = _CurrecyID; Nature = _Nature;
            PrimaryNature = _PrimaryNature; CreditDays = _CreditDays; POBox = _POBox; ZIPCode = _ZIPCode; HardCopy = _HardCopy; cid = _cid;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            DataColumn vendor_id = new DataColumn();
            vendor_id.DataType = System.Type.GetType("System.Int32");
            vendor_id.ColumnName = "vendor_id";
            vendor_id.AutoIncrement = true;
            dtName.Columns.Add(vendor_id);
            dtName.Columns.Add("vendor_name", typeof(System.String));
            dtName.Columns.Add("vendor_address", typeof(System.String));
            dtName.Columns.Add("contact_person", typeof(System.String));
            dtName.Columns.Add("mobile", typeof(System.String));
            dtName.Columns.Add("phone", typeof(System.String));
            dtName.Columns.Add("Fax", typeof(System.String));
            dtName.Columns.Add("Email", typeof(System.String));
            dtName.Columns.Add("BankID", typeof(System.Int32));
            dtName.Columns.Add("BranchName", typeof(System.String));
            dtName.Columns.Add("bankACCNo", typeof(System.String));
            dtName.Columns.Add("IFSC", typeof(System.String));
            dtName.Columns.Add("TINNO", typeof(System.String));
            dtName.Columns.Add("UserName", typeof(System.String));
            dtName.Columns.Add("Password", typeof(System.String));
            dtName.Columns.Add("status", typeof(System.Int32));
            dtName.Columns.Add("Createdby", typeof(System.Int32));
            dtName.Columns.Add("Createdon", typeof(System.DateTime));
            dtName.Columns.Add("modifiedby", typeof(System.Int32));
            dtName.Columns.Add("modifiedon", typeof(System.DateTime));
            dtName.Columns.Add("LedgerID", typeof(System.Int32));
            dtName.Columns.Add("StateId", typeof(System.Int32));
            dtName.Columns.Add("IsActive", typeof(System.Boolean));
            dtName.Columns.Add("PayableACName", typeof(System.String));
            dtName.Columns.Add("PANCardNo", typeof(System.String));
            dtName.Columns.Add("DebitLimit", typeof(System.Double));
            dtName.Columns.Add("CreditLimit", typeof(System.Double));
            dtName.Columns.Add("ShortCode", typeof(System.String));
            dtName.Columns.Add("BranchId", typeof(System.Int32));
            dtName.Columns.Add("VendorNature", typeof(System.Int32));
            dtName.Columns.Add("CurrecyID", typeof(System.Int32));
            dtName.Columns.Add("Nature", typeof(System.Byte));
            dtName.Columns.Add("PrimaryNature", typeof(System.Byte));
            dtName.Columns.Add("CreditDays", typeof(System.Int32));
            dtName.Columns.Add("POBox", typeof(System.String));
            dtName.Columns.Add("ZIPCode", typeof(System.String));
            dtName.Columns.Add("HardCopy", typeof(System.String));
            dtName.Columns.Add("cid", typeof(System.Int32));

            DataColumn[] keys = new DataColumn[1];
            keys[0] = vendor_id;
            dtName.PrimaryKey = keys;
            return dtName;
        }
        //export interface VendorDetails {        //	vendor_id: number;         //	vendor_name: string;         //	vendor_address: string;         //	contact_person: string;         //	mobile: string;         //	phone: string;         //	Fax: string;         //	Email: string;         //	BankID: number;         //	BranchName: string;         //	bankACCNo: string;         //	IFSC: string;         //	TINNO: string;         //	UserName: string;         //	Password: string;         //	status: number;         //	Createdby: number;         //	Createdon: Date;         //	modifiedby: number;         //	modifiedon: Date;         //	LedgerID: number;         //	StateId: number;         //	IsActive: number;         //	PayableACName: string;         //	PANCardNo: string;         //	DebitLimit: number;         //	CreditLimit: number;         //	ShortCode: string;         //	BranchId: number;         //	VendorNature: number;         //	CurrecyID: number;         //	Nature: number;         //	PrimaryNature: number;         //	CreditDays: number;         //	POBox: string;         //	ZIPCode: string;         //	HardCopy: string;         //	cid: number;         // }

    }
}
