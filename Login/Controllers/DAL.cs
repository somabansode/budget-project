﻿using Login.Controllers.HMS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
// https://docs.microsoft.com/en-us/dotnet/framework/data/adonet/asynchronous-programming
namespace Login.Controllers {
    public static class DAL {
        public static async Task<List<t_g_countryMaster>> ReadCountriesAsync() {
            List<t_g_countryMaster> clCountries = new List<t_g_countryMaster>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                using(SqlCommand command = new SqlCommand("select * from  t_g_countryMaster where Bitsstatus = 1", con)) {
                    // IAsyncResult result = command.BeginExecuteReader();
                    using(SqlDataReader rdr = await command.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            t_g_countryMaster t = new t_g_countryMaster((int)rdr["CountryID"], rdr["CountryName"].ToString()
                                , (int)rdr["Bitsstatus"], rdr["Nationality"].ToString(), rdr["Nationality"].ToString());
                            clCountries.Add(t);
                        }
                    }
                }
                con.Close();
            }
            return clCountries;
        }
        public static async Task<List<t_g_currencyMaster>> ReadCurrenciesAsync() {
            List<t_g_currencyMaster> clCurrencies = new List<t_g_currencyMaster>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                using(SqlCommand command = new SqlCommand("select * from  t_g_currencyMaster where IsActive = 1", con)) {
                    using(SqlDataReader rdr = await command.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            t_g_currencyMaster t = new t_g_currencyMaster((int)rdr["CurrencyID"], rdr["CurrencyName"].ToString()
                                , rdr["Currency"] == DBNull.Value ? "" : rdr["Currency"].ToString() //rdr["Currency"].ToString(),
                                , rdr["Symbol"] == DBNull.Value ? "" : rdr["Symbol"].ToString() //, rdr["Symbol"].ToString()
                                , rdr["AlowDecimalPlaces"] == DBNull.Value ? -1 : (byte)rdr["AlowDecimalPlaces"] //(byte)rdr["AlowDecimalPlaces"]
                                , (bool)rdr["IsActive"]);
                            clCurrencies.Add(t);
                        }
                    }
                }
                con.Close();
            }
            return clCurrencies;
        }
        public static async Task<List<t_g_countryInfo>> ReadCountryInfoAsync() {
            List<t_g_countryInfo> clcountryInfo = new List<t_g_countryInfo>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                using(SqlCommand command = new SqlCommand("select * from  t_g_countryInfo", con)) {
                    using(SqlDataReader rdr = await command.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            t_g_countryInfo t = new t_g_countryInfo((int)rdr["CountryID"]
                                , rdr["CallingCode"] == DBNull.Value ? "" : rdr["CallingCode"].ToString() // rdr["CallingCode"].ToString(),
                                , rdr["CurrencyId"] == DBNull.Value ? -1 : (int)rdr["CurrencyId"]
                                , rdr["Code2"] == DBNull.Value ? "" : rdr["Code2"].ToString()
                                , rdr["Code3"] == DBNull.Value ? "" : rdr["Code3"].ToString());
                            clcountryInfo.Add(t);
                        }
                    }
                }
                con.Close();
            }
            return clcountryInfo;
        }
        public static async Task<List<t_g_stateMaster>> ReadStatesAsync(int countryID) {
            List<t_g_stateMaster> clstates = new List<t_g_stateMaster>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                using(SqlCommand command = new SqlCommand("select StateID, StateName, CountryID, Bitstatus  from  " +
                    "t_g_stateMaster where Bitstatus = 1 and countryID = " + countryID, con)) {
                    using(SqlDataReader rdr = await command.ExecuteReaderAsync()) {
                        //SqlDataReader rdr = SqlHelper.ExecuteReader(common.erpCS, CommandType.Text, "select StateID, StateName, CountryID, Bitstatus  from  t_g_stateMaster where Bitstatus = 1 and countryID = " + countryID);
                        while(await rdr.ReadAsync()) {
                            t_g_stateMaster t = new t_g_stateMaster((int)rdr["StateID"]
                               , rdr["CountryID"] == DBNull.Value ? -1 : (int)rdr["CountryID"]
                               , rdr["StateName"] == DBNull.Value ? "" : rdr["StateName"].ToString()
                               , rdr["Bitstatus"] == DBNull.Value ? -1 : (int)rdr["Bitstatus"]);
                            clstates.Add(t);
                        }
                    }
                }
                con.Close();
            }
            return clstates;
        }
        public static async Task<List<t_g_cityMaster>> ReadCitiesAsync(int stateID) {
            List<t_g_cityMaster> clCities = new List<t_g_cityMaster>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                using(SqlCommand command = new SqlCommand("select CityID, CityName, StateId, status  " +
                    "from  t_g_cityMaster where status = 1 and stateID = " + stateID, con)) {
                    using(SqlDataReader rdr = await command.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            t_g_cityMaster t = new t_g_cityMaster((int)rdr["CityID"]
                               , rdr["CityName"] == DBNull.Value ? "" : rdr["CityName"].ToString()
                               , rdr["status"] == DBNull.Value ? -1 : (int)rdr["status"]
                               , rdr["StateId"] == DBNull.Value ? -1 : (int)rdr["StateId"]);
                            clCities.Add(t);
                        }
                    }
                }
                con.Close();
            }
            return clCities;
        }
        //-------------------
        public static async Task<List<th_worksitelatLng>> ReadWorksitelatLngAsync(int cid) {
            List<th_worksitelatLng> clth_worksitelatLng = new List<th_worksitelatLng>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "select t1.*  from th_worksitelatLng t1 join worksite t2 on t1.id = t2.site_id where t2.CompanyID = @cid";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    //cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            th_worksitelatLng t = new th_worksitelatLng();
                            t.id = rdr["id"] == DBNull.Value ? -1 : (int)rdr["id"];
                            t.lat = rdr["lat"] == DBNull.Value ? 0 : Convert.ToDouble(rdr["lat"]);
                            t.lng = rdr["lng"] == DBNull.Value ? 0 : Convert.ToDouble(rdr["lng"]);
                            clth_worksitelatLng.Add(t);
                        }
                    }
                }
                con.Close();
                return clth_worksitelatLng;
            }
        }
        public static async Task<string> CreateWorksitelatLngAsync(List<th_worksitelatLng> clWorksitelatLngs, int cid) {
            try {
                List<th_worksitelatLng> clRead = new List<th_worksitelatLng>();
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    //changes
                    string queryString = "select t1.*  from th_worksitelatLng t1 join worksite t2 on t1.id = t2.site_id " +
                        "where t2.CompanyID = " + @cid + " order by t2.site_id";
                    using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                        cmd.Parameters.Add(new SqlParameter("@cid", cid));
                        using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                            while(await rdr.ReadAsync()) {
                                th_worksitelatLng t = new th_worksitelatLng();
                                t.id = rdr["id"] == DBNull.Value ? -1 : (int)rdr["id"];
                                t.lat = rdr["lat"] == DBNull.Value ? 0 : Convert.ToDouble(rdr["lat"]);
                                t.lng = rdr["lng"] == DBNull.Value ? 0 : Convert.ToDouble(rdr["lng"]);
                                clRead.Add(t);
                            }
                        }
                    }
                    var ForUpdate = (from t1 in clRead join t2 in clWorksitelatLngs on t1.id equals t2.id select t2).ToList();
                    var uIDs = ForUpdate.Select(t => t.id).ToList();
                    var ForInsert = (from t in clWorksitelatLngs where !uIDs.Contains(t.id) select t).ToList();
                    int updatedRecords = 0;
                    if(ForUpdate.Count > 0) {
                        th_worksitelatLng tbl2 = new th_worksitelatLng();
                        DataTable dtClass2 = tbl2.dtClass("dtClass2");
                        foreach(th_worksitelatLng E in ForUpdate) {
                            DataRow dr = dtClass2.NewRow();
                            dr["id"] = E.id;
                            dr["lat"] = E.lat;
                            dr["lng"] = E.lng;
                            dtClass2.Rows.Add(dr);
                        }
                        using(SqlCommand cmd = new SqlCommand("sh_worksitelatLngBulkUpdate", con)) {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@cid", cid));
                            cmd.Parameters.Add(new SqlParameter("@dt", dtClass2));
                            updatedRecords = await cmd.ExecuteNonQueryAsync();
                        }
                    }
                    if(ForInsert.Count == 0) return updatedRecords.ToString() + " record(s) updated!";
                    else {
                        if(ForInsert.Count > 0) {
                            SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("worksitelatLng"));
                            th_worksitelatLng tbl = new th_worksitelatLng();
                            DataTable dtClass = tbl.dtClass("dtClass");
                            foreach(th_worksitelatLng E in ForInsert) {
                                DataRow dr = dtClass.NewRow();
                                dr["id"] = E.id;
                                dr["lat"] = E.lat;
                                dr["lng"] = E.lng;
                                dtClass.Rows.Add(dr);
                            }
                            if(dtClass.Rows.Count > 0) {
                                using(SqlBulkCopy bcp = new SqlBulkCopy(con, SqlBulkCopyOptions.Default, tran)) {
                                    bcp.DestinationTableName = "[dbo].[th_worksitelatLng]";
                                    try {
                                        await bcp.WriteToServerAsync(dtClass);
                                        tran.Commit();
                                        con.Close();
                                        return (updatedRecords > 1 ? updatedRecords.ToString() + " record(s) updated!" : "") + "\r" +
                                            (ForInsert.Count > 0 ? ForInsert.Count.ToString() + " record(s) inserted!" : "");
                                    } catch(Exception ex) {
                                        tran.Rollback();
                                        con.Close();
                                        return ex.Message;
                                    }
                                }
                            }
                            return (updatedRecords > 1 ? updatedRecords.ToString() + " record(s) updated!" : "");
                        } else {
                            return "Nothing to update or insert!";
                        }
                    }
                }
            } catch(Exception ex) { return ex.Message; }
        }
        public static async Task<worksite> CreateWorksitesAsync(ws WS) {
            try {
                using(SqlConnection conn = new SqlConnection(common.erpCS)) {
                    await conn.OpenAsync();
                    using(SqlCommand cmd = new SqlCommand("sh_AddWorkSite", conn)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlParameter[] sps = new SqlParameter[5];
                        sps[0] = new SqlParameter("@WorkSite", WS.WorkSite);
                        sps[1] = new SqlParameter("@Address", WS.Address);
                        sps[2] = new SqlParameter("@STATEID", WS.STATEID);
                        sps[3] = new SqlParameter("@CompanyID", WS.cid);
                        sps[4] = new SqlParameter("@CityID", WS.CityID);
                        cmd.Parameters.AddRange(sps);
                        DataSet ds = new DataSet();
                        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                        adapter.Fill(ds);
                        if(ds.Tables[0].Rows.Count > 0) {
                            DataRow dr = ds.Tables[0].Rows[0];
                            worksite t = new worksite(
                                    (int)dr["Site_ID"]
                                    , dr["Site_Name"].ToString()
                                    , dr["Site_Address"].ToString()
                                    , dr["WSStatus"].ToString()
                                    , (int)dr["PreOrder"]
                                    , (int)dr["STATEID"]
                                    , (int)dr["LedgerID"]
                                    , (int)dr["PMLedgerID"]
                                    , (int)dr["CityID"]
                                    , (int)dr["CompanyID"]
                                );
                            return t;
                        } else return null;
                    }
                }
            } catch { return null; }
        }
        public static async Task<List<worksite>> ReadWorksitesAsync(int cid) {
            List<worksite> list = new List<worksite>();
            using(SqlConnection conn = new SqlConnection(common.erpCS)) {
                await conn.OpenAsync();
                DataSet ds = new DataSet();
                using(SqlCommand cmd = new SqlCommand("sh_getWorkSitesOrderByID", conn)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@cid", cid);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(ds);
                    foreach(DataRow dr in ds.Tables[0].Rows) {
                        worksite t = new worksite(
                            (int)dr["Site_ID"]
                            , dr["Site_Name"].ToString()
                            , dr["Site_Address"].ToString()
                            , dr["WSStatus"].ToString()
                            , (int)dr["PreOrder"]
                            , (int)dr["STATEID"]
                            , (int)dr["LedgerID"]
                            , (int)dr["PMLedgerID"]
                            , (int)dr["CityID"]
                            , (int)dr["CompanyID"]
                            );
                        list.Add(t);
                    }
                }
                conn.Close();
            }
            return list;
        }
        public static async Task<string> UpdateWorksiteAsync(int id, ws WS) {
            try {
                using(SqlConnection conn = new SqlConnection(common.erpCS)) {
                    await conn.OpenAsync();
                    int recordsUpdated = 0;
                    using(SqlCommand cmd = new SqlCommand("sh_updateWorksite", conn)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlParameter[] sps = new SqlParameter[6];
                        sps[0] = new SqlParameter("@WorkSite", WS.WorkSite);
                        sps[1] = new SqlParameter("@Address", WS.Address);
                        sps[2] = new SqlParameter("@STATEID", WS.STATEID);
                        sps[3] = new SqlParameter("@CompanyID", WS.cid);
                        sps[4] = new SqlParameter("@CityID", WS.CityID);
                        sps[5] = new SqlParameter("@siteid", id);
                        cmd.Parameters.AddRange(sps);
                        recordsUpdated = await cmd.ExecuteNonQueryAsync();
                    }
                    conn.Close();
                    if(recordsUpdated >= 1) return "success"; else return "failure";  // there is a chance for updation of one or more tables -- worksite and t_a_ledgers
                }
            } catch(Exception ex) { return ex.Message; }
        }
        public static async Task<string> DeleteWorksiteAsync(int id) {
            try {
                using(SqlConnection conn = new SqlConnection(common.erpCS)) {
                    await conn.OpenAsync();
                    int recordsUpdated = 0;
                    using(SqlCommand cmd = new SqlCommand("sh_deleteWorkSite", conn)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlParameter[] sps = new SqlParameter[2];
                        sps[0] = new SqlParameter("@siteid", id);
                        sps[1] = new SqlParameter("@status", false);
                        cmd.Parameters.AddRange(sps);
                        recordsUpdated = await cmd.ExecuteNonQueryAsync();
                    }
                    conn.Close();
                    if(recordsUpdated >= 1) return "success"; else return "failure";
                }
            } catch(Exception ex) { return ex.Message; }
        }
        //----------------
        public static async Task<List<EmployeeType>> ReadEemployeeTypeAsync(int cid) {
            List<EmployeeType> empType = new List<EmployeeType>();
            using(SqlConnection conn = new SqlConnection(common.erpCS)) {
                await conn.OpenAsync();
                DataSet ds = new DataSet();
                using(SqlCommand cmd = new SqlCommand("HR_GetEmployeeTypes", conn)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@cid", cid);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(ds);
                    foreach(DataRow dr in ds.Tables[0].Rows) {
                        EmployeeType t = new EmployeeType((int)dr["EmpTyID"], dr["EMpType"].ToString(), cid);
                        empType.Add(t);
                    }
                }
                conn.Close();
            }
            return empType;
        }
        public static async Task<List<T_G_CategoryMaster>> ReadEmployeeTradesAsync(int cid) {
            List<T_G_CategoryMaster> trades = new List<T_G_CategoryMaster>();
            using(SqlConnection conn = new SqlConnection(common.erpCS)) {
                await conn.OpenAsync();
                DataSet ds = new DataSet();
                using(SqlCommand cmd = new SqlCommand("HR_GetCategories", conn)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@cid", cid);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(ds);
                    foreach(DataRow dr in ds.Tables[0].Rows) {
                        T_G_CategoryMaster t = new T_G_CategoryMaster((Int16)dr["CateId"], dr["Category"].ToString(), (bool)dr["Status"], cid);
                        trades.Add(t);
                    }
                }
                conn.Close();
            }
            return trades;
        }
        public static async Task<List<T_G_DesignationMaster>> ReadDesignationsAsync(int cid) {
            List<T_G_DesignationMaster> clDesigs = new List<T_G_DesignationMaster>();
            using(SqlConnection conn = new SqlConnection(common.erpCS)) {
                await conn.OpenAsync();
                using(SqlCommand cmd = new SqlCommand("select [DesigId],[Designation] from T_G_DesignationMaster where  [Status] = 1 and [cid]  =" + cid, conn)) {
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            T_G_DesignationMaster t = new T_G_DesignationMaster((Int16)rdr["DesigId"], rdr["Designation"].ToString(), true, cid);
                            clDesigs.Add(t);
                        }
                    }
                }
                conn.Close();
                return clDesigs;
            }
        }
        public static async Task<List<T_G_DepartmentMaster>> ReadDepartmentAsync(int cid) {
            List<T_G_DepartmentMaster> clDeptts = new List<T_G_DepartmentMaster>();
            using(SqlConnection conn = new SqlConnection(common.erpCS)) {
                await conn.OpenAsync();
                using(SqlCommand cmd = new SqlCommand("select [DepartmentUId],[DepartmentName] from T_G_DepartmentMaster where [BitsStatus] = 1  and [cid] =" + cid, conn)) {
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            T_G_DepartmentMaster t = new T_G_DepartmentMaster((decimal)rdr["DepartmentUId"], rdr["DepartmentName"].ToString(), "1", cid);
                            clDeptts.Add(t);
                        }
                    }
                }
                conn.Close();
                return clDeptts;
            }
        }
        public static async Task<List<T_HR_NatureOfEmployment>> ReadNatureOfEmploymentAsync(int cid) {
            List<T_HR_NatureOfEmployment> clNature = new List<T_HR_NatureOfEmployment>();
            using(SqlConnection conn = new SqlConnection(common.erpCS)) {
                await conn.OpenAsync();
                string query = "select [NatureOfEmp],[Nature],[NoofWD], [WD1] from T_HR_NatureOfEmployment where [IsActive] = 1 and cid = " + cid;
                using(SqlCommand cmd = new SqlCommand(query, conn)) {
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            T_HR_NatureOfEmployment t = new T_HR_NatureOfEmployment((int)rdr["NatureOfEmp"], rdr["Nature"].ToString()
                                , rdr["NoofWD"].ToString(), true, rdr["WD1"] == DBNull.Value ? 0 : (byte)rdr["WD1"], cid);
                            clNature.Add(t);
                        }
                    }
                }
                conn.Close();
                return clNature;
            }
        }
        public static async Task<string> UpdateNatureOfEmploymentAsync(int NatureOfEmp, int value) {
            try {
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("blkUpdate"));
                    string query = "update T_HR_NatureOfEmployment set WD1 = " + value + " where NatureOfEmp = " + NatureOfEmp;
                    int recordsUpdated = 0;
                    using(SqlCommand cmd = new SqlCommand(query, con, tran)) {
                        recordsUpdated = await cmd.ExecuteNonQueryAsync();
                    }
                    if(recordsUpdated > 0) tran.Commit();
                    else return "failed to update!";
                    con.Close();
                    return "Value updated!";
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
        public static async Task<List<T_G_Shifts>> ReadShiftsAsync(int cid) {
            List<T_G_Shifts> clShifts = new List<T_G_Shifts>();
            using(SqlConnection conn = new SqlConnection(common.erpCS)) {
                await conn.OpenAsync();
                string query = "select [ShiftID],[Name],[InTime],[OutTime] from T_G_Shifts  where [IsActive] = 1 and cid = " + cid;
                using(SqlCommand cmd = new SqlCommand(query, conn)) {
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            T_G_Shifts t = new T_G_Shifts((int)rdr["ShiftID"], rdr["Name"].ToString(), rdr["InTime"].ToString(), rdr["OutTime"].ToString(), true, cid);
                            clShifts.Add(t);
                        }
                    }
                }
                conn.Close();
                return clShifts;
            }
        }
        public static async Task<List<T_G_BankMaster>> ReadBanksAsync() {
            List<T_G_BankMaster> clBanks = new List<T_G_BankMaster>();
            using(SqlConnection conn = new SqlConnection(common.erpCS)) {
                await conn.OpenAsync();
                string query = "select [BankId],[BankName] from T_G_BankMaster where [IsActive] = 1";
                using(SqlCommand cmd = new SqlCommand(query, conn)) {
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            T_G_BankMaster t = new T_G_BankMaster((int)rdr["BankId"], rdr["BankName"].ToString(), true);
                            clBanks.Add(t);
                        }
                    }
                }
                conn.Close();
                return clBanks;
            }
        }
        public static async Task<List<T_G_BankBranches>> xxxReadBankBranchesByBankAsync(int bankID) {
            List<T_G_BankBranches> clBranches = new List<T_G_BankBranches>();
            using(SqlConnection conn = new SqlConnection(common.erpCS)) {
                await conn.OpenAsync();
                string query = "sh_bankBranches" + bankID;
                using(SqlCommand cmd = new SqlCommand(query, conn)) {
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            T_G_BankBranches t = new T_G_BankBranches(
                             (int)rdr["BranchId"]
                             , bankID
                             , rdr["BranchName"] == DBNull.Value ? "" : rdr["BranchName"].ToString()
                             , rdr["SWIFT"] == DBNull.Value ? "" : rdr["SWIFT"].ToString()
                             , rdr["CityID"] == DBNull.Value ? -1 : (int)rdr["CityID"]
                             , rdr["StateID"] == DBNull.Value ? -1 : (int)rdr["StateID"]
                             , rdr["CountryID"] == DBNull.Value ? -1 : (int)rdr["CountryID"]
                             , true);
                            clBranches.Add(t);
                        }
                    }
                }
                conn.Close();
                return clBranches;
            }
        }
        public static async Task<List<BankBranches>> ReadAllBankBranchesAsync() {
            List<BankBranches> clBranches = new List<BankBranches>();
            using(SqlConnection conn = new SqlConnection(common.erpCS)) {
                await conn.OpenAsync();
                using(SqlCommand cmd = new SqlCommand("sh_bankBranches", conn)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            BankBranches t = new BankBranches(
                                (int)rdr["BranchId"]
                                , rdr["BranchName"] == DBNull.Value ? "" : rdr["BranchName"].ToString()
                                , rdr["BankID"] == DBNull.Value ? -1 : (int)rdr["BankID"]
                                , rdr["BankName"] == DBNull.Value ? "" : rdr["BankName"].ToString()
                                , rdr["SWIFT"] == DBNull.Value ? "" : rdr["SWIFT"].ToString());
                            clBranches.Add(t);
                        }
                    }
                }
                conn.Close();
                return clBranches;
            }
        }
        public static async Task<List<T_HR_AppDetails>> ReadEmpAppDetailsAsync(int cid) {
            List<T_HR_AppDetails> clAppdets = new List<T_HR_AppDetails>();
            using(SqlConnection conn = new SqlConnection(common.erpCS)) {
                await conn.OpenAsync();
                DataSet ds = new DataSet();
                using(SqlCommand cmd = new SqlCommand("sh_AppDetails", conn)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@cid", cid);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(ds);
                    foreach(DataRow dr in ds.Tables[0].Rows) {
                        T_HR_AppDetails t = new T_HR_AppDetails(
                        cid,
                        dr["AppDetId"] == DBNull.Value ? -1 : (int)dr["AppDetId"],
                        dr["EmpId"] == DBNull.Value ? -1 : (int)dr["EmpId"],
                        dr["PerAddress"] == DBNull.Value ? "" : dr["PerAddress"].ToString(),
                        dr["PerPhone"] == DBNull.Value ? "" : dr["PerPhone"].ToString(),
                        dr["PerPin"] == DBNull.Value ? "" : dr["PerPin"].ToString(),
                        dr["SameAddress"] == DBNull.Value ? false : (bool)dr["SameAddress"],
                        dr["ResAddress"] == DBNull.Value ? "" : dr["ResAddress"].ToString(),
                        dr["ResPin"] == DBNull.Value ? "" : dr["ResPin"].ToString(),
                        dr["ResPhone"] == DBNull.Value ? "" : dr["ResPhone"].ToString(),
                        dr["Bloodgroup"] == DBNull.Value ? "" : dr["Bloodgroup"].ToString(),
                        dr["Salary"] == DBNull.Value ? -1 : (int)dr["Salary"],
                        dr["DOJ"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(dr["DOJ"]),
                        dr["permanentCountry"] == DBNull.Value ? -1 : (int)dr["permanentCountry"],
                        dr["permanentState"] == DBNull.Value ? -1 : (int)dr["permanentState"],
                        dr["permanentCity"] == DBNull.Value ? -1 : (int)dr["permanentCity"],
                        dr["ResidentCountry"] == DBNull.Value ? -1 : (int)dr["ResidentCountry"],
                        dr["ResidentState"] == DBNull.Value ? -1 : (int)dr["ResidentState"],
                        dr["ResidentCity"] == DBNull.Value ? -1 : (int)dr["ResidentCity"],
                        dr["PassportNo"] == DBNull.Value ? "" : dr["PassportNo"].ToString(),
                        dr["Issuer"] == DBNull.Value ? "" : dr["Issuer"].ToString(),
                        dr["IssuePlace"] == DBNull.Value ? "" : dr["IssuePlace"].ToString(),

                        dr["IssueDate"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(dr["IssueDate"]),
                        dr["ExpiryDate"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(dr["ExpiryDate"]),

                        dr["PasportRemarks"] == DBNull.Value ? "" : dr["PasportRemarks"].ToString(),
                        dr["PassportExt"] == DBNull.Value ? "" : dr["PassportExt"].ToString(),
                        dr["EmergContact"] == DBNull.Value ? "" : dr["EmergContact"].ToString(),
                        dr["EmergContactName"] == DBNull.Value ? "" : dr["EmergContactName"].ToString(),
                        dr["EmergRelation"] == DBNull.Value ? "" : dr["EmergRelation"].ToString(),
                        dr["EmergEmail"] == DBNull.Value ? "" : dr["EmergEmail"].ToString(),
                        dr["EmergResiPhone"] == DBNull.Value ? "" : dr["EmergResiPhone"].ToString(),

                        dr["Dateofmarriage"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(dr["Dateofmarriage"]),
                        dr["MaritalStat"] == DBNull.Value ? -1 : (int)dr["MaritalStat"],

                        dr["OREmergContact"] == DBNull.Value ? "" : dr["OREmergContact"].ToString(),
                        dr["OREmergContactName"] == DBNull.Value ? "" : dr["OREmergContactName"].ToString(),
                        dr["OREmergEmail"] == DBNull.Value ? "" : dr["OREmergEmail"].ToString(),
                        dr["OREmergRelation"] == DBNull.Value ? "" : dr["OREmergRelation"].ToString(),
                        dr["OREmergResiPhone"] == DBNull.Value ? "" : dr["OREmergResiPhone"].ToString(),
                        dr["EOC_date"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(dr["EOC_date"])
                        );
                        clAppdets.Add(t);
                    }
                }
                conn.Close();
            }
            return clAppdets;
        }
        public static async Task<string> CreateBulkEmpAppDetailsAsync(List<T_HR_AppDetails> clEmpAppDetails, int cid, int userID) {
            try {
                List<CityStateCountry> allCs = await Task.Run(() => DAL.clCityStatCountryAsync());
                using(SqlConnection conn = new SqlConnection(common.erpCS)) {
                    await conn.OpenAsync();
                    //SqlCommand cmd = conn.CreateCommand();  
                    SqlTransaction trans = null;
                    trans = await Task.Run<SqlTransaction>(() => conn.BeginTransaction("clEmpAppDetails"));// Start a local transaction.  
                    T_HR_AppDetails Appdet = new T_HR_AppDetails();
                    DataTable dtEmps = Appdet.dtAppDetails("dtAppDets");
                    int i = 0;
                    foreach(T_HR_AppDetails E in clEmpAppDetails) {
                        DataRow dr = dtEmps.NewRow();
                        dr["AppDetId"] = i++;
                        dr["EmpId"] = E.EmpId;
                        dr["PerAddress"] = E.PerAddress;
                        dr["PerPhone"] = E.PerPhone;
                        dr["PerPin"] = E.PerPin;
                        dr["SameAddress"] = E.SameAddress;
                        if(E.SameAddress == true) {
                            dr["ResAddress"] = E.PerAddress;
                            dr["ResPin"] = E.PerPhone;
                            dr["ResPhone"] = E.PerPin;
                        } else {
                            dr["ResAddress"] = E.ResAddress;
                            dr["ResPin"] = E.ResPin;
                            dr["ResPhone"] = E.ResPhone;
                        }
                        dr["Bloodgroup"] = E.Bloodgroup;
                        dr["Salary"] = E.Salary;
                        dr["DOJ"] = E.DOJ;
                        var pObj = (from t in allCs where t.CityID == E.permanentCity select t).FirstOrDefault();
                        dr["permanentCountry"] = pObj.CountryID;
                        dr["permanentState"] = pObj.StateID;
                        dr["permanentCity"] = E.permanentCity;
                        var rObj = (from t in allCs where t.CityID == E.ResidentCity select t).FirstOrDefault();
                        dr["ResidentCountry"] = rObj.CountryID;
                        dr["ResidentState"] = rObj.StateID;
                        dr["ResidentCity"] = E.ResidentCity;
                        dr["PassportNo"] = E.PassportNo;
                        dr["Issuer"] = E.Issuer;
                        dr["IssuePlace"] = E.IssuePlace;

                        dr["IssueDate"] = E.IssueDate;
                        dr["ExpiryDate"] = E.ExpiryDate;

                        dr["PasportRemarks"] = E.PasportRemarks;
                        dr["PassportExt"] = DBNull.Value;
                        dr["EmergContact"] = E.EmergContact;
                        dr["EmergContactName"] = E.EmergContactName;
                        dr["EmergRelation"] = E.EmergRelation;
                        dr["EmergEmail"] = E.EmergEmail;
                        dr["EmergResiPhone"] = E.EmergResiPhone;

                        dr["Dateofmarriage"] = E.Dateofmarriage;
                        dr["MaritalStat"] = E.MaritalStat;

                        dr["OREmergContact"] = E.OREmergContact;
                        dr["OREmergContactName"] = E.OREmergContactName;
                        dr["OREmergEmail"] = E.OREmergEmail;
                        dr["OREmergRelation"] = E.OREmergRelation;
                        dr["OREmergResiPhone"] = E.OREmergResiPhone;
                        dr["EOC_date"] = E.EOC_date;

                        dr["Contract_Yr"] = DBNull.Value;
                        dr["Contract_Month"] = DBNull.Value;
                        dr["Contract_days"] = DBNull.Value;
                        dr["PerCity"] = DBNull.Value;
                        dr["PerState"] = DBNull.Value;
                        dr["PerCountry"] = DBNull.Value;
                        dr["ResCity"] = DBNull.Value;
                        dr["ResState"] = DBNull.Value;
                        dr["ResCountry"] = DBNull.Value;
                        dr["PerDoorNo"] = DBNull.Value;
                        dr["PerBuilding"] = DBNull.Value;
                        dr["PerStreet"] = DBNull.Value;
                        dr["PerArea"] = DBNull.Value;
                        dr["ResDoorNo"] = DBNull.Value;
                        dr["ResBuilding"] = DBNull.Value;
                        dr["ResStreet"] = DBNull.Value;
                        dr["ResArea"] = DBNull.Value;
                        dtEmps.Rows.Add(dr);
                    }
                    using(SqlBulkCopy bcp = new SqlBulkCopy(conn, SqlBulkCopyOptions.Default, trans)) {
                        bcp.DestinationTableName = "[dbo].[T_HR_AppDetails]";
                        try { await bcp.WriteToServerAsync(dtEmps); } catch(Exception ex) { return ex.Message; }
                    }
                    if(await Task.Run(() => UpdateOtherTablesAsync(clEmpAppDetails, conn, trans, cid, userID))) {
                        await Task.Run(() => trans.Commit());
                        conn.Close();
                        return clEmpAppDetails.Count + " record(s) are inserted";
                    } else {
                        trans.Rollback();
                        conn.Close();
                        return "Falied to insert records!";
                    }

                }
            } catch(Exception ex) { return ex.Message; }
        }
        public static async Task<bool> UpdateOtherTablesAsync(List<T_HR_AppDetails> clEmpAppDetails, SqlConnection con, SqlTransaction tran, int cid, int userID) {
            try {
                using(SqlBulkCopy bulkCopy = new SqlBulkCopy(con, SqlBulkCopyOptions.Default, tran)) {
                    T_HR_EmpSal sal = new T_HR_EmpSal(); DataTable dtSal = sal.dtEmpSal("dtSal");
                    T_HR_EmpWages wages = new T_HR_EmpWages(); DataTable dtWages = wages.dtEmpWages("dtWages");
                    T_HR_EmpWagesCentage centage = new T_HR_EmpWagesCentage(); DataTable dtCentage = centage.dtEmpWagesCentage("dtCentage");
                    DataTable dtEmpIDs = new DataTable(); dtEmpIDs.Columns.Add("ID", typeof(System.Int32));
                    SqlCommand cmd = con.CreateCommand();
                    cmd.CommandText = "select top 1  WagesID from T_HR_Wages where cid=@cid";
                    cmd.Connection = con;
                    cmd.Transaction = tran;
                    // SqlCommand cmd = new SqlCommand("select top 1  WagesID from T_HR_Wages where cid=@cid", con, tran);
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    int wageid = (int)await cmd.ExecuteScalarAsync();//name of the basic master(BS)

                    int i = 0, j = 0, k = 0;

                    foreach(T_HR_AppDetails ad in clEmpAppDetails) {
                        if(ad.Salary > 0) {
                            DataRow drSal = dtSal.NewRow();
                            drSal["EmpSalID"] = i++;
                            drSal["EmpId"] = ad.EmpId;
                            drSal["Salary"] = decimal.Parse(ad.Salary.ToString());
                            drSal["FromDate"] = Convert.ToDateTime(ad.DOJ.Year.ToString() + "-" + ad.DOJ.Month.ToString() + "-01");
                            drSal["ToDate"] = DBNull.Value;
                            drSal["CreatedBY"] = userID;
                            drSal["UpdatedBY"] = DBNull.Value;
                            drSal["CreatedON"] = DateTime.Today;
                            drSal["UpdatedON"] = DBNull.Value;
                            dtSal.Rows.Add(drSal);

                            DataRow drEmpID = dtEmpIDs.NewRow();
                            drEmpID["ID"] = ad.EmpId;
                            dtEmpIDs.Rows.Add(drEmpID);
                        }
                        if(ad.Salary > 0) {
                            DataRow drWages = dtWages.NewRow();
                            drWages["EmpWageId"] = j++;
                            drWages["WagesID"] = wageid;
                            drWages["EmpId"] = ad.EmpId;
                            drWages["IsActive"] = true;
                            dtWages.Rows.Add(drWages);
                        }
                    }
                    if(dtSal.Rows.Count > 0) {
                        using(SqlBulkCopy bcp = new SqlBulkCopy(con, SqlBulkCopyOptions.Default, tran)) {
                            bcp.DestinationTableName = "[dbo].[T_HR_EmpSal]";
                            await bcp.WriteToServerAsync(dtSal);
                        }
                    }
                    if(dtWages.Rows.Count > 0) {
                        using(SqlBulkCopy bcp = new SqlBulkCopy(con, SqlBulkCopyOptions.Default, tran)) {
                            bcp.DestinationTableName = "[dbo].[T_HR_EmpWages]";
                            await bcp.WriteToServerAsync(dtWages);
                        }
                    }
                    List<twoIDs> empSalIDs = new List<twoIDs>();
                    SqlCommand cmd2 = con.CreateCommand();
                    cmd2.Connection = con;
                    cmd2.Transaction = tran;
                    cmd2.CommandText = "sh_empSalIDs";
                    //cmd2 = new SqlCommand("sh_empSalIDs", con, tran);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cmd2.Parameters.Add(new SqlParameter("@empIDs", dtEmpIDs));

                    using(SqlDataReader rdr = await cmd2.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            twoIDs t = new twoIDs();
                            t.empID = (int)rdr["empID"];
                            t.ID2 = (int)rdr["empSalID"];
                            empSalIDs.Add(t);
                        }
                    }
                    foreach(T_HR_AppDetails ad in clEmpAppDetails) {
                        if(ad.Salary > 0) {
                            DataRow drCentage = dtCentage.NewRow();
                            drCentage["CentageID"] = k++;
                            drCentage["WagesID"] = wageid;
                            drCentage["EmpId"] = ad.EmpId;
                            drCentage["Centage"] = 1.000000;
                            drCentage["IsActive"] = true;
                            drCentage["CreatedBy"] = userID;
                            drCentage["CreatedOn"] = DateTime.Today;
                            drCentage["UpdatedBy"] = DBNull.Value;
                            drCentage["UpdatedOn"] = DBNull.Value;
                            var salID = (from t in empSalIDs where t.empID == ad.EmpId select t.ID2).FirstOrDefault();
                            drCentage["EmpSalID"] = salID;
                            dtCentage.Rows.Add(drCentage);
                        }
                    }
                    if(dtCentage.Rows.Count > 0) {
                        using(SqlBulkCopy bcp = new SqlBulkCopy(con, SqlBulkCopyOptions.Default, tran)) {
                            bcp.DestinationTableName = "[dbo].[T_HR_EmpWagesCentage]";
                            await bcp.WriteToServerAsync(dtCentage);
                        }
                    }
                    return true;
                }

            } catch(Exception ex) { string error = ex.Message; return false; }
        }
        public static async Task<List<CityStateCountry>> clCityStatCountryAsync() {
            try {
                List<CityStateCountry> clCities = new List<CityStateCountry>();
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlCommand cmd = new SqlCommand("th_allCities", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            CityStateCountry t = new CityStateCountry(
                                (int)rdr["CityID"]
                                    , rdr["CityName"] == DBNull.Value ? "" : rdr["CityName"].ToString()
                                    , rdr["CountryID"] == DBNull.Value ? -1 : (int)rdr["CountryID"]
                                    , rdr["CountryName"] == DBNull.Value ? "" : rdr["CountryName"].ToString()
                                    , rdr["StateID"] == DBNull.Value ? -1 : (int)rdr["StateID"]
                                    , rdr["StateName"] == DBNull.Value ? "" : rdr["StateName"].ToString());
                            clCities.Add(t);
                        }
                    }
                    con.Close();
                    return clCities;
                }
            } catch { return null; }
        }
        public static async Task<List<StateCountry>> clStateCountryAsync() {
            try {
                List<StateCountry> clAllStates = new List<StateCountry>();
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlCommand cmd = new SqlCommand("th_allStates", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            StateCountry t = new StateCountry(
                                rdr["CountryID"] == DBNull.Value ? -1 : (int)rdr["CountryID"]
                                , rdr["CountryName"] == DBNull.Value ? "" : rdr["CountryName"].ToString()
                                , rdr["StateID"] == DBNull.Value ? -1 : (int)rdr["StateID"]
                                , rdr["StateName"] == DBNull.Value ? "" : rdr["StateName"].ToString());
                            clAllStates.Add(t);
                        }
                    }
                    con.Close();
                    return clAllStates;
                }
            } catch { return null; }
        }
        public static async Task<List<T_HR_Insurance>> ReadInsuranceAsync(int cid) {
            List<T_HR_Insurance> clInsurance = new List<T_HR_Insurance>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                SqlCommand cmd = new SqlCommand("select t1.* from T_HR_Insurance t1 join T_G_EmployeeMaster t2 on t1.EmpID = t2.EmpId where t2.cid = @cid", con);
                cmd.Parameters.Add(new SqlParameter("@cid", cid));
                using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                    while(await rdr.ReadAsync()) {
                        T_HR_Insurance t = new T_HR_Insurance();
                        t.ID = (int)rdr["ID"];
                        t.EmpID = (int)rdr["EmpID"];
                        t.PolicyNo = rdr["PolicyNo"] == DBNull.Value ? "" : rdr["PolicyNo"].ToString();
                        t.IssueDate = rdr["IssueDate"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["IssueDate"]);
                        t.MonthlyPremium = rdr["MonthlyPremium"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["MonthlyPremium"]);
                        t.CertificateNo = rdr["CertificateNo"] == DBNull.Value ? "" : rdr["CertificateNo"].ToString();
                        t.ExpiryDate = rdr["ExpiryDate"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["ExpiryDate"]);
                        t.Remarks = rdr["Remarks"] == DBNull.Value ? "" : rdr["Remarks"].ToString();
                        t.Ext = rdr["Ext"] == DBNull.Value ? "" : rdr["Ext"].ToString();
                        t.NameOfInsurer = rdr["NameOfInsurer"] == DBNull.Value ? "" : rdr["NameOfInsurer"].ToString();
                        clInsurance.Add(t);
                    }
                }
                con.Close();
            }
            return clInsurance;
        }
        public static async Task<string> BulkInsertInsuranceAsync(List<T_HR_Insurance> clInsurance) {
            using(SqlConnection conn = new SqlConnection(common.erpCS)) {
                await conn.OpenAsync();
                SqlTransaction trans = trans = await Task.Run<SqlTransaction>(() => conn.BeginTransaction("clInsurance"));
                T_HR_Insurance insureObj = new T_HR_Insurance();
                DataTable dtIns = insureObj.dtInsurance("dtInsure");
                int i = 0;
                foreach(T_HR_Insurance E in clInsurance) {
                    DataRow dr = dtIns.NewRow();
                    dr["ID"] = i++;
                    dr["EmpID"] = E.EmpID;
                    dr["PolicyNo"] = E.PolicyNo;
                    dr["IssueDate"] = E.IssueDate;
                    dr["MonthlyPremium"] = E.MonthlyPremium;
                    dr["CertificateNo"] = E.CertificateNo;
                    dr["ExpiryDate"] = E.ExpiryDate;
                    dr["Remarks"] = E.Remarks;
                    dr["Ext"] = E.Ext;
                    dr["NameOfInsurer"] = E.NameOfInsurer;
                    dtIns.Rows.Add(dr);
                }
                using(SqlBulkCopy bcp = new SqlBulkCopy(conn, SqlBulkCopyOptions.Default, trans)) {
                    bcp.DestinationTableName = "[dbo].[T_HR_Insurance]";
                    try {
                        await bcp.WriteToServerAsync(dtIns);
                    } catch(Exception ex) {
                        trans.Rollback();
                        conn.Close(); return ex.Message;
                    }
                }
                await Task.Run(() => trans.Commit());
                conn.Close();
                return clInsurance.Count + " record(s) are inserted";
            }
        }
        public static async Task<List<T_HR_EmpEducationDetails>> ReadQualiicationsAsync(int cid) {
            List<T_HR_EmpEducationDetails> clQualifs = new List<T_HR_EmpEducationDetails>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                SqlCommand cmd = new SqlCommand("select t1.* from T_HR_EmpEducationDetails t1 join T_G_EmployeeMaster t2 on t1.EmpID = t2.EmpId where t2.cid = @cid", con);
                cmd.Parameters.Add(new SqlParameter("@cid", cid));
                using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                    while(await rdr.ReadAsync()) {
                        T_HR_EmpEducationDetails t = new T_HR_EmpEducationDetails();
                        t.EduID = (int)rdr["EduID"];
                        t.EmpID = (int)rdr["EmpID"];
                        t.Qualification = rdr["Qualification"] == DBNull.Value ? "" : rdr["Qualification"].ToString();
                        t.Institute = rdr["Institute"] == DBNull.Value ? "" : rdr["Institute"].ToString();
                        t.YOP = rdr["YOP"] == DBNull.Value ? -1 : (int)rdr["YOP"];
                        t.Specialization = rdr["Specialization"] == DBNull.Value ? "" : rdr["Specialization"].ToString();
                        t.Percentage = rdr["Percentage"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["Percentage"]);
                        t.Mode = rdr["Mode"] == DBNull.Value ? "" : rdr["Mode"].ToString();
                        clQualifs.Add(t);
                    }
                }
                con.Close();
            }
            return clQualifs;
        }
        public static async Task<string> BulkInsertQualificationsAsync(List<T_HR_EmpEducationDetails> clQualifs) {
            try {
                using(SqlConnection conn = new SqlConnection(common.erpCS)) {
                    await conn.OpenAsync();
                    SqlTransaction trans = trans = await Task.Run<SqlTransaction>(() => conn.BeginTransaction("clQualifs"));
                    T_HR_EmpEducationDetails t = new T_HR_EmpEducationDetails();
                    DataTable dtClass = t.dtClass("dtClass");
                    int i = 0;
                    foreach(T_HR_EmpEducationDetails E in clQualifs) {
                        DataRow dr = dtClass.NewRow();
                        dr["EduID"] = i++;
                        dr["EmpID"] = E.EmpID;
                        dr["Qualification"] = E.Qualification;
                        dr["Institute"] = E.Institute;
                        dr["YOP"] = E.YOP;
                        dr["Specialization"] = E.Specialization;
                        dr["Percentage"] = E.Percentage;
                        dr["Mode"] = E.Mode;
                        dtClass.Rows.Add(dr);
                    }
                    using(SqlBulkCopy bcp = new SqlBulkCopy(conn, SqlBulkCopyOptions.Default, trans)) {
                        bcp.DestinationTableName = "[dbo].[T_HR_EmpEducationDetails]";
                        try {
                            await bcp.WriteToServerAsync(dtClass);
                        } catch(Exception ex) {
                            trans.Rollback();
                            conn.Close(); return ex.Message;
                        }
                    }
                    await Task.Run(() => trans.Commit());
                    conn.Close();
                    return clQualifs.Count + " record(s) are inserted";
                }
            } catch(Exception ex) { return ex.Message; }
        }
        public static async Task<List<T_HR_EmpExperience>> ReadExperiencesAsync(int cid) {
            List<T_HR_EmpExperience> clEmpExperience = new List<T_HR_EmpExperience>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                SqlCommand cmd = new SqlCommand(" select t1.* from T_HR_EmpExperience t1 join T_G_EmployeeMaster t2 on t1.EmpID = t2.EmpId where t2.cid = @cid", con);
                cmd.Parameters.Add(new SqlParameter("@cid", cid));
                using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                    while(await rdr.ReadAsync()) {
                        T_HR_EmpExperience t = new T_HR_EmpExperience();
                        t.ExpID = rdr["ExpID"] == DBNull.Value ? -1 : (int)rdr["ExpID"];
                        t.EmpID = rdr["EmpID"] == DBNull.Value ? -1 : (int)rdr["EmpID"];
                        t.Organization = rdr["Organization"] == DBNull.Value ? "" : rdr["Organization"].ToString();
                        t.City = rdr["City"] == DBNull.Value ? -1 : (int)rdr["City"];
                        t.Type = rdr["Type"] == DBNull.Value ? "" : rdr["Type"].ToString();
                        t.FromDate = rdr["FromDate"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["FromDate"]);
                        t.ToDate = rdr["ToDate"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["ToDate"]);
                        t.Designation = rdr["Designation"] == DBNull.Value ? "" : rdr["Designation"].ToString();
                        t.CTC = rdr["CTC"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["CTC"]);
                        clEmpExperience.Add(t);
                    }
                }
                con.Close();
            }
            return clEmpExperience;
        }
        public static async Task<string> BulkInsertExperiencesAsync(List<T_HR_EmpExperience> clExperience) {
            try {
                using(SqlConnection conn = new SqlConnection(common.erpCS)) {
                    await conn.OpenAsync();
                    //using(SqlTransaction tran = con.BeginTransaction()) {
                    T_HR_EmpExperience t = new T_HR_EmpExperience();
                    //using(SqlBulkCopy bulkCopy = new SqlBulkCopy(con, SqlBulkCopyOptions.Default, tran)) {
                    DataTable dtClass = t.dtClass("dtClass");
                    int i = 0;
                    foreach(T_HR_EmpExperience E in clExperience) {
                        DataRow dr = dtClass.NewRow();
                        dr["ExpID"] = i++;
                        dr["EmpID"] = E.EmpID;
                        dr["Organization"] = E.Organization;
                        dr["City"] = E.City;
                        dr["Type"] = E.Type;
                        dr["FromDate"] = E.FromDate;
                        dr["ToDate"] = E.ToDate;
                        dr["Designation"] = E.Designation;
                        dr["CTC"] = E.CTC;
                        dtClass.Rows.Add(dr);
                    }
                    SqlTransaction trans = trans = await Task.Run<SqlTransaction>(() => conn.BeginTransaction("clExperience"));
                    using(SqlBulkCopy bcp = new SqlBulkCopy(conn, SqlBulkCopyOptions.Default, trans)) {
                        bcp.DestinationTableName = "[dbo].[T_HR_EmpExperience]";
                        try {
                            await bcp.WriteToServerAsync(dtClass);
                        } catch(Exception ex) {
                            trans.Rollback();
                            conn.Close(); return ex.Message;
                        }
                    }
                    await Task.Run(() => trans.Commit());
                    conn.Close();
                    return clExperience.Count + " record(s) are inserted";

                }
            } catch(Exception ex) { return ex.Message; }
        }
        public static async Task<List<t_g_employeemaster>> ReadEmployeeMaster(int cid, string st) {
            List<t_g_employeemaster> empMaster = new List<t_g_employeemaster>();
            using(SqlConnection conn = new SqlConnection(common.erpCS)) {
                await conn.OpenAsync();
                using(SqlCommand cmd = new SqlCommand("sh_empMaster", conn)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    cmd.Parameters.Add(new SqlParameter("@st", st));
                    DataSet ds = new DataSet();
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(ds);
                    //DataSet ds = SQLDBUtil.ExecuteDataset("sh_empMaster", sps);
                    foreach(DataRow rdr in ds.Tables[0].Rows) {
                        t_g_employeemaster t = new t_g_employeemaster();
                        t.EmpId = (int)rdr["EmpId"];
                        t.DeptNo = rdr["DeptNo"] == DBNull.Value ? -1 : (int)rdr["DeptNo"];
                        t.FName = rdr["FName"] == DBNull.Value ? "" : rdr["FName"].ToString();
                        t.MName = rdr["MName"] == DBNull.Value ? "" : rdr["MName"].ToString();
                        t.LName = rdr["LName"] == DBNull.Value ? "" : rdr["LName"].ToString();
                        t.UserName = rdr["UserName"] == DBNull.Value ? "" : rdr["UserName"].ToString();
                        t.Designation = rdr["Designation"] == DBNull.Value ? "" : rdr["Designation"].ToString();
                        t.DesgID = rdr["DesgID"] == DBNull.Value ? -1 : (Int16)rdr["DesgID"];
                        t.CateId = rdr["CateId"] == DBNull.Value ? -1 : (Int16)rdr["CateId"];
                        t.Mobile1 = rdr["Mobile1"] == DBNull.Value ? "" : rdr["Mobile1"].ToString();
                        t.Mobile2 = rdr["Mobile2"] == DBNull.Value ? "" : rdr["Mobile2"].ToString();
                        t.Status = rdr["Status"] == DBNull.Value ? "" : rdr["Status"].ToString();
                        t.Mailid = rdr["Mailid"] == DBNull.Value ? "" : rdr["Mailid"].ToString();
                        t.Type = rdr["Type"] == DBNull.Value ? -1 : (int)rdr["Type"];
                        t.Categary = rdr["Categary"] == DBNull.Value ? -1 : (int)rdr["Categary"];
                        t.Mgnr = rdr["Mgnr"] == DBNull.Value ? -1 : (int)rdr["Mgnr"];
                        t.DOB = rdr["DOB"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["DOB"]);
                        t.Mole1 = rdr["Mole1"] == DBNull.Value ? "" : rdr["Mole1"].ToString();
                        t.EmpNature = rdr["EmpNature"] == DBNull.Value ? -1 : (int)rdr["EmpNature"];
                        t.Gender = rdr["Gender"] == DBNull.Value ? "" : rdr["Gender"].ToString();
                        t.AccountNumber = rdr["AccountNumber"] == DBNull.Value ? "" : rdr["AccountNumber"].ToString();
                        t.Shift = rdr["Shift"] == DBNull.Value ? -1 : (Int16)rdr["Shift"];
                        t.BankID = rdr["BankID"] == DBNull.Value ? -1 : (int)rdr["BankID"];
                        t.BranchID = rdr["BranchID"] == DBNull.Value ? -1 : (int)rdr["BranchID"];
                        t.NEFT = rdr["NEFT"] == DBNull.Value ? "" : rdr["NEFT"].ToString();
                        t.PANNumber = rdr["PANNumber"] == DBNull.Value ? "" : rdr["PANNumber"].ToString();
                        t.OldEmpID = rdr["OldEmpID"] == DBNull.Value ? "" : rdr["OldEmpID"].ToString();
                        t.PlaceOfBirth = rdr["PlaceOfBirth"] == DBNull.Value ? "" : rdr["PlaceOfBirth"].ToString();
                        t.Qualification = rdr["Qualification"] == DBNull.Value ? "" : rdr["Qualification"].ToString();
                        t.PaymentMode = rdr["PaymentMode"] == DBNull.Value ? -1 : (int)rdr["PaymentMode"];
                        t.StatusChangeOn = rdr["StatusChangeOn"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(rdr["StatusChangeOn"]);
                        t.StatusChangeRemarks = rdr["StatusChangeRemarks"] == DBNull.Value ? "" : rdr["StatusChangeRemarks"].ToString();
                        t.Mpassword = rdr["Mpassword"] == DBNull.Value ? "" : rdr["Mpassword"].ToString();
                        empMaster.Add(t);
                    }
                }//dr["StatusChangeOn"] = (object)e.StatusChangeOn ?? DBNull.Value
            }
            return empMaster;
        }
        //-------------------------------
        private static List<BankBranches> clBankBranches = new List<BankBranches>();
        private static List<T_G_DesignationMaster> clDesignations = new List<T_G_DesignationMaster>();
        private static List<EmployeeType> clEmpType = new List<EmployeeType>();
        private static List<T_HR_Prj_Manager> clSiteHeads = new List<T_HR_Prj_Manager>();
        private static List<t_hr_prj_dept> clDeptHeads = new List<t_hr_prj_dept>();
        //--------------------------------
        private static async Task<string> ReadBankBranchesAsync() {
            try {
                clBankBranches = new List<BankBranches>();
                using(SqlConnection conn = new SqlConnection(common.erpCS)) {
                    await conn.OpenAsync();
                    using(SqlCommand cmd = new SqlCommand("sh_bankBranches", conn)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                            while(await rdr.ReadAsync()) {
                                BankBranches t = new BankBranches(
                                    (int)rdr["BranchId"]
                                     , rdr["BranchName"] == DBNull.Value ? "" : rdr["BranchName"].ToString()
                                     , rdr["BankID"] == DBNull.Value ? -1 : (int)rdr["BankID"]
                                     , rdr["BankName"] == DBNull.Value ? "" : rdr["BankName"].ToString()
                                     , rdr["SWIFT"] == DBNull.Value ? "" : rdr["SWIFT"].ToString());
                                clBankBranches.Add(t);
                            }
                        }
                    }
                }
                return "success";
            } catch(Exception ex) { return ex.Message; }
        }
        private static async Task<string> ReadDesignations(int cid) { // http://localhost/Login/api/hmsMasters/designations?cid=1
            try {
                List<T_G_DesignationMaster> clDesignations = new List<T_G_DesignationMaster>();
                using(SqlConnection conn = new SqlConnection(common.erpCS)) {
                    await conn.OpenAsync();
                    string query = "select [DesigId],[Designation] from T_G_DesignationMaster where  [Status] = 1 and [cid]  =" + cid;
                    using(SqlCommand cmd = new SqlCommand(query, conn)) {
                        cmd.CommandType = CommandType.Text;
                        using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                            while(await rdr.ReadAsync()) {
                                T_G_DesignationMaster t = new T_G_DesignationMaster((Int16)rdr["DesigId"], rdr["Designation"].ToString(), true, cid);
                                clDesignations.Add(t);
                            }
                        }
                    }
                }
                return "success";
            } catch { return null; }
        }
        private static async Task<string> ReadEmpType(int cid) {
            try {
                List<EmployeeType> clEmpType = new List<EmployeeType>();
                using(SqlConnection conn = new SqlConnection(common.erpCS)) {
                    await conn.OpenAsync();
                    using(SqlCommand cmd = new SqlCommand("HR_GetEmployeeTypes", conn)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@cid", cid));
                        using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                            while(await rdr.ReadAsync()) {
                                EmployeeType t = new EmployeeType((int)rdr["EmpTyID"], rdr["EMpType"].ToString(), cid);
                                clEmpType.Add(t);
                            }
                        }
                    }
                    return "success";
                }
            } catch(Exception ex) { return ex.Message; }
        }
        private static async Task<string> ReadSiteHeads(int cid) {
            try {
                List<T_HR_Prj_Manager> clSiteHeads = new List<T_HR_Prj_Manager>();
                using(SqlConnection conn = new SqlConnection(common.erpCS)) {
                    await conn.OpenAsync();
                    string query = "select [PrjId],[MgnrId],[Status] from T_HR_Prj_Manager pm join worksite ws on ws.site_id = pm.prjId where ws.companyID =" + cid;
                    using(SqlCommand cmd = new SqlCommand(query, conn)) {
                        cmd.CommandType = CommandType.Text;
                        using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                            while(await rdr.ReadAsync()) {
                                T_HR_Prj_Manager t = new T_HR_Prj_Manager();
                                t.PrjId = (int)rdr["PrjId"];
                                t.MgnrId = rdr["MgnrId"] == DBNull.Value ? -1 : (int)rdr["MgnrId"];
                                t.Status = rdr["Status"] == DBNull.Value ? -1 : (byte)rdr["Status"];
                                clSiteHeads.Add(t);
                            }
                        }
                    }
                }
                return "success";
            } catch(Exception ex) { return ex.Message; }
        }
        private static async Task<string> ReadDeptHeads(int cid) {
            try {
                List<t_hr_prj_dept> clDeptHeads = new List<t_hr_prj_dept>();
                using(SqlConnection conn = new SqlConnection(common.erpCS)) {
                    await conn.OpenAsync();
                    string query = "select [PrjID],[DeptID],[HeadID],[Status] from t_hr_prj_dept dh join worksite ws on ws.site_id = dh.prjId where ws.companyID =" + cid;
                    using(SqlCommand cmd = new SqlCommand(query, conn)) {
                        cmd.CommandType = CommandType.Text;
                        using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                            while(await rdr.ReadAsync()) {
                                t_hr_prj_dept t = new t_hr_prj_dept();
                                t.PrjID = (int)rdr["PrjId"];
                                t.DeptID = rdr["DeptID"] == DBNull.Value ? -1 : (int)rdr["DeptID"];
                                t.HeadID = rdr["HeadID"] == DBNull.Value ? -1 : (int)rdr["HeadID"];
                                t.Status = rdr["Status"] == DBNull.Value ? 0 : (byte)rdr["Status"];
                                clDeptHeads.Add(t);
                            }
                        }
                    }
                }
                return "success";
            } catch(Exception ex) { return ex.Message; }
        }
        //-------------------------
        private static async Task<int> InitializeBulkInsertSession(SqlConnection conn, SqlTransaction tran) {
            //1st Trip to database
            SqlCommand cmd = new SqlCommand("INSERT INTO [dbo].[BulkInsertSession]([TsCreated]) VALUES (CURRENT_TIMESTAMP)", conn, tran);
            await cmd.ExecuteNonQueryAsync();
            cmd = new SqlCommand("SELECT [SessionID] FROM [dbo].[BulkInsertSession] WHERE @@ROWCOUNT > 0 and [SessionID] = SCOPE_IDENTITY()", conn, tran);
            return (int)await cmd.ExecuteScalarAsync();
        }
        private static async Task<int> getInsertedEmpIDs(List<t_g_employeemaster> clEmployees, int bulkInsertSessionId, SqlConnection conn, SqlTransaction tran) {
            SqlCommand cmd = new SqlCommand("SELECT [EmpId] FROM [dbo].[t_g_employeemaster] WHERE [sgID]=@bulkInsertSessionId ORDER BY [EmpId] ASC", conn, tran);
            cmd.Parameters.Add(new SqlParameter("@bulkInsertSessionId", bulkInsertSessionId));
            int index = 0;
            using(SqlDataReader reader = await cmd.ExecuteReaderAsync()) {
                while(await reader.ReadAsync()) { //iterate the Res IDs and update the foreign keys to OTHER TABLES
                    int dbID = (int)reader[0];//fetched from Database autogenerated after BULK insertion
                    clEmployees[index].EmpId = dbID;//update the LOCAL LIST CLASS
                    index++;
                }
            }
            return index;
        }
        private static async Task BulkInsertOtherTables(List<t_g_employeemaster> clEmployees, int bulkInsertSessionId, SqlConnection conn, SqlTransaction tran, int cid, int userID) {
            //using(SqlBulkCopy bulkCopy = new SqlBulkCopy(conn, SqlBulkCopyOptions.Default, tran)) {
            t_a_ledgers dtLgr = new t_a_ledgers();
            DataTable dtLedger = dtLgr.dtLedgers("dtLedger");

            t_g_employeeledgermapping lMap = new t_g_employeeledgermapping();
            DataTable dtMap = lMap.dtEmployeeledgermapping("dtMap");

            T_HR_Prj_Manager pm = new T_HR_Prj_Manager();
            DataTable dtPM = pm.dtPrj_Manager("dtPM");

            t_hr_prj_dept dept = new t_hr_prj_dept();
            DataTable dtDept = dept.dtPrj_dept("dtDept");

            var empTyRank = clEmpType.Select((x, y) => new { rank = y + 1, type = x.EmpTyID }).ToList();
            int i = 0;
            foreach(t_g_employeemaster emp in clEmployees) {
                DataRow dr1 = dtLedger.NewRow();
                dr1["ledgerID"] = i++;
                dr1["GroupId"] = 7;
                dr1["Ledger"] = "E" + emp.EmpId.ToString().PadLeft(5, '0') + " " + emp.FName + " " + (emp.MName != "" ? emp.MName + " " : "") + emp.LName;
                dr1["LedgerAlias"] = "E" + emp.EmpId.ToString().PadLeft(5, '0') + " " + emp.FName + " " + (emp.MName != "" ? emp.MName + " " : "") + emp.LName;
                dr1["InventoryEffected"] = DBNull.Value;
                dr1["Bitsstatus"] = 1;
                dr1["CompanyID"] = cid;
                dr1["Category"] = DBNull.Value;
                dr1["code"] = DBNull.Value;
                dr1["CompanyID"] = cid;
                dr1["ssid"] = emp.EmpId;
                dtLedger.Rows.Add(dr1);
                var rnk = (from t in empTyRank where t.type == emp.Type select t).FirstOrDefault();
                if(rnk != null && rnk.rank > 2 && rnk.rank < 5) {
                    if(rnk.rank == 3) { //project manager
                        var siteHead = (from t in clSiteHeads where t.PrjId == emp.Categary & t.MgnrId == -1 select t).FirstOrDefault();
                        if(siteHead != null) {
                            DataRow dr3 = dtPM.NewRow();
                            dr3["PrjId"] = emp.Categary;//worksite
                            dr3["MgnrId"] = emp.EmpId;
                            dr3["Status"] = 1;
                            dr3["CreatedOn"] = DateTime.Today;
                            dr3["CreatedBy"] = userID;
                            dr3["UpdatedBy"] = DBNull.Value;
                            dr3["UpdatedOn"] = DBNull.Value;
                            dtPM.Rows.Add(dr3);
                        }
                    }
                    if(rnk.rank == 4) {
                        var deptHead = (from t in clDeptHeads where t.PrjID == emp.Categary & t.HeadID == -1 select t).FirstOrDefault();
                        if(deptHead != null) {
                            DataRow dr4 = dtDept.NewRow();
                            dr4["PrjID"] = emp.Categary;//worksite
                            dr4["DeptID"] = emp.DeptNo;
                            dr4["HeadID"] = emp.EmpId;
                            dr4["Status"] = 1;
                            dr4["UpdatedOn"] = DBNull.Value;
                            dr4["UpdatedBY"] = DBNull.Value;
                            dr4["CreatedOn"] = DateTime.Today;
                            dr4["CreatedBY"] = userID;
                            dtDept.Rows.Add(dr4);
                        }
                    }
                }
            }
            if(dtLedger.Rows.Count > 0) {
                using(SqlBulkCopy bcp = new SqlBulkCopy(conn, SqlBulkCopyOptions.Default, tran)) {
                    bcp.DestinationTableName = "[dbo].[t_a_ledgers]";
                    try { await bcp.WriteToServerAsync(dtLedger); } catch(Exception ex) { }
                }
            }
            if(dtPM.Rows.Count > 0) {
                using(SqlBulkCopy bcp = new SqlBulkCopy(conn, SqlBulkCopyOptions.Default, tran)) {
                    bcp.DestinationTableName = "[dbo].[T_HR_Prj_Manager]";
                    try { await bcp.WriteToServerAsync(dtPM); } catch(Exception ex) { }
                }
            }
            if(dtDept.Rows.Count > 0) {
                using(SqlBulkCopy bcp = new SqlBulkCopy(conn, SqlBulkCopyOptions.Default, tran)) {
                    bcp.DestinationTableName = "[dbo].[t_hr_prj_dept]";
                    try { await bcp.WriteToServerAsync(dtDept); } catch(Exception ex) { }
                }
            }
            List<twoIDs> clEmpLgrs = await Task.Run(() => getInsertedLedgerIDs(bulkInsertSessionId, conn, tran));
            foreach(twoIDs el in clEmpLgrs) {
                DataRow dr = dtMap.NewRow();
                dr["MapId"] = i++;
                dr["EmpId"] = el.empID;
                dr["LedgerId"] = el.ID2;
                dtMap.Rows.Add(dr);
            }
            if(dtMap.Rows.Count > 0) {
                using(SqlBulkCopy bcp = new SqlBulkCopy(conn, SqlBulkCopyOptions.Default, tran)) {
                    bcp.DestinationTableName = "[dbo].[t_g_employeeledgermapping]";
                    try { await bcp.WriteToServerAsync(dtMap); } catch(Exception ex) { }
                }
            }
        }
        private static async Task<List<twoIDs>> getInsertedLedgerIDs(int bulkInsertSessionId, SqlConnection conn, SqlTransaction tran) {
            SqlCommand cmd = new SqlCommand("SELECT em.EmpId, lg.ledgerID FROM [dbo].[t_g_employeemaster] em join t_a_ledgers lg on lg.ssid = em.EmpId " +
                "WHERE [sgID]= @bulkInsertSessionId ORDER BY [EmpId] ASC", conn, tran);
            cmd.Parameters.Add(new SqlParameter("@bulkInsertSessionId", bulkInsertSessionId));
            List<twoIDs> empLgrs = new List<twoIDs>();
            using(SqlDataReader reader = await cmd.ExecuteReaderAsync()) {
                while(await reader.ReadAsync()) { //iterate the Res IDs and update the foreign keys to OTHER TABLES
                    twoIDs el = new twoIDs();
                    el.empID = (int)reader[0];//fetched from Database autogenerated after BULK insertion
                    el.ID2 = (int)reader[1];
                    empLgrs.Add(el);
                }
            }
            return empLgrs;
        }
        private static async Task CompleteBulkInsertSession(int bulkInsertSessionId, SqlConnection conn, SqlTransaction tran) {
            //3rd trip to Database
            SqlCommand cmd = new SqlCommand("UPDATE [dbo].[t_g_employeemaster] SET [sgID] = NULL WHERE [sgID] = @bulkInsertSessionId", conn, tran);
            cmd.Parameters.Add(new SqlParameter("@bulkInsertSessionId", bulkInsertSessionId));
            await cmd.ExecuteNonQueryAsync();
            cmd = new SqlCommand("DELETE FROM [dbo].[BulkInsertSession] WHERE [SessionID] = @bulkInsertSessionId", conn, tran);
            cmd.Parameters.Add(new SqlParameter("@bulkInsertSessionId", bulkInsertSessionId));
            await cmd.ExecuteNonQueryAsync();
        }
        private static async Task<int> BulkInsertFlatEMPs(List<t_g_employeemaster> clEmployees, int bulkInsertSessionId, SqlConnection conn
                , SqlTransaction tran, int userID, int cid) {
            // DELETED fields >> Password, Designation, Mobile2, AltMail,SkypeID, DesigID, AppID, Mole2, PreOrder, UpdatedBY, UpdatedON, StatusChangeOn, 
            // StausChangeBy, StatusChangeRemarks, RejoinOn, POB, Qualification, Mpassword, sgID, Grade, cid, BankID, CreateBy              
            t_g_employeemaster omsEmp = new t_g_employeemaster();
            int i = 0;
            DataTable dtEmps = omsEmp.dtEmployeemaster("dtEmpsXXXX");
            foreach(t_g_employeemaster E in clEmployees) {
                DataRow dr = dtEmps.NewRow();
                dr["EmpId"] = i++;
                dr["DeptNo"] = E.DeptNo;
                dr["FName"] = E.FName;
                dr["MName"] = E.MName;
                dr["LName"] = E.LName;
                dr["UserName"] = E.UserName;
                dr["Password"] = DBNull.Value;
                string des = (from t in clDesignations where t.DesigId == E.DesgID select t.Designation).FirstOrDefault();
                dr["Designation"] = des;
                dr["DesgID"] = E.DesgID;//T_G_DesignationMaster table
                dr["CateId"] = E.CateId;  // from T_G_CategoryMaster table Specialisation/trade
                dr["Mobile1"] = E.Mobile1;
                dr["Mobile2"] = E.Mobile2;
                dr["Status"] = "y";
                dr["Mailid"] = E.Mailid;
                dr["AltMail"] = DBNull.Value;
                dr["skypeid"] = DBNull.Value;
                dr["Type"] = E.Type;//T_HR_EmployeeType table 
                dr["Categary"] = E.Categary; // from WorkSite table
                dr["Mgnr"] = E.Mgnr;//T_HR_Prj_Dept table
                dr["Image"] = DBNull.Value;
                dr["DesigID"] = DBNull.Value;//repetition of this to above "DesgID" property
                dr["DOB"] = E.DOB;
                dr["AppID"] = DBNull.Value;
                dr["Mole1"] = E.Mole1;
                dr["Mole2"] = DBNull.Value;
                dr["PreOrder"] = DBNull.Value;
                dr["UpdatedBY"] = DBNull.Value;
                dr["UpdatedON"] = DBNull.Value;
                dr["CreatedOn"] = DateTime.Now;
                dr["CreatedBy"] = userID;
                dr["EmpNature"] = E.EmpNature;//T_HR_NatureOfEmployment table
                dr["Gender"] = E.Gender;
                dr["AccountNumber"] = E.AccountNumber;
                dr["Shift"] = E.Shift;//t_g_shifts table
                int bkID = (from t in clBankBranches where t.BranchId == E.BranchID select t.BankID).FirstOrDefault();
                dr["BankID"] = bkID;
                dr["BranchID"] = E.BranchID; // T_G_BankBranches
                string swift = (from t in clBankBranches where t.BranchId == E.BranchID select t.SWIFT).FirstOrDefault();
                dr["NEFT"] = swift;//IFSC/SWIFT Code
                dr["StatusChangeOn"] = (object)E.StatusChangeOn ?? DBNull.Value;//date of marriage
                dr["StausChangeBy"] = DBNull.Value;
                dr["StatusChangeRemarks"] = E.StatusChangeRemarks;//religion
                dr["RejoinOn"] = DBNull.Value;
                dr["PANNumber"] = E.PANNumber;
                dr["OldEmpID"] = E.OldEmpID;
                dr["PlaceOfBirth"] = E.PlaceOfBirth;
                dr["POB"] = DBNull.Value;
                dr["Qualification"] = E.Qualification;
                dr["Mpassword"] = E.Mpassword;//race
                dr["PaymentMode"] = E.PaymentMode;
                dr["Grade"] = DBNull.Value;
                dr["cid"] = cid;
                dr["sgID"] = bulkInsertSessionId;

                dtEmps.Rows.Add(dr);
            }
            //2nd trip to Database
            using(SqlBulkCopy bcp = new SqlBulkCopy(conn, SqlBulkCopyOptions.Default, tran)) {
                bcp.DestinationTableName = "[dbo].[t_g_employeemaster]";
                try { await bcp.WriteToServerAsync(dtEmps); } catch(Exception ex) { }
            }
            return await Task.Run(() => getInsertedEmpIDs(clEmployees, bulkInsertSessionId, conn, tran));
        }
        public static async Task<string> CreateEmployeesBulkAsync(List<t_g_employeemaster> clEmployees, int cid, int userID) {
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("createEmployees"));
                List<Task<string>> tasks = new List<Task<string>>();
                tasks.Add(Task.Run(() => ReadBankBranchesAsync()));
                tasks.Add(Task.Run(() => ReadSiteHeads(cid)));
                tasks.Add(Task.Run(() => ReadDeptHeads(cid)));
                tasks.Add(Task.Run(() => ReadEmpType(cid)));
                tasks.Add(Task.Run(() => ReadDesignations(cid)));
                tasks.Add(Task.Run(() => ReadDesignations(cid)));
                var results = await Task.WhenAll(tasks);
                int bulkInsertSessionId = await Task.Run(() => InitializeBulkInsertSession(con, tran));
                int count = await Task.Run(() => BulkInsertFlatEMPs(clEmployees, bulkInsertSessionId, con, tran, userID, cid));
                if(count > 0) {
                    await BulkInsertOtherTables(clEmployees, bulkInsertSessionId, con, tran, cid, userID);
                    await CompleteBulkInsertSession(bulkInsertSessionId, con, tran);
                    tran.Commit();
                    con.Close();
                    return clEmployees.Count.ToString() + " Employee record(s) are inserted!";
                } else {
                    con.Close();
                    return "Error from database!";
                }
            }
        }
        public static async Task<List<T_G_EmployeeMaster_add>> ReadBioRegisteredEmployeesAsync(int cid) {
            List<T_G_EmployeeMaster_add> clEmployeeMaster_add = new List<T_G_EmployeeMaster_add>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = " select t1.* from T_G_EmployeeMaster_add t1 join T_G_EmployeeMaster t2 on t1.EmpID = t2.EmpId " +
                    "where t2.status = 'y' and t2.cid = @cid";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            T_G_EmployeeMaster_add t = new T_G_EmployeeMaster_add();
                            t.id = rdr["id"] == DBNull.Value ? -1 : (int)rdr["id"];
                            t.empID = rdr["empID"] == DBNull.Value ? -1 : (int)rdr["empID"];
                            t.fingerPrintValue = rdr["fingerPrintValue"] == DBNull.Value ? "" : rdr["fingerPrintValue"].ToString();
                            t.ia = rdr["ia"] == DBNull.Value ? false : (bool)rdr["ia"];
                            clEmployeeMaster_add.Add(t);
                        }
                    }
                }
                con.Close();
                return clEmployeeMaster_add;
            }
        }
        public static async Task<List<emplatlng>> ReadEmpLocationsAsync(int cid, DateTime dt) {
            List<emplatlng> clEmpAttGeoLoc = new List<emplatlng>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string procedure = "sh_bioEmpPositionsByDate";
                using(SqlCommand cmd = new SqlCommand(procedure, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    IFormatProvider culture = new CultureInfo("en-US", true);
                    cmd.Parameters.Add(new SqlParameter("@dt", dt.ToString("yyyy-MM-dd")));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            emplatlng t = new emplatlng();
                            t.wsID = rdr["wsID"] == DBNull.Value ? -1 : (int)rdr["wsID"];
                            t.empID = rdr["empID"] == DBNull.Value ? -1 : (int)rdr["empID"];
                            t.lat = rdr["lat"] == DBNull.Value ? 0 : Convert.ToDouble(rdr["lat"]);
                            t.lng = rdr["lng"] == DBNull.Value ? 0 : Convert.ToDouble(rdr["lng"]);
                            clEmpAttGeoLoc.Add(t);
                        }
                    }
                }
                con.Close();
                return clEmpAttGeoLoc;
            }
        }
        public static async Task<List<EmpAttTrack>> ReadEmpAttTrackAsync(int cid, DateTime dt, int? wsID) {
            List<EmpAttTrack> clEmpAttGeoLoc = new List<EmpAttTrack>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string procedure = "sh_EmpAttTrack";
                using(SqlCommand cmd = new SqlCommand(procedure, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    IFormatProvider culture = new CultureInfo("en-US", true);
                    cmd.Parameters.Add(new SqlParameter("@dt", dt.ToString("yyyy-MM-dd")));
                    cmd.Parameters.Add(new SqlParameter("@wsID", wsID));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            EmpAttTrack t = new EmpAttTrack();
                            t.id = rdr["id"] == DBNull.Value ? -1 : (int)rdr["id"];
                            t.dtStamp = rdr["dtStamp"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["dtStamp"]);
                            t.lat = rdr["lat"] == DBNull.Value ? 0 : Convert.ToDouble(rdr["lat"]);
                            t.lng = rdr["lng"] == DBNull.Value ? 0 : Convert.ToDouble(rdr["lng"]);
                            t.empID = rdr["empID"] == DBNull.Value ? -1 : (int)rdr["empID"];
                            t.cid = rdr["cid"] == DBNull.Value ? -1 : (int)rdr["cid"];
                            t.wsID = rdr["wsID"] == DBNull.Value ? -1 : (int)rdr["wsID"];
                            clEmpAttGeoLoc.Add(t);
                        }
                    }
                }
                con.Close();
                return clEmpAttGeoLoc;
            }
        }
        public static async Task<List<th_geoFence>> ReadGeoFencesAsync(int cid) {
            List<th_geoFence> clGeoFence = new List<th_geoFence>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string query = " select t1.* from th_geoFence t1  where t1.cid = @cid";
                using(SqlCommand cmd = new SqlCommand(query, con)) {
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            th_geoFence t = new th_geoFence();
                            t.id = rdr["id"] == DBNull.Value ? -1 : (int)rdr["id"];
                            t.cid = rdr["cid"] == DBNull.Value ? -1 : (int)rdr["cid"];
                            t.ws = rdr["ws"] == DBNull.Value ? -1 : (int)rdr["ws"];
                            t.fenceID = rdr["fenceID"] == DBNull.Value ? "" : rdr["fenceID"].ToString();
                            t.ia = rdr["ia"] == DBNull.Value ? false : (bool)rdr["ia"];
                            clGeoFence.Add(t);
                        }
                    }
                }
                con.Close();
                return clGeoFence;
            }
        }
        public static async Task<string> UpdateGeoFencesAsync(int fid, string fenceName) {
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                int updatedRecords = 0;
                using(SqlCommand cmd = new SqlCommand("sh_geoFenceRename", con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@fid", fid));
                    cmd.Parameters.Add(new SqlParameter("@fenceName", fenceName));
                    updatedRecords = await cmd.ExecuteNonQueryAsync();
                }
                con.Close();
                if(updatedRecords > 0) return "success";
                else return "Failed to update! Max length should not excced 30 chars!";
            }
        }
        public static async Task<string> CreateGeoFencesAsync(List<th_geoFence> clGeoFence, int cid) {
            try {
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("createEmployees"));
                    th_geoFence t = new th_geoFence();
                    using(SqlBulkCopy bulkCopy = new SqlBulkCopy(con, SqlBulkCopyOptions.Default, tran)) {
                        DataTable dtClass = t.dtClass("dtClass");
                        int i = 0; //CONSIDER CHANGING THE AUTOINCREMENTING TABLE ID FIELD TO THIS VARIABLE
                        foreach(th_geoFence E in clGeoFence) {
                            DataRow dr = dtClass.NewRow();
                            dr["id"] = i++;
                            dr["cid"] = E.cid;
                            dr["ws"] = E.ws;
                            dr["fenceID"] = E.fenceID;
                            dr["ia"] = E.ia;
                            dtClass.Rows.Add(dr);
                        }
                        using(SqlBulkCopy bcp = new SqlBulkCopy(con, SqlBulkCopyOptions.Default, tran)) {
                            bcp.DestinationTableName = "[dbo].[th_geoFence]";
                            try {
                                await bcp.WriteToServerAsync(dtClass);
                                tran.Commit();
                                con.Close();
                                return clGeoFence.Count.ToString() + " record(s) inserted!";
                            } catch(Exception ex) {
                                tran.Rollback();
                                con.Close();
                                return ex.Message;
                            }
                        }
                    }
                }
            } catch(Exception ex) { return ex.Message; }
        }
        public static async Task<string> CreateGeoFenceWithCoordsAsync(List<fenceCoords> clFenceCoords, int cid, int ws) {
            try {
                DataTable dt = new DataTable();
                dt.Columns.Add(new DataColumn("fID", typeof(System.String)));
                dt.Columns.Add(new DataColumn("v", typeof(System.Int32)));
                dt.Columns.Add(new DataColumn("x", typeof(System.Decimal)));
                dt.Columns.Add(new DataColumn("y", typeof(System.Decimal)));
                foreach(fenceCoords fc in clFenceCoords) {
                    DataRow dr = dt.NewRow();
                    dr[0] = fc.fid; dr[1] = fc.v; dr[2] = fc.x; dr[3] = fc.y;
                    dt.Rows.Add(dr);
                }
                int recordsUpdated = 0;
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("createEmployees"));
                    using(SqlCommand cmd = new SqlCommand("sh_addFences", con, tran)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@cid", cid));
                        cmd.Parameters.Add(new SqlParameter("@ws", ws));
                        cmd.Parameters.Add(new SqlParameter("@fxy", dt));
                        DataSet ds = new DataSet();
                        recordsUpdated = await cmd.ExecuteNonQueryAsync();
                    }
                    if(recordsUpdated > 0) { tran.Commit(); return "SUCCESS, this affect is seen on next login!"; } else { tran.Rollback(); return "FAILED"; }
                }
            } catch(Exception ex) { return ex.Message + " FAILED"; }
        }
        public static async Task<List<th_geoFenceCoords>> ReadGeoFenceCoordsAsync(int cid) {
            List<th_geoFenceCoords> clgeoFenceCoords = new List<th_geoFenceCoords>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string query = " select t1.* from th_geoFenceCoords t1 join th_geoFence t2 on t1.gfid = t2.id where t2.cid = @cid";
                using(SqlCommand cmd = new SqlCommand(query, con)) {
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            th_geoFenceCoords t = new th_geoFenceCoords();
                            t.gfid = rdr["gfid"] == DBNull.Value ? -1 : (int)rdr["gfid"];
                            t.v = rdr["v"] == DBNull.Value ? -1 : (int)rdr["v"];
                            t.x = rdr["x"] == DBNull.Value ? 0 : Convert.ToDouble(rdr["x"]);
                            t.y = rdr["y"] == DBNull.Value ? 0 : Convert.ToDouble(rdr["y"]);
                            clgeoFenceCoords.Add(t);
                        }
                    }
                }
                con.Close();
                return clgeoFenceCoords;
            }
        }
        public static async Task<string> CreateGeoFenceCoordsAsync(List<th_geoFenceCoords> clth_geoFenceCoords, int cid) {
            try {
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("geoFenceCoords"));
                    th_geoFenceCoords t = new th_geoFenceCoords();
                    DataTable dtClass = t.dtClass("dtClass");
                    int i = 0; //CONSIDER CHANGING THE AUTOINCREMENTING TABLE ID FIELD TO THIS VARIABLE
                    foreach(th_geoFenceCoords E in clth_geoFenceCoords) {
                        DataRow dr = dtClass.NewRow();
                        dr["gfid"] = E.gfid;
                        dr["v"] = E.v;
                        dr["x"] = E.x;
                        dr["y"] = E.y;
                        dtClass.Rows.Add(dr);
                    }
                    using(SqlBulkCopy bcp = new SqlBulkCopy(con, SqlBulkCopyOptions.Default, tran)) {
                        bcp.DestinationTableName = "[dbo].[th_geoFenceCoords]";
                        try {
                            await bcp.WriteToServerAsync(dtClass);
                            tran.Commit();
                            con.Close();
                            return clth_geoFenceCoords.Count.ToString() + " record(s) inserted!";
                        } catch(Exception ex) {
                            tran.Rollback();
                            con.Close();
                            return ex.Message;
                        }
                    }
                }
            } catch(Exception ex) { return ex.Message; }
        }
        public static async Task<List<th_geoFenceVsEmpID>> ReadGeoFenceVsEmpIDAsync(int cid) {
            List<th_geoFenceVsEmpID> clgeoFenceVsEmpID = new List<th_geoFenceVsEmpID>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string query = " select t1.* from th_geoFenceVsEmpID t1 join T_G_EmployeeMaster t2 on t1.EmpID = t2.EmpId where t2.cid = @cid";
                using(SqlCommand cmd = new SqlCommand(query, con)) {
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            th_geoFenceVsEmpID t = new th_geoFenceVsEmpID();
                            t.empID = rdr["empID"] == DBNull.Value ? -1 : (int)rdr["empID"];
                            t.gfID = rdr["gfID"] == DBNull.Value ? -1 : (int)rdr["gfID"];
                            t.dtStamp = rdr["dtStamp"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["dtStamp"]);
                            t.ia = rdr["ia"] == DBNull.Value ? false : (bool)rdr["ia"];
                            clgeoFenceVsEmpID.Add(t);
                        }
                    }
                }
                con.Close();
                return clgeoFenceVsEmpID;
            }
        }
        public static async Task<string> CreateGeoFenceVsEmpIDAsync(List<th_geoFenceVsEmpID> clgeoFenceVsEmpID, int cid) {
            try {
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("geoFenceVsEmpID"));
                    th_geoFenceVsEmpID t = new th_geoFenceVsEmpID();
                    DataTable dtClass = t.dtClass("dtClass");
                    int i = 0; //CONSIDER CHANGING THE AUTOINCREMENTING TABLE ID FIELD TO THIS VARIABLE
                    foreach(th_geoFenceVsEmpID E in clgeoFenceVsEmpID) {
                        DataRow dr = dtClass.NewRow();
                        dr["empID"] = E.empID;
                        dr["gfID"] = E.gfID;
                        dr["dtStamp"] = E.dtStamp;
                        dr["ia"] = E.ia;
                        dtClass.Rows.Add(dr);
                    }
                    using(SqlBulkCopy bcp = new SqlBulkCopy(con, SqlBulkCopyOptions.Default, tran)) {
                        bcp.DestinationTableName = "[dbo].[th_geoFenceVsEmpID]";
                        try {
                            await bcp.WriteToServerAsync(dtClass);
                            tran.Commit();
                            con.Close();
                            return clgeoFenceVsEmpID.Count.ToString() + " record(s) inserted!";
                        } catch(Exception ex) {
                            tran.Rollback();
                            con.Close();
                            return ex.Message;
                        }
                    }
                }
            } catch(Exception ex) { return ex.Message; }
        }
        public static async Task<string> UpdateGeoFenceVsEmpIDAsync(List<twoIDs> clGfEmp) {
            try {
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("geoFenceVsEmpID"));
                    DataTable dt2IDs = new twoIDs().dt2IDs("dt2IDs");
                    foreach(twoIDs E in clGfEmp) {
                        DataRow dr = dt2IDs.NewRow();
                        dr["id1"] = E.empID;
                        dr["id2"] = E.ID2;
                        dt2IDs.Rows.Add(dr);
                    }
                    string query = "sh_assignGeoFence2EmpIDupdate";
                    int recordsUpdated = 0;
                    using(SqlCommand cmd = new SqlCommand(query, con, tran)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@twoIDs", dt2IDs));
                        recordsUpdated = await cmd.ExecuteNonQueryAsync();
                    }
                    if(recordsUpdated > 0) tran.Commit();
                    con.Close();
                    return recordsUpdated.ToString() + " records(s) updated";
                }
            } catch(Exception ex) { return ex.Message; }
        }
        public static async Task<List<th_empAtt2Geofence>> ReadPassAtt2GeofenceAsync(int cid, DateTime dt, int? wsID) {
            List<th_empAtt2Geofence> clempAtt2Geofence = new List<th_empAtt2Geofence>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                IFormatProvider culture = new CultureInfo("en-US", true);
                string query = "select wsID,gfID, t1.empID,attID, pass from th_empAtt2Geofence t1 join vh_EmpAttTrack t2 on t1.attID = t2.id " +
                    "where convert(date,dtStamp) = '" + dt.ToString("yyyy-MM-dd") + (wsID != null ? "' and wsID = " + wsID : "'");
                using(SqlCommand cmd = new SqlCommand(query, con)) {
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            th_empAtt2Geofence t = new th_empAtt2Geofence();
                            t.empID = rdr["empID"] == DBNull.Value ? -1 : (int)rdr["empID"];
                            t.gfID = rdr["gfID"] == DBNull.Value ? -1 : (int)rdr["gfID"];
                            t.attID = rdr["attID"] == DBNull.Value ? -1 : (int)rdr["attID"];
                            t.pass = rdr["pass"] == DBNull.Value ? false : (bool)rdr["pass"];
                            clempAtt2Geofence.Add(t);
                        }
                    }
                }
                con.Close();
                return clempAtt2Geofence;
            }
        }
        public static async Task<string> CreatePassAtt2GeofenceAsync(List<th_empAtt2Geofence> clth_empAtt2Geofence, int cid) {
            try {
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("empAtt2Geofence"));
                    th_empAtt2Geofence t = new th_empAtt2Geofence();
                    DataTable dtClass = t.dtClass("dtClass");
                    foreach(th_empAtt2Geofence E in clth_empAtt2Geofence) {
                        DataRow dr = dtClass.NewRow();
                        dr["empID"] = E.empID;
                        dr["gfID"] = E.gfID;
                        dr["attID"] = E.attID;
                        dr["pass"] = E.pass;
                        dtClass.Rows.Add(dr);
                    }
                    using(SqlBulkCopy bcp = new SqlBulkCopy(con, SqlBulkCopyOptions.Default, tran)) {
                        bcp.DestinationTableName = "[dbo].[th_empAtt2Geofence]";
                        try {
                            await bcp.WriteToServerAsync(dtClass);
                            tran.Commit();
                            con.Close();
                            return clth_empAtt2Geofence.Count.ToString() + " record(s) inserted!";
                        } catch(Exception ex) {
                            tran.Rollback();
                            con.Close();
                            return ex.Message;
                        }
                    }
                }
            } catch(Exception ex) { return ex.Message; }
        }
        public static async Task<List<vh_empAttInOut>> ReadEmpAttInOutAsync(int cid, int? wsID, int? empID, DateTime? dt) {
            List<vh_empAttInOut> clvh_empAttInOut = new List<vh_empAttInOut>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "sh_empAttInOut";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    cmd.Parameters.Add(new SqlParameter("@wsID", wsID));
                    cmd.Parameters.Add(new SqlParameter("@empID", empID));
                    cmd.Parameters.Add(new SqlParameter("@dt", dt));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            vh_empAttInOut t = new vh_empAttInOut();
                            t.EmpID = rdr["EmpID"] == DBNull.Value ? -1 : (int)rdr["EmpID"];
                            t.Employee = rdr["Employee"] == DBNull.Value ? "" : rdr["Employee"].ToString();
                            t.Date = rdr["Date"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["Date"]);
                            t.Times = rdr["Times"] == DBNull.Value ? -1 : (int)rdr["Times"];
                            t.InTime = rdr["InTime"] == DBNull.Value ? "" : rdr["InTime"].ToString();
                            t.OutTime = rdr["OutTime"] == DBNull.Value ? "" : rdr["OutTime"].ToString();
                            t.AttRes = rdr["AttRes"] == DBNull.Value ? false : (bool)rdr["AttRes"];
                            clvh_empAttInOut.Add(t);
                        }
                    }
                }
                con.Close();
                return clvh_empAttInOut;
            }
        }
        public static bool PointInsidePolygon(double[] Coordinates, double[] ChkPoint, bool Is3D) {
            int m = 2; if(Is3D) m = 3;
            //Declare X Ray start and End Points
            double[] LnSP = new double[] { Math.Round(ChkPoint[0], 4), Math.Round(ChkPoint[1], 4) };
            double[] LnEP = new double[] { ChkPoint[0] + 10000000000, ChkPoint[1] };
            double[] tmpLnSP = new double[2];
            double[] tmpLnEP = new double[2];
            int count = 0;
            double[] intPt = null;
            int Ln = Coordinates.Length;
            if(Math.Round(Coordinates[0], 4) == Math.Round(Coordinates[Ln - m], 4)
                    && Math.Round(Coordinates[1], 4) == Math.Round(Coordinates[Ln - m + 1], 4))
                Ln = Ln - m;
            for(int i = 0; i <= Ln - m; i += m) {
                if(i == 0) {
                    tmpLnSP[0] = Math.Round(Coordinates[i], 4);
                    tmpLnSP[1] = Math.Round(Coordinates[i + 1], 4);
                    tmpLnEP[0] = Math.Round(Coordinates[Ln - m], 4);
                    tmpLnEP[1] = Math.Round(Coordinates[Ln - m + 1], 4);
                } else {
                    tmpLnSP[0] = Math.Round(Coordinates[i], 4);
                    tmpLnSP[1] = Math.Round(Coordinates[i + 1], 4);
                    tmpLnEP[0] = Math.Round(Coordinates[i - m], 4);
                    tmpLnEP[1] = Math.Round(Coordinates[i - m + 1], 4);
                }
                intPt = GetIntersectPointTwoLines2D((double[])LnSP.Clone(), (double[])LnEP.Clone(),
                        (double[])tmpLnSP.Clone(), (double[])tmpLnEP.Clone(), false);
                if(intPt != null) count++;
            }
            if(count % 2 == 0)
                return false;
            else
                return true;
        }
        public static double[] GetIntersectPointTwoLines2D(double[] p1, double[] p2, double[] p3, double[] p4, bool Extend) {
            Double[] intPt = new double[3];
            try {
                double d = ((p2[0] - p1[0]) * (p4[1] - p3[1])) - ((p2[1] - p1[1]) * (p4[0] - p3[0]));   // Is Parellel or Not Checking
                if(Math.Round(d, 6) == 0) return null;       //Lines are parellel
                else {
                    double q = ((p1[1] - p3[1]) * (p4[0] - p3[0])) - ((p1[0] - p3[0]) * (p4[1] - p3[1]));    //-p1[1]
                    double r = q / d;
                    double p = ((p1[1] - p3[1]) * (p2[0] - p1[0])) - ((p1[0] - p3[0]) * (p2[1] - p1[1]));
                    double s = p / d;
                    if(r < 0 || r > 1 || s < 0 || s > 1) { // Not Eql to Zero
                        if(!Extend) intPt = null;
                        else {
                            intPt[0] = p1[0] + r * (p2[0] - p1[0]);      // X-Value
                            intPt[1] = p1[1] + r * (p2[1] - p1[1]);      // Y-Value
                            if(p1.Length > 2 || p2.Length > 2) intPt[2] = p1[2] + r * (p2[2] - p1[2]);  // Z-Value
                        }
                    }  //  None // intersection is not within the line segments
                    else {//r or s are Eql to Zero then
                        intPt[0] = p1[0] + r * (p2[0] - p1[0]);          // X-Value
                        intPt[1] = p1[1] + r * (p2[1] - p1[1]);          // Y-Value
                        if(p1.Length > 2 || p2.Length > 2) intPt[2] = p1[2] + r * (p2[2] - p1[2]);      // Z-Value
                    }

                }
            } catch { intPt = null; }
            return intPt;
        }
        public static double[] toCoordsArray(List<th_geoFenceCoords> gfCoords) {
            List<double> coords = new List<double>();
            var opls = (from t in gfCoords select t).OrderBy(x => x.v).ToList();
            Dictionary<int, double> blg = new Dictionary<int, double>();
            foreach(th_geoFenceCoords pl in opls) {
                coords.Add(pl.x);
                coords.Add(pl.y);
            }
            return coords.ToArray();
        }
        public static async Task<List<EmpInOutStatus>> ComputeEmpInsideFenceAsync(int cid, DateTime dt, int? wsID) {
            List<th_geoFenceVsEmpID> clgeoFenceVsEmpID = await Task.Run(() => DAL.ReadGeoFenceVsEmpIDAsync(cid));
            List<th_geoFenceCoords> clgeoFenceCoords = await Task.Run(() => DAL.ReadGeoFenceCoordsAsync(cid));
            List<EmpAttTrack> clEmpAttGeoLoc = await Task.Run(() => DAL.ReadEmpAttTrackAsync(cid, dt, wsID));
            var gfIDs = (from t in clgeoFenceVsEmpID select t.gfID).Distinct().ToList();
            Dictionary<int, intBool> eIdSt = new Dictionary<int, intBool>();

            foreach(int g in gfIDs) { // check by each geofence
                var gEmpIDs = (from t in clgeoFenceVsEmpID where t.gfID == g select t.empID).ToList(); //Employees in each fence
                if(gEmpIDs.Count > 0) {
                    var gCoords = (from t in clgeoFenceCoords where t.gfid == g select t).ToList(); // coords of each fence
                    double[] gPL = toCoordsArray(gCoords);
                    foreach(int e in gEmpIDs) {
                        var eCoords = (from t in clEmpAttGeoLoc where t.empID == e select t).ToList();
                        intBool inbl = new intBool(-1, false);
                        if(eCoords.Count > 0) {
                            foreach(EmpAttTrack ec in eCoords) {
                                double[] ePos = new double[2] { ec.lat, ec.lng };
                                if(PointInsidePolygon(gPL, ePos, false) == true) {
                                    inbl.id = ec.id;
                                    inbl.bl = true;
                                    eIdSt.Add(ec.empID, inbl);
                                    break;
                                } else { inbl.id = ec.id; inbl.bl = false; }
                            }
                            if(inbl.bl == false) eIdSt.Add(e, inbl);
                        }
                    }
                }
            }
            List<EmpInOutStatus> egfInOut = new List<EmpInOutStatus>();
            foreach(KeyValuePair<int, intBool> kvp in eIdSt) {
                var wsGf = (from t1 in clgeoFenceVsEmpID join t2 in clEmpAttGeoLoc on t1.empID equals t2.empID
                            where t1.empID == kvp.Key select new { wsID = t2.wsID, gfID = t1.gfID }).FirstOrDefault();
                egfInOut.Add(new EmpInOutStatus(wsGf.wsID, wsGf.gfID, kvp.Key, kvp.Value.id, dt, kvp.Value.bl));
            }
            return egfInOut;
        }
        public static async Task<List<vh_bioRegistrations>> ReadBioRegistrationsAsync(int cid) {
            List<vh_bioRegistrations> clvh_bioRegistrations = new List<vh_bioRegistrations>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "sh_bioRegistrations";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            vh_bioRegistrations t = new vh_bioRegistrations();
                            t.cid = rdr["cid"] == DBNull.Value ? -1 : (int)rdr["cid"];
                            t.wsID = rdr["wsID"] == DBNull.Value ? -1 : (int)rdr["wsID"];
                            t.empID = rdr["empID"] == DBNull.Value ? -1 : (int)rdr["empID"];
                            t.emSt = rdr["emSt"] == DBNull.Value ? -1 : (int)rdr["emSt"];
                            t.deviceID = rdr["deviceID"] == DBNull.Value ? "" : rdr["deviceID"].ToString();
                            t.rgSt = rdr["rgSt"] == DBNull.Value ? false : (bool)rdr["rgSt"];
                            t.gfID = rdr["gfID"] == DBNull.Value ? -1 : (int)rdr["gfID"];
                            t.gfSt = rdr["gfSt"] == DBNull.Value ? false : (bool)rdr["gfSt"];
                            clvh_bioRegistrations.Add(t);
                        }
                    }
                }
                con.Close();
                return clvh_bioRegistrations;
            }
        }
        public static async Task<List<vh_wsGeoFenceCoords>> Readvh_wsGeoFenceCoordsAsync(int cid, int ws) {
            List<vh_wsGeoFenceCoords> clwsGeoFenceCoords = new List<vh_wsGeoFenceCoords>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "sh_wsGeoFenceCoords";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    cmd.Parameters.Add(new SqlParameter("@ws", ws));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            vh_wsGeoFenceCoords t = new vh_wsGeoFenceCoords();
                            t.cid = rdr["cid"] == DBNull.Value ? -1 : (int)rdr["cid"];
                            t.ws = rdr["ws"] == DBNull.Value ? -1 : (int)rdr["ws"];
                            t.id = rdr["id"] == DBNull.Value ? -1 : (int)rdr["id"];
                            t.fenceID = rdr["fenceID"] == DBNull.Value ? "" : rdr["fenceID"].ToString();
                            t.v = rdr["v"] == DBNull.Value ? -1 : (int)rdr["v"];
                            t.x = rdr["x"] == DBNull.Value ? 0 : Convert.ToDouble(rdr["x"]);
                            t.y = rdr["y"] == DBNull.Value ? 0 : Convert.ToDouble(rdr["y"]);
                            clwsGeoFenceCoords.Add(t);
                        }
                    }
                }
                con.Close();
                return clwsGeoFenceCoords;
            }
        } 
        public static async Task<string> DeleteGeoFenceCoordsAsync(int id) {
            List<vh_wsGeoFenceCoords> clwsGeoFenceCoords = new List<vh_wsGeoFenceCoords>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "sh_geofenceDelete";
                int recordsUpdated = 0;
                string res = string.Empty;
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@id", id));
                    cmd.Parameters.Add(new SqlParameter("@res", SqlDbType.VarChar, 200)).Direction = ParameterDirection.Output;;
                    recordsUpdated = await cmd.ExecuteNonQueryAsync();
                    res =  cmd.Parameters["@res"].Value.ToString();
                }
                con.Close();
                return res;
            }
        }
        public static async Task<string> UpdateEmpMasterAsync(List<t_g_employeemaster> clEmpMaster, int cid, int userID) {
            try {
                string resDes = await ReadDesignations(cid);
                string resBanks = await ReadBankBranchesAsync();
                if(resDes != "success" && resBanks != "success" && clDesignations.Count == 0) {
                    return "failed to load designations and/or bank branches";
                } else {
                    DataTable dtEmp = new t_g_employeemaster().dtEmployeemaster("dtEmp");
                    foreach(t_g_employeemaster e in clEmpMaster) {
                        DataRow dr = dtEmp.NewRow();
                        dr["EmpId"] = e.EmpId;
                        dr["DeptNo"] = e.DeptNo;
                        dr["FName"] = e.FName;
                        dr["MName"] = e.MName;
                        dr["LName"] = e.LName;
                        dr["UserName"] = e.UserName;
                        dr["Password"] = DBNull.Value;
                        string des = (from t in clDesignations where t.DesigId == e.DesgID select t.Designation).FirstOrDefault();
                        dr["Designation"] = des;
                        dr["DesgID"] = e.DesgID;//T_G_DesignationMaster table
                        dr["CateId"] = e.CateId;  // from T_G_CategoryMaster table Specialisation/trade
                        dr["Mobile1"] = e.Mobile1;
                        dr["Mobile2"] = e.Mobile2;
                        dr["Status"] = "y";
                        dr["Mailid"] = e.Mailid;
                        dr["AltMail"] = e.AltMail;
                        dr["skypeid"] = e.skypeid;
                        dr["Type"] = e.Type;//T_HR_EmployeeType table 
                        dr["Categary"] = e.Categary; // from WorkSite table
                        dr["Mgnr"] = e.Mgnr;//T_HR_Prj_Dept table
                        dr["Image"] = DBNull.Value;
                        dr["DesigID"] = DBNull.Value;//repetition of this to above "DesgID" property
                        dr["DOB"] = e.DOB;
                        dr["AppID"] = DBNull.Value;
                        dr["Mole1"] = e.Mole1;
                        dr["Mole2"] = e.Mole2;
                        dr["PreOrder"] = DBNull.Value;
                        dr["UpdatedBY"] = userID;
                        dr["UpdatedON"] = DBNull.Value;
                        dr["CreatedOn"] = DBNull.Value;
                        dr["CreatedBy"] = DBNull.Value;
                        dr["EmpNature"] = e.EmpNature;//T_HR_NatureOfEmployment table
                        dr["Gender"] = e.Gender;
                        dr["AccountNumber"] = e.AccountNumber;
                        dr["Shift"] = e.Shift;//t_g_shifts table
                        if(e.BranchID != null) {
                            int bkID = (from t in clBankBranches where t.BranchId == e.BranchID select t.BankID).FirstOrDefault();
                            dr["BankID"] = bkID;
                            dr["BranchID"] = e.BranchID;// T_G_BankBranches
                        } else {
                            dr["BankID"] = DBNull.Value;
                            dr["BranchID"] = DBNull.Value;
                        }
                        string swift = (from t in clBankBranches where t.BranchId == e.BranchID select t.SWIFT).FirstOrDefault();
                        dr["NEFT"] = swift;//IFSC/SWIFT Code
                        dr["StatusChangeOn"] = (object)e.StatusChangeOn ?? DBNull.Value; //Date of marriage
                        dr["StausChangeBy"] = DBNull.Value;
                        dr["StatusChangeRemarks"] = e.StatusChangeRemarks; //Religion
                        dr["RejoinOn"] = DBNull.Value;
                        dr["PANNumber"] = e.PANNumber;
                        dr["OldEmpID"] = e.OldEmpID;
                        dr["PlaceOfBirth"] = e.PlaceOfBirth;
                        dr["POB"] = e.POB;
                        dr["Qualification"] = e.Qualification;
                        dr["Mpassword"] = e.Mpassword;//Race
                        dr["PaymentMode"] = e.PaymentMode;
                        dr["Grade"] = DBNull.Value;
                        dtEmp.Rows.Add(dr);
                    }
                    using(SqlConnection con = new SqlConnection(common.erpCS)) {
                        await con.OpenAsync();
                        SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("blkEmpUpdate"));
                        string query = "sh_empUpdate";
                        int recordsUpdated = 0;
                        using(SqlCommand cmd = new SqlCommand(query, con, tran)) {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@tblType", dtEmp));
                            recordsUpdated = await cmd.ExecuteNonQueryAsync();
                        }
                        if(recordsUpdated > 0) tran.Commit();
                        con.Close();
                        return recordsUpdated.ToString() + " records(s) updated";
                    }
                }
            } catch(Exception ex) { return ex.Message; }
        }
        public static async Task<List<th_otDesignations>> ReadOTDesignationsAsync(int cid) {
            List<th_otDesignations> clth_otDesignations = new List<th_otDesignations>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "select t1.desID, t2.Designation, t1.isSup from th_otDesignations t1 " +
                    "join T_G_DesignationMaster t2 on t1.desID = t2.DesigId where t2.cid =  @cid order by t1.desID";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            th_otDesignations t = new th_otDesignations();
                            t.desID = rdr["desID"] == DBNull.Value ? -1 : (Int16)rdr["desID"];
                            t.Designation = rdr["Designation"] == DBNull.Value ? "" : rdr["Designation"].ToString();
                            t.isSup = rdr["isSup"] == DBNull.Value ? false : (bool)rdr["isSup"];
                            clth_otDesignations.Add(t);
                        }
                    }
                }
                con.Close();
                return clth_otDesignations;
            }
        }
        public static async Task<string> CreateOTDesignations(List<th_otDesignations> clotDesignations, int cid, int userID) {
            try {
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("clotDesignations"));
                    th_otDesignations t = new th_otDesignations();
                    DataTable dtClass = t.dtClass("dtClass");
                    foreach(th_otDesignations E in clotDesignations) {
                        DataRow dr = dtClass.NewRow();
                        dr["desID"] = E.desID;
                        dr["isSup"] = E.isSup;
                        dr["cb"] = userID;
                        dr["co"] = DateTime.Now;
                        dtClass.Rows.Add(dr);
                    }
                    using(SqlBulkCopy bcp = new SqlBulkCopy(con, SqlBulkCopyOptions.Default, tran)) {
                        bcp.DestinationTableName = "[dbo].[th_otDesignations]";
                        try {
                            await bcp.WriteToServerAsync(dtClass);
                            tran.Commit();
                            con.Close();
                            return clotDesignations.Count.ToString() + " record(s) inserted!";
                        } catch(Exception ex) {
                            tran.Rollback();
                            con.Close();
                            return "failed " + ex.Message;
                        }
                    }
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
        public static async Task<string> UpdateOTDesignationsAsync(List<intBool> clDesignations, int cid, int userID) {
            try {
                DataTable dtClass = new intBool().dtClass("dtClass");
                foreach(intBool E in clDesignations) {
                    DataRow dr = dtClass.NewRow();
                    dr["id"] = E.id;
                    dr["bl"] = E.bl;
                    dtClass.Rows.Add(dr);
                }
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("blkUpdate"));
                    string query = "sh_otDesignationsUpdate";
                    int recordsUpdated = 0;
                    using(SqlCommand cmd = new SqlCommand(query, con, tran)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@tblType", dtClass));
                        cmd.Parameters.Add(new SqlParameter("@userID", userID));
                        recordsUpdated = await cmd.ExecuteNonQueryAsync();
                    }
                    if(recordsUpdated > 0) tran.Commit();
                    else return "failed to update!";
                    con.Close();
                    return recordsUpdated.ToString() + " records(s) updated";
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
        public static async Task<string> DeleteOTDesignation(List<int> ids, int userID) {
            DataTable dt = new DataTable();
            dt.Columns.Add("id", typeof(System.Int32));
            foreach(int t in ids) {
                DataRow dr = dt.NewRow();
                dr["id"] = t;
                dt.Rows.Add(dr);
            }
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("blkUpdate"));
                string query = "sh_otDesignationsDelete";
                int recordsUpdated = 0;
                using(SqlCommand cmd = new SqlCommand(query, con, tran)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@tblType", dt));
                    recordsUpdated = await cmd.ExecuteNonQueryAsync();
                }
                if(recordsUpdated > 0) tran.Commit();
                else return "failed to delete!";
                con.Close();
                return recordsUpdated.ToString() + " records(s) deleted";
            }
        }
        public static async Task<List<th_otMalaysia_Incentives>> ReadIncentivesAsync(int cid) {
            List<th_otMalaysia_Incentives> clth_otMalaysia_Incentives = new List<th_otMalaysia_Incentives>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "select t1.* from th_otMalaysia_Incentives t1 where t1.cid = @cid";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    //cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            th_otMalaysia_Incentives t = new th_otMalaysia_Incentives();
                            t.id = rdr["id"] == DBNull.Value ? -1 : (int)rdr["id"];
                            t.bracket = rdr["bracket"] == DBNull.Value ? "" : rdr["bracket"].ToString();
                            t.start = rdr["start"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["start"]);
                            t.finish = rdr["finish"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["finish"]);
                            t.rate = rdr["rate"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["rate"]);
                            t.isNight = rdr["isNight"] == DBNull.Value ? false : (bool)rdr["isNight"];
                            t.cid = rdr["cid"] == DBNull.Value ? -1 : (int)rdr["cid"];
                            clth_otMalaysia_Incentives.Add(t);
                        }
                    }
                }
                con.Close();
                return clth_otMalaysia_Incentives;
            }
        }
        public static async Task<string> UpdateIncentivesAsync(List<th_otMalaysia_Incentives> clth_otMalaysia_Incentives, int cid, int userID) {
            try {
                DataTable dtClass = new th_otMalaysia_Incentives().dtClass("dtClass");
                foreach(th_otMalaysia_Incentives E in clth_otMalaysia_Incentives) {
                    DataRow dr = dtClass.NewRow();
                    dr["id"] = E.id;
                    dr["bracket"] = E.bracket;
                    dr["start"] = E.start;
                    dr["finish"] = E.finish;
                    dr["rate"] = E.rate;
                    dr["isNight"] = E.isNight;
                    dr["cid"] = E.cid;
                    dtClass.Rows.Add(dr);
                }
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("blkUpdate"));
                    string query = "sh_th_otMalaysia_IncentivesUpdate";
                    int recordsUpdated = 0;
                    using(SqlCommand cmd = new SqlCommand(query, con, tran)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@tblType", dtClass));
                        cmd.Parameters.Add(new SqlParameter("@userID", userID));
                        recordsUpdated = await cmd.ExecuteNonQueryAsync();
                    }
                    if(recordsUpdated > 0) tran.Commit();
                    else return "failed to update!";
                    con.Close();
                    return recordsUpdated.ToString() + " records(s) updated";
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
        public static async Task<List<vh_employeesOnOT>> Readvh_employeesOnOTAsync(int cid) {
            List<vh_employeesOnOT> clvh_employeesOnOT = new List<vh_employeesOnOT>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "select t1.* from vh_employeesOnOT t1 join T_G_EmployeeMaster t2 on t1.EmpID = t2.EmpId where t2.cid = @cid";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            vh_employeesOnOT t = new vh_employeesOnOT();
                            t.cid = rdr["cid"] == DBNull.Value ? -1 : (int)rdr["cid"];
                            t.empID = rdr["empID"] == DBNull.Value ? -1 : (int)rdr["empID"];
                            t.FullName = rdr["FullName"] == DBNull.Value ? "" : rdr["FullName"].ToString();
                            t.desID = rdr["desID"] == DBNull.Value ? -1 : (Int16)rdr["desID"];
                            t.Designation = rdr["Designation"] == DBNull.Value ? "" : rdr["Designation"].ToString();
                            t.Nature = rdr["Nature"] == DBNull.Value ? "" : rdr["Nature"].ToString();
                            t.isSup = rdr["isSup"] == DBNull.Value ? false : (bool)rdr["isSup"];
                            clvh_employeesOnOT.Add(t);
                        }
                    }
                }
                con.Close();
                return clvh_employeesOnOT;
            }
        }
        public static async Task<List<vh_otParameters>> ReadOTParametersAsync(int cid) {
            List<vh_otParameters> clvh_otParameters = new List<vh_otParameters>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "select t1.* from vh_otParameters t1 join T_G_EmployeeMaster t2 on t1.EmpID = t2.EmpId where t2.cid = @cid";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            vh_otParameters t = new vh_otParameters();
                            t.EmpID = rdr["EmpID"] == DBNull.Value ? -1 : (int)rdr["EmpID"];
                            t.DesgID = rdr["DesgID"] == DBNull.Value ? -1 : (Int16)rdr["DesgID"];
                            t.AttDate = rdr["AttDate"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["AttDate"]);
                            t.inTime = rdr["inTime"] == DBNull.Value ? "" : rdr["inTime"].ToString();
                            t.outTime = rdr["outTime"] == DBNull.Value ? "" : rdr["outTime"].ToString();
                            t.nat = rdr["nat"] == DBNull.Value ? -1 : (int)rdr["nat"];
                            t.shift = rdr["shift"] == DBNull.Value ? -1 : (Int16)rdr["shift"];
                            t.Basic = rdr["Basic"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["Basic"]);
                            t.wkHrs = rdr["wkHrs"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["wkHrs"]);
                            t.shHrs = rdr["shHrs"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["shHrs"]);
                            t.isSplDay = rdr["isSplDay"] == DBNull.Value ? false : (bool)rdr["isSplDay"];
                            t.splWkHrs = rdr["splWkHrs"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["splWkHrs"]);
                            t.isWO = rdr["isWO"] == DBNull.Value ? false : (bool)rdr["isWO"];
                            t.isPH = rdr["isPH"] == DBNull.Value ? false : (bool)rdr["isPH"];
                            t.br = rdr["br"] == DBNull.Value ? false : (bool)rdr["br"];
                            clvh_otParameters.Add(t);
                        }
                    }
                }
                con.Close();
                return clvh_otParameters;
            }
        }
        public static async Task<List<th_otMalaysia_Workers>> ReadotMalaysia_WorkersAsync(int cid) {
            List<th_otMalaysia_Workers> clth_otMalaysia_Workers = new List<th_otMalaysia_Workers>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "select t1.* from th_otMalaysia_Workers t1  where t1.cid = @cid";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            th_otMalaysia_Workers t = new th_otMalaysia_Workers();
                            t.id = rdr["id"] == DBNull.Value ? -1 : (int)rdr["id"];
                            t.salUpto = rdr["salUpto"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["salUpto"]);
                            t.NorRateEx = rdr["NorRateEx"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["NorRateEx"]);
                            t.woRate = rdr["woRate"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["woRate"]);
                            t.woRateEx = rdr["woRateEx"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["woRateEx"]);
                            t.phRate = rdr["phRate"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["phRate"]);
                            t.phRatEx = rdr["phRatEx"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["phRatEx"]);
                            t.cid = rdr["cid"] == DBNull.Value ? -1 : (int)rdr["cid"];
                            t.ia = rdr["ia"] == DBNull.Value ? false : (bool)rdr["ia"];
                            clth_otMalaysia_Workers.Add(t);
                        }
                    }
                }
                con.Close();
                return clth_otMalaysia_Workers;
            }
        }
        public static async Task<List<vh_otSupervisors>> ReadotSupervisorsAsync(int cid, DateTime st, DateTime ed) {
            List<vh_otSupervisors> clvh_otSupervisors = new List<vh_otSupervisors>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "sh_otSupervisors";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    cmd.Parameters.Add(new SqlParameter("@st", st));
                    cmd.Parameters.Add(new SqlParameter("@ed", ed));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            vh_otSupervisors t = new vh_otSupervisors();
                            t.attDate = rdr["attDate"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["attDate"]);
                            t.empID = rdr["empID"] == DBNull.Value ? -1 : (int)rdr["empID"];
                            t.DesgID = rdr["DesgID"] == DBNull.Value ? -1 : (Int16)rdr["DesgID"];
                            t.InTime = rdr["InTime"] == DBNull.Value ? "" : rdr["InTime"].ToString();
                            t.OutTime = rdr["OutTime"] == DBNull.Value ? "" : rdr["OutTime"].ToString();
                            t.wkHrs = rdr["wkHrs"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["wkHrs"]);
                            t.shHrs = rdr["shHrs"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["shHrs"]);
                            t.br = rdr["br"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["br"]);
                            t.isWO = rdr["isWO"] == DBNull.Value ? false : (bool)rdr["isWO"];
                            t.isPH = rdr["isPH"] == DBNull.Value ? false : (bool)rdr["isPH"];
                            t.isSplDay = rdr["isSplDay"] == DBNull.Value ? false : (bool)rdr["isSplDay"];
                            t.Basic = rdr["Basic"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["Basic"]);
                            t.OT = rdr["OT"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["OT"]);
                            t.Bkt1 = rdr["Bkt1"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["Bkt1"]);
                            t.Rate1 = rdr["Rate1"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["Rate1"]);
                            t.Bkt2 = rdr["Bkt2"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["Bkt2"]);
                            t.Rate2 = rdr["Rate2"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["Rate2"]);
                            t.NgtAlw = rdr["NgtAlw"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["NgtAlw"]);
                            clvh_otSupervisors.Add(t);
                        }
                    }
                }
                con.Close();
                return clvh_otSupervisors;
            }
        }
        public static async Task<List<vh_otWorkers>> ReadotWorkersAsync(int cid, DateTime st, DateTime ed) {
            List<vh_otWorkers> clvh_otWorkers = new List<vh_otWorkers>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "sh_otWorkers";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    cmd.Parameters.Add(new SqlParameter("@st", st));
                    cmd.Parameters.Add(new SqlParameter("@ed", ed));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            vh_otWorkers t = new vh_otWorkers();
                            t.attDate = rdr["attDate"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["attDate"]);
                            t.empID = rdr["empID"] == DBNull.Value ? -1 : (int)rdr["empID"];
                            t.DesgID = rdr["DesgID"] == DBNull.Value ? -1 : (Int16)rdr["DesgID"];
                            t.InTime = rdr["InTime"] == DBNull.Value ? "" : rdr["InTime"].ToString();
                            t.OutTime = rdr["OutTime"] == DBNull.Value ? "" : rdr["OutTime"].ToString();
                            t.wkHrs = rdr["wkHrs"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["wkHrs"]);
                            t.shHrs = rdr["shHrs"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["shHrs"]);
                            t.br = rdr["br"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["br"]);
                            t.isWO = rdr["isWO"] == DBNull.Value ? false : (bool)rdr["isWO"];
                            t.isPH = rdr["isPH"] == DBNull.Value ? false : (bool)rdr["isPH"];
                            t.isSplDay = rdr["isSplDay"] == DBNull.Value ? false : (bool)rdr["isSplDay"];
                            t.Basic = rdr["Basic"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["Basic"]);
                            t.OT = rdr["OT"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["OT"]);
                            t.nrRateEx = rdr["nrRateEx"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["nrRateEx"]);
                            t.woRate = rdr["woRate"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["woRate"]);
                            t.woRateEx = rdr["woRateEx"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["woRateEx"]);
                            t.phRate = rdr["phRate"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["phRate"]);
                            t.phRateEx = rdr["phRateEx"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["phRateEx"]);
                            clvh_otWorkers.Add(t);
                        }
                    }
                }
                con.Close();
                return clvh_otWorkers;
            }
        }
    }
}