﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Login.Controllers {
    public class EmployeeController :ApiController {
        [HttpGet]
        [Route("api/employee/master")]// http://localhost/Login/api/employee/master?cid=1&st=y
        public async Task<HttpResponseMessage> getEmployees(int cid, string st) {
            try {
                List<t_g_employeemaster> empList = await Task.Run(() => DAL.ReadEmployeeMaster(cid, st));
                if(empList.Count != 0) {
                    return Request.CreateResponse(HttpStatusCode.OK, empList);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Empty List!");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost]
        [Route("api/employee/bulkInsert")]// http://localhost/Login/api/employee/bulkInsert?cid=1&userID=1
        public async Task<HttpResponseMessage> BulkInsertEmployees([FromBody]List<t_g_employeemaster> clEmployees, int cid, int userID) {
            try {
                // [{ EmpId:1,DeptNo:1,FName:'F111X',MName:'',LName:'L111X',UserName:'F111X', DesgID:1,CateId:1,Mobile1:'1234567890',Mailid :'FirstName@gmail.com',Type :5,Categary:1 ,Mgnr :1, DOB  :'01/01/2000',Mole1:'', EmpNature:1,Gender :'M', Shift:1, AccountNumber:'123456789000',BranchID:1,PANNumber:null,OldEmpID:'A123', PlaceOfBirth:'garikapadu',Qualification:'BTECH', PaymentMode :1  }]
                if(clEmployees.Count > 0) {
                    string result = await Task.Run(() => DAL.CreateEmployeesBulkAsync(clEmployees, cid, userID));
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No records to insert");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpPut]
        [Route("api/employee/bulkUpdate")]
        public async Task<HttpResponseMessage> BulkUpdateEmployees([FromBody]List<t_g_employeemaster> clEmployees, int cid, int userID) {
            // http://localhost/Login/api/employee/bulkUpdate?cid=1&userID=1 // 
            // [{"EmpId":9,"DeptNo":2,"FName":"Amit","MName":"","LName":"Meena","UserName":"amit@6dproptech.com","DesgID":6,"CateId":3,"Mobile1":"8977567000","Mailid":"yudhi@aeclogic.com","Type":1,"Categary":1,"Mgnr":9,"Mole1": "EPF1234567", "DOB":"1982-06-05","StatusChangeOn":"1982-06-05","StatusChangeRemarks":"Muslim","EmpNature":1,"Gender":"M","Shift":1,"AccountNumber":"","BranchID":"002","PANNumber":"","OldEmpID":"","PlaceOfBirth":"","Qualification":"MBA","Mpassword":"Malaysian","PaymentMode":1}]
            try {   
                if(clEmployees.Count > 0) {
                    string result = await Task.Run(() => DAL.UpdateEmpMasterAsync(clEmployees, cid, userID));
                    if(!result.Contains("failed")) {
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No records to insert");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }
    
    public class rankEmpType {
        public int rank { get; set; }
        public int tyID { get; set; }
        public rankEmpType(int _rank, int _tyID) { rank = _rank; tyID = _tyID; }
    }
    public class t_a_ledgers {
        public int ledgerID { get; set; }
        public int GroupId { get; set; }
        public string Ledger { get; set; }
        public string LedgerAlias { get; set; } //null
        public string InventoryEffected { get; set; } //null
        public decimal Bitsstatus { get; set; }
        public decimal CompanyID { get; set; }
        public int Category { get; set; } //null
        public string code { get; set; } //null
        public int ssid { get; set; }

        public t_a_ledgers() { }        public t_a_ledgers(int _ledgerID, int _GroupId, string _Ledger, string _LedgerAlias, string _InventoryEffected            , decimal _Bitsstatus, decimal _CompanyID, int _Category, string _code, int _ssid) {
            ledgerID = _ledgerID; GroupId = _GroupId; Ledger = _Ledger; LedgerAlias = _LedgerAlias; InventoryEffected = _InventoryEffected;
            Bitsstatus = _Bitsstatus; CompanyID = _CompanyID; Category = _Category; code = _code; ssid = _ssid;        }        public DataTable dtLedgers(string tblName) {            DataTable dtName = new DataTable(tblName);            DataColumn ledgerID = new DataColumn();
            ledgerID.DataType = System.Type.GetType("System.Int32");            ledgerID.ColumnName = "ledgerID";            dtName.Columns.Add(ledgerID);            dtName.Columns.Add("GroupId", typeof(System.Int32));            dtName.Columns.Add("Ledger", typeof(System.String));            dtName.Columns.Add("LedgerAlias", typeof(System.String));            dtName.Columns.Add("InventoryEffected", typeof(System.String));            dtName.Columns.Add("Bitsstatus", typeof(System.Decimal));            dtName.Columns.Add("CompanyID", typeof(System.Decimal));            dtName.Columns.Add("Category", typeof(System.Int32));            dtName.Columns.Add("code", typeof(System.String));
            dtName.Columns.Add("ssid", typeof(System.Int32));
            DataColumn[] keys = new DataColumn[1];            keys[0] = ledgerID;            dtName.PrimaryKey = keys;            return dtName;        }    }
    public class t_g_employeeledgermapping {
        public int MapId { get; set; }
        public int EmpId { get; set; }
        public int LedgerId { get; set; }
        public t_g_employeeledgermapping() { }        public t_g_employeeledgermapping(int _MapId, int _EmpId, int _LedgerId) {            MapId = _MapId; EmpId = _EmpId; LedgerId = _LedgerId;
        }        public DataTable dtEmployeeledgermapping(string tblName) {            DataTable dtName = new DataTable(tblName);            DataColumn MapId = new DataColumn();
            MapId.DataType = System.Type.GetType("System.Int32");            MapId.ColumnName = "MapId";            dtName.Columns.Add(MapId);            dtName.Columns.Add("EmpId", typeof(System.Int32));            dtName.Columns.Add("LedgerId", typeof(System.Int32));
            DataColumn[] keys = new DataColumn[1];            keys[0] = MapId;            dtName.PrimaryKey = keys;            return dtName;        }
    }
    public class T_HR_Prj_Manager {
        public int PrjId { get; set; }
        public int MgnrId { get; set; }
        public int Status { get; set; }
        public DateTime CreatedOn { get; set; } //null
        public int CreatedBy { get; set; } //null
        public int UpdatedBy { get; set; } //null
        public DateTime UpdatedOn { get; set; }//null 
        public T_HR_Prj_Manager() { }        public T_HR_Prj_Manager(int _PrjId, int _MgnrId, int _Status) { PrjId = _PrjId; MgnrId = _MgnrId; Status = _Status; }        public T_HR_Prj_Manager(int _PrjId, int _MgnrId, int _Status, DateTime _CreatedOn, int _CreatedBy, int _UpdatedBy, DateTime _UpdatedOn) {
            PrjId = _PrjId; MgnrId = _MgnrId; Status = _Status; CreatedOn = _CreatedOn; CreatedBy = _CreatedBy; UpdatedBy = _UpdatedBy; UpdatedOn = _UpdatedOn;
        }        public DataTable dtPrj_Manager(string tblName) {            DataTable dtName = new DataTable(tblName);            dtName.Columns.Add("PrjId", typeof(System.Int32));            dtName.Columns.Add("MgnrId", typeof(System.Int32));            dtName.Columns.Add("Status", typeof(System.Byte));
            dtName.Columns.Add("CreatedOn", typeof(System.DateTime));
            dtName.Columns.Add("CreatedBy", typeof(System.Int32));
            dtName.Columns.Add("UpdatedBy", typeof(System.Int32));
            dtName.Columns.Add("UpdatedOn", typeof(System.DateTime));
            return dtName;        }    }
    public class t_hr_prj_dept {
        public int PrjID { get; set; }
        public int DeptID { get; set; }
        public int HeadID { get; set; }
        public int Status { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBY { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBY { get; set; }
        public t_hr_prj_dept() { }        public t_hr_prj_dept(int _PrjID, int _DeptID, int _HeadID, int _Status) { PrjID = _PrjID; DeptID = _DeptID; HeadID = _HeadID; Status = _Status; }        public t_hr_prj_dept(int _PrjID, int _DeptID, int _HeadID, int _Status, DateTime _UpdatedOn, int _UpdatedBY, DateTime _CreatedOn, int _CreatedBY) { // 
            PrjID = _PrjID; DeptID = _DeptID; HeadID = _HeadID; Status = _Status; UpdatedOn = _UpdatedOn; UpdatedBY = _UpdatedBY;
            CreatedOn = _CreatedOn; CreatedBY = _CreatedBY;
        }        public DataTable dtPrj_dept(string tblName) {            DataTable dtName = new DataTable(tblName);            DataColumn PrjID = new DataColumn();
            dtName.Columns.Add("PrjID", typeof(System.Int32));            dtName.Columns.Add("DeptID", typeof(System.Int32));            dtName.Columns.Add("HeadID", typeof(System.Int32));            dtName.Columns.Add("Status", typeof(System.Byte));
            dtName.Columns.Add("UpdatedOn", typeof(System.DateTime));
            dtName.Columns.Add("UpdatedBY", typeof(System.Int32));
            dtName.Columns.Add("CreatedOn", typeof(System.DateTime));
            dtName.Columns.Add("CreatedBY", typeof(System.Int32));
            return dtName;        }
    }
    public class t_g_employeemaster {
        public int      EmpId               { get; set; }
        public int      DeptNo              { get; set; }
        public string   FName               { get; set; }
        public string   MName               { get; set; }
        public string   LName               { get; set; }
        public string   UserName            { get; set; }
        public string   Password            { get; set; }
        public string   Designation         { get; set; }
        public int      DesgID              { get; set; }//short
        public int      CateId              { get; set; }//short
        public string   Mobile1             { get; set; }
        public string   Mobile2             { get; set; }
        public string   Status              { get; set; }
        public string   Mailid              { get; set; }
        public string   AltMail             { get; set; }
        public string   skypeid             { get; set; }
        public int      Type                { get; set; }
        public int      Categary            { get; set; }
        public int      Mgnr                { get; set; }
        public string   Image               { get; set; }
        public int      DesigID             { get; set; }
        public DateTime DOB                 { get; set; }
        public int      AppID               { get; set; }
        public string   Mole1               { get; set; }
        public string   Mole2               { get; set; }
        public int      PreOrder            { get; set; }//short
        public int      UpdatedBY           { get; set; }
        public DateTime UpdatedON           { get; set; }
        public DateTime CreatedOn           { get; set; }
        public int      CreatedBY           { get; set; }
        public int      EmpNature           { get; set; }
        public string   Gender              { get; set; }
        public string   AccountNumber       { get; set; }
        public int      Shift               { get; set; }//short
        public int?     BankID              { get; set; }
        public int?     BranchID            { get; set; }
        public string   NEFT                { get; set; }
        public DateTime? StatusChangeOn     { get; set; }
        public int      StausChangeBy       { get; set; }
        public string   StatusChangeRemarks { get; set; }
        public DateTime RejoinOn            { get; set; }
        public string   PANNumber           { get; set; }
        public string   OldEmpID            { get; set; }
        public string   PlaceOfBirth        { get; set; }
        public int      POB                 { get; set; }
        public string   Qualification       { get; set; }
        public string   Mpassword           { get; set; }
        public int      PaymentMode         { get; set; }
        public int      sgID                { get; set; }
        public int      Grade               { get; set; }
        public int      cid                 { get; set; }

        public t_g_employeemaster() { }

        public t_g_employeemaster(int _EmpId, int _DeptNo, string _FName, string _MName, string _LName, string _UserName, string _Password, string _Designation
        , short _DesgID, short _CateId, string _Mobile1, string _Mobile2, string _Status, string _Mailid, string _AltMail, string _skypeid, int _Type
        , int _Categary, int _Mgnr, string _Image, int _DesigID, DateTime _DOB, int _AppID, string _Mole1, string _Mole2, short _PreOrder, int _UpdatedBY
        , DateTime _UpdatedON, DateTime _CreatedOn, int _CreatedBY, int _EmpNature, string _Gender, string _AccountNumber, short _Shift, int? _BankID, int? _BranchID
        , string _NEFT, DateTime? _StatusChangeOn, int _StausChangeBy, string _StatusChangeRemarks, DateTime _RejoinOn, string _PANNumber, string _OldEmpID
        , string _PlaceOfBirth, int _POB, string _Qualification, string _Mpassword, int _PaymentMode, int _sgID, int _Grade, int _cid
        ) {
            DeptNo = _DeptNo; FName = _FName; MName = _MName; LName = _LName; UserName = _UserName; Password = _Password; Designation = _Designation;
            DesgID = _DesgID; CateId = _CateId; Mobile1 = _Mobile1; Mobile2 = _Mobile2; Status = _Status; Mailid = _Mailid; AltMail = _AltMail;
            skypeid = _skypeid; Type = _Type; Categary = _Categary; Mgnr = _Mgnr; Image = _Image; DesigID = _DesigID; DOB = _DOB;
            AppID = _AppID; Mole1 = _Mole1; Mole2 = _Mole2; PreOrder = _PreOrder; UpdatedBY = _UpdatedBY; UpdatedON = _UpdatedON; CreatedOn = _CreatedOn;
            CreatedBY = _CreatedBY; EmpNature = _EmpNature; Gender = _Gender; AccountNumber = _AccountNumber; Shift = _Shift; BankID = _BankID;
            BranchID = _BranchID; NEFT = _NEFT; StatusChangeOn = _StatusChangeOn; StausChangeBy = _StausChangeBy; StatusChangeRemarks = _StatusChangeRemarks;
            RejoinOn = _RejoinOn; PANNumber = _PANNumber; OldEmpID = _OldEmpID; PlaceOfBirth = _PlaceOfBirth; POB = _POB; Qualification = _Qualification;
            Mpassword = _Mpassword; PaymentMode = _PaymentMode; sgID = _sgID; Grade = _Grade; cid = _cid;
        }

        public DataTable dtEmployeemaster(string tblName) {
            DataTable dtName = new DataTable(tblName);
            DataColumn EmpId = new DataColumn();
            EmpId.DataType = System.Type.GetType("System.Int32");
            EmpId.ColumnName = "EmpId";
            dtName.Columns.Add(EmpId);

            dtName.Columns.Add("DeptNo", typeof(System.Int32));
            dtName.Columns.Add("FName", typeof(System.String));
            dtName.Columns.Add("MName", typeof(System.String));
            dtName.Columns.Add("LName", typeof(System.String));
            dtName.Columns.Add("UserName", typeof(System.String));
            dtName.Columns.Add("Password", typeof(System.String));
            dtName.Columns.Add("Designation", typeof(System.String));
            dtName.Columns.Add("DesgID", typeof(System.Int16));
            dtName.Columns.Add("CateId", typeof(System.Int16));
            dtName.Columns.Add("Mobile1", typeof(System.String));
            dtName.Columns.Add("Mobile2", typeof(System.String));
            dtName.Columns.Add("Status", typeof(System.String));
            dtName.Columns.Add("Mailid", typeof(System.String));
            dtName.Columns.Add("AltMail", typeof(System.String));
            dtName.Columns.Add("skypeid", typeof(System.String));
            dtName.Columns.Add("Type", typeof(System.Int32));
            dtName.Columns.Add("Categary", typeof(System.Int32));
            dtName.Columns.Add("Mgnr", typeof(System.Int32));
            dtName.Columns.Add("Image", typeof(System.String));
            dtName.Columns.Add("DesigID", typeof(System.Int32));
            dtName.Columns.Add("DOB", typeof(System.DateTime));
            dtName.Columns.Add("AppID", typeof(System.Int32));
            dtName.Columns.Add("Mole1", typeof(System.String));
            dtName.Columns.Add("Mole2", typeof(System.String));
            dtName.Columns.Add("PreOrder", typeof(System.Int16));
            dtName.Columns.Add("UpdatedBY", typeof(System.Int32));
            dtName.Columns.Add("UpdatedON", typeof(System.DateTime));
            dtName.Columns.Add("CreatedOn", typeof(System.DateTime));
            dtName.Columns.Add("CreatedBY", typeof(System.Int32));
            dtName.Columns.Add("EmpNature", typeof(System.Int32));
            dtName.Columns.Add("Gender", typeof(System.String));
            dtName.Columns.Add("AccountNumber", typeof(System.String));
            dtName.Columns.Add("Shift", typeof(System.Int16));
            dtName.Columns.Add("BankID", typeof(System.Int32)).AllowDBNull=true;
            dtName.Columns.Add("BranchID", typeof(System.Int32)).AllowDBNull=true;
            dtName.Columns.Add("NEFT", typeof(System.String));
            dtName.Columns.Add("StatusChangeOn", typeof(System.DateTime)).AllowDBNull =true;
            dtName.Columns.Add("StausChangeBy", typeof(System.Int32));
            dtName.Columns.Add("StatusChangeRemarks", typeof(System.String));
            dtName.Columns.Add("RejoinOn", typeof(System.DateTime));
            dtName.Columns.Add("PANNumber", typeof(System.String));
            dtName.Columns.Add("OldEmpID", typeof(System.String));
            dtName.Columns.Add("PlaceOfBirth", typeof(System.String));
            dtName.Columns.Add("POB", typeof(System.Int32));
            dtName.Columns.Add("Qualification", typeof(System.String));
            dtName.Columns.Add("Mpassword", typeof(System.String));
            dtName.Columns.Add("PaymentMode", typeof(System.Int32));
            dtName.Columns.Add("sgID", typeof(System.Int32));
            dtName.Columns.Add("Grade", typeof(System.Int32));
            dtName.Columns.Add("cid", typeof(System.Int32));
            DataColumn[] keys = new DataColumn[1];
            keys[0] = EmpId;
            dtName.PrimaryKey = keys;
            return dtName;
        }
    }

}

