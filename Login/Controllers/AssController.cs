﻿using SD.COMMON.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Login {
    public class AssignmentsController :ApiController {
        [HttpGet]
        public List<assignment> GetAssignments(int pID, int? gpID, int? tID) { //http://localhost/Login/api/Assignments?pID=1&gpID=1&tID=1
            List<assignment> list = new List<assignment>();
            SqlParameter[] sps = new SqlParameter[3];
            sps[0] = new SqlParameter("@pID", pID);
            sps[1] = new SqlParameter("@gpID", gpID);
            sps[2] = new SqlParameter("@tID", tID);
            DataSet ds = SQLDBUtil.ExecuteDataset("sb_Assignments", sps);
            foreach(DataRow dr in ds.Tables[0].Rows) {
                assignment t = new assignment((int)dr["GID"], (int)dr["Seq"], (int)dr["TID"], (int)dr["AID"], (int)dr["CID"], dr["CategoryName"].ToString(),
                (int)dr["RID"], dr["ResourceName"].ToString(), (int)dr["UID"], dr["Unit"].ToString(), Convert.ToDouble(dr["Rate"]), Convert.ToDouble(dr["LQ"]), Convert.ToDouble(dr["LR"]), Convert.ToDouble(dr["FQ"]),
                Convert.ToDouble(dr["FR"]), Convert.ToDouble(dr["Coeff"]), dr["Remarks"].ToString(), Convert.ToDouble(dr["Cost"]), Convert.ToDouble(dr["Wastage"]));
                list.Add(t);
            }
            return list;
        }
        [HttpPost]
        [Route("api/Assignments/Add")]
        public HttpResponseMessage Add([FromBody] List<int> rIDs, int tID, int userID) {
            try {   //http://localhost/Login/api/assignments/add?tID=1&userID=1 >> fromBody [91,95]
                DataTable dt = new DataTable();
                dt.Columns.Add("id", typeof(System.Int32));
                foreach(int item in rIDs) {
                    DataRow dr = dt.NewRow(); dr[0] = item; dt.Rows.Add(dr);
                }
                SqlParameter[] sps = new SqlParameter[3];
                sps[0] = new SqlParameter("@TaskID", tID);
                sps[1] = new SqlParameter("@RIDs", dt);
                sps[2] = new SqlParameter("@UserID", userID);
                DataSet ds = SQLDBUtil.ExecuteDataset("sb_assignmentsAdd", sps);
                if(ds.Tables[0].Rows.Count > 0)
                    return Request.CreateResponse(HttpStatusCode.OK, ds.Tables[0].Rows[0][0].ToString());
                else return Request.CreateResponse(HttpStatusCode.BadRequest, "Unknown error from database");
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost]
        [Route("api/Assignments/Edit")]
        public HttpResponseMessage Edit([FromBody] List<tyAssignment> ags, int userID) {
            try { //http://localhost/Login/api/assignments/edit?userID=1 [{aid:2,Coeff:0.57,Remarks:'bulk update2',LeadResID:0, Wastage:0}, {aid:3,Coeff:1.07,Remarks:'bulk update2',LeadResID:0, Wastage:0}]
                DataTable dt = new DataTable();
                dt.Columns.Add("aid", typeof(System.Int32));
                dt.Columns.Add("Coeff", typeof(System.Double));
                dt.Columns.Add("Remarks", typeof(System.String));
                dt.Columns.Add("LeadResID", typeof(System.Int32));
                dt.Columns.Add("Wastage", typeof(System.Double));
                foreach(tyAssignment at in ags) {
                    DataRow dr = dt.NewRow();
                    dr["aid"] = at.aid; dr["Coeff"] = at.Coeff; dr["Remarks"] = at.Remarks;
                    dr["LeadResID"] = at.LeadResID; dr["Wastage"] = at.Wastage;
                    dt.Rows.Add(dr);
                }
                SqlParameter[] sps = new SqlParameter[2];
                sps[0] = new SqlParameter("@ags", dt);
                sps[1] = new SqlParameter("@UserID", userID);
                DataSet ds = SQLDBUtil.ExecuteDataset("sb_assignmentsUpdate", sps);
                if(ds.Tables[0].Rows.Count > 0)
                    return Request.CreateResponse(HttpStatusCode.OK, ds.Tables[0].Rows[0][0].ToString());
                else return Request.CreateResponse(HttpStatusCode.BadRequest, "Unknown error from database");
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost]
        [Route("api/Assignments/Delete")]
        public HttpResponseMessage Delete([FromBody] List<int> aids, int userID) {
            try {   //http://localhost/Login/api/assignments/Delete?userID=1 [1,2]
                DataTable dt = new DataTable();
                dt.Columns.Add("id", typeof(System.Int32));
                foreach(int item in aids) {
                    DataRow dr = dt.NewRow(); dr[0] = item; dt.Rows.Add(dr);
                }
                SqlParameter[] sps = new SqlParameter[2];
                sps[0] = new SqlParameter("@aids", dt);
                sps[1] = new SqlParameter("@UserID", userID);
                DataSet ds = SQLDBUtil.ExecuteDataset("sb_assignmentsDelete", sps);
                if(ds.Tables[0].Rows.Count > 0)
                    return Request.CreateResponse(HttpStatusCode.OK, ds.Tables[0].Rows[0][0].ToString());
                else return Request.CreateResponse(HttpStatusCode.BadRequest, "Unknown error from database");
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost]
        [Route("api/assignments/copy")]
        public HttpResponseMessage copy(int stID, [FromBody] List<int> ttIDs, int userID) {
            try {   // http://localhost/Login/api/assignments/copy?stID=1&userID=1 >> fromBody [91,95]
                DataTable dt = new DataTable();
                dt.Columns.Add("id", typeof(System.Int32));
                foreach(int item in ttIDs) {
                    DataRow dr = dt.NewRow(); dr[0] = item; dt.Rows.Add(dr);
                }
                SqlParameter[] sps = new SqlParameter[3];
                sps[0] = new SqlParameter("@srcTaskID", stID);
                sps[1] = new SqlParameter("@tgtTaskIDs", dt);
                sps[2] = new SqlParameter("@UserID", userID);
                DataSet ds = SQLDBUtil.ExecuteDataset("sb_assignmentsCopy", sps);
                if(ds.Tables[0].Rows.Count > 0)
                    return Request.CreateResponse(HttpStatusCode.OK, ds.Tables[0].Rows[0][0].ToString());
                else return Request.CreateResponse(HttpStatusCode.BadRequest, "Unknown error from database");
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        // (@srcPID int,@srcTaskID int,@tgtPID int, @tgtTaskIDs ty_ID readonly, @UserID int) 
        [HttpPost] [Route("api/assignments/copyFrom")]
        public HttpResponseMessage assignmentsCopyFromOtherProject(int srcPID, int srcTaskID, int tgtPID, [FromBody] List<int> tgtTaskIDs, int userID) {
            try {   // http://localhost/Login/api/assignments/copyFrom?srcPID=1&srcTaskID=1&tgtPID=2&userID=1 >> fromBody [2953,2954]
                DataTable dt = new DataTable();
                dt.Columns.Add("id", typeof(System.Int32));
                foreach(int item in tgtTaskIDs) {
                    DataRow dr = dt.NewRow(); dr[0] = item; dt.Rows.Add(dr);
                }
                SqlParameter[] sps = new SqlParameter[5];
                sps[0] = new SqlParameter("@srcPID", srcPID);
                sps[1] = new SqlParameter("@srcTaskID", srcTaskID);
                sps[2] = new SqlParameter("@tgtPID", tgtPID);
                sps[3] = new SqlParameter("@tgtTaskIDs", dt);
                sps[4] = new SqlParameter("@UserID", userID);
                DataSet ds = SQLDBUtil.ExecuteDataset("sb_assignmentsCopyFromOtherProject", sps);
                if(ds.Tables[0].Rows.Count > 0)
                    return Request.CreateResponse(HttpStatusCode.OK, ds.Tables[0].Rows[0][0].ToString());
                else return Request.CreateResponse(HttpStatusCode.BadRequest, "Unknown error from database");
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }
    public class assignment {
        public int GID { get; set; }
        public int Seq { get; set; }
        public int TID { get; set; }
        public int AID { get; set; }
        public int CID { get; set; }
        public string CategoryName { get; set; }
        public int RID { get; set; }
        public string ResourceName { get; set; }
        public int UID { get; set; }
        public string Unit { get; set; }
        public double Rate { get; set; }
        public double LQ { get; set; }
        public double LR { get; set; }
        public double FQ { get; set; }
        public double FR { get; set; }
        public double Coeff { get; set; }
        public string Remarks { get; set; }
        public double Cost { get; set; }
        public double Wastage { get; set; }
        public assignment() { }
        public assignment(int gID, int seq, int tID, int aID, int cID, string categoryName, int rID, string resourceName, int uID, string unit,
            double rate, double lq, double lr, double fq, double fr, double coeff, string remarks, double cost, double wastage) {
            GID = gID; Seq = seq; TID = tID; AID = aID; CID = cID; CategoryName = categoryName; RID = rID; ResourceName = resourceName; UID = uID; Unit = unit;
            Rate = rate; LQ = lq; LR = lr; FQ = fq; FR = fr; Coeff = coeff; Remarks = remarks; Cost = cost; Wastage = wastage;
        }
    }
    public class tyAssignment {
        public int aid { get; set; }
        public double Coeff { get; set; }
        public string Remarks { get; set; }
        public int LeadResID { get; set; }
        public double Wastage { get; set; }
    }
}
