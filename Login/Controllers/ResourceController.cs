﻿using Login.Controllers;
using Login.Models;
using SD.COMMON.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
namespace Login {
    public class ResourcesController :ApiController {
        [HttpGet]//http://localhost/Login/api/Resources 
        public List<t_oms_resources> GetResources() {
            List<t_oms_resources> clResources = new List<t_oms_resources>();
            using(SqlDataReader rdr = SQLDBUtil.ExecuteReader("sb_resources")) {
                while(rdr.Read()) {
                    t_oms_resources res = new t_oms_resources();
                    res.ResourceID = (int)rdr["ResourceID"];
                    res.ResourceName = rdr["ResourceName"].ToString();
                    res.ResourceMake = rdr["ResourceMake"].ToString();
                    res.ResourceUnit = (int)rdr["ResourceUnit"];
                    res.CategoryID = (int)rdr["CategoryID"];
                    res.ResourceRate = Convert.ToDouble(rdr["ResourceRate"]);
                    res.ResourceBasicRate = Convert.ToDouble(rdr["ResourceBasicRate"]);
                    res.ResourceLead = rdr["ResourceLead"] == DBNull.Value ? 0 : Convert.ToDouble(rdr["ResourceLead"]);
                    res.ResourceLeadRate = rdr["ResourceLeadRate"] == DBNull.Value ? 0 : Convert.ToDouble(rdr["ResourceLeadRate"]);
                    res.ResourceLPH = rdr["ResourceLPH"] == DBNull.Value ? 0 : Convert.ToDouble(rdr["ResourceLPH"]);
                    res.ResourcerRemarks = rdr["ResourcerRemarks"] == DBNull.Value ? "" : rdr["ResourcerRemarks"].ToString();
                    res.ResourceOutPut = rdr["ResourceOutPut"] == DBNull.Value ? "" : rdr["ResourceOutPut"].ToString();
                    res.CreatedOn = rdr["CreatedOn"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(rdr["CreatedOn"]);
                    res.CreatedBy = rdr["CreatedBy"] == DBNull.Value ? -1 : (int)rdr["CreatedBy"];
                    res.UpdatedOn = rdr["UpdatedOn"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(rdr["UpdatedOn"]);
                    res.UpdatedBy = rdr["UpdatedBy"] == DBNull.Value ? -1 : (int)rdr["UpdatedBy"];
                    res.IsActive = (int)rdr["IsActive"];
                    res.Extension = rdr["Extension"] == DBNull.Value ? "" : rdr["Extension"].ToString();
                    res.GoodsCategory = rdr["GoodsCategory"] == DBNull.Value ? 1 : (int)rdr["GoodsCategory"];
                    res.DeliveryDays = rdr["DeliveryDays"] == DBNull.Value ? 1 : (byte)rdr["DeliveryDays"];
                    clResources.Add(res);
                }
            }
            return clResources;
        }
        //for other than machinery >> [{GS:1, gpID:2, LedgerID:null, cfgID:-1, clsID:1, rnkID:5, verID:4, duID:8, PropValue:1600, ResourceName :  "Aggregate 12.5 mm", ResourceMake :  "Local", ResourceUnit :  12, CategoryID : 2, ResourceRate : 300, ResourceBasicRate : 300, ResourceLead : 0.00, ResourceLeadRate : 0.00, ResourceLPH : 0.00, ResourcerRemarks : "", ResourceOutPut : "", CreatedBy : 1, UpdatedBy : 1, IsActive : 1, Extension : "", GoodsCategory : 1, DeliveryDays : 1}    ]
        //for machinery >> [{GS:1, gpID:66, LedgerID:null, cfgID:28, clsID:1, rnkID:5, verID:4, duID:8, PropValue:1600, ResourceName :  "Scoda Car", ResourceMake :  "Local", ResourceUnit :  20, CategoryID : 1, ResourceRate : 300, ResourceBasicRate : 1100000, ResourceLead : 0.00, ResourceLeadRate : 0.00, ResourceLPH : 5.00, ResourcerRemarks : "", ResourceOutPut : "", CreatedBy : 1, UpdatedBy : 1, IsActive : 1, Extension : "", GoodsCategory : 1, DeliveryDays : 1}]
        [HttpPost] [Route("api/Resources/bulkInsert")]
        public HttpResponseMessage BulkInsertResources([FromBody]List<Resource> clResources, int cid) {
            if(clResources.Count > 0) {
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    con.Open();
                    using(SqlTransaction tran = con.BeginTransaction()) {
                        int bulkInsertSessionId = InitializeBulkInsertSession(con, tran);
                        int count = BulkInsertFlatRES(clResources, bulkInsertSessionId, con, tran);
                        if(count > 0) {
                            BulkInsertOtherTables(clResources, bulkInsertSessionId, con, tran, cid);
                            CompleteBulkInsertSession(bulkInsertSessionId, con, tran);
                            tran.Commit();
                            con.Close();
                        } else {
                            con.Close();
                            return Request.CreateResponse(HttpStatusCode.BadRequest, "Duplicate Resources found!");
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, clResources.Count.ToString() + " Record(s) Inserted");
            } else return Request.CreateResponse(HttpStatusCode.BadRequest, "No records to insert");
        }
        int InitializeBulkInsertSession(SqlConnection conn, SqlTransaction tran) {
            //1st Trip to database
            SqlCommand cmd = new SqlCommand("INSERT INTO [dbo].[BulkInsertSession]([TsCreated]) VALUES (CURRENT_TIMESTAMP)", conn, tran);
            cmd.ExecuteNonQuery();
            cmd = new SqlCommand("SELECT [SessionID] FROM [dbo].[BulkInsertSession] WHERE @@ROWCOUNT > 0 and [SessionID] = SCOPE_IDENTITY()", conn, tran);
            int bulkInsertSessionId = (int)cmd.ExecuteScalar();
            return bulkInsertSessionId;
        }
        int BulkInsertFlatRES(List<Resource> clResources, int bulkInsertSessionId, SqlConnection conn, SqlTransaction tran) {
            using(SqlBulkCopy bulkCopy = new SqlBulkCopy(conn, SqlBulkCopyOptions.Default, tran)) {
                t_oms_resources omsRes = new t_oms_resources();
                int i = 0;
                DataTable dtResources = omsRes.dataTable("dtResources");
                foreach(Resource R in clResources) {
                    DataRow dr = dtResources.NewRow();
                    dr["ResourceID"] = i++;
                    dr["ResourceName"] = R.ResourceName;
                    dr["ResourceMake"] = R.ResourceMake;
                    dr["ResourceUnit"] = R.ResourceUnit;
                    dr["CategoryID"] = R.CategoryID;
                    dr["ResourceRate"] = R.ResourceRate;
                    dr["ResourceBasicRate"] = R.ResourceBasicRate;
                    dr["ResourceLead"] = R.ResourceLead;
                    dr["ResourceLeadRate"] = R.ResourceLeadRate;
                    dr["ResourceLPH"] = R.ResourceLPH;
                    dr["ResourcerRemarks"] = R.ResourcerRemarks;
                    dr["ResourceOutPut"] = R.ResourceOutPut;
                    dr["CreatedOn"] = DateTime.Now;
                    dr["CreatedBy"] = R.CreatedBy;
                    dr["UpdatedOn"] = DateTime.Now;
                    dr["UpdatedBy"] = R.UpdatedBy;
                    dr["IsActive"] = R.IsActive;
                    dr["Extension"] = R.Extension;
                    dr["GoodsCategory"] = R.GoodsCategory;
                    dr["DeliveryDays"] = R.DeliveryDays;
                    dr["ssid"] = bulkInsertSessionId;
                    dtResources.Rows.Add(dr);
                }
                //2nd trip to Database
                bulkCopy.DestinationTableName = "[dbo].[T_OMS_Resources]";
                bulkCopy.BulkCopyTimeout = 30000;//30 secs
                try { bulkCopy.WriteToServer(dtResources); } catch { return -1; }
            }
            return getInsertedResIDs(clResources, bulkInsertSessionId, conn, tran);
        }
        int getInsertedResIDs(List<Resource> clResources, int bulkInsertSessionId, SqlConnection conn, SqlTransaction tran) {
            SqlCommand cmd = new SqlCommand("SELECT [ResourceID] FROM [dbo].[T_OMS_Resources] WHERE [ssid]=@bulkInsertSessionId ORDER BY [ResourceID] ASC", conn, tran);
            cmd.Parameters.Add(new SqlParameter("@bulkInsertSessionId", bulkInsertSessionId));
            int index = 0;
            using(SqlDataReader reader = cmd.ExecuteReader()) {
                while(reader.Read()) { //iterate the Res IDs and update the foreign keys to OTHER TABLES
                    int dbID = (int)reader[0];//fetched from Database autogenerated after BULK insertion
                    clResources[index].ResourceID = dbID;//update the LOCAL LIST CLASS
                    index++;
                }
            }
            return index;
        }
        void BulkInsertOtherTables(List<Resource> clResources, int bulkInsertSessionId, SqlConnection conn, SqlTransaction tran, int cid) {
            using(SqlBulkCopy bulkCopy = new SqlBulkCopy(conn, SqlBulkCopyOptions.Default, tran)) {
                T_PM_ResVsGroup rvg = new T_PM_ResVsGroup();
                DataTable dtResVsGp = rvg.dtResVsGroup("dtResVsGp");

                T_MMS_MaterialConfig cfg = new T_MMS_MaterialConfig();
                DataTable dtCfg = cfg.dtMaterialConfig("dtCfg");

                T_G_ResourceAttributes att = new T_G_ResourceAttributes();
                DataTable dtAtt = att.dtResourceAttributes("dtAtt");

                int i = 0;
                foreach(Resource res in clResources) {
                    DataRow dr = dtResVsGp.NewRow();
                    dr["ResourceId"] = res.ResourceID;
                    dr["Category_Id"] = res.gpID;
                    dr["Cat_Type"] = res.GS;
                    dr["LedgerId"] = DBNull.Value;
                    dr["cid"] = cid;
                    dtResVsGp.Rows.Add(dr);

                    if(res.cfgID > 0) { // services Cat_Type = 2
                        dr = dtResVsGp.NewRow();
                        dr["ResourceId"] = res.ResourceID;
                        dr["Category_Id"] = res.cfgID;
                        dr["Cat_Type"] = 2;
                        dr["LedgerId"] = DBNull.Value;
                        dr["cid"] = cid;
                        dtResVsGp.Rows.Add(dr);
                    }

                    DataRow dr2 = dtCfg.NewRow();
                    dr2["MCId"] = i++;//auto increment
                    dr2["ResourceID"] = res.ResourceID;
                    dr2["MRId"] = res.rnkID;//importance rank on 'ease of theft 1-10'
                    dr2["ClassingId"] = res.clsID; // Incorporated:1,  Consumable:2, Servicing:4, Secured:6 ;
                    string code = "";
                    if(res.cfgID > 0) {
                        code = "1_" + res.gpID.ToString() + "_2_" + res.cfgID.ToString() + "_" + res.ResourceID.ToString();//both goods and services
                    } else code = res.CategoryID.ToString() + "_" + res.gpID.ToString() + "_" + res.ResourceID.ToString();
                    dr2["Code"] = code;
                    dr2["Period"] = res.verID;
                    dr2["Allowable"] = 0.00;
                    dr2["CreatedOn"] = DateTime.Now;
                    dr2["CreatedBy"] = 1;
                    dr2["UpdatedOn"] = DateTime.Now;
                    dr2["UpdatedBy"] = 1;
                    dr2["IsActive"] = 1;
                    dr2["StockIn"] = 0;//FIFO=0, LIFO=1,
                    dr2["StockOut"] = 0;//FIFO=0, LIFO=1,
                    dr2["NoOfRepetition"] = 1;
                    dr2["Tolerance"] = 0;
                    dr2["ClosingMode"] = 0;//Auto=1, Manual=2   
                    dtCfg.Rows.Add(dr2);

                    DataRow dr3 = dtAtt.NewRow();
                    dr3["ResourceId"] = res.ResourceID;
                    dr3["DUTId"] = res.duID;
                    dr3["Value"] = res.PropValue;
                    dr3["Description"] = "Default Value";
                    dtAtt.Rows.Add(dr3);

                }
                bulkCopy.DestinationTableName = "[dbo].[T_PM_ResVsGroup]";
                bulkCopy.BulkCopyTimeout = 30000;
                bulkCopy.WriteToServer(dtResVsGp);

                bulkCopy.DestinationTableName = "[dbo].[T_MMS_MaterialConfig]";
                bulkCopy.BulkCopyTimeout = 30000;
                bulkCopy.WriteToServer(dtCfg);


                bulkCopy.DestinationTableName = "[dbo].[T_G_ResourceAttributes]";
                bulkCopy.BulkCopyTimeout = 30000;
                bulkCopy.WriteToServer(dtAtt);
            }
        }
        void CompleteBulkInsertSession(int bulkInsertSessionId, SqlConnection conn, SqlTransaction tran) {
            //3rd trip to Database
            SqlCommand cmd = new SqlCommand("UPDATE [dbo].[T_OMS_Resources] SET [ssid] = NULL WHERE [ssid] = @bulkInsertSessionId", conn, tran);
            cmd.Parameters.Add(new SqlParameter("@bulkInsertSessionId", bulkInsertSessionId));
            cmd.ExecuteNonQuery();
            cmd = new SqlCommand("DELETE FROM [dbo].[BulkInsertSession] WHERE [SessionID] = @bulkInsertSessionId", conn, tran);
            cmd.Parameters.Add(new SqlParameter("@bulkInsertSessionId", bulkInsertSessionId));
            cmd.ExecuteNonQuery();
        }
        [HttpPost]
        public HttpResponseMessage xxxxxxxxPost([FromBody] secJson json) {
            try {
                //http://localhost/login/api/Resources >>>> {Section:"Sanitory Works", pid: 1, cid: 1}
                SqlParameter[] sps = new SqlParameter[3];
                sps[0] = new SqlParameter("@sectionName", json.Section);
                sps[1] = new SqlParameter("@pid", json.pid);
                sps[2] = new SqlParameter("@cid", json.cid);
                object id = SQLDBUtil.ExecuteScalar("-------------------------------------", sps);
                if((int)id > 0) {
                    section t = new section((int)id, json.Section);
                    return Request.CreateResponse(HttpStatusCode.OK, t);
                } else
                    return null;
            } catch(Exception ex) {

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        //sb_PullResources2Project
        [HttpPost]
        [Route("api/Resources/PullResources2Project")]
        public HttpResponseMessage PullResources2Project([FromBody]List<int> rIDs, int pid, int userID) {
            try {
                DataTable ResIDs = new DataTable();
                ResIDs.Columns.Add("id", typeof(System.Int32));
                foreach(int i in rIDs) {
                    DataRow dr = ResIDs.NewRow();
                    dr[0] = i; ResIDs.Rows.Add(i);
                }
                List<T_A_Groups> clGroups = new List<T_A_Groups>();
                SqlParameter[] sps = new SqlParameter[3];
                sps[0] = new SqlParameter("@PrjID", pid);
                sps[1] = new SqlParameter("@ResIDs", ResIDs);
                sps[2] = new SqlParameter("@UserID", userID);
                DataSet ds = SQLDBUtil.ExecuteDataset("sb_PullResources2Project", sps);
                string msg = (int)ds.Tables[0].Rows[0][0] > 0 ? ds.Tables[0].Rows[0][0].ToString() + " Project Resource(s) is/are activated! \r" : "";
                msg = msg + ((int)ds.Tables[1].Rows[0][0] > 0 ? ds.Tables[1].Rows[0][0].ToString() + " Resource(s) is/are inserted in to Project Resources!" : "");
                return Request.CreateResponse(HttpStatusCode.OK, msg);
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]
        [Route("api/Resources/ProjectResources")]
        public HttpResponseMessage ProjectResources(int pid) { //http://localhost/Login/api/Resources/ProjectResources?pid=1 
            try {
                SqlParameter[] sps = new SqlParameter[1];
                sps[0] = new SqlParameter("@pid", pid);
                List<Proj_Resource> clPrjResources = new List<Proj_Resource>();
                using(SqlDataReader rdr = SQLDBUtil.ExecuteReader("sb_ProjectResources", sps)) {
                    while(rdr.Read()) {
                        Proj_Resource res = new Proj_Resource();
                        res.PrjID = (int)rdr["PrjID"];
                        res.ResourceID = (int)rdr["ResourceID"];
                        res.ResourceName = rdr["ResourceName"].ToString();
                        res.ResourceMake = rdr["ResourceMake"].ToString();
                        res.ResourceUnit = (int)rdr["ResourceUnit"];
                        res.CategoryID = (int)rdr["CategoryID"];
                        res.ResourceRate = Convert.ToDouble(rdr["ResourceRate"]);
                        res.ResourceBasicRate = Convert.ToDouble(rdr["ResourceBasicRate"]);
                        res.ResourceLead = rdr["ResourceLead"] == DBNull.Value ? 0 : Convert.ToDouble(rdr["ResourceLead"]);
                        res.ResourceLeadRate = rdr["ResourceLeadRate"] == DBNull.Value ? 0 : Convert.ToDouble(rdr["ResourceLeadRate"]);
                        res.ResourceLPH = rdr["ResourceLPH"] == DBNull.Value ? 0 : Convert.ToDouble(rdr["ResourceLPH"]);
                        res.ResourcerRemarks = rdr["ResourcerRemarks"] == DBNull.Value ? "" : rdr["ResourcerRemarks"].ToString();
                        res.ResourceOutPut = rdr["ResourceOutPut"] == DBNull.Value ? "" : rdr["ResourceOutPut"].ToString();
                        res.CreateOn = rdr["CreateOn"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(rdr["CreateOn"]);
                        res.CreateBy = rdr["CreateBy"] == DBNull.Value ? -1 : (int)rdr["CreateBy"];
                        res.IsActive = (bool)rdr["IsActive"];
                        res.Nos = rdr["Nos"] == DBNull.Value ? 0 : (int)(rdr["Nos"]);
                        res.useType = rdr["useType"] == DBNull.Value ? 0 : (int)(rdr["useType"]);
                        clPrjResources.Add(res);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, clPrjResources);
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        //[{PrjID: 1, ResourceID: 1, ResourceName : 'sand Updated',  ResourceMake : 'sand make', ResourceUnit : 12, ResourceRate : 200, ResourceBasicRate : 200, CategoryID : 2, ResourceLead : 2, CreateOn: null, CreateBy: null, IsActive:1, useType:null,
        // ResourceLeadRate : 10, ResourceLPH : 0, ResourcerRemarks : 'Sand remarks', ResourceOutPut : 'output=1', Nos : 0}]
        [HttpPost]
        [Route("api/Resources/PrEdit")] //http://localhost/Login/api/Resources/PrEdit?pid=1&userID=1 
        public HttpResponseMessage UpdateProjectResources([FromBody]List<Proj_Resource> clPrjResources, int pid, int userID) {
            try {
                DataTable dtPR = dtProj_Resource("dtPR");
                foreach(Proj_Resource pr in clPrjResources) {
                    DataRow dr = dtPR.NewRow();
                    dr["ResourceID"] = pr.ResourceID;
                    dr["ResourceName"] = pr.ResourceName;
                    dr["ResourceMake"] = pr.ResourceMake;
                    dr["ResourceUnit"] = pr.ResourceUnit;
                    dr["ResourceRate"] = pr.ResourceRate;
                    dr["CategoryID"] = pr.CategoryID;
                    dr["ResourceLead"] = pr.ResourceLead;
                    dr["ResourceLeadRate"] = pr.ResourceLeadRate;
                    dr["ResourceLPH"] = pr.ResourceLPH;
                    dr["ResourcerRemarks"] = pr.ResourcerRemarks;
                    dr["ResourceOutPut"] = pr.ResourceOutPut;
                    dr["Nos"] = pr.Nos;
                    dtPR.Rows.Add(dr);
                }
                SqlParameter[] sps = new SqlParameter[3];
                sps[0] = new SqlParameter("@PrjID", pid);
                sps[1] = new SqlParameter("@dt", dtPR);
                sps[2] = new SqlParameter("@CreateBy", userID);
                DataSet ds = SQLDBUtil.ExecuteDataset("sb_UpdateProjectResources", sps);
                if(ds.Tables[0].Rows.Count > 0)
                    return Request.CreateResponse(HttpStatusCode.OK, ds.Tables[0].Rows[0][0].ToString());
                else return Request.CreateResponse(HttpStatusCode.BadRequest, "Unknown error from database");
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        public static DataTable dtProj_Resource(string tblName) {            DataTable dtName = new DataTable(tblName);            DataColumn ResourceID = new DataColumn();
            ResourceID.DataType = System.Type.GetType("System.Int32");            ResourceID.ColumnName = "ResourceID";            dtName.Columns.Add(ResourceID);            dtName.Columns.Add("ResourceName", typeof(System.String));            dtName.Columns.Add("ResourceMake", typeof(System.String));            dtName.Columns.Add("ResourceUnit", typeof(System.Int32));            dtName.Columns.Add("ResourceRate", typeof(System.Double));            dtName.Columns.Add("CategoryID", typeof(System.Int32));            dtName.Columns.Add("ResourceLead", typeof(System.Double));            dtName.Columns.Add("ResourceLeadRate", typeof(System.Double));            dtName.Columns.Add("ResourceLPH", typeof(System.Double));            dtName.Columns.Add("ResourcerRemarks", typeof(System.String));            dtName.Columns.Add("ResourceOutPut", typeof(System.String));            dtName.Columns.Add("Nos", typeof(System.Int32));            DataColumn[] keys = new DataColumn[1];            keys[0] = ResourceID;            dtName.PrimaryKey = keys;            return dtName;        }
        [HttpPost]
        [Route("api/Resources/Pull2Group")]
        public HttpResponseMessage Pull2Group([FromBody] List<int> resIDs, int cid, int tgtGrp) {
            try {//http://localhost/Login/api/Resources/Pull2Group?cid=46&tgtGrp=1076
                DataTable dtResIDs = new DataTable();
                dtResIDs.Columns.Add("id", typeof(System.Int32));
                foreach(int i in resIDs) {
                    DataRow dr = dtResIDs.NewRow();
                    dr[0] = i; dtResIDs.Rows.Add(i);
                }
                SqlParameter[] sps = new SqlParameter[3];
                sps[0] = new SqlParameter("@cid", cid);
                sps[1] = new SqlParameter("@tgtGp", tgtGrp);
                sps[2] = new SqlParameter("@resIDs", dtResIDs);
                DataSet ds = SQLDBUtil.ExecuteDataset("sb_PullResources2Group", sps);
                if(ds.Tables[0].Rows.Count > 0)
                    return Request.CreateResponse(HttpStatusCode.OK, ds.Tables[0].Rows[0][0].ToString());
                else return Request.CreateResponse(HttpStatusCode.BadRequest, "Unknown error from database");
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]
        [Route("api/Resources/ResWithGroups")]
        public HttpResponseMessage ResWithGroups(int cid) {
            try {//http://localhost/Login/api/Resources/ResWithGroups?cid=46
                SqlParameter[] sps = new SqlParameter[1];
                sps[0] = new SqlParameter("@cid", cid);
                DataSet ds = SQLDBUtil.ExecuteDataset("sb_GroupsVsResources", sps);
                List<ResGroup> resGroups = new List<ResGroup>();
                if(ds.Tables[0].Rows.Count > 0) {
                    foreach(DataRow dr in ds.Tables[0].Rows) {
                        ResGroup rg = new ResGroup((int)dr["pgID"], dr["ParentGroup"].ToString()
                           , (int)dr["ratID"], dr["RATType"].ToString(), (int)dr["cgpID"], dr["ChildGroup"].ToString()
                           , (int)dr["resID"], dr["ResourceName"].ToString(), (int)dr["cid"], (int)dr["mid"], (int)dr["gs"], (int)dr["cat"]
                        );
                        resGroups.Add(rg);
                    }
                }
                if(resGroups.Count > 0) return Request.CreateResponse(HttpStatusCode.OK, resGroups);
                else return Request.CreateResponse(HttpStatusCode.BadRequest, "Unknown error from database");
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet][Route("api/Resources/AuditResVsGroup")]
        public HttpResponseMessage AuditResVsGroup() {
            try {//http://localhost/Login/api/Resources/AuditResVsGroup
                DataSet ds = SQLDBUtil.ExecuteDataset("sp_AuditResVsGroup");
                if(ds.Tables[0].Rows.Count > 0) {
                    return Request.CreateResponse(HttpStatusCode.OK, ds.Tables[0].Rows[0][0].ToString());
                } else return Request.CreateResponse(HttpStatusCode.BadRequest, "Unknown error from database");
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        ////(@cid int, @rid int, @newName varchar(50), @result varchar(max) output)
        //public HttpResponseMessage Delete(int cid, int rid, string newName, string result) {
        //    try {//http://localhost/Login/api/Resources/Rename?cid=1&rid=1
        //        SqlParameter[] sps = new SqlParameter[1];
        //        sps[0] = new SqlParameter("@cid", cid);
        //        sps[1] = new SqlParameter("@rid", rid);
        //        sps[2] = new SqlParameter("@newName", newName);
        //        sps[3] = new SqlParameter("@result",SqlDbType.VarChar, 200) ;
        //        sps[3].Direction = ParameterDirection.Output;
        //        SQLDBUtil.ExecuteDataset("bms.renameRecource", sps);
        //         result = cmd.Parameters["@result"].Value.ToString();
        //         if(ds.Tables[0].Rows.Count > 0) {
        //            return Request.CreateResponse(HttpStatusCode.OK, ds.Tables[0].Rows[0][0].ToString());
        //        } else return Request.CreateResponse(HttpStatusCode.BadRequest, "Unknown error from database");
        //    } catch(Exception ex) {
        //        return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
        //    }
        //}
        [HttpGet]  //  http://localhost/Login/api/Resources/Rename?cid=1&rid=1668&newName=sand1235
        [Route("api/Resources/Rename")]
        public async Task<HttpResponseMessage> RenameAsync(int cid, int rid, string newName) {
            string result = string.Empty;
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "bms.renameRecource";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    cmd.Parameters.Add(new SqlParameter("@rid", rid));
                    cmd.Parameters.Add(new SqlParameter("@newName", newName));
                    cmd.Parameters.Add(new SqlParameter("@result", SqlDbType.VarChar, 200)).Direction = ParameterDirection.Output;
                    await cmd.ExecuteNonQueryAsync();
                    result = cmd.Parameters["@result"].Value.ToString();
                }
                con.Close();
                if(result.Contains("success")) return Request.CreateResponse(HttpStatusCode.OK, result);
                else  return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
            }
        }
    }
    class ResGroup {
        public int pgID { get; set; }
        public string ParentGroup { get; set; }
        public int ratID { get; set; }
        public string RATType { get; set; }
        public int cgpID { get; set; }
        public string ChildGroup { get; set; }
        public int resID { get; set; }
        public string ResourceName { get; set; }
        public int cid { get; set; }
        public int mid { get; set; }
        public int gs { get; set; }
        public int cat { get; set; }
        public ResGroup(int _pgID, string _ParentGroup, int _ratID, string _RATType, int _cgpID
            , string _ChildGroup, int _resID, string _ResourceName, int _cid, int _mid, int _gs, int _cat) {
            pgID = _pgID;
            ParentGroup = _ParentGroup;
            ratID = _ratID;
            RATType = _RATType;
            cgpID = _cgpID;
            ChildGroup = _ChildGroup;
            resID = _resID;
            ResourceName = _ResourceName;
            cid = _cid;
            mid= _mid;
            gs= _gs;
            cat= _cat;
        }
    }
}
