﻿using Login.Controllers;
using Login.Models;
using SD.COMMON.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
//common mistakes we do in webapi
// forget the name of xxxxController : ApiController
//Using wrongly "=" in objects in [FromBody] {TaskRefNo="1.2"} in place of required colon ":" {TaskRefNo:"1.2"}
//not Using [ { }] for List of Objects
//Dates will not be considered -- needs R&D
//Datatables must have same order as that of the SQL table
//  e.LedgerID =  rdr[5] == DBNull.Value ? -1 : (int)rdr[5];
//null byte > Class must have int type for SQL tinyInt/c# byte >> ex:  res.DeliveryDays = rdr["DeliveryDays"] == DBNull.Value ? 1 : (byte)rdr["DeliveryDays"];
//null dates >> res.CreatedOn = rdr["CreatedOn"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(rdr["CreatedOn"]);
//public DateTime? StatusChangeOn     { get; set; }
//dtName.Columns.Add("StatusChangeOn", typeof(System.DateTime)).AllowDBNull =true;
// dr["StatusChangeOn"] =  (object)e.StatusChangeOn ?? DBNull.Value; //Date of marriage
//null double >> rdr["ResourceLPH"] == DBNull.Value ? 0 : Convert.ToDouble(rdr["ResourceLPH"]);
// null strings >>  res.Extension = rdr["Extension"] == DBNull.Value ? "" : rdr["Extension"].ToString();
namespace Login {

    public class ProjectsController :ApiController {
        [HttpGet]
        [Route("api/projects")]
        public List<project> GetProjects(int cid, int userID) { //http://localhost/Login/api/Projects?cid=1&userID=1 
            SqlParameter[] sps = new SqlParameter[2];
            sps[0] = new SqlParameter("@cid", cid);
            sps[1] = new SqlParameter("@userID", userID);
            List<project> list = new List<project>();
            DataSet ds = SQLDBUtil.ExecuteDataset("sb_projects", sps);
            foreach(DataRow dr in ds.Tables[0].Rows) {
                project t = new project((int)dr["ProjectID"], dr["ProjectName"].ToString());
                list.Add(t);
            }
            return list;
        }
        [HttpGet]
        [Route("api/projects/projectsForAccess")]
        public HttpResponseMessage projectsForAccess(int userID, int empID) {
            try { //http://localhost/Login/api/Projects/projectsForAccess?userID=1&empID=12 
                SqlParameter[] sps = new SqlParameter[2];
                sps[0] = new SqlParameter("@userID", userID);
                sps[1] = new SqlParameter("@empID", empID);
                List<project> list = new List<project>();
                DataSet ds = SQLDBUtil.ExecuteDataset("sb_projectsForAccess", sps);
                if(ds.Tables[0].Rows.Count > 0) {
                    foreach(DataRow dr in ds.Tables[0].Rows) {
                        project t = new project((int)dr["id"], dr["name"].ToString());
                        list.Add(t);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, list);
                } else return Request.CreateResponse(HttpStatusCode.OK, "Unknown error from database");
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]
        [Route("api/projects/employeesForAccess")]
        public HttpResponseMessage employeesForAccess(int userID) {
            try {  //http://localhost/Login/api/Projects/employeesForAccess?userID=1  
                SqlParameter[] sps = new SqlParameter[1];
                sps[0] = new SqlParameter("@userID", userID);
                List<employee> list = new List<employee>();
                DataSet ds = SQLDBUtil.ExecuteDataset("sb_employeesForAccess", sps);
                if(ds.Tables[0].Rows.Count > 0) {
                    foreach(DataRow dr in ds.Tables[0].Rows) {
                        employee t = new employee((int)dr["id"], dr["name"].ToString());
                        list.Add(t);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, list);
                } else return Request.CreateResponse(HttpStatusCode.OK, "Unknown error from database");
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost]
        [Route("api/projects/assign")]
        public HttpResponseMessage grantAccess(int empId, int projectID, int userID) {
            try {   //http://localhost/Login/api/projects/assign?empId=1&projectID=1&userID=1  
                SqlParameter[] sps = new SqlParameter[3];
                sps[0] = new SqlParameter("@EmpId", empId);
                sps[1] = new SqlParameter("@PrjId", projectID);
                sps[2] = new SqlParameter("@UserID", userID);
                DataSet ds = SQLDBUtil.ExecuteDataset("sb_assignProjectRole", sps);
                if(ds.Tables[0].Rows.Count > 0) {
                    if(ds.Tables[0].Rows[0][0].ToString() == "1") return Request.CreateResponse(HttpStatusCode.OK, "Success");
                    else return Request.CreateResponse(HttpStatusCode.OK, ds.Tables[0].Rows[0][0].ToString());
                } else return Request.CreateResponse(HttpStatusCode.BadRequest, "Unknown error from database");
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]// http://localhost/Login/api/projects/read?cid=1
        [Route("api/projects/read")]
        public async Task<HttpResponseMessage> readProjects(int cid) {
            try {
                List<t_oms_projects> clt_oms_projects = await Task.Run(() => tskDAL.ReadProjectsAsync(cid));
                return Request.CreateResponse(HttpStatusCode.OK, clt_oms_projects);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost] // http://localhost/Login/api/projects/create?cid=1&ws=1&userID=1
        [Route("api/projects/create")]
        public async Task<HttpResponseMessage> bulkCreateProjectsAsync([FromBody]List<t_oms_projects> clProjects, int cid, int ws, int userID) {
            try {   // [{ProjectName: 'Angular Project', ProjectDesc: 'Angular Project', Employer: 1, ProjectLocation: 'Khammam', ProjectAddress: 'Khammam', ProjectDuration: 6
                    // , ProjectSchStartDate: '2019-03-29',ProjectType: 2,  value: 10000000, Loa: 'pdf', refNo: '115', PrincipalContractor: null, StateId: 2, Status: 1 , EmployerOffice: 1, PrincipleEmployer: 1, PrincipleEmployerOffice: 1 , UoM_Currency: 22, ProgressType: 2, CompanyId: 1  }]
                string result = await Task.Run(() => tskDAL.CreateProjectsAsync(clProjects, cid, ws, userID));
                if(!result.Contains("failed")) {
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPut]// http://localhost/Login/api/projects/update?cid=1&userID=1
        [Route("api/projects/update")]
        public async Task<HttpResponseMessage> BulkUpdatet_oms_projects([FromBody]List<t_oms_projects> clProjects, int cid, int userID) {
            try { // 
                if(clProjects.Count > 0) {
                    string result = await Task.Run(() => tskDAL.UpdateProjectsAsync(clProjects, cid, userID));
                    if(!result.Contains("failed")) {
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No records to Update");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpDelete] // http://localhost/Login/api/projects/Delete?userID=1
        [Route("api/projects/Delete")]
        public async Task<HttpResponseMessage> DeleteProjects([FromBody] List<int> IDs, int userID) {
            try {
                if(IDs.Count > 0) {
                    string result = await Task.Run(() => tskDAL.DeleteProjectsAsync(IDs, userID));
                    if(!result.Contains("failed")) {
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, result);
                } else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No records to Delete");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }
    public class Optionscontroller :ApiController {
        [HttpGet]
        [Route("api/Options")]
        public HttpResponseMessage getOPtions(int userID) {
            try {   //http://localhost/Login/api/Options?userID=1  
                SqlParameter[] sps = new SqlParameter[1];
                sps[0] = new SqlParameter("@UserID", userID);
                DataSet ds = SQLDBUtil.ExecuteDataset("sb_Options", sps);
                List<POptions> clOptions = new List<POptions>();
                if(ds.Tables[0].Rows.Count > 0) {
                    foreach(DataRow dr in ds.Tables[0].Rows) {
                        POptions op = new POptions();
                        op.ProjectID = (int)dr["ProjectID"];
                        op.FuelCost = Convert.ToDouble(dr["FuelCost"]);
                        op.LagDays = (int)dr["LagDays"];
                        op.TaskActMinDu = (int)dr["TaskActMinDu"];
                        op.MobilizationDu = (int)dr["MobilizationDu"];
                        op.AdFixDu = (int)dr["AdFixDu"];
                        op.MHead = dr["MHead"].ToString();
                        op.NoRec = Convert.ToDouble(dr["NoRec"]);
                        op.ISGovt = (int)dr["ISGovt"];
                        op.IsLocked = (bool)dr["IsLocked"];
                        op.HrPerDay = (int)dr["HrPerDay"];
                        op.GrpPerMonth = (int)dr["GrpPerMonth"];
                        op.BID = (int)dr["BID"];
                        op.CurrencyID = (int)dr["CurrencyID"];
                        op.ConvRate = Convert.ToDouble(dr["ConvRate"]);
                        op.FuelCost_Forex = Convert.ToDouble(dr["FuelCost_Forex"]);
                        op.CaptiveMode = (int)dr["CaptiveMode"];
                        clOptions.Add(op);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, clOptions);
                } else return Request.CreateResponse(HttpStatusCode.BadRequest, "Unknown error from database");
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost]
        [Route("api/Options")]
        public HttpResponseMessage UpdateOptions([FromBody] POptions pOptions, int userID) {
            try {   //http://localhost/Login/api/Options?userID=1  
                SqlParameter[] sps = new SqlParameter[15];
                sps[0] = new SqlParameter("@ProjectID", pOptions.ProjectID);
                sps[1] = new SqlParameter("@FuelCost", pOptions.FuelCost);
                sps[2] = new SqlParameter("@LagDays", pOptions.LagDays);
                sps[3] = new SqlParameter("@TaskActMinDu", pOptions.TaskActMinDu);
                sps[4] = new SqlParameter("@MobilizationDu", pOptions.MobilizationDu);
                sps[5] = new SqlParameter("@AdFixDu", pOptions.AdFixDu);
                sps[6] = new SqlParameter("@NoRec", pOptions.NoRec);
                sps[7] = new SqlParameter("@MHead", pOptions.MHead);
                sps[8] = new SqlParameter("@ISSSR", pOptions.ISGovt);
                sps[9] = new SqlParameter("@ISLock", pOptions.IsLocked);
                sps[10] = new SqlParameter("@ShiftID", pOptions.HrPerDay);
                sps[11] = new SqlParameter("@GpTypeID", pOptions.GrpPerMonth);
                sps[12] = new SqlParameter("@CurID", pOptions.CurrencyID);
                sps[13] = new SqlParameter("@CurVal", pOptions.ConvRate);
                sps[14] = new SqlParameter("@userID", userID);
                DataSet ds = SQLDBUtil.ExecuteDataset("sb_optionsUpdate", sps);
                List<POptions> clOptions = new List<POptions>();
                if(ds.Tables[0].Rows.Count > 0) {
                    return Request.CreateResponse(HttpStatusCode.OK, ds.Tables[0].Rows[0][0].ToString());
                } else return Request.CreateResponse(HttpStatusCode.BadRequest, "Unknown error from database");
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }
    public class TasksController :ApiController {

        [HttpGet]//http://localhost/Login/api/Tasks?data=1,1
        public List<task> GetTasks(string data) {
            string[] ar = data.Split(',');
            List<task> list = new List<task>();
            SqlParameter[] sps = new SqlParameter[2];
            sps[0] = new SqlParameter("@pid", int.Parse(ar[0]));
            sps[1] = new SqlParameter("@cid", int.Parse(ar[1]));
            DataSet dscomp = SQLDBUtil.ExecuteDataset("sb_tasks", sps);
            foreach(DataRow dr in dscomp.Tables[0].Rows) {
                task t = new task((int)dr["CompanyId"], (int)dr["ProjectID"], (int)dr["slno"], (int)dr["TaskID"], (int)dr["UnitID"],
                (int)dr["SectionID"], (int)dr["CreatedBy"], (int)dr["UpdatedBy"], (int)dr["IsActive"], (int)dr["TaskMasterID"], (int)dr["LinkTo"],

                dr["TaskRefNo"].ToString(), dr["TaskName"].ToString(), dr["TaskDesc"].ToString(), dr["Method"].ToString(), dr["StDt"].ToString(), dr["EdDt"].ToString(),
                dr["MTask"].ToString(), dr["GDesc"].ToString(), dr["BaseLine"].ToString(), dr["Unit"].ToString(), dr["Section"].ToString(),

                Convert.ToDouble(dr["Quantity"]), Convert.ToDouble(dr["RaQty"]), Convert.ToDouble(dr["TotalCost"]), Convert.ToDouble(dr["TotalLength"]),
                Convert.ToDouble(dr["TaskCost"]), Convert.ToDouble(dr["AdFixed"]), Convert.ToDouble(dr["Adruning"]), Convert.ToDouble(dr["OP"]),
                 Convert.ToDouble(dr["TenderRate"]), Convert.ToDouble(dr["AgreementRate"]), dr["XlRef"].ToString());
                list.Add(t);
            }
            return list;
        }
        // GET  //http://localhost/Login/api/Tasks/1
        public HttpResponseMessage Get(int id) {
            SqlParameter[] sps = new SqlParameter[1];
            sps[0] = new SqlParameter("@taskID", id);
            DataSet ds = SQLDBUtil.ExecuteDataset("sb_task", sps);
            DataRow dr = ds.Tables[0].Rows[0];
            if((int)dr["TaskID"] > 0) {
                task t = new task(
                (int)dr["CompanyId"], (int)dr["ProjectID"], (int)dr["slno"], (int)dr["TaskID"], (int)dr["UnitID"],
                (int)dr["SectionID"], (int)dr["CreatedBy"], (int)dr["UpdatedBy"], (int)dr["IsActive"], (int)dr["TaskMasterID"], (int)dr["LinkTo"],

                dr["TaskRefNo"].ToString(), dr["TaskName"].ToString(), dr["TaskDesc"].ToString(), dr["Method"].ToString(), dr["StDt"].ToString(), dr["EdDt"].ToString(),
                dr["MTask"].ToString(), dr["GDesc"].ToString(), dr["BaseLine"].ToString(), dr["Unit"].ToString(), dr["Section"].ToString(),

                Convert.ToDouble(dr["Quantity"]), Convert.ToDouble(dr["RaQty"]), Convert.ToDouble(dr["TotalCost"]), Convert.ToDouble(dr["TotalLength"]),
                Convert.ToDouble(dr["TaskCost"]), Convert.ToDouble(dr["AdFixed"]), Convert.ToDouble(dr["Adruning"]), Convert.ToDouble(dr["OP"]),
                Convert.ToDouble(dr["TenderRate"]), Convert.ToDouble(dr["AgreementRate"]), dr["XlRef"].ToString());
                return Request.CreateResponse(HttpStatusCode.OK, t);
            } else return Request.CreateResponse(HttpStatusCode.BadRequest, "Error to fetch TaskID: " + id.ToString());
        }

        // POST SINGLE TASK CREATION with ID Repeat >> http://localhost/login/api/Tasks/Post1?id=2 >>>> {TaskRefNo:"1.2", TaskName:"Soil Excavation", TaskDesc:"", UnitID:12, SectionID:1, Quantity: 100.00, UserID: 1, ProjectID: 1}
        [HttpPost]
        [Route("api/tasks/post1")]
        public HttpResponseMessage CopyRepeatTask([FromUri] int id, [FromBody]taskMin newTask) {
            taskMin list = new taskMin();
            if(id <= 10) {
                try {
                    SqlParameter[] sps = new SqlParameter[9];//@TaskRefNo, @TaskName, @TaskDesc, @Unit, @SectionID, @Quantity, @UserID, @ProjectID 
                    sps[0] = new SqlParameter("@TaskRefNo", newTask.TaskRefNo);
                    sps[1] = new SqlParameter("@TaskName", newTask.TaskName);
                    sps[2] = new SqlParameter("@TaskDesc", newTask.TaskDesc);
                    sps[3] = new SqlParameter("@Unit", newTask.UnitID);
                    sps[4] = new SqlParameter("@SectionID", newTask.SectionID);
                    sps[5] = new SqlParameter("@Quantity", newTask.Quantity);
                    sps[6] = new SqlParameter("@UserID", newTask.UserID);
                    sps[7] = new SqlParameter("@ProjectID", newTask.ProjectID);
                    sps[8] = new SqlParameter("@repeat", id);// these number of tasks are created
                    DataSet ds = SQLDBUtil.ExecuteDataset("sb_taskCreateMin", sps);
                    foreach(DataRow dr in ds.Tables[0].Rows) {
                        if((int)dr["TaskID"] > 0) {
                            task t = new task(
                                (int)dr["CompanyId"], (int)dr["ProjectID"], (int)dr["slno"], (int)dr["TaskID"], (int)dr["UnitID"],
                                (int)dr["SectionID"], (int)dr["CreatedBy"], (int)dr["UpdatedBy"], (int)dr["IsActive"], (int)dr["TaskMasterID"], (int)dr["LinkTo"],

                                dr["TaskRefNo"].ToString(), dr["TaskName"].ToString(), dr["TaskDesc"].ToString(), dr["Method"].ToString(), dr["StDt"].ToString(), dr["EdDt"].ToString(),
                                dr["MTask"].ToString(), dr["GDesc"].ToString(), dr["BaseLine"].ToString(), dr["Unit"].ToString(), dr["Section"].ToString(),

                                Convert.ToDouble(dr["Quantity"]), Convert.ToDouble(dr["RaQty"]), Convert.ToDouble(dr["TotalCost"]), Convert.ToDouble(dr["TotalLength"]),
                                Convert.ToDouble(dr["TaskCost"]), Convert.ToDouble(dr["AdFixed"]), Convert.ToDouble(dr["Adruning"]), Convert.ToDouble(dr["OP"]),
                                Convert.ToDouble(dr["TenderRate"]), Convert.ToDouble(dr["AgreementRate"]), dr["XlRef"].ToString()
                            );
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, ds.Tables[0].Rows.Count.ToString() + " TASK records are created");
                } catch { return Request.CreateResponse(HttpStatusCode.BadRequest, "Falied to created Task"); }
            } else return Request.CreateResponse(HttpStatusCode.BadRequest, "ONLY 10 records are allowed for inserting at a time!");
        }
        // POST MULTIPLE TASKS CREATION  
        [HttpPost] // http://localhost/login/api/Tasks/Post2 
        [Route("api/tasks/post2")] // [{"TaskID":0,"ProjectID":2,"TaskName":"Cutting upto the level of +6.0m","TaskDesc":"Cutting upto the level of +6.0m","Unit":"12","SectionID":56,"Quantity":"300","Method":null,"RaQty":1,"CreatedBy":1,"UpdatedBy":1,"IsActive":1,"TotalCost":0,"TotalLength":0,"AdFixed":0,"Adruning":0,"OP":0,"TaskCost":0,"TaskMasterID":1,"TenderRate":0,"slno":"1","XlRef":"$G$2","StDt":"23/01/2019","EdDt":"23/01/2019", "LinkTo": 0}]
        public HttpResponseMessage CopySelectedTasks([FromBody]List<task> tasks2Edit) {
            T_OMS_Tasks omsTaskTbl = new T_OMS_Tasks();
            DataTable dtTasks = omsTaskTbl.dataTable("dtTasks");
            foreach(task T in tasks2Edit) {
                DataRow dr = dtTasks.NewRow();
                dr["TaskID"] = T.TaskID;
                dr["ProjectID"] = T.ProjectID;
                dr["TaskRefNo"] = T.TaskRefNo;
                dr["TaskName"] = T.TaskName;
                dr["TaskDesc"] = T.TaskDesc;
                dr["Unit"] = T.UnitID;
                dr["SectionID"] = T.SectionID;
                dr["Quantity"] = T.Quantity;
                dr["StartDate"] = Convert.ToDateTime(DateTime.ParseExact(T.StDt, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                dr["EndDate"] = Convert.ToDateTime(DateTime.ParseExact(T.EdDt, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                dr["Method"] = T.Method;
                dr["RaQty"] = T.RaQty;
                dr["CreatedOn"] = DateTime.Now;
                dr["CreatedBy"] = T.CreatedBy;
                dr["UpdatedOn"] = DateTime.Now;
                dr["UpdatedBy"] = T.UpdatedBy;
                dr["IsActive"] = T.IsActive;
                dr["TotalCost"] = T.TotalCost;
                dr["TotalLength"] = T.TotalLength;
                dr["AdFixed"] = T.AdFixed;
                dr["Adruning"] = T.Adruning;
                dr["OP"] = T.OP;
                dr["TaskCost"] = 0;
                dr["TaskMasterID"] = T.TaskMasterID;
                dr["AgreementRate"] = 0;
                dr["TenderRate"] = 0;
                dr["OrderID"] = T.slno;
                dr["PCID"] = DBNull.Value;
                dr["Predessor"] = DBNull.Value;
                dr["Duration"] = (Convert.ToDateTime(DateTime.ParseExact(T.EdDt, "dd/MM/yyyy", CultureInfo.InvariantCulture))
                                - Convert.ToDateTime(DateTime.ParseExact(T.StDt, "dd/MM/yyyy", CultureInfo.InvariantCulture))).Days;
                dr["BaseID"] = 10;
                dr["IsCaptive"] = false;
                dr["SlNo"] = T.slno;
                dr["LinkTo"] = T.LinkTo;
                dr["TSuc"] = DBNull.Value;
                dr["Density"] = 1000;
                dr["ProdQty"] = 1;
                dr["LagHrs"] = DBNull.Value;
                dr["CTCRate"] = DBNull.Value;
                dr["LagPred"] = DBNull.Value;
                dr["SecDesc"] = DBNull.Value;
                dr["SecLvl1"] = DBNull.Value;
                dr["SecLvl2"] = DBNull.Value;
                dr["SecLvl3"] = DBNull.Value;
                dr["SecLvl4"] = DBNull.Value;
                dr["SecLvl5"] = DBNull.Value;
                dr["XlRef"] = T.XlRef;
                dr["NoofHours"] = DBNull.Value;
                dr["NoofDays"] = DBNull.Value;
                dr["Cid"] = T.CompanyId;
                dtTasks.Rows.Add(dr);
            }
            using(SqlConnection conn = new SqlConnection(common.erpCS)) {
                conn.Open();
                using(SqlBulkCopy bulkCopy = new SqlBulkCopy(conn)) {
                    bulkCopy.DestinationTableName = "dbo." + "T_OMS_Tasks";
                    try {
                        bulkCopy.WriteToServer(dtTasks);
                        List<int> StIDs = getInsertedTIDs(conn);
                        if(StIDs.Count > 0) {
                            DataTable dt = idList2Table(StIDs);
                            string query = "oms.mrpBlTaskCreate";
                            int recordsUpdated = 0;
                            using(SqlCommand cmd = new SqlCommand(query, conn)) {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add(new SqlParameter("@tIDs", dt));
                                recordsUpdated = cmd.ExecuteNonQuery();
                            }
                        }
                    } catch(Exception ex) {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "Falied to insert: " + ex.Message);
                    }
                }
                conn.Close();
            }
            return Request.CreateResponse(HttpStatusCode.OK, tasks2Edit.Count.ToString() + " Records Inserted");
        }
        private static DataTable idList2Table(List<int> ids) {
            DataTable dt = new DataTable();
            dt.Columns.Add("id", typeof(System.Int32));
            foreach(int t in ids) {
                DataRow dr = dt.NewRow();
                dr["id"] = t;
                dt.Rows.Add(dr);
            }
            return dt;
        }
        private static List<int> getInsertedTIDs(SqlConnection conn) {
            SqlCommand cmd = new SqlCommand("select TaskID  from T_OMS_Tasks T LEFT JOIN T_OMS_MRP_BL_Tasks BL " +
                                "ON BL.MRP_TID=T.Taskid where bl.MRP_TID is null", conn);
            List<int> StIDs = new List<int>();
            using(SqlDataReader reader = cmd.ExecuteReader()) {
                while(reader.Read()) {
                    StIDs.Add((int)reader[0]);
                }
            }
            return StIDs;
        }
        [Route("api/tasks/xlImport")]
        public HttpResponseMessage xlImport([FromBody]List<task> tasks2Edit) {
            T_OMS_Tasks omsTaskTbl = new T_OMS_Tasks();
            DataTable dtTasks = omsTaskTbl.dataTable("dtTasks");
            int i = 0;
            foreach(task T in tasks2Edit) {
                DataRow dr = dtTasks.NewRow();
                dr["TaskID"] = i++;
                dr["ProjectID"] = T.ProjectID;
                dr["TaskRefNo"] = T.TaskRefNo;
                dr["TaskName"] = T.TaskName;
                dr["TaskDesc"] = T.TaskDesc;
                dr["Unit"] = T.UnitID;
                dr["SectionID"] = T.SectionID;
                dr["Quantity"] = T.Quantity;
                dr["StartDate"] = Convert.ToDateTime(DateTime.ParseExact(T.StDt, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                dr["EndDate"] = Convert.ToDateTime(DateTime.ParseExact(T.EdDt, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                dr["Method"] = T.Method;
                dr["RaQty"] = T.RaQty;
                dr["CreatedOn"] = DateTime.Now;
                dr["CreatedBy"] = T.CreatedBy;
                dr["UpdatedOn"] = DateTime.Now;
                dr["UpdatedBy"] = T.UpdatedBy;
                dr["IsActive"] = T.IsActive;
                dr["TotalCost"] = T.TotalCost;
                dr["TotalLength"] = T.TotalLength;
                dr["AdFixed"] = T.AdFixed;
                dr["Adruning"] = T.Adruning;
                dr["OP"] = T.OP;
                dr["TaskCost"] = 0;
                dr["TaskMasterID"] = T.TaskMasterID;
                dr["AgreementRate"] = T.AgreementRate;
                dr["TenderRate"] = T.TenderRate;
                dr["OrderID"] = T.slno;
                dr["PCID"] = DBNull.Value;
                dr["Predessor"] = DBNull.Value;
                dr["Duration"] = (Convert.ToDateTime(DateTime.ParseExact(T.EdDt, "dd/MM/yyyy", CultureInfo.InvariantCulture))
                                - Convert.ToDateTime(DateTime.ParseExact(T.StDt, "dd/MM/yyyy", CultureInfo.InvariantCulture))).Days;
                dr["BaseID"] = 10;
                dr["IsCaptive"] = false;
                dr["SlNo"] = T.slno;
                dr["LinkTo"] = T.LinkTo;
                dr["TSuc"] = DBNull.Value;
                dr["Density"] = 1000;
                dr["ProdQty"] = 1;
                dr["LagHrs"] = DBNull.Value;
                dr["CTCRate"] = DBNull.Value;
                dr["LagPred"] = DBNull.Value;
                dr["SecDesc"] = DBNull.Value;
                dr["SecLvl1"] = DBNull.Value;
                dr["SecLvl2"] = DBNull.Value;
                dr["SecLvl3"] = DBNull.Value;
                dr["SecLvl4"] = DBNull.Value;
                dr["SecLvl5"] = DBNull.Value;
                dr["XlRef"] = T.XlRef;
                dr["NoofHours"] = DBNull.Value;
                dr["NoofDays"] = DBNull.Value;
                dr["Cid"] = T.CompanyId;
                dtTasks.Rows.Add(dr);
            }
            using(SqlConnection conn = new SqlConnection(common.erpCS)) {
                conn.Open();
                using(SqlBulkCopy bulkCopy = new SqlBulkCopy(conn)) {
                    bulkCopy.DestinationTableName = "dbo." + "T_OMS_Tasks";
                    try {
                        bulkCopy.WriteToServer(dtTasks);
                        List<int> StIDs = getInsertedTIDs(conn);
                        if(StIDs.Count > 0) {
                            DataTable dt = idList2Table(StIDs);
                            string query = "oms.mrpBlTaskCreate";
                            int recordsUpdated = 0;
                            using(SqlCommand cmd = new SqlCommand(query, conn)) {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add(new SqlParameter("@tIDs", dt));
                                recordsUpdated = cmd.ExecuteNonQuery();
                            }
                        }
                    } catch(Exception ex) {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "Falied to insert: " + ex.Message);
                    }
                }
                conn.Close();
            }
            return Request.CreateResponse(HttpStatusCode.OK, tasks2Edit.Count.ToString() + " Records Inserted");
        }
        // PUT http://localhost/Login/api/Tasks?id=1 >> id = 0 for no-cost-change and 1 for cost-change
        //{AdFixed: 11,Adruning: 11, AgreementRate: 0, BaseLine: "Tender BL", CompanyId: 1, CreatedBy: 1, EdDt: "26/10/2018", GDesc: "", IsActive: 1, LinkTo: 0, MTask: "_General Item", Method: "", OP: 11, ProjectID: 1, Quantity: "155", RaQty: 1, Section: "Earth Work", SectionID: 1, StDt: "25/10/2018", TaskCost: 367705.172, TaskDesc: "Earth Work 1234", TaskID: 1, TaskMasterID: 1, TaskName: "Earth Work 12345", TaskRefNo: "1.12.4", TenderRate: 0, TotalCost: 276470.054, TotalLength: 0, Unit: "Cum", UnitID: 12, UpdatedBy: 1, slno: 13}
        [HttpPut]
        [Route("api/tasks/Update")]
        public HttpResponseMessage UpdateTask([FromUri]int id, [FromBody]task task2Edit) {
            SqlParameter[] sps = new SqlParameter[21];
            sps[0] = new SqlParameter("@ProjectID", task2Edit.ProjectID);
            sps[1] = new SqlParameter("@orderID", task2Edit.slno);
            sps[2] = new SqlParameter("@TaskID", task2Edit.TaskID);
            sps[3] = new SqlParameter("@Unit", task2Edit.UnitID);
            sps[4] = new SqlParameter("@SectionID", task2Edit.SectionID);
            sps[5] = new SqlParameter("@UserID", task2Edit.UpdatedBy);
            sps[6] = new SqlParameter("@IsActive", task2Edit.IsActive);
            sps[7] = new SqlParameter("@TaskMasterID", task2Edit.TaskMasterID);
            sps[8] = new SqlParameter("@TaskRefNo", task2Edit.TaskRefNo);
            sps[9] = new SqlParameter("@TaskName", task2Edit.TaskName);
            sps[10] = new SqlParameter("@TaskDesc", task2Edit.TaskDesc);
            sps[11] = new SqlParameter("@Method", task2Edit.Method);
            sps[12] = new SqlParameter("@StartDate", Convert.ToDateTime(DateTime.ParseExact(task2Edit.StDt, "dd/MM/yyyy", CultureInfo.InvariantCulture)));
            sps[13] = new SqlParameter("@EndDate", Convert.ToDateTime(DateTime.ParseExact(task2Edit.EdDt, "dd/MM/yyyy", CultureInfo.InvariantCulture)));
            sps[14] = new SqlParameter("@GDesc", task2Edit.GDesc);
            sps[15] = new SqlParameter("@Quantity", task2Edit.Quantity);
            sps[16] = new SqlParameter("@RaQty", task2Edit.RaQty);
            sps[17] = new SqlParameter("@AdFixed", task2Edit.AdFixed);
            sps[18] = new SqlParameter("@Adruning", task2Edit.Adruning);
            sps[19] = new SqlParameter("@OP", task2Edit.OP);
            sps[20] = new SqlParameter("@costChange", id);

            DataSet dscomp = SQLDBUtil.ExecuteDataset("sb_taskUpdate", sps);
            DataRow dr = dscomp.Tables[0].Rows[0];
            if((int)dr["TaskID"] > 0) {
                task t = new task(
                    (int)dr["CompanyId"], (int)dr["ProjectID"], (int)dr["slno"], (int)dr["TaskID"], (int)dr["UnitID"],
                    (int)dr["SectionID"], (int)dr["CreatedBy"], (int)dr["UpdatedBy"], (int)dr["IsActive"], (int)dr["TaskMasterID"], (int)dr["LinkTo"],

                    dr["TaskRefNo"].ToString(), dr["TaskName"].ToString(), dr["TaskDesc"].ToString(), dr["Method"].ToString(), dr["StDt"].ToString(), dr["EdDt"].ToString(),
                    dr["MTask"].ToString(), dr["GDesc"].ToString(), dr["BaseLine"].ToString(), dr["Unit"].ToString(), dr["Section"].ToString(),

                    Convert.ToDouble(dr["Quantity"]), Convert.ToDouble(dr["RaQty"]), Convert.ToDouble(dr["TotalCost"]), Convert.ToDouble(dr["TotalLength"]),
                    Convert.ToDouble(dr["TaskCost"]), Convert.ToDouble(dr["AdFixed"]), Convert.ToDouble(dr["Adruning"]), Convert.ToDouble(dr["OP"]),
                    Convert.ToDouble(dr["TenderRate"]), Convert.ToDouble(dr["AgreementRate"]), dr["XlRef"].ToString()
                );
                return Request.CreateResponse(HttpStatusCode.OK, "TaskID: " + t.TaskID.ToString() + " is updated!");
            } else return Request.CreateResponse(HttpStatusCode.BadRequest, "Failed to created Task");

        }
        [HttpPut]
        [Route("api/tasks/bulkUpdate")]
        public HttpResponseMessage bulkUpdateTasks([FromBody]List<task> tasks2Edit) {
            task t = new task();
            // if(clBaseLines.Count == 0) SelectBaseLines();
            DataTable dtTasks = t.Dttask("dtTasks");
            DataTable dtXlRef = new DataTable();
            dtXlRef.Columns.Add("TaskID", typeof(System.Int32));
            dtXlRef.Columns.Add("XLRef", typeof(System.String));
            foreach(task T in tasks2Edit) {
                DataRow dr = dtTasks.NewRow();
                DataRow dr2 = dtXlRef.NewRow();
                dr["TaskID"] = T.TaskID;
                dr["slno"] = T.slno;
                dr["ProjectID"] = T.ProjectID;
                // int bid = (from tbl in clBaseLines where tbl.baseLine == T.BaseLine select tbl.ID).FirstOrDefault();
                dr["BaseID"] = 10;
                dr["TaskRefNo"] = T.TaskRefNo;
                dr["TaskName"] = T.TaskName;
                dr["TaskDesc"] = T.TaskDesc;
                dr["GDesc"] = T.GDesc;
                dr["LinkTo"] = T.LinkTo = 0;
                dr["TaskMasterID"] = T.TaskMasterID;
                dr["UnitID"] = T.UnitID;
                dr["SectionID"] = T.SectionID;
                dr["Quantity"] = T.Quantity;
                dr["Method"] = T.Method;
                dr["RaQty"] = T.RaQty;
                dr["StDt"] = Convert.ToDateTime(DateTime.ParseExact(T.StDt, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                dr["EdDt"] = Convert.ToDateTime(DateTime.ParseExact(T.EdDt, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                dr["UpdatedBy"] = T.UpdatedBy;
                dr["IsActive"] = T.IsActive;
                dr["TotalCost"] = T.TotalCost;
                dr["TaskCost"] = T.TaskCost;
                dr["AdFixed"] = T.AdFixed;
                dr["Adruning"] = T.Adruning;
                dr["OP"] = T.OP;
                dr["AgreementRate"] = T.AgreementRate;
                dr["TenderRate"] = T.TenderRate;
                dtTasks.Rows.Add(dr);
                dr2["TaskID"] = T.TaskID;
                dr2["XlRef"] = T.XlRef;
                dtXlRef.Rows.Add(dr2);
            }
            if(dtTasks.Rows.Count > 0) {
                string result = "";
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    SqlCommand cmd = new SqlCommand("sb_taskUpdates", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TaskType", dtTasks).SqlDbType = SqlDbType.Structured;
                    cmd.Parameters.AddWithValue("@isKVP", dtXlRef).SqlDbType = SqlDbType.Structured;
                    con.Open();
                    result = cmd.ExecuteScalar().ToString() + " task(s) updated"; // cmd.ExecuteNonQuery();
                    con.Close();
                }
                return Request.CreateResponse(HttpStatusCode.OK, result);
            } else return Request.CreateResponse(HttpStatusCode.BadRequest, "Falied to update records");
        }
        public static List<bl> clBaseLines = new List<bl>();
        public static void SelectBaseLines() {
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                con.Open();
                string query = "Select * from to_bl where ia <> 0";
                using(SqlCommand cmd = new SqlCommand(query, con)) {
                    using(SqlDataReader rdr = cmd.ExecuteReader()) {
                        while(rdr.Read()) {
                            bl p = new bl();
                            p.ID = (int)rdr[0];
                            p.baseLine = ((int)rdr[0]).ToString() + ":" + rdr[1].ToString();
                            p.ia = (bool)rdr[2];
                            clBaseLines.Add(p);
                        }
                    }
                }
            }
        }
        // DELETE api/values/5
        [HttpDelete]
        [Route("api/tasks/bulkDelete")]
        public HttpResponseMessage Delete([FromBody] List<int> IDs) {
            DataTable dt = new DataTable();
            dt.Columns.Add("id", typeof(System.Int32));
            foreach(int T in IDs) {
                DataRow dr = dt.NewRow();
                dr["id"] = T;
                dt.Rows.Add(dr);
            }
            if(dt.Rows.Count > 0) {
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    SqlCommand cmd = new SqlCommand("sb_taskDeletes", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id", dt).SqlDbType = SqlDbType.Structured;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
                return Request.CreateResponse(HttpStatusCode.OK, dt.Rows.Count.ToString() + " records deleted");
            } else return Request.CreateResponse(HttpStatusCode.BadRequest, "Falied to delete records");
        }
        //[]  SectionID	SectionNAme	TAmount	SName	SecID
        [HttpGet]
        [Route("api/tasks/budget/{id}")]// http://localhost/login/api/tasks/budget?pid=1 
        public HttpResponseMessage gBudget(int pid, int id) {
            List<gBudget> clBudget = new List<gBudget>();
            try { //For all groups wise costs >>sb_budget 1, null &&&&& all tasks >> sb_budget 1,0 >>For task in a selected section >> sb_budget 1, 2
                SqlParameter[] sps = new SqlParameter[2];
                sps[0] = new SqlParameter("@PrjID", pid);
                sps[1] = new SqlParameter("@sid", id);
                DataSet ds = SQLDBUtil.ExecuteDataset("sb_budget", sps);
                if(ds.Tables[0].Rows.Count > 0) {
                    foreach(DataRow dr in ds.Tables[0].Rows) {
                        gBudget bg = new gBudget();
                        bg.SectionID = (int)dr["SectionID"];
                        bg.SectionName = dr["SectionName"].ToString();
                        bg.TaskID = (int)dr["TaskID"];
                        bg.TaskName = dr["TaskName"].ToString();
                        bg.Amount = Convert.ToDouble(dr["Amount"]);
                        bg.SharePc = Convert.ToDouble(dr["SharePc"]);
                        bg.OrderID = (int)dr["OrderID"];
                        clBudget.Add(bg);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, clBudget);
                } else return Request.CreateResponse(HttpStatusCode.BadRequest, "Unknown error from database");
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]
        [Route("api/tasks/budget")]// http://localhost/login/api/tasks/budget?pid=1
        public HttpResponseMessage pBudget(int pid) {
            List<pBudget> clBudget = new List<pBudget>();
            try { //For all groups wise costs >>sb_budget 1, null &&&&& all tasks >> sb_budget 1,0 >>For task in a selected section >> sb_budget 1, 2
                SqlParameter[] sps = new SqlParameter[2];
                sps[0] = new SqlParameter("@PrjID", pid);
                sps[1] = new SqlParameter("@sid", null);
                DataSet ds = SQLDBUtil.ExecuteDataset("sb_budget", sps);
                if(ds.Tables[0].Rows.Count > 0) {
                    foreach(DataRow dr in ds.Tables[0].Rows) {
                        pBudget bg = new pBudget();
                        bg.SectionID = (int)dr["SectionID"];
                        bg.SectionName = dr["SectionName"].ToString();
                        bg.Amount = Convert.ToDouble(dr["Amount"]);
                        bg.SharePc = Convert.ToDouble(dr["SharePc"]);
                        clBudget.Add(bg);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, clBudget);
                } else return Request.CreateResponse(HttpStatusCode.BadRequest, "Unknown error from database");
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]
        [Route("api/tasks/allTasksDCTCUpdate")]
        public HttpResponseMessage allTasksDCTCUpdate(int pid) {
            try {//http://localhost/Login/api/tasks/allTasksDCTCUpdate
                string result = "";
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    SqlCommand cmd = new SqlCommand("sb_allTasksDCTCUpdate", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@pid", pid);
                    con.Open();
                    result = cmd.ExecuteScalar().ToString();
                    con.Close();
                }
                return Request.CreateResponse(HttpStatusCode.OK, result);
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }
    public class ResourceCategoryController :ApiController {
        [HttpGet]
        [Route("api/masters/ResourceCategory")] //http://localhost/Login/api/masters/ResourceCategory
        public List<resCat> ResourceCategories() {//tested OK
            List<resCat> rcat = new List<resCat>();
            SqlDataReader rdr = SQLDBUtil.ExecuteReader("sb_ResCategory");
            while(rdr.Read()) { resCat e = new resCat((int)rdr[0], rdr[1].ToString()); rcat.Add(e); }
            return rcat;
        }
        [HttpGet]
        [Route("api/masters/ClassingMaster")]//http://localhost/Login/api/masters/ClassingMaster 
        public List<ClassMaster> ClassingMasters() {//tested OK
            List<ClassMaster> clClassMaster = new List<ClassMaster>();
            SqlDataReader rdr = SQLDBUtil.ExecuteReader("sb_ClassingMaster");
            while(rdr.Read()) { ClassMaster e = new ClassMaster((int)rdr[0], rdr[1].ToString()); clClassMaster.Add(e); }
            return clClassMaster;
        }
        [HttpGet]
        [Route("api/masters/BillGenType")]//http://localhost/Login/api/masters/BillGenType 
        public List<BillGenType> BillGenTypes() {//tested OK
            List<BillGenType> clBillGenType = new List<BillGenType>();
            SqlDataReader rdr = SQLDBUtil.ExecuteReader("sb_BillGenType");
            while(rdr.Read()) { BillGenType e = new BillGenType((int)rdr[0], rdr[1].ToString()); clBillGenType.Add(e); }
            return clBillGenType;
        }
        [HttpGet]
        [Route("api/masters/DerivedUnitTypes")]//http://localhost/Login/api/masters/DerivedUnitTypes 
        public List<DerivedUnitTypes> DerivedUnitTypes() {//tested OK
            List<DerivedUnitTypes> clDerivedUnitTypes = new List<DerivedUnitTypes>();
            SqlDataReader rdr = SQLDBUtil.ExecuteReader("sb_DerivedUnitTypes");
            while(rdr.Read()) { DerivedUnitTypes e = new DerivedUnitTypes((int)rdr[0], rdr[1].ToString()); clDerivedUnitTypes.Add(e); }
            return clDerivedUnitTypes;
        }
        [HttpGet]
        [Route("api/masters/heads")]//http://localhost/Login/api/masters/heads 
        public List<T_A_Heads> Heads() {
            List<T_A_Heads> clHeads = new List<T_A_Heads>();
            SqlDataReader rdr = SQLDBUtil.ExecuteReader("sb_Heads");
            while(rdr.Read()) { T_A_Heads e = new T_A_Heads(Convert.ToDecimal(rdr[0]), rdr[1].ToString()); clHeads.Add(e); }
            return clHeads;
        }
        [HttpGet]
        [Route("api/masters/AccountGroups")]//http://localhost/Login/api/masters/AccountGroups  
        public List<T_A_Groups> AccountGroups() {
            List<T_A_Groups> clGroups = new List<T_A_Groups>();
            //SqlParameter[] sps = new SqlParameter[1];
            //sps[0] = new SqlParameter("@cid", id);
            DataSet ds = SQLDBUtil.ExecuteDataset("sb_Groups");
            foreach(DataRow dr in ds.Tables[0].Rows) {
                T_A_Groups e = new T_A_Groups(
                    (int)dr["GroupID"], dr["GroupName"].ToString(), Convert.ToDecimal(dr["ParentId"]), (int)dr["HeadID"]
                    , Convert.ToDecimal(dr["Bitsstatus"]));
                clGroups.Add(e);
            }
            return clGroups;
        }
        [HttpGet]
        [Route("api/masters/ResourceAssetType")]//http://localhost/Login/api/masters/ResourceAssetType 
        public List<T_G_ResourceAssetType> ResourceAssetType() {
            List<T_G_ResourceAssetType> clRatss = new List<T_G_ResourceAssetType>();
            DataSet ds = SQLDBUtil.ExecuteDataset("sb_ResourceAssetType");
            foreach(DataRow dr in ds.Tables[0].Rows) {
                T_G_ResourceAssetType e = new T_G_ResourceAssetType(
                    (int)dr["AssetTypeId"], dr["AssetType"].ToString(), (int)dr["GroupID"], (int)dr["ModuleID"], (bool)dr["IsGoods"], (bool)dr["IsActive"]);
                clRatss.Add(e);
            }
            return clRatss;
        }
        [HttpGet]
        [Route("api/masters/ResourceIntegration")]//http://localhost/Login/api/masters/ResourceIntegration?cid=1 
        public List<vb_4msVsCatVsGpsVsRat> vb_4msVsCatVsGpsVsRat(int cid) {
            List<vb_4msVsCatVsGpsVsRat> cl4msVsGroups = new List<vb_4msVsCatVsGpsVsRat>();
            SqlParameter[] sps = new SqlParameter[1];
            sps[0] = new SqlParameter("@cid", cid);
            DataSet ds = SQLDBUtil.ExecuteDataset("sb_4msVsCatVsGpsVsRat", sps);
            foreach(DataRow dr in ds.Tables[0].Rows) {
                vb_4msVsCatVsGpsVsRat e = new vb_4msVsCatVsGpsVsRat(
                   (int)dr["catID"], dr["CatName"].ToString(), (int)dr["GS"], (int)dr["RatID"], (int)dr["ModuleId"], dr["AccPath"].ToString());
                cl4msVsGroups.Add(e);
            }
            return cl4msVsGroups;
        }

    }
    public class PMCategoriesController :ApiController {
        [HttpGet]//http://localhost/Login/api/PMCategories?id=46
        public List<T_PM_Categories> GetPMCategories(int id) {//tested OK
            List<T_PM_Categories> T_PM_Categoriess = new List<T_PM_Categories>();
            SqlParameter[] sps = new SqlParameter[1];
            sps[0] = new SqlParameter("@cid", id);
            DataSet ds = SQLDBUtil.ExecuteDataset("sb_PMCategories", sps);
            //SqlDataReader rdr = SQLDBUtil.ExecuteReader("sb_PMCategories");
            foreach(DataRow rdr in ds.Tables[0].Rows) {
                T_PM_Categories e = new T_PM_Categories();
                e.Category_Id = (int)rdr[0];
                e.Category_Name = rdr[1].ToString();
                e.Cat_Type = (int)rdr[2];
                e.AssetTypeId = (int)rdr[3];
                e.ModuleId = (int)rdr[4];
                e.LedgerID = rdr[5] == DBNull.Value ? -1 : (int)rdr[5];
                e.IsActive = (bool)rdr[6];
                e.cid = (int)rdr[7];
                T_PM_Categoriess.Add(e);
            }
            return T_PM_Categoriess;
        }        [HttpGet]
        [Route("api/PMCategories/integrated")]//http://localhost/Login/api/pmCategories/integrated?cid=46
        public HttpResponseMessage pmCategoriesIntegrated(int cid) {//tested OK
            try {
                SqlParameter[] sps = new SqlParameter[1];
                sps[0] = new SqlParameter("@cid", cid);
                DataSet ds = SQLDBUtil.ExecuteDataset("sb_pmCategoriesIntegrated", sps);
                if(ds.Tables[0].Rows.Count > 0) {
                    return Request.CreateResponse(HttpStatusCode.OK, ds.Tables[0]);
                } else return Request.CreateResponse(HttpStatusCode.BadRequest, "No records to return!");
            } catch(Exception ex) {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }        [HttpPost]
        [Route("api/PMCategories/bulkInsert")] //http://localhost/Login/api/PMCategories/bulkInsert
        //[{Category_Id:2, Category_Name:"Aggregates", Cat_Type:1,	AssetTypeId:4,	ModuleId:7,	LedgerID:null,	IsActive:1, cid:1}]
        public HttpResponseMessage bulkInsert([FromBody]List<T_PM_Categories> cats) { //tested OK
            if(cats.Count > 0) {
                T_PM_Categories cat = new T_PM_Categories();
                DataTable dtCats = cat.dtTable("dtCats");
                foreach(T_PM_Categories R in cats) {
                    DataRow dr = dtCats.NewRow();
                    dr["Category_Id"] = R.Category_Id;
                    dr["Category_Name"] = R.Category_Name;
                    dr["Cat_Type"] = R.Cat_Type;
                    dr["AssetTypeId"] = R.AssetTypeId;
                    dr["ModuleId"] = R.ModuleId;
                    dr["LedgerID"] = DBNull.Value;
                    dr["IsActive"] = R.IsActive;
                    dr["cid"] = R.cid;
                    dtCats.Rows.Add(dr);
                }
                common.BulkInsertAny(dtCats, "T_PM_Categories");
                return Request.CreateResponse(HttpStatusCode.OK, cats.Count.ToString() + " Records Inserted");
            } else return Request.CreateResponse(HttpStatusCode.BadRequest, "No records to insert");
        }
    }
    public class UnitsController :ApiController {
        [HttpGet]
        public List<unit> GetUnits() { //http://localhost/Login/api/Units
            List<unit> list = new List<unit>();
            DataSet dscomp = SQLDBUtil.ExecuteDataset("OMS_Get_Unit");
            foreach(DataRow dr in dscomp.Tables[0].Rows) {
                unit t = new unit((int)dr["Au_ID"], dr["Au_Name"].ToString(), (int)dr["DUTId"]);
                list.Add(t);
            }
            return list;
        }

    }
    public class SectionsController :ApiController {
        public string erpCS { get { return ConfigurationManager.ConnectionStrings["ERPConnectionString"].ConnectionString; } }
        [HttpGet]
        public List<section> Get(string data) { //http://localhost/Login/api/Sections?data=1,1 
            string[] ar = data.Split(',');
            SqlParameter[] sps = new SqlParameter[2];
            sps[0] = new SqlParameter("@pid", int.Parse(ar[0]));
            sps[1] = new SqlParameter("@cid", int.Parse(ar[1])); List<section> list = new List<section>();
            DataSet ds = SQLDBUtil.ExecuteDataset("sb_taskSections", sps);
            foreach(DataRow dr in ds.Tables[0].Rows) {
                section t = new section((int)dr["SectionID"], dr["SectionName"].ToString());
                list.Add(t);
            }
            return list;
        }
        // GET api/values/5 http://localhost/Login/api/Sections/5
        public string Get(int id) {
            string SectionName = "";
            using(SqlConnection con = new SqlConnection(erpCS)) {
                SqlCommand cmd = new SqlCommand("select SectionName from T_OMS_Sections where SectionID = " + id.ToString(), con);
                con.Open();
                SectionName = cmd.ExecuteScalar().ToString();
                cmd.Dispose();
                con.Close();
            }
            return SectionName;
        }
        // POST api/values
        public HttpResponseMessage Post([FromBody] secJson json) {
            try {
                //http://localhost/login/api/Sections >>>> {Section:"Sanitory Works", pid: 1, cid: 1}
                SqlParameter[] sps = new SqlParameter[3];
                sps[0] = new SqlParameter("@sectionName", json.Section);
                sps[1] = new SqlParameter("@pid", json.pid);
                sps[2] = new SqlParameter("@cid", json.cid);
                object id = SQLDBUtil.ExecuteScalar("sh_AddSection", sps);
                section t = new section((int)id, json.Section);
                return Request.CreateResponse(HttpStatusCode.OK, t.Section + " is created/activated!");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpPost]
        [Route("api/sections/bulkInsert")] //http://localhost/Login/api/sections/bulkInsert?pid=1&uid=1 [{Section:"Sanitory Works"}]
        public HttpResponseMessage bulkInsert([FromBody] List<string> names, int pid, int uid) {
            try {
                DataTable dt = new DataTable();
                dt.Columns.Add("name", typeof(System.String));
                foreach(string s in names) {
                    DataRow dr = dt.NewRow();
                    dr["name"] = s;
                    dt.Rows.Add(dr);
                }
                SqlParameter[] sps = new SqlParameter[3];
                sps[0] = new SqlParameter("@names", dt);
                sps[1] = new SqlParameter("@pid", pid);
                sps[2] = new SqlParameter("@uid", uid);
                DataSet ds = SQLDBUtil.ExecuteDataset("sb_AddSections", sps);
                if(ds.Tables[0].Rows.Count > 0) {
                    return Request.CreateResponse(HttpStatusCode.OK, ds.Tables[0]);
                } else return Request.CreateResponse(HttpStatusCode.OK, ds.Tables[0].Rows[0][0].ToString());
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        // PUT api/values/5
        public HttpResponseMessage Put(int id, [FromBody]string str) {
            // //http://localhost/login/api/Sections/5 >>>>  "Signages Works" 
            try {
                SqlParameter[] sps = new SqlParameter[2];
                sps[0] = new SqlParameter("@sectionName", str);
                sps[1] = new SqlParameter("@sid", id);
                DataSet ds = SQLDBUtil.ExecuteDataset("sh_EditSection", sps);
                if(ds.Tables[0].Rows[0][0].ToString() == "success") return Request.CreateResponse(HttpStatusCode.OK, "success");
                else return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Name already exists");
            } catch(Exception ex) {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        // DELETE api/values/5
        public HttpResponseMessage Delete(int id) { //http://localhost/login/api/Sections/8  
            using(SqlConnection con = new SqlConnection(erpCS)) {
                SqlCommand cmd = new SqlCommand("update T_OMS_Sections set IsActive = 0 where SectionID = " + id.ToString(), con);
                con.Open();
                cmd.ExecuteScalar();
                cmd.Dispose();
                con.Close();
                return Request.CreateResponse(HttpStatusCode.OK, "Section with ID: " + id.ToString() + " is deleted!");
            }
        }
    }
    public class NestedController :ApiController {
        [HttpPost]
        public HttpResponseMessage GetUserMenu(int ModuleID, string ModuleName, string ModuleCode) { //http://localhost/Login/api/Nested?ModuleID=1&ModuleName=HMS&ModuleCode=HMS
            HttpError error = null;
            try {
                DataSet ds = SQLDBUtil.ExecuteDataset("cs_userModuleMenu_Cloud", new SqlParameter[]
                        { new SqlParameter("@mids", ModuleID), new SqlParameter("@uid", 12) });//?????????????
                List<NavMenu> lstMenu = ds.Tables[0].AsEnumerable().Select(datarow => new NavMenu {
                    id = datarow.Field<int>("MenuID"),
                    title = datarow.Field<string>("MenuName"),
                    code = datarow.Field<string>("ModuleName"),
                    parent = datarow.Field<int?>("Under"),
                    order = datarow.Field<int?>("PreOrder"),
                    type = "item",
                    url = datarow.Field<string>("URL"),
                }).ToList().OrderBy(s => s.parent).ToList();
                if(lstMenu != null) {
                    List<NavMenu> mainMenu = new List<NavMenu>();
                    List<NavMenu> herarichy = GetChildrens(lstMenu, null);
                    if(herarichy != null) {
                        herarichy.FindAll(s => s.children != null && s.children.Count > 0).ForEach(s => s.type = "collapse");
                        NavMenu main = new NavMenu { children = herarichy, type = "collapse", id = 0, title = ModuleName, code = ModuleCode };
                        mainMenu.Add(main);
                        return Request.CreateResponse(HttpStatusCode.OK, mainMenu);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, herarichy);
                } else {
                    error = new HttpError("No Record Found");
                    return Request.CreateResponse(HttpStatusCode.NotFound, error);
                }
            } catch(Exception ex) {
                error = new HttpError(ex.Message);
                return Request.CreateResponse(HttpStatusCode.BadRequest, error);
            }
        }
        private List<NavMenu> GetChildrens(List<NavMenu> allElements, NavMenu currentElement) {
            List<NavMenu> children = null;
            if(currentElement != null) {
                children = allElements.FindAll(s => s.parent == currentElement.id);
            } else {
                children = allElements.FindAll(s => s.parent == null);
            }
            foreach(NavMenu item in children) {
                item.children = GetChildrens(allElements, item);
            }
            return children;
        }
    }
    public class project {
        public int ID { get; set; }
        public string Project { get; set; }
        public project(int id, string project) {
            ID = id; Project = project;
        }
    }
    public class employee {
        public int ID { get; set; }
        public string EmployeeName { get; set; }
        public employee(int id, string empName) {
            ID = id; EmployeeName = empName;
        }
    }
    public class section {
        public int ID { get; set; }
        public string Section { get; set; }
        public section(int id, string section) {
            ID = id; Section = section;
        }
    }
    public class secJson {
        public string Section { get; set; }
        public int pid { get; set; }
        public int cid { get; set; }
        public secJson(string section, int pID, int cID) {
            Section = section; pid = pID; cid = cID;
        }
    }
    public class unit {
        public int ID { get; set; }
        public string Unit { get; set; }
        public int duID { get; set; }
        public unit(int id, string unit, int duid) {
            ID = id; Unit = unit; duID = duid;
        }
    }
    public class task {
        public int CompanyId { get; set; }
        public int ProjectID { get; set; }
        public int slno { get; set; }
        public int TaskID { get; set; }
        public int UnitID { get; set; }
        public int SectionID { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public int IsActive { get; set; }
        public int TaskMasterID { get; set; }
        public int? LinkTo { get; set; }
        public string TaskRefNo { get; set; }
        public string TaskName { get; set; }
        public string Unit { get; set; }
        public string Section { get; set; }
        public string TaskDesc { get; set; }
        public string Method { get; set; }
        public string StDt { get; set; }
        public string EdDt { get; set; }
        public string MTask { get; set; }
        public string GDesc { get; set; }
        public string BaseLine { get; set; }
        public double Quantity { get; set; }
        public double RaQty { get; set; }
        public double TotalCost { get; set; }
        public double TotalLength { get; set; }
        public double TaskCost { get; set; }
        public double AdFixed { get; set; }
        public double Adruning { get; set; }
        public double OP { get; set; }
        public double TenderRate { get; set; }
        public double AgreementRate { get; set; }
        public string XlRef { get; set; }
        public task() { }
        public task(int companyId, int projectID, int Slno, int taskID, int unitID, int sectionID, int createdBy, int updatedBy,
            int isActive, int taskMasterID, int? linkTo, string taskRefNo, string taskName, string taskDesc, string method, string stDt,
            string edDt, string mTask, string gDesc, string baseLine, string unit, string section, double quantity, double raQty,
            double totalCost, double totalLength, double taskCost, double adFixed, double adruning, double oP, double tenderRate, double agreementRate, string _xlRef) {
            CompanyId = companyId; ProjectID = projectID; slno = Slno; TaskID = taskID; UnitID = unitID; SectionID = sectionID; Unit = unit; Section = section;
            CreatedBy = createdBy; UpdatedBy = updatedBy; IsActive = isActive; TaskMasterID = taskMasterID; LinkTo = linkTo;
            TaskRefNo = taskRefNo; TaskName = taskName; TaskDesc = taskDesc; Method = method; StDt = stDt; EdDt = edDt;
            MTask = mTask; GDesc = gDesc; BaseLine = baseLine; Quantity = quantity; RaQty = raQty; TotalCost = totalCost;
            TotalLength = totalLength; TaskCost = taskCost; AdFixed = adFixed; Adruning = adruning; OP = oP;
            TenderRate = tenderRate; AgreementRate = agreementRate; XlRef = _xlRef;
        }
        public DataTable Dttask(string tblName) { // equivalent to table type '@TaskType' in DATABASE
            DataTable dtName = new DataTable(tblName);
            DataColumn TaskID = new DataColumn();
            TaskID.DataType = System.Type.GetType("System.Int32");
            TaskID.ColumnName = "TaskID";
            dtName.Columns.Add(TaskID);

            dtName.Columns.Add("slno", typeof(System.Int32));
            dtName.Columns.Add("ProjectID", typeof(System.Int32));
            dtName.Columns.Add("BaseID", typeof(System.Int32));
            dtName.Columns.Add("TaskRefNo", typeof(System.String));
            dtName.Columns.Add("TaskName", typeof(System.String));
            dtName.Columns.Add("TaskDesc", typeof(System.String));
            dtName.Columns.Add("GDesc", typeof(System.String));
            dtName.Columns.Add("LinkTo", typeof(System.Int32));
            dtName.Columns.Add("TaskMasterID", typeof(System.Int32));
            dtName.Columns.Add("UnitID", typeof(System.Int32));
            dtName.Columns.Add("SectionID", typeof(System.Int32));
            dtName.Columns.Add("Quantity", typeof(System.Double));
            dtName.Columns.Add("Method", typeof(System.String));
            dtName.Columns.Add("RaQty", typeof(System.Double));
            dtName.Columns.Add("StDt", typeof(System.String));
            dtName.Columns.Add("EdDt", typeof(System.String));
            dtName.Columns.Add("UpdatedBy", typeof(System.Int32));
            dtName.Columns.Add("IsActive", typeof(System.Int32));
            dtName.Columns.Add("TotalCost", typeof(System.Double));
            dtName.Columns.Add("TaskCost", typeof(System.Double));
            dtName.Columns.Add("AdFixed", typeof(System.Double));
            dtName.Columns.Add("Adruning", typeof(System.Double));
            dtName.Columns.Add("OP", typeof(System.Double));
            dtName.Columns.Add("TenderRate", typeof(System.Double));
            dtName.Columns.Add("AgreementRate", typeof(System.Double));
            DataColumn[] keys = new DataColumn[1];
            keys[0] = TaskID;
            dtName.PrimaryKey = keys;
            return dtName;
        }
    }
    public class xlTask {
        public int ProjectID { get; set; }
        public int slno { get; set; }
        public int TaskID { get; set; }
        public int UnitID { get; set; }
        public int SectionID { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public int IsActive { get; set; }
        public int TaskMasterID { get; set; }
        public int? LinkTo { get; set; }
        public string TaskRefNo { get; set; }
        public string TaskName { get; set; }
        public string Unit { get; set; }
        public string Section { get; set; }
        public string TaskDesc { get; set; }
        public string Method { get; set; }
        public string StDt { get; set; }
        public string EdDt { get; set; }
        public string MTask { get; set; }
        public string GDesc { get; set; }
        public string BaseLine { get; set; }
        public double Quantity { get; set; }
        public double RaQty { get; set; }
        public double TotalCost { get; set; }
        public double TotalLength { get; set; }
        public double TaskCost { get; set; }
        public double AdFixed { get; set; }
        public double Adruning { get; set; }
        public double OP { get; set; }
        public double TenderRate { get; set; }
        public double AgreementRate { get; set; }
        public xlTask() { }
        public xlTask(int companyId, int projectID, int Slno, int taskID, int unitID, int sectionID, int createdBy, int updatedBy,
            int isActive, int taskMasterID, int? linkTo, string taskRefNo, string taskName, string taskDesc, string method, string stDt,
            string edDt, string mTask, string gDesc, string baseLine, string unit, string section, double quantity, double raQty,
            double totalCost, double totalLength, double taskCost, double adFixed, double adruning, double oP, double tenderRate, double agreementRate) {
            ProjectID = projectID; slno = Slno; TaskID = taskID; UnitID = unitID; SectionID = sectionID; Unit = unit; Section = section;
            CreatedBy = createdBy; UpdatedBy = updatedBy; IsActive = isActive; TaskMasterID = taskMasterID; LinkTo = linkTo;
            TaskRefNo = taskRefNo; TaskName = taskName; TaskDesc = taskDesc; Method = method; StDt = stDt; EdDt = edDt;
            MTask = mTask; GDesc = gDesc; BaseLine = baseLine; Quantity = quantity; RaQty = raQty; TotalCost = totalCost;
            TotalLength = totalLength; TaskCost = taskCost; AdFixed = adFixed; Adruning = adruning; OP = oP; TenderRate = tenderRate; AgreementRate = agreementRate;
        }
        public DataTable Dttask(string tblName) {
            DataTable dtName = new DataTable(tblName);
            DataColumn TaskID = new DataColumn();
            TaskID.DataType = System.Type.GetType("System.Int32");
            TaskID.ColumnName = "TaskID";
            dtName.Columns.Add(TaskID);

            dtName.Columns.Add("slno", typeof(System.Int32));
            dtName.Columns.Add("ProjectID", typeof(System.Int32));
            dtName.Columns.Add("BaseID", typeof(System.Int32));
            dtName.Columns.Add("TaskRefNo", typeof(System.String));
            dtName.Columns.Add("TaskName", typeof(System.String));
            dtName.Columns.Add("TaskDesc", typeof(System.String));
            dtName.Columns.Add("GDesc", typeof(System.String));
            dtName.Columns.Add("LinkTo", typeof(System.Int32));
            dtName.Columns.Add("TaskMasterID", typeof(System.Int32));
            dtName.Columns.Add("UnitID", typeof(System.Int32));
            dtName.Columns.Add("SectionID", typeof(System.Int32));
            dtName.Columns.Add("Quantity", typeof(System.Double));
            dtName.Columns.Add("Method", typeof(System.String));
            dtName.Columns.Add("RaQty", typeof(System.Double));
            dtName.Columns.Add("StDt", typeof(System.String));
            dtName.Columns.Add("EdDt", typeof(System.String));
            dtName.Columns.Add("UpdatedBy", typeof(System.Int32));
            dtName.Columns.Add("IsActive", typeof(System.Int32));
            dtName.Columns.Add("TotalCost", typeof(System.Double));
            dtName.Columns.Add("TaskCost", typeof(System.Double));
            dtName.Columns.Add("AdFixed", typeof(System.Double));
            dtName.Columns.Add("Adruning", typeof(System.Double));
            dtName.Columns.Add("OP", typeof(System.Double));
            dtName.Columns.Add("TenderRate", typeof(System.Double));
            dtName.Columns.Add("AgreementRate", typeof(System.Double));
            DataColumn[] keys = new DataColumn[1];
            keys[0] = TaskID;
            dtName.PrimaryKey = keys;
            return dtName;
        }
    }
    public class taskMin {
        public string TaskRefNo { get; set; }
        public string TaskName { get; set; }
        public string TaskDesc { get; set; }
        public int UnitID { get; set; }
        public int SectionID { get; set; }
        public double Quantity { get; set; }
        public int ProjectID { get; set; }
        public int UserID { get; set; }
        public taskMin() { }
        public taskMin(string taskRefNo, string taskName, string taskDesc, int unitID, int sectionID, double quantity, int projectID, int userID) {
            TaskRefNo = taskRefNo; TaskName = taskName; TaskDesc = taskDesc; UnitID = unitID; SectionID = sectionID; Quantity = quantity;
            UserID = UserID; ProjectID = projectID;
        }
    }
    public class gBudget {
        public int SectionID { get; set; }
        public string SectionName { get; set; }
        public int TaskID { get; set; }
        public int OrderID { get; set; }
        public string TaskName { get; set; }
        public double Amount { get; set; }
        public double SharePc { get; set; }
    }
    public class pBudget {
        public int SectionID { get; set; }
        public string SectionName { get; set; }
        public double Amount { get; set; }
        public double SharePc { get; set; }
    }
    // DUTId, DUTName  from T_G_DerivedUnitTypes
    public class DerivedUnitTypes {
        public int DUTId { get; }
        public string DUTName { get; }
        public DerivedUnitTypes(int rcID, string catName) {
            DUTId = rcID; DUTName = catName;
        }
    }
    public class BillGenType {
        public int typeID { get; }
        public string typeName { get; }
        public BillGenType(int rcID, string catName) {
            typeID = rcID; typeName = catName;
        }
    }
    public class ClassMaster {
        public int clsID { get; }
        public string clsName { get; }
        public ClassMaster(int rcID, string catName) {
            clsID = rcID; clsName = catName;
        }
    }
    public class resCat {
        public int CategoryID { get; }
        public string CategoryName { get; }
        public resCat(int rcID, string catName) {
            CategoryID = rcID; CategoryName = catName;
        }
    }
    public class t_oms_projects {
        public int ProjectID { get; set; }
        public string ProjectName { get; set; }
        public string ProjectDesc { get; set; }
        public int Employer { get; set; }
        public string ProjectLocation { get; set; }
        public string ProjectAddress { get; set; }
        public int ProjectDuration { get; set; }
        public DateTime ProjectSchStartDate { get; set; }
        public DateTime ProjectSchFinishDate { get; set; }
        public DateTime ProjectStartDate { get; set; }
        public DateTime ProjectEndDate { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public int IsActive { get; set; }
        public int Tenderid { get; set; }
        public int ProjectType { get; set; }
        public int Factorid { get; set; }//use for temporary session 
        public decimal value { get; set; }
        public string PROGROUP { get; set; }
        public decimal MAINTENANCECOST { get; set; }
        public string Loa { get; set; }
        public string refNo { get; set; }
        public int PrincipalContractor { get; set; }
        public int StateId { get; set; }
        public int CountryId { get; set; }
        public int RevLedgerId { get; set; }
        public int Status { get; set; }
        public int EmployerOffice { get; set; }
        public string FileExt { get; set; }
        public int PrincipleEmployer { get; set; }
        public int PrincipleEmployerOffice { get; set; }
        public int RecordableOffice { get; set; }
        public int PrincipleRecordableOffice { get; set; }
        public int UoM_Currency { get; set; }
        public int ProgressType { get; set; }
        public decimal ExecutedValue { get; set; }
        public string Remarks { get; set; }
        public int SpecialStatusID { get; set; }
        public string SpecialRemarks { get; set; }
        public decimal ContractCallValue { get; set; }
        public decimal BudgetedValue { get; set; }
        public decimal ActualExpenditure { get; set; }
        public decimal ActualBilledValue { get; set; }
        public int ExpLedgerID { get; set; }
        public int CompanyId { get; set; }

        public t_oms_projects() { }
        public t_oms_projects(int _ProjectID, string _ProjectName, string _ProjectDesc, int _Employer, string _ProjectLocation
            , string _ProjectAddress, int _ProjectDuration, DateTime _ProjectSchStartDate, DateTime _ProjectSchFinishDate
            , DateTime _ProjectStartDate, DateTime _ProjectEndDate, DateTime _CreatedOn, int _CreatedBy, DateTime _UpdatedOn
            , int _UpdatedBy, int _IsActive, int _Tenderid, int _ProjectType, int _Factorid, decimal _value, string _PROGROUP
            , decimal _MAINTENANCECOST, string _Loa, string _refNo, int _PrincipalContractor, int _StateId, int _CountryId
            , int _RevLedgerId, byte _Status, int _EmployerOffice, string _FileExt, int _PrincipleEmployer, int _PrincipleEmployerOffice
            , int _RecordableOffice, int _PrincipleRecordableOffice, int _UoM_Currency, byte _ProgressType, decimal _ExecutedValue
            , string _Remarks, byte _SpecialStatusID, string _SpecialRemarks, decimal _ContractCallValue, decimal _BudgetedValue
            , decimal _ActualExpenditure, decimal _ActualBilledValue, int _ExpLedgerID, int _CompanyId) {
            ProjectID = _ProjectID; ProjectName = _ProjectName; ProjectDesc = _ProjectDesc; Employer = _Employer;
            ProjectLocation = _ProjectLocation; ProjectAddress = _ProjectAddress; ProjectDuration = _ProjectDuration;
            ProjectSchStartDate = _ProjectSchStartDate; ProjectSchFinishDate = _ProjectSchFinishDate; ProjectStartDate = _ProjectStartDate;
            ProjectEndDate = _ProjectEndDate; CreatedOn = _CreatedOn; CreatedBy = _CreatedBy; UpdatedOn = _UpdatedOn;
            UpdatedBy = _UpdatedBy; IsActive = _IsActive; Tenderid = _Tenderid; ProjectType = _ProjectType; Factorid = _Factorid;
            value = _value; PROGROUP = _PROGROUP; MAINTENANCECOST = _MAINTENANCECOST; Loa = _Loa; refNo = _refNo;
            PrincipalContractor = _PrincipalContractor; StateId = _StateId; CountryId = _CountryId; RevLedgerId = _RevLedgerId;
            Status = _Status; EmployerOffice = _EmployerOffice; FileExt = _FileExt; PrincipleEmployer = _PrincipleEmployer;
            PrincipleEmployerOffice = _PrincipleEmployerOffice; RecordableOffice = _RecordableOffice;
            PrincipleRecordableOffice = _PrincipleRecordableOffice; UoM_Currency = _UoM_Currency; ProgressType = _ProgressType;
            ExecutedValue = _ExecutedValue; Remarks = _Remarks; SpecialStatusID = _SpecialStatusID; SpecialRemarks = _SpecialRemarks;
            ContractCallValue = _ContractCallValue; BudgetedValue = _BudgetedValue; ActualExpenditure = _ActualExpenditure;
            ActualBilledValue = _ActualBilledValue; ExpLedgerID = _ExpLedgerID; CompanyId = _CompanyId;
        }
        public DataTable dtClass(string tblName) {
            DataTable dtName = new DataTable(tblName);
            //DataColumn ProjectID = new DataColumn();
            //ProjectID.DataType = System.Type.GetType("System.Int32");
            //ProjectID.ColumnName = "ProjectID";
            //ProjectID.AutoIncrement = true;
            //dtName.Columns.Add(ProjectID);
            dtName.Columns.Add("ProjectID", typeof(System.Int32));
            dtName.Columns.Add("ProjectName", typeof(System.String));
            dtName.Columns.Add("ProjectDesc", typeof(System.String));
            dtName.Columns.Add("Employer", typeof(System.Int32));
            dtName.Columns.Add("ProjectLocation", typeof(System.String));
            dtName.Columns.Add("ProjectAddress", typeof(System.String));
            dtName.Columns.Add("ProjectDuration", typeof(System.Int32));
            dtName.Columns.Add("ProjectSchStartDate", typeof(System.DateTime));
            dtName.Columns.Add("ProjectSchFinishDate", typeof(System.DateTime));
            dtName.Columns.Add("ProjectStartDate", typeof(System.DateTime));
            dtName.Columns.Add("ProjectEndDate", typeof(System.DateTime));
            dtName.Columns.Add("CreatedOn", typeof(System.DateTime));
            dtName.Columns.Add("CreatedBy", typeof(System.Int32));
            dtName.Columns.Add("UpdatedOn", typeof(System.DateTime));
            dtName.Columns.Add("UpdatedBy", typeof(System.Int32));
            dtName.Columns.Add("IsActive", typeof(System.Int32));
            dtName.Columns.Add("Tenderid", typeof(System.Int32));
            dtName.Columns.Add("ProjectType", typeof(System.Int32));
            dtName.Columns.Add("Factorid", typeof(System.Int32));
            dtName.Columns.Add("value", typeof(System.Double));
            dtName.Columns.Add("PROGROUP", typeof(System.String));
            dtName.Columns.Add("MAINTENANCECOST", typeof(System.Double));
            dtName.Columns.Add("Loa", typeof(System.String));
            dtName.Columns.Add("refNo", typeof(System.String));
            dtName.Columns.Add("PrincipalContractor", typeof(System.Int32));
            dtName.Columns.Add("StateId", typeof(System.Int32));
            dtName.Columns.Add("CountryId", typeof(System.Int32));
            dtName.Columns.Add("RevLedgerId", typeof(System.Int32));
            dtName.Columns.Add("Status", typeof(System.Byte));
            dtName.Columns.Add("EmployerOffice", typeof(System.Int32));
            dtName.Columns.Add("FileExt", typeof(System.String));
            dtName.Columns.Add("PrincipleEmployer", typeof(System.Int32));
            dtName.Columns.Add("PrincipleEmployerOffice", typeof(System.Int32));
            dtName.Columns.Add("RecordableOffice", typeof(System.Int32));
            dtName.Columns.Add("PrincipleRecordableOffice", typeof(System.Int32));
            dtName.Columns.Add("UoM_Currency", typeof(System.Int32));
            dtName.Columns.Add("ProgressType", typeof(System.Byte));
            dtName.Columns.Add("ExecutedValue", typeof(System.Double));
            dtName.Columns.Add("Remarks", typeof(System.String));
            dtName.Columns.Add("SpecialStatusID", typeof(System.Byte));
            dtName.Columns.Add("SpecialRemarks", typeof(System.String));
            dtName.Columns.Add("ContractCallValue", typeof(System.Double));
            dtName.Columns.Add("BudgetedValue", typeof(System.Double));
            dtName.Columns.Add("ActualExpenditure", typeof(System.Double));
            dtName.Columns.Add("ActualBilledValue", typeof(System.Double));
            dtName.Columns.Add("ExpLedgerID", typeof(System.Int32));
            dtName.Columns.Add("CompanyId", typeof(System.Int32));
            //DataColumn[] keys = new DataColumn[1];
            //dtName.PrimaryKey = keys;
            return dtName;
        }
        public DataTable dtTrunc(string tblName) {
            DataTable dtName = new DataTable(tblName);
            dtName.Columns.Add("ProjectID", typeof(System.Int32));
            dtName.Columns.Add("ProjectName", typeof(System.String));
            dtName.Columns.Add("ProjectDesc", typeof(System.String));
            dtName.Columns.Add("Employer", typeof(System.Int32));
            dtName.Columns.Add("ProjectLocation", typeof(System.String));
            dtName.Columns.Add("ProjectAddress", typeof(System.String));
            dtName.Columns.Add("ProjectDuration", typeof(System.Int32));
            dtName.Columns.Add("ProjectSchStartDate", typeof(System.DateTime));
            dtName.Columns.Add("ProjectType", typeof(System.Int32));
            dtName.Columns.Add("value", typeof(System.Double));
            dtName.Columns.Add("Loa", typeof(System.String));
            dtName.Columns.Add("refNo", typeof(System.String));
            dtName.Columns.Add("PrincipalContractor", typeof(System.Int32));
            dtName.Columns.Add("StateId", typeof(System.Int32));
            dtName.Columns.Add("Status", typeof(System.Byte));
            dtName.Columns.Add("EmployerOffice", typeof(System.Int32));
            dtName.Columns.Add("PrincipleEmployer", typeof(System.Int32));
            dtName.Columns.Add("PrincipleEmployerOffice", typeof(System.Int32));
            dtName.Columns.Add("UoM_Currency", typeof(System.Int32));
            dtName.Columns.Add("ProgressType", typeof(System.Byte));
            dtName.Columns.Add("CompanyId", typeof(System.Int32));
            return dtName;
        }
    }

}
