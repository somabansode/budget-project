﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Login.Controllers.MMS
{
    public class ProdIssuesController : ApiController
    {
        [HttpGet] // http://localhost/Login/api/ProdIssues/Resources?pid=71&dtFrom=2019-04-13&dtTo=2019-04-13&rid=null
        [Route("api/ProdIssues/Resources")]
        public async Task<HttpResponseMessage> Resources(int pid, DateTime dtFrom, DateTime dtTo, int? rid)
        {
            try
            {
                List<materialsForIssueRes> clmaterialsForIssueRes = await Task.Run(() =>
                            prodIssuesDAL.Resources(pid, dtFrom, dtTo, rid));
                return Request.CreateResponse(HttpStatusCode.OK, clmaterialsForIssueRes);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet] // http://localhost/Login/api/ProdIssues/TASKs?pid=71&dtFrom=2019-04-13&dtTo=2019-04-13&rid=null
        [Route("api/ProdIssues/TASKs")]
        public async Task<HttpResponseMessage> TASKs(int pid, DateTime dtFrom, DateTime dtTo, int? rid)
        {
            try
            {
                List<materialsForIssueTASKs> clmaterialsForIssueTASKs = await Task.Run(() =>
                prodIssuesDAL.TASKs(pid, dtFrom, dtTo, rid));
                return Request.CreateResponse(HttpStatusCode.OK, clmaterialsForIssueTASKs);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet] // http://localhost/Login/api/ProdIssues/STs?pid=71&dtFrom=2019-04-13&dtTo=2019-04-13&tid=3088&rid=null
        [Route("api/ProdIssues/STs")]
        public async Task<HttpResponseMessage> STs(int pid, DateTime dtFrom, DateTime dtTo, int tid, int? rid)
        {
            try
            {
                List<materialsForIssueSTs> clmaterialsForIssueSTs = await Task.Run(() =>
                prodIssuesDAL.STs(pid, dtFrom, dtTo, tid, rid));
                return Request.CreateResponse(HttpStatusCode.OK, clmaterialsForIssueSTs);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet] // http://localhost/Login/api/ProdIssues/RSPlans?pid=71&dtFrom=2019-04-13&dtTo=2019-04-13&stid=24547&rid=null
        [Route("api/ProdIssues/RSPlans")]
        public async Task<HttpResponseMessage> RSPlans(int pid, DateTime dtFrom, DateTime dtTo, int stid, int rid)
        {
            try
            {
                List<materialsForIssueRSPlans> clmaterialsForIssueRSPlans = await Task.Run(() =>
                prodIssuesDAL.RSPlans(pid, dtFrom, dtTo, stid, rid));
                return Request.CreateResponse(HttpStatusCode.OK, clmaterialsForIssueRSPlans);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
        [HttpGet]// http://localhost/Login/api/ProdIssues/GdnPopUp?pid=71&dtFrom=2019-04-13&dtTo=2019-04-13&rid=null
        [Route("api/ProdIssues/GdnPopUp")]
        public async Task<HttpResponseMessage> GdnPopUp(int pid, DateTime dtFrom, DateTime dtTo, int rid)
        {
            try
            {
                List<materialsForIssuePop> clmaterialsForIssuePop = await Task.Run(() =>
                prodIssuesDAL.GdnPopUp(pid, dtFrom, dtTo, rid));
                return Request.CreateResponse(HttpStatusCode.OK, clmaterialsForIssuePop);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }
}

