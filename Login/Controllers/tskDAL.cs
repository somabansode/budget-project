﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Login.Controllers {
    public class tskDAL {
        public static async Task<List<t_oms_projects>> ReadProjectsAsync(int cid) {
            List<t_oms_projects> clt_oms_projects = new List<t_oms_projects>();
            using(SqlConnection con = new SqlConnection(common.erpCS)) {
                await con.OpenAsync();
                string queryString = "select * from t_oms_projects where CompanyId = @cid and IsActive = 1";
                using(SqlCommand cmd = new SqlCommand(queryString, con)) {
                    //cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@cid", cid));
                    using(SqlDataReader rdr = await cmd.ExecuteReaderAsync()) {
                        while(await rdr.ReadAsync()) {
                            t_oms_projects t = new t_oms_projects();
                            t.ProjectID = rdr["ProjectID"] == DBNull.Value ? -1 : (int)rdr["ProjectID"];
                            t.ProjectName = rdr["ProjectName"] == DBNull.Value ? "" : rdr["ProjectName"].ToString();
                            t.ProjectDesc = rdr["ProjectDesc"] == DBNull.Value ? "" : rdr["ProjectDesc"].ToString();
                            t.Employer = rdr["Employer"] == DBNull.Value ? -1 : (int)rdr["Employer"];
                            t.ProjectLocation = rdr["ProjectLocation"] == DBNull.Value ? "" : rdr["ProjectLocation"].ToString();
                            t.ProjectAddress = rdr["ProjectAddress"] == DBNull.Value ? "" : rdr["ProjectAddress"].ToString();
                            t.ProjectDuration = rdr["ProjectDuration"] == DBNull.Value ? -1 : (int)rdr["ProjectDuration"];
                            t.ProjectSchStartDate = rdr["ProjectSchStartDate"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["ProjectSchStartDate"]);
                            t.ProjectSchFinishDate = rdr["ProjectSchFinishDate"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["ProjectSchFinishDate"]);
                            t.ProjectStartDate = rdr["ProjectStartDate"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["ProjectStartDate"]);
                            t.ProjectEndDate = rdr["ProjectEndDate"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["ProjectEndDate"]);
                            t.CreatedOn = rdr["CreatedOn"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["CreatedOn"]);
                            t.CreatedBy = rdr["CreatedBy"] == DBNull.Value ? -1 : (int)rdr["CreatedBy"];
                            t.UpdatedOn = rdr["UpdatedOn"] == DBNull.Value ? DateTime.Today : Convert.ToDateTime(rdr["UpdatedOn"]);
                            t.UpdatedBy = rdr["UpdatedBy"] == DBNull.Value ? -1 : (int)rdr["UpdatedBy"];
                            t.IsActive = rdr["IsActive"] == DBNull.Value ? -1 : (int)rdr["IsActive"];
                            t.Tenderid = rdr["Tenderid"] == DBNull.Value ? -1 : (int)rdr["Tenderid"];
                            t.ProjectType = rdr["ProjectType"] == DBNull.Value ? -1 : (int)rdr["ProjectType"];
                            t.Factorid = rdr["Factorid"] == DBNull.Value ? -1 : (int)rdr["Factorid"];
                            t.value = rdr["value"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["value"]);
                            t.PROGROUP = rdr["PROGROUP"] == DBNull.Value ? "" : rdr["PROGROUP"].ToString();
                            t.MAINTENANCECOST = rdr["MAINTENANCECOST"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["MAINTENANCECOST"]);
                            t.Loa = rdr["Loa"] == DBNull.Value ? "" : rdr["Loa"].ToString();
                            t.refNo = rdr["refNo"] == DBNull.Value ? "" : rdr["refNo"].ToString();
                            t.PrincipalContractor = rdr["PrincipalContractor"] == DBNull.Value ? -1 : (int)rdr["PrincipalContractor"];
                            t.StateId = rdr["StateId"] == DBNull.Value ? -1 : (int)rdr["StateId"];
                            t.CountryId = rdr["CountryId"] == DBNull.Value ? -1 : (int)rdr["CountryId"];
                            t.RevLedgerId = rdr["RevLedgerId"] == DBNull.Value ? -1 : (int)rdr["RevLedgerId"];
                            t.Status = rdr["Status"] == DBNull.Value ? -1 : (byte)rdr["Status"];
                            t.EmployerOffice = rdr["EmployerOffice"] == DBNull.Value ? -1 : (int)rdr["EmployerOffice"];
                            t.FileExt = rdr["FileExt"] == DBNull.Value ? "" : rdr["FileExt"].ToString();
                            t.PrincipleEmployer = rdr["PrincipleEmployer"] == DBNull.Value ? -1 : (int)rdr["PrincipleEmployer"];
                            t.PrincipleEmployerOffice = rdr["PrincipleEmployerOffice"] == DBNull.Value ? -1 : (int)rdr["PrincipleEmployerOffice"];
                            t.RecordableOffice = rdr["RecordableOffice"] == DBNull.Value ? -1 : (int)rdr["RecordableOffice"];
                            t.PrincipleRecordableOffice = rdr["PrincipleRecordableOffice"] == DBNull.Value ? -1 : (int)rdr["PrincipleRecordableOffice"];
                            t.UoM_Currency = rdr["UoM_Currency"] == DBNull.Value ? -1 : (int)rdr["UoM_Currency"];
                            t.ProgressType = rdr["ProgressType"] == DBNull.Value ? -1 : (byte)rdr["ProgressType"];
                            t.ExecutedValue = rdr["ExecutedValue"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["ExecutedValue"]);
                            t.Remarks = rdr["Remarks"] == DBNull.Value ? "" : rdr["Remarks"].ToString();
                            t.SpecialStatusID = rdr["SpecialStatusID"] == DBNull.Value ? -1 : (byte)rdr["SpecialStatusID"];
                            t.SpecialRemarks = rdr["SpecialRemarks"] == DBNull.Value ? "" : rdr["SpecialRemarks"].ToString();
                            t.ContractCallValue = rdr["ContractCallValue"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["ContractCallValue"]);
                            t.BudgetedValue = rdr["BudgetedValue"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["BudgetedValue"]);
                            t.ActualExpenditure = rdr["ActualExpenditure"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["ActualExpenditure"]);
                            t.ActualBilledValue = rdr["ActualBilledValue"] == DBNull.Value ? 0 : Convert.ToDecimal(rdr["ActualBilledValue"]);
                            t.ExpLedgerID = rdr["ExpLedgerID"] == DBNull.Value ? -1 : (int)rdr["ExpLedgerID"];
                            t.CompanyId = rdr["CompanyId"] == DBNull.Value ? -1 : (int)rdr["CompanyId"];
                            clt_oms_projects.Add(t);
                        }
                    }
                }
                con.Close();
                return clt_oms_projects;
            }
        }
        public static async Task<string> CreateProjectsAsync(List<t_oms_projects> clProjects, int cid, int ws, int userID) {
            try {
                // [{ProjectName: 'Angular Project', ProjectDesc: 'Angular Project', Employer: 1, ProjectLocation: 'Khammam', ProjectAddress: 'Khammam', ProjectDuration: 6
                // , ProjectSchStartDate: '2019-03-29',ProjectType: 2,  value: 10000000, Loa: 'pdf', refNo: '115', PrincipalContractor: null, StateId: 2, Status: 1 , EmployerOffice: 1, PrincipleEmployer: 1, PrincipleEmployerOffice: 1 , UoM_Currency: 22, ProgressType: 2  }]
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("createt_oms_projects"));
                    t_oms_projects t = new t_oms_projects();
                    var clStates = await Task.Run(() => DAL.clStateCountryAsync());
                    DataTable dtClass = t.dtClass("dtClass");
                    int i = 1; //CONSIDER CHANGING THE AUTOINCREMENTING TABLE ID FIELD TO THIS VARIABLE
                    foreach(t_oms_projects E in clProjects) {
                        int j = i++;
                        DataRow dr = dtClass.NewRow();
                        dr["ProjectID"] = j;
                        dr["ProjectName"] = E.ProjectName;
                        dr["ProjectDesc"] = E.ProjectDesc;
                        dr["Employer"] = E.Employer;
                        dr["ProjectLocation"] = E.ProjectLocation;
                        dr["ProjectAddress"] = E.ProjectAddress;
                        dr["ProjectDuration"] = E.ProjectDuration;
                        dr["ProjectSchStartDate"] = E.ProjectSchStartDate;
                        dr["ProjectSchFinishDate"] = E.ProjectSchStartDate.AddDays(E.ProjectDuration * 30);
                        dr["ProjectStartDate"] = E.ProjectSchStartDate;
                        dr["ProjectEndDate"] = E.ProjectSchStartDate.AddDays(E.ProjectDuration * 30);
                        dr["CreatedOn"] = DateTime.Now;
                        dr["CreatedBy"] = userID;
                        dr["UpdatedOn"] = DBNull.Value;
                        dr["UpdatedBy"] = DBNull.Value;
                        dr["IsActive"] = 1;
                        dr["Tenderid"] = DBNull.Value;
                        dr["ProjectType"] = E.ProjectType;
                        dr["Factorid"] = j;//use for temp session 
                        dr["value"] = E.value;
                        dr["PROGROUP"] = DBNull.Value;
                        dr["MAINTENANCECOST"] = DBNull.Value;
                        dr["Loa"] = E.Loa;
                        dr["refNo"] = E.refNo;
                        dr["PrincipalContractor"] = E.PrincipalContractor;
                        dr["StateId"] = E.StateId;
                        dr["CountryId"] = (from cl in clStates where cl.StateID == E.StateId select cl.CountryID).FirstOrDefault();
                        dr["RevLedgerId"] = DBNull.Value;
                        dr["Status"] = E.Status;
                        dr["EmployerOffice"] = E.EmployerOffice;
                        dr["FileExt"] = DBNull.Value;
                        dr["PrincipleEmployer"] = E.PrincipleEmployer;
                        dr["PrincipleEmployerOffice"] = E.PrincipleEmployerOffice;
                        dr["RecordableOffice"] = DBNull.Value;
                        dr["PrincipleRecordableOffice"] = DBNull.Value;
                        dr["UoM_Currency"] = E.UoM_Currency;
                        dr["ProgressType"] = E.ProgressType;
                        dr["ExecutedValue"] = DBNull.Value;
                        dr["Remarks"] = DBNull.Value;
                        dr["SpecialStatusID"] = DBNull.Value;
                        dr["SpecialRemarks"] = DBNull.Value;
                        dr["ContractCallValue"] = DBNull.Value;
                        dr["BudgetedValue"] = DBNull.Value;
                        dr["ActualExpenditure"] = DBNull.Value;
                        dr["ActualBilledValue"] = DBNull.Value;
                        dr["ExpLedgerID"] = DBNull.Value;
                        dr["CompanyId"] = cid;
                        dtClass.Rows.Add(dr);
                    }
                    string res = string.Empty;
                    int flush = await Task.Run(() => flushFactorIds(con, tran));
                    if (flush == 0)  { 
                        tran.Rollback();
                        con.Close();
                        return "failed"; 
                    } 
                    using(SqlBulkCopy bcp = new SqlBulkCopy(con, SqlBulkCopyOptions.Default, tran)) {
                        bcp.DestinationTableName = "[dbo].[t_oms_projects]";
                        try {
                            await bcp.WriteToServerAsync(dtClass);
                            List<threeIDs> clPIDs = await Task.Run(() => getInsertedPIDs(con, tran));
                            DataTable dtThree = common.threeIDsList2Table(clPIDs);
                            var joinResult = from t1 in dtClass.AsEnumerable() join t2 in dtThree.AsEnumerable()
                                on Convert.ToInt32(t1.Field<int>("ProjectID")) equals Convert.ToInt32(t2.Field<int>("id2"))
                                             select new { ProjectID = Convert.ToInt32(t2["id1"]), UoM_Currency = Convert.ToInt32(t2["id3"]) };

                            twoIDs two = new twoIDs();
                            DataTable dtTwo = two.dt2IDs("two");
                            foreach(var e in joinResult) {
                                DataRow dr = dtTwo.NewRow();
                                dr["id1"] = e.ProjectID;
                                dr["id2"] = e.UoM_Currency;
                                dtTwo.Rows.Add(dr);
                            }
                            string query = "cms.projectCreationLinkTables";
                            int recordsUpdated = 0;
                            using(SqlCommand cmd = new SqlCommand(query, con, tran)) {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add(new SqlParameter("@ws", ws));
                                cmd.Parameters.Add(new SqlParameter("@pid_curs", dtTwo));
                                cmd.Parameters.Add(new SqlParameter("@userID", userID));
                                cmd.Parameters.Add(new SqlParameter("@result", SqlDbType.VarChar, 10)).Direction = ParameterDirection.Output;
                                recordsUpdated = await cmd.ExecuteNonQueryAsync();
                                res = cmd.Parameters["@result"].Value.ToString();
                            }
                            if(res == "success") {
                                tran.Commit();
                                con.Close();
                                return clProjects.Count.ToString() + " record(s) inserted!";
                            } else {
                                tran.Rollback();
                                con.Close();
                                return "failed";
                            }

                        } catch(Exception ex) {
                            tran.Rollback();
                            con.Close();
                            return "failed " + ex.Message;
                        }
                    }
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
        private static async Task<List<threeIDs>> getInsertedPIDs(SqlConnection conn, SqlTransaction tran) {
            
            SqlCommand cmd = new SqlCommand("select ProjectID, Factorid, UoM_Currency  from t_oms_projects where Factorid > 0", conn, tran);
            List<threeIDs> clIDs = new List<threeIDs>();
            using(SqlDataReader reader = await cmd.ExecuteReaderAsync()) {
                while(await reader.ReadAsync()) {
                    threeIDs three = new threeIDs();
                    three.id1 = (int)reader[0];
                    three.id2 = (int)reader[1];
                    three.id3 = (int)reader[2];
                    clIDs.Add(three);
                }
            }
            using(SqlCommand cmd2 = new SqlCommand("update  t_oms_projects set Factorid = null where Factorid > 0", conn, tran)) {
                await cmd.ExecuteNonQueryAsync();
            }
            return clIDs;
        }
         private static async Task<int> flushFactorIds(SqlConnection conn, SqlTransaction tran) {
            try {
                string query = "update  t_oms_projects set Factorid = null where Factorid is not null";
                using(SqlCommand cmd = new SqlCommand(query, conn, tran)) {
                    await cmd.ExecuteNonQueryAsync();
                }
                return 1;
            } catch  { return 0; }
        }
        public static async Task<string> UpdateProjectsAsync(List<t_oms_projects> clProjects, int cid, int userID) {
            try {
                // [{ProjectID: 79, ProjectName: 'Angular33 Project', ProjectDesc: 'Angular33 Project', Employer: 1, ProjectLocation: 'Hyd', ProjectAddress: 'Hyd', ProjectDuration:8
                //, ProjectSchStartDate: '2019-03-31',ProjectType: 2,  value: 30000000, Loa: 'pdf', refNo: '117', PrincipalContractor: null, StateId: 2, Status: 1 , EmployerOffice: 1, PrincipleEmployer: 1, PrincipleEmployerOffice: 1 , UoM_Currency: 22, ProgressType: 2  }]
                DataTable dtTrunc = new t_oms_projects().dtTrunc("dtTrunc");
                foreach(t_oms_projects E in clProjects) {
                    DataRow dr = dtTrunc.NewRow();
                    dr["ProjectID"] = E.ProjectID;
                    dr["ProjectName"] = E.ProjectName;
                    dr["ProjectDesc"] = E.ProjectDesc;
                    dr["Employer"] = E.Employer;
                    dr["ProjectLocation"] = E.ProjectLocation;
                    dr["ProjectAddress"] = E.ProjectAddress;
                    dr["ProjectDuration"] = E.ProjectDuration;
                    dr["ProjectSchStartDate"] = E.ProjectSchStartDate;
                    dr["ProjectType"] = E.ProjectType;
                    dr["value"] = E.value;
                    dr["Loa"] = E.Loa;
                    dr["refNo"] = E.refNo;
                    dr["PrincipalContractor"] = E.PrincipalContractor;
                    dr["StateId"] = E.StateId;
                    dr["Status"] = E.Status;
                    dr["EmployerOffice"] = E.EmployerOffice;
                    dr["PrincipleEmployer"] = E.PrincipleEmployer;
                    dr["PrincipleEmployerOffice"] = E.PrincipleEmployerOffice;
                    dr["UoM_Currency"] = E.UoM_Currency;
                    dr["ProgressType"] = E.ProgressType;
                    dr["CompanyId"] = cid;
                    dtTrunc.Rows.Add(dr);
                }
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("blkUpdate"));
                    string query = "cms.projectsUpdate";
                    int recordsUpdated = 0;
                    using(SqlCommand cmd = new SqlCommand(query, con, tran)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@tblType", dtTrunc));
                        cmd.Parameters.Add(new SqlParameter("@userID", userID));
                        recordsUpdated = await cmd.ExecuteNonQueryAsync();
                    }
                    if(recordsUpdated > 0) tran.Commit();
                    else return "failed to update!";
                    con.Close();
                    return recordsUpdated.ToString() + " records(s) updated";
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }
        public static async Task<string> DeleteProjectsAsync(List<int> ids, int userID) {
            try {
                DataTable dt = common.idList2Table(ids);
                using(SqlConnection con = new SqlConnection(common.erpCS)) {
                    await con.OpenAsync();
                    SqlTransaction tran = await Task.Run<SqlTransaction>(() => con.BeginTransaction("blkDelete"));
                    string query = "cms.projectsDelete";
                    int recordsUpdated = 0;
                    using(SqlCommand cmd = new SqlCommand(query, con, tran)) {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@tblType", dt));
                        cmd.Parameters.Add(new SqlParameter("@userID", userID));
                        recordsUpdated = await cmd.ExecuteNonQueryAsync();
                    }
                    if(recordsUpdated > 0) tran.Commit();
                    else return "failed to update!";
                    con.Close();
                    return recordsUpdated.ToString() + " records(s) deleted";
                }
            } catch(Exception ex) { return "failed " + ex.Message; }
        }

    }
}