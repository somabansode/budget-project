﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Login.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUsers : IdentityUser
    {
        public int CompanyID { get; set; }
        public int TypeID { get; set; }
        public string  Purpose { get; set; }
        
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUsers> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            userIdentity.AddClaim(new Claim("CompanyID",this.CompanyID.ToString()));
            userIdentity.AddClaim(new Claim("TypeID", this.TypeID.ToString()));
            userIdentity.AddClaim(new Claim("Purpose", this.Purpose.ToString()));

            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationsDbContext : IdentityDbContext<ApplicationUsers>
    {
        public ApplicationsDbContext()
            : base("ERPConnectionString", throwIfV1Schema: false)
        {
        }

        public static ApplicationsDbContext Create()
        {
            return new ApplicationsDbContext();
        }
    }
}