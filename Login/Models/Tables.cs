﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Reflection;
using System.Text;


namespace Login.Models {
    // alternate method from SQL itself >> select dbo.CreateClass('t_oms_tasks')
    public class GeneralMethods {
        public static void CreateDataTableFromGivenClass(Type typeofClass) {
            //CALLING METTHOD IN STARTUP FORM >>> CreateDataTable(typeof(class));
            string dQ = "\"";
            StringBuilder sb = new StringBuilder();
            string str = "";
            sb.Append("DataTable dtName = new DataTable(dtStringName);\r\r");
            foreach(PropertyInfo info in typeofClass.GetProperties()) {
                str = "";
                str = "DataColumn " + info.Name + " = new DataColumn(); \r";
                str = str + info.Name + ".DataType = System.Type.GetType(" + dQ + info.PropertyType.FullName + dQ + ");\r";
                str = str + info.Name + ".ColumnName = " + dQ + info.Name + dQ + ";\r";
                if(info.Name.ToLower() == "id") {
                    str = str + info.Name + ".AutoIncrement = true;\r";
                }
                str = str + "dtName.Columns.Add(" + info.Name + ");\r\r";
                sb = sb.Append(str);
            }
            str = "\nDataColumn[] keys = new DataColumn[1];\r";
            str = str + "keys[0] = ID;\r";
            str = str + "dtName.PrimaryKey = keys;\r";
            str = str + "return dtName;\r";
            sb = sb.Append(str);
            using(StreamWriter writer = new StreamWriter("D:\\datatable.txt", true)) {
                writer.WriteLine(sb.ToString());
            }
        }
        public static DataTable ConvertList2DataTable<T>(List<T> items) {
            DataTable dataTable = new DataTable(typeof(T).Name);
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);//Get all the properties
            foreach(PropertyInfo prop in Props) { dataTable.Columns.Add(prop.Name); }//Setting column names as Property names
            foreach(T item in items) {
                var values = new object[Props.Length];
                for(int i = 0; i < Props.Length; i++) { values[i] = Props[i].GetValue(item, null); }//inserting property values to datatable rows
                dataTable.Rows.Add(values);
            }
            return dataTable;//put a breakpoint here and check datatable
        }
        /// <summary>
        /// coverting a DataTable to List<class> VERY USEFUL TOOL
        /// </summary>
        public static List<T> ConvertDataTable2LIST<T>(DataTable dt) {
            List<T> data = new List<T>();
            foreach(DataRow row in dt.Rows) {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        /// <summary>
        ///  USED in ConvertDataTable method extensively
        /// </summary>
        public static T GetItem<T>(DataRow dr) {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach(DataColumn column in dr.Table.Columns) {
                foreach(PropertyInfo pro in temp.GetProperties()) {
                    if(pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
    }
    public class T_OMS_Tasks {
        public T_OMS_Tasks() {
        }
        public int TaskID { get; set; }
        public int ProjectID { get; set; }
        public string TaskRefNo { get; set; }
        public string TaskName { get; set; }
        public string TaskDesc { get; set; }
        public int Unit { get; set; }
        public int SectionID { get; set; }
        public double Quantity { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Method { get; set; }
        public double RaQty { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public int IsActive { get; set; }
        public double TotalCost { get; set; }
        public double TotalLength { get; set; }
        public double AdFixed { get; set; }
        public double Adruning { get; set; }
        public double OP { get; set; }
        public double TaskCost { get; set; }
        public int TaskMasterID { get; set; }
        public double AgreementRate { get; set; }
        public double TenderRate { get; set; }
        public int OrderID { get; set; }
        public int? PCID { get; set; }
        public string Predessor { get; set; }
        public int Duration { get; set; }
        public int BaseID { get; set; }
        public bool IsCaptive { get; set; }
        public int SlNo { get; set; }
        public int LinkTo { get; set; }
        public string TSuc { get; set; }
        public double Density { get; set; }
        public double ProdQty { get; set; }
        public int? LagHrs { get; set; }
        public double? CTCRate { get; set; }
        public string LagPred { get; set; }
        public string SecDesc { get; set; }
        public string SecLvl1 { get; set; }
        public string SecLvl2 { get; set; }
        public string SecLvl3 { get; set; }
        public string SecLvl4 { get; set; }
        public string SecLvl5 { get; set; }
        public string XlRef { get; set; }
        public double? NoofHours { get; set; }
        public int? NoofDays { get; set; }
        public int Cid { get; set; }
        public DataTable dataTable(string dtStringName) {
            DataTable dtName = new DataTable(dtStringName);

            DataColumn TaskID = new DataColumn();
            TaskID.DataType = System.Type.GetType("System.Int32");
            TaskID.ColumnName = "TaskID";
            dtName.Columns.Add(TaskID);

            DataColumn ProjectID = new DataColumn();
            ProjectID.DataType = System.Type.GetType("System.Int32");
            ProjectID.ColumnName = "ProjectID";
            dtName.Columns.Add(ProjectID);

            DataColumn TaskRefNo = new DataColumn();
            TaskRefNo.DataType = System.Type.GetType("System.String");
            TaskRefNo.ColumnName = "TaskRefNo";
            dtName.Columns.Add(TaskRefNo);

            DataColumn TaskName = new DataColumn();
            TaskName.DataType = System.Type.GetType("System.String");
            TaskName.ColumnName = "TaskName";
            dtName.Columns.Add(TaskName);

            DataColumn TaskDesc = new DataColumn();
            TaskDesc.DataType = System.Type.GetType("System.String");
            TaskDesc.ColumnName = "TaskDesc";
            dtName.Columns.Add(TaskDesc);

            DataColumn Unit = new DataColumn();
            Unit.DataType = System.Type.GetType("System.Int32");
            Unit.ColumnName = "Unit";
            dtName.Columns.Add(Unit);

            DataColumn SectionID = new DataColumn();
            SectionID.DataType = System.Type.GetType("System.Int32");
            SectionID.ColumnName = "SectionID";
            dtName.Columns.Add(SectionID);

            DataColumn Quantity = new DataColumn();
            Quantity.DataType = System.Type.GetType("System.Double");
            Quantity.ColumnName = "Quantity";
            dtName.Columns.Add(Quantity);

            DataColumn StartDate = new DataColumn();
            StartDate.DataType = System.Type.GetType("System.DateTime");
            StartDate.ColumnName = "StartDate";
            dtName.Columns.Add(StartDate);

            DataColumn EndDate = new DataColumn();
            EndDate.DataType = System.Type.GetType("System.DateTime");
            EndDate.ColumnName = "EndDate";
            dtName.Columns.Add(EndDate);

            DataColumn Method = new DataColumn();
            Method.DataType = System.Type.GetType("System.String");
            Method.ColumnName = "Method";
            dtName.Columns.Add(Method);

            DataColumn RaQty = new DataColumn();
            RaQty.DataType = System.Type.GetType("System.Double");
            RaQty.ColumnName = "RaQty";
            dtName.Columns.Add(RaQty);

            DataColumn CreatedOn = new DataColumn();
            CreatedOn.DataType = System.Type.GetType("System.DateTime");
            CreatedOn.ColumnName = "CreatedOn";
            dtName.Columns.Add(CreatedOn);

            DataColumn CreatedBy = new DataColumn();
            CreatedBy.DataType = System.Type.GetType("System.Int32");
            CreatedBy.ColumnName = "CreatedBy";
            dtName.Columns.Add(CreatedBy);

            DataColumn UpdatedOn = new DataColumn();
            UpdatedOn.DataType = System.Type.GetType("System.DateTime");
            UpdatedOn.ColumnName = "UpdatedOn";
            dtName.Columns.Add(UpdatedOn);

            DataColumn UpdatedBy = new DataColumn();
            UpdatedBy.DataType = System.Type.GetType("System.Int32");
            UpdatedBy.ColumnName = "UpdatedBy";
            dtName.Columns.Add(UpdatedBy);

            DataColumn IsActive = new DataColumn();
            IsActive.DataType = System.Type.GetType("System.Int32");
            IsActive.ColumnName = "IsActive";
            dtName.Columns.Add(IsActive);

            DataColumn TotalCost = new DataColumn();
            TotalCost.DataType = System.Type.GetType("System.Double");
            TotalCost.ColumnName = "TotalCost";
            dtName.Columns.Add(TotalCost);

            DataColumn TotalLength = new DataColumn();
            TotalLength.DataType = System.Type.GetType("System.Double");
            TotalLength.ColumnName = "TotalLength";
            dtName.Columns.Add(TotalLength);

            DataColumn AdFixed = new DataColumn();
            AdFixed.DataType = System.Type.GetType("System.Double");
            AdFixed.ColumnName = "AdFixed";
            dtName.Columns.Add(AdFixed);

            DataColumn Adruning = new DataColumn();
            Adruning.DataType = System.Type.GetType("System.Double");
            Adruning.ColumnName = "Adruning";
            dtName.Columns.Add(Adruning);

            DataColumn OP = new DataColumn();
            OP.DataType = System.Type.GetType("System.Double");
            OP.ColumnName = "OP";
            dtName.Columns.Add(OP);

            DataColumn TaskCost = new DataColumn();
            TaskCost.DataType = System.Type.GetType("System.Double");
            TaskCost.ColumnName = "TaskCost";
            dtName.Columns.Add(TaskCost);

            DataColumn TaskMasterID = new DataColumn();
            TaskMasterID.DataType = System.Type.GetType("System.Int32");
            TaskMasterID.ColumnName = "TaskMasterID";
            dtName.Columns.Add(TaskMasterID);

            DataColumn AgreementRate = new DataColumn();
            AgreementRate.DataType = System.Type.GetType("System.Double");
            AgreementRate.ColumnName = "AgreementRate";
            dtName.Columns.Add(AgreementRate);

            DataColumn TenderRate = new DataColumn();
            TenderRate.DataType = System.Type.GetType("System.Double");
            TenderRate.ColumnName = "TenderRate";
            dtName.Columns.Add(TenderRate);

            DataColumn OrderID = new DataColumn();
            OrderID.DataType = System.Type.GetType("System.Int32");
            OrderID.ColumnName = "OrderID";
            dtName.Columns.Add(OrderID);

            DataColumn PCID = new DataColumn();
            PCID.DataType = System.Type.GetType("System.Int32");
            PCID.ColumnName = "PCID";
            dtName.Columns.Add(PCID);

            DataColumn Predessor = new DataColumn();
            Predessor.DataType = System.Type.GetType("System.String");
            Predessor.ColumnName = "Predessor";
            dtName.Columns.Add(Predessor);

            DataColumn Duration = new DataColumn();
            Duration.DataType = System.Type.GetType("System.Int32");
            Duration.ColumnName = "Duration";
            dtName.Columns.Add(Duration);

            DataColumn BaseID = new DataColumn();
            BaseID.DataType = System.Type.GetType("System.Int32");
            BaseID.ColumnName = "BaseID";
            dtName.Columns.Add(BaseID);

            DataColumn IsCaptive = new DataColumn();
            IsCaptive.DataType = System.Type.GetType("System.Boolean");
            IsCaptive.ColumnName = "IsCaptive";
            dtName.Columns.Add(IsCaptive);

            DataColumn SlNo = new DataColumn();
            SlNo.DataType = System.Type.GetType("System.Int32");
            SlNo.ColumnName = "SlNo";
            dtName.Columns.Add(SlNo);

            DataColumn LinkTo = new DataColumn();
            LinkTo.DataType = System.Type.GetType("System.Int32");
            LinkTo.ColumnName = "LinkTo";
            dtName.Columns.Add(LinkTo);

            DataColumn TSuc = new DataColumn();
            TSuc.DataType = System.Type.GetType("System.String");
            TSuc.ColumnName = "TSuc";
            dtName.Columns.Add(TSuc);

            DataColumn Density = new DataColumn();
            Density.DataType = System.Type.GetType("System.Double");
            Density.ColumnName = "Density";
            dtName.Columns.Add(Density);

            DataColumn ProdQty = new DataColumn();
            ProdQty.DataType = System.Type.GetType("System.Double");
            ProdQty.ColumnName = "ProdQty";
            dtName.Columns.Add(ProdQty);

            DataColumn LagHrs = new DataColumn();
            LagHrs.DataType = System.Type.GetType("System.Int32");
            LagHrs.ColumnName = "LagHrs";
            dtName.Columns.Add(LagHrs);

            DataColumn CTCRate = new DataColumn();
            CTCRate.DataType = System.Type.GetType("System.Double");
            CTCRate.ColumnName = "CTCRate";
            dtName.Columns.Add(CTCRate);

            DataColumn LagPred = new DataColumn();
            LagPred.DataType = System.Type.GetType("System.String");
            LagPred.ColumnName = "LagPred";
            dtName.Columns.Add(LagPred);

            DataColumn SecDesc = new DataColumn();
            SecDesc.DataType = System.Type.GetType("System.String");
            SecDesc.ColumnName = "SecDesc";
            dtName.Columns.Add(SecDesc);

            DataColumn SecLvl1 = new DataColumn();
            SecLvl1.DataType = System.Type.GetType("System.String");
            SecLvl1.ColumnName = "SecLvl1";
            dtName.Columns.Add(SecLvl1);

            DataColumn SecLvl2 = new DataColumn();
            SecLvl2.DataType = System.Type.GetType("System.String");
            SecLvl2.ColumnName = "SecLvl2";
            dtName.Columns.Add(SecLvl2);

            DataColumn SecLvl3 = new DataColumn();
            SecLvl3.DataType = System.Type.GetType("System.String");
            SecLvl3.ColumnName = "SecLvl3";
            dtName.Columns.Add(SecLvl3);

            DataColumn SecLvl4 = new DataColumn();
            SecLvl4.DataType = System.Type.GetType("System.String");
            SecLvl4.ColumnName = "SecLvl4";
            dtName.Columns.Add(SecLvl4);

            DataColumn SecLvl5 = new DataColumn();
            SecLvl5.DataType = System.Type.GetType("System.String");
            SecLvl5.ColumnName = "SecLvl5";
            dtName.Columns.Add(SecLvl5);

            DataColumn XlRef = new DataColumn();
            XlRef.DataType = System.Type.GetType("System.String");
            XlRef.ColumnName = "XlRef";
            dtName.Columns.Add(XlRef);

            DataColumn NoofHours = new DataColumn();
            NoofHours.DataType = System.Type.GetType("System.Double");
            NoofHours.ColumnName = "NoofHours";
            dtName.Columns.Add(NoofHours);

            DataColumn NoofDays = new DataColumn();
            NoofDays.DataType = System.Type.GetType("System.Int32");
            NoofDays.ColumnName = "NoofDays";
            dtName.Columns.Add(NoofDays);

            DataColumn Cid = new DataColumn();
            Cid.DataType = System.Type.GetType("System.Int32");
            Cid.ColumnName = "Cid";
            dtName.Columns.Add(Cid);

            DataColumn[] keys = new DataColumn[1];
            keys[0] = TaskID;
            dtName.PrimaryKey = keys;
            return dtName;        }
    }
    public class Assignment {
        public int AssignMent { get; set; }
        public int TaskID { get; set; }
        public int ResourceID { get; set; }
        public double Coeff { get; set; }
        public string Remarks { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public int IsActive { get; set; }
        public string ResourceName { get; set; }
        public int CategoryId { get; set; }
        public string Unit { get; set; }
        public int LeadResID { get; set; }
        public int ResNos { get; set; }
        public double Wastage { get; set; }

    }
    public class Resource {
        //GS gsName  gpID ChildProcurementGroup   LedgerID cfgID   clsID Classing    rnkID verID   VerifyType CategoryGroup   Au_Name
        //2	Service	24	Hiring Heavy Equipment	1440	5	4	Servicing	5	1	Instant Machinery and Assets    Hour
        public int GS { get; set; }
        public string gsName { get; set; }
        public int gpID { get; set; }
        public string ChildProcurementGroup { get; set; }
        public int? LedgerID { get; set; }
        public int cfgID { get; set; }
        public int clsID { get; set; }
        public string Classing { get; set; }
        public int rnkID { get; set; }
        public int verID { get; set; }
        public string VerifyType { get; set; }
        public string CategoryGroup { get; set; }
        public string Au_Name { get; set; }
        public int duID { get; set; }
        public decimal PropValue { get; set; }
        // t_oms_resource table fields below
        public int ResourceID { get; set; }
        public string ResourceName { get; set; }
        public string ResourceMake { get; set; }
        public int ResourceUnit { get; set; }
        public int CategoryID { get; set; }
        public double ResourceRate { get; set; }
        public double ResourceBasicRate { get; set; }
        public double ResourceLead { get; set; }
        public double ResourceLeadRate { get; set; }
        public double ResourceLPH { get; set; }
        public string ResourcerRemarks { get; set; }
        public string ResourceOutPut { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public int IsActive { get; set; }
        public string Extension { get; set; }
        public int? GoodsCategory { get; set; }
        public int? DeliveryDays { get; set; }

        public Resource() { }
        public Resource(int _GS, string _gsName, int _gpID, string _ChildProcurementGroup, int? _LedgerID, int _cfgID, int _clsID, string _Classing,
        int _rnkID, int _verID, string _VerifyType, string _CategoryGroup, string _Au_Name, int _duID, decimal _PropValue, int _ResourceID, string _ResourceName, string _ResourceMake,
        int _ResourceUnit, int _CategoryID, double _ResourceRate, double _ResourceBasicRate, double _ResourceLead, double _ResourceLeadRate, double _ResourceLPH,
        string _ResourcerRemarks, string _ResourceOutPut, DateTime _CreatedOn, int _CreatedBy, DateTime _UpdatedOn,
        int _UpdatedBy, int _IsActive, string _Extension, int? _GoodsCategory, int? _DeliveryDays
        ) {
            GS = _GS; gsName = _gsName; gpID = _gpID; ChildProcurementGroup = _ChildProcurementGroup; LedgerID = _LedgerID; cfgID = _cfgID; clsID = _clsID;
            Classing = _Classing; rnkID = _rnkID; verID = _verID; VerifyType = _VerifyType; CategoryGroup = _CategoryGroup; Au_Name = _Au_Name; duID = _duID; PropValue = _PropValue;
            ResourceID = _ResourceID; ResourceName = _ResourceName; ResourceMake = _ResourceMake; ResourceUnit = _ResourceUnit;
            CategoryID = _CategoryID; ResourceRate = _ResourceRate; ResourceBasicRate = _ResourceBasicRate; ResourceLead = _ResourceLead;
            ResourceLeadRate = _ResourceLeadRate; ResourceLPH = _ResourceLPH; ResourcerRemarks = _ResourcerRemarks; ResourceOutPut = _ResourceOutPut;
            CreatedBy = _CreatedBy; UpdatedBy = _UpdatedBy; IsActive = _IsActive;
            Extension = _Extension; GoodsCategory = _GoodsCategory; DeliveryDays = _DeliveryDays;
        }

    }
    public class t_oms_resources {
        public int ResourceID { get; set; }
        public string ResourceName { get; set; }
        public string ResourceMake { get; set; }
        public int ResourceUnit { get; set; }
        public int CategoryID { get; set; }
        public double ResourceRate { get; set; }
        public double ResourceBasicRate { get; set; }
        public double? ResourceLead { get; set; }
        public double? ResourceLeadRate { get; set; }
        public double? ResourceLPH { get; set; }
        public string ResourcerRemarks { get; set; }
        public string ResourceOutPut { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public int IsActive { get; set; }
        public string Extension { get; set; }
        public int? GoodsCategory { get; set; }
        public int? DeliveryDays { get; set; }

        public t_oms_resources() { }
        public t_oms_resources(int _ResourceID, string _ResourceName, string _ResourceMake, int _ResourceUnit, int _CategoryID,
        double _ResourceRate, double _ResourceBasicRate, double? _ResourceLead, double? _ResourceLeadRate, double? _ResourceLPH,
        string _ResourcerRemarks, string _ResourceOutPut, DateTime _CreatedOn, int _CreatedBy, DateTime _UpdatedOn,
        int _UpdatedBy, int _IsActive, string _Extension, int? _GoodsCategory, int? _DeliveryDays
        ) {
            ResourceID = _ResourceID; ResourceName = _ResourceName; ResourceMake = _ResourceMake; ResourceUnit = _ResourceUnit;
            CategoryID = _CategoryID; ResourceRate = _ResourceRate; ResourceBasicRate = _ResourceBasicRate; ResourceLead = _ResourceLead;
            ResourceLeadRate = _ResourceLeadRate; ResourceLPH = _ResourceLPH; ResourcerRemarks = _ResourcerRemarks; ResourceOutPut = _ResourceOutPut;
            CreatedOn = _CreatedOn; CreatedBy = _CreatedBy; UpdatedOn = _UpdatedOn; UpdatedBy = _UpdatedBy; IsActive = _IsActive;
            Extension = _Extension; GoodsCategory = _GoodsCategory; DeliveryDays = _DeliveryDays;
        }
        public DataTable dataTable(string tblName) {
            DataTable dtName = new DataTable(tblName);
            DataColumn ResourceID = new DataColumn();
            ResourceID.DataType = System.Type.GetType("System.Int32");
            ResourceID.ColumnName = "ResourceID";
            dtName.Columns.Add(ResourceID);

            dtName.Columns.Add("ResourceName", typeof(System.String));
            dtName.Columns.Add("ResourceMake", typeof(System.String));
            dtName.Columns.Add("ResourceUnit", typeof(System.Int32));
            dtName.Columns.Add("CategoryID", typeof(System.Int32));
            dtName.Columns.Add("ResourceRate", typeof(System.Decimal));
            dtName.Columns.Add("ResourceBasicRate", typeof(System.Decimal));
            dtName.Columns.Add("ResourceLead", typeof(System.Decimal));
            dtName.Columns.Add("ResourceLeadRate", typeof(System.Decimal));
            dtName.Columns.Add("ResourceLPH", typeof(System.Decimal));
            dtName.Columns.Add("ResourcerRemarks", typeof(System.String));
            dtName.Columns.Add("ResourceOutPut", typeof(System.String));
            dtName.Columns.Add("CreatedOn", typeof(System.DateTime));
            dtName.Columns.Add("CreatedBy", typeof(System.Int32));
            dtName.Columns.Add("UpdatedOn", typeof(System.DateTime));
            dtName.Columns.Add("UpdatedBy", typeof(System.Int32));
            dtName.Columns.Add("IsActive", typeof(System.Int32));
            dtName.Columns.Add("Extension", typeof(System.String));
            dtName.Columns.Add("GoodsCategory", typeof(System.Int32));
            dtName.Columns.Add("DeliveryDays", typeof(System.Int32));
            dtName.Columns.Add("ssid", typeof(System.Int32));

            DataColumn[] keys = new DataColumn[1];
            keys[0] = ResourceID;
            dtName.PrimaryKey = keys;
            return dtName;
        }
    }
    public class T_PM_Categories {
        public int Category_Id { get; set; }
        public string Category_Name { get; set; }
        public int Cat_Type { get; set; }
        public int AssetTypeId { get; set; }
        public int ModuleId { get; set; }
        public int? LedgerID { get; set; }
        public bool IsActive { get; set; }
        public int cid { get; set; }//companyid

        public T_PM_Categories() { }
        public T_PM_Categories(int _Category_Id, string _Category_Name, int _Cat_Type, int _AssetTypeId, int _ModuleId, int? _LedgerID, bool _IsActive, int _cid) {
            Category_Id = _Category_Id; Category_Name = _Category_Name; Cat_Type = _Cat_Type;
            AssetTypeId = _AssetTypeId; ModuleId = _ModuleId; LedgerID = _LedgerID; IsActive = _IsActive; cid = _cid;
        }
        public DataTable dtTable(string tblName) {
            DataTable dtName = new DataTable(tblName);
            DataColumn Category_Id = new DataColumn();
            Category_Id.DataType = System.Type.GetType("System.Int32");
            Category_Id.ColumnName = "Category_Id";
            dtName.Columns.Add(Category_Id);

            dtName.Columns.Add("Category_Name", typeof(System.String));
            dtName.Columns.Add("Cat_Type", typeof(System.Int32));
            dtName.Columns.Add("AssetTypeId", typeof(System.Int32));
            dtName.Columns.Add("ModuleId", typeof(System.Int32));
            DataColumn dc = dtName.Columns.Add("LedgerID", typeof(System.Int32)); dc.AllowDBNull = true;
            dtName.Columns.Add("IsActive", typeof(System.Boolean));
            dtName.Columns.Add("cid", typeof(System.Int32));
            DataColumn[] keys = new DataColumn[1];
            keys[0] = Category_Id;
            dtName.PrimaryKey = keys;
            return dtName;
        }
    }
    public class T_PM_ResVsGroup {
        public int ResourceId { get; set; }
        public int Category_Id { get; set; }
        public int Cat_Type { get; set; }
        public int? LedgerId { get; set; }
        public int cid { get; set; }//companyid

        public T_PM_ResVsGroup() { }
        public T_PM_ResVsGroup(int _ResourceId, int _Category_Id, int _Cat_Type, int? _LedgerId, int _cid) {
            ResourceId = _ResourceId; Category_Id = _Category_Id; Category_Id = _Category_Id; LedgerId = _LedgerId; cid = _cid;
        }
        public DataTable dtResVsGroup(string tblName) {
            DataTable dtName = new DataTable(tblName);
            DataColumn ResourceId = new DataColumn();
            ResourceId.DataType = System.Type.GetType("System.Int32");
            ResourceId.ColumnName = "ResourceId";
            dtName.Columns.Add(ResourceId);

            dtName.Columns.Add("Category_Id", typeof(System.Int32));
            dtName.Columns.Add("Cat_Type", typeof(System.Int32));
            DataColumn dc = dtName.Columns.Add("LedgerId", typeof(System.Int32)); dc.AllowDBNull = true;
            dtName.Columns.Add("cid", typeof(System.Int32));

            //DataColumn[] keys = new DataColumn[1];
            //keys[0] = ResourceId;
            //dtName.PrimaryKey = keys;
            return dtName;
        }

    }
    public class T_MMS_MaterialConfig {
        public int MCId { get; set; }
        public int ResourceID { get; set; }
        public int MRId { get; set; }
        public int ClassingId { get; set; }
        public string Code { get; set; }
        public int Period { get; set; }
        public decimal Allowable { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public int IsActive { get; set; }
        public byte StockIn { get; set; }
        public byte StockOut { get; set; }
        public int NoOfRepetition { get; set; }
        public decimal Tolerance { get; set; }
        public int ClosingMode { get; set; }

        public T_MMS_MaterialConfig() { }
        public T_MMS_MaterialConfig(int _MCId, int _ResourceID, int _MRId, int _ClassingId, string _Code, int _Period, decimal _Allowable,
        DateTime _CreatedOn, int _CreatedBy, DateTime _UpdatedOn, int _UpdatedBy, int _IsActive, byte _StockIn, byte _StockOut, int _NoOfRepetition, decimal _Tolerance, int _ClosingMode) {
            MCId = _MCId;
            ResourceID = _ResourceID;
            MRId = _MRId;
            ClassingId = _ClassingId;
            Code = _Code;
            Period = _Period;
            Allowable = _Allowable;
            CreatedOn = _CreatedOn;
            CreatedBy = _CreatedBy;
            UpdatedOn = _UpdatedOn;
            UpdatedBy = _UpdatedBy;
            NoOfRepetition = _NoOfRepetition;
            StockIn = _StockIn;
            StockOut = _StockOut;
            Tolerance = _Tolerance;
            ClosingMode = _ClosingMode;
        }
        public DataTable dtMaterialConfig(string tblName) {
            DataTable dtName = new DataTable(tblName);
            DataColumn MCId = new DataColumn();
            MCId.DataType = System.Type.GetType("System.Int32");
            MCId.ColumnName = "MCId";
            dtName.Columns.Add(MCId);

            dtName.Columns.Add("ResourceID", typeof(System.Int32));
            dtName.Columns.Add("MRId", typeof(System.Int32));
            dtName.Columns.Add("ClassingId", typeof(System.Int32));
            dtName.Columns.Add("Code", typeof(System.String));
            dtName.Columns.Add("Period", typeof(System.Int32));
            dtName.Columns.Add("Allowable", typeof(System.Decimal));
            dtName.Columns.Add("CreatedOn", typeof(System.DateTime));
            dtName.Columns.Add("CreatedBy", typeof(System.Int32));
            dtName.Columns.Add("UpdatedOn", typeof(System.DateTime));
            dtName.Columns.Add("UpdatedBy", typeof(System.Int32));
            dtName.Columns.Add("IsActive", typeof(System.Int32));
            dtName.Columns.Add("StockIn", typeof(System.Byte));
            dtName.Columns.Add("StockOut", typeof(System.Byte));//
            dtName.Columns.Add("NoOfRepetition", typeof(System.Int32));
            dtName.Columns.Add("Tolerance", typeof(System.Decimal));
            dtName.Columns.Add("ClosingMode", typeof(System.Int32));
            DataColumn[] keys = new DataColumn[1];
            keys[0] = MCId;
            dtName.PrimaryKey = keys;
            return dtName;
        }

    }
    public class T_G_ResourceAttributes {
        public int ResourceID { get; set; }
        public int DUTId { get; set; }
        public decimal Value { get; set; }
        public string Description { get; set; }

        public T_G_ResourceAttributes() { }
        public T_G_ResourceAttributes(int _ResourceID, int _DUTId, decimal _Value, string _Description) {
            ResourceID = _ResourceID; DUTId = _DUTId; Value = _Value; Description = _Description;
        }
        public DataTable dtResourceAttributes(string tblName) {
            DataTable dtName = new DataTable(tblName);
            DataColumn ResourceID = new DataColumn();
            ResourceID.DataType = System.Type.GetType("System.Int32");
            ResourceID.ColumnName = "ResourceID";
            dtName.Columns.Add(ResourceID);

            dtName.Columns.Add("DUTId", typeof(System.Int32));
            dtName.Columns.Add("Value", typeof(System.Decimal));
            dtName.Columns.Add("Description", typeof(System.String));

            DataColumn[] keys = new DataColumn[1];
            keys[0] = ResourceID;
            dtName.PrimaryKey = keys;
            return dtName;
        }
    }
    public class vb_4msVsCatVsGpsVsRat {
        public int catID { get; set; }
        public string CatName { get; set; }
        public int GS { get; set; }
        public int RatID { get; set; }
        public int ModuleId { get; set; }
        public string AccPath { get; set; }

        public vb_4msVsCatVsGpsVsRat() { }
        public vb_4msVsCatVsGpsVsRat(int _catID, string _CatName, int _GS, int _RatID, int _ModuleId, string _AccPath) {
            catID = _catID; CatName = _CatName; GS = _GS; RatID = _RatID; ModuleId = _ModuleId; AccPath = _AccPath;
        }
    }
    public class T_G_ResourceAssetType {
        public int AssetTypeId { get; set; }
        public string AssetType { get; set; }
        public int GroupID { get; set; }
        public bool IsGoods { get; set; }
        public bool IsActive { get; set; } 
        public int ModuleID { get; set; }
        public T_G_ResourceAssetType() { }
        public T_G_ResourceAssetType(int _AssetTypeId, string _AssetType, int _GroupID, int _ModuleID, bool _IsGoods, bool _IsActive) {
            AssetTypeId = _AssetTypeId; AssetType = _AssetType; GroupID = _GroupID;  ModuleID = _ModuleID; IsGoods = _IsGoods; IsActive = _IsActive;
        }
    }
    public class T_A_Groups {
        public int GroupID { get; set; }
        public string GroupName { get; set; }
        public decimal ParentId { get; set; }
        public int HeadID { get; set; }
        public decimal Bitsstatus { get; set; }
        public T_A_Groups() { }
        public T_A_Groups(int _GroupID, string _GroupName, decimal _ParentId, int _HeadID, decimal _Bitsstatus) {
            GroupID = _GroupID; GroupName = _GroupName; ParentId = _ParentId; HeadID = _HeadID; Bitsstatus = _Bitsstatus;
        }
    }
    public class T_A_Heads {
        public decimal HeadID { get; set; }
        public string AccountHead { get; set; }
        public T_A_Heads() { }
        public T_A_Heads(decimal _HeadID, string _AccountHead
        ) { HeadID = _HeadID; AccountHead = _AccountHead; }
    }

    public class xxProj_Resource {
        public int ResourceID { get; set; }
        public int PrjID { get; set; }
        public string ResourceName { get; set; }
        public string ResourceMake { get; set; }
        public int ResourceUnit { get; set; }
        public double ResourceRate { get; set; }
        public int CategoryID { get; set; }
        public double ResourceBasicRate { get; set; }
        public double ResourceLead { get; set; }
        public double ResourceLeadRate { get; set; }
        public double ResourceLPH { get; set; }
        public string ResourcerRemarks { get; set; }
        public string ResourceOutPut { get; set; }
        public DateTime CreateOn { get; set; }
        public int CreateBy { get; set; }
        public bool IsActive { get; set; }
        public int Nos { get; set; }
        public int useType { get; set; }
    }
    public class Proj_Resource {
        public int ResourceID { get; set; }
        public int PrjID { get; set; }
        public string ResourceName { get; set; }
        public string ResourceMake { get; set; }
        public int ResourceUnit { get; set; }
        public double ResourceRate { get; set; }
        public int CategoryID { get; set; }
        public double ResourceBasicRate { get; set; }
        public double ResourceLead { get; set; }
        public double ResourceLeadRate { get; set; }
        public double ResourceLPH { get; set; }
        public string ResourcerRemarks { get; set; }
        public string ResourceOutPut { get; set; }
        public DateTime CreateOn { get; set; }
        public int CreateBy { get; set; }
        public bool IsActive { get; set; }
        public int Nos { get; set; }
        public int useType { get; set; }

        public Proj_Resource() { }
        public Proj_Resource(int _ResourceID, int _PrjID, string _ResourceName, string _ResourceMake, int _ResourceUnit, double _ResourceRate,
            int _CategoryID, double _ResourceBasicRate, double _ResourceLead, double _ResourceLeadRate, double _ResourceLPH,
            string _ResourcerRemarks, string _ResourceOutPut, DateTime _CreateOn, int _CreateBy, bool _IsActive, int _Nos, int _useType) {
            ResourceID = _ResourceID;
            PrjID = _PrjID;
            ResourceName = _ResourceName;
            ResourceMake = _ResourceMake;
            ResourceUnit = _ResourceUnit;
            ResourceRate = _ResourceRate;
            CategoryID = _CategoryID;
            ResourceBasicRate = _ResourceBasicRate;
            ResourceLead = _ResourceLead;
            ResourceLeadRate = _ResourceLeadRate;
            ResourceLPH = _ResourceLPH;
            ResourcerRemarks = _ResourcerRemarks;
            ResourceOutPut = _ResourceOutPut;
            CreateOn = _CreateOn;
            CreateBy = _CreateBy;
            IsActive = _IsActive;
            Nos = _Nos;
            useType = _useType;
        }
    }
    public class TasksMaster {
        public int TaskMasterID { get; set; }
        public string Task { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int LedgerID { get; set; }
        public int UnitID { get; set; }
        public double Weight { get; set; }
        public double TargetQty { get; set; }
        public int resID { get; set; }

    }
    public class Project {
        public int ProjectID { get; set; }
        public string ProjectName { get; set; }
        public string ProjectDesc { get; set; }
        public int Employer { get; set; }
        public string ProjectLocation { get; set; }
        public string ProjectAddress { get; set; }
        public int ProjectDuration { get; set; }
        public DateTime ProjectSchStartDate { get; set; }
        public DateTime ProjectSchFinishDate { get; set; }
        public DateTime ProjectStartDate { get; set; }
        public DateTime ProjectEndDate { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public int IsActive { get; set; }
        public int Tenderid { get; set; }
        public int ProjectType { get; set; }
        public int Factorid { get; set; }
        public double value { get; set; }
        public string PROGROUP { get; set; }
        public double MAINTENANCECOST { get; set; }
        public string Loa { get; set; }
        public string refNo { get; set; }
        public int PrincipalContractor { get; set; }
        public int StateId { get; set; }
        public int CountryId { get; set; }
        public int RevLedgerId { get; set; }
        public Int16 Status { get; set; }
        public int EmployerOffice { get; set; }
        public string FileExt { get; set; }
        public int PrincipleEmployer { get; set; }
        public int PrincipleEmployerOffice { get; set; }
        public int RecordableOffice { get; set; }
        public int PrincipleRecordableOffice { get; set; }
        public int UoM_Currency { get; set; }
        public Int16 ProgressType { get; set; }
        public double ExecutedValue { get; set; }
        public string Remarks { get; set; }
        public Int16 SpecialStatusID { get; set; }
        public string SpecialRemarks { get; set; }
        public double ContractCallValue { get; set; }
        public double BudgetedValue { get; set; }
        public double ActualExpenditure { get; set; }
        public double ActualBilledValue { get; set; }
        public int ExpLedgerID { get; set; }
        public int CompanyId { get; set; }

    }

    public class Section {
        public int SectionID { get; set; }
        public string SectionName { get; set; }
        public int SectionParent { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public int IsActive { get; set; }
        public int pid { get; set; }
        public int cid { get; set; }

    }
    public class mrp_bl_tasks {
        public int MRP_TID { get; set; }
        public int BID_TID { get; set; }
        public int MRP_ID_T { get; set; }
        bool IsFull { get; set; }

    }
    public class bl {
        public int ID { get; set; }
        public string baseLine { get; set; }
        public bool ia { get; set; }
        public bl() { }
        public bl(int id, string BaseLine, bool IA) {
            ID = id; baseLine = BaseLine; ia = IA;
        }
    }

    public class POptions {
        public int ProjectID { get; set; }
        public double FuelCost { get; set; }
        public int LagDays { get; set; }
        public int TaskActMinDu { get; set; }
        public int MobilizationDu { get; set; }
        public int AdFixDu { get; set; }
        public string MHead { get; set; }
        public double NoRec { get; set; }
        public int ISGovt { get; set; }
        public bool IsLocked { get; set; }
        public int HrPerDay { get; set; }
        public int GrpPerMonth { get; set; }
        public int BID { get; set; }
        public int CurrencyID { get; set; }
        public double ConvRate { get; set; }
        public double FuelCost_Forex { get; set; }
        public int CaptiveMode { get; set; }

        public POptions() { }
        public POptions(int _ProjectID, double _FuelCost, int _LagDays, int _TaskActMinDu, int _MobilizationDu, int _AdFixDu, string _MHead,
        double _NoRec, int _ISGovt, bool _IsLocked, int _HrPerDay, int _GrpPerMonth, int _BID, int _CurrencyID,
        double _ConvRate, double _FuelCost_Forex, int _CaptiveMode) {
            FuelCost = _FuelCost; LagDays = _LagDays; TaskActMinDu = _TaskActMinDu; MobilizationDu = _MobilizationDu;
            AdFixDu = _AdFixDu; MHead = _MHead; NoRec = _NoRec; ISGovt = _ISGovt; IsLocked = _IsLocked; HrPerDay = _HrPerDay;
            GrpPerMonth = _GrpPerMonth; BID = _BID; CurrencyID = _CurrencyID; ConvRate = _ConvRate;
            FuelCost_Forex = _FuelCost_Forex; CaptiveMode = _CaptiveMode;
        }
    }

}
