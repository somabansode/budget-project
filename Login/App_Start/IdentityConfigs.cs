﻿using Login.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace Login
{
    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.

    public class ApplicationUserManagers : UserManager<ApplicationUsers>
    {
        public ApplicationUserManagers(IUserStore<ApplicationUsers> store)
            : base(store)
        {
        }

        public static ApplicationUserManagers Create(IdentityFactoryOptions<ApplicationUserManagers> options, IOwinContext context)
        {
            var manager = new ApplicationUserManagers(new UserStore<ApplicationUsers>(context.Get<ApplicationsDbContext>()));
            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<ApplicationUsers>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };
            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            };

            manager.EmailService = new Services.EMail();

            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUsers>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }
    }
}
