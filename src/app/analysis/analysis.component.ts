import { Component, ViewChild, OnInit, Injectable } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { AgGridNg2 } from 'ag-grid-angular';
import { GridOptions } from 'ag-grid-community';
import 'ag-grid-enterprise';
import { IdbService } from '../service/idb.service';
import { ITask, IUnit, ISection, IProject, IAssignment } from '../models/masters';
import * as $ from 'jquery';
import { UserService } from '../shared/services/user.service';
import { Subscription } from 'rxjs/Subscription';
import { ApiService } from '../core/services/api.service';
import { MessageService } from '../service/message.service';

@Component({
  selector: 'app-analysis',
  templateUrl: './analysis.component.html',
  styleUrls: ['./analysis.component.scss'],
  providers: []
})
export class AnalysisComponent implements OnInit {
  @ViewChild('agGrid') agGrid: AgGridNg2;
  title = 'budget';
  private gridApi;
  private gridColumnApi;
  projects: IProject[] = []; defProject: IProject = { ID: null, Project: null }; defPID = -1;
  tasks: ITask[] = [];
  assignments: IAssignment[] = [];
  taskAssignments: any = [];
  private subscription: Subscription;
  _sections: ISection[] = [];
  defSection: ISection = { ID: -1, Section: 'All Groups' }; defSec = -1;
  uniqueSections: ISection[] = [];
  public columnDefs; gridOptions: GridOptions = {};
  public detailCellRendererParams; public isRowMaster; public isFullWidthCell; public getRowHeight;
  public statusBar;
  public rowData: any;
  private altTasks: any;
  public frameworkComponents;
  public defaultColDef;
  constructor(private http: HttpClient, private userService: UserService, private apiService: ApiService
    , private messageService: MessageService) {
    this.columnDefs = [
      {
        headerName: 'Main Bill of Quantities',
        children: [
          { headerName: 'Seq', field: 'slno', width: 60, minWidth: 30, maxWidth: 100, cellRenderer: 'agGroupCellRenderer', cellStyle: { textAlign: 'right' } },
          { headerName: 'ID', field: 'TaskID', checkboxSelection: true, width: 80, minWidth: 30, maxWidth: 100 },
          {
            headerName: 'Gp', field: 'Section', width: 60, minWidth: 30, maxWidth: 100, editable: true, cellEditor: 'agSelectCellEditor'
            , cellEditorParams: { cellHeight: 50, values: this.userService.sectionNames }
          },
          { headerName: 'Ref', field: 'TaskRefNo', width: 80, minWidth: 30, maxWidth: 100 },
          { headerName: 'BOQ Code', field: 'TaskName', width: 150, minWidth: 30, maxWidth: 200, editable: true },
          {
            headerName: 'Unit', field: 'Unit', width: 60, minWidth: 30, maxWidth: 100, editable: true, cellEditor: 'agSelectCellEditor'
            , cellEditorParams: { cellHeight: 50, values: this.userService.unitNames }
          },
          { headerName: 'Qty', field: 'Quantity', width: 80, minWidth: 30, maxWidth: 100, editable: true, cellStyle: { textAlign: 'right' } },
          { headerName: 'Rate', field: 'TotalCost', width: 80, minWidth: 30, maxWidth: 100, cellStyle: { textAlign: 'right' } },
          { headerName: 'RAQ', field: 'RaQty', width: 80, minWidth: 30, maxWidth: 100, cellStyle: { textAlign: 'right' } },
        ]
      },
      {
        headerName: 'Narrative Details',
        children: [
          { headerName: 'BOQ Description', field: 'TaskDesc', editable: true, cellEditor: 'agLargeTextCellEditor' },
          { headerName: 'Group Description', field: 'GDesc', width: 80, minWidth: 30, maxWidth: 100 },
          { headerName: 'Method of Execution', field: 'Method', width: 80, minWidth: 30, maxWidth: 100 },
        ]
      },
      {
        headerName: 'System Fields',
        children: [
          { field: 'LinkTo', width: 60, minWidth: 30, maxWidth: 100 },
          { field: 'BaseLine', width: 80, minWidth: 30, maxWidth: 100 },
          { field: 'MTask', width: 80, minWidth: 30, maxWidth: 100 },
        ]
      },
      {
        headerName: 'Schedule',
        children: [
          { field: 'StDt', width: 80, minWidth: 30, maxWidth: 100 },
          { field: 'EdDt', width: 80, minWidth: 30, maxWidth: 100 },
        ]
      },
      {
        headerName: '% of Overhead Expenses',
        children: [
          { headerName: 'Fix', field: 'AdFixed', width: 60, minWidth: 30, maxWidth: 60 },
          { headerName: 'Run', field: 'Adruning', width: 60, minWidth: 30, maxWidth: 60 },
          { headerName: 'OP', field: 'OP', width: 60, minWidth: 30, maxWidth: 60 },
        ]
      },
      {
        headerName: 'Tender & Agreement',
        children: [
          { field: 'TenderRate', width: 80, minWidth: 30, maxWidth: 100 },
        ]
      }
    ];
    this.statusBar = {
      statusPanels: [
        { statusPanel: 'agTotalRowCountComponent', align: 'left' },
        { statusPanel: 'agFilteredRowCountComponent' },
        { statusPanel: 'agSelectedRowCountComponent' },
        { statusPanel: 'agAggregationComponent' }
      ]
    };
    this.defaultColDef = {
      enableValue: true,
      enableRowGroup: true,
      enablePivot: true
    };
    this.detailCellRendererParams = {
      detailGridOptions: {
        columnDefs: [
          { headerName: 'CID', field: 'CID', width: 50, rowGroup: true },
          { headerName: 'Category', field: 'CategoryName', width: 150 },
          { field: 'AID', width: 50, minWidth: 30, maxWidth: 70 },
          { field: 'RID', width: 50, minWidth: 30, maxWidth: 70 },
          { field: 'ResourceName' },
          { field: 'Unit', width: 50, minWidth: 30, maxWidth: 70 },
          { field: 'Coeff', width: 70, editable: true },
          { field: 'LQ', width: 60 },
          { field: 'LR', width: 60 },
          { field: 'FQ', width: 60 },
          { field: 'FR', width: 60 },
          { field: 'Cost', width: 70, minWidth: 30, maxWidth: 100 },
          { field: 'Remarks', editable: true, cellEditor: 'agLargeTextCellEditor' }
        ], onGridReady: function (params) {
          params.api.setGridAutoHeight(true);
        }, enableSorting: true,
        enableFilter: true, animateRows: true,
        onFirstDataRendered(params) {
          params.api.sizeColumnsToFit();
        }
      }, getDetailRowData: function (params) {
        params.successCallback(params.data.tAssignments);
      }
    };
    this.gridOptions.isRowMaster = function (dataItem) {
      return dataItem ? dataItem.tAssignments.length > 0 : false;
    };
    this.isFullWidthCell = function () {
      return false;
    };
    this.getRowHeight = function (params) {
      if (params.node && params.node.detail) {
        const offset = 80;
        const allDetailRowHeight = params.data.tAssignments.length * 28; // detail rows
        return allDetailRowHeight + offset;
      } else {
        return 30;
      }
    };
    this.mapProjects();
  }
  public onPID_Change(PID, event): void {
    this.defProject = this.projects.filter(i => i.ID === event)[0];
    this.userService.user.pid = PID;
    this.LoadProject(PID, event);
  }

  public LoadProject(PID, event) {
    this.rowData = [];
    this.uniqueSections = [];
    this.apiService.get('Tasks?data=' + PID + ',1').subscribe(data => {
      this.rowData = data;
      this.tasks = data;
      const ids = $.unique(data.map(function (d) { return d.SectionID; }));
      ids.sort((a, b) => a - b);
      ids.forEach(element => {
        const fSec = this.userService.allSections.filter(i => i.ID === element);
        this.uniqueSections.push(fSec[0]);
      });

      this.getAssignments(PID, event);
    });
    console.log(this.uniqueSections);
  }
  public getAssignments(PID, params) {
    this.taskAssignments = [];
    this.apiService.get('Assignments?pID=' + PID + '&gpID=null&tID=null').subscribe(data => {
      this.assignments = data;
      this.tasks.forEach(t => { t.tAssignments = this.assignments.filter(a => a.TID === t.TaskID); });
      this.rowData = this.tasks;
      this.gridApi = params.api;
      this.gridColumnApi = params.columnApi;
    });
  }
  uniquSecs(PID): void {
    this.uniqueSections = [];
    this.apiService.get('Tasks?data=' + PID + ',' + this.userService.user.cid).subscribe(data => {
      const ids = $.unique(data.map(function (d) { return d.SectionID; }));
      ids.sort((a, b) => a - b); // https://stackoverflow.com/questions/1063007/how-to-sort-an-array-of-integers-correctly
      ids.forEach(element => {
        const fSec = this.userService.allSections.filter(i => i.ID === element);
        this.uniqueSections.push(fSec[0]);
      });
    });
  }
  public onGp_Change(event): void {
    this.defSection = this.userService.allSections.filter(i => i.ID === event)[0];
    this.rowData = this.tasks.filter(t => t.SectionID === this.defSection.ID);
  }
  mapProjects(): void {
    const projectNames: string[] = [];
    this.apiService.get('Projects?cid=' + this.userService.user.cid).subscribe((us: IProject[]) => us.map(u => {
      const prj = { ID: u.ID, Project: u.Project };
      this.projects.push(prj);
    }));
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    console.log(this.defPID);
    this.getAssignments(this.defPID, params);
  }
  onCellValueChanged(params) {
    const colId = params.column.getId();
  }
  setData(params) {
    params.api.setRowData(this.rowData);
  }
  getRowData() { return this.altTasks; }
  getSelectedRows() {
    const selectedNodes = this.agGrid.api.getSelectedNodes();
    const selectedData = selectedNodes.map(node => node.data);
    const selectedDataStringPresentation = selectedData.map(node => node.TaskID + ' ' + node.TaskName).join(', ');
    alert(`Selected nodes: ${selectedDataStringPresentation}`);
  }

  ngOnInit() {
    this.subscription = this.userService.observableProjectId
      .subscribe(item => { if (item !== undefined) { this.onPID_Change(item, item); } });
    this.apiService.get(('Projects?cid=' + this.userService.user.cid)) // ('http://adc.6dproptech.com/Login/api/Sections?data=1,1')
      .subscribe((us: ISection[]) => us.map(u => {
        const sec = { ID: u.ID, Section: u.Section };
        this._sections.push(sec);
      }));
    console.log(this._sections);
  }
  getRows() {
    const selectedNodes = this.agGrid.api.getSelectedNodes();
    const selectedRow = selectedNodes.map(node => node.data);
    return selectedRow;
  }

  addRepeatTask(): void {
    const SelectedRow = this.getRows();
    if (SelectedRow.length > 0) {
      const id = prompt('Please enter number repeat number', '2');
      if (id != null) {
        this.apiService.post('tasks/post1?id=' + id, SelectedRow[0])
          .subscribe(
            data => {
              this.LoadProject(this.userService.projectId, event);
            },
            err => { });
      }
    } else {
      this.messageService.warning('Please select row and add.');
    }
  }

  AddBulkTask(): void {
    const SelectedRow = this.getRows();
    if (SelectedRow.length > 0) {
      this.apiService.post('tasks/post2', SelectedRow)
        .subscribe(
          data => {
            this.LoadProject(this.userService.projectId, event);
          },
          err => {
            this.messageService.error(err);
          });
    } else {
      this.messageService.warning('Please select row and add.');
    }
  }
}