export interface task {
    taskRefNo: number,
    taskName: string,
    taskDesc: string,
    unitID: number,
    sectionID: number,
    quantity: any,
    userID: string,
    projectID: number;
}