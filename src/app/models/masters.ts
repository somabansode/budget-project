export interface IUnit {
    ID: number;
    Unit: string;
    duID: number;
}
export interface IProject {
    ID: number;
    Project: string;
}
export interface ISection {
    ID: number;
    Section: string;
}
export interface ITask {
    CompanyId: number;
    ProjectID: number;
    slno: number;
    TaskID: number;
    UnitID: number;
    SectionID: number;
    CreatedBy: number;
    UpdatedBy: number;
    IsActive: number;
    TaskMasterID: number;
    LinkTo: number;
    TaskRefNo: string;
    TaskName: string;
    Unit: string;
    Section: string;
    TaskDesc: string;
    Method: string;
    StDt: string;
    EdDt: string;
    MTask: string;
    GDesc: string;
    BaseLine: string;
    Quantity: string;
    RaQty: number;
    TotalCost: number;
    TotalLength: number;
    TaskCost: number;
    AdFixed: number;
    Adruning: number;
    OP: number;
    TenderRate: number;
    TaskBasicCost: number;
    tAssignments: {};
}
export interface IAssignment {
    GID: number;
    Seq: number;
    TID: number;
    AID: number;
    CID: number;
    RID: number;
    UID: number;
    Rate: number;
    LQ: number;
    LR: number;
    FQ: number;
    FR: number;
    Coeff: number;
    Cost: number;
    Wastage: number;
    CategoryName: string;
    ResourceName: string;
    Unit: string;
    Remarks: string;
}
export interface IPMCategories {
    catID: number;
    CatName: string;
    GS: number;
    RatID: number;
    ModuleId: number;
    AccPath: string;
}
export interface IBillGenType {
    typeID: number;
    typeName: string;
}
export interface IClassingMaster {
     clsID: number;
    clsName: string;
}
export interface IDerivedUnitTypes {
     DUTId: number;
     DUTName: string;
}
export interface IResource {
    GS: number;
    gsName: string;
    gpID: number;
    ChildProcurementGroup: string;
    LedgerID: number;
    cfgID: number;
    clsID: number;
    Classing: string;
    rnkID: number;
    verID: number;
    VerifyType: string;
    CategoryGroup: string;
    Unit: string;
    duID: number;
    Property: string;
    PropValue: number;
    ResourceID: number;
    ResourceName: string;
    ResourceMake: string;
    ResourceUnit: number;
    ResourceUnitName: string;
    CategoryID: number;
    ResourceRate: number;
    ResourceBasicRate: number;
    ResourceLead: number;
    ResourceLeadRate: number;
    ResourceLPH: number;
    ResourcerRemarks: string;
    ResourceOutPut: string;
    CreatedOn: Date;
    CreatedBy: number;
    UpdatedOn: Date;
    UpdatedBy: number;
    IsActive: number;
    Extension: string;
    GoodsCategory: number;
    DeliveryDays: number;
    ssID: number;
    gpID1:number,
    gpID2:number,
    //for ddldisplay
    gpIDDisplay:string,
    cfgIDDisplay: string,
    clsIDDisplay: string,
    verIDDisplay: string,
    duIDDisplay: string,
    CategoryDisplay:string,
    GSDisplay : string;
}
export interface IAccountGroups {GroupID : number;  GroupName : number ;ParentId : number;  HeadID : number ;Bitsstatus :number;}
export interface IHeads { HeadID: number; AccountHead : string;}
export interface IResouurceAssetType {AssetTypeId: number;AssetType: string;  GroupID: number;IsGoods: boolean;IsActive: boolean;}
export interface IGdsSvc { GS: number; gsName: string;}
export interface IResourceCategory {   CategoryID: number; CategoryName: string;}
export class AccountGroups implements IAccountGroups {GroupID : number;  GroupName : number ;ParentId : number;  HeadID : number ;Bitsstatus :number;}
export class Heads implements IHeads { HeadID: number; AccountHead : string;}
export class ResouurceAssetType implements  IResouurceAssetType {AssetTypeId: number;AssetType: string;  GroupID: number;IsGoods: boolean;IsActive: boolean;}
export class GdsSvc implements IGdsSvc { GS: number; gsName: string;}
export class ResourceCategory implements IResourceCategory {   CategoryID: number; CategoryName: string;}





















