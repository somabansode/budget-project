import { Injectable, OnInit, OnDestroy } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse} from '@angular/common/http';
import { MessageService } from './message.service';
import { HttpErrorHandler, HandleError } from './http-error-handler.service';
import { IUnit, ITask, ISection, IProject, IAssignment, IGdsSvc, IResourceCategory, IPMCategories, IClassingMaster, IDerivedUnitTypes, IBillGenType } from '../models/masters';
import { toObservable } from '@angular/forms/src/validators';

@Injectable({
  providedIn: 'root' // Beginning with Angular 6.0 https://angular.io/guide/singleton-services
})
export class IdbService implements OnInit, OnDestroy {
  baseUrl = 'http://adc.6dproptech.com/Login/api/';
  _tasks: ITask[] = [];
  _projects: IProject[] = [];
  // sections: ISection[] = [];
  private _units: IUnit[] = [];
  public get units(): IUnit[] {
    // If present in Local Storage, return that
    // Else return whatever is there in memory
    // Else fetch from network, i.e. httpClient.get()
    return this._units;
  }
  public set units(value: IUnit[]) {
    // Store the countries in memory
    // Store the countries in the Local Storage
    this._units = value;
  }
  _sections: ISection[] = [];
  handleError: HandleError;
  projectsUrl = 'http://adc.6dproptech.com/Login/api/Projects?cid=1';
  unitsUrl = 'http://adc.6dproptech.com/Login/api/Units';
  // tasksURL = 'http://localhost/Login/api/Tasks?data=1,1' ; // ?data arguments companyID, ProjectID
  // sectionsURL = 'http://localhost/Login/api/Sections?data=1,1' ; // ?data arguments companyID, ProjectID
  constructor(private http: HttpClient, httpErrorHandler: HttpErrorHandler, private msg: MessageService) {
    this.handleError = httpErrorHandler.createHandleError('IdbService');
   // msg.add('constructor accessed'); console.log(msg.messages);
  }
  // getUnits(): Observable<Unit[]> {
  //   if (this._units) {
  //     return toObservable(this._units);
  //   } else {
  //     return this.http.get<Unit[]>(this.unitsUrl).pipe(catchError(this.handleError('getUnits', [])));
  //   }
  // }
  ngOnDestroy() {
    console.log(this._sections);
  }
  ngOnInit() {
    // this.getSections('http://adc.6dproptech.com/Login/api/Sections?data=1,1')
    //   .subscribe((us: ISection[]) => us.map(u => { const sec = { ID: u.ID, Section: u.Section };
    //   this._sections.push(sec); }));
    //   console.log(this._sections);
  }
  getAssignments(assUrl): Observable<IAssignment[]> { return this.http.get<IAssignment[]>(assUrl)
    .pipe(catchError(this.handleError('getAssignments', []))); }
  getTasks(tskURL): Observable<ITask[]> { return this.http.get<ITask[]>(tskURL)
    .pipe(catchError(this.handleError('getTasks', []))); }
  getUnits(): Observable<IUnit[]> { return this.http.get<IUnit[]>(this.unitsUrl)
      .pipe(catchError(this.handleError('getUnits', []))); }
  getSections(secURL): Observable<ISection[]> { return this.http.get<ISection[]>(secURL)
      .pipe(catchError(this.handleError('getSections', []))); }
  getProjects(): Observable<IProject[]> { return this.http.get<IProject[]>(this.projectsUrl)
      .pipe(catchError(this.handleError('getProjects', []))); }
  getGdsSvc(): IGdsSvc[] { return [{GS: 1, gsName: 'Goods'}, {GS: 2, gsName: 'Service'}]; }
  getRnkIds(): number[] { return  [1,2,3,4,5,6,7,8,9,10] }

  getResourceCategory(): Observable<IResourceCategory[]> { return this.http.get<IResourceCategory[]>(this.baseUrl + 'masters/ResourceCategory')
  .pipe(catchError(this.handleError('getSuperGroup', []))); }

  getPMCategories(cid:number): Observable<IPMCategories[]> { return this.http.get<IPMCategories[]>(this.baseUrl + 'masters/ResourceIntegration?cid='+cid)
  .pipe(catchError(this.handleError('getPMCategories', []))); }

  getClassingMaster(): Observable<IClassingMaster[]> { return this.http.get<IClassingMaster[]>(this.baseUrl + 'masters/ClassingMaster')
  .pipe(catchError(this.handleError('getClassingMaster', []))); }

  getDerivedUnitTypes(): Observable<IDerivedUnitTypes[]> { return this.http.get<IDerivedUnitTypes[]>(this.baseUrl + 'masters/DerivedUnitTypes')
  .pipe(catchError(this.handleError('getDerivedUnitTypes', []))); }

  getBillGenType(): Observable<IBillGenType[]> { return this.http.get<IBillGenType[]>(this.baseUrl + 'masters/BillGenType')
  .pipe(catchError(this.handleError('getBillGenType', []))); }
//   http://localhost/Login/api/masters/AccountGroups
// http://localhost/Login/api/masters/Heads
// http://localhost/Login/api/masters/ResourceAssetType
}
