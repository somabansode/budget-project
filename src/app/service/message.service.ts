import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class MessageService {

  constructor(private toastrService: ToastrService) { }

  success(msg: string) {
    this.toastrService.success(msg);
  }

  info(msg: string) {
    this.toastrService.info(msg);
  }

  warning(msg: string) {
    this.toastrService.warning(msg);
  }

  error(msg: string) {
    this.toastrService.error(msg);
  }
}