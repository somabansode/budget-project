import { NgModule } from '@angular/core';
import { RouterModule, Routes, ROUTES } from '@angular/router';
import { AnalysisComponent } from './analysis/analysis.component';
import { SectionsComponent } from './home/sections/sections.component';
import { TasksComponent } from './home/tasks/tasks.component';
import { ResourcesComponent } from './home/resources/resources.component';
import { ImportExcelComponent } from './home/import-excel/import-excel.component';
import { ResBulkComponent } from './home/res-bulk/res-bulk.component';
const appRoutes: Routes = [
  { path: 'analysis', component: AnalysisComponent, },
  { path: 'sections', component: SectionsComponent },
  { path: 'projects', component: AnalysisComponent },
  { path: 'task', component: TasksComponent },
  { path: 'resources', component: ResourcesComponent },
  { path: '', redirectTo: '/analysis', pathMatch: 'full' },
  { path: 'importexcel', component: ImportExcelComponent },
  { path: 'resourcesbulkinsert', component: ResBulkComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

