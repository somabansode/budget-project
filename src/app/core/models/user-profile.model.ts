import { User } from 'src/app/core/models/user.model';
import { AuthToken } from 'src/app/core/models/auth-token.model';

export interface UserProfile {
    tokenInformation: AuthToken;
    userInformation: User;
}
