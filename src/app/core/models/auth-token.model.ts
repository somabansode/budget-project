export interface AuthToken {
  id: string;
  authToken: string;
  expiration: number;
}
