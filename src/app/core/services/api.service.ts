import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ApiService {
    constructor(private http: HttpClient) { }
    get(path: string, params: HttpParams = new HttpParams(), applyBaseApiUrl = true): Observable<any> {
        const url = this.getUrl(path, applyBaseApiUrl);
        return this.http.get(url, { params }).pipe( catchError(this.formatErrors));
    }
    put(path: string, body: object = {}, applyBaseApiUrl = true): Observable<any> {
        return this.http.put(this.getUrl(path, applyBaseApiUrl), body).pipe(catchError(this.formatErrors));
    }
    post(path: string, body: object = {}, applyBaseApiUrl = true): Observable<any> {
        const url = this.getUrl(path, applyBaseApiUrl);
        return this.http.post(url, body).pipe(catchError(this.formatErrors));
    }
    delete(path: string , params: HttpParams= new HttpParams(), applyBaseApiUrl = true): Observable<any> {
        return this.http.delete(this.getUrl(path, applyBaseApiUrl), { params }).pipe(
            catchError(this.formatErrors));
    }
    private formatErrors(error: any) { return new ErrorObservable(error.error); }
    getUrl = (path: string, applyBaseApiUrl: boolean): string => {
        const url = applyBaseApiUrl === true ? `${environment.api_url}${path}` : `${path}`;
        return url;
    }
}