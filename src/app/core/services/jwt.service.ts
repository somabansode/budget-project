import { Injectable } from '@angular/core';
import { StorageService } from 'src/app/core/services/storage.service';
import { UserProfile } from '../models/user-profile.model';


@Injectable()
export class JwtService {

  constructor(private storageService: StorageService) {
  }

  getToken(): string {
    const userProfile: UserProfile = this.get();
    if (userProfile && userProfile.tokenInformation) {
      return userProfile.tokenInformation.authToken;
    }
  }

  get(): UserProfile {
    const userData = this.storageService.get();
    if (userData) {
      return JSON.parse(userData);
    }
  }

  save(token: UserProfile) {
    this.storageService.save(token);
  }

  destroy() {
    this.storageService.destroy();
  }
}
