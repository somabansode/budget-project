import { Injectable } from '@angular/core';
import { AuthToken } from 'src/app/core/models/auth-token.model';
import { UserProfile } from '../models/user-profile.model';
import { AuthorizationSettings } from '../../shared/constants/core/api-service.constants';


@Injectable()
export class StorageService {

  constructor() { }

  get(): string {
    if (window.localStorage[AuthorizationSettings.TokenInformation]) {
      return window.localStorage[AuthorizationSettings.TokenInformation];
    }
  }
  
  save(token: UserProfile) {
    window.localStorage[AuthorizationSettings.TokenInformation] = JSON.stringify(token);
  }

  destroy() {
    window.localStorage.removeItem(AuthorizationSettings.TokenInformation);
  }
}
