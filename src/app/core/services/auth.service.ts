import { Injectable } from '@angular/core';
import { map } from 'rxjs/Operators';
import { ApiService } from './api.service';
import { Login } from 'src/app/core/models/login.model';
import { JwtService } from './jwt.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ApiSettings } from '../../shared/constants/core/api-service.constants';
import { UserProfile } from '../models/user-profile.model';

@Injectable()
export class AuthService {

  public userProfile: UserProfile;

  constructor(private _apiService: ApiService, private jwtService: JwtService,
    public jwtHelper: JwtHelperService) {
      if (this.isAuthenticated()) {
            this.userProfile = jwtService.get();
      }
  }

  isAuthenticated(): boolean {
    const token = this.jwtService.getToken();
    return !this.jwtHelper.isTokenExpired(token);
  }

  attemptAuth(loginModel: Login) {
    return this._apiService.post(ApiSettings.ApiAuthLogin, loginModel)
      .pipe(map(result => {
        if (result) {
          this.setAuth(result);
        }
        return result;
      }));
  }

  setAuth(userProfile: UserProfile) {
    this.jwtService.save(userProfile);
    this.userProfile = userProfile;
  }

  purgeAuth() {
    return this._apiService.get(ApiSettings.ApiAuthLogout, )
      .pipe(map(result => {
        this.jwtService.destroy();
        this.userProfile = undefined;
      }));
  }
}
