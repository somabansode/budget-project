import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AgGridModule } from 'ag-grid-angular';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientXsrfModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './service/in-memory-data.service';
import { IdbService } from './service/idb.service';
import { MessageService } from './service/message.service';
import { HttpErrorHandler } from './service/http-error-handler.service';
import { AppRoutingModule } from './app-routing.module';
import { AnalysisComponent } from './analysis/analysis.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSidenavModule } from '@angular/material/sidenav';
import { SidenavComponent } from './shared/layout/sidenav/sidenav.component';
import { MaterialModule } from './material/material.module'
import { SectionsComponent } from './home/sections/sections.component';
import { TasksComponent } from './home/tasks/tasks.component';
import { ApiService } from './core/services/api.service';
import { AddSectionComponent } from './home/sections/add-section/add-section.component';
import { UserService } from './shared/services/user.service';
import { AddTaskComponent } from './home/tasks/add-task/add-task.component';
import { HeaderComponent } from './shared/layout/header/header.component';
import { ResourcesComponent } from './home/resources/resources.component';
import { ToastrModule } from 'ngx-toastr';
import { AddResourceComponent } from './home/resources/add-resource/add-resource.component';
import { ImportExcelComponent } from './home/import-excel/import-excel.component';
import { ResBulkComponent } from './home/res-bulk/res-bulk.component';

@NgModule({
  declarations: [
    AppComponent,
    SectionsComponent,
    AnalysisComponent,
    SidenavComponent,
    TasksComponent,
    AddSectionComponent,
    AddTaskComponent,
    AddResourceComponent,
    HeaderComponent,
    ResourcesComponent,
    ImportExcelComponent,
    ResBulkComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, FormsModule, ReactiveFormsModule, AppRoutingModule, AgGridModule.withComponents([]),
    HttpClientXsrfModule.withOptions({ cookieName: 'My-Xsrf-Cookie', headerName: 'My-Xsrf-Header', }),
    HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService, { dataEncapsulation: false, passThruUnknownUrl: true, put204: false }),
    MatSidenavModule, MaterialModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  entryComponents: [
    AddSectionComponent,
    AddTaskComponent,
    AddResourceComponent
  ],
  providers: [
    IdbService,
    HttpErrorHandler,
    MessageService,
    ApiService,
    UserService
  ],
  bootstrap: [AppComponent]

})
export class AppModule { }
