import { Injectable } from '@angular/core';
import { user } from 'src/app/models/user';
import { Subject } from 'rxjs';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Router } from '@angular/router';
import { NavItem } from 'src/app/models/NavItem';
import { IUnit, ISection } from '../../models/masters';
import { ApiService } from '../../core/services/api.service';

@Injectable()
export class UserService {
    public user: user;
    public projectId: number;
    public navItems: NavItem[];
    public observableProjectId: Subject<number> = new Subject();
    public units: IUnit[] = [];
    public unitNames: string[] = [];
    allSections: ISection[] = [];
    sectionNames: string[] = [];
    
    constructor(private router: Router, private apiService: ApiService) {
        this.observableProjectId = new BehaviorSubject<number>(this.projectId);
        this.user = <user>{  pid: 1, cid: 1 };
        this.GetNavigationMenus();
        this.mapUnits();
    }

    eventChange() { 
        this.observableProjectId.next(this.projectId); 
        this.mapSections();
    }

    GetNavigationMenus() {
        this.navItems = [
            {
                displayName: 'Analysis',
                iconName: 'home',
                route: '/analysis',
            },
            {
                displayName: 'Tasks',
                iconName: 'donut_small',
                route: '/task',
            },
            {
                displayName: 'Sections',
                iconName: 'info',
                route: '/sections'
            },
            {
                displayName: 'Resources',
                iconName: 'supervisor_account',
                route: '/resources'
            },

            {
                displayName: 'Import-Excel',
                iconName: 'import_export',
                route: '/importexcel'
            },

            {
                displayName: 'Bulk-Import-Resources',
                iconName: 'vertical_split',
                route: '/resourcesbulkinsert'
            },
            {
                displayName: 'Help',
                iconName: 'help_outline',
                route: '/'
            }
        ];
    }
    mapUnits() {
        const unitNames: string[] = [];
        this.apiService.get('Units').subscribe((us: IUnit[]) => us.map(u => {
          const unit = { ID: u.ID, Unit: u.Unit, duID: u.duID };
          this.units.push(unit);
          this.unitNames.push(u.Unit);
        }));
      }

      mapSections() {
        const sectionNames: string[] = [];
        if (this.projectId) {
          this.apiService.get('Sections?data=' + this.projectId + ',' + this.user.cid)
            .subscribe((res: ISection[]) => res.map(u => {
              const sec = { ID: u.ID, Section: u.Section };
              this.allSections.push(sec);
              this.sectionNames.push(u.Section);
            }));
        }
      }
}
