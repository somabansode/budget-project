export class Urls {
    public static CustomerQuote = '/customer/quote';
    public static CustomerLogin = '/customer/login';
    public static CustomerRegister = '/customer/register';

    public static RegisterHome = '/register/home';
    public static Models = '/models';
    public static ModelsCompare = '/models/compare';
    public static ModelsQuote = '/models/quote';
    public static ShareQuoteSuccess = '/models/shareQuoteSuccess';
    public static Home = '/home';
    public static QuoteInfo = '/customer/quote';
    public static Payment = '/customer/payment';
    public static KNetPayment = '/customer/knet';
    public static PaymentSuccess = '/customer/payment-success';
    public static UploadDocuments = '/customer/upload-documents';
    public static TrackOrder = '/customer/track-order';
    public static OrderStatus = '/customer/order-status';
    public static DeliveryConfirmation = '/customer/delivery-confirmation';
    public static ServiceRequest = '/customer//service-request';

    public static Service = '/service';
    public static ServiceConfirm = Urls.Service + '/confirm';
    public static ServiceDate = Urls.Service + '/select-date';
    public static ServiceDetails = Urls.Service + '/your-details';
    public static ServiceQuote = Urls.Service + '/your-quote';
    public static ServiceRetailer = Urls.Service + '/select-retailer';
    public static ServiceVechile = Urls.Service + '/your-vehicle';
}
