export class ApiSettings {

    public static ApiAuthLogin = "/auth/login";
    public static ApiAuthLogout = "/auth/logout";
    public static ApiQuoteByUserId = "/userquote/GetRecentUserQuote";
    public static ApiQuoteByRef = "/userquote/";
    public static ApiQuoteSave = "/userquote/create";
    public static ApiQuoteShare = "/userquote/Share";
    public static ApiUploadDoc = "/userbooking/uploaddocs";
    public static ApiGetDocsUploaded = "/userbooking/GetDocsUploaded";
    
    public static ApiVehicleModelsUrl = "./assets/data/vehicles.json";
    public static ApiCiviIdsUrl = "./assets/data/civilIds.json";
    public static ApiBookingUrlGet = "/userbooking/RecentOrder";
    public static ApiPlaceOrderUrl = "/userbooking/PlaceOrder";

    public static ApiAccountRegister = "/Account/Register";
    public static ApiUserServiceGetAll = "/userservice/GetAllAsync";
    public static ApiUserServiceGetById = "/userservice/GetByIdAsync";
    public static ApiUserServiceCreate = "/userservice/Create";
    public static ApiEstimationUrl = "./assets/data/estimation.json";

    public static ApiMenuStatus = "/menu/status";

}

export class AuthorizationSettings {
    public static Forbidden = "/unauthorized";
    public static Login = "/login";
    public static AccessDenied = "/accessdenied";
    public static TokenInformation = "tokenInformation";
}