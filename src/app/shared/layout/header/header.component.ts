import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/core/services/api.service';
import { IProject } from 'src/app/models/masters';
import { UserService } from '../../services/user.service';
import { Router, NavigationEnd, RouteConfigLoadEnd, ActivatedRoute } from '@angular/router';
import { filter, debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public projects: IProject[];
  public selectedValue: string;
  public currentMenu: string;
  constructor(private apiService: ApiService, private userService: UserService, private router: Router) {
    this.selectedValue = '-1';
    router.events.pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => {
        this.userService.navItems.filter(item => {
          if (item.route === event.url) { this.currentMenu = item.displayName; }
          if (event.url === '/') { this.currentMenu = 'Analysis'; }
        });
      });
  }

  ngOnInit() {
    this.getProjects();
  }

  getProjects() {
    const ss = this.apiService.get('Projects?cid=1').subscribe(result => {
      this.projects = result;
    })
  }

  onPID_Change(pid, event) {
    this.userService.projectId = pid;
    this.userService.eventChange();
  }
}

