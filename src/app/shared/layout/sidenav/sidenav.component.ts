import { Component, OnInit } from '@angular/core';
import { NavItem } from 'src/app/models/NavItem';
import { MatIconRegistry } from '@angular/material';
import { ApiService } from 'src/app/core/services/api.service';
import { IProject } from 'src/app/models/masters';
import { AnalysisComponent } from 'src/app/analysis/analysis.component';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
  providers: [AnalysisComponent]
})
export class SidenavComponent implements OnInit {
  public status: boolean;
  public projects: IProject[];
  public navItems:NavItem[];

  constructor(private apiService: ApiService, private userService: UserService, private analysisComponent: AnalysisComponent) { }

  ngOnInit() {
    this.getProjects();
    this.navItems= this.userService.navItems;
  }

  public onPID_Change(PID, event): void {
    this.analysisComponent.onPID_Change(PID, event);
  }

  getProjects() {
    var ss = this.apiService.get("Projects?cid=1").subscribe(result => {
      this.projects = result;
    })
  }
}
