import { Component, OnInit, ViewChild } from '@angular/core';
import * as XLSX from 'xlsx';
import { AgGridNg2 } from 'ag-grid-angular';
import { GridOptions } from 'ag-grid-community';
@Component({
  selector: 'app-import-excel',
  templateUrl: './import-excel.component.html',
  styleUrls: ['./import-excel.component.scss']
})
export class ImportExcelComponent implements OnInit {
  @ViewChild('agGrid') agGrid: AgGridNg2;
  public gridOptions: GridOptions;
  bufferData: any;
  constructor() {
    this.gridOptions = <GridOptions>{};
    this.gridOptions.columnDefs = [{ headerName: "Athlete", field: "athlete" },
    { headerName: "Age", field: "age" },
    { headerName: "Country", field: "country" },
    { headerName: "Year", field: "year" },
    { headerName: "Date", field: "date" },
    { headerName: "Sport", field: "sport" },
    { headerName: "Gold", field: "gold" },
    { headerName: "Silver", field: "silver" },
    { headerName: "Bronze", field: "bronze" },
    { headerName: "Total", field: "total" }
    ];
    this.gridOptions.rowData = [];
  }
  ngOnInit() {
  }
  importFile(event) {
    if (event.target.files.length == 0) {
      console.log("No file selected!");
      return
    }
    let file: File = event.target.files[0];
    let fileData = new Blob([file]);
    var promise = new Promise((resolve) => {
      var reader = new FileReader();
      reader.readAsArrayBuffer(fileData);
      reader.onloadend = function () {
        var arrayBuffer = reader.result
        resolve(arrayBuffer);
      }
    });
    // Wait for promise to be resolved, or log error.
    promise.then(function (data: any) {
      var worksheet = this.convertDataToWorkbook(data);
      this.populateGrid(worksheet);
      console.log(data);
    }.bind(this)).catch(function (err) {
      console.log('Error: ', err);
    });
  }
  convertDataToWorkbook(data) {
    data = new Uint8Array(data);
    var arr = new Array();
    for (var i = 0; i !== data.length; ++i) {
      arr[i] = String.fromCharCode(data[i]);
    }
    var bstr = arr.join("");
    return XLSX.read(bstr, { type: "binary" });
  }
  populateGrid(workbook) {
    // our data is in the first sheet
    var firstSheetName = workbook.SheetNames[0];
    var worksheet = workbook.Sheets[firstSheetName];
    // we expect the following columns to be present
    var columns = {
      'A': 'athlete',
      'B': 'age',
      'C': 'country',
      'D': 'year',
      'E': 'date',
      'F': 'sport',
      'G': 'gold',
      'H': 'silver',
      'I': 'bronze',
      'J': 'total'
    };
    var rowData = [];
    // start at the 2nd row - the first row are the headers
    var rowIndex = 2;
    // iterate over the worksheet pulling out the columns we're expecting
    while (worksheet['A' + rowIndex]) {
      var row = {};
      Object.keys(columns).forEach(function (column) {
        row[columns[column]] = worksheet[column + rowIndex].w;
      });
      rowData.push(row);
      rowIndex++;
    }
    // finally, set the imported rowData into the grid
    this.gridOptions.api.setRowData(rowData);
  }
  importExcelByFilePath() {
    this.makeRequest('GET',
      'http://localhost:4200/assets/OlymicData.xlsx',
      // success
      function (data) {
        var workbook = this.convertDataToWorkbook(data);
        this.populateGrid(workbook);
      }.bind(this),
      // error
      function (error) {
        throw error;
      }
    );
  }
  makeRequest(method, url, success, error) {
    var httpRequest = new XMLHttpRequest();
    httpRequest.open("GET", url, true);
    httpRequest.responseType = "arraybuffer";
    httpRequest.open(method, url);
    httpRequest.onload = function () {
      success(httpRequest.response);
    };
    httpRequest.onerror = function () {
      error(httpRequest.response);
    };
    httpRequest.send();
  }
  clear() {
    this.gridOptions.api.setRowData([]);
  }
}







