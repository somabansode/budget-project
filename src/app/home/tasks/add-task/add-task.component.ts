import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/core/services/api.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/shared/services/user.service';
import { MatDialogRef } from '@angular/material';
import { task } from 'src/app/models/task';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.scss']
})
export class AddTaskComponent implements OnInit {
  taskForm: FormGroup

  constructor(private apiService: ApiService,
    private formBuilder: FormBuilder, private dialogRef: MatDialogRef<AddTaskComponent>,
    private userService: UserService) { }

  ngOnInit() {
    this.taskForm = this.formBuilder.group({
      'taskName': [null, Validators.required],
      'taskDesc': [null],
      'quantity': [null, Validators.required],
      'taskRefNo': [null, Validators.required],
      'projectID': [this.userService.user.pid, Validators.required],
      'userID': [1, Validators.required],
    });
  }

  onFormSubmit(model: task): void {

    model.unitID = 12;
    model.sectionID = 1;

    this.apiService.post('tasks/post1?id=1', model)
      .subscribe(
        data => {
          this.closeDialog();
        },
        err => {
          //this.showError = err.error;
        });
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
