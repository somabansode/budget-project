import { Component, OnInit, ViewChild } from '@angular/core';
import { AddTaskComponent } from './add-task/add-task.component';
import { AgGridNg2 } from 'ag-grid-angular';
import { GridOptions } from 'ag-grid-community';
import { ApiService } from 'src/app/core/services/api.service';
import { MatDialog } from '@angular/material';
import { HttpClient, HttpParams } from '@angular/common/http';
import { UserService } from 'src/app/shared/services/user.service';
import { RequestOptions } from '@angular/http';
import { MessageService } from 'src/app/service/message.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {
  @ViewChild('agGrid') agGrid: AgGridNg2;
  public gridOptions: GridOptions;
  constructor(private apiService: ApiService, public dialog: MatDialog, private http: HttpClient,
    public userService: UserService, public messageService: MessageService) {
    this.gridOptions = <GridOptions>{};
    this.gridOptions.columnDefs = [
      {
        headerName: 'Tasks Details',
        children: [
          { headerName: 'Task Id', field: 'TaskID', width: 100, checkboxSelection: true, cellStyle: { textAlign: 'left' } },
          { headerName: 'Task Reference No', field: 'TaskRefNo', width: 100, cellStyle: { textAlign: 'left' } },
          { headerName: 'Task Name', field: 'TaskName', width: 350, cellStyle: { textAlign: 'left' } },
          { headerName: 'Unit', field: 'Unit', width: 100 },
          { headerName: 'Task Desc', field: 'TaskDesc', width: 500 },
          { headerName: 'Quantity', field: 'Quantity', width: 100 },
        ]
      }];
    this.gridOptions.rowData = [];
  }

  ngOnInit() {
    this.getTasks();
  }

  getSelectedRows() {
    const selectedNodes = this.agGrid.api.getSelectedNodes();
    var selectedRow = selectedNodes.map(node => node.data);
    return selectedRow;
  }

  getTasks() {
    if (this.userService.projectId) {
      this.apiService.get("Tasks?data=" + this.userService.projectId + ",1").subscribe(data => {
        this.gridOptions.api.setRowData(data);
      });
    }
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddTaskComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getTasks();
    });
  }

  deleteSection() {
    var selectedRow = this.getSelectedRows();
    if (selectedRow.length == 0) {
      this.messageService.warning("Please select task and delete.");
      return;
    }

    confirm("Do you want to delete selected task ?")
    {
      var ids = [];
      selectedRow.forEach(i => {
        ids.push(i['TaskID']);
      });

      var param = new RequestOptions({ body: ids });
      this.apiService.delete("tasks/bulkDelete").subscribe(result => {
        this.getTasks();
      });
    }
  }
}