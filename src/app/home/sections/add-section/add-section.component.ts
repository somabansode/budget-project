import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/core/services/api.service';
import { section } from 'src/app/models/section';
import { MatDialogRef } from '@angular/material';
import { UserService } from 'src/app/shared/services/user.service';


@Component({
  selector: 'app-add-section',
  templateUrl: './add-section.component.html',
  styleUrls: ['./add-section.component.scss']
})
export class AddSectionComponent implements OnInit {
  sectionForm: FormGroup;

  constructor(private apiService: ApiService,
    private formBuilder: FormBuilder, private dialogRef: MatDialogRef<AddSectionComponent>,
    private userService: UserService) { }

  ngOnInit() {
    this.sectionForm = this.formBuilder.group({
      'section': [null, Validators.required],
      'pid': [null, Validators.required],
      'cid': [null, Validators.required]
    });
  }


  onFormSubmit(model: section): void {
    debugger;
    model.pid = this.userService.user.pid;
    model.cid = this.userService.user.cid;
    this.apiService.post('Sections', model)
      .subscribe(
        data => {
          this.closeDialog();
        },
        err => {
          //this.showError = err.error;
        });
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
