import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/core/services/api.service';
import { GridOptions } from "ag-grid-community";
import { AgGridNg2 } from 'ag-grid-angular';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddSectionComponent } from './add-section/add-section.component';
import { HttpClient } from '@angular/common/http';
import { UserService } from 'src/app/shared/services/user.service';
import { MessageService } from 'src/app/service/message.service';

@Component({
  selector: 'app-sections',
  templateUrl: './sections.component.html',
  styleUrls: ['./sections.component.css']
})

export class SectionsComponent implements OnInit {
  @ViewChild('agGrid') agGrid: AgGridNg2;
  public gridOptions: GridOptions;
  constructor(private apiService: ApiService, public dialog: MatDialog, private http: HttpClient,
    public userService :UserService, private messageService:MessageService) {
    this.gridOptions = <GridOptions>{};
    this.gridOptions.columnDefs = [
      {
        headerName: 'Sections',
        children: [
          { headerName: 'ID', field: 'ID', width: 250, checkboxSelection: true, cellStyle: { textAlign: 'left' } },
          { headerName: 'Sections', field: 'Section', width: 500 },
        ]
      }];
    this.gridOptions.rowData = [];
  }

  ngOnInit() {
    this.getSection();
  }

  getSelectedRows() {
    const selectedNodes = this.agGrid.api.getSelectedNodes();
    var selectedRow = selectedNodes.map(node => node.data);
    return selectedRow;
  }

  getSection() {
    this.apiService.get("Sections?data=1,1").subscribe(data => {
      this.gridOptions.api.setRowData(data);
    });
  }


  openDialog(): void {
    const dialogRef = this.dialog.open(AddSectionComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getSection();
    });
  }

  deleteSection() {
    var selectedRow = this.getSelectedRows();
    if (selectedRow.length == 0) {
      this.messageService.warning("Please select section and delete.");
      return;
    }

    confirm("Do you want to delete selected Section ?")
    {
      this.apiService.delete("Sections/" + selectedRow[0]['ID']).subscribe(result => {
        this.getSection();
      });
    }
  }
}
