import { Component, OnInit, ViewChild } from '@angular/core';
import { AgGridNg2 } from 'ag-grid-angular';
import { GridOptions } from 'ag-grid-community';
import { ApiService } from 'src/app/core/services/api.service';
import { MatDialog } from '@angular/material';
import { UserService } from 'src/app/shared/services/user.service';
import { MessageService } from 'src/app/service/message.service';
import { AddResourceComponent } from './add-resource/add-resource.component';
@Component({
  selector: 'app-resources',
  templateUrl: './resources.component.html',
  styleUrls: ['./resources.component.scss']
})

export class ResourcesComponent implements OnInit {
  @ViewChild('agGrid') agGrid: AgGridNg2;
  public gridOptions: GridOptions;
  constructor(private apiService: ApiService, public dialog: MatDialog,
    public userService: UserService, private messageService:MessageService) {
    this.gridOptions = <GridOptions>{};
    this.gridOptions.columnDefs = [
      { headerName: 'Resource ID', field: 'ResourceID', width: 80, checkboxSelection: true, cellStyle: { textAlign: 'left' } },
      { headerName: 'Category ID', field: 'CategoryID', width: 80, cellStyle: { textAlign: 'left' } },
      { headerName: 'Delivery Days ', field: 'DeliveryDays', width: 70, cellStyle: { textAlign: 'left' } },
      { headerName: 'Extension', field: 'Extension', width: 100 },
      { headerName: 'Goods Category', field: 'GoodsCategory', width: 70 },
      { headerName: 'Resource ID', field: 'ResourceID', width: 70 },
      { headerName: 'Resource LPH', field: 'ResourceLPH', width: 70 },
      { headerName: 'Resource Lead', field: 'ResourceLead', width: 70 },
      { headerName: 'Resource Lead Rate', field: 'ResourceLeadRate', width: 70 },
      { headerName: 'Resource Make', field: 'ResourceMake', width: 100 },
      { headerName: 'Resource Name', field: 'ResourceName', width: 200 },
      { headerName: 'Resource OutPut', field: 'ResourceOutPut', width: 70 },
      { headerName: 'Resource Rate', field: 'ResourceRate', width: 70 },
      { headerName: 'Resource Unit', field: 'ResourceUnit', width: 70 },
      { headerName: 'Resourcer Remarks', field: 'ResourcerRemarks',width: 70},
    ];
    this.gridOptions.rowData = [];
  }

  ngOnInit() {
    this.getResources();
  }

  getResources() {
    this.apiService.get("resources").subscribe(data => {
      this.gridOptions.api.setRowData(data);
    });
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(AddResourceComponent, {
      width: '1000px',
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getResources();
    });
  }
}