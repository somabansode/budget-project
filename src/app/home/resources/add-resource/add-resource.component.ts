import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { IUnit, IGdsSvc, IResourceCategory, IPMCategories, IClassingMaster, IDerivedUnitTypes, IBillGenType, IResource } from 'src/app/models/masters';
import { IdbService } from 'src/app/service/idb.service';
import { ApiService } from 'src/app/core/services/api.service';
import { UserService } from 'src/app/shared/services/user.service';
@Component({
  selector: 'app-add-resource',
  templateUrl: './add-resource.component.html',
  styleUrls: ['./add-resource.component.scss']
})
export class AddResourceComponent implements OnInit {
  resourceForm: FormGroup;
  // units: IUnit[];
  goodsService: IGdsSvc[];
  resourceCategory: IResourceCategory[];
  resourceCategoryFiltered: IResourceCategory[];
  pMCategories: IPMCategories[];
  pMCategoriesFiltered: IPMCategories[];
  pMServiceCategoriesFiltered: IPMCategories[];
  classingMaster: IClassingMaster[];
  classingMasterFiltered: IClassingMaster[];
  DerivedUnitTypes: IDerivedUnitTypes[];
  BillGenType: IBillGenType[];
  RnkIds: number[];
  GoodsLabel: string;
  ServiceLabel: string;
  disableResourceUnit = false;
  disableLead = false;
  disableLPH = false;
  densityOn = false;

  constructor(private formBuilder: FormBuilder, public userService: UserService, private dialogRef: MatDialogRef<AddResourceComponent>
    , private svc: IdbService, private apiService: ApiService) { }
  ngOnInit() {
    this.getGdsSvc(); this.getResourceCategory(); this.getPMCategories(this.userService.user.cid);
    this.getClassingMaster(); this.getDerivedUnitTypes(); this.getBillGenType(); this.getRnkIds();
    this.resourceForm = this.formBuilder.group({
      'GS': [1, Validators.required],
      'gpID1': [2, Validators.required],
      'gpID2': [null],
      'LedgerID': [null],
      'cfgID': [-1],
      'clsID': [1, Validators.required],
      'rnkID': [5, Validators.required],
      'verID': [1, Validators.required],
      'duID': [8, Validators.required],
      'PropValue': [1000, Validators.required],
      'ResourceName': ['', Validators.required],
      'ResourceMake': [''],
      'ResourceUnit': [2, Validators.required],
      'CategoryID': [2, Validators.required],
      'ResourceRate': [0.00, Validators.min(1)],
      'ResourceBasicRate': [0.00, Validators.min(1)],
      'ResourceLead': [0.00],
      'ResourceLeadRate': [0.00],
      'ResourceLPH': [0.00],
      'ResourcerRemarks': [''],
      'DeliveryDays': [2],
      'ResourceOutPut': [''],
      'CreatedBy': [1],
      'UpdatedBy': [1],
      'IsActive': [1],
      'Extension': [''],
      'GoodsCategory': [1]
    });
  }
  // mapUnits(): void {
  //   this.svc.getUnits().subscribe((us: IUnit[]) => us.map(u => {
  //     this.units = us;
  //   }));
  // }
  getGdsSvc(): any { this.goodsService = this.svc.getGdsSvc(); }
  getResourceCategory(): any {
    this.svc.getResourceCategory().subscribe(
      (data: IResourceCategory[]) => {
        this.resourceCategoryFiltered = data;
        this.resourceCategory = data;
        this.resourceCategoryFiltered = this.resourceCategory.filter(r => r.CategoryID === 1 || r.CategoryID === 2);
      });
  }
  getPMCategories(cid: number): any {
    this.svc.getPMCategories(cid).subscribe(
      (data: IPMCategories[]) => {
        this.pMCategoriesFiltered = data;
        this.pMServiceCategoriesFiltered = data;
        this.pMCategories = data;
      });
  }
  getClassingMaster(): any {
    this.svc.getClassingMaster().subscribe(
      (data: IClassingMaster[]) => {
      this.classingMaster = data; this.onGdsSvcDdlChange(1);
        this.onCategoryDdlChange(2);
        this.onGoodsGroupChange(2);
      });
  }
  getDerivedUnitTypes(): any {
    this.svc.getDerivedUnitTypes().subscribe(
      (data: IDerivedUnitTypes[]) => { this.DerivedUnitTypes = data; });
  }
  getBillGenType(): any {
    this.svc.getBillGenType().subscribe(
      (data: IBillGenType[]) => { this.BillGenType = data; });
  }
  post_resource(resource: IResource) { }
  onGdsSvcDdlChange(GS: any) {
    if (+GS !== 1) {
      this.ServiceLabel = '';
      this.GoodsLabel = '';
    }
    this.resourceCategoryFiltered = [];
    this.classingMasterFiltered = [];
    this.pMCategoriesFiltered = [];
    this.pMServiceCategoriesFiltered = [];
    if (+GS === 1) {
      this.classingMasterFiltered = this.classingMaster.filter(r => r.clsID !== 4);
      this.resourceCategoryFiltered = this.resourceCategory.filter(r => r.CategoryID === 1 || r.CategoryID === 2);
      this.resourceForm.get('CategoryID').setValue(2);
      // tslint:disable-next-line:no-unused-expression
      this.resourceForm.get('ResourceLead').enabled;
      // tslint:disable-next-line:no-unused-expression
      this.resourceForm.get('ResourceLeadRate').enabled;
      this.
        onCategoryDdlChange(2);
      this.densityOn = false;
      this.disableResourceUnit = false;
    } else {
      this.classingMasterFiltered = this.classingMaster.filter(r => r.clsID === 4);
      this.resourceCategoryFiltered = this.resourceCategory.filter(r => r.CategoryID !== 2 && r.CategoryID !== 1);
      this.pMCategoriesFiltered = this.pMCategories.filter(r => r.GS === GS);
      this.pMServiceCategoriesFiltered = this.pMCategoriesFiltered;
      this.resourceForm.get('CategoryID').setValue(this.resourceCategoryFiltered[0].CategoryID);
      this.onCategoryDdlChange(this.resourceCategoryFiltered[0].CategoryID);
    }
    this.resourceForm.get('clsID').setValue(this.classingMasterFiltered[0].clsID);
    if (this.pMCategoriesFiltered.length > 0) {
      this.resourceForm.get('gpID1').setValue(this.pMCategoriesFiltered[0].catID);
      this.onGoodsGroupChange(this.pMCategoriesFiltered[0].catID);
    }
    if (this.pMServiceCategoriesFiltered.length > 0) {
      this.onServiceGroupChange(this.pMServiceCategoriesFiltered[0].catID);
      this.resourceForm.get('gpID2').setValue(this.pMServiceCategoriesFiltered[0].catID);
    }
    this.disableLead = false;
    this.disableLPH = false;
    this.disableResourceUnit = false;
    if (+this.resourceCategoryFiltered[0].CategoryID === 1 || +this.resourceCategoryFiltered[0].CategoryID === 3) {
      this.disableResourceUnit = true;
    }
    if (+this.resourceCategoryFiltered[0].CategoryID === 1) {
      this.disableLPH = false;
      this.disableLead = true;
    }
    if (+this.resourceCategoryFiltered[0].CategoryID === 2) {
      this.disableLead = false;
      this.disableLPH = true;
    }
    if (this.resourceForm.controls.GS.value === 2) {
      this.pMCategoriesFiltered = [];
    } else if (this.resourceForm.controls.GS.value === 1 && this.resourceForm.controls.CategoryID.value === 2) {
      this.pMServiceCategoriesFiltered = [];
    }

  }
  getRnkIds() {
    this.RnkIds = this.svc.getRnkIds();
  }
  onCategoryDdlChange(value: any) {
    this.ServiceLabel = '';
    this.GoodsLabel = '';
    this.pMCategoriesFiltered = [];
    this.disableResourceUnit = false;
    this.disableLead = false;
    this.disableLPH = false;
    if (+value === 1 && +this.resourceForm.controls.GS.value === 1) {
      this.classingMasterFiltered = this.classingMaster.filter(r => r.clsID === 4);
      this.resourceForm.get('clsID').setValue(this.classingMasterFiltered[0].clsID);
      this.disableResourceUnit = false;
    } else if (+value !== 1 && +this.resourceForm.controls.GS.value === 1) {

      this.classingMasterFiltered = this.classingMaster.filter(r => r.clsID !== 4);
      this.resourceForm.get('clsID').setValue(this.classingMasterFiltered[0].clsID);
      this.disableResourceUnit = false;
    }
    this.resourceForm.get('clsID').setValue(this.classingMasterFiltered[0].clsID);
    if (+value === 1 || +value === 3) {
      this.disableResourceUnit = true;
      this.resourceForm.get('ResourceUnit').setValue(20);
    } else {
      this.resourceForm.get('ResourceUnit').setValue(2);
    }
    if (+value !== 2) {
      this.densityOn = true;
    }
    if (+value === 1) {
      this.disableLPH = false;
      this.disableLead = true;
    }
    if (+value === 2) {
      this.disableLead = false;
      this.disableLPH = true;
      this.densityOn = false;
    }// for machinery & goods
    if (+value === 1 && +this.resourceForm.controls.GS.value === 1) {
      this.pMCategoriesFiltered = this.pMCategories.filter(r => r.GS === 1 && r.RatID !== 4);
    } else if (+value === 2 && +this.resourceForm.controls.GS.value === 1) {
      this.pMCategoriesFiltered = this.pMCategories.filter(r => r.GS === 1 && r.RatID === 4);
    }// for machinary & service
    if (+value === 1 && +this.resourceForm.controls.GS.value === 2) {
      this.pMCategoriesFiltered = this.pMCategories.filter(r => r.GS === 2 && r.RatID === 3);
    }// for labor & service
    if (+value === 3 && +this.resourceForm.controls.GS.value === 2) {
      this.pMCategoriesFiltered = this.pMCategories.filter(r => r.GS === 2 && r.RatID === 11);
    } // for contract & service
    if (+value === 4 && +this.resourceForm.controls.GS.value === 2) {
      this.pMCategoriesFiltered = this.pMCategories.filter(r => r.GS === 2 && r.RatID !== 11 && r.RatID !== 3);
    }
    this.pMServiceCategoriesFiltered = this.pMCategoriesFiltered;
    if (+value === 1 && +this.resourceForm.controls.GS.value === 1) {
      this.pMServiceCategoriesFiltered = [];
      this.pMServiceCategoriesFiltered = this.pMCategories.filter(r => r.GS === 2 && r.RatID === 3);
    }

    if (this.resourceForm.controls.GS.value === 1 && this.resourceForm.controls.CategoryID.value === 2) {
      this.pMServiceCategoriesFiltered = [];
    }
    if (this.resourceForm.controls.GS.value === 2) {
      this.pMCategoriesFiltered = [];
    }
    if (this.pMCategoriesFiltered.length > 0) {
      this.resourceForm.get('gpID1').setValue(this.pMCategoriesFiltered[0].catID);
      this.onGoodsGroupChange(this.pMCategoriesFiltered[0].catID);
    }
    if (this.pMServiceCategoriesFiltered.length > 0) {
      this.onServiceGroupChange(this.pMServiceCategoriesFiltered[0].catID);
      this.resourceForm.get('gpID2').setValue(this.pMServiceCategoriesFiltered[0].catID);

    }
  }
  onServiceGroupChange(value: any) {
    this.ServiceLabel = this.pMCategories.filter(r => r.catID === +value)[0].AccPath;
  }
  onGoodsGroupChange(value: any) {
    this.GoodsLabel = this.pMCategories.filter(r => r.catID === +value)[0].AccPath;
  }
  onFormSubmit(model: IResource): void {
    if (model.gpID1 != null && model.gpID1 !== 0 && model.gpID2 != null && model.gpID2 !== 0) {
      model.gpID = model.gpID1;
      model.cfgID = model.gpID2;
    } else if (model.gpID1 != null && model.gpID1 !== 0) {
      model.gpID = model.gpID1;
    } else if (model.gpID2 != null && model.gpID2 !== 0) {
      model.cfgID = model.gpID2;
    } else {
      model.cfgID = -1;
    }
    const clResources: IResource[] = [];
    clResources.push(model);
    console.log(clResources);
    this.apiService.post('Resources/bulkInsert?cid=1', clResources)
      .subscribe(
        data => {
          alert(data);
          this.closeDialog();
        },
        err => {
          alert(err);
          // this.showError = err.error;
        });
  }
  closeDialog() {
    this.dialogRef.close();
  }
}
