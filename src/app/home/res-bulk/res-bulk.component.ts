import { Component, OnInit, ViewChild } from '@angular/core';
import * as XLSX from 'xlsx';
import { AgGridNg2 } from 'ag-grid-angular';
import { GridOptions } from 'ag-grid-community';
import 'ag-grid-enterprise';
import { IResource, IDerivedUnitTypes, IBillGenType, IClassingMaster, IPMCategories, IResourceCategory,
  IUnit, IGdsSvc } from '../../models/masters';
import { ApiService } from '../../core/services/api.service';
import { IdbService } from '../../service/idb.service';
import { UserService } from '../../shared/services/user.service';
@Component({
  selector: 'app-res-bulk',
  templateUrl: './res-bulk.component.html',
  styleUrls: ['./res-bulk.component.scss']
})
export class ResBulkComponent implements OnInit {
  @ViewChild('agGrid') agGrid: AgGridNg2;
  public gridOptions: GridOptions;
  bufferData: any;
  resources: IResource[] = [];
  // units: IUnit[];
  // unitNames: string[] = [];
  goodsService: IGdsSvc[];
  goodsServiceNames: string[] = [];
  resourceCategory: IResourceCategory[];
  resourceCategoryNames: string[] = [];
  pMCategories: IPMCategories[];
  pMCategoriesNames: string[] = [];
  classingMaster: IClassingMaster[];
  classingMasterNames: string[] = [];
  DerivedUnitTypes: IDerivedUnitTypes[];
  derivedUnitTypesNames: string[] = [];
  BillGenType: IBillGenType[];
  billGenTypeNames: string[] = [];
  categoryID: number;
  public defaultColDef; // cell editing https://plnkr.co/edit/?p=preview
  constructor(private apiService: ApiService, private userService: UserService , private svc: IdbService) {
    this.getResourceCategory();
    this.getClassingMaster();
    this.getPMCategories(1);
    // this.mapUnits();
    this.getBillGenType();
    this.getDerivedUnitTypes();
    this.gridOptions = <GridOptions>{};
    this.gridOptions.columnDefs = [];

    this.defaultColDef = {
      enableValue: true,
      enableRowGroup: true
    };

    function currencyFormatter(params) {
      return '$hi ' + (params.value);
    }
    this.gridOptions.rowData = [];
  }
  ngOnInit() {
  }
  importFile(event) {
    // tslint:disable-next-line:no-debugger
    debugger;
    if (event.target.files.length === 0) {
      console.log('No file selected!');
      return;
    }
    const file: File = event.target.files[0];
    const fileData = new Blob([file]);
    const promise = new Promise((resolve) => {
      const reader = new FileReader();
      reader.readAsArrayBuffer(fileData);
      reader.onloadend = function () {
        const arrayBuffer = reader.result;
        resolve(arrayBuffer);
      };
    });
    // Wait for promise to be resolved, or log error.
    promise.then(function (data: any) {
      const worksheet = this.convertDataToWorkbook(data);
      this.populateGridForMaterial(worksheet);
      console.log(data);
    }.bind(this)).catch(function (err) {
      console.log('Error: ', err);
    });
  }
  convertDataToWorkbook(data) {
    data = new Uint8Array(data);
    const arr = new Array();
    for (let i = 0; i !== data.length; ++i) {
      arr[i] = String.fromCharCode(data[i]);
    }
    // tslint:disable-next-line:prefer-const
    let bstr = arr.join('');
    return XLSX.read(bstr, { type: 'binary' });
  }
  populateGrid(workbook) {
    // our data is in the first sheet
    const firstSheetName = workbook.SheetNames[0];
    const worksheet = workbook.Sheets[firstSheetName];
    // we expect the following columns to be present
    const columns = {
      'A': 'GS',
      'B': 'gpID',
      'C': 'cfgID',
      'D': 'clsID',
      'E': 'rnkID',
      'F': 'verID',
      'G': 'duID',
      'H': 'PropValue',
      'I': 'ResourceName',
      'J': 'ResourceMake',
      'K': 'ResourceUnit',
      'L': 'CategoryID',
      'M': 'ResourceRate',
      'N': 'ResourceBasicRate',
      'O': 'ResourceLead',
      'P': 'ResourceLeadRate',
      'Q': 'ResourceLPH',
      // 'R': 'ResourcerRemarks',
      // 'S': 'ResourceOutPut',
      // 'T': 'CreatedBy',
      // 'U': 'UpdatedBy',
      // 'V': 'IsActive',
      // 'W': 'GoodsCategory',
      'R': 'DeliveryDays'

    };

    const rowData = [];
    // start at the 2nd row - the first row are the headers
    let rowIndex = 2;
    // iterate over the worksheet pulling out the columns we're expecting
    while (worksheet['A' + rowIndex]) {
      const row = {};
      Object.keys(columns).forEach(function (column) {
        if (worksheet[column + rowIndex] !== undefined) {
          row[columns[column]] = worksheet[column + rowIndex].w;
        }
      });
      rowData.push(row);
      rowIndex++;
    }
    // debugger;
    Object.assign(this.resources, rowData);

    this.resources.forEach((value, i) => {
      this.userService.units.forEach((val) => {
        if (+value.ResourceUnit === val.ID) {
          this.resources[i].ResourceUnitName = val.ID + ': ' + val.Unit;
        }
      });

      this.resourceCategory.forEach((val) => {
        if (+value.CategoryID === val.CategoryID) {
          this.resources[i].CategoryDisplay = val.CategoryID + ': ' + val.CategoryName;
        }
      });

      this.pMCategories.forEach((val) => {
        if (+value.gpID === val.catID) {
          this.resources[i].gpIDDisplay = val.catID + ': ' + val.CatName;
        }
        if (+value.cfgID === val.catID) {
          this.resources[i].cfgIDDisplay = val.catID + ': ' + val.CatName;
        }
      });

      this.classingMaster.forEach((val) => {
        if (+value.clsID === val.clsID) {
          this.resources[i].clsIDDisplay = val.clsID + ': ' + val.clsName;
        }
      });

      // this.BillGenType.forEach((val) => {
      //   if (+value.verID === val.typeID) {
      //     this.resources[i].verIDDisplay = val.typeID + ': ' + val.typeName;
      //   }
      // });
      // this.DerivedUnitTypes.forEach((val) => {
      //   if (+value.duID === val.DUTId) {
      //     this.resources[i].duIDDisplay = val.DUTId + ': ' + val.DUTName;
      //   }
      // });

      this.goodsService.forEach((val) => {
        if (+value.GS === val.GS) {
          this.resources[i].GSDisplay = val.GS + ': ' + val.gsName;
        }
      });

    });
    // finally, set the imported rowData into the grid
    this.gridOptions.api.setRowData(this.resources);
  }
  populateGridForMaterial(workbook) {
    // our data is in the first sheet
    const firstSheetName = workbook.SheetNames[0];
    const worksheet = workbook.Sheets[firstSheetName];
    // we expect the following columns to be present
    const columns = {
      'A': 'gpID',
      'B': 'clsID',
      'C': 'rnkID',
      'D': 'verID',
      'E': 'duID',
      'F': 'PropValue',
      'G': 'ResourceName',
      'H': 'ResourceMake',
      'I': 'ResourceUnit',
      'J': 'CategoryID',
      'K': 'ResourceRate',
      'L': 'ResourceBasicRate',
      'M': 'ResourceLead',
      'N': 'ResourceLeadRate',
      'O': 'DeliveryDays'
    };

    const rowData = [];
    // start at the 2nd row - the first row are the headers
    let rowIndex = 2;
    // iterate over the worksheet pulling out the columns we're expecting
    while (worksheet['A' + rowIndex]) {
      const row = {};
      Object.keys(columns).forEach(function (column) {
        if (worksheet[column + rowIndex] !== undefined) {
          row[columns[column]] = worksheet[column + rowIndex].w;
        }
      });
      rowData.push(row);
      rowIndex++;
    }
    // debugger;
    Object.assign(this.resources, rowData);
    this.resources.forEach((value, i) => {
      this.userService.units.forEach((val) => {
        if (+value.ResourceUnit === val.ID) {
          this.resources[i].ResourceUnitName = val.ID + ': ' + val.Unit;
        }
      });

      this.resourceCategory.forEach((val) => {
        if (+value.CategoryID === val.CategoryID) {
          this.resources[i].CategoryDisplay = val.CategoryID + ': ' + val.CategoryName;
        }
      });

      this.pMCategories.forEach((val) => {
        if (+value.gpID === val.catID) {
          this.resources[i].gpIDDisplay = val.catID + ': ' + val.CatName;
        }
        if (+value.cfgID === val.catID) {
          this.resources[i].cfgIDDisplay = val.catID + ': ' + val.CatName;
        }
      });
      this.classingMaster.forEach((val) => {
        if (+value.clsID === val.clsID) {
          this.resources[i].clsIDDisplay = val.clsID + ': ' + val.clsName;
        }
      });
      // this.BillGenType.forEach((val) => {
      //   if (+value.verID === val.typeID) {
      //     this.resources[i].verIDDisplay = val.typeID + ': ' + val.typeName;
      //   }
      // });
      // this.DerivedUnitTypes.forEach((val) => {
      //   if (+value.duID === val.DUTId) {
      //     this.resources[i].duIDDisplay = val.DUTId + ': ' + val.DUTName;
      //   }
      // });

      // this.goodsService.forEach((val) => {
      //   if (+value.GS === val.GS) {
      //     this.resources[i].GSDisplay = val.GS + ': ' + val.gsName;
      //   }
      // });

    });
    // finally, set the imported rowData into the grid
    this.gridOptions.api.setRowData(this.resources);
  }

  populateGridForMachinary(workbook) {
    // our data is in the first sheet
    const firstSheetName = workbook.SheetNames[0];
    const worksheet = workbook.Sheets[firstSheetName];
    // we expect the following columns to be present
    const columns = {
      'B': 'gpID',
      'C': 'cfgID',
      'D': 'clsID',
      'E': 'rnkID',
      'F': 'verID',
      'G': 'duID',
      'H': 'PropValue',
      'I': 'ResourceName',
      'J': 'ResourceMake',
      'K': 'ResourceUnit',
      'L': 'CategoryID',
      'M': 'ResourceRate',
      'N': 'ResourceBasicRate',
      'O': 'ResourceLead',
      'P': 'ResourceLeadRate',
      'Q': 'ResourceLPH',
      'R': 'DeliveryDays'
    };

    const rowData = [];
    // start at the 2nd row - the first row are the headers
    let rowIndex = 2;
    // iterate over the worksheet pulling out the columns we're expecting
    while (worksheet['A' + rowIndex]) {
      const row = {};
      Object.keys(columns).forEach(function (column) {
        if (worksheet[column + rowIndex] !== undefined) {
          row[columns[column]] = worksheet[column + rowIndex].w;
        }
      });
      rowData.push(row);
      rowIndex++;
    }
    // tslint:disable-next-line:no-debugger
    debugger;
    Object.assign(this.resources, rowData);
    this.resources.forEach((value, i) => {
      this.userService.units.forEach((val) => {
        if (+value.ResourceUnit === val.ID) {
          this.resources[i].ResourceUnitName = val.ID + ': ' + val.Unit;
        }
      });

      this.resourceCategory.forEach((val) => {
        if (+value.CategoryID === val.CategoryID) {
          this.resources[i].CategoryDisplay = val.CategoryID + ': ' + val.CategoryName;
        }
      });

      this.pMCategories.forEach((val) => {
        if (+value.gpID === val.catID) {
          this.resources[i].gpIDDisplay = val.catID + ': ' + val.CatName;
        }
        if (+value.cfgID === val.catID) {
          this.resources[i].cfgIDDisplay = val.catID + ': ' + val.CatName;
        }
      });
      this.classingMaster.forEach((val) => {
        if (+value.clsID === val.clsID) {
          this.resources[i].clsIDDisplay = val.clsID + ': ' + val.clsName;
        }
      });
      // this.BillGenType.forEach((val) => {
      //   if (+value.verID === val.typeID) {
      //     this.resources[i].verIDDisplay = val.typeID + ': ' + val.typeName;
      //   }
      // });
      // this.DerivedUnitTypes.forEach((val) => {
      //   if (+value.duID === val.DUTId) {
      //     this.resources[i].duIDDisplay = val.DUTId + ': ' + val.DUTName;
      //   }
      // });

      // this.goodsService.forEach((val) => {
      //   if (+value.GS === val.GS) {
      //     this.resources[i].GSDisplay = val.GS + ': ' + val.gsName;
      //   }
      // });

    });
    // finally, set the imported rowData into the grid
    this.gridOptions.api.setRowData(this.resources);
  }

  insertBulk(): void {
    // tslint:disable-next-line:no-debugger
    debugger;
    if (this.resources.length > 0) {
      if (this.categoryID === 2) {
        this.resources.forEach((value, i) => {
          this.resources[i].cfgID = -1;
          this.resources[i].CategoryID = 2;
          this.resources[i].GS = 1;
          this.resources[i].verID = 2;
          this.resources[i].duID = 8;
          this.resources[i].ResourcerRemarks = '';
          this.resources[i].ResourceOutPut = '';
          this.resources[i].CreatedBy = 1;
          this.resources[i].UpdatedBy = 1;
          this.resources[i].IsActive = 1,
          this.resources[i].Extension = '';
          this.resources[i].GoodsCategory = 0;
          this.resources[i].LedgerID = null;

          this.userService.units.forEach((val) => {
            if (value.ResourceUnitName === val.ID + ': ' + val.Unit) {
              this.resources[i].ResourceUnit = val.ID;
            }
          });
          this.pMCategories.forEach((val) => {
            if (value.gpIDDisplay === val.catID + ': ' + val.CatName) {
              this.resources[i].gpID = val.catID;
            }
            if (value.cfgIDDisplay === val.catID + ': ' + val.CatName) {
              this.resources[i].cfgID = val.catID;
            }
          });
          this.classingMaster.forEach((val) => {
            if (value.clsIDDisplay === val.clsID + ': ' + val.clsName) {
              this.resources[i].clsID = val.clsID;
            }
          });
          this.DerivedUnitTypes.forEach((val) => {
            if (value.duIDDisplay === val.DUTId + ': ' + val.DUTId) {
              this.resources[i].duID = val.DUTId;
            }
          });
          this.BillGenType.forEach((val) => {
            if (value.verIDDisplay === val.typeID + ': ' + val.typeName) {
              this.resources[i].verID = val.typeID;
            }
          });
        delete  this.resources[i].duIDDisplay ;
          delete this.resources[i].cfgIDDisplay ;
          delete this.resources[i].CategoryDisplay ;
          delete this.resources[i].gpIDDisplay ;
          delete this.resources[i].GSDisplay ;
          delete  this.resources[i].ResourceUnitName ;
          delete  this.resources[i].verIDDisplay ;
          delete this.resources[i].ResourceUnitName ;
          delete this.resources[i].clsIDDisplay ;

        });
      }
      // tslint:disable-next-line:no-debugger
      debugger;
      console.log(this.resources);
      this.apiService.post('Resources/bulkInsert?cid=1', this.resources)
        .subscribe(
          data => {
            alert(data);
          },
          err => {
            // tslint:disable-next-line:no-debugger
            debugger;
            alert(err);
            // this.showError = err.error;
          });
    } else {
      alert('There is no record in grid to insert');
    }
  }

  clear() {
    this.gridOptions.api.setRowData([]);
  }
  // mapUnits(): string[] {
  //   this.units = [];
  //   this.svc.getUnits().subscribe((us: IUnit[]) => {
  //     this.units = us; us.forEach((item: IUnit) => {
  //       this.unitNames.push(item.ID + ': ' + item.Unit);
  //     });
  //   });
  //   return this.unitNames;
  // }
  getGdsSvc(): string[] {
    this.goodsService = [];
    this.goodsService = this.svc.getGdsSvc();
    this.goodsService.forEach((item: IGdsSvc) => {
      this.goodsServiceNames.push(item.GS + ': ' + item.gsName);
    });
    return this.goodsServiceNames;

  }
  getResourceCategory(): string[] {
    this.resourceCategory = [];
    this.svc.getResourceCategory().subscribe(
      (data: IResourceCategory[]) => {
        this.resourceCategory = data;
        this.resourceCategory.forEach((item: IResourceCategory) => {
          this.resourceCategoryNames.push(item.CategoryID + ': ' + item.CategoryName);
        });
      });
    return this.resourceCategoryNames;
  }
  getPMCategories(cid: number): string[] {
    this.pMCategories = [];
    this.svc.getPMCategories(cid).subscribe(
      (data: IPMCategories[]) => {
        this.pMCategories = data;
        this.pMCategories.forEach((item: IPMCategories) => {
          this.pMCategoriesNames.push(item.catID + ': ' + item.CatName);
        });

      });
    return this.pMCategoriesNames;
  }
  getClassingMaster(): string[] {
    this.classingMaster = [];
    this.svc.getClassingMaster().subscribe(
      (data: IClassingMaster[]) => {
        this.classingMaster = data;
        this.classingMaster.forEach((item: IClassingMaster) => {
          this.classingMasterNames.push(item.clsID + ': ' + item.clsName);
        });
      });
    return this.classingMasterNames;
  }
  getDerivedUnitTypes(): string[] {
    this.DerivedUnitTypes = [];
    this.svc.getDerivedUnitTypes().subscribe(
      (data: IDerivedUnitTypes[]) => {
        // tslint:disable-next-line:no-debugger
        debugger; this.DerivedUnitTypes = data;
        this.DerivedUnitTypes.forEach((item: IDerivedUnitTypes) => {
          this.derivedUnitTypesNames.push(item.DUTId + ': ' + item.DUTName);
        });
      });
    return this.derivedUnitTypesNames;

  }
  getBillGenType(): string[] {
    this.BillGenType = [];
    this.svc.getBillGenType().subscribe(
      (data: IBillGenType[]) => { this.BillGenType = data; });
    this.BillGenType.forEach((item: IBillGenType) => {
      this.billGenTypeNames.push(item.typeID + ': ' + item.typeName);
    });
    return this.billGenTypeNames;
  }

  onCategoryDdlChange(value) {
    // tslint:disable-next-line:no-debugger
    debugger;
      this.gridOptions.api.setColumnDefs(this.getGridColumns(value));
  }

  getGridColumns(categoryID: number) {

    if (categoryID === 1) {
      return [
        { headerName: '4-Ms', field: 'CategoryDisplay', width: 100, headerTooltip: '4 M Category', editable: true, cellEditor: 'agSelectCellEditor', cellEditorParams: { cellHeight: 50, values: this.getResourceCategory() } },
        {
          headerName: 'Goods Gp', field: 'gpIDDisplay', width: 110, headerTooltip: 'Goods groups', editable: true, cellEditor: 'agSelectCellEditor', cellEditorParams: { values: this.getPMCategories(1) }
        },
        { headerName: 'Service Gp', field: 'cfgIDDisplay', width: 110, headerTooltip: 'Service groups', editable: true, cellEditor: 'agSelectCellEditor', cellEditorParams: { values: this.getPMCategories(1) } },

        { headerName: 'Cls', field: 'clsIDDisplay', width: 110, headerTooltip: 'Usage Classification', editable: true, cellEditor: 'agSelectCellEditor', cellEditorParams: { values: this.getClassingMaster() } },
        { headerName: 'EoT', field: 'rnkID', width: 70, headerTooltip: 'Ease of Theft(1-10)', editable: true },
        { headerName: 'Wt(kg)', field: 'PropValue', width: 80, headerTooltip: 'Value(KG/CUM)', editable: true },
        { headerName: 'Resource Name', field: 'ResourceName', width: 140, headerTooltip: 'Item/Resource Name(max 25 characters)', editable: true },
        { headerName: 'Make/Spec', field: 'ResourceMake', width: 140, headerTooltip: 'Make/Brand/Specification', editable: true },
        { headerName: 'Unit', field: 'ResourceUnitName', width: 70, headerTooltip: 'Unit of Measurement', editable: true, cellEditor: 'agSelectCellEditor', cellEditorParams: { cellHeight: 50, values: this.userService.unitNames } },
        { headerName: 'Rate', field: 'ResourceRate', width: 70, headerTooltip: 'Rate(Rate per use/unit)', editable: true },
        { headerName: 'Base-Rate', field: 'ResourceBasicRate', width: 70, headerTooltip: 'Basic Rate', editable: true },
        { headerName: 'Lead ', field: 'ResourceLead', width: 70, headerTooltip: 'Lead in Km', editable: true },
        { headerName: 'LR', field: 'ResourceLeadRate', width: 70, headerTooltip: 'Lead Rate(per Km)', editable: true },
        { headerName: 'D-Days', field: 'DeliveryDays', width: 60, headerTooltip: 'Delivery Days' }
      ];
    } else if (categoryID === 2) {
      return [
        { headerName: '4-Ms', field: 'CategoryDisplay', width: 100, headerTooltip: '4 M Category', editable: true, cellEditor: 'agSelectCellEditor', cellEditorParams: { cellHeight: 50, values: this.getResourceCategory() } },
        { headerName: 'Goods Gp', field: 'gpIDDisplay', width: 110, headerTooltip: 'Goods groups', editable: true, cellEditor: 'agSelectCellEditor', cellEditorParams: { values: this.getPMCategories(1) } },
        { headerName: 'Cls', field: 'clsIDDisplay', width: 110, headerTooltip: 'Usage Classification', editable: true, cellEditor: 'agSelectCellEditor', cellEditorParams: { values: this.getClassingMaster() } },
        { headerName: 'EoT', field: 'rnkID', width: 80, headerTooltip: 'Ease of Theft(1-10)', editable: true },
        { headerName: 'Wt(kg)', field: 'PropValue', width: 80, headerTooltip: 'Value(KG/CUM)', editable: true },
        { headerName: 'Resource Name', field: 'ResourceName', width: 140, headerTooltip: 'Item/Resource Name(max 25 characters)', editable: true },
        { headerName: 'Make/Spec', field: 'ResourceMake', width: 140, headerTooltip: 'Make/Brand/Specification', editable: true },
        { headerName: 'Unit', field: 'ResourceUnitName', width: 80, headerTooltip: 'Unit of Measurement', editable: true, cellEditor: 'agSelectCellEditor', cellEditorParams: { cellHeight: 50, values: this.userService.unitNames } },
        { headerName: 'Rate', field: 'ResourceRate', width: 80, headerTooltip: 'Rate(Rate per use/unit)', editable: true },
        { headerName: 'Base-Rate', field: 'ResourceBasicRate', width: 80, headerTooltip: 'Basic Rate', editable: true },
        { headerName: 'Lead ', field: 'ResourceLead', width: 80, headerTooltip: 'Lead in Km', editable: true },
        { headerName: 'LR', field: 'ResourceLeadRate', width: 80, headerTooltip: 'Lead Rate(per Km)', editable: true },
        { headerName: 'D-Days', field: 'DeliveryDays', width: 80, headerTooltip: 'Delivery Days' }
      ];
    } else if (categoryID === 3) {
      return [{ headerName: 'GS', field: 'GSDisplay', width: 70, editable: true, cellEditor: 'agSelectCellEditor', cellEditorParams: { values: this.getGdsSvc() } },
      { headerName: '4-Ms', field: 'CategoryDisplay', width: 100, headerTooltip: '4 M Category', editable: true, cellEditor: 'agSelectCellEditor', cellEditorParams: { cellHeight: 50, values: this.getResourceCategory() } },
      { headerName: 'Goods Gp', field: 'gpIDDisplay', width: 110, headerTooltip: 'Goods groups', editable: true, cellEditor: 'agSelectCellEditor', cellEditorParams: { values: this.getPMCategories(1) } },
      { headerName: 'Cls', field: 'clsIDDisplay', width: 110, headerTooltip: 'Usage Classification', editable: true, cellEditor: 'agSelectCellEditor', cellEditorParams: { values: this.getClassingMaster() } },
      { headerName: 'EoT', field: 'rnkID', width: 70, headerTooltip: 'Ease of Theft(1-10)', editable: true },
      { headerName: 'Wt(kg)', field: 'PropValue', width: 80, headerTooltip: 'Value(KG/CUM)', editable: true },
      { headerName: 'Resource Name', field: 'ResourceName', width: 140, headerTooltip: 'Item/Resource Name(max 25 characters)', editable: true },
      { headerName: 'Make/Spec', field: 'ResourceMake', width: 140, headerTooltip: 'Make/Brand/Specification', editable: true },
      { headerName: 'Unit', field: 'ResourceUnitName', width: 70, headerTooltip: 'Unit of Measurement', editable: true, cellEditor: 'agSelectCellEditor', cellEditorParams: { cellHeight: 50, values: this.userService.unitNames } },
      { headerName: 'Rate', field: 'ResourceRate', width: 70, headerTooltip: 'Rate(Rate per use/unit)', editable: true },
      { headerName: 'Base-Rate', field: 'ResourceBasicRate', width: 70, headerTooltip: 'Basic Rate', editable: true },
      { headerName: 'Lead ', field: 'ResourceLead', width: 70, headerTooltip: 'Lead in Km', editable: true },
      { headerName: 'LR', field: 'ResourceLeadRate', width: 70, headerTooltip: 'Lead Rate(per Km)', editable: true },
      { headerName: 'D-Days', field: 'DeliveryDays', width: 60, headerTooltip: 'Delivery Days' }
      ];
    } else if (categoryID === 4) {
      return [{ headerName: 'GS', field: 'GSDisplay', width: 70, editable: true, cellEditor: 'agSelectCellEditor', cellEditorParams: { values: this.getGdsSvc() } },
      { headerName: '4-Ms', field: 'CategoryDisplay', width: 100, headerTooltip: '4 M Category', editable: true, cellEditor: 'agSelectCellEditor', cellEditorParams: { cellHeight: 50, values: this.getResourceCategory() } },
      {
        headerName: 'Goods Gp', field: 'gpIDDisplay', width: 110, headerTooltip: 'Goods groups', editable: true, cellEditor: 'agSelectCellEditor', cellEditorParams: { values: this.getPMCategories(1) }
      },
      { headerName: 'Cls', field: 'clsIDDisplay', width: 110, headerTooltip: 'Usage Classification', editable: true, cellEditor: 'agSelectCellEditor', cellEditorParams: { values: this.getClassingMaster() } },
      { headerName: 'EoT', field: 'rnkID', width: 70, headerTooltip: 'Ease of Theft(1-10)', editable: true },
      { headerName: 'Wt(kg)', field: 'PropValue', width: 80, headerTooltip: 'Value(KG/CUM)', editable: true },
      { headerName: 'Resource Name', field: 'ResourceName', width: 140, headerTooltip: 'Item/Resource Name(max 25 characters)', editable: true },
      { headerName: 'Make/Spec', field: 'ResourceMake', width: 140, headerTooltip: 'Make/Brand/Specification', editable: true },
      { headerName: 'Unit', field: 'ResourceUnitName', width: 70, headerTooltip: 'Unit of Measurement', editable: true, cellEditor: 'agSelectCellEditor', cellEditorParams: { cellHeight: 50, values: this.userService.unitNames } },
      { headerName: 'Rate', field: 'ResourceRate', width: 70, headerTooltip: 'Rate(Rate per use/unit)', editable: true },
      { headerName: 'Base-Rate', field: 'ResourceBasicRate', width: 70, headerTooltip: 'Basic Rate', editable: true },
      { headerName: 'Lead ', field: 'ResourceLead', width: 70, headerTooltip: 'Lead in Km', editable: true },
      { headerName: 'LR', field: 'ResourceLeadRate', width: 70, headerTooltip: 'Lead Rate(per Km)', editable: true },
      { headerName: 'D-Days', field: 'DeliveryDays', width: 60, headerTooltip: 'Delivery Days' }
      ];
    }
  }
}
