import 'hammerjs';
import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
// see cto mial dated 28 Sep 2018 these twwo lines are added after getting enterprise trial version of grid for 2 months
// after runnig this no error >> npm install --save ag-grid-enterprise
import {LicenseManager} from 'ag-grid-enterprise';
LicenseManager.setLicenseKey('Evaluation_License_Valid_Until__1_December_2018__MTU0MzYyMjQwMDAwMA==1ad2f30a69c8ef5812a4e7617dd15ac1');

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.log(err));
