using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace ConsoleApp {
    public class SQLDBUtil {
        #region Constants
        private const string _strSectionName = "ProMan/database";
        private const string _strItemName = "SQL";
        private static string _strConnString = ConfigurationManager.ConnectionStrings["ERPConnectionString"].ConnectionString;//ObjectDeclarationLayer.Common.getWebConfig(_strSectionName, _strItemName).ToString();
        private static int _intCommandTimeout = 1000; //Convert.ToInt32(ObjectDeclarationLayer.Common.getWebConfig(_strSectionName, "CommandTimeout").ToString());
        #endregion
        #region Constructor
        public SQLDBUtil() {
        }
        #endregion
        //Overloaded Functions for ExecuteNonQuery
        /// <summary>
        /// Execute a T-SQL Storedprocedure
        /// 
        /// </summary>
        /// <param name="sSPName"></param>
        #region ExecuteNonQuery
        public static int ExecuteNonQuery(SqlTransaction transaction, string commandText, params SqlParameter[] commandParameters) {
            if (transaction == null) throw new ArgumentNullException("transaction");
            if (transaction != null && transaction.Connection == null) throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
            // Create a command and prepare it for execution
            SqlCommand cmd = new SqlCommand();
            bool mustCloseConnection = false;
            PrepareCommand(cmd, transaction.Connection, transaction, CommandType.StoredProcedure, commandText, commandParameters, out mustCloseConnection);
            // Finally, execute the command
            int retval = cmd.ExecuteNonQuery();
            // Detach the SqlParameters from the command object, so they can be used again
            cmd.Parameters.Clear();
            return retval;
        }
        public static void ExecuteNonQuery(string sSPName) {
            SqlConnection objconnection = new SqlConnection(_strConnString);
            SqlCommand objCommand = new SqlCommand(sSPName, objconnection);
            objCommand.CommandType = CommandType.Text;
            objCommand.CommandTimeout = _intCommandTimeout;
            objCommand.Connection = objconnection;
            try {
                objCommand.Connection.Open();
                objCommand.ExecuteNonQuery();
            } catch (Exception e) {
                throw e;
            } finally {
                objCommand.Connection.Close();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sSPName"></param>
        /// <param name="parameters"></param>
        /// <param name="dummy"></param>
        /// <returns></returns>
        public static int ExecuteNonQuery(string sSPName, SqlParameter[] parameters, string dummy) {
            using (SqlConnection objconnection = new SqlConnection(_strConnString)) {
                SqlCommand objCommand = new SqlCommand(sSPName, objconnection);
                objCommand.CommandType = CommandType.StoredProcedure;
                objCommand.CommandTimeout = _intCommandTimeout;
                objCommand.Connection = objconnection;
                try {
                    objCommand.Connection.Open();
                    foreach (SqlParameter parameter in parameters) {
                        objCommand.Parameters.Add(parameter);
                    }
                    objCommand.ExecuteNonQuery();
                    return 1;
                } catch (Exception e) {
                    throw e;
                } finally {
                    objCommand.Connection.Close();
                }
            }
        }
        public static int ExecuteNonQuery(string sSPName, SqlParameter[] parameters) {
            SqlConnection objconnection = new SqlConnection(_strConnString);
            SqlCommand objCommand = new SqlCommand(sSPName, objconnection);
            objCommand.CommandType = CommandType.StoredProcedure;
            objCommand.CommandTimeout = _intCommandTimeout;
            objCommand.Connection = objconnection;
            try {
                objCommand.Connection.Open();
                foreach (SqlParameter parameter in parameters) {
                    objCommand.Parameters.Add(parameter);
                }
                objCommand.ExecuteNonQuery();
                return 1;
            } catch (Exception e) {
                throw e;
            } finally {
                objCommand.Connection.Close();
            }
        }
        public static int ExecuteNonQuery(string sSPName, SqlParameter[] parameters, int retparam) {
            SqlConnection objconnection = new SqlConnection(_strConnString);
            SqlCommand objCommand = new SqlCommand(sSPName, objconnection);
            objCommand.CommandType = CommandType.StoredProcedure;
            objCommand.CommandTimeout = _intCommandTimeout;
            objCommand.Connection = objconnection;
            try {
                objCommand.Connection.Open();
                foreach (SqlParameter parameter in parameters) {
                    objCommand.Parameters.Add(parameter);
                }
                objCommand.ExecuteNonQuery();
                return int.Parse(objCommand.Parameters["ReturnValue"].Value.ToString());
            } catch (Exception e) {
                throw e;
            } finally {
                objCommand.Connection.Close();
            }
        }
        #endregion
        //Overloaded Functions for ExecuteDataset
        #region ExecuteDataset
        public static DataSet ExecuteDataset(string sSPName) {
            SqlConnection objconnection = new SqlConnection(_strConnString);
            SqlCommand objCommand = new SqlCommand();
            SqlDataAdapter objDataAdapter = new SqlDataAdapter();
            objCommand.Connection = objconnection;
            objCommand.CommandType = CommandType.StoredProcedure;
            objCommand.CommandText = sSPName;
            objCommand.CommandTimeout = _intCommandTimeout;
            DataSet ds = new DataSet();
            try {
                objCommand.Connection.Open();
                objDataAdapter.SelectCommand = objCommand;
                objDataAdapter.Fill(ds);
                return ds;
            } catch (Exception e) {
                throw e;
            } finally {
                objCommand.Connection.Close();
            }
        }
        public static DataSet ExecuteDataset(string sSPName, SqlParameter[] parameters) {
            DataSet ds = new DataSet();
            SqlConnection objconnection = new SqlConnection(_strConnString);
            SqlCommand objCommand = new SqlCommand();
            SqlDataAdapter objDataAdapter = new SqlDataAdapter();
            objCommand.Connection = objconnection;
            objCommand.CommandType = CommandType.StoredProcedure;
            objCommand.CommandText = sSPName;
            objCommand.CommandTimeout = _intCommandTimeout;
            try {
                objCommand.Connection.Open();
                foreach (SqlParameter parameter in parameters) {
                    objCommand.Parameters.Add(parameter);
                }
                objDataAdapter.SelectCommand = objCommand;
                objDataAdapter.Fill(ds);
                return ds;
            } catch (Exception e) {
                throw e;
            } finally {
                objconnection.Close();
            }
        }
        #endregion
        //Overloaded Functions for ExecuteDataReader
        #region ExecuteDataReader
        public static SqlDataReader ExecuteDataReader(string sSPName) {
            SqlConnection objconnection = new SqlConnection(_strConnString);
            SqlCommand objCommand = new SqlCommand(sSPName, objconnection);
            objCommand.CommandType = CommandType.StoredProcedure;
            objCommand.CommandTimeout = _intCommandTimeout;
            objCommand.Connection = objconnection;
            try {
                objCommand.Connection.Open();
                return objCommand.ExecuteReader();
            } catch (Exception e) {
                throw e;
            }
        }
        public static SqlDataReader ExecuteDataReader(string sSPName, SqlParameter[] parameters) {
            SqlConnection objconnection = new SqlConnection(_strConnString);
            SqlCommand objCommand = new SqlCommand(sSPName, objconnection);
            objCommand.CommandType = CommandType.StoredProcedure;
            objCommand.CommandTimeout = _intCommandTimeout;
            objCommand.Connection = objconnection;
            try {
                objCommand.Connection.Open();
                foreach (SqlParameter parameter in parameters) {
                    objCommand.Parameters.Add(parameter);
                }
                return objCommand.ExecuteReader(CommandBehavior.CloseConnection);
            } catch (Exception e) {
                throw e;
            }
        }
        #endregion
        #region ExecuteScalar
        public static object ExecuteScalar(string commandText) {
            return ExecuteScalar(commandText, (SqlParameter[])null);
        }
        public static object ExecuteScalar(string commandText, CommandType text) {
            SqlConnection connection = new SqlConnection(_strConnString);
            if (text == CommandType.Text) {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ERPConnectionString"].ConnectionString)) {
                    SqlCommand cmd = new SqlCommand(commandText, con);
                    con.Open(); object retval = cmd.ExecuteScalar(); cmd.Dispose(); con.Close();
                    return retval;
                }
            } else {
                return ExecuteScalar(commandText, (SqlParameter[])null);
            }
        }
        public static object ExecuteScalar(string commandText, params SqlParameter[] commandParameters) {
            SqlCommand cmd = new SqlCommand();
            SqlConnection connection = new SqlConnection(_strConnString);
            bool mustCloseConnection = false;
            PrepareCommand(cmd, connection, (SqlTransaction)null, CommandType.StoredProcedure, commandText, commandParameters, out mustCloseConnection);
            object retval = cmd.ExecuteScalar();
            cmd.Parameters.Clear();
            if (mustCloseConnection) connection.Close();
            return retval;
        }
        public static object ExecuteScalar(SqlTransaction transaction, string commandText) {
            // Pass through the call providing null for the set of SqlParameters
            return ExecuteScalar(transaction, commandText, (SqlParameter[])null);
        }
        public static object ExecuteScalar(SqlTransaction transaction, string commandText, params SqlParameter[] commandParameters) {
            if (transaction == null) throw new ArgumentNullException("transaction");
            if (transaction != null && transaction.Connection == null) throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
            // Create a command and prepare it for execution
            SqlCommand cmd = new SqlCommand();
            bool mustCloseConnection = false;
            PrepareCommand(cmd, transaction.Connection, transaction, CommandType.StoredProcedure, commandText, commandParameters, out mustCloseConnection);
            // Execute the command & return the results
            object retval = cmd.ExecuteScalar();
            // Detach the SqlParameters from the command object, so they can be used again
            cmd.Parameters.Clear();
            return retval;
        }
        private static void PrepareCommand(SqlCommand command, SqlConnection connection, SqlTransaction transaction, CommandType commandType, string commandText, SqlParameter[] commandParameters, out bool mustCloseConnection) {
            if (command == null) throw new ArgumentNullException("command");
            if (commandText == null || commandText.Length == 0) throw new ArgumentNullException("commandText");
            // If the provided connection is not open, we will open it
            if (connection.State != ConnectionState.Open) {
                mustCloseConnection = true;
                try {
                    connection.Open();
                } catch (Exception e) {
                    string msg = "Database connection Error! Maybe your Database is not runing or database connection string is mistake?";
                    throw new Exception(msg, e);
                }
            } else {
                mustCloseConnection = false;
            }
            // Associate the connection with the command
            command.Connection = connection;
            // Set the command text (stored procedure name or SQL statement)
            command.CommandText = commandText;
            // If we were provided a transaction, assign it
            if (transaction != null) {
                if (transaction.Connection == null) throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
                command.Transaction = transaction;
            }
            // Set the command type
            command.CommandType = commandType;
            // Attach the command parameters if they are provided
            if (commandParameters != null) {
                AttachParameters(command, commandParameters);
            }
            return;
        }
        private static void AttachParameters(SqlCommand command, SqlParameter[] commandParameters) {
            if (command == null) throw new ArgumentNullException("command");
            if (commandParameters != null) {
                foreach (SqlParameter p in commandParameters) {
                    if (p != null) {
                        // Check for derived output value with no value assigned
                        if ((p.Direction == ParameterDirection.InputOutput ||
                            p.Direction == ParameterDirection.Input) &&
                            (p.Value == null)) {
                            p.Value = DBNull.Value;
                        }
                        command.Parameters.Add(p);
                    }
                }
            }
        }
        #endregion ExecuteScalar
        #region ExecuteReader
        /// <summary>
        /// This enum is used to indicate whether the connection was provided by the caller, or created by SqlHelper, so that
        /// we can set the appropriate CommandBehavior when calling ExecuteReader()
        /// </summary>
        private enum SqlConnectionOwnership {
            /// <summary>Connection is owned and managed by SqlHelper</summary>
            Internal,
            /// <summary>Connection is owned and managed by the caller</summary>
            External
        }
        private static SqlDataReader ExecuteReader(SqlTransaction transaction, string commandText, SqlParameter[] commandParameters, SqlConnectionOwnership connectionOwnership) {
            bool mustCloseConnection = false;
            // Create a command and prepare it for execution
            SqlCommand cmd = new SqlCommand();
            SqlConnection connection = new SqlConnection();
            if (transaction == null) {
                connection = new SqlConnection(_strConnString);
            } else {
                connection = transaction.Connection;
            }
            try {
                PrepareCommand(cmd, connection, transaction, CommandType.StoredProcedure, commandText, commandParameters, out mustCloseConnection);
                // Create a reader
                SqlDataReader dataReader;
                // Call ExecuteReader with the appropriate CommandBehavior
                if (connectionOwnership == SqlConnectionOwnership.External) {
                    dataReader = cmd.ExecuteReader();
                } else {
                    dataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                }
                bool canClear = true;
                foreach (SqlParameter commandParameter in cmd.Parameters) {
                    if (commandParameter.Direction != ParameterDirection.Input)
                        canClear = false;
                }
                if (canClear) {
                    cmd.Parameters.Clear();
                }
                return dataReader;
            } catch {
                if (mustCloseConnection)
                    connection.Close();
                throw;
            }
        }
        public static SqlDataReader ExecuteReader(string commandText) {
            // Pass through the call providing null for the set of SqlParameters
            return ExecuteReader(commandText, (SqlParameter[])null);
        }
        public static SqlDataReader ExecuteReader(string commandText, params SqlParameter[] commandParameters) {
            return ExecuteReader(null, commandText, commandParameters, SqlConnectionOwnership.Internal);
        }
        public static SqlDataReader ExecuteReader(SqlTransaction transaction, string commandText) {
            // Pass through the call providing null for the set of SqlParameters
            return ExecuteReader(transaction, commandText, (SqlParameter[])null, SqlConnectionOwnership.External);
        }
        public static SqlDataReader ExecuteReader(SqlTransaction transaction, string commandText, params SqlParameter[] commandParameters) {
            if (transaction == null) throw new ArgumentNullException("transaction");
            if (transaction != null && transaction.Connection == null) throw new ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction");
            // Pass through to private overload, indicating that the connection is owned by the caller
            return ExecuteReader(transaction, commandText, commandParameters, SqlConnectionOwnership.External);
        }
        #endregion ExecuteReader
        public static bool IsDBNull(string dbvalue) {
            return (dbvalue == DBNull.Value.ToString().Trim());
        }
    }
}
