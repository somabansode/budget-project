using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;

namespace ConsoleApp {
    class Program {
        static void Main(string[] args) {
            menuAll();
            getRoleMenu();
            MenuHreferences();
            Console.WriteLine(clRoleMenu.Count);
            Console.ReadKey();
        }
        public static List<AppMenu> clMenuAll = new List<AppMenu>();
        public static List<RoleMenu> clRoleMenu = new List<RoleMenu>();
        public static List<AppMenu> userAppMenu = new List<AppMenu>();
        public static List<MenuPathHref> clMenuPaths = new List<MenuPathHref>();

        public static void menuAll() {
            try {
                DataSet ds = SQLDBUtil.ExecuteDataset("sg_menuAll"); // if this SP is slow use other SP 'sg_menuAll_Admin'
                foreach (DataRow dr in ds.Tables[0].Rows) {
                    AppMenu mnu = new AppMenu(1, dr["Title"].ToString(), Convert.ToInt32(dr["MenuID"]), dr["MenuName"].ToString(),
                        dr["MenuGroup"].ToString(), dr["URL"].ToString()
                        , dr["Under"] == DBNull.Value ? -1 : (int)dr["Under"]
                        , dr["PreOrder"] == DBNull.Value ? -1 : (int)dr["PreOrder"]
                        , (int)dr["Level"], dr["Help"].ToString());
                    clMenuAll.Add(mnu);
                }
            } catch (Exception ex) {
                throw ex;
            }
        }
        private static void getRoleMenu() {

            string erpCS = ConfigurationManager.ConnectionStrings["ERPConnectionString"].ConnectionString;
            DataSet ds = SQLDBUtil.ExecuteDataset("sg_roleBasedMenu", new SqlParameter[] {
                            new SqlParameter("@ModuleID", 1), new SqlParameter("@RoleId", 6) });
            foreach (DataRow dr in ds.Tables[0].Rows) {
                RoleMenu rm = new RoleMenu(Convert.ToInt32(dr["MenuID"]), Convert.ToInt32(dr["ModuleID"]), Convert.ToInt32(dr["RoleID"])
                    , Convert.ToInt32(dr["Allowed"]), Convert.ToInt32(dr["Editable"]), Convert.ToInt32(dr["ViewAll"]));
                if (!clRoleMenu.Contains(rm)) clRoleMenu.Add(rm);
            }

        }
        private static void MenuHreferences() {
            clMenuPaths = new List<MenuPathHref>();
            var nwArray  = (from t1 in clMenuAll join t2 in clRoleMenu on t1.MenuID equals t2.MenuID where t2.ModuleID == 1
                          select t1 ).ToList();
            //DataTable dt = ConvertList2DataTable<AppMenu>(nwArray);
            foreach (AppMenu am in nwArray) {
                if (am.Under != -1 && am.URL != "") {
                    MenuPathHref mp = new MenuPathHref(
                        am.MenuID, 
                        am.MenuGroup + " -> " + am.MenuName + " -> " + am.URL,
                         buildHref(am.MenuID, am.URL).Replace(" -> " + am.URL,"")
                        );
                    clMenuPaths.Add(mp);
                }
            }
            //DataTable  dt2 = ConvertList2DataTable<MenuPathHref>(clMenuPaths);
        }
        public class MenuPathHref {
            public int MenuID { get; set; }
            public string Path { get; set; }
            public string Href { get; set; }
            public MenuPathHref(int menuID, string path, string href) {
                MenuID = menuID; Path = path; Href = href;
            }
        }
        private static string buildHref(int menuID, string pageURL) {
            string xPath = string.Empty;
            var varPar = (from t in clMenuAll where t.MenuID == menuID select new { t.Under, t.MenuName, t.URL }).FirstOrDefault();
            if (varPar != null) {
                int parent = varPar.Under;
                if (varPar.URL == "") xPath = varPar.MenuName;
                else xPath = "<a href='" + varPar.URL + "' >" + varPar.MenuName + "</a>";
                if (parent > 0) xPath = buildHref(parent, xPath);
            }
            if (pageURL != "") xPath = xPath + " -> " + pageURL;
            return xPath;
        }
        private static string getPath( int menuID, string pageURL) {
            string xPath = string.Empty;
            var varPar = (from t in clMenuAll where t.MenuID == menuID select new { t. Under, t.MenuName }).FirstOrDefault();
            if (varPar != null) {
                int parent = varPar.Under;
                xPath = varPar.MenuName;
                if (parent > 0) xPath = getPath(parent, xPath);
                if (pageURL != "") xPath = xPath + " -> " + pageURL;
            } else return pageURL;
            return xPath;
        }
        public static DataTable ConvertList2DataTable<T>(List<T> items) {
            DataTable dataTable = new DataTable(typeof(T).Name);
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);//Get all the properties
            foreach (PropertyInfo prop in Props) { dataTable.Columns.Add(prop.Name); }//Setting column names as Property names
            foreach (T item in items) {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++) { values[i] = Props[i].GetValue(item, null); }//inserting property values to datatable rows
                dataTable.Rows.Add(values);
            }
            return dataTable;//put a breakpoint here and check datatable
        }
    }
    public class RoleMenu {
        public int MenuID { get; set; }
        public int ModuleID { get; set; }
        public int RoleID { get; set; }
        public int Allowed { get; set; }
        public int Editable { get; set; }
        public int ViewAll { get; set; }
        public RoleMenu(int menuID, int moduleID, int roleID, int allowed, int editable, int viewAll) {
            MenuID = menuID; ModuleID = moduleID; RoleID = roleID; Allowed = allowed; Editable = editable; ViewAll = viewAll;
        }
    }
    public class AppMenu {
        public int ModID { get; set; }
        public string Title { get; set; }
        public int MenuID { get; set; }
        public string MenuName { get; set; }
        public string MenuGroup { get; set; }
        public string URL { get; set; }
        public int Under { get; set; }
        public int PreOrder { get; set; }
        public int Level { get; set; }
        public string Help { get; set; }
        public AppMenu(int modId, string title, int menuId, string menuName, string menuGp, string url, int under, int preOrder, int level, string help) {
            ModID = modId; Title = title; MenuID = menuId; MenuName = menuName; MenuGroup = menuGp; URL = url; Under = under; PreOrder = preOrder; Level = level; Help = help;
        }

    }
}
